<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<title> ZTREE DEMO - checkbox</title>
	<link rel="stylesheet" href="../../TopJUI/ztree/css/demo.css">
	<link rel="stylesheet" href="../../TopJUI/ztree/css/metroStyle/metroStyle.css">
	<script type="text/javascript" src="../../TopJUI/static/plugins/jquery/jquery.min.js"></script>
	<script type="text/javascript" src="../../TopJUI/ztree/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="../../TopJUI/ztree/js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="../../TopJUI//ztree/js/jquery.ztree.exedit.js"></script>

	<script type="text/javascript">
		var setting = {
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			},
            callback:{
                onCheck:onCheck
            }
		};

 		var zNodes =[
 			{ id:1, pId:0, name:"随意勾选 1", open:true},
 			{ id:11, pId:1, name:"随意勾选 1-1", open:false},
 			{ id:111, pId:11, name:"随意勾选 1-1-1"},
 			{ id:112, pId:11, name:"随意勾选 1-1-2"},
 			{ id:12, pId:1, name:"随意勾选 1-2", open:false},
 			{ id:121, pId:12, name:"随意勾选 1-2-1"},
 			{ id:122, pId:12, name:"随意勾选 1-2-2"},
			{ id:2, pId:0, name:"随意勾选 2", checked:false, open:false},
 			{ id:21, pId:2, name:"随意勾选 2-1"},
 			{ id:22, pId:2, name:"随意勾选 2-2", open:false},
 			{ id:221, pId:22, name:"随意勾选 2-2-1", checked:false},
 			{ id:222, pId:22, name:"随意勾选 2-2-2"},
 			{ id:23, pId:2, name:"随意勾选 2-3"},
			
			
 			{ id:3, pId:0, name:"随意勾选 3", checked:false, open:false},
 			{ id:231, pId:3, name:"随意勾选 3-1"},
 			{ id:232, pId:3, name:"随意勾选 3-2", open:false},
 			{ id:2321, pId:232, name:"随意勾选 3-2-1", checked:false},
 			{ id:2322, pId:232, name:"随意勾选 3-2-2"},
 			{ id:233, pId:3, name:"随意勾选 3-3"}		
 ];
		
		var code;
		
		function setCheck() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			py = $("#py").attr("checked")? "p":"",
			sy = $("#sy").attr("checked")? "s":"",
			pn = $("#pn").attr("checked")? "p":"",
			sn = $("#sn").attr("checked")? "s":"",
			type = { "Y":py + sy, "N":pn + sn};
			zTree.setting.check.chkboxType = type;
// 			showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
// 			showCode('setting.check.chkboxType = { "Y" : "", "N" : "" };');
			showCode('setting.check.chkboxType = { "Y" : "s", "N" : "ps" };');
			
// 			setting.check.chkboxType = { "Y" : "", "N" : "" };
		}
		function showCode(str) {
			if (!code) code = $("#code");
			code.empty();
			code.append("<li>"+str+"</li>");
		}
		
		/* var zNodes =[];
		$(document).ready(function(){
			var t = $("#treeDemo");
			$.ajax({
		    	type: "POST", 
				url: "/Units/ListTree",
		        dataType: 'json',
				success: function(result) { 
					console.log(result);
					$.extend( true, zNodes, result );
					console.log(zNodes);
		             t = $.fn.zTree.init(t, setting, zNodes);
					// demoIframe = $("#testIframe");
				} 
		    });
		}); */
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			setCheck();
			$("#py").bind("change", setCheck);
			$("#sy").bind("change", setCheck);
			$("#pn").bind("change", setCheck);
			$("#sn").bind("change", setCheck);
			
 		});
		
		function onCheck(e,treeId,treeNode){
            var treeObj=$.fn.zTree.getZTreeObj("treeDemo"),
            nodes=treeObj.getCheckedNodes(true),
            v="";
            for(var i=0;i<nodes.length;i++){
            v+=nodes[i].name + ",";
            alert(nodes[i].id); //获取选中节点的值
            }
		}
		
	            
	</script>
</head>

<body>
		<ul id="treeDemo" class="ztree" style="background: white;height: inherit;margin-top: 295px;"></ul>
			<!-- <input type="checkbox" id="py" class="checkbox first" checked style="display:none;" />
			<input type="checkbox" id="sy" class="checkbox first" checked style="display:none;"/>
			<input type="checkbox" id="pn" class="checkbox first" checked style="display:none;"/>
			<input type="checkbox" id="sn" class="checkbox first" checked style="display:none;"/> -->
						
</body>
</html>