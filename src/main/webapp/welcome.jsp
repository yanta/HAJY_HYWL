<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="com.sdry.model.zc.ZcSysUserEntity" %>
<% ZcSysUserEntity zcSysUserEntity =  (ZcSysUserEntity)request.getSession().getAttribute("user"); %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>恒安捷运物流仓储系统</title>
        <meta name="renderer" content="webkit">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
        <jsp:include page="resource_path.jsp" flush="true"/>
    </head>
    <body>
	    <div class="x-body layui-anim layui-anim-up">
	        <blockquote class="layui-elem-quote">欢迎您：
	            <span class="x-red"><%=zcSysUserEntity.getUserName() %></span></blockquote>
	    </div>
    </body>
</html>