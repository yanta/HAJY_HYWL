<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>恒安捷运项目公用资源路径</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>
    <link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/css/font.css">
    <link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/lib/layui/css/layui.css">
	<link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/css/xadmin.css">
    <link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/lib/toastr/toastr.min.css">
    
    <script charset="utf-8" src="${pageContext.request.contextPath }/lib/jquery/jquery-3.3.1.js"></script>
    <script charset="utf-8" src="${pageContext.request.contextPath }/lib/layui/layui.js"></script>
    <script charset="utf-8" src="${pageContext.request.contextPath }/js/xadmin.js"></script>
    <script charset="utf-8" src="${pageContext.request.contextPath }/lib/md5/md5.js"></script>
    <script charset="utf-8" src="${pageContext.request.contextPath }/lib/toastr/toastr.min.js"></script>
    <!-- zc start -->
    <style type="text/css">
		.layui-table-click{
		    background-color:#ddf2e9 !important;
		}
	</style>
	<!-- zc end -->
    <script type="text/javascript">
	  	//提示框样式
		function toastrStyle(){
			toastr.options = {
				"closeButton": false,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-center",
				"preventDuplicates": false,
				"onclick": null,
				"showDuration": "5000",
				"hideDuration": "1000",
				"timeOut&quot": "100000",
				"extendedTimeOut": "0",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut",
			};
		}
    </script>
    <script type="text/javascript">
		$(function () {
			$(".xing").html("*");
		});
	</script>
	<style type="text/css">
		.xing
		{
			color: red;
			float: left;
			display: block;
			width: 0.1%;
			margin-top:-25px;
			margin-left: 83%; 
		}
	</style>
</html>