<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>监控看板</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>监控看板</cite>
			</a>
		</span>
	</div>
	
	<!-- 轮播 -->
    <div class="layui-carousel" id="test1">
      <div carousel-item>
        <div><table class="layui-hide" id="stockTable" lay-filter="stockTable"></table></div>
        <div><table class="layui-hide" id="putStockTable" lay-filter="putStockTable"></table></div>
        <div><table class="layui-hide" id="outStockTable" lay-filter="outStockTable"></table></div>
      </div>
    </div>
	
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','laydate', 'carousel'], function(){
			var table = layui.table //表格
			,layer = layui.layer	//弹层
			,laydate = layui.laydate	//日期
			,carousel = layui.carousel;	//轮播
			
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			
			/* 仓库物料列表 */
			table.render({
				elem: '#stockTable'
				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getLocationStock.do'
				,title: '库存列表'
				,id : 'stockTable'
				,even:true	//开启隔行变色
				,cols: [
					[
						{field:'', title:'库位物料现状',colspan: 10, align:'center'},
					],
					[
						{field:'', title:'序号', type:'numbers', align:'center'},
					  	{field:'warehouseName', title:'仓库名称', align:'center'},
					  	{field:'regionName', title:'库区名称', align:'center'},
					  	{field:'locationName', title:'库位名称', align:'center'},
					  	{field:'locationStatus', title:'库位状态', align:'center'},
					  	{field:'materielName', title:'物料名称', align:'center'},
					  	{field:'materielQuantity', title:'物料数量', align:'center'},
					  	{field:'upper', title:'上限预警值', align:'center'},
					  	{field:'floor', title:'下限预警值', align:'center'},
					  	{field:'remark', title:'备注', align:'center'},
					]]
				,page: false
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                    /*   $('td').css({
                    'color': '#fff','font-weight':'bold',
                    'font-size': 10,
	                })*/
	                /* 设置隔行变色的颜色 */
	               /* var that = this.elem.next();
	  		        res.data.forEach(function (item, index) {
	  		        	//console.log(item.empName);item表示每列显示的数据
	  		            if (index%2==0) {
	  		                 var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#ee9300");
	  		            } else{
	  		            	var tr = that.find(".layui-table-box tbody tr[data-index='" + index + "']").css("background-color", "#33ff33");
	  		            } 
	  		        }); */
                }
			});
			
			/* 入库记录 */
			table.render({
				elem: '#putStockTable'
				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getPutStock.do'
				,title: '实时监控'
				,id :'putStockTable'
				,even:true	//开启隔行变色
				,cols: [
					[
						{field:'', title:'入库记录',colspan: 11, align:'center'},
					],
					[
					  	{field:'', title:'序号', type:'numbers', align:'center'},
					  	{field:'materielName', title:'物料名称', align:'center'},
					  	{field:'singleNum', title:'单品数量', align:'center'},
					  	{field:'unitNum', title:'单位数量', align:'center'},
					  	{field:'container', title:'容器名称', align:'center'},
					  	{field:'containerNum', title:'容器数量', align:'center'},
					  	{field:'nogoodNum', title:'不良数量', align:'center'},
					  	{field:'comeNum', title:'入库数量', align:'center'},
					  	{field:'pici', title:'批次', align:'center'},
					  	{field:'comeNumber', title:'入库单号', align:'center'},
					  	{field:'remark', title:'备注', align:'center'}
				]]
				,page: false
				,done : function(res, curr, count){// 表格渲染完成之后的回调
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
			/* 出库记录 */
			table.render({
				elem: '#outStockTable'
				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getOutStock.do'
				,title: '实时监控'
				,id :'outStockTable'
				,even:true	//开启隔行变色
				,cols: [
					[
						{field:'', title:'出库记录',colspan: 13, align:'center'},
					],
					[
					  	{field:'', title:'序号', type:'numbers', align:'center'},
					  	{field:'materiel_name', title:'物料名称', align:'center'},
					  	{field:'materiel_num', title:'产品码', align:'center'},
					  	{field:'brevity_num', title:'简码', align:'center'},
					  	{field:'packing_quantity', title:'包装数量', align:'center'},
					  	{field:'unit', title:'单位', align:'center'},
					  	{field:'mBatch', title:'批次', align:'center'},
					  	{field:'mNum', title:'数量', align:'center'},
					  	{field:'warehouse_name', title:'仓库', align:'center'},
					  	{field:'region_name', title:'库区', align:'center'},
					  	{field:'location_name', title:'库位', align:'center'},
					  	{field:'required_out_quantity', title:'需求数量', align:'center'},
					  	{field:'actual_quantity', title:'实际数量', align:'center'}
				]]
				,page: false
				,done : function(res, curr, count){// 表格渲染完成之后的回调
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
			//轮播实例
			carousel.render({
				elem: '#test1'
			    ,width: '90%' //设置容器宽度(百分比，像素都可以)
			    ,height: '300px'
			    ,arrow: 'always' //不显示箭头   always 始终显示  hover 悬停显示
			    ,anim: 'fade' //切换动画方式（渐隐渐现） updown上下切换  default上下切换
			    ,interval:'1000' //自动切换时间，默认3000，不能小于800，单位 毫秒ms
			    ,full:'false' //是否全屏轮播
			});
		});
	</script>
</body>
</html>