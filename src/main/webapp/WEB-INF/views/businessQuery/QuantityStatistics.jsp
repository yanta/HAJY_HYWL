<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>数量统计</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>业务查询</cite>
			</a>
		</span>
	</div>
	<!-- 顶部搜索框 -->
<div class="x-body">
     <div class="layui-row">
		<form class="layui-form layui-col-md12 x-so" onsubmit="return false">
			<input type="text" name="keyword" id="keyword"  placeholder="请输入物料名称" autocomplete="off" class="layui-input">
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</form>
	</div>
	<!-- 数量统计列表 -->
 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword = $("#keyword").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword":keyword}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			/* 特殊订单列表 */
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/materielQuantityStatistics.do'
				,title: '数量统计'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [[
					{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
					{field:'materiel_name', title:'物料名称', align:'center'},
				  	{field:'materiel_num', title:'产品码', align:'center'},
				  	{field:'brevity_num', title:'简码', align:'center'},
				  	{field:'unit', title:'单位', align:'center'},
				  	{field:'putQuantity', title:'入库数量', align:'center'},
				  	{field:'outQuantity', title:'出库数量', align:'center'},
				  	{field:'stockQuantity', title:'库存数量', align:'center'},
				]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
		});
	</script>
</body>
</html>