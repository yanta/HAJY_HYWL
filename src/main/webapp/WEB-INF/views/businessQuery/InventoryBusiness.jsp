<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>盘点业务</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>业务查询</cite>
			</a>
		</span>
	</div>
	<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<select id="keyword01">
								<option value="">请选择盘点结果</option>
								<option>盈亏</option>
								<option>盈余</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
     	<!-- 盘点记录列表 -->
	 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					table.reload('contenttable',{
						method:'get',
						where:{"loss_spillover_type":keyword01}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			/* 特殊订单列表 */
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/queryInventory/listPageInventory.do'
				,title: '盘点记录'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [[
					{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
					{field:'inventory_order_num', title:'盘点单号', align:'center'},
					{field:'inventory_type', title:'盘点类型', align:'center'},
					{field:'inventory_man', title:'创建人', align:'center'},
					{field:'inventory_time', title:'创建时间', align:'center'},
					{field:'loss_spillover_type', title:'损溢类型', align:'center'},
					{field:'inventory_before', title:'盘点前', align:'center'},
					{field:'inventory_after', title:'盘点后', align:'center'},
					{field:'inventory_different', title:'盘点差异', align:'center'},
					{field:'different_remark', title:'差异备注', align:'center'},
					{field:'approval_name', title:'审批人', align:'center'},
				]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
		});
	</script>
</body>
</html>