<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>特殊订单查询</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>业务查询</cite>
			</a>
		</span>
	</div>
	<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<select id="keyword01">
								<option value="">请选择订单类型</option>
								<option>空进空出单</option>
								<option>补货单</option>
								<option>异常单</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
     	<!-- 特殊订单列表 -->
	 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					var keyword02 = $("#keyword02").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword01":keyword01, "keyword02":keyword02}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			/* 特殊订单列表 */
			table.render({
				elem: '#tableList'
				,url:'/json/json1.json'
				//,toolbar: '#toolbar'
				,title: '特殊订单'
				,id :'contenttable'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,cols: [[
					{field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
					{field:'number', title:'订单编号', align:'center'},
					{field:'deliver', title:'发货单位', align:'center'},
					{field:'shipAddress', title:'发货地址', align:'center'},
					{field:'shipPeople', title:'发货人', align:'center'},
					{field:'shipPhone', title:'发货人电话', align:'center'},
					{field:'supplier', title:'收货单位', align:'center'},
					{field:'supplierAddress', title:'收货地址', align:'center'},
					{field:'supplierPeople', title:'收货人人', align:'center'},
					{field:'supplierPhone', title:'收货人电话', align:'center'},
					{field:'reason', title:'订单类型', align:'center'},
					{field:'remark', title:'备注', align:'center'}
				]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
		});
	</script>
</body>
</html>