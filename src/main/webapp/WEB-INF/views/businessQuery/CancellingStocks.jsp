<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退库</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>业务查询</cite>
			</a>
		</span>
	</div>
	<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<select id="keyword01">
								<option value="">请选择退库原因</option>
								<option>破损</option>
								<option>废弃</option>
								<option>不合格</option>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
	<!-- 退库单行编辑 -->
 	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="take"><i class="layui-icon layui-icon-search"></i>收货记录</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="accept"><i class="layui-icon layui-icon-search"></i>验收记录</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="putaway"><i class="layui-icon layui-icon-search"></i>上架记录</a>
	</script>
    <!-- 退库单列表 -->
 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
    	
    <!-- 销退收货记录列表 -->
    <div id="div1" hidden>
 	<table class="layui-hide" id="receivingRecord" lay-filter="receivingRecord"></table>
    </div>
 	
    <!-- 销退验收记录列表 -->
 	<div id="div2" hidden>
 	<table class="layui-hide" id="inspectionRecord" lay-filter="inspectionRecord"></table>
    </div>
    	
    <!-- 销退上架记录列表 -->
 	<div id="div3" hidden>
 	<table class="layui-hide" id="onRecord" lay-filter="onRecord"></table>
    </div>
	 	
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					table.reload('contenttable',{
						method:'get',
						where:{"withdrawingReason":keyword01}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			/* 退库单列表 */
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/CancellingStockManager/listPageCancellingStock.do'
				,title: '退货单'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [
					[
						{field:'', title:'退库单',colspan: 15, align:'center'},
					],
					[
					  	{field:'', title:'序号', type:'numbers', align:'center'},
					  	{field:'withdrawingNumber', title:'退库单号', align:'center'},
					  	{field:'withdrawingDate', title:'退库时间', align:'center'},
					  	{field:'withdrawingUnit', title:'仓库名称', align:'center'},
					  	{field:'withdrawingAdress', title:'仓库地址', align:'center'},
					  	{field:'withdrawingOne', title:'仓库管理员', align:'center'},
					  	{field:'withdrawingCall', title:'管理员电话', align:'center'},
					  	{field:'customerName', title:'供应商名称', align:'center'},
					  	{field:'customerAdress', title:'供应商地址', align:'center'},
					  	{field:'contactsName', title:'供应商负责人', align:'center'},
					 	{field:'contactsTel', title:'负责人电话', align:'center'},
					  	{field:'withdrawingReason', title:'退库原因', align:'center'},
					  	{field:'remark01', title:'状态', align:'center'},
					  	{field:'remarks', title:'备注', align:'center'},
					  	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:300, align: 'center'}
					]
				]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				var id = data.csid;
				var idArr = new Array();
				if(obj.event === "take"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '销退的收货记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div1')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#receivingRecord'
			        				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getPutStockByCancellingStockId.do?id='+id
			        				,title: '销退的收货记录'
			        				,id :'receivingRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        					  	{field:'receiveNumber', title:'收货单号', align:'center'},
			        						{field:'materielName', title:'物料名称', align:'center'},
			        					  	{field:'remark', title:'批次', align:'center'},
			        					  	{field:'sendNum', title:'发货数量', align:'center'},
			        					  	{field:'receiveNum', title:'收货数量', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "accept"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '销退的验收记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div2')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#inspectionRecord'
			        				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getCheckStockByCancellingStockId.do?id='+id
			        				,title: '销退的验收记录'
			        				,id :'inspectionRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'materielName', title:'物料名称', align:'center'},
			        					  	{field:'pici', title:'批次号', align:'center'},
			        					  	{field:'cnum', title:'检查数量', align:'center'},
			        					  	{field:'snum', title:'破损数量', align:'center'},
			        					  	{field:'hnum', title:'合格数量', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "putaway"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '销退的上架记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div3')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#onRecord'
			        				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/getPutawayByCancellingStockId.do?id='+id
			        				,title: '销退的上架记录'
			        				,id :'onRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'update', title:'上架时间', align:'center'},
			        					  	{field:'upname', title:'上架人', align:'center'},
			        					  	{field:'warehouseName', title:'仓库名称', align:'center'},
			        					  	{field:'regionName', title:'库区名称', align:'center'},
			        					  	{field:'locationName', title:'库位名称', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}
			});
		});
	</script>
</body>
</html>