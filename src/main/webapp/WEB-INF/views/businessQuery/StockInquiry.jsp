<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>库存查询</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>业务查询</cite>
			</a>
		</span>
	</div>
	
<input type="text" id="regionId" hidden="hidden">
<input type="text" id="locationId" hidden="hidden">
<!-- 顶部搜索框 -->
<div class="x-body">
	<div class="layui-row">
		<form class="layui-form layui-col-md12 x-so" onsubmit="return false">
			<input type="text" name="keyword" id="keyword01"  placeholder="请输入批号" autocomplete="off" class="layui-input">
			<div class="layui-input-inline">
	        	<select class="layui-input" name="keyword01" id="keyword01" lay-filter="keyword02" style="width:200px">
					<option value="">请选择库区</option>
					<c:forEach items="${regionList}" var="region">
						<option value="${region.id}">${region.region_name}</option>
					</c:forEach>
				</select>
	      	</div>
	      	<div class="layui-input-inline">
	        	<select class="layui-input" name="keyword03" id="keyword03" lay-filter="keyword03" style="width:200px">
					<option value="">请选择库位</option>
					<c:forEach items="${locationList}" var="location">
						<option value="${location.id}">${location.location_name}</option>
					</c:forEach>
				</select>
	      	</div>
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</form>
	</div>
     	<!-- 库存列表 -->
	 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					var keyword02 = $("#regionId").val();
					var keyword03 = $("#locationId").val();
					table.reload('inventoryID',{
						method:'get',
						where:{"mBatch":keyword01, "regionId":keyword02, "locationId":keyword03}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			
			form.on('select(keyword02)', function(data){
		        $('#regionId').val(data.value);
		    });
			
			form.on('select(keyword03)', function(data){
		        $('#locationId').val(data.value);
		    });
			
			/* 仓库物料列表 */
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/inquiryBusinessManager/listPageInventoryInfo.do'
				,title: '库存列表'
				,id : 'inventoryID'
				,limits:[10,20,30]
				//,totalRow: true //开启合计行
				,cols: [[
					  	{field:'materiel_name', title:'物料名称', align:'center', totalRowText: '合计：'},
					  	{field:'container_name', title:'容器名称', align:'center'},
					  	{field:'code', title:'条码', align:'center'},
					  	{field:'mBatch', title:'批次', align:'center'},
					  	{field:'mNum', title:'数量', align:'center'/* , totalRow: true */},
					  	{field:'enterDate', title:'入库日期', align:'center'},
					  	{field:'enterPerson', title:'入库人', align:'center'},
					  	{field:'warehouse_name', title:'仓库名称', align:'center'},
					  	{field:'region_name', title:'库区名称', align:'center'},
					  	{field:'location_name', title:'库位名称', align:'center'}
					]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
		});
	</script>
</body>
</html>