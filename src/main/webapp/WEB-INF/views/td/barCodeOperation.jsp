<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>条码操作记录</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>条码操作记录</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
		<div class="layui-row">
			<div class="layui-form">
				<div class="layui-input-inline">

					<div class="layui-input-inline" >
						<input class="layui-input" name="startDate" id="startDate" placeholder="请输入开始时间" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</div>

					<div class="layui-input-inline">
						&nbsp;至
					</div>

					<div class="layui-input-inline">
						<input class="layui-input" name="endDate" id="endDate" placeholder="请输入结束时间" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</div>


					<div class="layui-input-inline">
						<select style="width: 285px" id="selectType"   lay-filter="selectType">
							<option value="TYPE_STATE_KK">请选择类型</option>
							<option value="TYPE_STATE_SH">收货</option>
							<option value="TYPE_STATE_ZJ">质检</option>
							<option value="TYPE_STATE_SJ">上架</option>
							<option value="TYPE_STATE_CK">出库</option>
							<option value="TYPE_STATE_CX">拆箱</option>
						</select>
					</div>

					<div class="layui-input-inline">
						<select style="width: 285px" id="selectState"   lay-filter="selectState">
							<option value="TYPE_STATE_KK">请选择状态</option>
							<option value="TYPE_STATE_CG">成功</option>
							<option value="TYPE_STATE_SB">失败</option>
						</select>
					</div>

					<div class="layui-input-inline">
						<input class="layui-input" id="barCode" placeholder="请输入条码/零件号/人员" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</div>
					<div class="layui-input-inline">
						<input type="hidden" id="dateTime" onchange="myFunction()">
					</div>

					<div class="layui-input-inline">
						<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>		</div>
				</div>

			</div>
		</div>




	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script src="${pageContext.request.contextPath }/js/td/layuiRowspan.js" charset="utf-8"></script>
	<script type="text/javascript" charset="utf-8" language="JavaScript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//执行一个laydate实例
			laydate.render({
				elem: '#startDate', //指定元素
				type: 'date'
			});

			laydate.render({
				elem: '#endDate', //指定元素
				type: 'date'
			});


			var $ = layui.jquery, active = {
				reload:function () {

	                var selectType = $("#selectType").val();
	                var selectState = $("#selectState").val();
	                var startDate = $("#startDate").val();
	                var endDate = $("#endDate").val();
	                var barCode = $.trim($("#barCode").val());

					//var barCode = encodeURIComponent(barCode);

	                console.info("selectType="+selectType);
	                console.info("selectState="+selectState);
	                console.info("startDate="+startDate);
	                console.info("endDate="+endDate);
	                console.info("barCode="+barCode);


					table.reload('tableList',{
						method:'post'
						,where:{
							'operatingTime':startDate,
							'locationId':endDate,
							'result':selectState,
							'type':selectType,
							'barCode':barCode
						}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			//$("#date").val(curentDate())


			var selectType = $("#selectType").val();
			var selectState = $("#selectState").val();
			var startDate = $("#startDate").val();
			var endDate = $("#endDate").val();
			var barCode = $.trim($("#barCode").val());
			//var barCode = encodeURIComponent(barCode);

			console.info("selectType="+selectType);
			console.info("selectState="+selectState);
			console.info("startDate="+startDate);
			console.info("endDate="+endDate);
			console.info("barCode="+barCode);


			table.render({
				elem: '#tableList'
				,method:'post'
				,where:{
					'operatingTime':startDate,
					'locationId':endDate,
					'result':selectState,
					'type':selectType,
					'barCode':barCode
				}
                ,url:'${pageContext.request.contextPath }/barCodeOperation/list.do'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
				,page:true
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                    type: 'checkbox',
		                    fixed: 'left'
		                }, {
		                	field:'id',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                } , {
		                    field: 'operator',
		                    title: '人员',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.user){
		                    		value = row.user.userName;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名称',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'operatingTime',
		                    title: '操作时间',
		                    align:'center',
		                }, {
		                    field: 'barCode',
		                    title: '条码',
		                    align:'center',
		                }, {
		                    field: 'batch',
		                    title: '批次',
		                    align:'center',
		                }, {
		                    field: 'type',
		                    title: '操作类型',
		                    align:'center',
		                }, {
		                    field: 'result',
		                    title: '操作结果',
		                    align:'center',
		                } , {
		                    field: 'locationId',
		                    title: '库区',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.warehouseRegion){
		                    		value = row.warehouseRegion.region_name;
	                            }
                                return value;
	                        },
		                }
		                ]
		               ]

				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
		});
		//获取当前日期
	   	function curentDate() {
	   		var now=new Date();
	   		var year = now.getFullYear();       //年
	   	    var month = now.getMonth() + 1;     //月
	   	    var day = now.getDate();            //日
	   	    var time=year+"-"+add0(month)+"-"+add0(day);
	   		return time;
	   	}
	   	function add0(m){return m<10?'0'+m:m }
		toastrStyle()
	</script>
</body>
</html>