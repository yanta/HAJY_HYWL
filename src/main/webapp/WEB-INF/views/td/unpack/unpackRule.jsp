<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>拆箱</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>拆箱任务</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword01" id="keyword01" placeholder="请输入产品码" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
					<td>
						<input class="layui-input" name="keyword02" id="keyword02" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px"> 
					</td>
					<%-- <td>
						<div class="layui-input-block" style="width:180px;margin-left: 10px;">
							<select class="layui-input" name="customer_id" id="customer_id" lay-verify="required">
								<option value="">请选择供应商</option>
								<c:forEach items="${customers}" var="customer">
									<option value="${customer.id}">${customer.customer_name}</option>
								</c:forEach>  
							</select>
						</div>
					</td> --%>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	  <table class="layui-hide" id="tableDetails1" lay-filter="tableDetails1"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		{{#  
			if(d.unum > 0){ 
	 	}}
			<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon"></i>新增任务</a>
			
		{{#
	  		} 
	 	}}
	</script>
	<script type="text/html" id="rowToolbard">
		{{#  
			if(d.state == 0){ 
	 	}}
			<a class="layui-btn layui-btn-xs" lay-event="edit1"><i class="layui-icon layui-icon-edit"></i>拆箱完成</a>
		{{#
	  		} 
	 	}}
	</script>
	

	<div id="addDivID" hidden="hidden">
		<form class="layui-form" id="addFormID">
			<br>
				<label class="layui-form-label">任务单号</label></td>
				<input class="layui-input" id="unumber" name="unumber" lay-verify="only" style="width: 300px;" placeholder="请输入任务单号">
				<!-- 拆箱id -->
				<input type="hidden" id="uid" name="uid">
				<!-- 需拆箱数量 -->
				<input type="hidden" id="unum" name="unum">
				<span class="xing"></span>
			<br>
				<label class="layui-form-label">拆箱日期</label></td>
				<input class="layui-input" id="time" name="time" lay-verify="required" style="width: 300px;" placeholder="请输入拆箱日期">
				<span class="xing"></span>
			<br>
				<label class="layui-form-label">拆箱数量</label></td>
				<input class="layui-input" id="utnumX" lay-verify="utnumX" style="width: 81px;display:inline;">
				* <input class="layui-input" id="packing_quantity" style="width: 80px;display: inline;" readonly> =
				<input class="layui-input" id="utnum" name="utnum" lay-verify="number" style="width: 110px;display: inline;" readonly>
				<span class="xing"></span>
			<br>
			<!-- 	<label class="layui-form-label">拆箱箱数</label></td>
				<input class="layui-input" id="utnumX" lay-verify="utnumX" style="width: 300px;">
				<input type="hidden" class="layui-input" id="packing_quantity" style="width: 80px;display: inline;" readonly>
				<input type="hidden" class="layui-input" id="utnum" name="utnum" lay-verify="number" style="width: 110px;display: inline;" readonly>
				<span class="xing"></span> -->
			<br>
				<label class="layui-form-label">发货数量</label></td>
				<input class="layui-input" id="sendNumX" name="sendNumX" lay-verify="sendNumX" style="width: 81px;display:inline;">
				* <input class="layui-input" id="devanning_num" style="width: 80px;display:inline;" readonly> =
				<input class="layui-input" id="sendNum" name="sendNum" lay-verify="number" style="width: 110px;display:inline;" readonly>
				<span class="xing"></span>
			<br> 
				<!-- <label class="layui-form-label">发货箱数</label></td>
				<input class="layui-input" id="sendNumX" name="sendNumX" lay-verify="sendNumX" style="width: 300px;">
				<input type="hidden" class="layui-input" id="devanning_num" style="width: 80px;display:inline;" readonly>
				<input type="hidden" class="layui-input" id="sendNum" name="sendNum" lay-verify="number" style="width: 110px;display:inline;" readonly>
				<span class="xing"></span> -->
			<br>
			<!--	 <label class="layui-form-label">入库数量</label></td>
				<input class="layui-input" id="upperNumX" name="upperNumX" lay-verify="upperNumX" style="width: 81px;display: inline;">
				* <input class="layui-input" id="devanning_num1" style="width: 80px;display: inline;" readonly> =
				<input class="layui-input" id="upperNum" name="upperNum" lay-verify="number" style="width: 110px;display: inline;" readonly>
				<span class="xing"></span>
			<br> --> 
				<input type="hidden" class="layui-input" id="upperNumX" name="upperNumX" lay-verify="upperNumX" style="width: 81px;display: inline;">
				<input type="hidden" class="layui-input" id="devanning_num1" style="width: 80px;display: inline;" readonly>
				<input type="hidden" class="layui-input" id="upperNum" name="upperNum" lay-verify="number" style="width: 110px;display: inline;" readonly>
			<br>
			<br>
			<script type="text/javascript">
				$(function(){
				   	//输入框的值改变时触发
				  	$("#utnumX").on("input",function(e){
					    //获取input输入的值
					    var value = e.delegateTarget.value;
				    	var packing_quantity = $("#packing_quantity").val()
				    	$("#utnum").val(packing_quantity*value);
				    	
				    	//修改后操作入库数量
				    	var devanning_num1 = $("#devanning_num1").val()
				    	var upperNum = packing_quantity*value;
				    	$("#upperNum").val(upperNum);
				    	$("#upperNumX").val(Math.ceil(upperNum/devanning_num1));
				  	});
				   	//输入框的值改变时触发
				  	$("#sendNumX").on("input",function(e){
					    //获取input输入的值
					    var value = e.delegateTarget.value;
				    	var devanning_num = $("#devanning_num").val()
				    	$("#sendNum").val(devanning_num*value);
				    	
				    	//修改后操作入库数量
				    	var utnum = $("#utnum").val()
				    	var devanning_num1 = $("#devanning_num1").val()
				    	var upperNum = utnum - devanning_num*value;
				    	$("#upperNum").val(upperNum);
				    	$("#upperNumX").val(Math.ceil(upperNum/devanning_num1));
				    	
				  	});
				   	//输入框的值改变时触发
				  	$("#upperNumX").on("input",function(e){
					    //获取input输入的值
					    var value = e.delegateTarget.value;
				    	var devanning_num = $("#devanning_num1").val()
				    	$("#upperNum").val(devanning_num*value);
				  	});
				});
			</script>
			<button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:140px;">立即提交</button>
			&emsp;&emsp;&emsp;&emsp;
			<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		</form>
    </div>

	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $.trim($("#keyword01").val());
	                var keyword02 = $.trim($("#keyword02").val());
	                var customer_id = $.trim($("#customer_id").val());
					table.reload('tableList',{
						method:'get'
						,where:{
							'materiel.materiel_num':keyword01
	                        ,'materiel.materiel_name':keyword02
	                        ,'customer_id':customer_id
						}
						,page: {
		                    curr: 1//重新从第 1 页开始
		                }
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
                ,url:'${pageContext.request.contextPath }/unpacking/list.do'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                    type: 'checkbox',
		                    fixed: 'left'
		                }, {
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                } , {
		                    field: 'customer_name',
		                    title: '客户名称',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'customer_num',
		                    title: '客户编号',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'materiel_num',
		                    title: '产品码',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '简码',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'materiel_name',
		                    title: '物料名称',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'materiel_size',
		                    title: '物料规格',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_size;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'materiel_properties',
		                    title: '物料型号',
		                    align:'center',
	                    	templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_properties;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'packing_quantity',
		                    title: '原包装数量',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.packing_quantity;
	                            }
                                return value;
	                        },
		                /* }, {
		                    field: 'removal_container',
		                    title: '拆箱后容器',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.removal_container;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'devanning_num',
		                    title: '现包装数量',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.devanning_num;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'unum',
		                    title: '可拆箱个数',
		                    align:'center'
		                }, {
		                    field: 'unum',
		                    title: '可拆箱箱数',
		                    align:'center',
		                    templet: function (row){
		                    	var value = 0;
		                    	if(null != row.materiel){
		                    		value = row.materiel.packing_quantity;
	                            }
                                return Math.ceil(row.unum/value);
	                        },
		                }, {
		                    fixed: 'right',
		                    title:'操作',
		                    toolbar: '#rowToolbar',
		                    align:'center',
		                    unresize: true,
		                    width: 90 
		                }
		                ]
		               ]
				,page: true
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
			
        	//表格
            table.render({
                elem: '#tableDetails1'
                ,url:'${pageContext.request.contextPath }/unpackingTask/list.do?uid='+"-1"+"&state=0"
                ,title: '拆箱任务单'
                ,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
                ,request: {   //如果无需自定义请求参数，可不加该参数
                    pageName: 'page' //页码的参数名称，默认：page
                    ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
                }
                ,response: {   //如果无需自定义数据响应名称，可不加该参数
                    countName: 'total' //规定数据总数的字段名称，默认：count
                    ,dataName: 'rows' //规定数据列表的字段名称，默认：data
                } 
                ,cols: [
                    [{
                        field: 'utid',
                        title: 'id',
                        hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
                    }, {
                    	field:'utid',
                    	title:'序号', 
                    	sort: true,
                    	width: 60,
                    	type:'numbers',
                    	align:'center'
                    } , {
                    	field: 'unumber',
                        title: '任务单号',
                        align:'center',
                    } , {
	                    field: 'time',
                        title: '拆箱日期',
                        align:'center',
                    }, {
	                    field: 'utnum',
                        title: '拆箱数量',
                        align:'center',
                    }, {
	                    field: 'sendNum',
                        title: '发货数量',
                        align:'center',
                    }, {
	                    field: 'sendNumX',
                        title: '发货箱数',
                        align:'center',
                    }, {
	                    field: 'upperNum',
                        title: '入库数量',
                        align:'center',
                    }, {
	                    field: 'upperNumX',
                        title: '入库箱数',
                        align:'center',
                    }, {
	                    field: 'state',
	                    title: '状态',
	                    align:'center',
	                    templet: function(d){
	                    	var value = d.state;
        	    			if(value=="0"){
        	    				value = "未拆箱"
        	    			}else if(value=="1"){
        	    				value = "拆箱完成"
        	    			}
        	    			return value;
        	    		},
                    }, {
	                    fixed: 'right',
	                    title:'操作',
	                    toolbar: '#rowToolbard',
	                    align:'center',
	                    unresize: true,
	                    width: 150
                    }
                    ]]
                ,page: true
                ,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            } 
                //,height: 500
            });
        	
	        table.on('row(tableList)', function(obj){
	        	var data = obj.data;
	        	table.reload('tableDetails1',{
						url:'${pageContext.request.contextPath }/unpackingTask/list.do?uid='+data.uid+"&state=0"
						,method:'get'
						,page: {
			                curr: 1//重新从第 1 页开始
			            }
        	  	});
	        	if(obj.checked == true && obj.type == 'all'){
	                //点击全选
	                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
	            }else if(obj.checked == false && obj.type == 'all'){
	                //点击全不选
	                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
	            }else if(obj.checked == true && obj.type == 'one'){
	                //点击单行
	                if(obj.checked == true){
	                    obj.tr.addClass('layui-table-click');
	                }else{
	                    obj.tr.removeClass('layui-table-click');
	                }
	            }else if(obj.checked == false && obj.type == 'one'){
	                //点击全选之后点击单行
	                if(obj.tr.hasClass('layui-table-click')){
	                    obj.tr.removeClass('layui-table-click');
	                }
	            }
	        });
	        
			//监听行工具事件
			table.on('tool(tableDetails1)', function(obj){
				var data = obj.data;
				var tr = obj.tr; //获得当前行 tr 的DOM对象
				var uid = data.uid;
				//单个删除
				if(obj.event === 'edit1'){
				  layer.confirm('确定完成吗？', function(index){
					  $.ajax({
	                        type:'post',
	                        url:'${pageContext.request.contextPath }/unpackingTask/updateState.do',
	                        data:{"utid":data.utid,"state":"1"},
	                        success:function(data){
	                            if(data>0){
	                                toastr.success("修改成功！");
	                                table.reload('tableDetails1',{
			  							url:'${pageContext.request.contextPath }/unpackingTask/list.do?uid='+uid+"&state=0"
		  								,method:'get'
		  								,page: {
		  					                curr: 1//重新从第 1 页开始
		  					            }
			                	  	});
	                            }else{
	                                toastr.warning("修改失败！");
	                            }
	                        }

	                    })
					  layer.close(index);
				  });
				}
			});
			
			//监听行工具事件
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				if(obj.event === 'edit'){
					layer.open({
					  type: 1 							//Page层类型
					  ,area: ['500px', '420px'] 	//宽  高
					  ,title: '新增任务'
					  ,shade: 0.6 						//遮罩透明度
					  ,maxmin: true 					//允许全屏最小化
					  ,anim: 1 							//0-6的动画形式，-1不开启
					  ,content: $('#addDivID')
	                  ,end: function () {
	                      var formDiv = document.getElementById('addDivID');
	                      formDiv.style.display = '';
	                  }
	                  ,success: function(){
	                	  //需拆箱数量
	                      $("#unum").val(data.unum)
	                      //包装数量
	                      $("#packing_quantity").val(data.materiel.packing_quantity)
	                      //拆后包装数量
	                      $("#devanning_num").val(data.materiel.devanning_num)
	                      $("#devanning_num1").val(data.materiel.devanning_num)
	                      $("#utnumX").val(0);
	                      $("#utnum").val(0);
	                      $("#sendNumX").val(0);
	                      $("#sendNum").val(0);
	                      $("#upperNumX").val(0);
	                      $("#upperNum").val(0);
	                      
	                      $("#uid").val(data.uid)
	                      $("#unumber").val(curentTimeNumber())
	                      $("#time").val(curentTime())
	                      $("#utnum").val(0)
	                      $("#sendNum").val(0)
	                      $("#upperNum").val(0)
	                      form.render();
	                    }
					});
				}
			});

			/**
			 * 通用表单提交(AJAX方式)(新增)
			 */
			form.on('submit(addForm)', function (data) {
				var index = layer.load(1, {
	        	    shade: [0.1,'#000'] //0.1透明度的背景
	        	});
				
				var formData = $("#addFormID").serialize();
				var url = '${pageContext.request.contextPath}/unpackingTask/insert.do';
				var title = "任务新增";
				$.ajax({
					url : url,
					data: formData,
					cache : false,
					type : "post",
					success: function (data) {
	                    if (data >= 1) {
	                        toastr.success(title+'成功');
	                        setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	                            window.location.reload();
	                            $("#reset").click();
	                        },1000);
	                    } else {
	                        toastr.error(title+'失败，任务单号不能同时重复');
	                    }
	                },
				});
				layer.close(index);
				return false;
			});

			

			/**
			 * 新增表单校验
			 */
			form.verify({
				//value：表单的值、item：表单的DOM对象
	            required: function(value, item){
	            	value = $.trim(value)
	            	if(value == '' || value == null){
	                    return '不能为空';
	                }
	            },
	          	//value：表单的值、item：表单的DOM对象
	            number: function(value, item){
	            	value = $.trim(value)
	            	var re = /^[0-9]\d*$/;
	            	if(value != '' && value != null && !re.test(value)){
	                    return '请输入正整数';
	                }
	            },
	          	//value：表单的值、item：表单的DOM对象
	            utnumX: function(value, item){
	            	var value = $.trim(value)
	            	var utnum = $.trim($("#utnum").val())
	            	var unum = $.trim($("#unum").val())
	            	var re = /^[1-9]\d*$/;
	            	if(value != '' && value != null && !re.test(value)){
	                    return '请输入正整数';
	                }else if(parseInt(utnum) > parseInt(unum)){
		                return '拆箱数量不能大于需拆箱数量';
	                } 
	            },
	          	//value：表单的值、item：表单的DOM对象
	            sendNumX: function(value, item){
	            	var value = $.trim(value)
	            	var utnum = $.trim($("#utnum").val())
	            	var sendNum = $.trim($("#sendNum").val())
	            	var upperNum = $.trim($("#upperNum").val())
	            	var he = parseInt(sendNum)+parseInt(upperNum);
	            	var re = /^[0-9]\d*$/;
	            	if(value != '' && value != null && !re.test(value)){
	                    return '请输入正整数';
	                }else if(parseInt(utnum) < parseInt(sendNum)){
		                return '发货数量不能大于拆箱数量';
	                }else if(parseInt(utnum) != he){
		                return '拆箱数量等于发货数量和入库数量之和';
	                } 
	            },
	          	//value：表单的值、item：表单的DOM对象
	            upperNumX: function(value, item){
	            	var value = $.trim(value)
	            	var utnum = $.trim($("#utnum").val())
	            	var sendNum = $.trim($("#sendNum").val())
	            	var upperNum = $.trim($("#upperNum").val())
	            	var he = parseInt(sendNum)+parseInt(upperNum);
	            	var re = /^[0-9]\d*$/;
	            	if(value != '' && value != null && !re.test(value)){
	                    return '请输入正整数';
	                }else if(parseInt(utnum) < parseInt(upperNum)){
		                return '入库数量不能大于拆箱数量';
	                }else if(parseInt(utnum) != he){
		                return '拆箱数量等于发货数量和入库数量之和';
	                }
	            },
				//value：表单的值、item：表单的DOM对象
	            only: function(value, item){
	            	value = $.trim(value)
	            	if(value == '' || value == null){
	                    return '不能为空';
	                }else{
	                    var flg = false;
	                    $.ajax({
	                        type:'post',
	                        data:'unumber='+value,
	                        url:'${pageContext.request.contextPath}/unpackingTask/queryAllByMution.do',
	                        dataType: 'JSON',
	                        async: false,
	                        success:function (data) {
	                            if(data.length == 0){
	                                flg  = true;
	                            }else if(data.length == 1){
	                                var ma = $.trim($("#utid").val());
	                                if (ma == data[0].utid) {
	                                    flg  = true;
	                                }else{
	                                    flg = false;
	                                }
	                            }else{
	                                flg = false;
	                            }
	
	                        }
	                    });
	                    if(!flg){
	                        return '任务单号不能重复';
	                    }
	                }
	            }
			});

			laydate.render({
                elem: '#time'
            });

			
			
		});
		
		//获取当前时间生成的编码
	   	function curentTimeNumber() {
	   		var now=new Date();
	   		var year = now.getFullYear();       //年
	   	    var month = now.getMonth() + 1;     //月
	   	    var day = now.getDate();            //日
	   	 	var hours = now.getHours(); //获取当前小时数(0-23)
		   	var minutes = now.getMinutes(); //获取当前分钟数(0-59)
		   	var seconds = now.getSeconds(); //获取当前秒数(0-59)
	   	    //var mill=now.getMilliseconds();
	   	    var time=year+""+add0(month)+""+add0(day)+""+add0(hours)+""+add0(minutes)+""+add0(seconds);
	   		return time;
	   	}
	    //获取当前日期
	   	function curentTime() {
	   		var now=new Date();
	   		var year = now.getFullYear();       //年
	   	    var month = now.getMonth() + 1;     //月
	   	    var day = now.getDate();            //日
	   	    var time=year+"-"+add0(month)+"-"+add0(day);
	   		return time;
	   	}
	   	function add0(m){return m<10?'0'+m:m }
		toastrStyle()
	</script>
</body>
</html>