<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>入库</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>入库记录查询</cite>
			</a>
		</span>
	</div>
	<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
							<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入入库单号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
	<!-- 退库单行编辑 -->
 	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="detail"><i class="layui-icon layui-icon-search"></i>入库详情</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="take"><i class="layui-icon layui-icon-search"></i>收货记录</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="accept"><i class="layui-icon layui-icon-search"></i>验收记录</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="putaway"><i class="layui-icon layui-icon-search"></i>上架记录</a>
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="rejection"><i class="layui-icon layui-icon-search"></i>拒收记录</a>
	</script>
    <!-- 入库单列表 -->
 	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
    	
    <!-- 入库收货记录列表 -->
    <div id="div1" hidden>
 	<table class="layui-hide" id="receivingRecord" lay-filter="receivingRecord"></table>
    </div>
 	
    <!-- 入库验收记录列表 -->
 	<div id="div2" hidden>
 	<table class="layui-hide" id="inspectionRecord" lay-filter="inspectionRecord"></table>
    </div>
    	
    <!-- 入库上架记录列表 -->
 	<div id="div3" hidden>
 	<table class="layui-hide" id="onRecord" lay-filter="onRecord"></table>
    </div>
    
     <!-- 入库拒收记录列表 -->
 	<div id="div4" hidden>
 	<table class="layui-hide" id="rejectionRecord" lay-filter="rejectionRecord"></table>
    </div>
	 	
	 	 <!-- 入库拒收记录列表 -->
 	<div id="div5" hidden>
 	<table class="layui-hide" id="comeKuDetail" lay-filter="comeKuDetail"></table>
    </div>
	 	
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword1 = $("#keyword1").val();
				
					table.reload('contenttable',{
						method:'get',
						where:{"keyword1":keyword1}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			/* 退库单列表 */
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/InHouse/getInHousePage.do'
				,title: '入库记录'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [
					[
						{field:'', title:'入库单',colspan: 15, align:'center'},
					],
					[
					  	{field:'', title:'序号', type:'numbers', align:'center'},
					  	{field:'come_number', title:'入库单号', align:'center'},
					  	{field:'come_date', title:'入库时间', align:'center'},
					  	{field:'come_name', title:'入库人', align:'center'},
					  	{field:'remark', title:'备注', align:'center'},
					  	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:500, align: 'center'}
					]
				]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				var id = data.csid;
				var idArr = new Array();
				if(obj.event === "take"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '收货记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div1')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#receivingRecord'
			        				,url:'${pageContext.request.contextPath }/InHouse/getReceivingRecordPage.do'
			        				,where:{id: obj.data.id}
			        				,title: '收货记录'
			        				,id :'receivingRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'materiel_name', title:'物料名称', align:'center'},
			        					  	{field:'pici', title:'批次', align:'center'},
			        					  	{field:'single_num', title:'单品数量', align:'center'},
			        					  	{field:'unit_num', title:'单位数量', align:'center'},
			        					  	{field:'container', title:'容器名称', align:'center'},
			        					  	{field:'container_num', title:'容器数量', align:'center'},
			        					  	{field:'nogood_num', title:'不良数量', align:'center'},
			        					  	{field:'come_num', title:'入库数量', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "accept"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '验收记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div2')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#inspectionRecord'
			        				,url:'${pageContext.request.contextPath }/InHouse/getCheckRecordPage.do'
			        				,where:{id: obj.data.id}
			        				,title: '验收记录'
			        				,id :'inspectionRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'materiel_name', title:'物料名称', align:'center'},
			        					  	{field:'pici', title:'批次号', align:'center'},
			        					  	{field:'cnum', title:'检查数量', align:'center'},
			        					  	{field:'snum', title:'破损数量', align:'center'},
			        					  	{field:'hnum', title:'合格数量', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "putaway"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '上架记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div3')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#onRecord'
			        				,url:'${pageContext.request.contextPath }/InHouse/getUppperPage.do'
			        				,where:{id: obj.data.id}
			        				,title: '上架记录'
			        				,id :'onRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'materiel_name', title:'物料名称', align:'center'},
			        						{field:'update', title:'上架时间', align:'center'},
			        					  	{field:'upname', title:'上架人', align:'center'},
			        					  	{field:'warehouse_name', title:'仓库名称', align:'center'},
			        					  	{field:'region_name', title:'库区名称', align:'center'},
			        					  	{field:'location_name', title:'库位名称', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "rejection"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '拒收记录'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div4')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#rejectionRecord'
			        				,url:'${pageContext.request.contextPath }/InHouse/getRejectionRecordPage.do'
			        				,where:{id: obj.data.id}
			        				,title: '拒收记录'
			        				,id :'rejectionRecord'
			        				,limits:[10,20,30]
			        				,cols: [[
			        						{field:'', title:'序号', width: 50, type:'numbers', align:'center'},
			        						{field:'rnumber', title:'退货单号', align:'center'},
			        					  	{field:'custom', title:'供货商', align:'center'},
			        					  	{field:'materiel_name', title:'物料名称', align:'center'},
			        					  	{field:'content', title:'容器', align:'center'},
			        					  	{field:'num', title:'容器数量', align:'center'},
			        					  	{field:'tnum', title:'退货数量', align:'center'},
			        					  	{field:'reson', title:'退货原因', align:'center'},
			        					  	{field:'remark', title:'备注', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}else if(obj.event === "detail"){
					layer.open({
			            type: 1 					//Page层类型
			            ,area: ['95%', '70%']   //宽  高
			            ,title: '入库详情'
			            ,shade: 0.6 				//遮罩透明度
			            ,maxmin: true 			    //允许全屏最小化
			            ,anim: 1 					//0-6的动画形式，-1不开启
			            ,content: $('#div5')
			            ,success: function(){
			                layui.use(['table'], function(){
			                    var table = layui.table;
			                    table.render({
			        				elem: '#comeKuDetail'
			        				,url:'${pageContext.request.contextPath }/comeKuDetail/list.do'
			        				,where:{comeNumber: obj.data.come_number}
			        				,title: '入库详情'
			        				,id :'detail'
			        				,limits:[10,20,30]
			        				,cols: [[
			        					{field:'id', title:'序号', width: 50,type:'numbers', align:'center'},
					                     {field:'mid', title:'物料id', align:'center',hide:true},
					                     {field:'materiel_num', title:'产品码', align:'center',width:100},
					                     {field:'materiel_name', title:'物料名称', align:'center',width:100},
					                     {field:'brevity_num', title:'物料简码', align:'center',width:100},
					                     {field:'unit', title:'单位', align:'center',width:60},
					                     {field:'container_name', title:'容器名称', align:'center'},
					                     {field:'is_devanning', title:'是否拆箱', align:'center',width:80},
					                     {field:'devanning_num', title:'拆箱数', align:'center',width:70},
					                     {field:'nogoodNum', title:'不良数量', align:'center'},
					                     {field:'comeNum', title:'入库数量', align:'center'}, 
					                     {field:'remark', title:'待上架数量', align:'center'},
			        					]]
			        				,page: true
			        				,done : function(){
			                            $('th').css({
			                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
			                                'font-size': 10,
			                            })
			                        }
			        			});
			                });
			            }
					});
				}
				
				
			});
		});
	</script>
</body>
</html>