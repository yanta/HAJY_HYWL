<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>补货查询</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
 	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>补货查询</cite>
        </a>
      </span>
    </div>
    
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入出库单号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
	   <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
    
</body>
<script type="text/javascript">
        var table;
		layui.use(['table'], function(){
			table = layui.table;
			/* var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate; */
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword1 = $("#keyword1").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword1":keyword1}
					});
				}
			}
			
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/ReplenishGoods/getReplenishGoodsPage.do'
				,title: 'machineList'
				,id :'contenttable'
				,limits:[10,20,30]
				,toolbar:true
                ,defaultToolbar:['filter', 'print']
				,cols: [
					[
						{field:'',title:'补货记录',colspan:7,align:'center'},
					],
					[
				  {type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'out_stock_order_num', title:'出库编号', align:'center'},
				  {field:'replenishTime', title:'补发日期', align:'center'},
				  {field:'replenish_man', title:'补发人', align:'center'},
				  {field:'replenish_reason', title:'补发原因', align:'center'},
				  {field:'remark', title:'备注', align:'center'}
				]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
		});
</script>
</html>