<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>我提交的</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<script charset="utf-8" src="${pageContext.request.contextPath}/lib/layui-xtree/layui-xtree.js"></script>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>我提交的</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" id="keyword01" style="width: 170px; margin-left: 10px" placeholder="请选择年月">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	   <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
	</script>

	<!-- 导入框 -->
	<div id="importDiv" hidden="hidden">
		<form class="layui-form" id="uploadform">
			<div style="margin: 20px 0 20px 140px;">
				<input id="fileInput" name="file" type="file" accept=".xls,.xlsx"/>
			</div>
		</form>
		<button class="layui-btn layui-btn-blue" lay-filter="uplodeform" style="margin-left: 80px">导入数据</button>&emsp;&emsp;&emsp;&emsp;
		<button type="button" class="layui-btn" id="downloadFile">下载模板</button>
	</div>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword01":keyword01}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/approvalManager/ICommitedPage.do'
				,toolbar: '#toolbar'
				,title: '损溢管理'
				,id :'contenttable'
				,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
				,cols: [[
				  //{type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'inventory_order', title:'盘点单', align:'center'},
				  {field:'loss_spillover_order', title:'损益单单号', align:'center'},
				  //{field:'loss_spillover_type', title:'损溢类型', align:'center', edit: true},
				  //field:'materialName', title:'物料名称', align:'center', edit: true},
				  //{field:'materialNumber', title:'物料编号', align:'center', edit: true},
				  //{field:'inventory_before', title:'盘点前', align:'center', edit: true},
				  //{field:'inventory_after', title:'盘点后', align:'center', edit: true},
				  //{field:'inventory_different', title:'差异数量', align:'center', edit: true},
				  //{field:'different_remark', title:'差异备注', align:'center', edit: true},
				  {field:'approval_name', title:'审批人名称', align:'center'},
				  {field:'approval_static', title:'状态', align:'center', templet: function(d){
				        if(d.approval_status == "0"){
				        	return "未审批"
				        }else if(d.approval_status == "1"){
				        	return "审批通过"
				        }else{
				       		return "审批驳回"
				        }
				      }  
				  },
				  {field:'approval_idea', title:'审批意见', align:'center'},
				  //{field:'copy_name', title:'抄送人名称', align:'center'},
				  {field:'createrName', title:'创建人', align:'center'},
				  {field:'creater_date', title:'创建时间', align:'center'},
				  //{fixed:'right', unresize: true, title:'操作', toolbar: '#rowToolbar', align:'center',width: 100}
				]]
				,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        
                    })
                }
			});
			 laydate.render({
					elem: '#keyword01',
				});
			//监听行工具事件
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				//alert(data.id)
				var idArr = new Array();
				//单个删除
				if(obj.event === 'del'){
				  layer.confirm('确定删除吗？', function(index){
					var id = obj.data.id;
					idArr[0] = id;
					$.ajax({
						type:'post',
						url:'${pageContext.request.contextPath }/lossSpillover/deleteLossSpilloverById.do',
						data:{ "idArr" : idArr },
						success:function(data){
							if(data > 0){
								toastrStyle();
								toastr.success("删除成功！");
								setTimeout(function(){
									location.reload();
								},1000);
								/*setTimeout(function(){
									//关闭模态框
									//父页面刷新
									window.location.reload();
								},2000);*/
							}else{
								toastrStyle();
								toastr.warning("删除失败！");
								setTimeout(function(){
									location.reload();
								},1000);
							}
						}
					});
					layer.close(index);
				  });
				}
			});
			
			
			

		
			laydate.render({
                elem: '#creater_date'
            });
			 //行单击事件
            table.on('row(tableList)', function(obj) {
                var data = obj.data;
                console.log(data.id);
                table.render({
                    elem: '#tableListSub'
                    ,url:'${pageContext.request.contextPath }/lossSpillover/queryLossDetail.do?lossid='+data.id
                    ,toolbar: '#toolbar'
                    ,title: 'machineList'
                    //,id :'contenttable'
                    ,limits:[10,20,30]
                    ,defaultToolbar:['filter', 'print']
                    ,cols: [[
                        //{type: 'checkbox', fixed: 'left'},
                        {field:'id', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
                        {field:'customer_name', title:'供应商名称', align:'center'}, //隐藏不显示
                        {field:'region_name', title:'库区名称', align:'center'}, //隐藏不显
                        //{field:'customer_name', title:'供应商名称', align:'center'},
                        {field:'materiel_id', title:'物料ID', align:'center', hide:true}, //隐藏不显示
                        {field:'materiel_name', title:'物料名称', align:'center'},
                        {field:'materiel_num', title:'物料编码', align:'center'},
                        {field:'materiel_size', title:'物料规格', align:'center'},
                        {field:'materiel_properties', title:'物料型号', align:'center'},
						{field:'difference_quantity', title:'差异数量', align:'center'},
						{field:'type', title:'损益类型', align:'center'},
						{field:'reason', title:'差异原因', align:'center'},
                    ]]
                    ,page: true
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });
			

			//批量删除
			$("#dels").click(function(){
				var rowData = table.checkStatus('contenttable');
				var data = rowData.data;
				var idArr = new Array();
				if(data.length == 0){
					toastrStyle();
					toastr.warning("请至少选择一条记录！");
				}
				for(var i=0;i < data.length;i++){
					idArr[i] = data[i].id;
				}
				$.ajax({
					type:'post',
					url:'${pageContext.request.contextPath }/lossSpillover/deleteLossSpilloverById.do',
					data:{"idArr" : idArr},
					success:function(data){
						layer.confirm('确定删除吗？', function(index){
							if(data > 0){
								toastrStyle();
								toastr.success("删除成功！");
								setTimeout(function(){
									location.reload();
								},1000);
								setTimeout(function(){
									//使用setTimeout（）方法设定定时2000毫秒
									//关闭模态框
									//父页面刷新
									window.location.reload();
								},2000);
							}else{
								toastrStyle();
								toastr.warning("删除失败！");
								location.reload();
							}
						});
					}
				});
			});

		
		});
	</script>
</body>
</html>