<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>我审批的</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<script charset="utf-8" src="${pageContext.request.contextPath}/lib/layui-xtree/layui-xtree.js"></script>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>待我审批</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
						<input class="layui-input" id="keyword01" style="width: 170px; margin-left: 10px" placeholder="请选择年月">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	   <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		{{#if(d.approval_status == "0"){}}
		<a  class="layui-btn layui-btn-danger layui-btn-xs " lay-event="edit">审批</a>
		{{#}else{}}
		<a  class="layui-btn layui-btn-primary layui-btn-xs layui-disabled" lay-event="edit">审批</a>
		{{#}}}	
	</script>

	<!-- 导入框 -->
	<div id="importDiv" hidden="hidden">
		<form class="layui-form" id="uploadform">
			<div style="margin: 20px 0 20px 140px;">
				<input id="fileInput" name="file" type="file" accept=".xls,.xlsx"/>
			</div>
		</form>
		<button class="layui-btn layui-btn-blue" lay-filter="uplodeform" style="margin-left: 80px">导入数据</button>&emsp;&emsp;&emsp;&emsp;
		<button type="button" class="layui-btn" value="123" id="downloadFile">下载模板</button>
	</div>
	
	
	<!-- 新增表单 -->
	
	 <div id="addDivID" hidden>
			<form class="layui-form" id="addFormID">
			<xblock>
				<table>
				<tr><label class="layui-form-label">审批意见:</label></tr>
				<tr><textarea id="approval_idea" rows="5" cols="50"  ></textarea></tr>
				</table>
			</xblock>
			<xblock>
				<button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="subForm" style="margin-left:200px; margin-top:10px; margin-bottom: 10px">通过</button>
				&emsp;&emsp;&emsp;&emsp;<button id="rejectBtn" lay-submit lay-filter="rejectForm" class="layui-btn layui-btn-danger" style="margin-top:10px; margin-bottom: 10px">驳回</button>
			</xblock> 	
			</form>
      </div>
	

	

	<script type="text/javascript">
	
		var id ;
		
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword01":keyword01}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/approvalManager/IApprovalPage.do'
				,toolbar: '#toolbar'
				,title: '我审批的'
				,id :'contenttable'
				,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
				,cols: [[
				  //{type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'inventory_order', title:'盘点单', align:'center'},
				  {field:'loss_spillover_order', title:'损益单单号', align:'center'},
				  
				  //{field:'different_remark', title:'差异备注', align:'center', edit: true},
				  {field:'approval_name', title:'审批人姓名', align:'center'},
				  {field:'approval_static', title:'状态', align:'center', 
					  templet: function(d){
					        if(d.approval_status == "0"){
					        	//$('#sh').attr('class','layui-btn layui-btn-danger layui-btn-xs');
					        	return "未审批";
					        }else if(d.approval_status == "1"){
					        	//$('#bsh').attr('class','layui-btn layui-btn-primary layui-btn-xs layui-disabled');
					        	return "审批通过";
					        }else{
					        	//$('#bsh').attr('class','layui-btn layui-btn-primary layui-btn-xs layui-disabled');
					       		return "审批驳回";
					        }
					      }  
				  },
				  //{field:'approval_idea', title:'审批意见', align:'center', edit: true},
				  //{field:'copy_name', title:'抄送人名称', align:'center'},
				  {field:'createrName', title:'创建人', align:'center'},
				  {field:'creater_date', title:'创建时间', align:'center'},
				  {fixed:'right', unresize: true, title:'操作', toolbar: '#rowToolbar', align:'center',width: 100  }
				]]
				,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        
                    })
                }
			});
			 laydate.render({
					elem: '#keyword01',
				});
	            
			//监听行工具事件
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				//alert(data.id)
				var idArr = new Array();
				//审批操作
				if(obj.event === 'edit'){
					id = data.id;
					
					if(data.approval_status == "0"){
					
						layer.open({
	                   	 	type: 1 					//Page层类型
	                    	,area: ['550px', ''] 	//宽  高
	                    	,title: '审批'
	                   	 	,shade: 0.6 				//遮罩透明度
	                   		,maxmin: true 				//允许全屏最小化
	                   		,anim: 1 					//0-6的动画形式，-1不开启
	                    	,content: $('#addDivID')
	                    	,success: function(){
	                        	laydate.render({
	                            	elem: '#creater_date'
	                        	});
	                        	form.render();
	                    	}
	                	});
					}
				}
			});
			
			 //行单击事件
            table.on('row(tableList)', function(obj) {
                var data = obj.data;
                console.log(data.id);
                table.render({
                    elem: '#tableListSub'
                    ,url:'${pageContext.request.contextPath }/lossSpillover/queryLossDetail.do?lossid='+data.id
                    ,toolbar: '#toolbar'
                    ,title: 'machineList'
                    //,id :'contenttable'
                    ,limits:[10,20,30]
                    ,defaultToolbar:['filter', 'print']
                    ,cols: [[
                        //{type: 'checkbox', fixed: 'left'},
                        {field:'id', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
                        {field:'customer_name', title:'供应商名称', align:'center'}, //隐藏不显示
                        {field:'region_name', title:'库区名称', align:'center'}, //隐藏不显
                        //{field:'customer_name', title:'供应商名称', align:'center'},
                        {field:'materiel_id', title:'物料ID', align:'center', hide:true}, //隐藏不显示
                        {field:'materiel_name', title:'物料名称', align:'center'},
                        {field:'materiel_num', title:'物料编码', align:'center'},
                        {field:'materiel_size', title:'物料规格', align:'center'},
                        {field:'materiel_properties', title:'物料型号', align:'center'},
						{field:'difference_quantity', title:'差异数量', align:'center'},
						{field:'type', title:'损益类型', align:'center'},
						{field:'reason', title:'差异原因', align:'center'},
                    ]]
                    ,page: true
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });

			/**
			 * 通用表单提交(AJAX方式)(通过审批)
			 */
			form.on('submit(subForm)', function (data) {
				console.log("***************************"+id)
			  	$.ajax({
					url : '${pageContext.request.contextPath}/approvalManager/CompleteApproval.do',
					data: {id:id, status:"1", idea:$("#approval_idea").val()},
					cache : false,
					type : "post",
					}).done(
						function(res) {
							if (res > 0) {
								toastrStyle();
								toastr.success('审批成功！');
								setTimeout(function(){
									location.reload();
								},1000);
							}
						}
					).fail(
						function(res) {
							toastrStyle();
							toastr.error('审批失败！');
							setTimeout(function(){
								location.reload();
							},1000);
						}
					)  
					return false;
			});
			
			
			/**
			 * 通用表单提交(AJAX方式)(驳回审批)
			 */
			form.on('submit(rejectForm)', function (data) {
				
				 $.ajax({
					url : '${pageContext.request.contextPath}/approvalManager/CompleteApproval1.do',
					data: {id:id,status:"2",idea:$("#approval_idea").val()},
					cache : false,
					type : "post",
					}).done(
						function(res) {
							if (res > 0) {
								toastrStyle();
								toastr.success('驳回成功！');
								setTimeout(function(){
									location.reload();
								},1000);
							}
						}
					).fail(
						function(res) {
							toastrStyle();
							toastr.error('驳回失败！');
							setTimeout(function(){
								location.reload();
							},1000);
						}
					) 
					return false;
			});

			/**
			 * 新增表单校验
			 */
			form.verify({
				//value：表单的值item：表单的DOM对象
				reactionPlan: function(value, item){
					if(value == ''){
						return '反应计划字段不能为空';
					}
				}
			});

			laydate.render({
                elem: '#creater_date'
            });
			
			
			
		
		});
	</script>
</body>
</html>