<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>库内查询</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
 	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>移库查询</cite>
        </a>
      </span>
    </div>
    
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
					<td>
						<input class="layui-input" name="keyword" id="keyword2" placeholder="请输入产品码" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
	   <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	 
	 
	   
	</div>
    <script type="text/html" id="rowToolbar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="lookupDetail"><i class="layui-icon layui-icon-search"></i>详细</a>
</script>
</body>

<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var laydate = layui.laydate;
        var form = layui.form;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword1 = $("#keyword1").val();
                var keyword2 = $("#keyword2").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword1":keyword1,"keyword2":keyword2}
                });
            }
        }
        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#tableList'
            ,url:'${pageContext.request.contextPath }/MoveHouse/getMoveHousePage.do'
            ,toolbar: '#toolbar'
            ,title: 'machineList'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,defaultToolbar:['filter', 'print']
            ,cols: [
            	[
            		{field:'',title:'移库记录',colspan:14,align:'center'},
            	],
            	[
                {type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'materiel_name', title:'物料名称', align:'center'},
				  /* {field:'business_order_num', title:'业务单号', align:'center'}, */
				  {field:'materiel_num', title:'产品码', align:'center'},
				  {field:'srcWarehouse_name', title:'原始仓库', align:'center'},
				  {field:'srcRegion_name', title:'原始库区', align:'center'},
				  {field:'srcLocation_name', title:'原始库位' ,align:'center'},
				  {field:'mNum', title:'库存' ,align:'center'},
				  {field:'totality', title:'移动总数', align:'center'},
				  {field:'warehouse_name', title:'目标仓库', align:'center'},
				  {field:'region_name', title:'目标库区', align:'center'},
				  {field:'location_name', title:'目标库位', align:'center'},
				  {field:'move_date', title:'移动日期', align:'center'},
				  {field:'movePerson', title:'移动人', align:'center'},
				 /*  {fixed:'right', unresize: true, title:'操作', toolbar: '#rowToolbar', align:'center',width: 200}  */
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    'font-size': 10,
                })
            }
        });


        //监听行工具事件
        table.on('tool(tableList)', function(obj){
            if(obj.event === 'lookupDetail'){
            	layer.open({
    				  type: 1 		//Page层类型
    				  ,area: ['95%','70%'] //宽  高
    				  ,title: '详情'
    				  ,shade: 0.1 	//遮罩透明度
    				  ,maxmin: true //允许全屏最小化
    				  ,anim: 1 		//0-6的动画形式，-1不开启
    				  ,content: $('#div1')
    				  ,success: function () {
    					//回显表单数据
    						
    					
    					
    					
    				  }	
    				  ,end: function () {
    				  	
    			  	  }
    				});
            }
        });


        //监听行单击事件（单击事件为：rowDouble）
        table.on('row(tableList)', function(obj){
        	
  
        	
        });
        
        laydate.render({
            elem: '#start_date'
        });
        laydate.render({
            elem: '#end_date'
        });
    });
</script>
</html>