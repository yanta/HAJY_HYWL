<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>物料信息</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
 	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>物料信息</cite>
        </a>
      </span>
    </div>
    
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
					<td>
						<input class="layui-input" name="keyword" id="keyword2" placeholder="请输入产品码" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
	   <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
    
</body>

<script type="text/javascript">
        var table;
		layui.use(['table'], function(){
			table = layui.table;
			/* var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate; */
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword1").val();
					var keyword02 = $("#keyword2").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword1":keyword01, "keyword2":keyword02}
					});
				}
			}
			
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/MaterielInfo/getMaterielInfoPage.do'
				,title: 'machineList'
				,id :'contenttable'
				,toolbar:true
				,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
				,cols: [
					[
						{field:'',title:'物料详情',colspan:18,align:'center'},
					],
					[
				  {type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'client', title:'所属客户', align:'center'},
				  {field:'materiel_num', title:'产品码', align:'center'},
				  {field:'materiel_name', title:'物料名称', align:'center'},
				  {field:'brevity_num', title:'简码', align:'center'},
				  {field:'container_name', title:'容器名称', align:'center'},
				  {field:'quality_guarantee_period', title:'保质期', align:'center'},
				  {field:'allowable_storage_days', title:'允许存放天数', align:'center'},
				  {field:'unit', title:'单位', align:'center'},
				  {field:'outDate', title:'出厂日期', align:'center'},
				  {field:'delivery_address', title:'收货地址', align:'center'},
				  {field:'outgoing_frequency', title:'出库频率', align:'center'},
				  {field:'storage_age', title:'入库类型', align:'center'
					,templet:function(row){
						if(row.storage_type == '0'){
	                        return "普通入库";
	                    } else {
	                        return "精准入库";
	                    }
					}  
				  },
				  {field:'placement_type', title:'放置类型', align:'center'
					  , templet: function (row){
		                    if(row.placement_type == '0'){
		                        return "货架";
		                    } else {
		                        return "平地";
		                    }
		                }  
				  },
				  {field:'warehouse_name', title:'所属仓库', align:'center'},
				  {field:'region_name', title:'所属库区', align:'center'},
				  {field:'location_name', title:'所属库位', align:'center'}
				 

				]]
				,page: true
				,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        'font-size': 10,
                    })
                }
			});
			
		});
</script>
</html>