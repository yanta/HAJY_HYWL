<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>验收查询</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
 	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>验收查询</cite>
        </a>
      </span>
    </div>
    
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入收货单号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
	   <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	  
	   <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	   
	   <table class="layui-hide" id="tableListSubDetail" lay-filter="tableListDetail"></table>
	 
	   
	</div>
    <script type="text/html" id="rowToolbar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="lookupDetail"><i class="layui-icon layui-icon-search"></i>详细</a>
</script>
</body>

<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var laydate = layui.laydate;
        var form = layui.form;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword1 = $("#keyword1").val();
                //var keyword2 = $("#keyword2").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"comeNumber":keyword1}
                });
            }
        }
        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#tableList'
            ,url:'${pageContext.request.contextPath }/receive/list.do'
            ,toolbar: '#toolbar'
            ,title: 'machineList'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,defaultToolbar:['filter', 'print']
            ,cols: [
            	[
            		{field:'',title:'验收记录',colspan:15,align:'center'},
            	],
            	[
                {type: 'checkbox', fixed: 'left', },
				  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
				  {field:'sendNumber', title:'发货单号', align:'center'},
				  /* {field:'business_order_num', title:'业务单号', align:'center'}, */
				  {field:'receiveNumber', title:'收货单号', align:'center'},
				  {field:'sendCompany', title:'发货单位', align:'center'},
				  {field:'sendName', title:'发货人', align:'center'},
				  {field:'sendDate', title:'发货日期', align:'center'},
				  {field:'receiveName', title:'收货人', align:'center'},
				  {field:'receiveDate', title:'收货日期', align:'center'},
				  {field:'state', title:'状态', align:'center'},
				  {field:'sureName', title:'确认人', align:'center'},
				  {field:'sureDate', title:'确认时间', align:'center'},
				  {field:'createName', title:'创建人', align:'center'},
				  {field:'createDate', title:'创建时间', align:'center'},
				  {field:'remark', title:'备注', align:'center'},
				  
				 /*  {fixed:'right', unresize: true, title:'操作', toolbar: '#rowToolbar', align:'center',width: 200}  */
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    'font-size': 10,
                })
            }
        });


        //监听行工具事件
        table.on('tool(tableList)', function(obj){
            if(obj.event === 'lookupDetail'){
            	layer.open({
    				  type: 1 		//Page层类型
    				  ,area: ['95%','70%'] //宽  高
    				  ,title: '详情'
    				  ,shade: 0.1 	//遮罩透明度
    				  ,maxmin: true //允许全屏最小化
    				  ,anim: 1 		//0-6的动画形式，-1不开启
    				  ,content: $('#div1')
    				  ,success: function () {
    					//回显表单数据
    						
    					
    					
    					
    				  }	
    				  ,end: function () {
    				  	
    			  	  }
    				});
            }
        });


        //监听行单击事件（单击事件为：rowDouble）
        table.on('row(tableList)', function(obj){
        	console.log(obj.data.id);
        	  table.render({
	                elem: '#tableListSub'
	                ,url:'${pageContext.request.contextPath }/receiveDetail/list.do'
	                ,where:{receiveNumber: obj.data.receiveNumber}
	                ,toolbar: '#toolbar'
	                ,title: 'machineList'
	                /* ,id :'contenttable' */
	                ,limits:[10,20,30]
	                ,defaultToolbar:['filter', 'print']
	                ,cols: [
	                	[
	                		{field:'',title:'验收详情',colspan:13,align:'center'},
	                	],
	                	[
	                         //{type: 'checkbox', fixed: 'left'},
	                         {field:'id', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
	                         {field:'materiel_name', title:'物料名称', align:'center'},
	                         {field:'materiel_num', title:'产品码', align:'center'},
	                         {field:'brevity_num', title:'简码', align:'center',edit: true},
	       				  	 {field:'sendNum', title:'发货数量', align:'center'},
	    				  	 {field:'receiveNum', title:'收货数量', align:'center'},
	                         {field:'is_devanning', title:'是否拆箱', align:'center',edit: true},
	                         {field:'devanning_num', title:'拆箱数量', align:'center',edit: true},
	                         {field:'storage_type', title:'入库类型', align:'center',edit: true},
	                         {field:'barcode_type', title:'条码类型', align:'center',edit: true},
	                         {field:'placement_type', title:'放置类型', align:'center',edit: true},
	                         {field:'removal_container', title:'拆箱后容器', align:'center',edit: true},
	                         {field:'remark', title:'备注', align:'center'}
	                       
	                     ]]
	                ,page: true
	                ,done : function(){
	                    $('th').css({
	                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                        'font-size': 10,
	                    });
	                    this.where={};
	                }
	            });
        	
        });
        
        
        
        //监听行单击事件（单击事件为：rowDouble）
        table.on('row(tableListSub)', function(obj){
        	console.log(obj.data.id);
        	  table.render({
	                elem: '#tableListSubDetail'
	                ,url:'${pageContext.request.contextPath }/receiveDetailCode/list.do'
	                ,where:{rdid: obj.data.id}
	                ,toolbar: '#toolbar'
	                ,title: 'machineList'
	                /* ,id :'contenttable' */
	                ,limits:[10,20,30]
	                ,defaultToolbar:['filter', 'print']
	                ,cols: [
	                	[
	                		{field:'',title:'验收明细',colspan:8,align:'center'},
	                	],
	                	[
	                         //{type: 'checkbox', fixed: 'left'},
	                         {field:'id', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
	                         {field:'code', title:'条码', align:'center'},
	                         {field:'receiveNum', title:'数量', align:'center'},
	                         {field:'pici', title:'批次', align:'center',edit: true},
	       				  	
	    				  	 {field:'state', title:'状态', align:'center'},
	                         {field:'remark', title:'备注', align:'center',edit: true},
	                       
	                     ]]
	                ,page: true
	                ,done : function(){
	                    $('th').css({
	                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                        'font-size': 10,
	                    });
	                    this.where={};
	                }
	            });
        	
        });
        
        laydate.render({
            elem: '#start_date'
        });
        laydate.render({
            elem: '#end_date'
        });
    });
</script>
</html>