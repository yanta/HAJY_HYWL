<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>入库报表</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>退库月报表</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="date" id="date" placeholder="请选择月份" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px"> 
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script src="${pageContext.request.contextPath }/js/td/layuiRowspan.js" charset="utf-8"></script>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//执行一个laydate实例
			laydate.render({
				elem: '#date', //指定元素
				type: 'month',
				value: new Date(),
				format: 'yyyy-MM', //自动生成的时间格式   
				istime: true, //必须填入时间 
				btns: ['now', 'confirm']
			});
			var $ = layui.jquery, active = {
				reload:function () {
	                var date = $.trim($("#date").val());
					table.reload('tableList',{
						method:'get'
						,where:{
	                        'date':date+'-01'
						}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
                ,url:'${pageContext.request.contextPath }/llmReport/getMonthCancellingStockReportByCustomer.do?type=month'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
				,cols: [
		                [{
		                    type: 'checkbox',
		                    fixed: 'left'
		                }, {
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                } , {
		                    field: 'customer_name',
		                    title: '客户名称',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_name;
	                            }
                                return value;
	                        },
		                /* } , {
		                    field: 'customer_num',
		                    title: '客户编号',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_num;
	                            }
                                return value;
	                        }, 
		                } , {
		                    field: 'materiel_num',
		                    title: '产品码',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '简码',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },*/
		                }, {
		                    field: 'materiel_name',
		                    title: '物料名称',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'num1',
		                    title: '1日',
		                    align:'center',
		                }, {
		                    field: 'num2',
		                    title: '2日',
		                    align:'center',
		                }, {
		                    field: 'num3',
		                    title: '3日',
		                    align:'center',
		                }, {
		                    field: 'num4',
		                    title: '4日',
		                    align:'center',
		                }, {
		                    field: 'num5',
		                    title: '5日',
		                    align:'center',
		                }, {
		                    field: 'num6',
		                    title: '6日',
		                    align:'center',
		                }, {
		                    field: 'num7',
		                    title: '7日',
		                    align:'center',
		                }, {
		                    field: 'num8',
		                    title: '8日',
		                    align:'center',
		                }, {
		                    field: 'num9',
		                    title: '9日',
		                    align:'center',
		                }, {
		                    field: 'num10',
		                    title: '10日',
		                    align:'center',
		                }, {
		                    field: 'num11',
		                    title: '11日',
		                    align:'center',
		                }, {
		                    field: 'num12',
		                    title: '12日',
		                    align:'center',
		                }, {
		                    field: 'num13',
		                    title: '13日',
		                    align:'center',
		                }, {
		                    field: 'num14',
		                    title: '14日',
		                    align:'center',
		                }, {
		                    field: 'num15',
		                    title: '15日',
		                    align:'center',
		                }, {
		                    field: 'num16',
		                    title: '16日',
		                    align:'center',
		                }, {
		                    field: 'num17',
		                    title: '17日',
		                    align:'center',
		                }, {
		                    field: 'num18',
		                    title: '18日',
		                    align:'center',
		                }, {
		                    field: 'num19',
		                    title: '19日',
		                    align:'center',
		                }, {
		                    field: 'num20',
		                    title: '20日',
		                    align:'center',
		                }, {
		                    field: 'num21',
		                    title: '21日',
		                    align:'center',
		                }, {
		                    field: 'num22',
		                    title: '22日',
		                    align:'center',
		                }, {
		                    field: 'num23',
		                    title: '23日',
		                    align:'center',
		                }, {
		                    field: 'num24',
		                    title: '24日',
		                    align:'center',
		                }, {
		                    field: 'num25',
		                    title: '25日',
		                    align:'center',
		                }, {
		                    field: 'num26',
		                    title: '26日',
		                    align:'center',
		                }, {
		                    field: 'num27',
		                    title: '27日',
		                    align:'center',
		                }, {
		                    field: 'num28',
		                    title: '28日',
		                    align:'center',
		                }, {
		                    field: 'num29',
		                    title: '29日',
		                    align:'center',
		                }, {
		                    field: 'num30',
		                    title: '30日',
		                    align:'center',
		                }, {
		                    field: 'num31',
		                    title: '31日',
		                    align:'center',
		                }
		                ]
		               ]
				,height: 680
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	                layuiRowspan(['customer_name'], 1);//支持数组
	            }
			});
		});
		toastrStyle()
	</script>
</body>
</html>