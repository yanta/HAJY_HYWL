<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>物料类型转换</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
<style type="text/css">
	.layui-table-click{
	    background-color:#ddf2e9 !important;
	}
</style>
</head>
<body>
<div class="x-nav">
	<span class="layui-breadcrumb">
		<a href="">首页</a>
		<a>
		  <cite>物料类型转换</cite>
		</a>
	</span>
</div>	
<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-input-inline layui-form" style="width:180px">
							<select class="layui-input" name="keyword02" id="keyword02"  lay-filter="keyword02">
								<option value="0">良品转不良品</option>
								<option value="1">不良品转良品</option>
							</select>
						</div>
					</td>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" name="keyword01" id="keyword01" placeholder="请选择创建时间" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload" style="margin-left: 25px;"><i class="layui-icon layui-icon-search"></i>检索</button>
    	<xblock>
			<button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
		</xblock>
    	<!-- 列表 -->
		<table class="layui-hide" id="barCodeTable" lay-filter="barCodeTable"></table>
		<br>
		<div id="subDiv" hidden="hidden">
			<xblock>
				<button class="layui-btn layui-btn-warm" id="addSub"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
			</xblock>
			<!-- 列表2 -->
			<table class="layui-hide" id="subBarCodeTable" lay-filter="subBarCodeTable"></table>
		</div>
	</div>
</div>

<div id="addDeliveryNode" hidden="hidden">
	<form class="layui-form" id="deliveryNodeFormId">
		<xblock>
			<br>
			<table>
				<tr>
					<td><input type="hidden" id="number" name="number"></td>
				</tr>
				<tr>
					<td><label class="layui-form-label">分类</label></td>
					<td>
						<div class="layui-input-inline" style="width:190px; float: left;">
							<select id="carId" name="carId" lay-verify="carId" lay-filter="carId">
								<option value="0">良品转不良品</option>
								<option value="1">不良品转良品</option>
							</select>
						</div>
						<div style="margin-top: 12px;">
							<span style="color:red; font-size: 24px; margin-left: 10px">*</span>
					    </div>
					</td>
				</tr>
			</table>
		</xblock>
		<xblock>
			<button class="layui-btn layui-btn-blue" lay-submit lay-filter="deliveryNodeForm" style="margin-left:120px; margin-bottom: 10px">提交</button>
			&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>
		</xblock>
	</form>
</div>

<script type="text/html" id="rowToolbar">
	<a class="layui-btn layui-btn-xs" lay-event="confirmor"><i class="layui-icon layui-icon-edit"></i>确认</a>
</script>

<script type="text/javascript">
       var table;
	layui.use(['table','layer','upload','form','laydate'], function(){
		table = layui.table;
		var layer = layui.layer;
		var form = layui.form;
		var laydate = layui.laydate;
		var $ = layui.jquery, active = {
			reload:function () {
				var keyword01 = $("#keyword01").val();
				var keyword02 = $("#keyword02").val();
				table.reload('barCodeTableId',{
					method:'get',
					where:{"create_date":keyword01, "type":keyword02}
				});
			}
		}
		$('.layui-btn').on('click', function(){
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		//日期控件
		laydate.render({
        	elem: '#keyword01'
        });
		
		/* 列表 */
		table.render({
			elem: '#barCodeTable'
			,url:'${pageContext.request.contextPath }/LlmCodemarkSwitch/list.do'
			,title: '物料类型转换列表'
			,id :'barCodeTableId'
			,limits:[10,20,30]
			,toolbar: '#toolbarDemo'
			,cols: [[
				{type: 'checkbox',fixed: 'left'},
				{field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
				{field:'number', title:'编号', align:'center'},
				{field:'type', title:'分类', align:'center'
					,templet: function(d){
                        var value = d.type;
                        if(value == 0){
                            value = "不良品转良品"
                        }else if(value == 1){
                            value = "良品转不良品"
                        }
                        return value;
                    }
                },
				{field:'userName', title:'创建人', align:'center'},
				{field:'createDate', title:'创建时间', align:'center'},
				{field:'approvalName', title:'审批人', align:'center'},
			 	{field:'approvalDate', title:'审批时间', align:'center'},
			  	{field:'confirmor', title:'确认人', align:'center'
			  		,templet: function(d){
                        var value = d.confirmor;
                        if(value == 0){
                            value = ""
                        }
                        return value;
                    }
			  	},
			 	{field:'confirmorDate', title:'确认时间', align:'center'},
			 	{field: 'status',title: '状态',align:'center'
                    ,templet: function(d){
                        var value = d.status;
                        if(value == 1){
                            value = "待确认"
                        }else if(value == 2){
                            value = "已确认"
                        }else if(value == 0){
                            value = "待审批"
                        }
                        return value;
                    }
                },
			 	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
			  	]]
			,page: true
			,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
		});
		
		//新增
       	$("#add").click(function(){
           	//时间戳
       		var timestamp = (new Date()).getTime();
       		layer.open({
				type: 1 				//Page层类型
				,area: ['400px', '200px'] 	//宽  高
				,title: '新增'
				,shade: 0.1 			//遮罩透明度
				,shadeClose: true 		//点击遮罩关闭
				,maxmin: true 			//允许全屏最小化
				,anim: 1 				//0-6的动画形式，-1不开启
				,content: $('#addDeliveryNode')
				,success: function () {
					document.getElementById("deliveryNodeFormId").reset();
                 	$("#number").val(curentTimeNumber());	//调用方法，自动生成单号
				}
				,end: function () {
					var formDiv = document.getElementById('deliveryNodeFormId');
					formDiv.style.display = '';
					layer.close();
				}
			});
       	});
		
       	/**
		 * 退库单的提交(新增/编辑)
		 */
		form.on('submit(deliveryNodeForm)', function (data) {
			$.ajax({
				url : '${pageContext.request.contextPath}/LlmCodemarkSwitch/insert.do',
				data: $("#deliveryNodeFormId").serialize(),
				cache : false,
				type : "post",
			}).done(
				function(res) {
					if (res > 0) {
						toastr.success('新增成功！');
					}else{
						toastr.error('新增失败！');
					}
				}
			).fail(
				function(res) {
					toastr.error('新增失败！');
				}
			)
			setTimeout(function(){
				location.reload();
			},1000);
			return false;
		});
       	
		//批量删除
		$('#deletes').click(function(obj){
        	var rowData = table.checkStatus('barCodeTable');
			var data = rowData.data;
        	var str = "";
        	var b = 0;
        	if(data.length==0){
        		toastr.warning("请至少选择一条记录！");
        	}

        	for (var i = 0; i < data.length; i++){
				if(data[i].status == "2"){
					b += 1;
				}
        	}
			if(b == 0){

        		for(var i=0;i<data.length;i++){
            		str += data[i].id;
            		if(i != data.length-1){
            			str += ",";
            		}
            	}
        		layer.confirm('确定删除吗？', function(index){
    				$.ajax({
    			    	type:'post',
    			    	url:'${pageContext.request.contextPath }/LlmCodemarkSwitch/delete.do',
    			    	data:{"ids":str},
    			    	dataType : "json",
    			    	success:function(data){
    			    		if(data>0){
    			    			toastr.success("删除成功！");
    			    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
    								//关闭模态框
    								// 父页面刷新
    								window.location.reload();
    							},1000);
    			    		}else{
    			    			toastr.error("删除失败！");
    			    		}
    			    	}
    			    })
    		    layer.close(index);
    			});
			}else{
        		toastrStyle();
				toastr.warning("已确认，不能删除！");
        	}
		});
	});
	//单号
	function curentTimeNumber() {
		var now=new Date();
		var year = now.getFullYear();       //年
	    var month = now.getMonth() + 1;     //月
	    var day = now.getDate();            //日
	 	var hours = now.getHours(); //获取当前小时数(0-23)
   	var minutes = now.getMinutes(); //获取当前分钟数(0-59)
   	var seconds = now.getSeconds(); //获取当前秒数(0-59)
	    //var mill=now.getMilliseconds();
	    var time=year+""+add0(month)+""+add0(day)+""+add0(hours)+""+add0(minutes)+""+add0(seconds);
		return time;
	}
	function add0(m){
		return m<10?'0'+m:m 
	}
	toastrStyle()
</script>	
</body>
</html>