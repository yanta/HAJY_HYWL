<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>退库原因</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
<style type="text/css">
	.layui-table-click{
	    background-color: #ddf2e9 !important;
	}
</style>
</head>
<body>
	
<div class="x-nav">
	<span class="layui-breadcrumb">
		<a href="">首页</a>
		<a>
		  <cite>退库原因</cite>
		</a>
	</span>
</div>

<div id="barCodeDiv" hidden="hidden">
	<br>
	<form class="layui-form" id="barCodeForm">
		<input id="id" name="id" hidden="hidden">
		<div class="layui-form-item">
			<div class="layui-inline">
		    	<label class="layui-form-label" style="margin-left: 50px;">退库原因</label>
		      	<div class="layui-input-inline" style="width:200px">
		      		<input lay-verify="cancellingReason" class="layui-input" type="text" id="cancellingReason" name="cancellingReason" style="width:200px">
				</div>
			</div>
			<div class="layui-inline">
				<font style="color:red; font-size: 24px;">*</font>
		    </div>
		</div>
		<button class="layui-btn layui-btn-blue" lay-submit lay-filter="barCodeFormSubmit" style="margin-left:150px">提交</button>
		&emsp;&emsp;&emsp;&emsp;
		<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>
	</form>
</div>	

<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" name="keyword01" id="keyword01" placeholder="请输入退库原因" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload" style="margin-left: 25px;"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
 	<!-- 头部按钮 -->
   	<xblock>
		<button id="addBarCode" class="layui-btn layui-btn-warm"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
		<button id="deleteBarCodes"" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
   	</xblock> 
   	<!-- 列表 -->
 	<table class="layui-hide" id="barCodeTable" lay-filter="barCodeTable"></table>
	<!-- 行编辑 -->
 	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
	</script>
	
</div>
	
<script type="text/javascript">
       var table;
	layui.use(['table','layer','upload','form','laydate'], function(){
		table = layui.table;
		var layer = layui.layer;
		var form = layui.form;
		var laydate = layui.laydate;
		var $ = layui.jquery, active = {
			reload:function () {
				var keyword01 = $("#keyword01").val();
				table.reload('barCodeTableId',{
					method:'get',
					where:{"cancellingReason":keyword01}
				});
			}
		}
		$('.layui-btn').on('click', function(){
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		/* 列表 */
		table.render({
			elem: '#barCodeTable'
			,url:'${pageContext.request.contextPath }/cancellingStockReasonManager/listPageCancellingStockReason.do'
			,title: '退库单列表'
			,id :'barCodeTableId'
			,limits:[10,20,30]
			,toolbar: '#toolbarDemo'
			,cols: [[
			  	{type: 'checkbox', fixed: 'left', },
			  	{field:'', title:'序号', type:'numbers', align:'center'},
			  	{field:'cancellingReason', title:'退库原因', align:'center'},
			  	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
			  	]]
			,page: true
			,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
		});
		
		
		
		//新增
		$('#addBarCode').click(function(){
			layer.open({
				type: 1 		//Page层类型
				,area: ['460px', '200px'] //宽  高
				,title: '新增'
				,shade: 0.1 	//遮罩透明度
				,shadeClose: true //点击遮罩关闭
				,maxmin: true //允许全屏最小化
				,anim: 1 		//0-6的动画形式，-1不开启
				,content: $('#barCodeDiv')
				,success: function () {
					document.getElementById("barCodeForm").reset();
					$("#id").val("")
				}
				,end: function () {
					var formDiv = document.getElementById('barCodeForm');
					formDiv.style.display = '';
					//layer.close(index);
				}
			});
		});
		
		//批量删除
		$('#deleteBarCodes').click(function(obj){
        	var rowData = table.checkStatus('barCodeTableId');
			var data = rowData.data;
        	var str = "";
        	if(data.length==0){
        		toastr.warning("请至少选择一条记录！");
        	}else{
        		for(var i=0;i<data.length;i++){
            		str += data[i].id;
            		if(i != data.length-1){
            			str += ",";
            		}
            	}
        		layer.confirm('确定删除吗？', function(index){
    				$.ajax({
    			    	type:'post',
    			    	url:'${pageContext.request.contextPath }/cancellingStockReasonManager/deleteCancellingStockReason.do',
    			    	data:{"ids":str},
    			    	dataType : "json",
    			    	success:function(data){
    			    		if(data>0){
    			    			toastr.success("删除成功！");
    			    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
    								//关闭模态框
    								// 父页面刷新
    								window.location.reload();  
    							},2000);
    			    		}else{
    			    			toastr.error("删除失败！");
    			    		}
    			    	}
    			    })
    		    layer.close(index);
    			});
        	}
		});
		
		//编辑
		table.on('tool(barCodeTable)', function(obj){
			var data = obj.data;
			var checkStatus = table.checkStatus(obj.data.id);
			$('#id').val(obj.data.id);
			if(obj.event === 'edit'){
				layer.open({
					type: 1 		//Page层类型
					,area: ['460px', '200px'] //宽  高
					,title: '编辑'
					,shade: 0.1 	//遮罩透明度
					,maxmin: true //允许全屏最小化
					,anim: 1 		//0-6的动画形式，-1不开启
					,content: $('#barCodeDiv')
					,success: function () {
						//回显表单数据
						for(var i=0;i<Object.entries(data).length;i++) {
							var id = '#' + Object.entries(data)[i][0];
							var text = Object.entries(data)[i][1];
							$(id).val(text);
						}
						form.render();
					}	
					,end: function () {
						var formDiv = document.getElementById('barCodeForm');
					  	formDiv.style.display = '';
					  	layer.close(index);
				  	}
				});
			}
		});
		
		/**
		 * 提交(新增/编辑)
		 */
		form.on('submit(barCodeFormSubmit)', function (data) {
			var title = "新增";
			var url = '${pageContext.request.contextPath}/cancellingStockReasonManager/saveCancellingStockReason.do';
			var id = $.trim($("#id").val());
			if(id != "" && id != null && id != 'undefined'){
				url = '${pageContext.request.contextPath}/cancellingStockReasonManager/updateCancellingStockReason.do';
				title = "修改";
			}
			$.ajax({
				url : url,
				data: $("#barCodeForm").serialize(),
				cache : false,
				type : "post",
			}).done(
				function(res) {
					if (res > 0) {
						toastr.success(title + '成功！');
					}else{
						toastr.error(title + '失败！');
					}
					setTimeout(function(){
						location.reload();
					},1000);
				}
			).fail(
				function(res) {
					toastr.error('新增失败！');
					setTimeout(function(){
						location.reload();
					},1000);
				}
			)
			return false;
		});
		
		/**
	     * 退库表单校验
	     */
	    form.verify({
	    	cancellingReason: function(value){
	    		 var str = value.replace(/\s+/g,"");
	    		 if(str == ''){
	    			return '退库原因不能为空';
	    		}
	    	}
	    });
	})	
	toastrStyle()
</script>	

</body>
</html>