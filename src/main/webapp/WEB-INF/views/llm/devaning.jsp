<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>拆箱记录</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
<style type="text/css">
	.layui-table-click{
	    background-color:#ddf2e9 !important;
	}
</style>
</head>
<body>
	
<div class="x-nav">
	<span class="layui-breadcrumb">
		<a href="">首页</a>
		<a>
		  <cite>拆箱记录</cite>
		</a>
	</span>
</div>	

<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" name="keyword01" id="keyword01" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</div>
					</td>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" name="keyword02" id="keyword02" placeholder="请选择拆箱日期" autocomplete="off" style=" width: 180px; margin-left: 10px">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload" style="margin-left: 25px;"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
 	
   	<!-- 列表 -->
 	<table class="layui-hide" id="barCodeTable" lay-filter="barCodeTable"></table>
	
	<!-- 列表2 -->
	<div id='detailDiv1' hidden="hidden">
		<br>
		<h2 style="text-align: center;color:#666">拆箱前条码</h2>
	 	<table class="layui-hide" id="subBarCodeTable" lay-filter="subBarCodeTable"></table>
	 	
	 	<br>
	 	<!-- 列表3 -->
	 	<h2 style="text-align: center;color:#666">拆箱后条码</h2>
	 	<table class="layui-hide" id="subBarCodeTable2" lay-filter="subBarCodeTable2"></table>
 	
	 	<br>
	 	<!-- 列表4 -->
	 	<h2 style="text-align: center;color:#666">工作量</h2>
	 	<table class="layui-hide" id="subBarCodeTable3" lay-filter="subBarCodeTable3"></table>
	</div>
 	
</div>
	
<script type="text/javascript">
       var table;
       var a = 0;
	layui.use(['table','layer','upload','form','laydate'], function(){
		table = layui.table;
		var layer = layui.layer;
		var form = layui.form;
		var laydate = layui.laydate;
		var $ = layui.jquery, active = {
			reload:function () {
				var keyword01 = $("#keyword01").val();
				var keyword02 = $("#keyword02").val();
				table.reload('barCodeTableId',{
					method:'get',
					where:{"materile_name":keyword01, "work_time":keyword02}
				});
			}
		}
		$('.layui-btn').on('click', function(){
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		//日期控件
		laydate.render({
		  elem: '#keyword02'
		});
		
		/* 列表 */
		table.render({
			elem: '#barCodeTable'
			,url:'${pageContext.request.contextPath }/devaning/list.do'
			,title: '拆箱物料列表'
			,id :'barCodeTableId'
			,limits:[10,20,30]
			,toolbar: '#toolbarDemo'
			,cols: [[
				{field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
				{field:'receive_detail_quality_id', title:'收货单详情数量ID', hide:true},
				{field:'bnum', title:'拆箱记录编号', hide:true},
				{field:'customer_name', title:'供应商名称', align:'center'},
				{field:'pici', title:'批次', align:'center'},
			  	{field:'materiel_name', title:'物料名称', align:'center'},
			 	{field:'materiel_num', title:'产品码', align:'center'},
   				{field:'brevity_num', title:'物料简码', align:'center'},
   				{field:'materiel_size', title:'物料规格', align:'center'},
   				{field:'materiel_properties', title:'物料型号', align:'center'},
   				{field:'knum', title:'拆箱总数', align:'center'},
   				{field:'remark', title:'操作时间', align:'center',width: 200},
			  	]]
			,page: true
			,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
		});
		table.on('row(barCodeTable)', function(obj){
			
			$('#detailDiv1').removeAttr('hidden');
			
			table.render({
				elem: "#subBarCodeTable"
                ,url:'${pageContext.request.contextPath }/devaning/queryCodemarkByNum1.do?num=' + obj.data.bnum
                ,title: 'machineList'
                //,totalRow: true //开启合计行
                ,cols:
					[[
                        {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                        {field: 'code',title: '条码',align:'center'/* , totalRowText: '合计：' */},
                        {field: 'quantity',title: '单箱数量(个)',align:'center', totalRow: true, templet: function(d){
							if(d.quantity == 0){
								return 0;
							} else {
								return d.quantity;
							}
						}},
						{field: 'is_ng',title: '物料质量',align:'center', templet: function(d){
							if(d.is_ng == 0){
								return	"良品";
							} else {
								return "不良品";
							}
						}},
						{field: 'status',title: '包装分类',align:'center', templet: function(d){
							if(d.status == 0){
								return	"拆箱前";
							} else {
								return "拆箱后";
							}
						}},
                    ]]
                ,page: true
			});
			
			table.render({
				elem: "#subBarCodeTable2"
                ,url:'${pageContext.request.contextPath }/devaning/queryCodemarkByNum2.do?num=' + obj.data.bnum
                ,title: 'machineList'
                //,totalRow: true //开启合计行
                ,cols:
					[[
                        {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                        {field: 'code',title: '条码',align:'center'/* , totalRowText: '合计：' */},
                        {field: 'quantity',title: '单箱数量(个)',align:'center', totalRow: true, templet: function(d){
							if(d.quantity == 0){
								return 0;
							} else {
								return d.quantity;
							}
						}},
						{field: 'is_ng',title: '物料质量',align:'center', templet: function(d){
							if(d.is_ng == 0){
								return	"良品";
							} else {
								return "不良品";
							}
						}},
						{field: 'status',title: '包装分类',align:'center', templet: function(d){
							if(d.status == 0){
								return	"拆箱前";
							} else {
								return "拆箱后";
							}
						}},
                    ]]
                ,page: true
			});
			
			table.render({
				elem: "#subBarCodeTable3"
                ,url:'${pageContext.request.contextPath }/devaningWork/queryWorkByNum.do?num=' + obj.data.bnum
                ,title: 'machineList'
                ,cols:
					[[
                        {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                        {field: 'userName',title: '拆箱人',align:'center'},
                        {field: 'boxQuantity',title: '拆箱后总箱数',align:'center'},
						{field: 'pcsQuantity',title: '拆箱物料总数',align:'center'},
                    ]]
                ,page: false
			});
		})
	});
		
	toastrStyle()
</script>	

</body>
</html>