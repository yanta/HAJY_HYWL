<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>绑码记录</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
<style type="text/css">
	.layui-table-click{
	    background-color:#ddf2e9 !important;
	}
</style>
</head>
<body>
	
<div class="x-nav">
	<span class="layui-breadcrumb">
		<a href="">首页</a>
		<a>
		  <cite>绑码记录</cite>
		</a>
	</span>
</div>	

<!-- 顶部搜索框 -->
<div class="x-body">
     <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<input class="layui-input" name="keyword01" id="keyword01" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload" style="margin-left: 25px;"><i class="layui-icon layui-icon-search"></i>检索</button>
	</div>
 	
   	<!-- 列表 -->
 	<table class="layui-hide" id="barCodeTable" lay-filter="barCodeTable"></table>
	
	<!-- 列表2 -->
 	<table class="layui-hide" id="subBarCodeTable" lay-filter="subBarCodeTable"></table>
</div>
	
<script type="text/javascript">
       var table;
	layui.use(['table','layer','upload','form','laydate'], function(){
		table = layui.table;
		var layer = layui.layer;
		var form = layui.form;
		var laydate = layui.laydate;
		var $ = layui.jquery, active = {
			reload:function () {
				var keyword01 = $("#keyword01").val();
				table.reload('barCodeTableId',{
					method:'get',
					where:{"materiel_name":keyword01}
				});
			}
		}
		$('.layui-btn').on('click', function(){
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		/* 列表 */
		table.render({
			elem: '#barCodeTable'
			,url:'${pageContext.request.contextPath }/codemark/listPageCodemark.do'
			,title: '退库单列表'
			,id :'barCodeTableId'
			,limits:[10,20,30]
			,toolbar: '#toolbarDemo'
			,cols: [[
				{field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
				{field:'receive_detail_quality_id', title:'收货单详情数量ID', hide:true},
				{field:'customer_name', title:'供应商名称', align:'center'},
				{field:'pici', title:'批次', align:'center'},
			  	{field:'materiel_name', title:'物料名称', align:'center'},
			 	{field:'materiel_num', title:'产品码', align:'center'},
   				{field:'brevity_num', title:'物料简码', align:'center'},
   				{field:'materiel_size', title:'物料规格', align:'center'},
   				{field:'materiel_properties', title:'物料型号', align:'center'},
   				{field:'knum', title:'入库数量', align:'center'},
			  	]]
			,page: true
			,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
		});
		
		table.on('row(barCodeTable)', function(obj){
			table.render({
				elem: "#subBarCodeTable"
                ,url:'${pageContext.request.contextPath }/codemark/queryCodemarkById2.do?detailId=' + obj.data.receive_detail_quality_id
                ,title: 'machineList'
                ,toolbar: '#toolbarDemo'
                ,cols:
					[[
                        {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                        {field: 'code',title: '条码',align:'center'},
                        {field: 'num',title: '单个数量(个)',align:'center', totalRow: true, templet: function(d){
							if(d.num == 0){
								return 0;
							} else {
								return d.num;
							}
						}},
						{field: 'is_ng',title: '物料类型',align:'center', templet: function(d){
							if(d.is_ng == 0){
								return	"良品";
							} else {
								return "不良品";
							}
						}} 
                    ]]
                ,page: false
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
			});
		})
	});
		
	toastrStyle()
</script>	

</body>
</html>