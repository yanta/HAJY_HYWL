<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>配送管理</title>
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
<style type="text/css">
	.layui-table-click{
	    background-color:#ddf2e9 !important;
	}
</style>
</head>
<body>
	
	<div class="x-nav">
		<span class="layui-breadcrumb">
			<a href="">首页</a>
			<a>
			  <cite>配送管理</cite>
			</a>
		</span>
	</div>
	
	<div class="x-body">
		<!-- 主表头部搜索框 -->
		<div style="margin-bottom: 18px">
			<div class="layui-inline">
				<table>
					<tr>
						<td>
							<div class="layui-form" style="width: 180px; margin-left: 10px">
								<input class="layui-input" type="text" name="keyword01" id="keyword01" placeholder="请输入配送单号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
							</div>
						</td>
						<td>
							<div class="layui-form" style="width: 180px; margin-left: 10px">
								<input class="layui-input" name="keyword02" id="keyword02" placeholder="请输入车牌号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
							</div>
						</td>
						<td>
							<div class="layui-form" style="width: 180px; margin-left: 10px">
								<input class="layui-input" name="keyword03" id="keyword03" placeholder="请输入司机姓名" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
							</div>
						</td>
					</tr>
				</table>
			</div>
			<button class="layui-btn layui-btn-normal" data-type="reload" style="margin-left: 20px"><i class="layui-icon layui-icon-search"></i>检索</button>
		</div>
		<!-- 主表头部按钮 -->
		<xblock>
			<button id="add" class="layui-btn layui-btn-warm"><i class="layui-icon layui-icon-add-circle-fine"></i>新增配送单</button>
			<button id="dels" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
  		</xblock> 
  		<!-- 主表（配送单） -->
		<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
		<!-- 主表的行事件 -->
		<script type="text/html" id="rowToolbar">
			<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
		</script>
	</div>
	<!-- 子表（出库单） -->
	<div id="sublist" hidden="hidden">
		<!-- 主表头部按钮 -->
		<xblock>
			<button id="addOutStock" class="layui-btn layui-btn-warm"><i class="layui-icon layui-icon-add-circle-fine"></i>添加出库单</button>
			<button id="delsOutStock" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
  		</xblock> 
		<table class="layui-hide" id="sublistTable" lay-filter="sublistTable"></table>
	</div>
	<!-- 子表的行事件 -->
	<script type="text/html" id="rowToolbar2">
		<a class="layui-btn layui-btn-xs" lay-event="select"><i class="layui-icon layui-icon-search"></i>查看</a>
	</script>
	
	<!-- 详情表（出库单详情） -->
	<div id="sublistDetail" hidden="hidden">
		<table class="layui-hide" id="sublistTableDetail" lay-filter="sublistTableDetail"></table>
	</div>
	
	<div id="addDeliveryNode" hidden="hidden">
		<form class="layui-form" id="deliveryNodeFormId">
			<xblock>
				<br>
				<table>
					<tr>
						<td><input type="hidden" id="deliveryNodeId" name="id"></td>
					</tr>
					<tr>
						<td><label class="layui-form-label">送货单号</label></td>
						<td>
							<input readonly class="layui-input" id="number" name="number" style="color: gray; width: 190px">
						</td>
						<td><label class="layui-form-label">车牌号</label></td>
						<td>
							<div class="layui-input-inline" style="width:190px; float: left;">
								<select id="carId" name="carId" lay-verify="carId" lay-filter="carId">
									<option value="">请选择车牌号</option>
									<c:forEach items="${allCar}" var="car">
										<option value="${car.id}">${car.plate}</option>
									</c:forEach>
								</select>
							</div>
							<div style="margin-top: 12px;margin-right: -20px;">
								<span style="color:red; font-size: 24px; margin-left: 10px">*</span>
						    </div>
						</td>
					</tr>
					<tr style="height: 10px"></tr>
					<tr>
						<td><label class="layui-form-label">司机姓名</label></td>
						<td>
							<input readonly class="layui-input" id="carOwner" name="carOwner" style="color: gray; width: 190px">
						</td>
						<td><label class="layui-form-label">联系方式</label></td>
						<td>
							<input readonly class="layui-input" id="phone" name="phone" style="color: gray; width: 190px">
						</td>
					</tr>
					<tr style="height: 10px"></tr>
					<tr>
						<td><label class="layui-form-label">备注</label></td>
						<td>
							<input class="layui-input" id="remark" name="remark" style="color: gray; width: 190px">
						</td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</xblock>
			<xblock>
				<button class="layui-btn layui-btn-blue" lay-submit lay-filter="deliveryNodeForm" style="margin-left:240px; margin-bottom: 10px">提交</button>
				&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>
			</xblock>
		</form>
	</div>
	
	<!-- 添加出库单的弹框 -->
	<div id="addOutStockDiv" hidden="hidden">
	
		<table class="layui-hide" id="outStockListTable" lay-filter="outStockListTable"></table>
		<xblock>
			<button class="layui-btn layui-btn-blue" lay-submit lay-filter="outStockForm" style="margin-left:600px; margin-bottom: 10px">提交</button>
		</xblock>
	</div>
	<script type="text/javascript">
		var deliveryId;
		var table;
		var deliveryStatus;
		layui.use(['table','layer','upload','form','laydate'], function(){
	        	table = layui.table,
	        	layer = layui.layer,
	        	upload = layui.upload,
	        	form = layui.form,
	        	laydate = layui.laydate;
	        
	        var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					var keyword02 = $("#keyword02").val();
					var keyword03 = $("#keyword03").val();
					table.reload('contenttable',{
						method:'get'
						,where:{
						    "number":keyword01
						    ,"carPlate":keyword02
						    ,"carOwner":keyword03
						}
						,page:{
							curr: 1//重新从第 1 页开始
						}
					});
				}
			}
	        
	        $('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
	        
	      	//监听复选框事件，被选中的行高亮显示
		    table.on('checkbox(tableList)', function(obj){
		    	if(obj.checked == true && obj.type == 'all'){
			        //点击全选
			        $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
			    }else if(obj.checked == false && obj.type == 'all'){
			        //点击全不选
			        $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
			    }else if(obj.checked == true && obj.type == 'one'){
			        //点击单行
				    if(obj.checked == true){
				          obj.tr.addClass('layui-table-click');
				    }else{
				          obj.tr.removeClass('layui-table-click');
				    }
			    }else if(obj.checked == false && obj.type == 'one'){
			        //点击全选之后点击单行
			        if(obj.tr.hasClass('layui-table-click')){
			        	obj.tr.removeClass('layui-table-click');
			        }
			    }
		    }); 
	        
	        //点击行checkbox选中
    	   	$(document).on("click",".layui-table-body table.layui-table tbody tr", function () {
    	        var index = $(this).attr('data-index');
    	        var tableBox = $(this).parents('.layui-table-box');
    	        //存在固定列
    	        if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length>0) {
    	            tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
    	        } else {
    	            tableDiv = tableBox.find(".layui-table-body.layui-table-main");
    	        }
    	        var checkCell = tableDiv.find("tr[data-index=" + index + "]").find("td div.laytable-cell-checkbox div.layui-form-checkbox I");
    	        if (checkCell.length>0) {
    	            checkCell.click();
    	        }
    	    });
    	
    	    $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
    	        e.stopPropagation();
    	    }); 
    	    
	        /* 主表（配送单） */
	        table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/DeliveryNode/list.do'
				,toolbar: '#toolbar'
				,title: '配送单'
				,id :'contenttable'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,cols: [[
					{type: 'checkbox', fixed: 'left', },
				  	{field:'', title:'序号', type:'numbers', align:'center'},
				  	{field:'number', title:'配送单号', align:'center'},
				  	{field:'carPlate', title:'车牌号', align:'center'},
				  	{field:'carOwner', title:'司机姓名', align:'center'},
				  	{field:'phone', title:'联系方式', align:'center'},
				  	/* {field:'startDate', title:'出发时间', align:'center'},
				  	{field:'endDate', title:'送达时间', align:'center'}, */
				  	{field:'status', title:'状态', align:'center', templet: function (row){
   	                    if (row.status == '0') {
   	                        return "未出发";
   	                    }else if(row.status == '1'){
   	                        return "配送中";
   	                    }else if(row.status == '2'){
   	                        return "已送完";
   	                    }
   	                }},
                    {field:'remark', title:'备注', align:'center'},
				  	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
			  	]]
				,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
			});
	        
	      	/* 主表的行点击事件 */
            table.on('row(tableList)', function(obj) {
            	$('#sublist').removeAttr("hidden");
            	var data = obj.data;
            	deliveryId = data.id;
            	deliveryStatus = data.status;
            	table.render({
                    elem: '#sublistTable'
                    //根据送货单id查询该送货单关联的所有出库单
                    ,url:'${pageContext.request.contextPath }/DeliveryNode/queryStockOutByDeliveryId.do?deliveryId='+data.id
                    ,toolbar: '#toolbar'
                    ,title: '出库单'
                    ,id :'subContenttable'
                    ,limits:[10,20,30]
                    //,defaultToolbar:['filter', 'print']
                    ,cols: [[
                    	{type: 'checkbox', fixed: 'left', },
                    	{field:'', title:'序号', type:'numbers', align:'center'},
    				  	{field:'out_stock_num', title:'出库单号', align:'center'},
    				  	{field:'create_date', title:'创建时间', align:'center'},
    				  	{field:'warehouse_name', title:'仓库名称', align:'center'},
    				  	{field:'warehouse_address', title:'仓库地址', align:'center'},
    				  	{field:'customer_name', title:'主机厂名称', align:'center'},
    				  	{field:'customer_adress', title:'主机厂地址', align:'center'},
    				  	{field:'contacts_name', title:'主机厂负责人', align:'center'},
    				 	{field:'contacts_tel', title:'负责人电话', align:'center'},
    				 	{field:'start_date', title:'出发时间', align:'center'},
    				  	{field:'end_date', title:'送达时间', align:'center'},
    				  	{field:'arrival_date', title:'签收时间', align:'center'},
    				  	{field:'status', title:'状态', align:'center', templet: function (row){
       	                    if (row.status == '0') {
       	                        return "未出库";
       	                    } else if (row.status == '1'){
       	                        return "已出库";
       	                    }else if (row.status == '2'){
       	                        return "配送中";
       	                    }else if (row.status == '3'){
       	                        return "已到达";
       	                    }else if (row.status == '4'){
    	                        return "已收货";
    	                    }
       	                }},
       	             	//{field:'return_reason', title:'退回原因', align:'center'},
                        /* {field:'customer_order', title:'客户单号', align:'center'}, */
       	             	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar2',width:100, align: 'center'}
     				]]
                    ,page: true
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });
	      	
          	//根据车辆ID获取联系人信息
    	    form.on('select(carId)', function(data){
    	    	var id = data.value;	//改变后的值
    	    	$.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/stockOutOrder/queryDriver.do?',
                   data: {id:data.value},
                    dataType: 'json',
                    async: false,
                    success: function (datas){
                    	console.log(datas[0])
                    	$("#carOwner").val(datas[0].user_name);
                    	$('#phone').val(datas[0].phone);
                    }
                });
    	    });
          
			//新增
           	$("#add").click(function(){
	           	//时间戳
	       		var timestamp = (new Date()).getTime();
	       		layer.open({
					type: 1 				//Page层类型
					,area: ['655px', ''] 	//宽  高
					,title: '新增'
					,shade: 0.1 			//遮罩透明度
					,shadeClose: true 		//点击遮罩关闭
					,maxmin: true 			//允许全屏最小化
					,anim: 1 				//0-6的动画形式，-1不开启
					,content: $('#addDeliveryNode')
					,success: function () {
						document.getElementById("deliveryNodeFormId").reset();
						$("#deliveryNodeId").val("")
	                 	$("#number").val(curentTimeNumber());	//调用方法，自动生成退货单号
					}
					,end: function () {
						var formDiv = document.getElementById('deliveryNodeFormId');
						formDiv.style.display = '';
						layer.close();
					}
				});
           	});
			
          	//送货单的编辑
    		table.on('tool(tableList)', function(obj){
    			var data = obj.data;
    			var tableId = data.id;
    			var checkStatus = table.checkStatus(obj.data.id);
    			if(obj.event === 'update'){
    				if(data.status == 0){
    					layer.open({
    						type: 1 				//Page层类型
    						,area: ['40%', ''] 	//宽  高
    						,title: '编辑'
    						,shade: 0.1 			//遮罩透明度
    						,maxmin: true 			//允许全屏最小化
    						,anim: 1 				//0-6的动画形式，-1不开启
    						,content: $('#addDeliveryNode')
    						,success: function () {
    							//回显表单数据
    							for(var i=0;i<Object.entries(data).length;i++) {
    								var id = '#' + Object.entries(data)[i][0];
    								var text = Object.entries(data)[i][1];
    								console.log(id);
    								$(id).val(text);
    							}
    							$('#deliveryNodeId').val(tableId);
    							form.render();
    						}
    						,end: function () {
    							var formDiv = document.getElementById('deliveryNodeFormId');
    						  	formDiv.style.display = '';
    						  	layer.closeAll();
    					  	}
    					});
    				}else{
    					toastr.warning("已配送的记录不能修改！");
    				}
    			}
    		});
          
			form.on('submit(deliveryNodeForm)', function(data){
				var title = "新增";
				var url = '${pageContext.request.contextPath}/DeliveryNode/insert.do';
				var id = $.trim($("#deliveryNodeId").val());
				if(id != "" && id != null && id != 'undefined'){
					url = '${pageContext.request.contextPath}/DeliveryNode/update.do';
					title = "修改";
				}
				$.ajax({
					url : url,
					data: $("#deliveryNodeFormId").serialize(),
					cache : false,
					type : "post",
				}).done(
					function(res) {
						if (res > 0) {
							toastr.success(title + '成功！');
						}else{
							toastr.error(title + '失败！');
						}
					}
				).fail(
					function(res) {
						toastr.error(title + '失败！');
					}
				)
				setTimeout(function(){
					location.reload();
				},1000);
				return false;
			})
			
			
			//批量删除送货单
			$('#dels').click(function(obj){
				var rowData = table.checkStatus('contenttable');
				var data = rowData.data;
	        	var str = "";
	        	var sum = 0;
	        	if(data.length == 0){
	        		toastr.warning("请至少选择一条记录！");
	        	}else{
	        		for(var i=0;i<data.length;i++){
	        			if(data[i].status != 0){
	            			sum += 1;
	            		}
	            	}
	        		if(sum > 0){
	        			toastr.warning("已配送的记录不能删除！");
	        		}else{
	        			for(var i=0;i<data.length;i++){
	                		str += data[i].id;
	                		if(i != data.length-1){
	                			str += ",";
	                		}
	                	}
	        			layer.confirm('确定删除吗？', function(index){
	        				$.ajax({
	        			    	type:'post',
	        			    	url:'${pageContext.request.contextPath }/DeliveryNode/delete.do',
	        			    	data:{"ids":str},
	        			    	dataType : "json",
	        			    	success:function(data){
	        			    		if(data>0){
	        			    			toastr.success("删除成功！");
	        			    		}else{
	        			    			toastr.error("删除失败！");
	        			    		}
        			    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
        								//关闭模态框
        								// 父页面刷新
        								window.location.reload();
        							},1000);
	        			    	}
	        			    })
	        		    	//layer.close(index);
	        			});
	        		}
	        	}
			});
			
			/* -------------------------------------------对字表的操作，添加，删除出库单-------------------------------- */
			//添加出库单
           	$("#addOutStock").click(function(){
           		if(deliveryStatus != 0){
        			toastr.warning("已配送的记录不能修改！");
        		}else{
        			//时间戳
    	       		var timestamp = (new Date()).getTime();
    	       		layer.open({
    					type: 1 				//Page层类型
    					,area: ['80%', '80%'] 	//宽  高
    					,title: '添加出库单'
    					,shade: 0.1 			//遮罩透明度
    					,shadeClose: true 		//点击遮罩关闭
    					,maxmin: true 			//允许全屏最小化
    					,anim: 1 				//0-6的动画形式，-1不开启
    					,content: $('#addOutStockDiv')
    					,success: function () {
    						//出库单列表
    						table.render({
    			                elem: '#outStockListTable'
    			                //根据送货单id查询该送货单关联的所有出库单
    			                ,url:'${pageContext.request.contextPath }/DeliveryNode/queryAllStockOut.do'
    			                ,title: '出库单'
    			                ,id:'outStockListTableId'
    			                ,limits:[10,20,30]
    			                ,cols: [[
    			                	{type: 'checkbox', fixed: 'left', },
    			                	{field:'', title:'序号', type:'numbers', align:'center'},
    							  	{field:'out_stock_num', title:'出库单号', align:'center'},
    							  	{field:'create_date', title:'创建时间', align:'center'},
    							  	{field:'warehouse_name', title:'仓库名称', align:'center'},
    							  	{field:'warehouse_address', title:'仓库地址', align:'center'},
    							  	{field:'customer_name', title:'主机厂名称', align:'center'},
    							  	{field:'customer_adress', title:'主机厂地址', align:'center'},
    							  	{field:'contacts_name', title:'主机厂负责人', align:'center'},
    							 	{field:'contacts_tel', title:'负责人电话', align:'center'},
    							  	{field:'status', title:'状态', align:'center', templet: function (row){
    							  		if (row.status == '0') {
    		       	                        return "未出库";
    		       	                    } else if (row.status == '1'){
    		       	                        return "已出库";
    		       	                    }else if (row.status == '2'){
    		       	                        return "配送中";
    		       	                    }else if (row.status == '3'){
    		       	                        return "已到达";
    		       	                    }
    			   	                }},
    			 				]]
    			                ,page: true
    			                ,done : function(){
    			                    $('th').css({
    			                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
    			                    })
    			                }
    			            });
    					}
    					,end: function () {
    						var formDiv = document.getElementById('outStockListTable');
    						formDiv.style.display = '';
    						layer.close();
    					}
    				});
        		}
           	});
			
			//保存出库单与送货单的关系
           	form.on('submit(outStockForm)', function(obj){
       			var rowData = table.checkStatus('outStockListTableId');
   				var data = rowData.data;
   				var str = "";
   	        	var sum = 0;
   	        	if(data.length == 0){
   	        		toastr.warning("请至少选择一条记录！");
   	        	}else{
           			for(var i=0;i<data.length;i++){
                   		str += data[i].id;
                   		if(i != data.length-1){
                   			str += ",";
                   		}
                   	}
       				$.ajax({
       			    	type:'post',
       			    	url:'${pageContext.request.contextPath }/DeliveryOutStock/insert.do',
       			    	data:{"ids":str, "deliveryId":deliveryId},
       			    	dataType : "json",
       			    	success:function(data){
       			    		if(data>0){
       			    			toastr.success("添加成功！");
       			    		}else{
       			    			toastr.error("添加失败！");
       			    		}
  			    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
  								//关闭模态框
  								// 父页面刷新
  								window.location.reload();
  							},1000);
       			    	}
       			    }); 
   	        	}
				return false;
			})
			
			//批量删除出库单(即删除中间表)
			$('#delsOutStock').click(function(obj){
	        	var rowData = table.checkStatus('subContenttable');
				var data = rowData.data;
	        	var str = "";
	        	var sum = 0;
	        	if(data.length == 0){
	        		toastr.warning("请至少选择一条记录！");
	        	}else{
	        		if(deliveryStatus != 0){
	        			toastr.warning("已配送的记录不能删除！");
	        		}else{
	        			for(var i=0;i<data.length;i++){
	                		str += data[i].id;
	                		if(i != data.length-1){
	                			str += ",";
	                		}
	                	}
	        			layer.confirm('确定删除吗？', function(index){
	        				$.ajax({
	        			    	type:'post',
	        			    	url:'${pageContext.request.contextPath }/DeliveryOutStock/delete.do',
	        			    	data:{"ids":str, "deliveryId":deliveryId},
	        			    	dataType : "json",
	        			    	success:function(data){
	        			    		if(data>0){
	        			    			toastr.success("删除成功！");
	        			    		}else{
	        			    			toastr.error("删除失败！");
	        			    		}
        			    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
        								//关闭模态框
        								// 父页面刷新
        								window.location.reload();
        							},1000);
	        			    	}
	        			    })
	        		    	//layer.close(index);
	        			});
	        		}
	        	}
			});
			
           	/**
             * 表单校验
             */
            form.verify({
                //value：表单的值、item：表单的DOM对象
                carId: function(value, item){
                    if(value == ''){
                        return '车牌号码不能为空';
                    }
                }
            });
		
          	//送货单的编辑
			table.on('tool(sublistTable)', function(obj){
    			var data = obj.data;
    			var tableId = data.id;
    			var checkStatus = table.checkStatus(obj.data.id);
    			if(obj.event === 'select'){
   					layer.open({
   						type: 1 				//Page层类型
   						,area: ['80%', '80%'] 	//宽  高
   						,title: '出库单详情'
   						,shade: 0.1 			//遮罩透明度
   						,maxmin: true 			//允许全屏最小化
   						,anim: 1 				//0-6的动画形式，-1不开启
   						,content: $('#sublistDetail')
   						,success: function () {
   							table.render({
   			                    elem: '#sublistTableDetail'
   			                    ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById.do?keyword01='+tableId 
   			                    ,toolbar: '#toolbar'
   			                    ,title: '出库单详细表'
   			                    ,id :'contenttable1'
   			                    ,limits:[10,20,30]
   			                    ,defaultToolbar:['filter', 'print']
   			                    ,cols: [[
   				   				  	//{type: 'checkbox', fixed: 'left', },
   				   				  	{field:'customer_name', title:'供应商名称', hide:true},
   				   				  	{field:'materiel_name', title:'物料名称', align:'center'},
   				   				 	{field:'materiel_num', title:'产品码', align:'center'},
   					   				{field:'brevity_num', title:'物料简码', align:'center'},
   					   				{field:'materiel_size', title:'物料规格', align:'center'},
   					   				{field:'materiel_properties', title:'物料型号', align:'center'},
   				   				  	{field:'material_quantity', title:'物料数量', align:'center'},
   				   				  	{field:'actual_quantity', title:'实际数量', align:'center'},
   			                        //{field:'single_num', title:'单个数量', align:'center'},
   			                        //{field:'big_num', title:'大箱数量', align:'center'},
   			                        //{field:'small_num', title:'小箱数量', align:'center'}
   				   				 	{field:'arrival_quantity', title:'到货数量', align:'center'},
   				   				  	{field:'return_quantity', title:'退回数量', align:'center'},
   				   					{field:'return_reason', title:'退回原因', align:'center'},
   			     				]]
   			                    ,page: true
   			                    ,done : function(res, curr, count){
   			                    	merge(res);
   			                    	$('th').css({
   			                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
   			                        })
   			                    }
   			                });
   						}
   						,end: function () {
   							var formDiv = document.getElementById('deliveryNodeFormId');
   						  	formDiv.style.display = '';
   						  	layer.closeAll();
   					  	}
   					});
   				}else{
   					toastr.warning("已配送的记录不能修改！");
   				}
    		});
		});
		
		//弹框位置设定
		toastrStyle();
		//生产送货单号
		function curentTimeNumber() {
			var now=new Date();
			var year = now.getFullYear();       //年
		    var month = now.getMonth() + 1;     //月
		    var day = now.getDate();            //日
		 	var hours = now.getHours(); //获取当前小时数(0-23)
	   	var minutes = now.getMinutes(); //获取当前分钟数(0-59)
	   	var seconds = now.getSeconds(); //获取当前秒数(0-59)
		    //var mill=now.getMilliseconds();
		    var time=year+""+add0(month)+""+add0(day)+""+add0(hours)+""+add0(minutes)+""+add0(seconds);
			return time;
		}
		function add0(m){
			return m<10?'0'+m:m 
		}
		function merge(res) {
	        var data = res.data;
	        var mergeIndex = 0;//定位需要添加合并属性的行数
	        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
	        var columsName = ['return_reason'];//需要合并的列名称
	        var columsIndex = [10];//需要合并的列索引值
	        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
	            var trArr = $("#sublistDetail .layui-table-body>.layui-table").find("tr");//所有行
	                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列
	                    if (data[i].return_reason === data[i-1].return_reason) { //后一行的值与前一行的值做比较，相同就需要合并
	                        mark += 1;
	                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", mark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else {
	                        mergeIndex = i;
	                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
	                    }
	                }
	            mergeIndex = 0;
	            mark = 1;
	        }
	    }
	</script>
</body>
</html>