<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>入库报表</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>入库报表</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-input-block" style="width:180px;margin-left: 10px;display:none">
							<select class="layui-input" name="type" id="type" lay-verify="required">
								<option value="week">周报表</option>
								<option value="month">月报表</option>
							</select>
						</div>
					</td>
					<td>
						<input class="layui-input" name="date" id="date" placeholder="请输入日期" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px"> 
					</td>
					<td>
						<div class="layui-input-block" style="width:180px;margin-left: 10px;display:none">
							<select class="layui-input" name="customer_id" id="customer_id" lay-verify="required">
								<option value="">请选择供应商</option>
								<c:forEach items="${customers}" var="customer">
									<option value="${customer.id}">${customer.customer_name}</option>
								</c:forEach>  
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<script src="${pageContext.request.contextPath }/js/td/layuiRowspan.js" charset="utf-8"></script>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//执行一个laydate实例
			laydate.render({
				elem: '#date', //指定元素
				type: 'month',
				value: new Date(),
				format: 'yyyy-MM', //自动生成的时间格式   
				btns: ['now', 'confirm']
			});
			var $ = layui.jquery, active = {
				reload:function () {
					var type = $.trim($("#type").val());
	                var date = $.trim($("#date").val());
	                var customer_id = $.trim($("#customer_id").val());
					table.reload('tableList',{
						method:'get'
						,where:{
							'type':type
	                        ,'date':date+'-01'
	                        ,'customer_id':customer_id
						}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			$("#date").val(curentMonth())
			$("#type").val('month')
			$('#type').attr("disabled",true)
			$("#customer_id").val('${customer_id}')
			$('#customer_id').attr("disabled",true)
			var type = $.trim($("#type").val());
            var date = $.trim($("#date").val());
            var customer_id = $.trim($("#customer_id").val());
			table.render({
				elem: '#tableList'
				,where:{
					'type':type
                       ,'date':date+'-01'
                       ,'customer_id':customer_id
				}
				,url:'${pageContext.request.contextPath }/json/list3.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                    align:'center',
		                    width: 100,
		                /* } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 250,
		                }, {
		                    field: 'num',
		                    title: '当日入库合计',
		                    align:'center',
		                }, {
		                    field: 'num1',
		                    title: '本月入库合计',
		                    align:'center',
		                }
		                ]
		               ]
				,height: 780
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	                layuiRowspan(['customer_name'], 1);//支持数组
	            }
			});
		});
		//获取当前日期
	   	function curentMonth() {
	   		var now=new Date();
	   		var year = now.getFullYear();       //年
	   	    var month = now.getMonth() + 1;     //月
	   	    var day = now.getDate();            //日
	   	    var time=year+"-"+add0(month);
	   		return time;
	   	}
	   	function add0(m){return m<10?'0'+m:m }
		toastrStyle()
	</script>
</body>
</html>