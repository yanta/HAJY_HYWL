<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>库存日报表</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>库存日报表</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-input-block" style="width:180px;margin-left: 10px;display:none">
							<select class="layui-input" name="type" id="type" lay-verify="required">
								<option value="week">周报表</option>
								<option value="month">月报表</option>
							</select>
						</div>
					</td>
					<td>
						<input class="layui-input" name="date" id="date" placeholder="请输入日期" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px"> 
					</td>
					<td>
						<div class="layui-input-block" style="width:180px;margin-left: 10px;display:none">
							<select class="layui-input" name="customer_id" id="customer_id" lay-verify="required">
								<option value="">请选择供应商</option>
								<c:forEach items="${customers}" var="customer">
									<option value="${customer.id}">${customer.customer_name}</option>
								</c:forEach>  
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<!-- 弹框1 -->
	<div id="div1" hidden="hidden">
		<div style="width: 100%;">
			<table id="table1" lay-filter="table1">
			</table>
		</div>
	</div>
	<!-- 弹框2 -->
	<div id="div2" hidden="hidden">
		<div style="width: 100%;">
			<table id="table2" lay-filter="table2">
			</table>
		</div>
	</div>
	<!-- 弹框3 -->
	<div id="div3" hidden="hidden">
		<div style="width: 100%;">
			<table id="table3" lay-filter="table3">
			</table>
		</div>
	</div>
	<!-- 弹框1 -->
	<div id="div4" hidden="hidden">
		<div style="width: 100%;">
			<table id="table4" lay-filter="table4">
			</table>
		</div>
	</div>
	<script src="${pageContext.request.contextPath }/js/td/layuiRowspan.js" charset="utf-8"></script>
	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//执行一个laydate实例
			laydate.render({
				elem: '#date', //指定元素
				type: 'date',
				value: new Date(),
				format: 'yyyy-MM-DD', //自动生成的时间格式   
				btns: ['now', 'confirm']
			});
			
			var $ = layui.jquery, active = {
				reload:function () {
					var type = $.trim($("#type").val());
	                var date = $.trim($("#date").val());
	                var customer_id = $.trim($("#customer_id").val());
					table.reload('tableList',{
						method:'get'
						,where:{
							'type':type
	                        ,'date':date+'-01'
	                        ,'customer_id':customer_id
						}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			$("#date").val(curentMonth())
			$("#type").val('month')
			$('#type').attr("disabled",true)
			$("#customer_id").val('${customer_id}')
			$('#customer_id').attr("disabled",true)
			var type = $.trim($("#type").val());
            var date = $.trim($("#date").val());
            var customer_id = $.trim($("#customer_id").val());
            table.render({
				elem: '#tableList'
				,where:{
					'type':type
                       ,'date':date+'-01'
                       ,'customer_id':customer_id
				}
                ,url:'${pageContext.request.contextPath }/json/list1.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                  /* } , {
		                    field: 'customer_name',
		                    title: '客户名称',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_name;
	                            }
                                return value;
	                        }, */
		                /* } , {
		                    field: 'customer_num',
		                    title: '客户编号',
		                    align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_num;
	                            }
                                return value;
	                        },  
		                } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        },*/
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 100,
		                /* }, {
		                    field: 'unit',
		                    title: '单位',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.unit;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'upper_value',
		                    title: 'max',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.upper_value;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'lower_value',
		                    title: 'min',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.lower_value;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'num1',
		                    title: '昨日结存',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'num2',
		                    title: '当日入库',
		                    align:'center',
		                    width: 100,
		                    event: 'tankuang1',
		                    templet: function (row){
		                    	var value = '<div style="color: red;">'+row.num2+'</div>';
                                return value;
	                        },
		                }, {
		                    field: 'num3',
		                    title: '当日出库',
		                    align:'center',
		                    width: 100,
		                    event: 'tankuang2',
		                    templet: function (row){
		                    	var value = '<div style="color: red;">'+row.num3+'</div>';
                                return value;
	                        },
		                }, {
		                    field: 'num4',
		                    title: '返厂',
		                    align:'center',
		                    width: 150,
		                    event: 'tankuang3',
		                    templet: function (row){
		                    	var value = '<div style="color: red;">'+row.num4+'</div>';
                                return value;
	                        },
		                }, {
		                    field: 'num5',
		                    title: '退货',
		                    align:'center',
		                    width: 100,
		                    event: 'tankuang4',
		                    templet: function (row){
		                    	var value = '<div style="color: red;">'+row.num5+'</div>';
                                return value;
	                        },
		                }, {
		                    field: 'num6',
		                    title: '不良总数',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'num7',
		                    title: '现存量',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'num8',
		                    title: '良品数量',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'num9',
		                    title: '盘点数量',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'num10',
		                    title: '差异',
		                    align:'center',
		                    width: 100,
		                /* }, {
		                    field: 'packing_quantity',
		                    title: '单箱数量',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.packing_quantity;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'num11',
		                    title: '箱数',
		                    align:'center',
		                    width: 100, 
		                }, {
		                    field: 'num11',
		                    title: '周转箱尺寸（mm)',
		                    align:'center',
		                    width: 100,*/
		                }, {
		                    field: 'kw',
		                    title: '库位',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'upper_value',
		                    title: '上限',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.upper_value;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'lower_value',
		                    title: '下限',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.lower_value;
	                            }
                                return value;
	                        },
		                }
		                ]
		               ]
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	                layuiRowspan(['customer_name'], 1);//支持数组
	            }
			});
            table.render({
				elem: '#table1'
                ,url:'${pageContext.request.contextPath }/json/list11.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'dannum',
		                    title: '单号',
		                    align:'center',
		                    width: 200,
		                /* } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 200,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 200,
		                }, {
		                    field: 'num1',
		                    title: '入库',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'man',
		                    title: '入库人员',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'date',
		                    title: '入库时间',
		                    align:'center',
		                    width: 100,
		                }
		                ]
		               ]
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
            table.render({
				elem: '#table2'
                ,url:'${pageContext.request.contextPath }/json/list12.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'dannum',
		                    title: '单号',
		                    align:'center',
		                    width: 200,
		                /* } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 200,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 200,
		                }, {
		                    field: 'num1',
		                    title: '出库',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'man',
		                    title: '出库人员',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'date',
		                    title: '出库时间',
		                    align:'center',
		                    width: 100,
		                }
		                ]
		               ]
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
            table.render({
				elem: '#table3'
                ,url:'${pageContext.request.contextPath }/json/list13.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                    align:'center',
		                    width: 100,
		                } , {
		                    field: 'customer_name',
		                    title: '供应商',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_name;
	                            }
                                return value;
	                        },
		                }, {
		                    field: 'dannum',
		                    title: '单号',
		                    align:'center',
		                    width: 150,
		                /* } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'num1',
		                    title: '返厂',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'reason',
		                    title: '返厂原因',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'man',
		                    title: '返厂人员',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'date',
		                    title: '返厂时间',
		                    align:'center',
		                    width: 100,
		                }
		                ]
		               ]
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
            table.render({
				elem: '#table4'
                ,url:'${pageContext.request.contextPath }/json/list14.json'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,request: {   //如果无需自定义请求参数，可不加该参数
	                pageName: 'page' //页码的参数名称，默认：page
	                ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	            }
	            ,response: {   //如果无需自定义数据响应名称，可不加该参数
	                countName: 'total' //规定数据总数的字段名称，默认：count
	                ,dataName: 'rows' //规定数据列表的字段名称，默认：data
	            }
				,cols: [
		                [{
		                /*     type: 'checkbox',
		                    fixed: 'left'
		                }, { */
		                	field:'uid',
		                	title:'序号', 
		                	sort: true,
		                	width: 60,
		                	type:'numbers',
		                	align:'center'
		                }, {
		                    field: 'uid',
		                    title: 'id',
		                    hide:true //是否初始隐藏列，默认：false。layui 2.4.0 新增
		                }, {
		                    field: '',
		                    title: '项目',
		                    align:'center',
		                    width: 100,
		                /* } , {
		                    field: 'materiel_num',
		                    title: 'SAP/QAD',
		                    align:'center',
		                    width: 200,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
                                return value;
	                        }, */
		                }, {
		                    field: 'materiel_name',
		                    title: '中文名',
		                    align:'center',
		                    width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
                                return value;
	                        },
		                } , {
		                    field: 'brevity_num',
		                    title: '零件号',
		                    align:'center',
		                    width: 200,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
                                return value;
	                        },
		                } , {
		                    field: '',
		                    title: '英文名',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'num1',
		                    title: '退货',
		                    align:'center',
		                    width: 100,
		                }, {
		                    field: 'man',
		                    title: '退货原因',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'remarks',
		                    title: '备注',
		                    align:'center',
		                    width: 150,
		                }, {
		                    field: 'dannum',
		                    title: '客户退货单号',
		                    align:'center',
		                    width: 150,
		                } , {
		                    field: 'customer_name',
		                    title: '客户',
		                    align:'center',
		                    width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.customer){
		                    		value = row.customer.customer_name;
	                            }
                                return value;
	                        },
		                }
		                ]
		               ]
				,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                    //'font-size': 10,
	                })
	            }
			});
          //监听单元格事件
            table.on('tool(tableList)', function(obj){
              var data = obj.data;
              for (var i = 1; i <= 4; i++) {
	              if(obj.event === ('tankuang'+i)){
	            	  tankuang(data.materiel.id,i)
	            	  break;
	              }
			  }
            });
			//获取当前日期
		   	function tankuang(id,type) {
				var divID  = "div"+type
				var title = "库存日报表-入库"
				if(type == 1){
					title = "库存日报表-入库"
				}else if(type == 2){
					title = "库存日报表-出库"
				}else if(type == 3){
					title = "库存日报表-返厂"
				}else if(type == 4){
					title = "库存日报表-退货"
				}
		   		layer.open({
	                type: 1 					//Page层类型
	                ,area: ['1380px', '600px'] 			//宽  高
	                ,title: title
	                ,shade: 0.6 				//遮罩透明度
	                ,maxmin: true 				//允许全屏最小化
	                ,anim: 1 					//0-6的动画形式，-1不开启
	                ,content: $('#'+divID)
	                ,end: function () {
	                    var formDiv = document.getElementById(divID);
	                    formDiv.style.display = '';
	                }
	                ,success: function(){
	                    form.render();
	                }
	            });
		   	}
		});
		//获取当前日期
	   	function curentMonth() {
	   		var now=new Date();
	   		var year = now.getFullYear();       //年
	   	    var month = now.getMonth() + 1;     //月
	   	    var day = now.getDate();            //日
	   	    var time=year+"-"+add0(month);
	   		return time;
	   	}
	   	function add0(m){return m<10?'0'+m:m }
		toastrStyle()
	</script>
</body>
</html>