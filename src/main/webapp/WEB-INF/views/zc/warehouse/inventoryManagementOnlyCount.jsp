<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.sdry.model.zc.ZcSysUserEntity" %>
<% 
	ZcSysUserEntity zcSysUserEntity =  (ZcSysUserEntity)request.getSession().getAttribute("user");
	//String roleId = zcSysUserEntity.getRoleId(); 
	//String roleIds[] = roleId.split(",");
	String roleName = zcSysUserEntity.getRoleName(); 
	String roleNames[] = roleName.split(",");
	boolean isAdmin= false;
	/* for(int i =0;i<roleIds.length;i++){
		if("2".equals(roleIds[i])){
			isAdmin = true;
		}
	} */
	for(int i =0;i<roleNames.length;i++){
		if("管理员".equals(roleNames[i])|| "超级管理员".equals(roleNames[i])){
			isAdmin = true;
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>精简库存管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		.layui-table-view .layui-table-body tr.table_tr_click {
	      background-color: pink;
	    }
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>精简库存管理</cite>
			</a>
		</span>
    </div>
	<div class="x-body">
		<div class="layui-row">
			<div class="layui-form">
				<div class="layui-input-inline">
					<div class="layui-input-inline" style="width: 285px">
						<select style="width: 285px" class="" name="keyword" id="keyword" lay-filter="keyword">
							<option value="">--请选择供应商--</option>
							<c:forEach items="${allCustomerList }" var="customer">
								<option value="${customer.id}">${customer.customer_name }</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 150px">
						<input class="layui-input" id="keyword2" placeholder="请输入产品码查询" style="width: 150px">
					</div>
				</div>
				<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
			</div>
		</div>
		
		<div id = "fDiv">
			<table class="layui-hide" id="inventoryInfoOnlyCountList" lay-filter="inventoryInfoOnlyCountList"></table>
		</div>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword = $("#keyword").val();
					var keyword2 = $("#keyword2").val();
					keyword2 = "'"+keyword2+"'";
					var strWhere = "";
					if(keyword!="" && keyword2 != "''"){
						strWhere = 'm.customer_id = '+keyword+' and m.materiel_num = '+keyword2+' and ';
					}else if(keyword!="" && keyword2 == "''"){
						strWhere = 'm.customer_id = '+keyword+' and ';
					}else if(keyword=="" && keyword2 != "''"){
						strWhere = 'm.materiel_num = '+keyword2+' and ';
					}else{
						strWhere = ' ';
					}
					table.reload('contenttable',{
						method:'get',
						where:{"strWhere":strWhere}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#inventoryInfoOnlyCountList'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectInventoryInfoOnlyCountList.do'
				,title: '库存列表'
				,id :'contenttable'
				,toolbar: '#toolbarDemo'
				,limits:[10,20,30]
				,cols: [[
					{type: 'checkbox', fixed: 'left'}
					,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
					,{field:'customer', title:'供应商',templet:function(row){
						if(row.materiel.customer!=null){
							return row.materiel.customer.customer_name;
						}else{
							return "";
						}
					}}
					,{field:'materiel_name', title:'物料名称',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.materiel_name;
						}else{
							return "";
						}
					}}
					,{field:'materiel_num', title:'产品码',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.materiel_num;
						}else{
							return "";
						}
					}}
					//,{field:'mBatch', title:'批次', align: 'center'}
					,{field:'mNum', title:'数量'}
					,{field:'countAll', title:'总库存'}
					,{field:'unit', title:'单位',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.unit;
						}else{
							return "";
						}
					}}
					,{field:'enterDate', title:'入库日期',templet:function(row){
						return row.enterDate.substr(0,16)
					}}
					,{field:'userName', title:'入库人',templet:function(row){
						if(row.zcSysUser!=null){
							return row.zcSysUser.userName;
						}else{
							return "";
						}
					}}
				]]
				,page: true
				,done:function(res, curr, count){
	                merge(res);
					$('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                })
				}
			});
			//监听复选框事件
			table.on('checkbox(inventoryInfoList)',function(obj){
				if(obj.checked == true && obj.type == 'all'){ 
					//点击全选 
					$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
				}else if(obj.checked == false && obj.type == 'all'){ 
					//点击全不选 
					$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
				}else if(obj.checked == true && obj.type == 'one'){ 
					//点击单行
					if(obj.checked == true){ 
						obj.tr.addClass('layui-table-click'); 
					}else{ 
						obj.tr.removeClass('layui-table-click'); 
					} 
				}else if(obj.checked == false && obj.type == 'one'){ 
					//点击全选之后点击单行 
					if(obj.tr.hasClass('layui-table-click')){
						obj.tr.removeClass('layui-table-click'); 
					} 
				}
			})
		})
		//合并单元格
		function merge(res) {
	        var data = res.data;
	        var mergeIndex = 0;//定位需要添加合并属性的行数
	        var mergeIndexAll = 0;//定位需要添加合并属性的行数
	        var mergeIndexSize = 0;//定位需要添加合并属性的行数
	        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
	        var columsName = ['customer_id'];//需要合并的列名称
	        var columsNameAll = ['materiel_name','materiel_num','countAll','unit'];//需要合并的列名称
	        var columsNameSize = ['materiel_size','materiel_properties'];//需要合并的列名称
	        var columsIndex = [2];//需要合并的列索引值
	        var columsIndexAll = [3,4,7,8];//需要合并的列索引值
	        var columsIndexSize = [5,6];//需要合并的列索引值
	    	var allMark = 1;
	    	var sizeMark = 1;
	        for (var k = 0; k < columsNameAll.length; k++) { //这里循环所有要合并的列
	            var trArr = $("#fDiv .layui-table-body>.layui-table").find("tr");//所有行
	                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndexAll[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndexAll).find("td").eq(columsIndexAll[k]);//获取相同列的第一列
	                    if(data[i].materiel.materiel_num === data[i-1].materiel.materiel_num){
	                    	allMark+=1;
	                    	tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", allMark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else{
	                    	mergeIndexAll = i;
	                    	allMark=1;
	                    }
	                }
	            mergeIndexAll = 0;
	            allMark = 1;
	        } 
	        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
	            var trArr = $("#fDiv .layui-table-body>.layui-table").find("tr");//所有行
	                for (var i =   1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列
	                    if (data[i].materiel[columsName[k]] === data[i-1].materiel[columsName[k]]) { //后一行的值与前一行的值做比较，相同就需要合并
	                        mark += 1;
	                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", mark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else {
	                        mergeIndex = i;
	                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
	                    }
	                }
	            mergeIndex = 0;
	            mark = 1;
	        } 
	    }
		function mergeDetails(res) {
	        var data = res.data;
	        var mergeIndex = 0;//定位需要添加合并属性的行数
	        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
	        var columsName = ['region_name','location_code'];//需要合并的列名称
	        var columsIndex = [2,4];//需要合并的列索引值
	        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
	            var trArr = $("#details .layui-table-body>.layui-table").find("tr");//所有行
	            //console.log(trArr)
	                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列
	                    if (data[i].zcTrayAndLocation[columsName[k]] === data[i-1].zcTrayAndLocation[columsName[k]]) { //后一行的值与前一行的值做比较，相同就需要合并
	                        mark += 1;
	                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", mark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else {
	                        mergeIndex = i;
	                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
	                    }
	                }
	            mergeIndex = 0;
	            mark = 1;
	        }
	    }
		$(document).on('click', '#fDiv .layui-table-view .layui-table-body tr', function (event) {
	    	var trElem = $(this);
	      	var tableView = trElem.closest('.layui-table-view');
	      	tableView.find('.layui-table-body tr.table_tr_click').removeClass('table_tr_click');
	      	tableView.find('.layui-table-body tr[data-index="' + trElem.data('index') + '"]').addClass('table_tr_click');
	    });
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>
</html>
