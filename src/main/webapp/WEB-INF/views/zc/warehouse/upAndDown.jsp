<<<<<<< HEAD
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>上架/下架</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		#up{
			width: 300px;
			height: 150px;
			background-color: green;
			margin-bottom: 20px;
			text-align: center;
		}
		#down{
			width: 300px;
			height: 150px;
			background-color: red;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
		#ob{
			width: 300px;
			height:320px;
			margin: 200px auto;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div id="formDiv" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" placeholder="可扫描多个条码" lay-verify="tray_code" id="tray_code" name="tray_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" id="location_code" lay-verify="location_code" name="location_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" id="subform">绑定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="formDiv2" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform2">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" id="location_code2" lay-verify="location_code" name="location_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" placeholder="可扫描多个条码" lay-verify="tray_code" id="tray_code" name="tray_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" id="subform2">解绑</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>上架/下架</cite>
			</a>
		</span>
    </div>
	<div style="width:100%;height:100%">
		<div id="ob">
			<div id="up">
				<span>上架</span>
			</div>
			<div id="down">
				<span>下架</span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			
			$("#up").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['500px', ''] 	//宽  高
	                ,title: '上架'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv')
	                ,success:function(){
	                	document.getElementById("addform").reset();
	                	form.render();
	                }
    			});
			})
			$("#down").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['500px', ''] 	//宽  高
	                ,title: '下架'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv2')
	                ,success:function(){
	                	document.getElementById("addform2").reset();
	                	form.render();
	                }
    			});
			})
			/**
		    * 表单校验
		    */
		   form.verify({
			   	//value：表单的值、item：表单的DOM对象
			   	location_code: function(value, item){
			   		if(value == ''){
			   			return '库位不能为空';
			   		}
			   	},
			   	tray_code: function(value, item){
			   		if(value == ''){
			   			return '托盘条码不能为空';
			   		}
			   	}
		   });
			//绑定托盘库位
	        form.on('submit(addform)', function (data) {
	        	$("#subform").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/upAndDown/bindingTrayAndLocation.do',
	       			data : $('#addform').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('上架成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else if(res == -2){
	    		  				toastr.error('上架失败，请勿上架空托盘');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -3){
	    		  				toastr.error('上架失败，入库类型为普通入库的不能上架');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else{
	    		  				toastr.error('上架失败！');
	    		  				$("#subform").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
			//解绑托盘库位
	        form.on('submit(addform2)', function (data) {
	        	$("#subform2").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/upAndDown/unbindTrayAndLocation.do',
	       			data : $('#addform2').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('下架成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else if(res==-2){
	    		  				toastr.warning('库位或托盘码错误！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else{
	    		  				toastr.error('下架失败！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>

=======
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>上架/下架</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		#up{
			width: 300px;
			height: 150px;
			background-color: green;
			margin-bottom: 20px;
			text-align: center;
		}
		#down{
			width: 300px;
			height: 150px;
			background-color: red;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
		#ob{
			width: 300px;
			height:320px;
			margin: 200px auto;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div id="formDiv" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" placeholder="可扫描多个条码" lay-verify="tray_code" id="tray_code" name="tray_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" id="location_code" lay-verify="location_code" name="location_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" id="subform">绑定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="formDiv2" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform2">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" id="location_code2" lay-verify="location_code" name="location_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" placeholder="可扫描多个条码" lay-verify="tray_code" id="tray_code" name="tray_code" type="text" />
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">解绑原因</label>
						<div class="layui-input-inline" style="width: 285px">
							<textarea class="layui-textarea" lay-verify="unbindle_reason" id="unbindle_reason" name="unbindle_reason" ></textarea>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" id="subform2">解绑</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>上架/下架</cite>
			</a>
		</span>
    </div>
	<div style="width:100%;height:100%">
		<div id="ob">
			<div id="up">
				<span>上架</span>
			</div>
			<div id="down">
				<span>下架</span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			
			$("#up").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['500px', ''] 	//宽  高
	                ,title: '上架'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv')
	                ,success:function(){
	                	document.getElementById("addform").reset();
	                	form.render();
	                }
    			});
			})
			$("#down").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['500px', ''] 	//宽  高
	                ,title: '下架'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv2')
	                ,success:function(){
	                	document.getElementById("addform2").reset();
	                	form.render();
	                }
    			});
			})
			/**
		    * 表单校验
		    */
		   form.verify({
			   	//value：表单的值、item：表单的DOM对象
			   	location_code: function(value, item){
			   		if(value == ''){
			   			return '库位不能为空';
			   		}
			   	},
			   	tray_code: function(value, item){
			   		if(value == ''){
			   			return '托盘条码不能为空';
			   		}
			   	}
		   });
			//绑定托盘库位
	        form.on('submit(addform)', function (data) {
	        	$("#subform").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/upAndDown/bindingTrayAndLocation.do',
	       			data : $('#addform').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('上架成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else if(res == -2){
	    		  				toastr.error('上架失败，请勿上架空托盘');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -3){
	    		  				toastr.error('上架失败，入库类型为普通入库的不能上架');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -4){
	    		  				toastr.error('上架失败，同一库位不能上架装有不同物料的托盘');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -5){
	    		  				toastr.error('上架失败，托盘已上架');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -6){
	    		  				toastr.error('上架失败，库位码不存在');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -7){
	    		  				toastr.error('上架失败，库位容量不足');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else{
	    		  				toastr.error('上架失败！');
	    		  				$("#subform").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
			//解绑托盘库位
	        form.on('submit(addform2)', function (data) {
	        	$("#subform2").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/upAndDown/unbindTrayAndLocation.do',
	       			data : $('#addform2').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('下架成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else if(res==-1){
	    		  				toastr.warning('下架的托盘不在同一库位！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}else if(res==-2){
	    		  				toastr.warning('库位为空！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}else if(res==-3){
	    		  				toastr.warning('托盘与库位未绑定！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}else if(res==-4){
	    		  				toastr.warning('请确认要下架的托盘在库位存在！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}else{
	    		  				toastr.error('下架失败！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>

>>>>>>> branch 'master' of https://gitee.com/Lebron_Irving/HAJY_HYWL.git
</html>
