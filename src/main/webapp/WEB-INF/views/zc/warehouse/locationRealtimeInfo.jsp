<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import= "java.util.* "%>
<%@ page import= "com.sdry.model.lz.WarehouseRegionLocation"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>库位实时信息架</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		.locaDiv{
			width:80%;
			margin:auto;
		}
		.locaDivChild {
			width: 60px;
			height: 60px;
			margin: 10px 5px 0 5px;
			display: inline-block;
			color: #FFF;
			font-size:10px;
			text-align: center;
		}
		.locaDivChild p{
			width: 60px;
			margin-top:20px; 
			word-wrap:break-word;
			word-break:break-all;
    		overflow: hidden;
		}
		.my-skin{
			background-color: #FFF;
		}
		.box{
			margin:0 20px 10px 20px;
			font-size:14px;
			line-height:30px;
			vertical-align: middle;
		}
		.header{
			width:80%;
			margin:auto;
			margin-top: 20px;
		}
		.green{
			background-color:#32CD32;
		}
		.yellow{
			background-color:#FFA500;
		}
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div class="header">
		<div class="layui-row">
			<div class="layui-form">
				<div class="layui-input-inline" id="materielDiv">
					<div class="layui-input-inline">
						<div class="layui-input-inline" style="width: 285px">
							<input class="layui-input" id="materiel_num" placeholder="请输入物料产品码"/>
						</div>
					</div>
					<div class="layui-input-inline layui-hide">
						<div class="layui-input-inline" style="width: 285px">
							<select style="width: 150px" class="" name="" id="sizeAndPre" lay-filter="sizeAndPre">
							</select>
						</div>
					</div>
				</div>
				<button class="layui-btn layui-btn-normal" onclick="search()"><i class="layui-icon layui-icon-search"></i>检索</button>
			</div>
		</div>
	</div>
	<div>
		<div style="position: fixed;right:0;top:200px">
			<div class="green locaDivChild">
				<p>空库位</p>
			</div><br>
			<div class="yellow locaDivChild">
				<p>有物料</p>
			</div>
		</div>
	</div>
	<div class="locaDiv">
		<%
	 		String region_name = "";
			int index = 0;
			int index1 = 0;
	 	%>
	 	<div class="layui-tab">
	 		<ul class="layui-tab-title">
	 			<c:forEach items="${warehouseRegionLocation }" var="wrl">
	 				<%List<WarehouseRegionLocation> list = (List)request.getAttribute("warehouseRegionLocation");%>
	 				<% if(index == 0){ %>
							<li class="layui-this">${wrl.region_name }</li>
					<% }else if(!list.get(index-1).getRegion_name().equals(list.get(index).getRegion_name())){ %>
							<li>${wrl.region_name }</li>
					<%} %>
					<%index +=1; %>
	 			</c:forEach>
	 		</ul>
	 		<div class="layui-tab-content">
	 			<c:forEach items="${warehouseRegionLocation }" var="wrl">
	 				<%List<WarehouseRegionLocation> list1 = (List)request.getAttribute("warehouseRegionLocation");%>
	 				<% if(index1 == 0){ %>
							<div class="layui-tab-item layui-show">
								<c:choose>
									<c:when test="${wrl.materiel_code == ''}">
										<div class="locaDivChild green" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:when>
									<c:otherwise>
										<div class="locaDivChild yellow" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:otherwise>
								</c:choose>
					<% }else if(!list1.get(index1-1).getRegion_name().equals(list1.get(index1).getRegion_name())){ %>
							</div>
							<div class="layui-tab-item">
								<c:choose>
									<c:when test="${wrl.materiel_code == ''}">
										<div class="locaDivChild green" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:when>
									<c:otherwise>
										<div class="locaDivChild yellow" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:otherwise>
								</c:choose>
					<% }else if(list1.get(index1-1).getRegion_name().equals(list1.get(index1).getRegion_name())){ %>
								<c:choose>
									<c:when test="${wrl.materiel_code == ''}">
										<div class="locaDivChild green" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:when>
									<c:otherwise>
										<div class="locaDivChild yellow" id="${wrl.id }" onclick="check(this)">
											<p>${wrl.location_name }</p>
										</div>
									</c:otherwise>
								</c:choose>
					<% }else{ %>
						</div>
					<%} %>
					<div id="check${wrl.id }" hidden>
						<div class="box">
						    <p>库位名称：${wrl.location_name }</p>
						    <p>库位编码：${wrl.location_num }</p>
						    <p>库位总库存：${wrl.total_capacity }</p>
							<p>所属库区：${wrl.region_name }</p>
							<p>物料：</p>
							<table class="layui-table">
								<thead>
									<tr>
										<th>物料名称</th>
										<th>物料条码</th>
										<th>物料数量</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${wrl.zcMaterielNameAndCode }" var="wrl2">
										<tr>
											<td>${wrl2.materiel_name }</td>
											<td>${wrl2.materiel_code }</td>
											<td>${wrl2.num }</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
					<%index1 +=1; %>
	 			</c:forEach>
			</div>
	 	</div>
	</div>
	<script type="text/javascript">
		function check(elem){
			layui.use(['table','layer','upload','form','laydate'], function(){
				var table = layui.table;
				var layer = layui.layer;
				var form = layui.form;
				var laydate = layui.laydate;
				//页面层
				/* layer.open({
				  type: 1,
				  skin: 'my-skin', 		//加上边框
				  title: '库位详情',
				  area: ['300px', ''], 	//宽高
				  content: $("#check"+elem.id+"")
				}); */
				layer.open({
					  type: 1,
					  skin: 'layui-layer-demo', //样式类名
					  closeBtn: 0, 				//不显示关闭按钮
					  anim: 2,
					  area: ['800px', ''], 		//宽高
					  shadeClose: true, 		//开启遮罩关闭
					  content: $("#check"+elem.id+"")
					});
			})
		}
		function search(){
			var region_keyword = $("#region_keyword").val();
			var materiel_num = $("#materiel_num").val();
			var sizeAndPre = $("#sizeAndPre").val();
			var materiel_size = sizeAndPre.split("/")[0];
			var materiel_properties = sizeAndPre.split("/")[1];
			$.ajax({
				type:'post',
				url:'${pageContext.request.contextPath }/bindAndUnbind/selectTopLocation.do',
				data:{"region_id":region_keyword,"materiel_num":materiel_num,"materiel_size":materiel_size,"materiel_properties":materiel_properties},
				dataType:'json',
				success:function(res){
					 $('.layui-tab-title li').remove().pather; 
					 $('.layui-tab-content div').remove().pather; 
					 var data = res.data;
					 for(var i =0;i<data.length;i++){
						if(i==0){
							$('.layui-tab-title').append(
								'<li class="layui-this">'+data[i].region_name+'</li>'
							)
						}else if(i>0 && data[i].region_name != data[i-1].region_name){
							$('.layui-tab-title').append(
								'<li>'+data[i].region_name+'</li>'
							)
						}
					 }
					 var idr ="";
					 for(var i =0;i<data.length;i++){
						if(i==0){
							if(data[i].materiel_code == ''){
								$('.layui-tab-content').append(
										'<div class="layui-tab-item layui-show" id="x'+data[0].id+'">'+
											'<div class="locaDivChild green" id="'+data[i].id+'" onclick="check(this)">'+
												'<p>'+data[i].location_name+'</p>'+
											'</div>'	
								)
							}else{
								$('.layui-tab-content').append(
										'<div class="layui-tab-item layui-show" id="x'+data[0].id+'">'+
											'<div class="locaDivChild yellow" id="'+data[i].id+'" onclick="check(this)">'+
												'<p>'+data[i].location_name+'</p>'+
											'</div>'	
								)
							}
							idr = 'x'+data[0].id+'';
						}else if(i>0 && data[i].region_name != data[i-1].region_name){
								if(data[i].materiel_code == ''){
									$('.layui-tab-content').append(
											'</div>'+
											'<div class="layui-tab-item" id="x'+data[i].id+'">'+
												'<div class="locaDivChild green" id="'+data[i].id+'" onclick="check(this)">'+
													'<p>'+data[i].location_name+'</p>'+
												'</div>'	
									)
								}else{
									$('.layui-tab-content').append(
											'</div>'+
											'<div class="layui-tab-item" id="x'+data[i].id+'">'+
												'<div class="locaDivChild yellow" id="'+data[i].id+'" onclick="check(this)">'+
													'<p>'+data[i].location_name+'</p>'+
												'</div>'	
									)
								}
								idr = 'x'+data[i].id+'';
						}else if(i>0 && data[i].region_name == data[i-1].region_name){
								if(data[i].materiel_code == ''){
									$('#'+idr+'').append(
												'<div class="locaDivChild green" id="'+data[i].id+'" onclick="check(this)">'+
													'<p>'+data[i].location_name+'</p>'+
												'</div>'	
									)
								}else{
									$('#'+idr+'').append(
												'<div class="locaDivChild yellow" id="'+data[i].id+'" onclick="check(this)">'+
													'<p>'+data[i].location_name+'</p>'+
												'</div>'	
									)
								}
						}
						for(var j =0;j<data[i].zcMaterielNameAndCode.length;j++){
							var materiel_name = "";
							var materiel_code = "";
							var num = "";
							if(data[i].zcMaterielNameAndCode[j]!=null){
								materiel_name = data[i].zcMaterielNameAndCode[j].materiel_name;
								materiel_code = data[i].zcMaterielNameAndCode[j].materiel_code;
								num = data[i].zcMaterielNameAndCode[j].num;
							}
							$('.locaDiv').append(
									 '<div id="check'+data[i].id+'" hidden>'+
										'<div class="box">'+
										    '<p>库位名称：'+data[i].location_name+'</p>'+
										    '<p>库位编码：'+data[i].location_num+'</p>'+
											'<p>所属库区：'+data[i].region_name+'</p>'+
											'<p>物料：</p>'+
											'<table class="layui-table">'+
											'<thead>'+
												'<tr>'+
													'<th>物料名称</th>'+
													'<th>物料条码</th>'+
													'<th>物料数量</th>'+
												'</tr>'+
											'</thead>'+
											'<tbody>'+
													'<tr>'+
														'<td>'+materiel_name+'</td>'+
														'<td>'+materiel_code+'</td>'+
														'<td>'+num+'</td>'+
													'</tr>'+
											'</tbody>'+
										'</table>'+
										'</div>'+
									'</div>'		 
								) 
						}
					 }
					 $('.layui-tab-content').append(
								'</div>'	
					)
				}
			})
		}
		$("#materiel_num").change(function(){
			var materiel_num = $("#materiel_num").val();
			materiel_num = "'"+materiel_num+"'";
			var empty = "''";
			var strWhere = '';
			strWhere = '(m.materiel_num = '+materiel_num+' or m.brevity_num = '+materiel_num+' or m.devanning_code = '+materiel_num+') and '
			//通过产品码查规格型号
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectMaterielSizeByCode.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					//$("#sizeAndPre").find("option").not(":first").remove();
					$("#sizeAndPre").find("option").remove();
					for(var i = 0;i < res.length;i++){
						$("#sizeAndPre").append('<option value="'+res[i].materiel_size+'/'+res[i].materiel_properties+'">'+res[i].materiel_size+'/'+res[i].materiel_properties+'</option>');
						layui.use(['form'], function(){
							var form = layui.form;
							form.render();
						})
					}
				}
			})
		})
		layui.use('element', function(){
			var element = layui.element;
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>
</html>
