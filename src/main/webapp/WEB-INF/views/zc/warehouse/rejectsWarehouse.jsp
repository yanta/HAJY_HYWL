<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>不良品库</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		.layui-table-view .layui-table-body tr.table_tr_click {
	      background-color: pink;
	    }
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>不良品库</cite>
			</a>
		</span>
    </div>
	<div class="x-body">
		<div class="layui-row">
			<div class="layui-form">
				<!-- <input type="text" style="width:200px" name="mcode" id="mcode"  placeholder="请输入物料产品码或简码" autocomplete="off" class="layui-input"> -->
				<div class="layui-input-inline">
					<div class="layui-input-inline" style="width: 285px">
						<select style="width: 285px" class="" name="keyword" id="keyword" lay-filter="keyword">
							<option value="">--请选择供应商--</option>
							<c:forEach items="${allCustomerList }" var="customer">
								<option value="${customer.id}">${customer.customer_name }</option>
							</c:forEach>
						</select>
					</div>
					<div class="layui-input-inline" style="width: 150px">
						<input class="layui-input" id="keyword2" placeholder="请输入产品码查询" style="width: 150px">
					</div>
				</div>
				<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
			</div>
		</div>
		<script type="text/html" id="toolbarDemo">
			<div class="layui-btn-container">
				<button class="layui-btn layui-btn-normal" lay-event="rejects"><i class="layui-icon"></i>不良品入库</button>
			</div>
		</script>
		<div id="fDiv">
			<table class="layui-hide" id="rejectsWarehouseList" lay-filter="rejectsWarehouseList"></table>
		</div>
		<div id = "details">
			<table class="layui-hide" id="inventoryInfoListDetails" lay-filter="inventoryInfoListDetails"></table>
		</div>
		<div id="formDiv" hidden>
			<form class="layui-form layui-card-body" id="addform" onsubmit="return false">
				<div id="codeDiv">
					<div class="layui-form-item">
						<label class="layui-form-label">产品码</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="mcode1">
						</div>
						<label class="layui-form-label">规格/型号</label>
						<div class="layui-input-inline" style="width: 150px">
							<select style="width: 150px" class="" name="" id="sizeAndPre" lay-filter="sizeAndPre">
								<option value="">--请选择规格/型号--</option>
							</select>
						</div>
						<label class="layui-form-label">批次</label>
						<div class="layui-input-inline" style="width: 150px">
							<select style="width: 150px" class="" name="" id="mBatch1" lay-filter="mBatch1">
								<option value="">--请选择批次--</option>
							</select>
						</div>
						<div class="layui-btn layui-btn-normal" onclick="selectInventoryInfo()">检索</div>
					</div>
				</div>
				<div id="tableDiv" style="background-color: #CDDDF1;padding: 10px 0 0.1px 0">
					<input id="mid" name="mid" class="layui-hide"/>
					<div class="layui-form-item">
						<label class="layui-form-label">供应商：</label>
						<div class="layui-input-inline" style="width: 690px">
							<input class="layui-input" id="customer" name="" readonly="readonly"/>
							<input class="layui-hide" id="customer_id"/>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">物料名称：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="materiel_name" name="" readonly="readonly"/>
						</div>
						<label class="layui-form-label">产品码：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="materiel_num" name="" readonly="readonly"/>
						</div>
						<label class="layui-form-label">简码：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="brevity_num" name="" readonly="readonly"/>
						</div>
					</div>
					<div class="layui-form-item">
						<label class="layui-form-label">包装数量：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="packing_quantity" name="" readonly="readonly"/>
						</div>
						<label class="layui-form-label">单位：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="unit" name="" readonly="readonly"/>
						</div>
						<label class="layui-form-label">批次：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-hide" id="materiel_size" name="materiel_size" readonly="readonly"/>
							<input class="layui-hide" id="materiel_properties" name="materiel_properties" readonly="readonly"/>
							<input class="layui-input" id="mBatch" name="mBatch" readonly="readonly"/>
						</div>
					</div>	
					<div class="layui-form-item">
						<label class="layui-form-label">库存数量：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="mNum" name="mNum" readonly="readonly"/>
						</div>
						<label class="layui-form-label">入库日期：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="enterDate" name="" readonly="readonly"/>
						</div>
						<label class="layui-form-label">入库人：</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="enterPerson" name="" readonly="readonly"/>
						</div>
					</div>
				</div>
				<div class="layui-form-item" id="targetDiv" style="margin-top: 10px;">
					<label class="layui-form-label">目标仓库：</label>
					<div class="layui-input-inline" style="width: 150px">
						<select lay-verify="warehouseId" id="warehouseId" name="warehouseId" lay-filter="warehouseId">
							<option value="">--请选择仓库--</option>
							<c:forEach items="${allWarehouseList }" var="ware">
								<option value="${ware.id }">${ware.warehouse_name }</option>
							</c:forEach>
						</select>
					</div>
					<label class="layui-form-label">不良库：</label>
					<div class="layui-input-inline" style="width: 150px">
						<select lay-verify="regionId" id="regionId" name="regionId" lay-filter="regionId">
							<option value="">--请选择不良库区--</option>
						</select>
					</div>
					<label class="layui-form-label">数量：</label>
					<div class="layui-input-inline" style="width: 150px">
						<div class="layui-input-inline" style="width: 70px">
							<select id="type" name="type" lay-filter="type">
								<option value="0">大箱</option>
								<option value="1">小箱</option>
							</select>
						</div>
						<div class="layui-input-inline" style="width: 60px">
							<input class="layui-input" id="totality" name="totality"/>
						</div>
					</div>
				</div>
				<div class="layui-form-item" id="targetDiv" style="margin-top: 10px;">
					<label class="layui-form-label">不良描述：</label>
					<div class="layui-input-inline" style="width: 690px">
						<input class="layui-input" id="describe" name="describe"  lay-filter="describe"/>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn layui-btn-blue" id="subform" lay-submit lay-filter="addform" style="margin-left:130px">提交</button>
						<button type="reset" id="addReset" class="layui-btn layui-btn-primary" style="margin-left:200px">重置</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//日期
			/* laydate.render({
			   elem: '#birthdate'
			   ,type: 'date'
			}); */
			var $ = layui.jquery, active = {
					reload:function () {
						var keyword = $("#keyword").val();
						var keyword2 = $("#keyword2").val();
						keyword2 = "'"+keyword2+"'";
						var strWhere = "";
						if(keyword!="" && keyword2 != "''"){
							strWhere = 'm.customer_id = '+keyword+' and m.materiel_num = '+keyword2+' and ';
						}else if(keyword!="" && keyword2 == "''"){
							strWhere = 'm.customer_id = '+keyword+' and ';
						}else if(keyword=="" && keyword2 != "''"){
							strWhere = 'm.materiel_num = '+keyword2+' and ';
						}else{
							strWhere = ' ';
						}
						/* if(keyword!=""){
							strWhere = 'm.customer_id = '+keyword+' and ';
						}else{
							strWhere = '';
						} */
						table.reload('contenttable',{
							method:'get',
							where:{"strWhere":strWhere}
						});
					}
				}
				$('.layui-btn').on('click', function(){
					var type = $(this).data('type');
					active[type] ? active[type].call(this) : '';
				});
			table.render({
				elem: '#rejectsWarehouseList'
				//,url:'${pageContext.request.contextPath }/warehouse/selectrejectsWarehouseList.do'
				,url:'${pageContext.request.contextPath }/rejectsWarehouse/selectRejectsWarehouseList.do'
				,title: '不良库列表'
				,id :'contenttable'
				/* ,toolbar: '#toolbarDemo' */
				,limits:[10,20,30]
				,cols: [[
					{type: 'checkbox', fixed: 'left'}
					,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
					,{field:'customer', title:'供应商',templet:function(row){
						if(row.materiel.customer!=null){
							return row.materiel.customer.customer_name;
						}else{
							return "";
						}
					}}
					,{field:'materiel_name', title:'物料名称',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.materiel_name;
						}else{
							return "";
						}
					}}
					,{field:'materiel_num', title:'产品码',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.materiel_num;
						}else{
							return "";
						}
					}}
					,{field:'brevity_num', title:'简码',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.brevity_num;
						}else{
							return "";
						}
					}}
					,{field:'packing_quantity', title:'包装数量',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.packing_quantity;
						}else{
							return "";
						}
					}}
					,{field:'countAll', title:'总数量'}
					,{field:'mBatch', title:'批次'}
					,{field:'totality', title:'数量'}
					,{field:'unit', title:'单位',templet:function(row){
						if(row.materiel!=null){
							return row.materiel.unit;
						}else{
							return "";
						}
					}}
					,{field:'enterDate', title:'入库日期',templet:function(row){
						return row.enterDate.substr(0,16)
					}}
					,{field:'enterPerson', title:'入库人',templet:function(row){
						if(row.zcSysUser!=null){
							return row.zcSysUser.userName;
						}else{
							return "";
						}
					}}
				]]
				,page: true
				,done:function(res){
					$('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                })
	                merge(res);
				}
			});
			//监听行单击事件
			table.on('row(rejectsWarehouseList)', function(obj){
				var mid = obj.data.mid;
				var mBatch = obj.data.mBatch;
				if(mBatch == ""){
					var strWhere = " mt.mid = "+mid +" and cm.is_ng = 1 and "
				}else{
					var strWhere = " mt.mid = "+mid +" and mt.mBatch = "+mBatch+" and cm.is_ng = 1 and "
				}
				table.render({
					elem: '#inventoryInfoListDetails'
					,url:'${pageContext.request.contextPath }/inventoryManagement/selectInventoryInfoListDetails.do?strWhere='+strWhere
					,title: '库存详情列表'
					,limits:[10,20,30]
					,cols: [[
						{type: 'checkbox', fixed: 'left'}
						,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
						,{field:'location_code', title:'库位条码',templet:function(row){
							if(row.zcTrayAndLocation!=null){
								return row.zcTrayAndLocation.location_code;
							}else{
								return "";
							}
						}}
						/* ,{field:'tray_code', title:'托盘条码'} */
						,{field:'materiel_code', title:'物料条码',templet:function(row){
							return row.materiel_code.substring(0,row.materiel_code.length-1)
						}}
						,{field:'mNum', title:'物料数量'}
						,{field:'binding_date', title:'绑定日期'}
						,{field:'userName', title:'绑定人',templet:function(row){
							if(row.zcSysUser!=null){
								return row.zcSysUser.userName;
							}else{
								return "";
							}
						}}
						]]
					,page: true
					,done:function(res, curr, count){
		                mergeDetails(res);
						$('th').css({
		                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
		                })
					}
				})
			});
			//监听复选框事件
			table.on('checkbox(rejectsWarehouseList)',function(obj){
				if(obj.checked == true && obj.type == 'all'){ 
					//点击全选 
					$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
				}else if(obj.checked == false && obj.type == 'all'){ 
					//点击全不选 
					$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
				}else if(obj.checked == true && obj.type == 'one'){ 
					//点击单行
					if(obj.checked == true){ 
						obj.tr.addClass('layui-table-click'); 
					}else{ 
						obj.tr.removeClass('layui-table-click'); 
					} 
				}else if(obj.checked == false && obj.type == 'one'){ 
					//点击全选之后点击单行 
					if(obj.tr.hasClass('layui-table-click')){
						obj.tr.removeClass('layui-table-click'); 
					} 
				}
			})
			//头事件
			table.on('toolbar(rejectsWarehouseList)', function(obj){
				var checkStatus = table.checkStatus(obj.config.id);
				var data = checkStatus.data;
				switch(obj.event){
		    		case 'rejects':
		    			layer.open({
			                type: 1 				//Page层类型
			                ,area: ['920px', ''] 	//宽  高
			                ,title: '不良品入库'
			                ,shade: 0.6 			//遮罩透明度
			                ,maxmin: true 			//允许全屏最小化
			                ,anim: 1 				//0-6的动画形式，-1不开启
			                ,content:$('#formDiv')
			                ,success:function(){
			                	
			                }
		    			});
					break;
		        }
			})
			$("#mcode1").change(function(){
				var mcode1 = $("#mcode1").val();
				mcode1 = "'"+mcode1+"'";
				var empty = "''";
				var strWhere = '';
				strWhere = '(m.materiel_num = '+mcode1+' or m.brevity_num = '+mcode1+' or m.devanning_code = '+mcode1+') and '
				//通过产品码查批次
				$.ajax({
					type:'post'
					,url:'${pageContext.request.contextPath }/inventoryManagement/selectInventoryInfoByCode.do'
					,data:{'strWhere':strWhere}
					,dataType:'json'
					,success:function(res){
						$("#mBatch1").find("option").not(":first").remove();
						for(var i = 0;i < res.length;i++){
							if(i-1 == -1){
								$("#mBatch1").append('<option value="'+res[i].mBatch+'">'+res[i].mBatch+'</option>');
							}else if(res[i].mBatch != res[i-1].mBatch){
								$("#mBatch1").append('<option value="'+res[i].mBatch+'">'+res[i].mBatch+'</option>');
							}
						}
						form.render();
					}
				})
				//通过产品码查规格型号
				$.ajax({
					type:'post'
					,url:'${pageContext.request.contextPath }/inventoryManagement/selectMaterielSizeByCode.do'
					,data:{'strWhere':strWhere}
					,dataType:'json'
					,success:function(res){
						$("#sizeAndPre").find("option").not(":first").remove();
						for(var i = 0;i < res.length;i++){
							$("#sizeAndPre").append('<option value="'+res[i].materiel_size+'/'+res[i].materiel_properties+'">'+res[i].materiel_size+'/'+res[i].materiel_properties+'</option>');
						}
						form.render();
					}
				})
			})
			form.on('select(warehouseId)', function(data){
				var val = data.value;
				var customer_id = $("#customer_id").val();
				if(val==""){
					val = 0;
				}
				if(customer_id==""){
					customer_id = 0;
				}
				//通过仓库id查询库区列表
				var strWhere = 'r.warehouse_id = '+ val + 'and r.region_type = -1 and cw.cid = '+customer_id+' and '
				$.ajax({
					type:'post'
					,url:'${pageContext.request.contextPath }/inventoryManagement/selectWarehouseRegionListByWarehouseId.do'
					,data:{'strWhere':strWhere}
					,dataType:'json'
					,success:function(res){
						$("#regionId").find("option").not(":first").remove();
						for(var i = 0;i < res.length;i++){
							$("#regionId").append('<option value="'+res[i].id+'">'+res[i].region_name+'</option>');
						}
						form.render();
					}
				})
			}); 
			 /**
		     * 通用表单提交(AJAX方式)（新增）
		     */
		    form.on('submit(addform)', function (data) {
		    	var totality = Number($("#totality").val());
		    	var mNum = Number($("#mNum").val());
		    	var mcode = $("#mcode1").val();
		    	var regionId = $("#regionId").val();
		    	if(mcode == ""){
		    		toastr.warning('请输入产品码或简码！')
		    		$("#subform").attr("disabled",false);
		    	}else if(totality < 1 || totality == ""){
		    		toastr.warning('不良品总数不能小于1或为空！')
		    		$("#subform").attr("disabled",false);
		    	}else if(totality>mNum){
		    		toastr.warning('不良品总数不能大于库存数量！')
		    		$("#subform").attr("disabled",false);
		    	}else if(regionId== ""){
		    		toastr.warning('不良品库不能为空！')
		    		$("#subform").attr("disabled",false);
		    	}else{
		    		$.ajax({
			    		url : '${pageContext.request.contextPath }/rejectsWarehouse/enterRejects.do',
			   			data : $("#addform").serialize(),
			   			type : 'post'
			   		}).done(
			   				function(res){
				   				if(res>0){
				   					toastr.success('入库成功！')
					  				setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
					  					//关闭模态框
					  					// 父页面刷新
					  					window.location.reload();  
					  				},2000);
				   				}
				   			}
						)
		    	}
		    })
		});
		
		//输入产品码或简码、规格型号、批次查询库存信息
		function selectInventoryInfo(){
			var mcode1 = $("#mcode1").val();
			var mBatch1 = $("#mBatch1").val();
			var sizeAndPre = $("#sizeAndPre").val();
			var size = sizeAndPre.split("/")[0];
			var pre = sizeAndPre.split("/")[1];
			mcode1 = "'"+mcode1+"'";
			mBatch1 = "'"+mBatch1+"'";
			size = "'"+size+"'";
			pre = "'"+pre+"'";
			var empty = "''";
			var strWhere = '';
			if(mBatch1 != "''"){
				strWhere = '(m.materiel_num = '+mcode1+' or m.brevity_num = '+mcode1+') and m.materiel_size = '+size+' and m.materiel_properties = '+pre+' and info.mBatch = '+mBatch1+' and '
			}else{
				strWhere = '(m.materiel_num = '+mcode1+' or m.brevity_num = '+mcode1+') and m.materiel_size = '+size+' and m.materiel_properties = '+pre+' and '
			}
			//通过产品码或简码查询库存信息
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectInventoryInfoByCode.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					if(res.length == 0){
						if(mBatch1 == "''"){
							toastr.warning('请选择批次！')
						}else if(sizeAndPre == ""){
							toastr.warning('请选择规格型号！')
						}else{
							toastr.warning('未查询到库存信息！')
							if($("#mid").val()!= ""){
								//document.getElementById("addReset").click();
								$("#tableDiv input").val("");
							}
						}
					}else if(res.length >= 2){
						if(mBatch1 == "''"){
							toastr.warning('请选择批次！')
						}else if(sizeAndPre == ""){
							toastr.warning('请选择规格型号！')
						}
					}else{
						$("#mid").val(res[0].mid);
						$("#customer").val(res[0].materiel.customer.customer_name);
						$("#customer_id").val(res[0].materiel.customer_id);
						/* $("#warehouseId").val(res[0].warehouseId);
						$("#regionId").val(res[0].regionId); */
						$("#warehouseId").val("");
						$("#regionId").val("");
						$("#materiel_name").val(res[0].materiel.materiel_name);
						$("#materiel_num").val(res[0].materiel.materiel_num);
						$("#brevity_num").val(res[0].materiel.brevity_num);
						$("#packing_quantity").val(res[0].materiel.packing_quantity);
						$("#unit").val(res[0].materiel.unit);
						$("#materiel_size").val(res[0].materiel.materiel_size);
						$("#materiel_properties").val(res[0].materiel.materiel_properties);
						$("#mBatch").val(res[0].mBatch);
						$("#mNum").val(res[0].mNum);
						$("#enterDate").val(res[0].enterDate);
						$("#enterPerson").val(res[0].enterPerson);
						layui.form.render('select');
					}
				}
			})
		}
		//合并单元格
		function merge(res) {
	        var data = res.data;
	        var mergeIndex = 0;//定位需要添加合并属性的行数
	        var mergeIndexAll = 0;//定位需要添加合并属性的行数
	        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
	        var columsName = ['customer_id','materiel_name'];//需要合并的列名称
	        var columsNameAll = ['materiel_num','brevity_num','packing_quantity','countAll','unit'];//需要合并的列名称
	        var columsIndex = [2,3];//需要合并的列索引值
	        var columsIndexAll = [4,5,6,7,10];//需要合并的列索引值
	    	var allMark = 1;
	        for (var k = 0; k < columsNameAll.length; k++) { //这里循环所有要合并的列
	            var trArr = $(".layui-table-body>.layui-table").find("tr");//所有行
	                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndexAll[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndexAll).find("td").eq(columsIndexAll[k]);//获取相同列的第一列
	                    if(data[i].materiel.materiel_num === data[i-1].materiel.materiel_num){
	                    	allMark+=1;
	                    	tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", allMark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else{
	                    	mergeIndexAll = i;
	                    	allMark=1;
	                    }
	                }
	            mergeIndexAll = 0;
	            allMark = 1;
	        } 
	        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
	            var trArr = $(".layui-table-body>.layui-table").find("tr");//所有行
	                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
	                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
	                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列
	                    if (data[i].materiel[columsName[k]] === data[i-1].materiel[columsName[k]]) { //后一行的值与前一行的值做比较，相同就需要合并
	                        mark += 1;
	                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
	                            $(this).attr("rowspan", mark);
	                        });
	                        tdCurArr.each(function () {//当前行隐藏
	                            $(this).css("display", "none");
	                        });
	                    }else {
	                        mergeIndex = i;
	                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
	                    }
	                }
	            mergeIndex = 0;
	            mark = 1;
	        } 
	    }
		$(document).on('click', '#fDiv .layui-table-view .layui-table-body tr', function (event) {
	    	var trElem = $(this);
	      	var tableView = trElem.closest('.layui-table-view');
	      	tableView.find('.layui-table-body tr.table_tr_click').removeClass('table_tr_click');
	      	tableView.find('.layui-table-body tr[data-index="' + trElem.data('index') + '"]').addClass('table_tr_click');
	    });
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>

</html>