<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>绑定/解绑</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		#bind{
			width: 300px;
			height: 150px;
			background-color: green;
			margin-bottom: 20px;
			text-align: center;
		}
		#unbind{
			width: 300px;
			height: 150px;
			background-color: red;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
		#ob{
			width: 300px;
			height:320px;
			margin: 200px auto;
			text-align: center;
			color:#FFF;
			font-size: 50px;
			line-height: 150px;
			vertical-align: center;
		}
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div id="formDiv" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform">
				<input id="mid" name="mid" class="layui-hide">
				<input id="mBatch" name="mBatch" class="layui-hide">
				<input id="is_ng" name="is_ng" class="layui-hide">
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">物料条码</label>
						<div class="layui-input-inline" style="width: 690px">
							<textarea class="layui-textarea" placeholder="请输入物料条码，多个条码用英文逗号隔开并以逗号结尾" id="materiel_code" name="materiel_code"></textarea>
						</div>
					</div>
				</div>
				<div class="layui-form-item layui-hide">
					<div class="layui-block">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="tray_code" name="tray_code" type="text" />
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="location_code" name="location_code" type="text" />
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" style="margin-left:130px" id="subform">绑定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:200px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div id="formDiv2" hidden>
		<div class="box">
			<form class="layui-form layui-card-body" id="addform2">
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">物料条码</label>
						<div class="layui-input-inline" style="width: 690px">
							<textarea class="layui-textarea" placeholder="请输入物料条码，多个条码用英文逗号隔开并以逗号结尾" id="materiel_code2" name="materiel_code"></textarea>
						</div>
					</div>
				</div>
				<div class="layui-form-item layui-hide">
					<div class="layui-block">
						<label class="layui-form-label">托盘条码</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" id="tray_code2" name="tray_code" type="text" />
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">库位条码</label>
						<div class="layui-input-inline" style="width: 150px">
							<input class="layui-input" readonly id="location_code2" name="location_code" type="text" />
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">下架类型</label>
						<div class="layui-input-inline" style="width: 150px">
							<select class="layui-select" name="downType">
								<option value="1">整盘下架</option>
								<option value="2">选定下架</option>
							</select>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-block">
						<label class="layui-form-label">下架原因</label>
						<div class="layui-input-inline" style="width: 690px">
							<textarea class="layui-textarea" placeholder="请输入下架原因" name="unbindle_reason"></textarea>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" style="margin-left:130px" id="subform2">解绑</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:200px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>上架/下架</cite>
			</a>
		</span>
    </div>
	<div style="width:100%;height:100%">
		<div id="ob">
			<div id="bind">
				<span>物料上架</span>
			</div>
			<div id="unbind">
				<span>物料下架</span>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			$("#materiel_code").change(function(){
				var materiel_code = $("#materiel_code").val();
				$.ajax({
	        		url : '${pageContext.request.contextPath }/bindAndUnbind/selectMaterielInfoByCode.do',
	       			data : {"materiel_code":materiel_code},
	       			type : "post",
	       			dataType : "json",
	       			success:function(res){
	       				var data = res.data;
	       				if(data != null){
		       				$("#mid").val(data.id)
		       				$("#mBatch").val(data.pici)
		       				$("#is_ng").val(data.is_ng)
	       				}
	       				if(data == null && $("#materiel_code").val() != ""){
	       					toastr.error('请扫描正确的物料条码');
	       					$("#materiel_code").val("");
	       				}else if(data.id == -1){
	       					toastr.error('物料已经绑定过了');
	       					$("#materiel_code").val("");
	       				}else if(data.id == -2){
	       					toastr.error('请扫描相同种类和批次的物料条码');
	       					$("#materiel_code").val("");
	       				}else if(data.is_bindtray == 0){
	       					var timestamp=new Date().getTime()
	       					$("#tray_code").val(timestamp);
	       					$("#tray_code").addClass("layui-hide");
	       				}else{
	       					$("#tray_code").val("");
	       					$("#tray_code").removeClass("layui-hide");
	       				}
	       			}
	    		}) 
			})
			$("#materiel_code2").change(function(){
				var materiel_code = $("#materiel_code").val();
				$.ajax({
	        		url : '${pageContext.request.contextPath }/bindAndUnbind/selectMaterielInfoByCode.do',
	       			data : {"materiel_code":materiel_code},
	       			type : "post",
	       			dataType : "json",
	       			success:function(res){
	       				var data = res.data;
	       				if(data == null && $("#materiel_code").val() != ""){
	       					toastr.error('请扫描正确的物料条码');
	       				}else if(data.id == -2){
	       					toastr.error('请扫描相同种类的物料条码');
	       				}else{
	       					//通过物料条码查询它所在的托盘和库位
	       					$.ajax({
				        		url : '${pageContext.request.contextPath }/bindAndUnbind/selectMaterielLocationByCode.do',
				       			data : {"materiel_code":materiel_code},
				       			type : "post",
				       			dataType : "json",
				       			success:function(res2){
				       				var data2 = res2.data;
				       				if(data2 != null){
					       				$("#location_code2").val(data2.location_code);
					       				$("#tray_code2").val(data2.tray_code);
				       				}else{
				       					toastr.error('物料不在货架上');
				       				}

				       			}
	       					})
	       				}
	       			}
	    		}) 
			})
			$("#bind").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['900px', ''] 	//宽  高
	                ,title: '绑定'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv')
	                ,success:function(){
	                	document.getElementById("addform").reset();
	                	form.render();
	                }
    			});
			})
			$("#unbind").click(function(){
				layer.open({
	                type: 1 				//Page层类型
	                ,area: ['900px', ''] 	//宽  高
	                ,title: '解绑'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv2')
	                ,success:function(){
	                	document.getElementById("addform2").reset();
	                	form.render();
	                }
    			});
			})
			//绑定托盘物料并上架
	        form.on('submit(addform)', function (data) {
	        	$("#subform").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/bindAndUnbind/bindingAndUp.do',
	       			data : $('#addform').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('绑定成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else if(res == -3){
	    		  				toastr.error('绑定失败，该托盘已上架');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -4){
	    		  				toastr.error('绑定失败，此物料不能上架');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -5){
	    		  				toastr.error('绑定失败，物料不能上架到此库区');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else if(res == -6){
	    		  				toastr.error('绑定失败，此物料为不良品，不能上架到此库位');
	    		  				$("#subform").attr("disabled",false);
	    		  			}else{
	    		  				toastr.error('绑定失败！');
	    		  				$("#subform").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
			//下架解绑托盘物料
	        form.on('submit(addform2)', function (data) {
	        	$("#subform2").attr("disabled",true);
	        	$.ajax({
	        		url : '${pageContext.request.contextPath }/bindAndUnbind/downAndUnbind.do',
	       			data : $('#addform2').serialize(),
	       			type : "post",
	       			//dataType : "json",
	    			}).done(
	    				function(res) {
	    		  			if(res>0){
	    		  				toastr.success('解绑成功！');
	    	                    setTimeout(function(){
	    	                        location.reload();
	    	                    },2000);
	    		  			}else{
	    		  				toastr.error('解绑失败！');
	    		  				$("#subform2").attr("disabled",false);
	    		  			}
	    				}
	    			)
	    			return false;
	        });
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>
</html>
