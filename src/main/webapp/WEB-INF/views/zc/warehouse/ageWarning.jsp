<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>库龄预警</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>库龄预警</cite>
			</a>
		</span>
    </div>
	<div class="x-body">
		<div class="layui-row">
			<form class="layui-form layui-col-md12 x-so" onsubmit="retuen false">
				<input type="text" name="materialName" id="materialName"  placeholder="请输入物料名称" autocomplete="off" class="layui-input">
				<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
			</form>
		</div>
		
		<table class="layui-hide" id="warehouseInfoList" lay-filter="warehouseInfoList"></table>
	</div>
	<!-- <div id="formDiv" hidden>
		<form class="layui-form layui-card-body" id="addform" onsubmit="return false;">
			<label class="layui-form-label">仓库安全期</label>
			<input class="layui-input" lay-verify="safeDate" id="safeDate" name="safeDate" type="text" value="" style="width:250px;">
			<div style="text-align: center;margin-top: 20px">
				<button id="subBtn" class="layui-btn layui-btn-blue" lay-submit lay-filter="submitBtn">提交</button>
				<button type="reset" id="addReset" class="layui-btn layui-btn-primary" style="margin-left:140px">重置</button>
			</div>
		</form>
	</div> -->
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var materialName = $("#materialName").val();
					var strWhere = "materialName like '%"+materialName+"%'";
					table.reload('contenttable',{
						method:'get',
						where:{"strWhere":strWhere}
					});
				}
			}
			
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#warehouseInfoList'
				//,url:'${pageContext.request.contextPath }/warehouse/selectWarehouseInfoList.do'
				,url:'${pageContext.request.contextPath }/json/selectAgeWarningList.json'
				,title: '库存列表'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [[
					{type: 'checkbox', fixed: 'left'}
					,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
					,{field:'materialCode', title:'仓库编码'}
					,{field:'materialName', title:'仓库名称'}
					,{field:'lastDate', title:'库龄'}
					,{field:'safeDate', title:'仓库安全期', templet: function (row) {
				        return '<a style="color:green" class="safeDate" name="safeDate" verify placeholder="请选择时间" style="border: 0">'+(row.safeDate || '')+'</a>';
				    }}
				]]
				,page: true
				,done:function(res){
					//日期
					$('.safeDate').each(function(){
						laydate.render({
							elem: this       //使用this指向当前元素,不能使用class名, 否则只有第一个有效
						});
					});
					$('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                })
				}
			});
			//监听复选框事件
			table.on('checkbox(warehouseInfoList)',function(obj){
				if(obj.checked == true && obj.type == 'all'){ 
					//点击全选 
					$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
				}else if(obj.checked == false && obj.type == 'all'){ 
					//点击全不选 
					$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
				}else if(obj.checked == true && obj.type == 'one'){ 
					//点击单行
					if(obj.checked == true){ 
						obj.tr.addClass('layui-table-click'); 
					}else{ 
						obj.tr.removeClass('layui-table-click'); 
					} 
				}else if(obj.checked == false && obj.type == 'one'){ 
					//点击全选之后点击单行 
					if(obj.tr.hasClass('layui-table-click')){
						obj.tr.removeClass('layui-table-click'); 
					} 
				}
			})
		  	/* //监听单元格事件
		  	table.on('tool(warehouseInfoList)', function(obj){
			    var data = obj.data;
			    if(obj.event === 'setSafeDate'){
			    	layer.open({
						type: 1 		//Page层类型
						,area: ['450px', '200px'] //宽  高
						,title: '选择仓库安全日期'
						,shade: 0.6 	//遮罩透明度
						,shadeClose: true //点击遮罩关闭
						,maxmin: true //允许全屏最小化
						,anim: 1 		//0-6的动画形式，-1不开启
						,content: $('#formDiv')
						,success: function () {
							$("#safeDate").val(data.safeDate);
						}
						,end: function () {
							var formDiv = document.getElementById('formDiv');
							formDiv.style.display = '';
							layer.closeAll();
						}
					});
			    }
		  	}); */
			//头事件——批量生成转移任务
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>

</html>