<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>精简入库</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.css">
	<style type="text/css">
		#head td{
			width: 300px
		}
	</style>
</head>
<script type="text/javascript">
	$(document).on('keydown','[lay-id="materiel_list"] .layui-table-edit',function(e){
		var td = $(this).parent('td'),
		tr = td.parent('tr'),
		trs = tr.parent().parent().find('tr'),
		tr_index = tr.index(),
		td_index = td.index(),
		td_last_index = tr.find('[data-edit="text"]:last').index(),
		td_first_index = tr.find('[data-edit="text"]:first').index();
		switch(e.keyCode){
			case 13:
				 return false;
				 break;
			case 39:
				td.nextAll('[data-edit="text"]:first').click();
				if(td_index == td_last_index){
					tr.next().find('td').eq(td_first_index).click();
					if(tr_index == trs.length-1){
						trs.eq(0).find('td').eq(td_first_index).click();
					}
				}
				setTimeout(function(){
					$('.layui-table-edit').select()
				},0)
				break;
			case 37:
				td.prevAll('[data-edit="text"]:first').click();
				setTimeout(function(){
					$('.layui-table-edit').select()
				},0)
				break;
			case 38:
				tr.prev().find('td').eq(td_index).click();
				setTimeout(function(){
					$('.layui-table-edit').select()
				},0)
				break;
			case 40:
				tr.next().find('td').eq(td_index).click();
				setTimeout(function(){
					$('.layui-table-edit').select()
				},0)
				break;
		} 
	})
</script>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
	<span class="layui-breadcrumb">
 			<a href="">首页</a>
		<a>
			<cite>精简入库</cite>
		</a>
	</span>
   </div>
<div class="x-body">

	<div class="layui-row">
		<div class="layui-form">
			<div class="layui-input-inline">



				<div class="layui-input-inline">
					<select style="width: 285px" class="" name="keyword" id="keyword" lay-filter="keyword">
						<option value="">--请选择供应商--</option>
						<c:forEach items="${allCustomerList }" var="customer">
							<option value="${customer.id}">${customer.customer_name }</option>
						</c:forEach>
					</select>

				</div>

				<div class="layui-input-inline">
					<select id="selectState" name="selectState" style="width: 285px">
						<option value="">--请选择--</option>
						<option value="1">已收货</option>
						<option value="0">未收货</option>
					</select>
				</div>

				<div class="layui-input-inline">
					<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>		</div>
			</div>

		</div>
	</div>

	<script type="text/html" id="toolbarDemo">
		<div class="layui-btn-container">
			<button class="layui-btn layui-btn-warm" lay-event="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
			<button class="layui-btn layui-btn-danger" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
			<button class="layui-btn" lay-event="confirm"><i class="layui-icon layui-icon-ok"></i>确认入库</button>
		</div>
	</script>
	<table class="layui-hide" id="test" lay-filter="test"></table>
	<script type="text/html" id="barDemo">
		<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit">编辑</i></a>
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete">删除</i></a>
	</script>
	<script type="text/html" id="barDemo2">
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete">删除</i></a>
	</script>
	
	<div id = "details">
			<table class="layui-hide" id="simplificationInDetails" lay-filter="simplificationInDetails"></table>
		</div>
</div>

<!-- 弹框 -->
<div id="formDiv" hidden>
	<div class="box">
		<form class="layui-form layui-card-body" id="addform">
			<table style="border: none;">
				<tbody id="head">
					<tr>
						<td>
							<span>入库单号&emsp;</span>
							<div class="layui-inline" style="width:65%;">
								<input class="layui-input layui-inline" name="receiveNumber" readonly="readonly" id="receiveNumber" lay-filter="receiveNumber"/>
							</div>
						</td>
						<td>
							<span>供应商&emsp;</span>
							<div class="layui-inline" style="width:65%">
								<select style="width: 100%" class="" name="sendCompany" id="sendCompany" lay-filter="sendCompany">
									<option value="">--请选择供应商--</option>
									<c:forEach items="${allCustomerList }" var="customer">
										<option value="${customer.id}">${customer.customer_name }</option>
									</c:forEach>
								</select>
							</div>
							<div class="layui-inline" style="float: right;margin-right: 1em;margin-top: 10px;">
								<font style="color:red; font-size: 24px;">*</font>
							</div>
						</td>
						<td>
							<span>发货人&emsp;</span>
							<div class="layui-inline" style="width:70%;">
								<input class="layui-input layui-inline" name="sendName" readonly="readonly" id="sendName" lay-filter="sendName"/>
							</div>
						</td>
					</tr>
					<tr><td><br></td></tr>
					<tr>
						<td>
							<span>日&emsp;&emsp;期&emsp;</span>
							<div class="layui-inline" style="width:65%">
								<input class="layui-input layui-inline" name="createDate" id="createDate" lay-filter="createDate"/>
							</div>
							<div class="layui-inline" style="float: right;margin-right: 1em;margin-top: 10px;">
								<font style="color:red; font-size: 24px;">*</font>
							</div>
						</td>
						<td>
							<input type="button" class="layui-btn" onclick="addProduct()" value="选择物料">
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<table class="layui-hide" id="materiel_list" lay-filter="materiel_list"></table>
			<div class="layui-form-item" style="margin-left: 250px">
				<div class="layui-input-block">
					<input type="button" class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" id="subform" value="确定">
					<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- 产品选择框 -->
<div id="productDiv" hidden="hidden">
	<form class="layui-form" onsubmit="return false">
		<div style="margin-bottom: 18px">
			<div class="layui-inline">
				<table style="margin-top: 10px">
					<tr>
						<td>
							<input class="layui-input" name="materiel_name" id="materiel_name" placeholder="请输入中文名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</td>
						<td>
							<input class="layui-input" name="materiel_num" id="materiel_num" placeholder="请输入SAP/PAD" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
						</td>
					</tr>
				</table>
			</div>
			&emsp;<button style="margin-top: 10px" class="layui-btn layui-btn-normal" type="button" data-type="reload2"><i class="layui-icon layui-icon-search"></i>检索</button>
		</div>
		<table id="productTable" lay-filter="productTable"></table>
		<xblock>
			<button class="layui-btn layui-btn-blue" lay-submit lay-filter="productform" style="margin: 0px 0px 10px 200px; width: 500px">提交</button>
		</xblock>
	</form>
</div>
<script>
layui.use(['table','layer','upload','form','laydate'], function(){
	var table = layui.table;
	var layer = layui.layer,
    form = layui.form,
    laydate = layui.laydate
	var $ = layui.jquery, active = {
			reload:function () {
				var keyword = $("#keyword").val();

				var selectState = $("#selectState").val();


				if(keyword == "" && selectState == ""){
					var strWhere = "r.isSimple = 1 and ";
				}else {

					if (keyword == "") {

						var strWhere = "r.isSimple = 1 and state = "+selectState+" and ";
					}else if (selectState == "") {

						var strWhere = "r.isSimple = 1 and c.id = "+keyword+" and ";
					}else {
						var strWhere = "r.isSimple = 1 and c.id = "+keyword+" and state = "+selectState+" and ";
					}


				}


				table.reload('contenttable',{
					method:'get',
					where:{"strWhere": strWhere}
				});
			},
			reload2:function () {
				var materiel_name=$('#materiel_name').val();
	    		var materiel_num=$('#materiel_num').val();
				table.reload('contenttable2',{
					method:'get',
					where:{"materiel_name":materiel_name,"materiel_num":materiel_num}
				});
			}
		}
	$('.layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
	table.render({
	    elem: '#test'
	    ,url:'${pageContext.request.contextPath }/simplificationIn/selectSimplificationInList.do'
	    ,title: '入库单列表'
	    ,toolbar: '#toolbarDemo'
	    ,id :'contenttable'
	    ,limits:[10,20,30]
	    ,cols: [[
			{type: 'checkbox', fixed: 'left'}
	      	,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
	      	,{field:'receiveNumber', title:'入库单号'}
	      	,{field:'sendCompany', title:'供应商'}
	      	,{field:'createDate', title:'发货日期',templet:function(row){
				return row.createDate.substr(0,10)
			}}
	      	,{field:'sendName', title:'发货人'}
	      	,{field:'createName', title:'创建人'}
	      	,{field:'state', title:'订单状态'
	      		,templet: function(d){
                    var value = d.state;
                    if(value == 0){
                        value = "未收货"
                    }else if(value == 1){
                        value = "已收货"
                    }
                    return value;
                }	
	      	}
	      	/* ,{fixed: 'right', title:'操作', toolbar: '#barDemo'} */
	    ]]
	    ,page: true
	    ,done:function(){
	    	$('th').css({
                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
            })
	    }
	});
	table.render({
        elem: '#materiel_list'
        ,defaultToolbar:['filter', 'print']
        ,cols: [[
            {type: 'checkbox',fixed: 'left'},
            {field:'', title:'序号', width: 50,type:'numbers', align:'center'},
            {field:'id', title:'id', align:'center', hide:true},
            {field:'mid', title:'mid', align:'center', hide:true},
            {field:'materiel_name', title:'中文名称', align:'center'},
            {field:'materiel_num', title:'SAP/PAD', align:'center'},
            {field:'packing_quantity', title:'包装数量', align:'center'},
            {field:'outgoing_frequency', title:'出库频率', align:'center'},
            {field:'totalNum', title:'<span style="color: red">发货总数</span>', edit: 'text', align:'center'},
            ,{fixed: 'right', title:'操作', toolbar: '#barDemo2'}
        ]]
        ,limit:Number.MAX_VALUE
        ,page: false
        ,height: 400
        ,done : function(){
            $('th').css({
                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
            })
        }
    });
	
	table.on('row(test)', function(obj){
        var data = obj.data;
        table.render({
            elem: '#simplificationInDetails'
            ,url:'${pageContext.request.contextPath }/receiveDetail/list.do?receiveNumber='+data.receiveNumber
            ,toolbar: '#Detailtoolbar'
            ,title: '明细表'
            ,limits:[10,20,30]
            //,defaultToolbar:['filter', 'print']
            ,cols: [[
                {type: 'checkbox',fixed: 'left'},
                {field:'id', title:'序号', width: 50,type:'numbers', align:'center'},
                {field:'mid', title:'物料id', align:'center', hide:true},
                {field:'materiel_name', title:'物料名称', align:'center'},
                {field:'materiel_num', title:'产品码', align:'center'},
                {field:'packing_quantity', title:'包装数量', align:'center'},
                {field:'totalNum', title:'发货总数(个)', align:'center'},
                {field:'codeNum', title:'收货总数(个)', align:'center'},
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });
    });
	//监听复选框事件
	table.on('checkbox(test)',function(obj){
		if(obj.checked == true && obj.type == 'all'){ 
			//点击全选 
			$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
		}else if(obj.checked == false && obj.type == 'all'){ 
			//点击全不选 
			$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
		}else if(obj.checked == true && obj.type == 'one'){ 
			//点击单行
			if(obj.checked == true){ 
				obj.tr.addClass('layui-table-click'); 
			}else{ 
				obj.tr.removeClass('layui-table-click'); 
			} 
		}else if(obj.checked == false && obj.type == 'one'){ 
			//点击全选之后点击单行 
			if(obj.tr.hasClass('layui-table-click')){
				obj.tr.removeClass('layui-table-click'); 
			} 
		}
	})
	
	//单子头事件
	table.on('toolbar(test)', function(obj){
		var checkStatus = table.checkStatus(obj.config.id);
		switch(obj.event){
    		case 'add':
    		layer.open({
    			  type: 1 		//Page层类型
    			  ,area: ['950px', ''] //宽  高
    			  ,skin: 'alert-skin'
    			  ,title: '新增入库单'
    			  ,shade: 0.6 	//遮罩透明度
    			  ,shadeClose: true //点击遮罩关闭
    			  ,maxmin: true //允许全屏最小化
    			  ,anim: 1 		//0-6的动画形式，-1不开启
    			  ,content: $('#formDiv')
    			  ,success: function () {
    				  var timestamp = (new Date()).getTime();
                      $("#receiveNumber").val("R-"+timestamp);

                      laydate.render({
                          elem: '#createDate'
                      });

                      $("#createDate").val(formatDate(new Date()))
                      table.reload('materiel_list',{
                          data : new Array()
                      });

                      $("#sendCompany").val("")
                      form.render();
    			  }
    			  ,end: function () {
    				  var formDiv = document.getElementById('formDiv');
    				  formDiv.style.display = '';
    			  }
    		});
    		break;
    		//批量删除
    		case 'delete':
    			var data = checkStatus.data;
	        	if(data.length==0){
	        		toastr.warning("请至少选择一条记录！");
	        	}else{
		        	var receiveNumber = new Array();
		        	for(var i=0;i<data.length;i++){
		        		receiveNumber[i] = data[i].receiveNumber;
		        		state = data[i].state;
		        		if(state!=0){
		        			toastr.warning("只能删除未收货的订单！");
		        		}else{
			        		layer.confirm('确定删除吗？', function(index){
			        			$.ajax({
			        		    	type:'post',
			        		    	url:'${pageContext.request.contextPath }/simplificationIn/deleteSimplificationInByNumber.do',
			        		    	data:{"receiveNumber":receiveNumber},
			        		    	success:function(data){
			        		    		if(data>0){
			        		    			toastr.success("删除成功！");
			        		    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
			        							//关闭模态框
			        							// 父页面刷新
			        							window.location.reload();
			        						},2000);
			        		    		}else{
			        		    			toastr.warning("删除失败！");
			        		    		}
			        		    	}
		
			        		    })
			        		    layer.close(index);
			        		})
		        		}
		        	}
	        	}
    		break;
    		case 'confirm':
    			var data = checkStatus.data;
    			if(data.length==0){
	        		toastr.warning("请至少选择一条记录！");
	        	}else{
	    			var receiveNumber = data[0].receiveNumber;
	    			if(data[0].state == 1){
	    				toastr.warning("已收货，请不要重复操作");
	    				return;
	    			}else{
	    				$.ajax({
		   		            type:'post',
		   		            data:'receiveNumber='+receiveNumber,
		   		            url:'${pageContext.request.contextPath}/simplificationIn/submitReceipt.do',
		   		            dataType: 'JSON',
		   		            async: false,
		   		            success:function (result) {
		   		               if(result > 0) {
		   		            		toastr.success("入库成功！");
		   		            		setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	        							// 父页面刷新
	        							window.location.reload();
	        						},2000);
		   		               }else {
		   		            		toastr.warning("入库失败！");
		   		               }
		   		            }
		   		        });
	    			}
	        	}
    		break;
		}
	});
	
	//选择供应商选中id查询该供应商的所有物料
    form.on('select(sendCompany)', function(data){
        var customer_id = data.value;
        $.ajax({
            type:'post',
            data:'id='+customer_id,
            url:'${pageContext.request.contextPath}/others/customerByName.do',
            dataType: 'JSON',
            async: false,
            success:function (json) {
                if(null != json){
                    $("#sendName").val(json.contacts_name)
                }
            }
        });
        table.reload('materiel_list',{
            data : new Array()
        });
    });
	//监听行工具事件
	table.on('tool(materiel_list)', function(obj){
		var data = obj.data;
		var oldData = table.cache["materiel_list"];
		if(obj.event === 'del'){
		  layer.confirm('确定删除吗？', function(index){
		  	oldData.pop(data);
		  	table.reload('materiel_list',{
                data : oldData
            });
		  	layer.close(index);
		  });
		}
	});
	/**
     * 表单校验
     */
    form.verify({
    	//value：表单的值、item：表单的DOM对象
    	sendCompany: function(value, item){
    		if(value == ''){
    			return '供应商不能为空';
    		}
    	},
    	createDate: function(value, item){
    		if(value == ''){
    			return '日期不能为空';
    		}
    	}
    });
    /**
     * 添加弹框中，选择物料后，点提交，带回的操作
     */
    form.on('submit(productform)', function (data) {
        var flag;
        var oldData = table.cache["materiel_list"];
        var checkStatus = table.checkStatus('contenttable2')
        //选中的物料
        var data = checkStatus.data;
        for (var j = 0; j < data.length; j++) {
            for (var k = 0; k < oldData.length; k++) {
                if (oldData[k].id == data[j].id) {
                    flag = 0;
                }
            }
        }
        if (flag != 0) {
            for (var i = 0; i < data.length; i++) {
                oldData.push(data[i]);
            }
        } else {
            toastr.warning("记录已经存在，不能重复添加");
        }
        table.reload('materiel_list',{
            data : oldData
        });
        $('[lay-id="materiel_list"] [data-edit="text"]:first').click();
        layer.close(productIndex)
    });
    /**
     * 通用表单提交(AJAX方式)（新增）
     */
    form.on('submit(addform)', function (data) {
    	$("#subform").attr("disabled",true);
    	var receiveNumber = $("#receiveNumber").val();
    	var oldData = table.cache["materiel_list"];
    	var data = oldData.filter(function(item){
            return item.totalNum == "" || item.totalNum == null || item.totalNum == undefined;
        })
    	if(oldData.length == 0){
    		toastr.warning("请选择物料！");
    		$("#subform").attr("disabled",false);
    	}/* else if(data.length > 0){
            toastr.warning("请输入正确的批次号");
    		$("#subform").attr("disabled",false);
        } */else{
        	//新增入库单
    		$.ajax({
    			type:'post'
    			,url:'${pageContext.request.contextPath }/simplificationIn/insertSimplificationIn.do'
    			,data:$('#addform').serialize()
    			,success:function(res){
    				if(res>0){
    					for (var i = 0; i < oldData.length; i++) {
    	                    var id = oldData[i].id;
    	                    var mid = oldData[i].id;
    	                    var totalNum = oldData[i].totalNum;
    	                    //添加收货明细
    	                    $.ajax({
    	                        url: '${pageContext.request.contextPath}/simplificationIn/insertDetails.do' ,
    	                        type: 'post',
    	                        data: {receiveNumber:receiveNumber, mid:mid, totalNum:totalNum},
    	                        async: false,
    	                        dataType:'JSON',
    	                        success: function (result) {
    	                            if (result > 0) {
    	                            	 toastr.success('新增成功');
                                         setTimeout(function(){
                                             window.location.reload();
                                         },1000);
    	                            }else{
    	                                toastr.warning("对不起，明细添加失败！");
    	                                $("#subform").attr("disabled",false);
    	                            }
    	                        },
    	                        error: function (data) {
    	                            toastr.error("对不起，明细添加错误！");
    	                            $("#subform").attr("disabled",false);
    	                        }
    	                    })
    	                }
    				}else{
    					toastr.error("对不起，入库单新增失败！");
    					$("#subform").attr("disabled",false);
    				}
    			}
    		})
    	}
    	var id= $("#id").val();
	})
});
function addProduct(){
	layui.use(['table','layer','upload','form'], function(){
		var table = layui.table;
		var layer = layui.layer,
	    form = layui.form
		var $ = layui.jquery
	    if ($("#sendCompany").val() == "") {
	        //toastr.warning("请选择供应商！");
	        layer.msg("请选择供应商！");
		} else {
	        productIndex = layer.open({
	            type: 1 					//Page层类型
	            ,area: ['900px', '450px'] 	//宽  高
	            ,title: '物料清单'
	            ,shade: 0.6 				//遮罩透明度
	            ,maxmin: true 				//允许全屏最小化
	            ,anim: 1 					//0-6的动画形式，-1不开启
	            ,content: $('#productDiv')
	            ,end: function () {
	                var formDiv = document.getElementById('productDiv');
	                formDiv.style.display = '';
	            }
	            ,success: function () {
	                var customer_id = $("#sendCompany").val();
	                table.render({
	                    elem: '#productTable'
	                    ,url:'${pageContext.request.contextPath }/receiveDetail/materielList.do?customer_id='+customer_id
	                    ,limits:[10,20,30]
	                    ,id :'contenttable2'
	                    ,defaultToolbar:['filter', 'print']
	                    ,request: {   //如果无需自定义请求参数，可不加该参数
	                        pageName: 'page' //页码的参数名称，默认：page
	                        ,limitName: 'pageSize' //每页数据量的参数名，默认：limit
	                    }
	                    ,cols: [
	                        [{
	                            type: 'checkbox',
	                            fixed: 'left'
	                        }, {
	                            field:'id',
	                            title:'序号',
	                            sort: true,
	                            width: 60,
	                            type:'numbers',
	                            align:'center'
	                        }, {
	                            field: 'id',
	                            title: 'id',
	                            hide:true
	                        } , {
	                            field: 'customer_name',
	                            title: '供应商名称',
	                            align:'center',
	                        }, {
	                            field: 'materiel_name',
	                            title: '中文名称',
	                            align:'center',
	                        }, {
	                            field: 'materiel_num',
	                            title: 'SAP/PAD',
	                            align:'center',
	                        }, {
	                            field: 'outgoing_frequency',
	                            title: '出库频率',
	                            align:'center',
	                        }, {
	                            field: 'packing_quantity',
	                            title: '包装数量',
	                            align:'center',
	                        }
	                        ]
	                    ]
	                    ,page: false
	                });
	            }
	        });
		}
	});
}
var formatDate = function (date) {  
    var y = date.getFullYear();  
    var m = date.getMonth() + 1;  
    m = m < 10 ? '0' + m : m;  
    var d = date.getDate();  
    d = d < 10 ? ('0' + d) : d;  
    return y + '-' + m + '-' + d;  
};  
toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>

</body>
</html>