<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>库房管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.css">
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
	<span class="layui-breadcrumb">
 			<a href="">首页</a>
		<a>
			<cite>库房管理</cite>
		</a>
	</span>
   </div>
<div class="x-body">
	<div class="layui-row">
		<div class="layui-form layui-col-md12 x-so">
			<input type="text" name="keyword" id="keyword"  placeholder="请输入库房名称" autocomplete="off" class="layui-input">
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</div>
	</div>
	<script type="text/html" id="toolbarDemo">
		<div class="layui-btn-container">
			<button class="layui-btn layui-btn-warm" lay-event="add"><i class="layui-icon layui-icon-delete"></i>新增</button>
			<button class="layui-btn layui-btn-danger" lay-event="delete"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
		</div>
	</script>
	<table class="layui-hide" id="test" lay-filter="test"></table>
	<script type="text/html" id="barDemo">
		<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit">编辑</i></a>
		<a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="del"><i class="layui-icon layui-icon-delete">删除</i></a>
	</script>
</div>

<!-- 弹框 -->
<div id="formDiv" hidden>
	<div class="box">
		<form class="layui-form layui-card-body" id="addform">
			<input hidden id="id" name="id">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">库房名称</label>
					<div class="layui-input-inline">
						<input lay-verify="house_name" class="layui-input" type="text" id="house_name" name="house_name" style="width: 375px">
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">库房编号</label>
					<div class="layui-input-inline">
						<input lay-verify="house_code" class="layui-input" type="text" id="house_code" name="house_code" style="width: 375px">
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">所属仓库</label>
					<div class="layui-input-inline" style="width: 375px">
						<select lay-verify="warehouse_id" id="warehouse_id" name="warehouse_id" lay-filter="warehouse_id">
							<c:forEach items="${allWarehouseList }" var="ware">
								<option value="${ware.id }">${ware.warehouse_name }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">库房管理员</label>
					<div class="layui-input-inline" style="width: 375px">
						<select lay-verify="house_clerk" id="house_clerk" name="house_clerk" lay-filter="house_clerk">
							<c:forEach items="${allSysUserList }" var="user">
								<option value="${user.id }">${user.userName }</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="button" class="layui-btn layui-btn-blue" lay-submit lay-filter="addform" id="subform" value="确定">
					<button class="layui-btn layui-btn-primary" style="margin-left:240px">取消</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
layui.use(['table','layer','upload','form'], function(){
	var table = layui.table;
	var layer = layui.layer,
    form = layui.form
	var $ = layui.jquery, active = {
			reload:function () {
				var keyword = $("#keyword").val();
				var strWhere = "s.status = 0 and s.isdel = 0 and s.house_name like '%"+keyword+"%' and ";
				table.reload('contenttable',{
					method:'get',
					where:{"strWhere": strWhere}
				});
			}
		}
	$('.layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
	table.render({
	    elem: '#test'
	    ,url:'${pageContext.request.contextPath }/base/selectStorehouseList.do'
	    ,title: '库房列表'
	    ,toolbar: '#toolbarDemo'
	    ,id :'contenttable'
	    ,limits:[10,20,30]
	    ,cols: [[
			{type: 'checkbox', fixed: 'left'}
	      	,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
	      	,{field:'house_name', title:'库房名称'}
	      	,{field:'house_code', title:'库房编号'}
	      	,{field:'warehouse_name', title:'所属仓库'}
	      	,{field:'user_name', title:'库房管理员'}
	      	,{fixed: 'right', title:'操作', toolbar: '#barDemo'}
	    ]]
	    ,page: true
	    ,done:function(){
	    	$('th').css({
                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
            })
	    }
	});
	//监听复选框事件
	table.on('checkbox(test)',function(obj){
		if(obj.checked == true && obj.type == 'all'){ 
			//点击全选 
			$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
		}else if(obj.checked == false && obj.type == 'all'){ 
			//点击全不选 
			$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
		}else if(obj.checked == true && obj.type == 'one'){ 
			//点击单行
			if(obj.checked == true){ 
				obj.tr.addClass('layui-table-click'); 
			}else{ 
				obj.tr.removeClass('layui-table-click'); 
			} 
		}else if(obj.checked == false && obj.type == 'one'){ 
			//点击全选之后点击单行 
			if(obj.tr.hasClass('layui-table-click')){
				obj.tr.removeClass('layui-table-click'); 
			} 
		}
	})
	//头事件
	table.on('toolbar(test)', function(obj){
		var checkStatus = table.checkStatus(obj.config.id);
		switch(obj.event){
    		case 'add':
    		layer.open({
    			  type: 1 		//Page层类型
    			  ,area: ['590px', ''] //宽  高
    			  ,skin: 'alert-skin'
    			  ,title: '新增库房'
    			  ,shade: 0.6 	//遮罩透明度
    			  ,shadeClose: true //点击遮罩关闭
    			  ,maxmin: true //允许全屏最小化
    			  ,anim: 1 		//0-6的动画形式，-1不开启
    			  ,content: $('#formDiv')
    			  ,success: function () {
    				  document.getElementById("addform").reset();
    				  $("#house_code").removeAttr("readonly")
    				  $("#house_code").css("background","#FFF");
    			  }
    			  ,end: function () {
    				  var formDiv = document.getElementById('formDiv');
    				  formDiv.style.display = '';
    			  }
    		});
    		break;
    		//批量删除
    		case 'delete':
    			var data = checkStatus.data;
	        	if(data.length==0){
	        		toastr.warning("请至少选择一条记录！");
	        	}
	        	var idArr = new Array();
	        	for(var i=0;i<data.length;i++){
	        		idArr[i] = data[i].id;
	        		layer.confirm('确定删除吗？', function(index){
	        			$.ajax({
	        		    	type:'post',
	        		    	url:'${pageContext.request.contextPath }/base/deleteStorehouseById.do',
	        		    	data:{"idArr":idArr},
	        		    	success:function(data){
	        		    		if(data>0){
	        		    			toastr.success("删除成功！");
	        		    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	        							//关闭模态框
	        							// 父页面刷新
	        							window.location.reload();
	        						},2000);
	        		    		}else{
	        		    			toastr.warning("删除失败！");
	        		    		}
	        		    	}

	        		    })
	        		    layer.close(index);
	        		})
	        	}
    		break;
		}
	});

	//监听行工具事件
	table.on('tool(test)', function(obj){
		var data = obj.data;
		if(obj.event === 'del'){
		  layer.confirm('确定删除吗？', function(index){
		    var id = obj.data.id;
		    var idArr = new Array();
		    idArr[0] = id;
		    $.ajax({
		    	type:'post',
		    	url:'${pageContext.request.contextPath }/base/deleteStorehouseById.do',
		    	data:{"idArr":idArr},
		    	success:function(data){
		    		if(data>0){
		    			toastr.success("删除成功！");
		    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
							//关闭模态框
							// 父页面刷新
							window.location.reload();
						},2000);
		    		}else{
		    			toastr.warning("删除失败！");
		    		}
		    	}

		    })
		    layer.close(index);
		  });
		} else if(obj.event === 'edit'){
			layer.open({
			  type: 1 		//Page层类型
			  ,area: ['590px', ''] //宽  高
			  ,skin: 'alert-skin'
			  ,title: '编辑仓库'
			  ,shade: 0.6 	//遮罩透明度
			  ,shadeClose: true //点击遮罩关闭
			  ,maxmin: true //允许全屏最小化
			  ,anim: 1 		//0-6的动画形式，-1不开启
			  ,content: $('#formDiv')
			  ,success: function () {
				  $("#house_code").attr("readonly","readonly")
				  $("#house_code").css("background","#DDD");
				  for(var i=0;i<Object.entries(data).length;i++) {
						var id = '#' + Object.entries(data)[i][0];
						var text = Object.entries(data)[i][1];
						$(id).val(text);
					}
			  }	
			  ,end: function () {
				  	var formDiv = document.getElementById('formDiv');
				  	formDiv.style.display = '';
		  	  }
			});
		}
	});
	/**
     * 表单校验
     */
    form.verify({
    	//value：表单的值、item：表单的DOM对象
    	house_name: function(value, item){
    		if(value == ''){
    			return '库房名称不能为空';
    		}
    	},
    	house_code: function(value, item){
    		if(value == ''){
    			return '库房编号不能为空';
    		}
    	},
    	warehouse_id: function(value, item){
    		if(value == ''){
    			return '所属仓库不能为空';
    		}
    	},
    	house_clerk: function(value, item){
    		if(value == ''){
    			return '库房管理员不能为空';
    		}
    	}
    });
    /**
     * 通用表单提交(AJAX方式)（新增）
     */
    form.on('submit(addform)', function (data) {
    	$("#subform").attr("disabled",true);
    	var id= $("#id").val();
		$.ajax({
    		url : '${pageContext.request.contextPath }/base/saveStorehouse.do',
   			data : $('#addform').serialize(),
   			type : "post",
   			//dataType : "json",
   			success:function(res){
   				var list = res.split('_');
	  			if (parseInt(list[0]) > 0) {
	  				if(list[1].indexOf('add') != -1){
	  					toastr.success('新增成功！');
	  				}
	  				if(list[1].indexOf('edit') != -1){
	  					toastr.success('修改成功！');
	  				}
	  				setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	  					//关闭模态框
	  					// 父页面刷新
	  					window.location.reload();  
	  				},2000);
	  			} else if (parseInt(list[0]) == -2) {
	  				if(list[1].indexOf('add') != -1){
	  					toastr.error('新增失败，库房编号已存在！');
	  				}
	  				/* if(list[1].indexOf('edit') != -1){
	  					toastr.success('修改失败，库房编号已存在！');
	  				} */
	  				$("#subform").attr("disabled",false);
	  			}else{
	  				if(list[1].indexOf('add') != -1){
						layer.close(1);
						toastr.error('新增失败！');
						$("#subform").attr("disabled",false);
					}
					if(list[1].indexOf('edit') != -1){
						layer.close(1);
						toastr.error('修改失败！');
						$("#subform").attr("disabled",false);
					}
	  			}
   			}
			})
	})
});
toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>

</body>
</html>