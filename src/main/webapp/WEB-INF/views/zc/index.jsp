<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.sdry.model.zc.ZcSysUserEntity" %>
<% ZcSysUserEntity zcSysUserEntity =  (ZcSysUserEntity)request.getSession().getAttribute("user"); %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>华缘物流仓储系统</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body>
	
    <!-- 顶部开始 -->
    <div class="container">
        <div class="logo"><a href="${pageContext.request.contextPath }/index.jsp">华缘物流仓储系统</a></div>
        <div class="left_open">
            <i title="展开左侧栏" class="iconfont">&#xe699;</i>
        </div>
        <ul class="layui-nav right" lay-filter="">
          <li class="layui-nav-item">
            <a href="javascript:;"><%=zcSysUserEntity.getUserName() %></a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
              <dd><a href="javascript:editPass()">修改密码</a></dd>
              <dd><a href="javascript:editCode()">调账功能</a></dd>
              <dd><a href="${pageContext.request.contextPath }/login/pcloginOut.do">退出</a></dd>
            </dl>
          </li>
          <li class="layui-nav-item to-index"><a href="/">前台首页</a></li>
        </ul>
        
    </div>
    <!-- 顶部结束 -->
    <!-- 中部开始 -->
     <!-- 左侧菜单开始 -->
    <div class="left-nav">
		<div id="side-nav">
			<ul id="nav">
			   <c:forEach items="${menuList }" var="menu">
			   		<li>
			       		<a href="javascript:;">
		          			<i class="iconfont">&#xe723;</i>
		           			<cite>${menu.permissionName }</cite>
		      			</a>
		      			<ul class="sub-menu">
		      				<c:forEach items="${menu.mList }" var="subMenu">
		      					<li>
		      						<a _href="${pageContext.request.contextPath }/${subMenu.url}">
			                            <i class="iconfont">&#xe6a7;</i>
			                            <cite>${subMenu.permissionName}</cite>
			                        </a>
		      					</li>
		      				</c:forEach>
		      			</ul>
		   			</li>
				</c:forEach>
			</ul>
		</div>
    </div>
    <!-- <div class="x-slide_left"></div> -->
    <!-- 左侧菜单结束 -->
    <!-- 右侧主体开始 -->
    <div class="page-content">
        <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="false">
          <ul class="layui-tab-title">
            <li class="home"><i class="layui-icon">&#xe68e;</i>我的桌面</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe src='${pageContext.request.contextPath}/welcome.jsp' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
          </div>
        </div>
    </div>
    <div class="page-content-bg"></div>
    <!-- 右侧主体结束 -->
    <!-- 中部结束 -->
    <!-- 底部开始 -->
    <div class="footer">
        <div style="text-align: center" class="copyright">Copyright ©2019 北京盛达融元科技有限公司</div>  
    </div>
    <!-- 密码修改 -->
    <div id="formDiv" hidden="hidden">
		<div class="box">
		    <form class="layui-form layui-card-body" id="addform">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">原密码</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="srcPsd" type="password" style="width: 300px" lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">新密码</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="newPsd" type="password" style="width: 300px"  lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">确认密码</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="affirmPas" type="password" style="width: 300px"  lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" id="subform">确定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- 调账功能 -->
	<div id="formDiv01" hidden="hidden">
		<div class="box">
		    <form class="layui-form layui-card-body" id="editform">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">原条码</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="beforeCode" name="beforeCode" style="width: 300px" lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">SAP/QAD</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="mcode" style="width: 300px" lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">库位</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="location" style="width: 300px" lay-verify="required">
							<font style="color: red">没有库位请填写“无”</font>
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">新条码</label>
						<div class="layui-input-inline" style="width: 300px">
							<input class="layui-input" id="newCode" name="newCode" style="width: 300px" lay-verify="required">
						</div>
					</div>
					<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
						<font style="color:red; font-size: 24px;">*</font>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="editform" id="subBtn">确定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
    <script type="text/javascript">
    	function editPass(){
    		layui.use(['table','layer','form'], function(){
    			var table = layui.table;
    			var layer = layui.layer;
    			var form = layui.form;
    			layer.open({
	                type: 1 				//Page层类型
	                ,area: ['515px', ''] 	//宽  高
	                ,title: '修改密码'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv')
	                ,success:function(){
	                	
	                }
    			});
    			//修改
    	        form.on('submit(addform)', function (data) {
    	        	$("#subform").attr("disabled",true);
    	        	var newPsd = $("#newPsd").val();
    	        	var srcPsd = $("#srcPsd").val();
    	        	$.ajax({
    	        		url : '${pageContext.request.contextPath }/system/checkPsd.do',
    	       			data : {"srcPsd":srcPsd},
    	       			type : "post",
    	       			//dataType : "json",
    	    			}).done(
    	    				function(res) {
    	    		  			if(res>0){
    	    		  				$.ajax({
    	    		  	        		url : '${pageContext.request.contextPath }/system/editPsd.do',
    	    		  	       			data : {"newPsd":newPsd},
    	    		  	       			type : "post",
    	    		  	       			//dataType : "json",
    	    		  	    			}).done(
    	    		  	    				function(res) {
    	    		  	    		  			if(res>0){
    	    		  	    		  				toastr.success('修改成功！');
	    	    		  	    		  			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	    	    					  					//关闭模态框
	    	    					  					// 父页面刷新
	    	    		  	    	                    window.location.href="${pageContext.request.contextPath }/login/pcloginOut.do"
	    	    					  				},2000);
    	    		  	    		  			}else{
    	    		  	    		  				toastr.error('修改失败！');
    	    		  	    		  				$("#subform").attr("disabled",false);
    	    		  	    		  			}
    	    		  	    				}
    	    		  	    			)
    	    		  			}else{
    	    		  				toastr.error('原密码错误！');
    	    		  				$("#subform").attr("disabled",false);
    	    		  			}
    	    				}
    	    			)
    	        	
    	    		return false;
    	        });
    		})
    	}
    	/* 更换条码 */
    	function editCode(){
    		layui.use(['table','layer','form'], function(){
    			var table = layui.table;
    			var layer = layui.layer;
    			var form = layui.form;
    			layer.open({
	                type: 1 				//Page层类型
	                ,area: ['515px', ''] 	//宽  高
	                ,title: '调账功能'
	                ,shade: 0.6 			//遮罩透明度
	                ,maxmin: true 			//允许全屏最小化
	                ,anim: 1 				//0-6的动画形式，-1不开启
	                ,content:$('#formDiv01')
    			});
    			//修改
    	        form.on('submit(editform)', function (data) {
    	        	$("#subform").attr("disabled",true);
    	        	//之前的条码
    	        	var beforeCode = $.trim($("#beforeCode").val());
    	        	//新条码
    	        	var newCode = $.trim($("#newCode").val());
    	        	//品号
    	        	var mcode = $.trim($("#mcode").val());
    	        	//库位
    	        	var location = $.trim($("#location").val());
    	        	$.ajax({
    	        		url : '${pageContext.request.contextPath }/car/queryCodeExist.do',
    	       			data : {"beforeCode":beforeCode,"mcode":mcode,"location":location},
    	       			type : "post",
    	       			//dataType : "json",
    	    		}).done(
   	    				function(result) {
   	    					
   	    				if(result > 0){
   	    		  				$.ajax({
   	    		  	        		url : '${pageContext.request.contextPath }/car/editCode.do',
   	    		  	       			data : {"beforeCode":beforeCode, "newCode":newCode},
   	    		  	       			type : "post",
   	    		  	       			//dataType : "json",
   	    		  	    		}).done(
	   		  	    				function(res) {
		   		  	    				if(res == -1) {
		   	   	    						toastr.warning('对不起，新条码已存在');
		   	   	    		  				$("#subBtn").attr("disabled",false);
		   	   	    					}else if(res > 0){
	   		  	    		  				toastr.success('修改条码成功！');
		   		  	    		  			setTimeout(function () {
		   		  	    		  				layer.closeAll();
			   		  	    			    }, 100);
	   		  	    		  			}else{
	   		  	    		  				toastr.error('修改条码失败！');
	   		  	    		  				$("#subBtn").attr("disabled",false);
	   		  	    		  			}
	   		  	    				}
   	    		  	    		)
   	    		  			} else {
   	    		  				toastr.error('原条码不存在或条码、SAP/QAD、库位对应错误！');
   	    		  				$("#subBtn").attr("disabled",false);
   	    		  			}
   	    				}
   	    			)
    	    		return false;
    	        });
    		})
    	}
    	
    	$("#newPsd").change(function(){
    		var newPsd = $("#newPsd").val();
    		var affirmPas = $("#affirmPas").val();
    		if(newPsd!=affirmPas){
    			toastr.warning('两次输入的密码不一致！');
    			$("#affirmPas").val("");
    		}
    	})
    	$("#affirmPas").change(function(){
    		var newPsd = $("#newPsd").val();
    		var affirmPas = $("#affirmPas").val();
    		if(newPsd!=affirmPas){
    			toastr.warning('两次输入的密码不一致！');
    			$("#affirmPas").val("");
    		}
    	})
    	toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    </script>
</body>
</html>
