<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>权限管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
	<span class="layui-breadcrumb">
 			<a href="">首页</a>
		<a>
			<cite>权限管理</cite>
		</a>
	</span>
   </div>
<div style="display: inline-block; width: 450px; height: 650px; padding: 10px; border: 1px solid #ddd; overflow: auto;margin-left: 35%;background-color: #fff;">
	<ul id="demo1" style="padding-left: 2em;font-size: 18px;">
		<li style="text-align: center;margin-bottom: 1em;"><a href="###" id="" class="add" style="font-size: 16px;padding-left: 2em;color: #08c;">添加一级操作</a></li>
	</ul>
</div>

<div id="formDiv" hidden>
	<div class="box">
		<form class="layui-form layui-card-body" id="addform">
			<input hidden id="id" name="id">
			<input hidden id="pid" name="pid">
			<input hidden id="permissionType" name="permissionType" value="m">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">权限名称</label>
					<div class="layui-input-inline">
						<input lay-verify="permissionName" class="layui-input" type="text" id="permissionName" name="permissionName" style="width: 375px;">
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">操作URL</label>
					<div class="layui-input-inline">
						<input class="layui-input" type="text" id="url" name="url" style="width: 375px;">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">权限序列</label>
					<div class="layui-input-inline">
						<input lay-verify="sort" class="layui-input" type="number" id="sort" name="sort" style="width: 375px;">
					</div>
				</div>
				<div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn layui-btn-primary" id="subform" lay-submit lay-filter="addform">确定</button>
					<button class="layui-btn layui-btn-primary" style="margin-left:240px">取消</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
//Demo
layui.use(['table','layer','upload','form','tree'], function(){
  var layer = layui.layer
  ,$ = layui.jquery
  ,form = layui.form;
  var strWhere = "status = 0 and isdel = 0 and pid = 0 and ";
  var nodes = new Array();
  jQuery(function($) {
	$.ajax({
		type:'post',
		url:'${pageContext.request.contextPath }/system/selectAllMenuList.do',
		data:{"strWhere":strWhere},
		success:function(data){
			var list = eval("("+data+")");
			for (var i = 0; i < list.length; i++) {
				var list2 = list[i].mList;
				var cNodes = new Array();
				for (var j = 0; j < list2.length; j++) {
					cNodes.push({name: list2[j].permissionName+'<div style="float: right;padding-right: 12%;">'+
						'<a href="###" id="childrenEdit_'+list2[j].id+'" class="edit" style="font-size: 14px;padding-left: 2em;color: #08c;">编辑</a>'+
						'<a href="###" id="childrenDel_'+list2[j].id+'" class="del" style="font-size: 14px;padding-left: 1em;color: #08c;">删除</a></div>',id: list2[j].id,sort: list[i].sort,url: list2[j].url});
				}
				if (i == 0) {
					nodes.push({name: list[i].permissionName+'<div style="float: right;padding-right: 12%;">'+
						'<a href="###" id="add_'+list[i].id+'" class="add" style="font-size: 14px;padding-left: 2em;color: #08c;">添加下级操作</a>'+
						'<a href="###" id="edit_'+list[i].id+'" class="edit" style="font-size: 14px;padding-left: 1em;color: #08c;">编辑</a>'+
						'<a href="###" id="del_'+list[i].id+'" class="del" style="font-size: 14px;padding-left: 1em;color: #08c;">删除</a></div>',
						id: list[i].id,sort: list[i].sort,spread: true,children: cNodes});
				} else {
					nodes.push({name: list[i].permissionName+'<div style="float: right;padding-right: 12%;">'+
						'<a href="###" id="add_'+list[i].id+'" class="add" style="font-size: 14px;padding-left: 2em;color: #08c;">添加下级操作</a>'+
						'<a href="###" id="edit_'+list[i].id+'" class="edit" style="font-size: 14px;padding-left: 1em;color: #08c;">编辑</a>'+
						'<a href="###" id="del_'+list[i].id+'" class="del" style="font-size: 14px;padding-left: 1em;color: #08c;">删除</a></div>',id: list[i].id,sort: list[i].sort,children: cNodes});
				}
				
			}
			layui.tree({
			    elem: '#demo1' //指定元素
			    ,target: '_blank' //是否新选项卡打开（比如节点返回href才有效）
			    ,nodes: nodes
			  });
			$('.add').on('click', function() {
				var id = this.id;
				layer.open({
				  type: 1 		//Page层类型
				  ,area: ['580px', ''] //宽  高
				  ,skin: 'alert-skin'
				  ,title: '新增权限'
				  ,shade: 0.6 	//遮罩透明度
				  ,shadeClose: true //点击遮罩关闭
				  ,maxmin: true //允许全屏最小化
				  ,anim: 1 		//0-6的动画形式，-1不开启
				  ,content: $('#formDiv')
				  ,success: function () {
					  document.getElementById("addform").reset();
					//回显表单数据
					$('#pid').val(id.split('_')[1]);
				  }	
				  ,end: function () {
				  	var formDiv = document.getElementById('formDiv');
				  	formDiv.style.display = '';
			  	  }
				});
			})
			$('.edit').on('click', function() {
				var id = this.id;
				var strWhere = "status = 0 and isdel = 0 and id = "+id.split('_')[1]+" and ";
				$.ajax({
    		    	type:'post',
    		    	url:'${pageContext.request.contextPath }/system/findPermissionByID.do',
    		    	data:{"strWhere":strWhere},
    		    	success:function(data){
    		    		if (data == 0) {
    		    			toastr.warning("菜单不存在！");
			        		return false;
						} else {
							var menuMana = eval("("+data+")");
							console.log(menuMana)
							layer.open({
							  type: 1 		//Page层类型
							  ,area: ['580px', ''] //宽  高
							  ,skin: 'alert-skin'
							  ,title: '修改权限'
							  ,shade: 0.6 	//遮罩透明度
							  ,shadeClose: true //点击遮罩关闭
							  ,maxmin: true //允许全屏最小化
							  ,anim: 1 		//0-6的动画形式，-1不开启
							  ,content: $('#formDiv')
							  ,success: function () {
								//回显表单数据
								$('#id').val(menuMana[0].id);
								$('#pid').val(menuMana[0].pid);
								$('#permissionName').val(menuMana[0].permissionName);
								$('#url').val(menuMana[0].url);
								$('#sort').val(menuMana[0].sort);
							  }	
							  ,end: function () {
							  	var formDiv = document.getElementById('formDiv');
							  	formDiv.style.display = '';
						  	  }
							});
						}
    		    	}
				});
			})
			$('.del').on('click', function() {
				var id = this.id;
				var strWhere = "id = "+id.split('_')[1];
				layer.confirm('确定删除吗？', function(index){
					$.ajax({
	    		    	type:'post',
	    		    	url:'${pageContext.request.contextPath }/system/deletePermissionById.do',
	    		    	data:{"strWhere":strWhere,"id":id.split('_')[1]},
	    		    	success:function(data){
	    		    		if (data > 0) {
	    		    			toastr.success("删除成功！");
	    		    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	    		  					//关闭模态框
	    		  					// 父页面刷新
	    		  					window.location.reload();  
	    		  				},2000);
							} else {
								toastr.error("删除失败！");
							}
	    		    	},
	    		    	
					});
	    		    layer.close(index);
	    		});
			})
		}
	});
  });
  /**
   * 表单校验
   */
   form.verify({
  	//value：表单的值、item：表单的DOM对象
  	name: function(value){
  		if(value == ''){
  			return '权限名称不能为空';
  		}
  	},
  	sort: function(value){
  		if(value == ''){
  			return '权限序列不能为空';
  		}
  	},
   })
   /**
    * 通用表单提交(AJAX方式)（新增）
    */
   form.on('submit(addform)', function (data) {
	   $("#subform").attr("disabled",true);
	   	$.ajax({
	  		url : '${pageContext.request.contextPath }/system/savePermission.do',
	  		data : $('#addform').serialize(),
	  		type : "post",
	  		//dataType : "json",
	  	}).done(
	  		function(res) {
	  			var list = res.split('_');
	  			if (parseInt(list[0]) > 0) {
	  				if(list[1].indexOf('add') != -1){
	  					toastr.success('新增成功！');
	  				}
	  				if(list[1].indexOf('edit') != -1){
	  					toastr.success('修改成功！');
	  				}
	  				setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
	  					//关闭模态框
	  					// 父页面刷新
	  					window.location.reload();  
	  				},2000);
	  			}
	  		}
	  	).fail(
	  		function(res) {
	  			var data = res.responseText;
	  			var list = data.split('_');
				if(list[1].indexOf('add') != -1){
					layer.close(1);
					toastr.error('新增失败！');
					$("#subform").attr("disabled",false);
				}
				if(list[1].indexOf('edit') != -1){
					layer.close(1);
					toastr.error('修改失败！');
					$("#subform").attr("disabled",false);
				}
	  		}
	  	)
		return false;
   });
  
});
toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>

</body>
</html>