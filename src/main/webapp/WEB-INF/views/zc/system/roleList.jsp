<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>角色管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" href="${pageContext.request.contextPath }/assets/ztree/css/demo.css" type="text/css">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<!-- ztree的js和css -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath }/assets/ztree/css/zTreeStyle/zTreeStyle.css">
	<script type="text/javascript" src="${pageContext.request.contextPath }/assets/ztree/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/assets/ztree/js/jquery.ztree.excheck.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath }/assets/ztree/js/jquery.ztree.exedit.js"></script>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
	<span class="layui-breadcrumb">
 			<a href="">首页</a>
		<a>
			<cite>角色管理</cite>
		</a>
	</span>
   </div>
<div class="x-body">
	<div class="layui-row">
		<div class="layui-form layui-col-md12 x-so">
			<input type="text" name="keyword" id="keyword"  placeholder="请输入角色名称" autocomplete="off" class="layui-input">
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</div>
	</div>
	<xblock>
		<button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-delete"></i>新增</button>
	</xblock>
	<table class="layui-hide" id="test" lay-filter="test"></table>
	<script type="text/html" id="barDemo">
		<a class="layui-btn layui-btn-xs" lay-event="authorize"><i class="layui-icon layui-icon-edit">分配权限</i></a>
		<a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit">编辑</i></a>
	</script>
</div>
<div id="formDiv" hidden>
	<div class="box">
		<form class="layui-form layui-card-body" id="addform">
			<input hidden id="id" name="id">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">角色名称</label>
					<div class="layui-input-inline">
						<input lay-verify="roleName" class="layui-input" lay-filter="roleName" type="text" id="roleName" name="roleName" style="width: 375px">
						<span class="xing" style="margin-left: 390px;font-size: 24px;"></span>
					</div>
				</div>
				<!-- <div class="layui-inline" style="float: right;margin-right: 3em;margin-top: 10px;">
					<font style="color:red; font-size: 24px;">*</font>
				</div> -->
			</div>
			<!-- <div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">角色类型</label>
					<div class="layui-input-inline" style="width: 375px;">
						<select lay-verify="remark" id="remark" name="remark" lay-filter="remark">
							<option>超级管理员</option>
							<option>管理员</option>
							<option>库管</option>
							<option>仓库人员</option>
						</select>
					</div>
				</div>
			</div> -->
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">角色描述</label>
					<div class="layui-input-inline">
						<textarea class="layui-textarea" id="description" name="description" style="width: 375px;"></textarea>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn layui-btn-primary" lay-submit lay-filter="addform" id="subform">确定</button>
					<button class="layui-btn layui-btn-primary" style="margin-left:240px">取消</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- 授权弹框 -->
<div id="formDiv2" hidden>
	<div class="box">
		<form class="layui-form layui-card-body" id="addform2">
			<input id="roleId" name="roleId" hidden>  	
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">权限</label>
					<div class="layui-input-inline">
						<ul class="ztree" id="ztree"></ul>
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn layui-btn-primary" lay-submit lay-filter="subform" id="subform1">确定</button>
					<button class="layui-btn layui-btn-primary" style="margin-left:100px">取消</button>
				</div>
			</div>
		</form>
	</div>
</div>

<script>
layui.use(['table','layer','upload','form','laydate'], function(){
	var table = layui.table;
	var laydate = layui.laydate;
	var layer = layui.layer,
    form = layui.form;

	var $ = layui.jquery, active = {
			reload:function () {
				var keyword = $("#keyword").val();
				var strWhere = "status = 0 and isdel = 0 and roleName like '%"+keyword+"%' and ";
				table.reload('contenttable',{
					method:'get',
					where:{"strWhere": strWhere}
				});
			}
		}
	$('.layui-btn').on('click', function(){
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });
	table.render({
	    elem: '#test'
	    ,url:'${pageContext.request.contextPath }/system/selectSysRoleList.do'
	    ,title: '角色数据表'
	    ,id :'contenttable'
	    ,limits:[10,20,30]
	    ,toolbar: '#toolbar'
	    ,cols: [[
	      {type: 'checkbox', fixed: 'left'}
	      ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
	      ,{field:'roleName', title:'角色名称'}
	      /* ,{field:'remark', title:'角色类型'} */
	      ,{field:'description', title:'角色说明'}
	      ,{fixed: 'right', title:'操作', toolbar: '#barDemo'}
	    ]]
	    ,page: true
	    ,done:function(){
	    	$('th').css({
                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
            })
	    }
	});

	//头事件
    $("#add").click(function(){
   		layer.open({
		  type: 1 		//Page层类型
		  ,area: ['585px', ''] //宽  高
		  ,skin: 'alert-skin'
		  ,title: '新增角色'
		  ,shade: 0.6 	//遮罩透明度
		  ,shadeClose: true //点击遮罩关闭
		  ,maxmin: true //允许全屏最小化
		  ,anim: 1 		//0-6的动画形式，-1不开启
		  ,content: $('#formDiv')
		  ,success: function () {
			  document.getElementById("addform").reset();
		  }
		  ,end: function () {
			  var formDiv = document.getElementById('formDiv');
			  formDiv.style.display = '';
		  }
		});
    });
  	//监听复选框事件
	table.on('checkbox(test)',function(obj){
		if(obj.checked == true && obj.type == 'all'){ 
			//点击全选 
			$('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click'); 
		}else if(obj.checked == false && obj.type == 'all'){ 
			//点击全不选 
			$('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click'); 
		}else if(obj.checked == true && obj.type == 'one'){ 
			//点击单行
			if(obj.checked == true){ 
				obj.tr.addClass('layui-table-click'); 
			}else{ 
				obj.tr.removeClass('layui-table-click'); 
			} 
		}else if(obj.checked == false && obj.type == 'one'){ 
			//点击全选之后点击单行 
			if(obj.tr.hasClass('layui-table-click')){
				obj.tr.removeClass('layui-table-click'); 
			} 
		}
	})
	//监听行工具事件
	table.on('tool(test)', function(obj){
		var data = obj.data;
		if(obj.event === 'del'){
		  layer.confirm('确定删除吗？', function(index){
		    var id = obj.data.id;
		    $.ajax({
		    	type:'post',
		    	url:'${pageContext.request.contextPath }/system/deleteRoleById.do',
		    	data:{"id":id},
		    	success:function(data){
		    		if(data>0){
		    			toastr.success("删除成功！");
		    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
							//关闭模态框
							// 父页面刷新
							window.location.reload();
						},2000);
		    		}else{
		    			toastr.warning("删除失败！");
		    		}
		    	}

		    })
		    layer.close(index);
		  });
		} else if(obj.event === 'edit'){
			layer.open({
				  type: 1 		//Page层类型
				  ,area: ['580px', ''] //宽  高
				  ,skin: 'alert-skin'
				  ,title: '修改角色'
				  ,shade: 0.6 	//遮罩透明度
				  ,shadeClose: true //点击遮罩关闭
				  ,maxmin: true //允许全屏最小化
				  ,anim: 1 		//0-6的动画形式，-1不开启
				  ,content: $('#formDiv')
				  ,success: function () {
					//回显表单数据
					for(var i=0;i<Object.entries(data).length;i++) {
						var id = '#' + Object.entries(data)[i][0];
						var text = Object.entries(data)[i][1];
						$(id).val(text);
					}
					//$("#remark").val(data.remark)
					form.render();
				  }	
				  ,end: function () {
				  	var formDiv = document.getElementById('formDiv');
				  	formDiv.style.display = '';
			  	  }
				});
		} else if(obj.event === 'authorize'){
			var roleId = obj.data.id;
			$("#roleId").val(roleId);
			layer.open({
				type: 1 		//Page层类型
				,area: ['460px', ''] //宽  高
				,skin: 'alert-skin'
				,title: '角色授权'
			  	,shade: 0.6 	//遮罩透明度
			  	,shadeClose: true //点击遮罩关闭
			  	,maxmin: true //允许全屏最小化
			  	,anim: 1 		//0-6的动画形式，-1不开启
			  	,content: $('#formDiv2')
		  	 	,success: function(data){
		  			successOther(laydate,data);
		  			form.render();
				}
				,end: function () {
				  	var formDiv = document.getElementById('formDiv2');
			  		formDiv.style.display = '';
		  	  	}
			});
		}
	});
	/**
     * 表单校验
     */
    form.verify({
    	//value：表单的值、item：表单的DOM对象
    	roleName: function(value, item){
    		if(value == ''){
    			return '角色名称不能为空';
    		}
    	}
    });
    /**
     * 通用表单提交(AJAX方式)（新增）
     */
    form.on('submit(addform)', function (data) {
    	$("#subform").attr("disabled",true);
    	$.ajax({
    		url : '${pageContext.request.contextPath }/system/saveSysRole.do',
   			data : $('#addform').serialize(),
   			type : "post",
   			//dataType : "json",
			}).done(
			  		function(res) {
			  			var list = res.split('_');
			  			if (parseInt(list[0]) > 0) {
			  				if(list[1].indexOf('add') != -1){
			  					toastr.success('新增成功！');
			  				}
			  				if(list[1].indexOf('edit') != -1){
			  					toastr.success('修改成功！');
			  				}
			  				setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
			  					//关闭模态框
			  					// 父页面刷新
			  					window.location.reload();  
			  				},2000);
			  			}
			  		}
			  	).fail(
			  		function(res) {
			  			var data = res.responseText;
			  			var list = data.split('_');
						if(list[1].indexOf('add') != -1){
							layer.close(1);
							toastr.error('新增失败！');
							$("#subform").attr("disabled",false);
						}
						if(list[1].indexOf('edit') != -1){
							layer.close(1);
							toastr.error('修改失败！');
							$("#subform").attr("disabled",false);
						}
			  		}
			  	)
			return false;
    });
    /**
     * 通用表单提交(AJAX方式)
     */
    form.on('submit(subform)', function (data) {
    	$("#subform1").attr("disabled",true);
    	var ztree = getCheck("ztree");
    	var roleId = $('#roleId').val();
    	if (ztree == '') {
			toastr.warning('请选择要分配的权限！');
			return false;
		}
    	$.ajax({
    		url : '${pageContext.request.contextPath }/system/saveRoleOperation.do',
   			data : {"roleId": roleId, "ztree":ztree},
   			type : "post",
   			dataType : "json",
		}).done(
				function(res) {
					if (res > 0) {
						toastr.success('授权成功！');
						setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
		  					//关闭模态框
		  					// 父页面刷新
		  					window.location.reload();  
		  				},2000);
					} else {
						toastr.error('授权失败！');
						layer.close();
						$("#subform1").attr("disabled",false);
					}
				}
			).fail(
				function(res) {
					toastr.error('授权失败！');
					$("#subform1").attr("disabled",false);
				}
			)
			return false;
    });
});

$(document).on('click', '#clear', function() {
	//layer.msg('响应点击事件');
	// 清空全部选中的节点
	var treeObj = $.fn.zTree.getZTreeObj("ztree");
	treeObj.checkAllNodes(false)
});
toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>

<script type="text/javascript">
var setting = {
	    data:{//表示tree的数据格式
	        simpleData:{
	            enable:true,//表示使用简单数据模式
	            idKey:"id",//设置之后id为在简单数据模式中的父子节点关联的桥梁
	            pidKey:"pId",//设置之后pid为在简单数据模式中的父子节点关联的桥梁和id互相对应
	            rootId:"null"//pid为null的表示根节点
	        }
	    },
	    view:{//表示tree的显示状态
	        selectMulti:false//表示禁止多选
	    },
	    check:{//表示tree的节点在点击时的相关设置
	        enable:true,//是否显示radio/checkbox
	        chkStyle:"checkbox",//值为checkbox或者radio表示
	        checkboxType:{p:"",s:""},//表示父子节点的联动效果
	        radioType:"level"//设置tree的分组
	    }, 
	    /* callback:{//表示tree的一些事件处理函数
	        onClick:handlerClick,
	    } */
	}
/* 弹出框后加载信息 */
function successOther(laydate,data){
	ztree(data);
}
function ztree(data){	
	//第一步，先获取所有的权限信息，用树结构进行展示
	var treeData = [];
	var strWhere = "status = 0 and isdel = 0 and pid = 0 and ";
	$.ajax({
		type:'post',
		url:'${pageContext.request.contextPath }/system/selectAllMenuList.do',
		data:{"strWhere": strWhere},
		success:function(data){
			var list = eval("("+data+")");
			for (var i = 0; i < list.length; i++) {
				var list2 = list[i].mList;
				var cNodes = new Array();
				for (var j = 0; j < list2.length; j++) {
					cNodes.push({name: list2[j].permissionName,id: list2[j].id,pId: list[i].id});
				}
				treeData.push({name: list[i].permissionName,id: list[i].id,pId: null,children: cNodes});
				
				
			}
		  	$.fn.zTree.init($("#ztree"),setting,treeData);
		 	// 第二步，根据要授权的角色的ID，获取它已有的权限
			if(data != null && data != ""){
				 var roleId = $('#roleId').val();
				  $.ajax({
		            type: "post",
		            data: {"roleId":roleId},
		            url: "${pageContext.request.contextPath }/system/selectAllRoleOperation.do",
		            dataType: 'JSON',
		            async: false,
		            success: function (arr) {
				        for(var i=0;i<arr.length;i++){ 
							var zTree_Menu = $.fn.zTree.getZTreeObj("ztree");
							//console.log(zTree_Menu);
							var node = zTree_Menu.getNodeByParam("id",arr[i].permissionId,null);
							//console.log(node)
							zTree_Menu.checkNode(node,true,true);//指定选中ID的节点
							zTree_Menu.expandNode(node, true, false);//指定选中ID节点展开
				      	} 
		         	}
				}); 
			}
		},
		error : function(msg) {
		    alert('树加载异常!');
		}
	
	});
}
function getCheck(id){
	//在提交表单之前，使用ztree提供的方式获取选中的节点
    var treeObj = $.fn.zTree.getZTreeObj(id);//获得ztree对象
    var nodes = treeObj.getCheckedNodes(true);//在提交表单之前将选中的checkbox收集
    var array = new Array();
    for(var i=0;i<nodes.length;i++){
        var id = nodes[i].id;
        array.push(id);
    }
    var ids = array.join(",");
    return ids;
}

</script>
</body>
</html>