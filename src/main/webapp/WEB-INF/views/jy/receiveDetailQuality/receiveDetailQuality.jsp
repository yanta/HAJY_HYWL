<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>质检管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>质检管理</cite>
        </a>
      </span>
</div>
<!-- 上表 -->
<table class="layui-hide" id="tableDetails" lay-filter="tableDetails"></table>
<!-- 下表 -->
<table class="layui-hide" id="tableDetailsSub" lay-filter="tableDetailsSub"></table>
<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        //点击行checkbox选中
        $(document).on("click",".layui-table-body table.layui-table tbody tr", function () {
            var index = $(this).attr('data-index');
            var tableBox = $(this).parents('.layui-table-box');
            //存在固定列
            if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length>0) {
                tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
            } else {
                tableDiv = tableBox.find(".layui-table-body.layui-table-main");
            }
            var checkCell = tableDiv.find("tr[data-index=" + index + "]").find("td div.laytable-cell-checkbox div.layui-form-checkbox I");
            if (checkCell.length > 0) {
                checkCell.click();
            }
        });

        $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
            e.stopPropagation();
        });

        table.render({
            elem: '#tableDetails'
            ,url:'${pageContext.request.contextPath }/receiveDetail/queryReceiveDetailQualityCriteria.do'
            ,toolbar: '#toolbar'
            ,title: '质检明细表'
            ,id :'contenttableDetails'
            ,limits:[10,20,30]
            //,defaultToolbar:['filter', 'print']
            ,cols: [[
                {type: 'checkbox',fixed: 'left'},
                {field:'id', title:'序号', width: 50,type:'numbers', align:'center'},
                {field:'mid', title:'物料id', align:'center', hide:true},
                /* {field:'pici', title:'批次', align:'center',event: 'collapse',
                    templet: function(d) {
                        return '<div style="position: relative;'+'padding: 0 10px 0 20px;">' + d.pici + '<i style="left: 0px;" lay-tips="展开" class="layui-icon layui-colla-icon layui-icon-right"></i></div>'
                    }
                }, */
                {field:'pici', title:'批次', align:'center'},
                {field:'materiel_name', title:'物料名称', align:'center', width: 250},
                /* {field:'materiel_name', title:'物料名称', align:'center', width: 250,
                   event: 'collapse',
                    templet: function(d) {
                        //return '<div style="position: relative;'+'padding: 0 10px 0 20px;">' + d.materiel_name + '<i style="left: 0px;" lay-tips="展开" class="layui-icon layui-colla-icon layui-icon-right"></i></div>'
                        return d.materiel_name;
                    }
                }, */
                //{field:'customer_name', title:'供应商名称', align:'center'},
                {field:'materiel_num', title:'产品码', align:'center'},
                {field:'brevity_num', title:'简码', align:'center'},
                //{field:'materiel_size', title:'物料规格', align:'center'},
                //{field:'materiel_properties', title:'物料型号', align:'center'},
                //{field:'unit', title:'单位', align:'center', width: 100},
                {field:'packing_quantity', title:'包装数量', width: 100, align:'center'},
                {field:'is_check', title:'是否质检', width: 100, align:'center'
                    ,templet: function(d){
                        var value = d.is_check;
                        if(value == 0){
                            return "是";
                        } else if (value == 1) {
                            return "否";
                        }
                    }
                },
                //{field:'container_name', title:'容器名称', align:'center'},
                /* {field:'is_devanning', title:'是否拆箱', align:'center'
                    ,templet: function(d){
                        var value = d.is_devanning;
                        if(value == 0){
                            return "是";
                        } else if (value == 1) {
                            return "否";
                        }
                    }
                }, */
                //{field:'devanning_num', title:'拆箱数', align:'center'},
                //{field:'outgoing_frequency', title:'出库频率', align:'center'},
                /* {
                    field: 'barcode_type',
                    title: '条码类型',
                    align:'center',
                    templet: function(d){
                        var value = d.barcode_type;
                        if(value == 0){
                            return "精准码";
                        } else if (value == 1) {
                            return "产品码";
                        } else {
                            return "简码";
                        }
                    }
                }, */
                //{field:'sendNum', title:'发货箱数(箱)', align:'center', width: 110},
                //{field:'singleNum', title:'发货单数(个)', align:'center', width: 110},
                /*{field:'totalNum', title:'发货总数(个)', align:'center', width: 110, templet: function(d){
                    if (d.barcode_type == 0) {
                        if (d.singleNum > 0) {
                            return ((d.sendNum - 1) * d.packing_quantity) + d.singleNum;
                        } else {
                            return d.sendNum * d.packing_quantity;
                        }
                    } else {
                        return d.sendNum * d.packing_quantity + d.singleNum;
                    }

                }},*/
                //{field:'receive_num', title:'收货箱数(箱)', align:'center', width: 110},
                //{field:'single_num', title:'收货单数(个)', align:'center', width: 110},
                {field:'codeNum', title:'收货总数(个)', align:'center', width: 110},
                {field:'ng_num', title:'不良品个数(个)', align:'center', width: 110}
                //{field:'rcode', title:'二维码', align:'center', templet:'<div><img style="height:100%; width: 50px"; src="http://localhost:8000/rcode/receive_rcode/{{ d.rcode }}"></div>'}
            ]]
            ,page: true
            ,done : function(res, curr, count){
            	//merge(res);
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });
        /* 监听表格的展开行事件 */
        /* table.on('tool(tableDetails)', function(obj){
        	if (obj.event === 'collapse') {
        		//当前行
				var trObj = layui.$(this).parent('tr'); 
				//开启手风琴，那么在进行折叠操作时，始终只会展现当前展开的表格。
				var accordion = true 
				var content = '<table></table>' //内容
				//表格行折叠方法
				collapseTable({
					elem: trObj,
					accordion: accordion,
					content: content,
					success: function (trObjChildren, index) { //成功回调函数
						//trObjChildren 展开tr层DOM
						//index 当前层索引
						trObjChildren.find('table').attr("id", index);
                        //===================================start===================================
						table.render({
							elem: "#" + index
                            ,url:'${pageContext.request.contextPath }/receiveDetail/queryWorkloadByreceiveDetailQualityId.do?receiveDetailQuality_id=' + obj.data.id
                            ,title: 'machineList'
                            //,limits:[10,20,30]
                            ,cols:
								[[
                                    {field:'id', title:'序号', sort: true, width: 60, type:'numbers', align:'center'},
                                    {field:'workman',title:'工作人员', align:'center'},
                                    {field:'good_num',title:'操作良品数量', align:'center'},
                                    {field:'ngood_num',title:'操作不良品数量', align:'center'}
                                ]]
                            ,page: false
						});
						//===================================end===================================
					}
				});
			}
        }); */
        
      	//表格行折叠方法
        /* function collapseTable(options) {
            var trObj = options.elem;
            if (!trObj) {
                return;
            }
            var accordion = options.accordion,
                success = options.success,
                content = options.content || '';
            var tableView = trObj.parents('.layui-table-view'); //当前表格视图
            var id = tableView.attr('lay-id'); //当前表格标识
            var index = trObj.data('index'); //当前行索引
            var leftTr = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + index + '"]'); //左侧当前固定行
            var rightTr = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + index + '"]'); //右侧当前固定行
            var colspan = trObj.find('td').length; //获取合并长度
            var trObjChildren = trObj.next(); //展开行Dom
            var indexChildren = id + '-' + index + '-children'; //展开行索引
            var leftTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + indexChildren + '"]'); //左侧展开固定行
            var rightTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + indexChildren + '"]'); //右侧展开固定行
            var lw = leftTr.width() + 15; //左宽
            var rw = rightTr.width() + 15; //右宽
            //不存在就创建展开行
            if (trObjChildren.data('index') != indexChildren) {
                //装载HTML元素
                var tr = '<tr data-index="' + indexChildren + '"><td colspan="' + colspan + '"><div style="height: auto;padding-left:' + lw + 'px;padding-right:' + rw + 'px" class="layui-table-cell">' + content + '</div></td></tr>';
                trObjChildren = trObj.after(tr).next().hide(); //隐藏展开行
                var fixTr = '<tr data-index="' + indexChildren + '"></tr>';//固定行
                leftTrChildren = leftTr.after(fixTr).next().hide(); //左固定
                rightTrChildren = rightTr.after(fixTr).next().hide(); //右固定
            }
            //展开|折叠箭头图标
            trObj.find('td[lay-event="collapse"] i.layui-colla-icon').toggleClass("layui-icon-right layui-icon-down");
            //显示|隐藏展开行
            trObjChildren.toggle();
            //开启手风琴折叠和折叠箭头
            if (accordion) {
                trObj.siblings().find('td[lay-event="collapse"] i.layui-colla-icon').removeClass("layui-icon-down").addClass("layui-icon-right");
                trObjChildren.siblings('[data-index$="-children"]').hide(); //展开
                rightTrChildren.siblings('[data-index$="-children"]').hide(); //左固定
                leftTrChildren.siblings('[data-index$="-children"]').hide(); //右固定
            }
            success(trObjChildren, indexChildren); //回调函数
            heightChildren = trObjChildren.height(); //展开高度固定
            rightTrChildren.height(heightChildren + 115).toggle(); //左固定
            leftTrChildren.height(heightChildren + 115).toggle(); //右固定
        } */
      	//出库单行单击事件
        table.on('row(tableDetails)', function(obj) {
        	//$("#subTable").show();
            var data = obj.data;
            console.log(obj.data.id);
            table.render({
            	elem: '#tableDetailsSub'
                ,url:'${pageContext.request.contextPath }/receiveDetail/queryWorkloadByreceiveDetailQualityId.do?receiveDetailQuality_id=' + obj.data.id
                ,title: 'machineList'
                //,limits:[10,20,30]
                ,cols:
					[[
                        {field:'id', title:'序号', sort: true, width: 60, type:'numbers', align:'center'},
                        {field:'workman',title:'工作人员', align:'center'},
                        {field:'good_num',title:'操作良品数量', align:'center'},
                        {field:'ngood_num',title:'操作不良品数量', align:'center'}
                    ]]
                ,page: false
			});
            /* table.render({
                elem: '#tableDetailsSub'
                ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById.do?keyword01='+data.id
                ,toolbar: '#toolbar'
                ,title: '出库单详细表'
                ,id :'contenttable1'
                ,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
                ,cols: [[
	   				  	{type: 'checkbox', fixed: 'left', },
	   				  	{field:'customer_name', title:'供应商名称', hide:true},
	   				  	{field:'materiel_name', title:'物料名称', align:'center'},
	   				 	{field:'materiel_num', title:'产品码', align:'center'},
		   				{field:'brevity_num', title:'物料简码', align:'center'},
		   				{field:'materiel_size', title:'物料规格', align:'center'},
		   				{field:'materiel_properties', title:'物料型号', align:'center'},
	   				  	{field:'material_quantity', title:'物料数量', align:'center'},
	   				  	{field:'actual_quantity', title:'实际数量', align:'center'},
    			]]
                ,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
            }); */
        });
    });
    /* function merge(res) {
        var data = res.data;
        var mergeIndex = 0;//定位需要添加合并属性的行数
        var mark = 1; //这里涉及到简单的运算，mark是计算每次需要合并的格子数
        var columsName = ['pici'];//需要合并的列名称
        var columsIndex = [3];//需要合并的列索引值
        for (var k = 0; k < columsName.length; k++) { //这里循环所有要合并的列
            var trArr = $(".layui-table-body>.layui-table").find("tr");//所有行
                for (var i = 1; i < res.data.length; i++) { //这里循环表格当前的数据
                    var tdCurArr = trArr.eq(i).find("td").eq(columsIndex[k]);//获取当前行的当前列
                    var tdPreArr = trArr.eq(mergeIndex).find("td").eq(columsIndex[k]);//获取相同列的第一列
                    if (data[i].pici === data[i-1].pici) { //后一行的值与前一行的值做比较，相同就需要合并
                        mark += 1;
                        tdPreArr.each(function () {//相同列的第一列增加rowspan属性
                            $(this).attr("rowspan", mark);
                        });
                        tdCurArr.each(function () {//当前行隐藏
                            $(this).css("display", "none");
                        });
                    }else {
                        mergeIndex = i;
                        mark = 1;//一旦前后两行的值不一样了，那么需要合并的格子数mark就需要重新计算
                    }
                }
            mergeIndex = 0;
            mark = 1;
        }
    } */
</script>
</body>
</html>

