<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>入库记录</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
	<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>入库记录</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
    <div style="margin-bottom: 18px">
    	<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" name="keyword" id="keyword1" placeholder="请输入产品码" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>
					<%--<td>
						<input class="layui-input" name="keyword2" id="keyword2" placeholder="请输入简称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
					</td>--%>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
    </div>  
    
    <!-- 详情框 -->
	<div id="detailDiv" hidden>
		<table class="layui-hide" id="tableDetails" lay-filter="tableDetails"></table>
	</div>
	
	
<script type="text/html" id="rowToolbar">
	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="detail"><i class="layui-icon layui-icon-search"></i>详细</a>
</script>
<script type="text/javascript">
	layui.use(['table','layer','upload','form','laydate'], function(){
		var table = layui.table;
		var layer = layui.layer;
		var form = layui.form;
		var laydate = layui.laydate;
		
		var $ = layui.jquery, active = {
			reload:function () {
				var keyword01 = $("#keyword1").val();
				//var keyword02 = $("#keyword2").val();
				table.reload('contenttable',{
					method:'get',
					where:{
						"keyword01":keyword01
						//,"keyword2":keyword2
					}
				});
			}
		}
		
		//三级联动
		form.on('select(warehouseId)', function(data){
			//通过仓库id查询库区列表
			var strWhere = 'warehouse_id = '+ data.value + 'and'
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectWarehouseRegionListByWarehouseId.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					$("#regionId").find("option").not(":first").remove();
					for(var i = 0;i < res.length;i++){
						$("#regionId").append('<option value="'+res[i].id+'">'+res[i].region_name+'</option>');
					}
					form.render();
				}
			})
		}); 
		form.on('select(regionId)', function(data){
			//通过库区id查询库位列表
			var strWhere = 'region_id = '+ data.value + 'and'
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectWarehouseRegionLocationListByRegionId.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					$("#locationId").find("option").not(":first").remove();
					for(var i = 0;i < res.length;i++){
						$("#locationId").append('<option value="'+res[i].id+'">'+res[i].location_name+'</option>');
					}
					form.render();
				}
			})
		}); 
		
		//三级联动(2)
		form.on('select(warehouseId2)', function(data){
			//通过仓库id查询库区列表
			var strWhere = 'warehouse_id = '+ data.value + 'and'
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectWarehouseRegionListByWarehouseId.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					$("#regionId2").find("option").not(":first").remove();
					for(var i = 0;i < res.length;i++){
						$("#regionId2").append('<option value="'+res[i].id+'">'+res[i].region_name+'</option>');
					}
					form.render();
				}
			})
		}); 
		form.on('select(regionId2)', function(data){
			//通过库区id查询库位列表
			var strWhere = 'region_id = '+ data.value + 'and'
			$.ajax({
				type:'post'
				,url:'${pageContext.request.contextPath }/inventoryManagement/selectWarehouseRegionLocationListByRegionId.do'
				,data:{'strWhere':strWhere}
				,dataType:'json'
				,success:function(res){
					$("#locationId2").find("option").not(":first").remove();
					for(var i = 0;i < res.length;i++){
						$("#locationId2").append('<option value="'+res[i].id+'">'+res[i].location_name+'</option>');
					}
					form.render();
				}
			})
		}); 
		
		$('.layui-btn').on('click', function(){
			var type = $(this).data('type');
			active[type] ? active[type].call(this) : '';
		});
		
		
		table.render({
			elem: '#tableList'
			,url:'${pageContext.request.contextPath }/up/queryReceiveDetailInstockCriteria.do'
			,toolbar: '#toolbar'
			,title: 'machineList'
			,id :'contenttable'
			,limits:[10,20,30]
            ,defaultToolbar:['filter', 'print']
			,cols: [[
				{type: 'checkbox',fixed: 'left'},
				{field:'id', title:'序号', width: 50,type:'numbers', align:'center'},
				{field:'mid', title:'物料id', align:'center',hide:true},
				{field:'receive_detail_id', title:'收货单号详细id', align:'center', hide:true},
				//{field:'customer_name', title:'供应商名称', align:'center'},
				/*{field:'materiel_name', title:'物料名称', align:'center', event: 'collapse',
					templet: function(d) {
						return '<div style="position: relative;\n' + '    padding: 0 10px 0 20px;">' + d.materiel_name + '<i style="left: 0px;" lay-tips="展开" class="layui-icon layui-colla-icon layui-icon-right"></i></div>'
					}
				},*/
                {field:'materiel_name', title:'物料名称', align:'center'},
				{field:'materiel_num', title:'产品码', align:'center'},
				{field:'pici', title:'批次', align:'center'},
				{field:'materiel_size', title:'物料规格', align:'center'},
				{field:'materiel_properties', title:'物料型号', align:'center'},
				{field:'code_num', title:'入库数量（个）', align:'center'},
				//{field:'createDate', title:'入库时间', align:'center'}
			]]
			,page: true
			,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold'
                })
            }
		});

        //监听收货计划管理详细表的父子表事件
        table.on('tool(tableList)', function(obj){
            if (obj.event === 'collapse') {
                var trObj = layui.$(this).parent('tr'); //当前行
                var accordion = true //开启手风琴，那么在进行折叠操作时，始终只会展现当前展开的表格。
                var content = '<table></table>' //内容
                //表格行折叠方法
                collapseTable({
                    elem: trObj,
                    accordion: accordion,
                    content: content,
                    success: function (trObjChildren, index) { //成功回调函数
                        //trObjChildren 展开tr层DOM
                        //index 当前层索引
                        trObjChildren.find('table').attr("id", index);
                        /*===================================start===================================*/
                        table.render({
                            elem: "#" + index
                            ,url:'${pageContext.request.contextPath }/receiveDetail/queryCodemarkByReceiveDetailId.do?receive_detail_id=' + obj.data.receive_detail_id
                            ,title: 'machineList'
                            //,limits:[10,20,30]
                            ,cols:
                                [[
                                    {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                                    {field: 'code',title: '条码',align:'center'},
                                    //{field: 'sendCompany',title: '收货数量(个)',align:'center',},
                                    {field: 'mark',title: '单个数量(个)',align:'center', templet: function(d){
										if(d.mark == 0){
											return obj.data.packing_quantity;
										} else {
											return obj.data.single_num;
										}
									}}
                                ]]
                            ,page: false
                            /*,done : function(){
                                $('th').css({
                                    'background-color': '#009688', 'color': '#fff','font-weight':'bold'
                                })
                            }*/
                        });
                        /*===================================end===================================*/
                    }
                });
            }
        });

        //表格行折叠方法
        function collapseTable(options) {
            var trObj = options.elem;
            if (!trObj) {
                return;
            }
            var accordion = options.accordion,
                success = options.success,
                content = options.content || '';
            var tableView = trObj.parents('.layui-table-view'); //当前表格视图
            var id = tableView.attr('lay-id'); //当前表格标识
            var index = trObj.data('index'); //当前行索引
            var leftTr = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + index + '"]'); //左侧当前固定行
            var rightTr = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + index + '"]'); //右侧当前固定行
            var colspan = trObj.find('td').length; //获取合并长度
            var trObjChildren = trObj.next(); //展开行Dom
            var indexChildren = id + '-' + index + '-children'; //展开行索引
            var leftTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + indexChildren + '"]'); //左侧展开固定行
            var rightTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + indexChildren + '"]'); //右侧展开固定行
            var lw = leftTr.width() + 15; //左宽
            var rw = rightTr.width() + 15; //右宽
            //不存在就创建展开行
            if (trObjChildren.data('index') != indexChildren) {
                //装载HTML元素
                var tr = '<tr data-index="' + indexChildren + '"><td colspan="' + colspan + '"><div style="height: auto;padding-left:' + lw + 'px;padding-right:' + rw + 'px" class="layui-table-cell">' + content + '</div></td></tr>';
                trObjChildren = trObj.after(tr).next().hide(); //隐藏展开行
                var fixTr = '<tr data-index="' + indexChildren + '"></tr>';//固定行
                leftTrChildren = leftTr.after(fixTr).next().hide(); //左固定
                rightTrChildren = rightTr.after(fixTr).next().hide(); //右固定
            }
            //展开|折叠箭头图标
            trObj.find('td[lay-event="collapse"] i.layui-colla-icon').toggleClass("layui-icon-right layui-icon-down");
            //显示|隐藏展开行
            trObjChildren.toggle();
            //开启手风琴折叠和折叠箭头
            if (accordion) {
                trObj.siblings().find('td[lay-event="collapse"] i.layui-colla-icon').removeClass("layui-icon-down").addClass("layui-icon-right");
                trObjChildren.siblings('[data-index$="-children"]').hide(); //展开
                rightTrChildren.siblings('[data-index$="-children"]').hide(); //左固定
                leftTrChildren.siblings('[data-index$="-children"]').hide(); //右固定
            }
            success(trObjChildren, indexChildren); //回调函数
            heightChildren = trObjChildren.height(); //展开高度固定
            rightTrChildren.height(heightChildren + 115).toggle(); //左固定
            leftTrChildren.height(heightChildren + 115).toggle(); //右固定
        }
	});
	
	toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>

</body>
</html>