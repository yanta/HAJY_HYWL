<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>仓库信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>仓库信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入仓库名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
</div>
<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/warehouse/queryWarehouseCriteria.do'
            ,title: '仓库列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,toolbar: '#toolbar'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'region_name', title:'地区名称'}
                ,{field:'staff_name', title:'仓库管理员'}
                ,{field:'warehouse_name', title:'仓库名称'}
                ,{field:'warehouse_num', title:'仓库编号'}
                ,{field:'warehouse_properties', title:'仓库属性'}
                ,{field:'warehouse_address', title:'仓库地址'}
                ,{field:'warehouse_owner', title:'仓库拥有者'}
                /* ,{field:'status', title:'仓库状态', templet: function (row){
                    if (row.status == '0') {
                        return "可用";
                    } else {
                        return "不可用";
                    }
                }} */
                ,{field:'create_time', title:'创建时间'}
                //,{field:'updatedTime', title:'修改时间'}
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            staff_id: function(value, item){
                if(value == ''){
                    return '仓库管理员不能为空';
                }
            },
            region_id: function(value, item){
                if(value == ''){
                    return '地区名称不能为空';
                }
            },
            warehouse_name: function(value, item){
                if(value == ''){
                    return '仓库名称不能为空';
                }
            },
            warehouse_num: function(value, item){
                if(value == ''){
                    return '仓库编号不能为空';
                }
            },
            warehouse_properties: function(value, item){
                if(value == ''){
                    return '仓库属性不能为空';
                }
            },
            warehouse_address: function(value, item){
                if(value == ''){
                    return '仓库地址不能为空';
                }
            },
            warehouse_owner: function(value, item){
                if(value == ''){
                    return '仓库拥有者不能为空';
                }
            } ,
            create_time: function(value, item){
                if(value == ''){
                    return '创建时间不能为空';
                }
            } 
        });

        //新增
        $("#add").click(function(){
            layer.open({
                type: 1 					//Page层类型
                ,area: ['670px', ''] 	//宽  高
                ,title: '新增'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content:
                    '<div id="addDivID">'+
                        '<form class="layui-form" id="addFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">仓库管理员</label></td>'+
	                                    '<td>'+
	                                        '<div class="layui-input-inline" style="width:190px">'+
												'<select lay-verify="staff_id" id="staff_id" name="staff_id" lay-filter="staff_id"> '+
                                                    '<option value="">--请选择仓库管理员--</option>'+
                                                    '<c:forEach items="${allUser}" var= "module">'+
                                                        '<option value="${module.id}">${module.userName}</option>'+
                                                    '</c:forEach>'+
												'</select>'+
											'</div>'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                        '<td><label class="layui-form-label">地区名称</label></td>'+
                                        '<td>'+
                                            '<div style="width: 190px; float: left;">' +
                                                '<select id="region_id" name="region_id" lay-verify="region_id">' +
                                                    '<option value="">请选择</option>' +
                                                '</select>'+
                                            '</div>'+
                                            '<div style="margin-top: 12px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">仓库名称</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_name" name="warehouse_name" lay-verify="warehouse_name" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">仓库编号</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="warehouse_num" name="warehouse_num" lay-verify="warehouse_num" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">仓库属性</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_properties" name="warehouse_properties" lay-verify="warehouse_properties" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">仓库地址</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="warehouse_address" name="warehouse_address" lay-verify="warehouse_address" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">仓库拥有者</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_owner" name="warehouse_owner" lay-verify="warehouse_owner" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">创建时间</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="create_time" name="create_time" lay-verify="create_time" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">备注</label></td>'+
                                        '<td colspan="3">'+
                                            '<input onfocus="editValue()" class="layui-input" id="remark" name="remark" style="width: 504px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 20px"></tr>'+
                                    '<tr align="center">'+
                                        '<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                        '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button  class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/region/queryAllRegion.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#region_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].region_name +"</option>");
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    form.render();
                }
            });
        });

        //编辑
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            layer.open({
                type: 1 				//Page层类型
                ,area: ['670px', ''] 	//宽  高
                ,title: '编辑'
                ,shade: 0.6 			//遮罩透明度
                ,maxmin: true 			//允许全屏最小化
                ,anim: 1 				//0-6的动画形式，-1不开启
                ,content:
                    '<div id="updateDivID">'+
                        '<form class="layui-form" id="updateFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td>'+
                                            '<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">仓库管理员</label></td>'+
	                                    '<td>'+
	                                    '<div class="layui-input-inline" style="width:190px">'+
											'<select lay-verify="staff_id" id="staff_id2" name="staff_id" lay-filter="staff_id"> '+
                                                '<c:forEach items="${allUser}" var= "module">'+
                                                    '<option value="${module.id}">${module.userName}</option>'+
                                                '</c:forEach>'+
											'</select>'+
										'</div>'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                        '<td><label class="layui-form-label">地区名称</label></td>'+
                                        '<td>'+
                                            '<div style="width: 190px; float: left;">' +
                                                '<select id="region_id" name="region_id" lay-verify="region_id">' +
                                                    '<option value="">请选择</option>' +
                                                '</select>'+
                                            '</div>'+
                                            '<div style="margin-top: 12px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                '<tr>'+
                                        '<td><label class="layui-form-label">仓库名称</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_name" name="warehouse_name" value="'+data.warehouse_name+'" lay-verify="warehouse_name" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">仓库编号</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="warehouse_num" name="warehouse_num" value="'+data.warehouse_num+'" lay-verify="warehouse_num" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                '<tr>'+
                                        '<td><label class="layui-form-label">仓库属性</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_properties" name="warehouse_properties" value="'+data.warehouse_properties+'" lay-verify="warehouse_properties" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">仓库地址</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="warehouse_address" name="warehouse_address" value="'+data.warehouse_address+'" lay-verify="warehouse_address" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                '<tr>'+
                                        '<td><label class="layui-form-label">仓库拥有者</label></td>'+
                                        '<td>'+
                                            '<input onfocus="editValue()" class="layui-input" id="warehouse_owner" name="warehouse_owner" value="'+data.warehouse_owner+'" lay-verify="warehouse_owner" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
	                                    '<td><label class="layui-form-label">创建时间</label></td>'+
	                                    '<td>'+
	                                        '<input onfocus="editValue()" class="layui-input" id="create_time" name="create_time" value="'+data.create_time+'" lay-verify="create_time" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">备注</label></td>'+
                                        '<td colspan="3">'+
                                            '<input onfocus="editValue()" class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 504px">'+
                                        '</td>'+
                                    '</tr>'+
	                                '<tr style="height: 20px"></tr>'+
                                    '<tr align="center">'+
                                        '<td colspan="2"><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                        '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button  class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/region/queryAllRegion.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#region_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].region_name +"</option>");
                                if(datas[i].id == data.region_id){
                                    $("#region_id").val(data.region_id);
                                }
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    $("#staff_id2").val(data.staff_id);
                    form.render();
                }
            });
        });

        //批量删除
        $("#del").click(function(){
            var rowData = table.checkStatus('contenttable');
            var data = rowData.data;
            var idArr = new Array();
            if(data.length == 0){
                toastrStyle();
                toastr.warning("请至少选择一条记录！");
            } else {
                for(var i=0;i < data.length;i++){
                    idArr[i] = data[i].id;
                }
                layer.confirm('确定删除吗？', function(){
	                $.ajax({
	                    type:'post',
	                    url:'${pageContext.request.contextPath }/warehouse/deleteWarehouseById.do',
	                    data:{"idArr" : idArr},
	                    success:function(data){
	                            if(data > 0){
	                                toastr.success("删除成功！");
	                                setTimeout(function(){
	                                    location.reload();
	                                },1000);
	                                setTimeout(function(){
	                                    window.location.reload();
	                                },2000);
	                            }else{
	                                toastr.warning("删除失败！");
	                                location.reload();
	                            }
	                        
		                    }
	                });
                });
            }
        });

        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(addForm)', function () {
        	$("#subBtn").attr("disabled",true);
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/warehouse/addWarehouse.do',
                data:$("#addFormID").serialize(),
                cache:false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })

        /**
         * 通用表单编辑(AJAX方式)
         */
        form.on('submit(updateForm)', function () {
            $.ajax({
                type : 'post',
                url : '${pageContext.request.contextPath }/warehouse/updateWarehouse.do',
                data : $("#updateFormID").serialize(),
                cache : false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('修改成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('修改失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function dateToStr(date) {
        var time = new Date(date.time);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        M = M < 10 ? ("0" + M) : M;
        var d = time.getDate();
        d = d < 10 ? ("0" + d) : d;
        var str = y + "-" + M + "-" + d;
        return str;
    }
    
    
    function editValue(){
    	$("#subBtn").attr("disabled",false);
    	
    }
</script>
</body>
</html>
