<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>物料明细</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
    <style type="text/css">
        #tab td {
            border:none;
        }
    </style>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>物料明细</cite>
			</a>
		</span>
    <!-- <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
    <i class="layui-icon" style="line-height:30px">ဂ</i></a> -->
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <!-- <input class="layui-input" placeholder="开始日" name="start" id="start">
            <input class="layui-input" placeholder="截止日" name="end" id="end"> -->
            <input type="text" name="keyword" id="keyword"  placeholder="请输入物料名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="delete"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
    <script type="text/html" id="status">
        <input lay-event="setStatus" type="checkbox" name="status" value="{{d.status}}" switchId={{ d.id}} lay-skin="switch" lay-text="启用|停用" lay-filter="status" {{ d.status == 0 ? 'checked' : '' }}>
    </script>
</div>
<div id="formDiv" hidden>
    <form class="layui-form layui-card-body" id="addform" onsubmit="return false;">
        <input class="layui-hide" name="id" id="id">
        <input class="layui-hide" name="number" id="number">
        <table class="layui-table" id="tab">
            <tbody>
            <tr>
                <td>
                    <div class="layui-inline">
                        <label class="layui-form-label">姓名</label>
                        <input class="layui-input" lay-verify="name" id="name" name="name" type="text" value="" style="width:200px;">
                    </div>
                    <div class="layui-inline" style="margin-top: 8px;">
                        <font style="color:red; font-size: 24px;">*</font>
                    </div>
                </td>
                <td>
                    <div class="layui-inline">
                        <label class="layui-form-label">性别</label>
                        <div class="layui-input-block" style="width: 200px">
                            <select class="layui-select" lay-verify="gender" id="gender" name="gender">
                                <option value="0">男</option>
                                <option value="1">女</option>
                            </select>
                        </div>
                    </div>
                    <div class="layui-inline" style="margin-top: 8px;">
                        <font style="color:red; font-size: 24px;">*</font>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label class="layui-form-label">部门</label>
                        <div class="layui-input-block" style="width: 200px">
                            <select class="layui-select" lay-verify="deptId" id="deptId" name="deptId">
                                <option value="">请选择部门名称</option>
                                <c:forEach items="${requestScope.deptList }" var="dept">
                                    <option value="${dept.id }">${dept.deptName }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </td>
                <td>
                    <div>
                        <label class="layui-form-label">岗位</label>
                        <div class="layui-input-block" style="width: 200px">
                            <select class="layui-select" lay-verify="postId" id="postId" name="postId">
                                <option value="">请选择岗位名称</option>
                                <c:forEach items="${requestScope.postList }" var="post">
                                    <option value="${post.id }">${post.postName }</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label class="layui-form-label">出生日期</label>
                        <input class="layui-input" readonly lay-verify="birthdate" id="birthdate" name="birthdate" type="text" value="" style="width:200px;">
                    </div>
                </td>
                <td>
                    <label class="layui-form-label">身份证号</label>
                    <input class="layui-input" lay-verify="idCard" id="idCard" name="idCard" type="text" value="" style="width:200px;">
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label class="layui-form-label">政治面貌</label>
                        <input class="layui-input" lay-verify="politicsStatus" id="politicsStatus" name="politicsStatus" type="text" value="" style="width:200px;">
                    </div>
                </td>
                <td>
                    <div class="layui-inline">
                        <label class="layui-form-label">婚姻状况</label>
                        <div class="layui-input-block" style="width: 200px">
                            <select class="layui-select" lay-verify="maritalStatus" id="maritalStatus" name="maritalStatus">
                                <option value="">--请选择--</option>
                                <option value="0">未婚</option>
                                <option value="1">已婚</option>
                            </select>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label class="layui-form-label">家庭住址</label>
                        <input class="layui-input" lay-verify="familyAddress" id="familyAddress" name="familyAddress" type="text" value="" style="width:566.5px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div>
                        <label class="layui-form-label">联系电话</label>
                        <input class="layui-input" lay-verify="phone" id="phone" name="phone" type="text" value="" style="width:200px;">
                    </div>
                </td>
                <td>
                    <div>
                        <label class="layui-form-label">银行卡号</label>
                        <input class="layui-input" lay-verify="cardNo" id="cardNo" name="cardNo" type="text" value="" style="width:200px;">
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div>
                        <label class="layui-form-label">备注</label>
                        <textarea class="layui-textarea" lay-verify="remark" id="remark" name="remark" type="text" value="" style="width:566.5px;"></textarea>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div style="text-align: center;margin-top: 20px">
            <button id="subBtn" class="layui-btn layui-btn-blue" lay-submit lay-filter="submitBtn">提交</button>
            <button type="reset" id="addReset" class="layui-btn layui-btn-primary" style="margin-left:200px">重置</button>
        </div>
    </form>
</div>
<script type="text/javascript">
    //日期格式转换
    function date2String(timestamp){
        var d = new Date(timestamp);
        var date = (d.getFullYear()) + "-" +
            (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
            (d.getDate()<10?"0"+d.getDate():d.getDate());
        return date;
    }
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#birthdate'
            ,type: 'date'
        });
        var $ = layui.jquery, active = {
            reload:function () {
                var keyword = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword":keyword}
                });
            }
        }
        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/json/materiel_detail.json'
            ,title: '员工列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'materiel_id', title:'物料名称'}
                ,{field:'supplier_id', title:'供应商'}
                ,{field:'part_num', title:'零件编号'}
                ,{field:'remark', title:'备注'}
                //,{field:'status',unresize: true, title:'启用状态',event: 'setStatus', templet: '#status',width:100}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    
                })
            }
        });
        //监听开关操作
        form.on('switch(status)', function(obj){
            var params = {};
            if(obj.elem.checked == true){
                params["status"] = 0;
            }else{
                params["status"] = 1;
            }
            params["id"] = obj.elem.getAttribute("switchId");
            //改变状态
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/staff/updateStaffStatus.do',
                data:{"id":params.id,"status":params.status},
                success:function(affact){
                    if(affact>0){
                        if(params.status==0){
                            toastr.success("启用成功！");
                        }else{
                            toastr.success("停用成功！");
                        }
                    }else{
                        if(params.status==0){
                            toastr.error("启用失败！");
                        }else{
                            toastr.error("停用失败！");
                        }
                    }
                }
            })
        });
        //头事件
        $("#add").click(function(){
            layer.open({
                type: 1 		//Page层类型
                ,area: ['770px', '600px'] //宽  高
                ,title: '新增员工'
                ,shade: 0.6 	//遮罩透明度
                ,shadeClose: true //点击遮罩关闭
                ,maxmin: true //允许全屏最小化
                ,anim: 1 		//0-6的动画形式，-1不开启
                ,content: $('#formDiv')
                ,success: function () {
                    $("option").removeAttr("selected");
                    document.getElementById("addform").reset();
                }
            });
        })
        $("#delete").click(function(){
            var checkStatus = table.checkStatus("contenttable");
            var data = checkStatus.data;
            var str = "";
            if(data.length==0){
                toastr.warning("请至少选择一条记录！");
            }else{
                layer.confirm('确定删除吗？', function(index){
                    var idArr = new Array();
                    var id = data.id;
                    idArr[0] = id;
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/staff/deleteStaff.do',
                        data:{"idArr":idArr},
                        success:function(affact){
                            if(affact>0){
                                toastr.success("删除成功！");
                                setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
                                    //关闭模态框
                                    // 父页面刷新
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                            }
                        }

                    })
                    layer.close(index);
                });
            }
        })
        //监听行工具事件
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            if(obj.event === 'edit'){
                layer.open({
                    type: 1 		//Page层类型
                    ,area: ['770px', '600px'] //宽  高
                    ,title: '编辑员工'
                    ,shade: 0.6 	//遮罩透明度
                    ,shadeClose: true //点击遮罩关闭
                    ,maxmin: true //允许全屏最小化
                    ,anim: 1 		//0-6的动画形式，-1不开启
                    ,content: $('#formDiv')
                    ,success: function () {
                        document.getElementById("addform").reset();
                        $("#id").val(data.id);
                        $("#name").val(data.name);
                        $("#number").val(data.number);
                        var depts = $("#deptId").find("option"); //获取select下拉框的所有值
                        for (var j = 0; j < depts.length; j++) {
                            if ($(depts[j]).val() == data.deptId) {
                                console.log($(depts[j]).val() + "," + data.deptId);
                                $(depts[j]).attr("selected", "selected");
                            };
                        }
                        var posts = $("#postId").find("option"); //获取select下拉框的所有值
                        for (var j = 0; j < posts.length; j++) {
                            if ($(posts[j]).val() == data.postId) {
                                console.log($(posts[j]).val() + "," + data.postId);
                                $(posts[j]).attr("selected", "selected");
                            };
                        }
                        var genders = $("#gender").find("option"); //获取select下拉框的所有值
                        for (var j = 0; j < genders.length; j++) {
                            if ($(genders[j]).val() == data.gender) {
                                $(genders[j]).attr("selected", "selected");
                            };
                        }
                        $("#birthdate").val(date2String(data.birthdate.time));
                        $("#politicsStatus").val(data.politicsStatus);
                        var maritalStatuses = $("#maritalStatus").find("option"); //获取select下拉框的所有值
                        for (var j = 0; j < maritalStatuses.length; j++) {
                            if ($(maritalStatuses[j]).val() == data.maritalStatus) {
                                $(maritalStatuses[j]).attr("selected", "selected");
                            };
                        }
                        $("#familyAddress").val(data.familyAddress);
                        $("#phone").val(data.phone);
                        $("#cardNo").val(data.cardNo);
                        $("#remark").val(data.remark);
                    }
                    ,end: function () {
                        var formDiv = document.getElementById('formDiv');
                        formDiv.style.display = '';
                        layer.closeAll();
                    }
                });
            }else if(obj.event === 'del'){
                layer.confirm('确定删除吗？', function(index){
                    var idArr = new Array();
                    var id = data.id;
                    idArr[0] = id;
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/staff/deleteStaff.do',
                        data:{"idArr":idArr},
                        success:function(affact){
                            if(affact>0){
                                toastr.success("删除成功！");
                                setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
                                    //关闭模态框
                                    // 父页面刷新
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                            }
                        }

                    })
                    layer.close(index);
                });
            }
        })
        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            name: function(value, item){
                if(value == ''){
                    return '姓名不能为空';
                }
            }
            ,gender: function(value, item){
                if(value == '-1'){
                    return '性别不能为空';
                }
            }
            ,birthdate: function(value, item){
                if(value == ''){
                    return '出生日期不能为空';
                }
            }
            ,phone: function(value, item){
                if((!(/^1[34578]\d{9}$/.test(value)) || !(/^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/.test(value)))&& value != ''){
                    return '请输入正确的手机或固话号码';
                }
            }
            ,cardNo: function(value, item){
                if(!(/^([1-9]{1})(\d{14}|\d{18})$/.test(value)) && value != ''){
                    return '请输入正确的银行卡号';
                }
            }
            ,idCard: function(value, item){
                if(!(/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value)) && value != ''){
                    return '请输入正确的身份证号';
                }
            }
        });
        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(submitBtn)', function (data) {
            $.ajax({
                type:'post'
                ,url:'${pageContext.request.contextPath }/staff/saveStaff.do'
                ,data:$("#addform").serialize()
                ,success:function(res){
                    var list = res.split('_');
                    if (parseInt(list[0]) > 0) {
                        if(list[1].indexOf('add') != -1){
                            layer.closeAll();
                            toastr.success('新增成功！');
                        }
                        if(list[1].indexOf('edit') != -1){
                            layer.closeAll();
                            toastr.success('修改成功！');
                        }
                        setTimeout(function(){
                            //使用  setTimeout（）方法设定定时2000毫秒
                            //关闭模态框
                            // 父页面刷新
                            window.location.reload();
                        },2000);
                    }else{
                        if(list[1].indexOf('add') != -1){
                            toastr.error('新增失败！');
                        }
                        if(list[1].indexOf('edit') != -1){
                            toastr.error('修改失败！');
                        }
                    }
                }
                ,error:function(){

                }
            })
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
</script>
</body>

</html>