<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>员工信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>员工信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入员工名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
</div>
<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/staff/queryStaffCriteria.do'
            ,title: '员工列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'name', title:'员工姓名'}
                ,{field:'number', title:'员工编号'}
                ,{field:'dept_name', title:'部门名称'}
                ,{field:'post_name', title:'岗位名称'}
                ,{field:'gender', title:'性别'}
                ,{field:'birth_time', title:'出生日期', templet: function (row){
                    if(row.birth_time != null){
                        return dateToStr(row.birth_time);
                    }else{
                        return "";
                    }
                }}
                ,{field:'idCard', title:'身份证号'}
                ,{field:'politics_status', title:'政治面貌'}
                ,{field:'marital_status', title:'婚姻状况'}
                ,{field:'family_address', title:'家庭住址'}
                ,{field:'phone', title:'员工电话'}
                ,{field:'card_no', title:'银行卡号'}
                /* ,{field:'status', title:'状态'} */
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            name: function(value, item){
                if(value == ''){
                    return '员工名称不能为空';
                }
            },
            number: function(value, item){
                if(value == ''){
                    return '员工编号不能为空';
                }
            },
            dept_id: function(value, item){
                if(value == ''){
                    return '所属部门不能为空';
                }
            },
            post_id: function(value, item){
                if(value == ''){
                    return '岗位名称不能为空';
                }
            },
            gender: function(value, item){
                if(value == ''){
                    return '性别不能为空';
                }
            }
        });

        //新增
        $("#add").click(function(){
            layer.open({
                type: 1 					//Page层类型
                ,area: ['670px', ''] 	//宽  高
                ,title: '新增'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content:
                    '<div id="addDivID">'+
                        '<form class="layui-form" id="addFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">员工名称</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="name" name="name" lay-verify="name" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">员工编号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="number" name="number" lay-verify="number" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                    	'<td><label class="layui-form-label">部门名称</label></td>'+
	                                    '<td>'+
		                                    '<div style="width: 190px; float: left;">' +
		                                        '<select id="dept_id" name="dept_id" lay-verify="dept_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
		                                '</td>'+
                                        '<td><label class="layui-form-label">岗位名称</label></td>'+
                                        '<td>'+
		                                    '<div style="width: 190px; float: left;">' +
		                                    	'<select id="post_id" name="post_id" lay-verify="post_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">性别</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="gender" name="gender" lay-verify="gender" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">出生日期</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="birth_time" name="birth_time" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">身份证号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="idCard" name="idCard" style="width: 190px">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">政治面貌</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="politics_status" name="politics_status" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">婚姻状况</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="marital_status" name="marital_status" style="width: 190px;">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">家庭住址</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="family_address" name="family_address" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">电话</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="phone" name="phone" style="width: 190px">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">银行卡号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="card_no" name="card_no" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        /* '<td><label class="layui-form-label">状态</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="status" name="status" style="width: 190px">'+
                                        '</td>'+ */
                                        '<td><label class="layui-form-label">备注</label></td>'+
                                        '<td colspan="3">'+
                                            '<input class="layui-input" id="remark" name="remark" style="width: 504px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 20px">'+
                                    '<tr align="center">'+
                                        '<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                        '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                	//下拉框查找所有部门
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllDept.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#dept_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].dept_name +"</option>");
                            }
                        }
                    });
                  	//下拉框查找所有岗位
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllPost.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#post_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].post_name +"</option>");
                            }
                        }
                    });
                	laydate.render({
                        elem: '#birth_time'
                    });
                    form.render();
                }
            });
        });

        //编辑
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            if(data.birth_time==null){data.birth_time="";}
            layer.open({
                type: 1 					//Page层类型
                ,area: ['670px', ''] 	//宽  高
                ,title: '编辑'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content:
                    '<div id="updateDivID">'+
                        '<form class="layui-form" id="updateFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td>'+
                                            '<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">员工名称</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="name" name="name" value="'+data.name+'" lay-verify="name" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">员工编号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="number" name="number" value="'+data.number+'" lay-verify="number" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                	'<td><label class="layui-form-label">部门名称</label></td>'+
                                    '<td>'+
	                                    '<div style="width: 190px; float: left;">' +
	                                        '<select id="dept_id" name="dept_id" lay-verify="dept_id">' +
	                                            '<option value="">请选择</option>' +
	                                        '</select>'+
	                                    '</div>'+
	                                    '<div style="margin-top: 12px;">'+
	                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                    '</div>'+
		                                '</td>'+
	                                    '<td><label class="layui-form-label">岗位名称</label></td>'+
	                                    '<td>'+
		                                    '<div style="width: 190px; float: left;">' +
		                                    	'<select id="post_id" name="post_id" lay-verify="post_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
	                                    '</td>'+
	                                '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">性别</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="gender" name="gender" value="'+data.gender+'" lay-verify="gender" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">出生日期</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="birth_time" name="birth_time" value="'+data.birth_time+'" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">身份证号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="idCard" name="idCard" value="'+data.idCard+'" style="width: 190px">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">政治面貌</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="politics_status" name="politics_status" value="'+data.politics_status+'" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">婚姻状况</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="marital_status" name="marital_status" value="'+data.marital_status+'" style="width: 190px;">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">家庭住址</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="family_address" name="family_address" value="'+data.family_address+'" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">电话</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="phone" name="phone" value="'+data.phone+'" style="width: 190px">'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">银行卡号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="card_no" name="card_no" value="'+data.card_no+'" style="width: 190px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        /* '<td><label class="layui-form-label">状态</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="status" name="status" value="'+data.status+'">'+
                                        '</td>'+ */
                                        '<td><label class="layui-form-label">备注</label></td>'+
                                        '<td colspan="3">'+
                                            '<input class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 504px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 20px">'+
                                    '<tr align="center">'+
                                        '<td colspan="2"><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                        '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                    //下拉框查找所有部门
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllDept.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#dept_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].dept_name +"</option>");
                                if(datas[i].id == data.dept_id){
                                    $("#dept_id").val(data.dept_id);
                                }
                            }
                        }
                    });
                    //下拉框查找所有岗位
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllPost.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#post_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].post_name +"</option>");
                                if(datas[i].id == data.post_id){
                                    $("#post_id").val(data.post_id);
                                }
                            }
                        }
                    });
                    laydate.render({
                        elem: '#birth_time'
                    });
                    form.render();
                }
            });
        });

        //批量删除
        $("#del").click(function(){
            var rowData = table.checkStatus('contenttable');
            var data = rowData.data;
            var idArr = new Array();
            if(data.length == 0){
                toastrStyle();
                toastr.warning("请至少选择一条记录！");
            } else {
                for(var i=0;i < data.length;i++){
                    idArr[i] = data[i].id;
                }
                $.ajax({
                    type:'post',
                    url:'${pageContext.request.contextPath }/staff/deleteStaffById.do',
                    data:{"idArr" : idArr},
                    success:function(data){
                        layer.confirm('确定删除吗？', function(){
                            if(data > 0){
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                                setTimeout(function(){
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                                location.reload();
                            }
                        });
                    }
                });
            }
        });

        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(addForm)', function () {
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/staff/addStaff.do',
                data:$("#addFormID").serialize(),
                cache:false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })

        /**
         * 通用表单编辑(AJAX方式)
         */
        form.on('submit(updateForm)', function () {
            $.ajax({
                type : 'post',
                url : '${pageContext.request.contextPath }/staff/updateStaff.do',
                data : $("#updateFormID").serialize(),
                cache : false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('修改成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('修改失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function dateToStr(date) {
        var time = new Date(date.time);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        M = M < 10 ? ("0" + M) : M;
        var d = time.getDate();
        d = d < 10 ? ("0" + d) : d;
        var str = y + "-" + M + "-" + d;
        return str;
    }
</script>
</body>
</html>
