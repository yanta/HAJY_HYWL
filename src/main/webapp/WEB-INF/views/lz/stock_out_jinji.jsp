<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>出库管理</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
	<%--隐藏选中行的id--%>
	<input class="layui-hide" id="firstTableId">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>出库管理</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<div class="layui-form" style="width: 180px; margin-left: 10px">
							<select id="keyword01" name="keyword01">
								<!-- <select id="customer_id" name="customer_id" lay-verify="customer_id" lay-filter="customer_id" style="width: 190px"> -->
								<option value="">请选择主机厂</option>
								<c:forEach items="${customer_type1}" var="customer1">
									<option value="${customer1.id}">${customer1.customer_name}</option>
								</c:forEach>
							</select>
						</div>
					</td>
				</tr>
			</table>
		</div>
        <button class="layui-btn layui-btn-normal" data-type="reload1"><i class="layui-icon layui-icon-search"></i>检索</button>
	  </div>
      <xblock>
			<button id="add" class="layui-btn layui-btn-warm"><i class="layui-icon layui-icon-add-circle-fine"></i>新增出库单</button>
			<button id="dels" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量取消</button>
					<button class="layui-btn layui-btn-danger" id="restors"><i class="layui-icon layui-icon-delete"></i>批量恢复</button>
			<!-- <button class="layui-btn layui-btn-danger" id="dybd"><i class="layui-icon layui-icon-upload-drag"></i>打印表单</button> -->
			<!-- <button class="layui-btn layui-btn-blue" onclick="uploadExcel()"><i class="layui-icon layui-icon-down"></i>出库单导入</button> -->
      </xblock> 
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<div id="addTwoFormIDDiv" hidden="hidden">
		<form class="layui-form" id="addTwoFormID">
			<table class="layui-hide" id="customerList" lay-filter="customerList"></table>
			<br>
			<div style="margin-bottom: 18px" hidden="hidden" id="selectDiv">
				<div class="layui-inline">
					<table>
						<tr>
							<!-- <td>
								<input class="layui-input" name="materiel_name" id="materiel_name" placeholder="请输入物料名称" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
							</td> -->
							<td>
								<input class="layui-input" name="materiel_num" id="materiel_num" placeholder="请输入SAP/QAD" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
							</td>
						</tr>
					</table>
				</div>
				<button style="margin-left: 10px" class="layui-btn layui-btn-normal" type="button" id="selectMateriel" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
			</div>
			<table class="layui-hide" id="customerMaterielList" lay-filter="customerMaterielList"></table>
			<xblock>
				<button id="addBtn" class="layui-btn layui-btn-blue" lay-submit lay-filter="addBtn" style="margin: 0px 0px 10px 200px; width: 500px">提交</button>
			</xblock>
		</form>
	</div>
	<!-- _____________________________________子表________________________________________ -->
	<div id="subTable" hidden="hidden">
		<xblock style="margin-left: 20px">
			<button id="add1" class="layui-btn layui-btn-warm"><i class="layui-icon layui-icon-add-circle-fine"></i>选择物料</button>
			<button id="dels1" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
			<!-- <button class="layui-btn layui-btn-blue" onclick="uploadExcel()"><i class="layui-icon layui-icon-down"></i>出库单导入</button> -->
      </xblock>
	  <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
	</script>
	<script type="text/html" id="rowToolbar2">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del2"><i class="layui-icon layui-icon-delete"></i>删除</a>
	</script>
	<!-- 导入框 -->
	<div id="importDiv" hidden="hidden">
		<form class="layui-form" id="uploadform">
			<div style="margin: 20px 0 20px 140px;">
				<input id="fileInput" name="file" type="file" accept=".xls,.xlsx"/>
			</div>
		</form>
		<button class="layui-btn layui-btn-blue" lay-filter="uplodeform" style="margin-left: 80px">导入数据</button>&emsp;&emsp;&emsp;&emsp;
		<button type="button" class="layui-btn" id="downloadFile">下载模板</button>
	</div>
	
	<!-- 打印表单框 -->
	<div id="dybdDivID" hidden="hidden">
		<div id="dyDiv" style="width: 100%;overflow-y:scroll;height: 549px">
			<table style="width: 100%;">
				<tr>
					<td align="center" rowspan="2">
						<img alt="" src="${pageContext.request.contextPath }/images/title.jpg">
					</td>
					<td align="center" colspan="2">
						<h1>上海华缘物流有限公司西安分公司</h1>
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<h1>送货单</h1>
					</td>
				</tr>
				<tr style="height:3px"></tr>
				<tr>
					<td colspan="3" width="100%" align="center">
						<table width="70%">
							<tr>
								<td width="40%">
									送货单号：<span id="d1"></span>
								</td>
								<td width="30%">
									到货日期：<span id="d2"></span>
								</td>
								<td rowspan="7" width="30%">
									<img width="200px" id="bd_img"/>
								</td>
							</tr>
							<tr>
								<td>
									创建时间：<span id="d3"></span>
								</td>
								<td>
									窗口时间：<span id="d4"></span>
								</td>
							</tr>
							<tr>
								<td>
									客户名称：<span id="d5"></span>
								</td>
								<td>
									到货地址：<span id="d6"></span>
								</td>
							</tr>
							<tr>
								<td>
									客户地址：<span id="d7"></span>
								</td>
								<td>
									交货道口：<span id="d8"></span>
								</td>
							</tr>
							<tr>
								<td>
									发货联系人：<span id="d9"></span>
								</td>
								<td>
									收货联系人：<span id="d10"></span>
								</td>
							</tr>
							<tr>
								<td>
									联系电话：<span id="d11"></span>
								</td>
								<td>
									联系电话：<span id="d12"></span>
								</td>
							</tr>
							<tr>
								<td>
									备注：<span id="d13"></span>
								</td>
								<td>
									备注：<span id="d14"></span>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr style="height:5px"></tr>
				<tr>
					<td colspan="6"  width="100%" align="center">
						<table id="dytable" lay-filter="dytable">
						</table>
					</td>
				</tr>
				<tr style="height:5px"></tr>
				<tr>
					<td align="center">
						发货人签字：
					</td>
					<td>
						承运商签字：
					</td>
					<td>
						收货人签字：
					</td>
				</tr>
				<tr>
					<td align="center">
						时间：
					</td>
					<td>
						时间：
					</td>
					<td>
						时间：
					</td>
				</tr>
				<tr style="height:5px"></tr>
				<tr>
					<td colspan="3" style="font-size: 12px;color: red">
						1. 发货时务必填全发货零件信息。
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 12px;color: red">
						2. 收货数量按实际数量核对签收。
					</td>
				</tr>
				<tr>
					<td colspan="3" style="font-size: 12px;color: red">
						3. 单据一式三联，发货、收货、收货回单。
					</td>
				</tr>
			</table>
		</div>
		<xblock>
			<div align="right" style="margin-bottom: 0px;">
				<button type="button" id="dayingTable" class="layui-btn" style="margin-left: 450px;background-color: #57575799;"><i class="layui-icon">&#xe66e;</i>打印</button>
			</div>
		</xblock>
	</div>
	<script type="text/javascript">
        var table;
        var tableStatus;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload1:function () {
					var keyword01 = $("#keyword01").val();
					var keyword02 = 1;
					//var keyword03 = $("#keyword03").val();
					table.reload('contenttable',{
						method:'get',
						where:{
						    keyword01:keyword01,
						    //keyword02:keyword02
						}
					});
				},
				reload:function () {
					var materiel_name=$('#materiel_name').val();
		    		var materiel_num=$('#materiel_num').val();
		    		var verify = /^[0-9,]*$/;
		    		if(verify.test(materiel_num)){
		    			var split = materiel_num.split(',')
		    			if(split.length <= 5){
		    				table.reload('contenttable2',{
								method:'get'
								,where:{"materielName":materiel_name,"materielNum":materiel_num}
				         		,page: {
				                    curr: 1//重新从第 1 页开始
				                }
							});
		    			}else{
		    				layer.msg("最多可同时查5个！");
		    			}
		    		}else{
		    			layer.msg("只能输入数字和英文逗号！");
		    		}
				}
			}
			
			//llm新增，添加物料的查询
	       /* $(document).on('click','#selectMateriel',function(){
	        	var materiel_name=$('#materiel_name').val();
	    		var materiel_num=$('#materiel_num').val();
	    		var table = layui.table;
	    		table.reload('customerMaterielList', {
	         		method:'get',
	         		where:{"materiel_name":materiel_name,"materiel_num":materiel_num},
	    		});
			 })  */
				 
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteria.do?keyword02=2'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,id :'contenttable'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,cols: [[
					{type: 'checkbox', fixed: 'left', },
				  	{field:'', title:'序号', type:'numbers', align:'center'},
				  	{field:'out_stock_num', title:'出库单号', align:'center'},
				  	{field:'create_date', title:'创建时间', align:'center'},
				  	{field:'warehouse_name', title:'仓库名称', align:'center'},
				  	{field:'warehouse_address', title:'仓库地址', align:'center'},
				  	//{field:'userName', title:'仓库管理员', align:'center'},
				  	//{field:'phone', title:'管理员电话', align:'center'},
				  	{field:'customer_name', title:'目的地名称', align:'center'},
				  	{field:'customer_adress', title:'目的地地址', align:'center'},
				  	{field:'contacts_name', title:'目的地负责人', align:'center'},
				 	{field:'contacts_tel', title:'负责人电话', align:'center'},
				  	{field:'status', title:'状态', align:'center', templet: function (row){
   	                    if (row.status == '0') {
   	                        return "未出库";
   	                    } else {
   	                        return "已出库";
   	                    }
   	                }},
                    {field:'customer_order', title:'客户单号', align:'center'},
                    {field: 'flag',title: '是否取消',align:'center' ,
                    	templet: function(d){
                        var value = d.flag;
                        if(value == 0){
                            value = "已取消"
                        }else if(value == 1){
                            value = "已恢复"
                        }else if(value == 2){
                            value = "未操作"
                        }
                        return value;
                    }},
                    {field: 'cancellation',title: '取消人',align:'center'},
                    {field: 'cancellation_time',title: '取消时间',align:'center'},
                    {field: 'restorer',title: '恢复人',align:'center'},
                    {field: 'restorer_time',title: '恢复时间',align:'center'},
                 {field: 'remark',title: '备注',align:'center'},
				  	{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
			  	]]
				,page: true
                ,done : function(){
                    $(".timepicker").each(function () {
						laydate.render({
							elem: this,
							format: "yyyy-MM-dd"
						});
					});
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
			});
			/* 打印单据 */
	        table.render({
	            elem: '#dytable'
	            //,url:'${pageContext.request.contextPath }/receiveDetail/list.do'
	            ,title: '明细表'
	            ,cols: [[
	                 {field:'id', title:'序号', width: 50,type:'numbers', align:'center'},
	                 {field:'materiel_num', title:'QAD/SAP', align:'center', width: 150,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_num;
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'brevity_num', title:'供应商零件号', align:'center', width: 200,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.brevity_num;
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'materiel_name', title:'中文描述', align:'center', width: 250,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.materiel_name;
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'unit', title:'单位', align:'center', width: 87,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.unit;
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'packing_quantity', title:'单包装数', width: 100, align:'center',
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel){
		                    		value = row.materiel.packing_quantity;
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'material_quantity', title:'发货包装数', align:'center', width: 100,
		                    templet: function (row){
		                    	var value = "";
		                    	if(null != row.materiel && row.pre_num != "" && row.pre_num != undefined && row.pre_num != 0){
		                    		value = Math.ceil(row.pre_num/row.materiel.packing_quantity);
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'pre_num', title:'发货件数', align:'center', width: 100},
	                 {field:'receiveNum', title:'实收包装数', align:'center', width: 100,
		                    templet: function (row){
		                    	var value = 0;
		                    	if(null != row.materiel && row.act_num != 0){
		                    		value = Math.ceil(row.act_num/row.materiel.packing_quantity);
	                            }
	                            return value;
	                        },
	                 },
	                 {field:'act_num', title:'实收件数', align:'center', width: 100},
	                 {field:'pici', title:'批号/备注', align:'center', width: 150},
	             ]]
	            ,page: false
	            ,done : function(){
	                $('th').css({
	                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
	                })
	            }
	        });
			//出库单的编辑
			table.on('tool(tableList)', function(obj){
				var data = obj.data;
				if(obj.event === 'update'){
					if (data.status == 1) {
						toastrStyle();
                        toastr.warning("已出库，不能编辑！");
					} else {
						layer.open({
							type: 1 				//Page层类型
							,area: ['680px', ''] 	//宽  高
							,title: '编辑'
							,shade: 0.1 			//遮罩透明度
							,maxmin: true 			//允许全屏最小化
							,anim: 1 				//0-6的动画形式，-1不开启
							,content: 
							'<div id="updateDivID">'+
								'<form class="layui-form" id="updateFormID">'+
								'<input readonly class="layui-input" id="type" name="type" value="2" type="hidden">'+
									'<xblock>'+
										'<table>'+
											'<tr>'+
												'<td>'+
													'<input class="layui-hide" name="id" value="'+data.id+'">'+
												'</td>'+
											'</tr>'+
											'<tr>'+
												'<td><label class="layui-form-label">出库单号</label></td>'+
												'<td>'+
													'<input readonly class="layui-input" id="out_stock_num" name="out_stock_num" value="'+data.out_stock_num+'" style="color: gray; width: 190px">'+
												'</td>'+
												'<td><label class="layui-form-label">创建日期</label></td>'+
												'<td>'+
													'<input class="layui-input" id="create_date" name="create_date" value="'+data.create_date+'" lay-verify="create_date" style="width: 190px; display:inline" placeholder="请选择">'+
													'<div class="layui-inline" style="margin-top: 10px;">'+
			                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                        '</div>'+
												'</td>'+
											'</tr>'+
											'<tr style="height: 10px"></tr>'+
											'<tr>'+
												/*'<td><label class="layui-form-label">仓库类型</label></td>'+
												'<td>'+
													'<div style="width: 190px; float: left;">' +
														'<select id="source" name="source" lay-verify="source">' +
															'<option value="">请选择</option>' +
															'<option value="0">良品库</option>' +
															'<option value="1">不良品库</option>' +
														'</select>'+
				                                    '</div>'+
				                                    '<div style="margin-top: 12px;">'+
				                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
				                                    '</div>'+
												'</td>'+*/
												'<td><label class="layui-form-label">仓库名称</label></td>'+
												'<td>'+
													'<div style="width: 190px; float: left;">' +
														'<select id="warehouse_id" name="warehouse_id" lay-verify="warehouse_id" lay-filter="warehouse_id">' +
															'<option value="">请选择</option>' +
														'</select>'+
				                                    '</div>'+
				                                    '<div style="margin-top: 12px;margin-right: -20px;">'+
				                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
				                                    '</div>'+
												'</td>'+
												'<td><label class="layui-form-label">仓库地址</label></td>'+
												'<td>'+
													'<input class="layui-input" id="warehouse_address" name="warehouse_address" value="'+data.warehouse_address+'" style="width: 190px; display:inline" placeholder="请选择">'+
												'</td>'+
											'</tr>'+
											/*'<tr style="height: 10px"></tr>'+
											'<tr>'+
												'<td><label class="layui-form-label">管理员姓名</label></td>'+
												'<td>'+
													'<input class="layui-input" id="userName" name="userName" value="'+data.userName+'" style="width: 190px; display:inline" placeholder="请选择">'+
												'</td>'+
												'<td><label class="layui-form-label">管理员电话</label></td>'+
												'<td>'+
													'<input class="layui-input" id="phone" name="phone" value="'+data.phone+'" style="width: 190px; display:inline" placeholder="请选择">'+
												'</td>'+
											'</tr>'+*/
											'<tr style="height: 10px"></tr>'+
											'<tr>'+
												'<td><label class="layui-form-label">目的地名称 </label></td>'+
												'<td>'+
													'<div style="width: 190px; float: left;">' +
				                                        '<select id="customer_id" name="customer_id" lay-verify="customer_id" lay-filter="customer_id">' +
				                                            '<option value="">请选择</option>' +
				                                        '</select>'+
				                                    '</div>'+
				                                    '<div style="margin-top: 12px;margin-right: -20px;">'+
				                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
				                                    '</div>'+
												'</td>'+
												'<td><label class="layui-form-label">负责人</label></td>'+
												'<td>'+
													'<input readonly class="layui-input" id="contacts_name" name="contacts_name" value="'+data.contacts_name+'" style="width: 190px; display:inline">'+
												'</td>'+
											'</tr>'+
											'<tr style="height: 10px"></tr>'+
											'<tr>'+
												'<td><label class="layui-form-label">负责人电话</label></td>'+
												'<td>'+
													'<input readonly class="layui-input" id="contacts_tel" name="contacts_tel" value="'+data.contacts_tel+'" style="width: 190px; display:inline">'+
												'</td>'+
												'<td><label class="layui-form-label">目的地地址</label></td>'+
												'<td>'+
													'<input readonly class="layui-input" id="customer_adress" name="customer_adress" value="'+data.customer_adress+'" style="width: 190px; display:inline">'+
												'</td>'+
											'</tr>'+
											'<tr style="height:10px"></tr>'+
											'<tr>'+
													'<td> <label class="layui-form-label">备注</label>'+
								                    '</td>'+
													'<td >'+
														'<input class="layui-input" id="remark" value="'+data.remark+'" name="remark" style="width: 190px" placeholder="请选输入备注信息">'+
												'</td>'+
													'</tr>'+
										'</table>'+
									'</xblock>'+
									'<xblock>'+
										'<button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="updateForm" style="margin-left:250px; margin-bottom: 10px">提交</button>'+
										'&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>'+
									'</xblock>'+
									'</table>'+
								'</form>'+
	                    	'</div>'
							,success: function () {
								//仓库类型回显
								
								//查找所有仓库
		                    	$.ajax({ 
		                            type: 'POST',
		                            url: '${pageContext.request.contextPath}/warehouse/queryAllWarehouse.do',
		                            dataType: 'json',
		                            async: false,
		                            success: function (datas){
		                                for (var i = 0; i < datas.length; i++) {
		                                	$("#warehouse_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].warehouse_name +"</option>");
		                                	if(datas[i].id == data.warehouse_id){
			                                    $("#warehouse_id").val(data.warehouse_id);
			                                }
		                                }
		                            }
		                        });
		                    	//查找所有客户/主机厂
		                    	$.ajax({ 
		                            type: 'POST',
		                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomerByType1.do',
		                            dataType: 'json',
		                            async: false,
		                            success: function (datas){
		                                for (var i = 0; i < datas.length; i++) {
		                                	$("#customer_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].customer_name +"</option>");
		                                	if(datas[i].id == data.customer_id){
			                                    $("#customer_id").val(data.customer_id);
			                                }
		                                }
		                            }
		                        });
		                        laydate.render({
		                            elem: '#create_date'
		                            	 ,type: 'datetime'
		                        });
		                        $("#create_date").val(curentTime())
		                    
		                        form.render();
							}
						});
					}
				}
			});

			/* var mIdArr = new Array();
			//监听待发货区表的checkbox选中事件
            table.on('checkbox(waitSendAreaMateriel)', function(obj){
                //选中行的大表id
                //alert($("#firstTableId").val());
                //选中行的
				//alert(obj.data.id);
                mIdArr.push(obj.data.id);
            });
			//监听库存表的checkbox选中事件
            table.on('checkbox(zcInventoryInfoMateriel)', function(obj){
                mIdArr.push(obj.data.id);
            }); */

          	//供应商列表行单击事件
            table.on('row(customerList)', function(obj) {
            	var data = obj.data;
            	//mIdArr.push(data.id);
            	$('#selectDiv').removeAttr("hidden");
            	table.render({
                    elem: '#customerMaterielList'
                    //根据供应商id查询该供应商的所有物料
                    //,url:'${pageContext.request.contextPath }/customer/queryInventoryInfoByCustomerId.do?customer_id='+data.id
                    ,url:'${pageContext.request.contextPath}/cancellingStockDetailManager/listPageMaterielByCustomerIdAndWarehouseId.do?customerId='+data.id+'&source=1'
                    //,toolbar: '#toolbar'
                    ,title: '出库单详细表'
                    ,id :'contenttable2'
                    ,limits:[10,20,30]
                    //,defaultToolbar:['filter', 'print']
                    ,cols: [[
						{type: 'checkbox', fixed: 'left', },
						{field:'materiel_num', title:'SAP/QAD', align:'center'},
						{field:'materiel_name', title:'中文名称', align:'center'},
						{field:'brevity_num', title:'零件号', align:'center'},
						//{field:'materiel_size', title:'物料规格', align:'center'},
						//{field:'materiel_properties', title:'物料型号', align:'center'},
						//{field:'sequence_number', title:'待发货区数量', align:'center'},
						//{field:'placement_type', title:'库存中小箱数', align:'center'}
     				]]
                    ,page: true
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });
            
            //出库单行单击事件
            table.on('row(tableList)', function(obj) {
            	$("#subTable").show();
                var data = obj.data;
                tableStatus = data.status;
                //单击每一行的时候将该行的id传入
                //mIdArr.push(data.id);
                $("#firstTableId").val(data.id);
                //点击出库单行单击事件时根据出库单状态展示不同的出库单详细
                //1表示已出库
                if (data.status != 0) {
                	table.render({
                        elem: '#tableListSub'
                        ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById3.do?keyword01='+data.id
                        ,toolbar: '#toolbar'
                        ,title: '出库单详细表'
                        ,id :'contenttable1'
                        ,limits:[10,20,30]
                        //,defaultToolbar:['filter', 'print']
                        ,cols: [[
    	   				  	{type: 'checkbox', fixed: 'left', },
    	   				  	{field:'customer_name', title:'供应商名称', hide:true},
    	   				 	{field:'materiel_num', title:'SAP/QAD', align:'center'},
    	   				  	{field:'materiel_name', title:'中文名称', align:'center', event: 'collapse',
   								templet: function(d) {
       								return '<div style="position: relative;'+'padding: 0 10px 0 20px;">' + d.materiel_name + '<i style="left: 0px;" lay-tips="展开" class="layui-icon layui-colla-icon layui-icon-right"></i></div>'
       						}},
    		   				{field:'brevity_num', title:'零件号', align:'center'},
    		   				//{field:'materiel_size', title:'物料规格', align:'center'},
    		   				//{field:'materiel_properties', title:'物料型号', align:'center'},
    		   				{field:'small_num', title:'可出库数量', align:'center'},
    		   				{field:'single_num', title:'质检区数量', align:'center'},
    	   				  	{field:'material_quantity', title:'出库数量', align:'center'},
    	   				  	{field:'actual_quantity', title:'实际出库', align:'center'},
    	   					//{field:'recommend_stock', title:'推荐库位', align:'center'}
                            //{field:'single_num', title:'单个数量', align:'center'},
                            //{field:'big_num', title:'大箱数量', align:'center'},
                            //{field:'small_num', title:'小箱数量', align:'center'}
         				]]
                        ,page: true
                        ,done : function(){
                            $('th').css({
                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                            })
                        }
                    });
                } else {
                	table.render({
                        elem: '#tableListSub'
                        ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById3.do?keyword01='+data.id
                        ,toolbar: '#toolbar'
                        ,title: '出库单详细表'
                        ,id :'contenttable1'
                        ,limits:[10,20,30]
                        //,defaultToolbar:['filter', 'print']
                        ,cols: [[
    	   				  	{type: 'checkbox', fixed: 'left', },
    	   				  	{field:'customer_name', title:'供应商名称', hide:true},
    	   				 	{field:'materiel_num', title:'SAP/QAD', align:'center'},
    	   				  	{field:'materiel_name', title:'中文名称', align:'center', event: 'collapse', templet: function(d) {
    							return '<div style="position: relative;'+'padding: 0 10px 0 20px;">' + d.materiel_name + '<i style="left: 0px;" lay-tips="展开" class="layui-icon layui-colla-icon layui-icon-right"></i></div>'
    						}},
    		   				{field:'brevity_num', title:'零件号', align:'center'},
    		   				//{field:'materiel_size', title:'物料规格', align:'center'},
    		   				//{field:'materiel_properties', title:'物料型号', align:'center'},
    		   				{field:'small_num', title:'可出库数量', align:'center'},
    		   				{field:'single_num', title:'质检区数量', align:'center'},
    		   				{field:'material_quantity', title:'预计数量', align:'center', edit: true, templet: function (row){
    							return '<span style="color: blue">'+row.material_quantity+'</span>';
    						}},
    	   				  	{field:'actual_quantity', title:'实际出库', align:'center'},
    	   					//{field:'recommend_stock', title:'推荐库位', align:'center'},
                            //{field:'single_num', title:'单个数量', align:'center'},
                            //{field:'big_num', title:'大箱数量', align:'center'},
                            //{field:'small_num', title:'小箱数量', align:'center'}
         				]]
                        ,page: true
                        ,done : function(){
                            $('th').css({
                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                            })
                        }
                    });
                }
            });

          	//点击行checkbox选中
    	    $(document).on("click",".layui-table-body table.layui-table tbody tr", function () {
    	        var index = $(this).attr('data-index');
    	        var tableBox = $(this).parents('.layui-table-box');
    	        //存在固定列
    	        if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length>0) {
    	            tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
    	        } else {
    	            tableDiv = tableBox.find(".layui-table-body.layui-table-main");
    	        }
    	        var checkCell = tableDiv.find("tr[data-index=" + index + "]").find("td div.laytable-cell-checkbox div.layui-form-checkbox I");
    	        if (checkCell.length>0) {
    	            checkCell.click();
    	        }
    	    });
    	
    	    $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
    	        e.stopPropagation();
    	    });
            
            //监听复选框事件
            table.on('checkbox(tableList)',function(obj){
                if(obj.checked == true && obj.type == 'all'){
                    //点击全选
                    $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
                }else if(obj.checked == false && obj.type == 'all'){
                    //点击全不选
                    $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
                }else if(obj.checked == true && obj.type == 'one'){
                    //点击单行
                    if(obj.checked == true){
                        obj.tr.addClass('layui-table-click');
                    }else{
                        obj.tr.removeClass('layui-table-click');
                    }
                }else if(obj.checked == false && obj.type == 'one'){
                    //点击全选之后点击单行
                    if(obj.tr.hasClass('layui-table-click')){
                        obj.tr.removeClass('layui-table-click');
                    }
                }
            })

            /*laydate.render({
				elem: '#keyword01',
			});*/
            
            $('#downloadFile').click(function(){
                layer.confirm('确认下载模板吗？', function(index){
                    var url = "${pageContext.request.contextPath }/stockOutOrder/exportFileMachineList.do"
                    window.location.href = url;
                    layer.close(index);
                });
            });

          	//"出库单详细"的行内编辑事件
            table.on('edit(tableListSub)', function(obj){
                var fieldValue = obj.value, 	//得到修改后的值
                    rowData = obj.data, 		//得到所在行所有键值
                    fieldName = obj.field; 		//得到字段
                    max = rowData.small_num;
               	var firstValue = $(this).prev().text(); 	//得到修改前的值
               	var patrn = /^[0-9]*$/;
               	if(!patrn.test(fieldValue)){
               		toastr.warning("只能是数字！");
               		//$(this)[0].value = $(this).prev().text(); //还原值
               		setTimeout(function(){
                        location.reload();
                    },1000);
               	} else {
            	   	if(fieldValue > max){
	               		toastr.warning("不能大于当前的库存！");
	               		setTimeout(function(){
                            location.reload();
                        },1000);
	               	}else{
	               	//保存修改出库单详细表的值    
	                    layui.use('jquery',function(){
	                        var $ = layui.$;
	                        $.ajax({
	                            type: "post",
	                            url: "${pageContext.request.contextPath}/stockOutOrder/updateOutStockOrderDetailById.do",
	                            data: {id:rowData.id, fieldName:fieldName, fieldValue:fieldValue },
	                            success: function(zcInventoryInfoId){
	                                if(zcInventoryInfoId > 0){
	                                    toastrStyle();
	                                    toastr.success("修改成功！");
	                                    /* setTimeout(function(){
	                                        location.reload();
	                                    },1000); */
	                                }else{
	                                    toastrStyle();
	                                    toastr.warning("修改失败！");
	                                    /* setTimeout(function(){
	                                        location.reload();
	                                    },1000); */
	                                }
	                            }
	                        });
	                    });
	               	}
               	}
            });

            //"出库单详细"的行内展开事件
            table.on('tool(tableListSub)', function(obj){
            	//展开详细表的下表效果
            	if (obj.event === 'collapse') {
    				var trObj = layui.$(this).parent('tr'); //当前行
    				var accordion = true //开启手风琴，那么在进行折叠操作时，始终只会展现当前展开的表格。
    				var content = '<table></table>' //内容
    				//表格行折叠方法
    				collapseTable({
    					elem: trObj,
    					accordion: accordion,
    					content: content,
    					success: function (trObjChildren, index) { //成功回调函数
    						//trObjChildren 展开tr层DOM
    						//index 当前层索引
    						trObjChildren.find('table').attr("id", index);
                            /*===================================start===================================*/
    						table.render({
    							elem: "#" + index
                                ,url:'${pageContext.request.contextPath }/receiveDetail/queryCodemarkByReceiveDetailId.do?receive_detail_id=' + obj.data.id
                                ,title: 'machineList'
                                //,limits:[10,20,30]
                                ,cols:
    								[[
                                        {field:'id',title:'序号',sort: true,width: 60,type:'numbers',align:'center'},
                                        {field: 'code',title: '条码',align:'center'},
                                        //{field: 'sendCompany',title: '收货数量(个)',align:'center',},
                                        {field: 'num',title: '单箱数量(个)',align:'center', templet: function(d){
    										if(d.num == 0){
    											return 0;
    										} else {
    											return d.num;
    										}
    									}}
                                    ]]
                                ,page: false
                                /*,done : function(){
                                    $('th').css({
                                        'background-color': '#009688', 'color': '#fff','font-weight':'bold'
                                    })
                                }*/
    						});
    						/*===================================end===================================*/
    					}
    				});
    			}
                /* layer.confirm('确定删除吗？', function(index){
                    var id = obj2.data.id;
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/stockOutOrder/deleteOutStockOrderDetailById1.do',
                        data:{ "id" : id },
                        success:function(data){
                            if(data > 0){
                                toastrStyle();
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                toastrStyle();
                                toastr.warning("删除失败！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }
                        }
                    });
                    layer.close(index);
                }); */
            });

            /**
             * 通用表单提交(AJAX方式)（导入）
             */
            form.on('submit(uplodeform)', function (data) {
                var formData = new FormData($("#uploadform")[0]);
                $.ajax({
                    url: '${pageContext.request.contextPath}/stockOutOrder/uplodeFileReactionPlan.do' ,
                    type: 'post',
                    data: formData,
                    //async: false,
                    //cache: false,
                    dataType:'JSON',
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        if (data == 1) {
                            toastr.success("文件导入成功！");
                            setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
                                window.location.reload();
                                $("#reset").click();
                            },1000);
                        }else if(data == -1){
                            toastr.warning("导入的数据不符合导入条件！");
                        }else{
                            toastr.warning("文件格式异常，文件导入失败！");
                        }
                    },
                    error: function (data) {
                        toastr.error("文件格式异常，文件导入失败！");
                    }
                })
                return false;
            });

			/**
			 * 提交出库单(AJAX方式)(新增)
			 */
			form.on('submit(addForm)', function (data) {
				$.ajax({
					url : '${pageContext.request.contextPath}/stockOutOrder/addStockOutOrder.do',
					data: $("#addFormID").serialize(),
					cache : false,
					type : "post",
				}).done(
					function(res) {
						if (res > 0) {
							/* —————————————— */
							toastrStyle();
							toastr.success('新增成功！');
							setTimeout(function(){
								location.reload();
							},1000);
							/* —————————————— */
						}
					}
				).fail(
					function(res) {
						toastrStyle();
						toastr.error('新增失败！');
						setTimeout(function(){
							location.reload();
						},1000);
					}
				)
				return false;
			});
			
			/**
			 * 提交出库单(AJAX方式)(编辑)
			 */
			form.on('submit(updateForm)', function (data) {
				$.ajax({
					url : '${pageContext.request.contextPath}/stockOutOrder/updateStockOutOrder.do',
					data: $("#updateFormID").serialize(),
					cache : false,
					type : "post",
				}).done(
					function(res) {
						if (res > 0) {
							/* —————————————— */
							toastrStyle();
							toastr.success('修改成功！');
							setTimeout(function(){
								location.reload();
							},1000);
							/* —————————————— */
						}
					}
				).fail(
					function(res) {
						toastrStyle();
						toastr.error('修改失败！');
						setTimeout(function(){
							location.reload();
						},1000);
					}
				)
				return false;
			});

			/**
			 * 提交待发货区和库存物料(AJAX方式)(新增)
			 */
			form.on('submit(addBtn)', function (data) {
				//获取出库单id
				var checkStatus1 = table.checkStatus('contenttable')
	      		var data1 = checkStatus1.data;
				var out_stock_num_id = data1[0].id;
				//供应商id
				var checkStatus2 = table.checkStatus('customerList')
	      		var data2 = checkStatus2.data;
				var customer_id = data2[0].id;
				//获取供应商的物料明细中的数据
				//var oldData = table.cache['contenttable2'];
				var checkStatus3 = table.checkStatus('contenttable2')
				var data3 = checkStatus3.data;
				if(data3.length == 0){
					toastr.warning("请选择物料！");
				} else {
					var affct = 0;
					//根据出库单的id和物料id查询该出库单是否存在该物料
					for (var i = 0; i < data3.length; i++) {
						var material_id = data3[i].id;
						$.ajax({
		                    url: '${pageContext.request.contextPath}/stockOutOrder/queryExistMaterial.do' ,
		                    type: 'post',
		                    data: {out_stock_num_id:out_stock_num_id, material_id:material_id, customer_id:customer_id},
		                    async: false,
		                    dataType:'JSON',
		                    success: function (result) {
		                    	if (result > 0) {
		                    		toastr.warning("记录已经存在，不能重复添加");
		                    	} else {
	    			            	//添加收货明细
	    							$.ajax({
	    			                    url: '${pageContext.request.contextPath}/stockOutOrder/addStockOutOrderDetail.do' ,
	    			                    type: 'post',
	    			                    data: {out_stock_num_id:out_stock_num_id, material_id:material_id, customer_id:customer_id},
	    			                    async: false,
	    			                    dataType:'JSON',
	    			                    success: function (result) {
	    			                    	if (result > 0) {
	    								 		affct = affct + 1;
	    								 		if(affct > 0) {
    												toastr.success('新增成功！');
    												setTimeout(function(){
    													location.reload();
    												},1000);
    											}else {
    												toastr.error('新增失败！');
    											}
	    			                    	}
	    			            		}
	    							});
		                    	}
		            		}
						});
					}
					/* $.ajax({
	                    url: '${pageContext.request.contextPath}/stockOutOrder/queryExistMaterial.do' ,
	                    type: 'post',
	                    data: {out_stock_num_id:out_stock_num_id, material_id:material_id, customer_id:customer_id},
	                    async: false,
	                    dataType:'JSON',
	                    success: function (result) {
	                    	if (result > 0) {
	                    		toastr.warning("记录已经存在，不能重复添加");
	                    	} else {
	                    		for (var i = 0; i < data3.length; i++) {
	    							var material_id = data3[i].id;
	    							//alert("物料id"+material_id)
	    			            	//添加收货明细
	    							$.ajax({
	    			                    url: '${pageContext.request.contextPath}/stockOutOrder/addStockOutOrderDetail.do' ,
	    			                    type: 'post',
	    			                    data: {out_stock_num_id:out_stock_num_id, material_id:material_id, customer_id:customer_id},
	    			                    async: false,
	    			                    dataType:'JSON',
	    			                    success: function (result) {
	    			                    	if (result > 0) {
	    								 		  affct = affct + 1;
	    			                    	}
	    			            		}
	    							});
	    						}
	                    	}
	            		}
					}); */
				}
				return false;
			});
			
			//根据仓库id查询管理员id，再查询user的姓名、电话
		    form.on('select(warehouse_id)', function(data) {
		    	$.ajax({
		            type: 'POST',
		            url: '${pageContext.request.contextPath}/stockOutOrder/queryWarehouseById.do',
		            data: {id:data.value},
		            //dataType: 'json',
		            //async: false,
		            success: function (data1){
		            	if(data1 != null){
		            		var warehouse = eval('(' +data1+ ')');
			            	$('#warehouse_address').val(warehouse[0].warehouse_address);
			            	$.ajax({
					            type: 'POST',
					            url: '${pageContext.request.contextPath}/stockOutOrder/queryUserById.do',
					            data: {id:warehouse[0].staff_id},
					            //dataType: 'json',
					            //async: false,
					            success: function (data2){
					            	if(data2 != null){
					            		var user = eval('(' +data2+ ')');
						            	$('#userName').val(user[0].userName);
						            	$('#phone').val(user[0].phone);
					            	}
					            }
					        });
		            	}
		            }
		        });
		    });
			
		  	//根据客户id查询联系人id，再查询负责人的姓名和电话
		    form.on('select(customer_id)', function(data){
				$.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/stockOutOrder/queryCustomerById.do',
                    data: {id:data.value},
                    //dataType: 'json',
                    //async: false,
                    success: function (data1){
                    	if(data1 != null){
		            		var customer = eval('(' +data1+ ')');
			            	$('#customer_adress').val(customer[0].customer_adress);
			            	$.ajax({
					            type: 'POST',
					            url: '${pageContext.request.contextPath}/stockOutOrder/queryContactsById.do',
					            data: {id:customer[0].contacts_id},
					            //dataType: 'json',
					            //async: false,
					            success: function (data2){
					            	if(data2 != null){
					            		var contacts = eval('(' +data2+ ')');
						            	$('#contacts_name').val(contacts[0].contacts_name);
						            	$('#contacts_tel').val(contacts[0].contacts_tel);
					            	}
					            }
					        });
		            	}
                    }
                });
			})
			
			/**
			 * 新增表单校验 
			 */
			form.verify({
				//value：表单的值item：表单的DOM对象
				create_date: function(value, item){
					if(value == ''){
						return '创建日期不能为空';
					}
				},
				/* source: function(value, item){
					if(value == ''){
						return '仓库类型不能为空';
					}
				}, */
				warehouse_id: function(value, item){
					if(value == ''){
						return '仓库名称不能为空';
					}
				},
				customer_id: function(value, item){
					if(value == ''){
						return '主机厂名称不能为空';
					}
				},
			});

			//新增
            $("#add").click(function(){
            	//时间戳
        		var timestamp = (new Date()).getTime();
                layer.open({
                    type: 1 				//Page层类型
                    ,area: ['680px', ''] 	//宽  高
                    ,title: '新增'
                    ,shade: 0.6 			//遮罩透明度
                    ,maxmin: true 			//允许全屏最小化
                    ,anim: 1 				//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="addDivID">'+
							'<form class="layui-form" id="addFormID">'+
							'<input readonly class="layui-input" id="type" name="type" value="2" type="hidden">'+
								'<xblock>'+
									'<table>'+
										'<tr>'+
											'<td><label class="layui-form-label">出库单号</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="out_stock_num" name="out_stock_num" value="CKDH-'+timestamp+'" style="color: gray; width: 190px">'+
											'</td>'+
											'<td><label class="layui-form-label">创建日期</label></td>'+
											'<td>'+
												'<input class="layui-input" id="create_date" name="create_date" lay-verify="create_date" style="width: 190px; display:inline" placeholder="请选择">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											/* '<td><label class="layui-form-label">仓库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="source" name="source" lay-verify="source">' +
														'<option value="">请选择</option>' +
														'<option value="0">良品库</option>' +
														'<option value="1">不良品库</option>' +
													'</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+ */
											'<td><label class="layui-form-label">仓库名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="warehouse_id" name="warehouse_id" lay-verify="warehouse_id" lay-filter="warehouse_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;margin-right: -20px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">管理员姓名</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="userName" name="userName" style="width: 190px; display:inline; color: gray" placeholder="请选择">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">管理员电话</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="phone" name="phone" style="width: 190px; display:inline; color: gray" placeholder="请选择">'+
											'</td>'+
											'<td><label class="layui-form-label">仓库地址</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="warehouse_address" name="warehouse_address" style="width: 190px; display:inline; color: gray" placeholder="请选择">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">目的地名称 </label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
			                                        '<select id="customer_id" name="customer_id" lay-verify="customer_id" lay-filter="customer_id">' +
			                                            '<option value="">请选择</option>' +
			                                        '</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;margin-right: -20px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">负责人</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="contacts_name" name="contacts_name" style="width: 190px; display:inline; color: gray">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">负责人电话</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="contacts_tel" name="contacts_tel" style="width: 190px; display:inline; color: gray">'+
											'</td>'+
											'<td><label class="layui-form-label">客户单号</label></td>'+
											'<td>'+
												'<input class="layui-input" id="customer_order" name="customer_order" style="width: 190px; display:inline">'+
											'</td>'+
										'</tr>'+
                        				'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">目的地地址</label></td>'+
											'<td colspan="3">'+
												'<input readonly class="layui-input" id="customer_adress" name="customer_adress" style="width: 490px; display:inline; color: gray">'+
											'</td>'+
										'</tr>'+
										'<tr style="height:10px"></tr>'+
										'<tr>'+
												'<td> <label class="layui-form-label">备注</label>'+
							                    '</td>'+
												'<td >'+
													'<input class="layui-input" id="remark" name="remark" style="width: 190px" placeholder="请选输入备注信息">'+
											'</td>'+
												'</tr>'+
									'</table>'+
								'</xblock>'+
								'<xblock>'+
									'<button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:250px; margin-bottom: 10px">提交</button>'+
									'&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>'+
								'</xblock>'+
								'</table>'+
							'</form>'+
                    	'</div>'
					,cancel : function(){
						//数组置空
						//hideArr = [];
					}
                    ,success: function(){
                    	//查找所有仓库
                    	$.ajax({ 
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/warehouse/queryAllWarehouse.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#warehouse_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].warehouse_name +"</option>");
                                }
                            }
                        });
                    	//查找所有客户/主机厂
                    	$.ajax({ 
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomerByType1.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#customer_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].customer_name +"</option>");
                                }
                            }
                        });
                        laydate.render({
                            elem: '#create_date'
                        });
                        form.render();
                    }
                });
			});

          	//查询所有的供应商
    		$('#add1').click(function(){
    			if(tableStatus == 0){
    				$("#customerMaterielList").hide();
        			layer.open({
        				type: 1 					//Page层类型
        				,area: ['1000px', '500px'] 	//宽  高
        				,title: '客户的物料列表'
        				,shade: 0.1 				//遮罩透明度
        				,shadeClose: true 			//点击遮罩关闭
        				,maxmin: true 				//允许全屏最小化
        				,anim: 1 					//0-6的动画形式，-1不开启
        				,content: $("#addTwoFormIDDiv")
        				,success: function () {
        					//1.查询待发货区
        					layui.use(['table'], function(){
        						var table = layui.table;
        						table.render({
        							elem: '#customerList'
        							,url:'${pageContext.request.contextPath}/customer/queryCustomerCriteriaByType.do'
        							,title: '客户列表'
        							,limits:[10,20,30]
        							,cols: [[
        							  	{type: 'checkbox', fixed: 'left', },
        							  	{field:'customer_name', title:'供应商姓名', align:'center'},
        							  	{field:'customer_num', title:'编号', align:'center'},
        							  	{field:'customer_region', title:'供应商所在地区', align:'center'},
        							  	{field:'customer_adress', title:'供应商地址', align:'center'},
        							  	{field:'customer_email', title:'供应商邮箱', align:'center'},
        							  	{field:'customer_zipcode', title:'供应商邮编', align:'center'}
        							]]
        							,page: true
        							,done : function(){
        				                $('th').css({
        				                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
        				                    'font-size': 10,
        				                })
        				            }
        						});
        			        });
        				}
        				,end: function () {
        					var formDiv = document.getElementById('inventoryInfoDiv');
        					formDiv.style.display = '';
        					layer.closeAll();
        				}
        			});
    			}else{
    				toastr.warning("对不起，已出库的数据不能操作！");
    			}
    		});
			
    		//"出库单"批量删除
			$("#dels").click(function(){
				var rowData = table.checkStatus('contenttable');
				var data = rowData.data;
				var idArr = new Array();
				if(data.length == 0){
					toastrStyle();
					toastr.warning("请至少选择一条记录！");
				} else {
					for(var i=0;i < data.length;i++){
						idArr[i] = data[i].id;
						if (data[i].status != 0) {
							toastr.warning("对不起，已出库的数据不能取消！");
		            		return;
						}if(data[i].flag == 0){
	                        toastr.warning("对不起，已取消的数据不能取消！");
	                        return;
	                    }
					}
					layer.confirm('确定取消吗？', function(index){
	    				$.ajax({
	    			    	type:'post',
	    			    	url:'${pageContext.request.contextPath }/stockOutOrder/deleteOutStockOrderById.do',
	    			    	data:{"idArr" : idArr},
	    			    	success:function(data){
	    			    		if(data > 0){
	    			    			toastrStyle();
	    			    			toastr.success("取消成功！");
	    			    			setTimeout(function(){  
	    								window.location.reload();  
	    							},1000);
	    			    		}else{
	    			    			toastrStyle();
	    			    			toastr.error("取消失败！");
	    			    			setTimeout(function(){  
	    								window.location.reload();  
	    							},1000);
	    			    		}
	    			    	}
	    			    })
	    		    	layer.close(index);
	    			});
				}
			});
			$("#restors").click(function(){
				var rowData = table.checkStatus('contenttable');
				var data = rowData.data;
				var idArr = new Array();
				if(data.length == 0){
					toastrStyle();
					toastr.warning("请至少选择一条记录！");
				} else {
					for(var i=0;i < data.length;i++){
						idArr[i] = data[i].id;
						if (data[i].status != 0) {
							toastr.warning("对不起，已出库的数据不能恢复！");
		            		return;
						}if(data[i].flag == 1){
	                        toastr.warning("对不起，已恢复的数据不能恢复！");
	                        return;
	                    }
					}
					layer.confirm('确定恢复吗？', function(index){
	    				$.ajax({
	    			    	type:'post',
	    			    	url:'${pageContext.request.contextPath }/stockOutOrder/restor.do',
	    			    	data:{"idArr" : idArr},
	    			    	success:function(data){
	    			    		if(data > 0){
	    			    			toastrStyle();
	    			    			toastr.success("恢复成功！");
	    			    			setTimeout(function(){  
	    								window.location.reload();  
	    							},1000);
	    			    		}else{
	    			    			toastrStyle();
	    			    			toastr.error("恢复失败！");
	    			    			setTimeout(function(){  
	    								window.location.reload();  
	    							},1000);
	    			    		}
	    			    	}
	    			    })
	    		    	layer.close(index);
	    			});
				}
			});
			
			//打印表单
	        $("#dybd").click(function(){
	        	var checkStatus = table.checkStatus('contenttable')
	            var datas = checkStatus.data;
	            if(datas.length!=1){
	                toastr.warning("请选择一条记录！");
	            }else{
	            	var data = datas[0]
		            layer.open({
		                type: 1 					//Page层类型
		                ,area: ['1450px', '650px'] 			//宽  高
		                ,title: '打印表单'
		                ,shade: 0.6 				//遮罩透明度
		                ,maxmin: true 				//允许全屏最小化
		                ,anim: 1 					//0-6的动画形式，-1不开启
		                ,content: $('#dybdDivID')
		                ,end: function () {
		                    var formDiv = document.getElementById('dybdDivID');
		                    formDiv.style.display = '';
		                }
		                ,success: function(){
	                        $("#d1").html(data.out_stock_num);
	                        $("#d3").html(data.create_date);
	                        $("#d5").html(data.customer_name);
	                        $("#d7").html(data.customer_adress);
	                        $("#d10").html(data.contacts_name);
	                        $("#d12").html(data.contacts_tel);
	                      //二维码路径
	                        var path = '${pageContext.request.contextPath}/'
	                        $.ajax({
	                            type:'post',
	                            data:'code='+data.out_stock_num,
	                            url:'${pageContext.request.contextPath}/label/erWeiMa.do',
	                            dataType: 'JSON',
	                            async: false,
	                            success:function (data) {
	                            	path = path+data.path;
	                            }
	                        });
	                        $("#bd_img").attr("src",path)
	                        $.ajax({
	                            type:'post',
	                            data:'id='+data.warehouse_id,
	                            url:'${pageContext.request.contextPath}/cancellingStockManager/getWarehouseById.do',
	                            dataType: 'JSON',
	                            async: false,
	                            success:function (warehouse) {
	                            	if(warehouse.length > 0){
	                            		$("#d9").html(warehouse[0].userName);
	     	                        	$("#d11").html(warehouse[0].phone);
	                            	}
	                            }
	                        });
	                        var newData = new Array();
	                        $.ajax({
	                            type:'post',
	                            data:'out_stock_num_id='+data.id,
	                            url:'${pageContext.request.contextPath}/stockOutOrder/queryOutStockOrderDetailByIdForPDA.do',
	                            dataType: 'JSON',
	                            async: false,
	                            success:function (json) {
	                                console.log(json);
	                                if(json.status == 1){
		                            	newData = json.data;
	                                }
	                            }
	                        });
	                        table.reload('dytable',{
	                            data : newData
	                            ,limit:newData.length
	                        });
		                }
		            });
	            }
	        });
	        $("#dayingTable").click(function(){
				//var innerHTML = document.getElementById("dyDiv").innerHTML;
	        	//printpage(innerHTML)
			    var obj =document.getElementById('dyDiv')
				var newWindow=window.open("打印窗口","_blank");//打印窗口要换成页面的url
				var css='<style>'
	                //+"#dyTable td{border:1px solid #00000080;}"
	                +'[lay-id="dytable"] table{border:1px solid #F00;border-collapse:collapse;}'
	                +'[lay-id="dytable"] table td{border:1px solid #222;}'
	                +'[lay-id="dytable"] table th{border:1px solid #222;font-size: 20px;}'
	                +'[lay-id="dytable"] table th span{color: black;}'
	                +'</style>'; 
				var docStr =  css+ obj.innerHTML;
			    newWindow.document.write(docStr);
			    newWindow.document.close();
			    setTimeout(function () {
				    newWindow.print();
				    newWindow.close();
			    }, 100);
			});
			
			
			//"出库单详细"批量删除
			$("#dels1").click(function(){
				var rowData = table.checkStatus('contenttable1');
				var datas = rowData.data;
				var idArr = new Array();
				if(datas.length == 0){
					toastrStyle();
					toastr.warning("请至少选择一条记录！");
				} else {
					for(var i=0;i < datas.length;i++){
						idArr[i] = datas[i].id;
					}
					/* _____________________start______________________ */
					$.ajax({
    			    	type:'post',
    			    	url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderStatusByOutStockNumId.do',
    			    	data:{"out_stock_num_id" : datas[0].out_stock_num_id},
    			    	success:function(status){
    			    		if(status > 0){
    			    			toastrStyle();
    			    			toastr.warning("对不起，已出库的数据不能删除！");
    			    		} else {
    			    			layer.confirm('确定删除吗？', function(index){
    			    				$.ajax({
    			    			    	type:'post',
    			    			    	url:'${pageContext.request.contextPath }/stockOutOrder/deleteOutStockOrderDetailById1.do',
    			    			    	data:{"idArr" : idArr},
    			    			    	success:function(data){
    			    			    		if(data > 0){
    			    			    			toastrStyle();
    			    			    			toastr.success("删除成功！");
    			    			    			setTimeout(function(){  
    			    								window.location.reload();  
    			    							},1000);
    			    			    		}else{
    			    			    			toastrStyle();
    			    			    			toastr.error("删除失败！");
    			    			    			setTimeout(function(){  
    			    								window.location.reload();  
    			    							},1000);
    			    			    		}
    			    			    	}
    			    			    })
    			    		    	layer.close(index);
    			    			});
    			    		}
    			    	}
    			    })
    			    /* ______________________end_______________________ */
					
				}
			});
			  function curentTime() {
			      /*   var now=new Date();
			        var year = now.getFullYear();       //年
			        var month = now.getMonth() + 1;     //月
			        var day = now.getDate();            //日
			        var time=year+"-"+add0(month)+"-"+add0(day); */
			        var now=new Date();
			        var year=now.getFullYear(); 
				     var month=now.getMonth()+1; 
				     var date=now.getDate(); 
				     var hour=now.getHours(); 
				     var minute=now.getMinutes(); 
				     var second=now.getSeconds(); 
				     if (month >= 1 && month <= 9) {
				        month = "0" + month;
				    }
				    if (date >= 0 && date <= 9) {
				       date = "0" + date;
				    } 
				   if (hour >= 1 && hour <= 9) {
					  hour = "0" + hour;
				    }
				  if (minute >= 1 && minute <= 9) {
					 minute = "0" + minute;
				    }
				 if (second >= 1 && second <= 9) {
					second = "0" + second;
				    }
				var time =year+"-"+month+"-"+date+" "+hour+":"+minute+":"+second; 

			       return time;
			    }
			    
			
			//表格行折叠方法
	        function collapseTable(options) {
	            var trObj = options.elem;
	            if (!trObj) {
	                return;
	            }
	            var accordion = options.accordion,
	                success = options.success,
	                content = options.content || '';
	            var tableView = trObj.parents('.layui-table-view'); //当前表格视图
	            var id = tableView.attr('lay-id'); //当前表格标识
	            var index = trObj.data('index'); //当前行索引
	            var leftTr = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + index + '"]'); //左侧当前固定行
	            var rightTr = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + index + '"]'); //右侧当前固定行
	            var colspan = trObj.find('td').length; //获取合并长度
	            var trObjChildren = trObj.next(); //展开行Dom
	            var indexChildren = id + '-' + index + '-children'; //展开行索引
	            var leftTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-l tr[data-index="' + indexChildren + '"]'); //左侧展开固定行
	            var rightTrChildren = tableView.find('.layui-table-fixed.layui-table-fixed-r tr[data-index="' + indexChildren + '"]'); //右侧展开固定行
	            var lw = leftTr.width() + 15; //左宽
	            var rw = rightTr.width() + 15; //右宽
	            //不存在就创建展开行
	            if (trObjChildren.data('index') != indexChildren) {
	                //装载HTML元素
	                var tr = '<tr data-index="' + indexChildren + '"><td colspan="' + colspan + '"><div style="height: auto;padding-left:' + lw + 'px;padding-right:' + rw + 'px" class="layui-table-cell">' + content + '</div></td></tr>';
	                trObjChildren = trObj.after(tr).next().hide(); //隐藏展开行
	                var fixTr = '<tr data-index="' + indexChildren + '"></tr>';//固定行
	                leftTrChildren = leftTr.after(fixTr).next().hide(); //左固定
	                rightTrChildren = rightTr.after(fixTr).next().hide(); //右固定
	            }
	            //展开|折叠箭头图标
	            trObj.find('td[lay-event="collapse"] i.layui-colla-icon').toggleClass("layui-icon-right layui-icon-down");
	            //显示|隐藏展开行
	            trObjChildren.toggle();
	            //开启手风琴折叠和折叠箭头
	            if (accordion) {
	                trObj.siblings().find('td[lay-event="collapse"] i.layui-colla-icon').removeClass("layui-icon-down").addClass("layui-icon-right");
	                trObjChildren.siblings('[data-index$="-children"]').hide(); //展开
	                rightTrChildren.siblings('[data-index$="-children"]').hide(); //左固定
	                leftTrChildren.siblings('[data-index$="-children"]').hide(); //右固定
	            }
	            success(trObjChildren, indexChildren); //回调函数
	            heightChildren = trObjChildren.height(); //展开高度固定
	            rightTrChildren.height(heightChildren + 115).toggle(); //左固定
	            leftTrChildren.height(heightChildren + 115).toggle(); //右固定
	        }
		});
		
		//导入
		function uploadExcel(){
			layer.open({
                type: 1 		//Page层类型
                ,area: ['410px', '200px'] //宽  高
                ,title: '导入'
                ,shade: 0.6 	//遮罩透明度
                ,maxmin: true //允许全屏最小化
                ,anim: 1 		//0-6的动画形式，-1不开启
                ,content: $('#importDiv')
                ,end: function () {
                    var formDiv = document.getElementById('importDiv');
                    formDiv.style.display = '';
                }
            });
		}

        //日期格式转换
        function dateToStr(date) {
            var time = new Date(date.time);
            var y = time.getFullYear();
            var M = time.getMonth() + 1;
            M = M < 10 ? ("0" + M) : M;
            var d = time.getDate();
            d = d < 10 ? ("0" + d) : d;
            var str = y + "-" + M + "-" + d;
            return str;
        }
        
      	//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
			return date;
		}
      	
		function formatDate(time) { 
			var d = new Date(time);
        	var dformat = [ d.getFullYear(),
        	checkTime(d.getMonth() + 1), checkTime(d.getDate()) ].join('-');
        	return dformat;
    	};
    	
   	  	//修改时间戳查询时间
		function checkTime(n) {
			return n < 10 ? '0' + n : n;
   	  	};
   	 toastrStyle()
	</script>
</body>
</html>
