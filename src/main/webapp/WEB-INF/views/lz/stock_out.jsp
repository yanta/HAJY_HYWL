<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>出库列表</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>出库列表</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
		<div style="margin-bottom: 18px">
			<div class="layui-inline">
				<table>
					<tr>
						<%--
                        <td>
                            <input class="layui-input" name="receiveNumber" id="receiveNumberc" placeholder="请输入收货单编号" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px">
                        </td>
                        --%>
						<td>
							<!-- <input class="layui-input" name="sendCompany" id="sendCompanyc" placeholder="请输入供应商" autocomplete="off" style="display: inline; width: 180px; margin-left: 10px"> -->
							<!-- <div style="width: 190px; float: left;"> -->
							<div class="layui-form" style="width: 180px; margin-left: 10px">
								<select id="sendCompanyc" name="sendCompany">
									<!-- <select id="customer_id" name="customer_id" lay-verify="customer_id" lay-filter="customer_id" style="width: 190px"> -->
									<option value="">请选择主机厂</option>
									<c:forEach items="${customer_type1}" var="customer">
										<option value="${customer.id}">${customer.customer_name}</option>
									</c:forEach>
								</select>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</div>
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	</div>
	<div id="subTable" hidden="hidden">
	  <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
	</script>
	<script type="text/html" id="rowToolbar2">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del2"><i class="layui-icon layui-icon-delete"></i>删除</a>
	</script>
	<!-- 导入框 -->
	<div id="importDiv" hidden="hidden">
		<form class="layui-form" id="uploadform">
			<div style="margin: 20px 0 20px 140px;">
				<input id="fileInput" name="file" type="file" accept=".xls,.xlsx"/>
			</div>
		</form>
		<button class="layui-btn layui-btn-blue" lay-filter="uplodeform" style="margin-left: 80px">导入数据</button>&emsp;&emsp;&emsp;&emsp;
		<button type="button" class="layui-btn" id="downloadFile">下载模板</button>
	</div>

	<script type="text/javascript">
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword01":keyword01}
						,page: {
		                    curr: 1//重新从第 1 页开始
		                }
					});
				}
			}
			
			//llm新增，添加物料的查询
	       $(document).on('click','#selectMateriel',function(){
	        	var materiel_name=$('#materiel_name').val();
	    		var materiel_num=$('#materiel_num').val();
	    		var table = layui.table;
	    		table.reload('inventoryInfoTable', {
	         		method:'post'
	         		,where:{"materielName":materiel_name,"materielNum":materiel_num}
	         		,page: {
	                    curr: 1//重新从第 1 页开始
	                }
	    		});
			 }) 
				 
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteria.do'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,id :'contenttable'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
				,cols: [[
                    {type: 'checkbox', fixed: 'left', },
                    {field:'', title:'序号', type:'numbers', align:'center'},
                    {field:'out_stock_num', title:'出库单号', align:'center'},
                    {field:'create_date', title:'创建时间', align:'center'},
                    {field:'warehouse_name', title:'仓库名称', align:'center'},
                    {field:'warehouse_address', title:'仓库地址', align:'center'},
                    //{field:'userName', title:'仓库管理员', align:'center'},
                    //{field:'phone', title:'管理员电话', align:'center'},
                    {field:'customer_name', title:'主机厂名称', align:'center'},
                    {field:'customer_adress', title:'主机厂地址', align:'center'},
                    {field:'contacts_name', title:'主机厂负责人', align:'center'},
                    {field:'contacts_tel', title:'负责人电话', align:'center'},
                    {field:'status', title:'状态', align:'center', templet: function (row){
   	                    if (row.status == '0') {
   	                        return "未出库";
   	                    } else {
   	                        return "已出库";
   	                    }
   	                }}
			  	]]
				,page: true
                ,done : function(){
                    $(".timepicker").each(function () {
						laydate.render({
							elem: this,
							format: "yyyy-MM-dd"
						});
					});
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
			});
			
            //出库单行单击事件
            table.on('row(tableList)', function(obj) {
            	$("#subTable").show();
                var data = obj.data;
                table.render({
                    elem: '#tableListSub'
                    ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById.do?keyword01='+data.id
                    ,toolbar: '#toolbar'
                    ,title: '出库单详细表'
                    ,id :'contenttable1'
                    ,limits:[10,20,30]
                    ,defaultToolbar:['filter', 'print']
                    ,cols: [[
   	   				  	{type: 'checkbox', fixed: 'left', },
   	   				  	{field:'customer_name', title:'供应商名称', hide:true},
   	   				  	{field:'materiel_name', title:'物料名称', align:'center'},
   	   				 	{field:'materiel_num', title:'产品码', align:'center'},
   		   				{field:'brevity_num', title:'物料简码', align:'center'},
   		   				{field:'materiel_size', title:'物料规格', align:'center'},
   		   				{field:'materiel_properties', title:'物料型号', align:'center'},
   	   				  	{field:'material_quantity', title:'物料数量', align:'center'},
   	   				  	{field:'actual_quantity', title:'实际数量', align:'center'},
        			]]
                    ,page: true
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });

            //监听复选框事件
            table.on('checkbox(tableList)',function(obj){
                if(obj.checked == true && obj.type == 'all'){
                    //点击全选
                    $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
                }else if(obj.checked == false && obj.type == 'all'){
                    //点击全不选
                    $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
                }else if(obj.checked == true && obj.type == 'one'){
                    //点击单行
                    if(obj.checked == true){
                        obj.tr.addClass('layui-table-click');
                    }else{
                        obj.tr.removeClass('layui-table-click');
                    }
                } else if (obj.checked == false && obj.type == 'one'){
                    //点击全选之后点击单行
                    if(obj.tr.hasClass('layui-table-click')){
                        obj.tr.removeClass('layui-table-click');
                    }
                }
            })

            laydate.render({
				elem: '#keyword01',
			});
		});
	</script>
</body>
</html>
