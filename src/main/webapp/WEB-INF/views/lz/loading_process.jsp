<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>装车处理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>装车处理</cite>
        </a>
      </span>
</div>
<div class="layui-tab">
  <ul class="layui-tab-title">
    <li class="layui-this">一般出库单装车</li>
    <li>异常补货单装车</li>
  </ul>
  <div class="layui-tab-content">
    <div class="layui-tab-item layui-show">
        <div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" id="keyword01" style="width: 210px;" placeholder="请选择时间">
					</td>
				</tr>
			</table>
		</div>
        <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
    	<table class="layui-hide" id="tableList" lay-filter="tableList"></table>
		<table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
		<script type="text/html" id="rowToolbar">
        	<a class="layui-btn layui-btn-xs" lay-event="loadingProcess"><i class="layui-icon layui-icon-edit"></i>装车处理</a>
    	</script>
    </div>
    <div class="layui-tab-item">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" id="keyword001" style="width: 210px;" placeholder="请选择年月">
					</td>
				</tr>
			</table>
		</div>
        <button class="layui-btn layui-btn-normal" data-type="reload1"><i class="layui-icon">&#xe615;</i>检索</button>
    	<table class="layui-hide" id="tableList1" lay-filter="tableList1"></table>
		<table class="layui-hide" id="tableListSub1" lay-filter="tableListSub1"></table>
		<script type="text/html" id="rowToolbar1">
        	<a class="layui-btn layui-btn-xs" lay-event="loadingProcess1"><i class="layui-icon layui-icon-edit"></i>装车处理</a>
    	</script>
    </div>
  </div>
</div>
<script type="text/javascript">
    layui.use(['element','table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var element = layui.element;
        var laydate = layui.laydate;
        
        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword01").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            },
	        reload1:function () {
	            var keyword001 = $("#keyword001").val();
	            table.reload('contenttable1',{
	                method:'get',
	                where:{"keyword01":keyword001}
	            });
	        }
        }
        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
			elem: '#tableList'
			,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteria2.do'
			,toolbar: '#toolbar'
			,title: 'machineList'
			,id :'contenttable'
			,limits:[10,20,30]
            ,defaultToolbar:['filter', 'print']
			,cols: [[
			  {type: 'checkbox', fixed: 'left', },
			  {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
			  {field:'out_stock_num', title:'出库单号', align:'center'},
			  /* {field:'business_order_num', title:'业务单号', align:'center'}, */
			  {field:'out_stock_status', title:'出库状态', align:'center', templet: function (row){
				  if(row.out_stock_status == '0'){
					  return '未出库';
				  } else {
					  return '已出库';
				  }
			  }},
			 /*{field:'out_stock_type', title:'出库类型', align:'center', templet: function (row){
				  if(row.out_stock_status == '0'){
					  return '一般出库';
				  } else {
					  return '紧急出库';
				  }
			  }}, */
			  {field:'customer_name', title:'主机厂名称', align:'center'},
			  {field:'customer_adress', title:'主机厂地址', align:'center'},
			  {field:'contacts_name', title:'主机厂负责人', align:'center'},
			  {field:'create_date', title:'创建时间', align:'center'},
			  {fixed:'right', title:'操作', toolbar: '#rowToolbar', width: 130, align:'center'}
			]]
			,page: true
            ,done : function(){
                $(".timepicker").each(function () {
					laydate.render({
						elem: this,
						format: "yyyy-MM-dd"
					});
				});
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
		});
        
        //出库单行单击事件
        table.on('row(tableList)', function(obj) {
        	$("#subTable").show();
            var data = obj.data;
            //单击每一行的时候将该行的id传入
            //mIdArr.push(data.id);
            $("#firstTableId").val(data.id);
            table.render({
                elem: '#tableListSub'
                ,url:'${pageContext.request.contextPath }/stockOutOrder/queryOutStockOrderCriteriaById.do?keyword01='+data.id
                ,toolbar: '#toolbar'
                ,title: '出库单详细表'
                ,id :'contenttable1'
                ,limits:[10,20,30]
                ,defaultToolbar:['filter', 'print']
                ,cols: [[
   				  	{type: 'checkbox', fixed: 'left', },
   				  	{field:'customer_name', title:'供应商名称', hide:true},
   				  	{field:'materiel_name', title:'物料名称', align:'center'},
   				 	{field:'materiel_num', title:'产品码', align:'center'},
	   				{field:'brevity_num', title:'物料简码', align:'center'},
	   				{field:'materiel_size', title:'物料规格', align:'center'},
	   				{field:'materiel_properties', title:'物料型号', align:'center'},
   				  	{field:'material_quantity', title:'物料数量', align:'center', templet: function (row){
						return '<span style="color: gray">'+row.material_quantity+'</span>';
					}},
   				  	{field:'actual_quantity', title:'实际数量', align:'center'},
   				  	/* {field:'status', title:'状态', align:'center', templet: function (row){
   	                    if (row.status == '0') {
   	                        return "未退库";
   	                    } else {
   	                        return "已退库";
   	                    }
   	                }}, */
 				]]
                ,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
            });
        });
        form.on('select(car_id)', function(data) {
			
        	console.log("**************"+data.value)
			$.ajax({
                type: 'POST',
                url: '${pageContext.request.contextPath}/stockOutOrder/queryDriver.do?',
               data: {id:data.value},
                dataType: 'json',
                async: false,
                success: function (datas){
                   
                	console.log(datas[0])
                	$("#driver_id").val(datas[0].owner);
                }
            });
		})
        
      	//监听行工具事件
        table.on('tool(tableList)', function(obj){
        	var data = obj.data;
            if(obj.event === 'loadingProcess'){
            	var timestamp = (new Date()).getTime();
            	layer.open({
                    type: 1 					//Page层类型
                    ,area: ['670px', ''] 	//宽  高
                    ,title: '装车处理'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="addDivID">'+
							'<form class="layui-form" id="addFormID">'+
								'<xblock>'+
									'<table>'+
										'<tr>'+
											'<td><input class="layui-hide" id="out_order_id" name="out_order_id" value="'+data.id+'"></td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">装车单号</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="loading_process_num" name="loading_process_num" value="ZCDH-'+timestamp+'" style="color: gray; width: 190px">'+
											'</td>'+
											'<td><label class="layui-form-label">装车类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
			                                        '<select id="loading_process_type" name="loading_process_type" lay-verify="loading_process_type">' +
			                                            '<option value="">请选择</option>' +
			                                            '<option value="0">一般装车</option>' +
			                                            '<option value="1">补货装车</option>' +
			                                        '</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
										'</tr>'+
	                       				'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">车牌号</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
			                                        '<select id="car_id" name="car_id" lay-verify="car_id"lay-filter="car_id">' +
			                                            '<option value="">请选择</option>' +
			                                        '</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+	
											'<td><label class="layui-form-label">司机姓名</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
												'<input class="layui-input" id="driver_id" name="driver_id" lay-verify="driver_id" style="width: 190px; display:inline" placeholder="司机姓名">'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">装车时间</label></td>'+
											'<td>'+
												'<input class="layui-input" id="loading_process_date" name="loading_process_date" lay-verify="loading_process_date" style="width: 190px; display:inline" placeholder="请选择装车时间">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">客户</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="customer_name" name="customer_name" value="'+data.customer_name+'" style="width: 190px">'+
											'</td>'+
										'</tr>'+
										'<tr style="height:20px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">客户地址</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="customer_adress" name="customer_adress" value="'+data.customer_adress+'" style="width: 190px">'+
											'</td>'+
											'<td><label class="layui-form-label">装车备注</label></td>'+
											'<td>'+
												'<input class="layui-input" name="remark" style="width: 190px">'+
											'</td>'+
										'</tr>'+
										'<tr style="height:20px"></tr>'+
										'<tr align="center">'+
											'<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 20px">提交</button></td>'+
											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button type="reset" class="layui-btn layui-btn-primary" style="margin-bottom: 20px">重置</button></td>'+
										'</tr>'+
									'</table>'+
								'<xblock>'+
							'</form>'+
                    	'</div>'
                    ,success: function(){
                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCar.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#car_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].plate +"</option>");
                                	//$("#driver_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].owner +"</option>");
                                	//$("#driver_id").val(datas[i].owner)
                                }
                            }
                        });

            			
                        
                      /*   $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllStaff.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#driver_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].name +"</option>");
                                }
                            }
                        }); */
                        
                        laydate.render({
                            elem: '#loading_process_date'
                        });
                        form.render();
                    }
                });
            }
        });
        
        /**
		 * 新增表单校验 
		 */
		form.verify({
			//value：表单的值item：表单的DOM对象
			car_id: function(value, item){
				if(value == ''){
					return '车牌号不能为空';
				}
			},
			loading_process_type: function(value, item){
				if(value == ''){
					return '装车类型不能为空';
				}
			},
			driver_id: function(value, item){
				if(value == ''){
					return '司机姓名不能为空';
				}
			},
			loading_process_date: function(value, item){
				if(value == ''){
					return '装车时间不能为空';
				}
			}
		});
      	
		/* -----------------------第二个选项卡----------------------- */
        table.render({
			elem: '#tableList1'
			,url:'${pageContext.request.contextPath }/abnormalAdjustment/queryAbnormalAdjustmentCriteria.do'
			,toolbar: '#toolbar'
			,title: 'machineList'
			,id :'contenttable2'
			,limits:[10,20,30]
               ,defaultToolbar:['filter', 'print']
               ,cols: [[
                   {type: 'checkbox', fixed: 'left', },
                   {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
                   {field:'out_stock_order_num', title:'出库单号', align:'center'},
                   /* {field:'business_order_num', title:'业务单号', align:'center'}, */
                   {field:'replenish_time', title:'补发日期', align:'center', templet: function (row){
                   	return '<a style="color: gray" class="timepicker" id="replenish_time" name="replenish_time">'+row.replenish_time+'</a>';
				}},
                   {field:'replenish_man', title:'补发人', align:'center', templet: function (row){
					return '<span style="color: gray">'+row.replenish_man+'</span>';
				}},
                   {field:'replenish_reason', title:'补发原因', align:'center', templet: function (row){
					return '<span style="color: gray">'+row.replenish_reason+'</span>';
				}},
				{field:'remark', title:'补发状态', align:'center', templet: function (row){
					if(row.remark == '0'){
						return '<span style="color: gray">未完成</span>';
					} else {
						return '<span style="color: gray">已完成</span>';
					}
				}},
				{fixed:'right', title:'操作', toolbar: '#rowToolbar1', width: 130, align:'center'}
               ]]
               ,page: true
               ,done : function(){
                   /* $(".timepicker").each(function () {
                       laydate.render({
                           elem: this,
                           format: "yyyy-MM-dd"
                       });
                   }); */
                   $('th').css({
                       'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                   })
               }
		});
        
    	//行单击事件
        table.on('row(tableList1)', function(obj) {
            var data = obj.data;
            console.log(data.id);
            table.render({
                elem: '#tableListSub1'
                ,url:'${pageContext.request.contextPath }/abnormalAdjustment/queryAbnormalDetail.do?aid='+data.id
                ,toolbar: '#toolbar'
                ,title: 'machineList'
                //,id :'contenttable'
                
                ,defaultToolbar:['filter', 'print']
                ,cols: [[
                    //{type: 'checkbox', fixed: 'left'},
                    {field:'id1', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
                    {field:'id', title:'ID', align:'center', hide:true}, //隐藏不显示
                    //{field:'customer_id', title:'供应商ID', align:'center', hide:true}, //隐藏不显示
                    {field:'customer_name', title:'供应商名称', align:'center'}, //隐藏不显示
                    {field:'region_name', title:'库区名称', align:'center'}, //隐藏不显
                    //{field:'customer_name', title:'供应商名称', align:'center'},
                    {field:'materiel_id', title:'物料ID', align:'center', hide:true}, //隐藏不显示
                    {field:'materiel_name', title:'物料名称', align:'center'},
                    {field:'materiel_num', title:'物料编码', align:'center'},
                    {field:'materiel_size', title:'物料规格', align:'center'},
                    {field:'materiel_properties', title:'物料型号', align:'center'},
                    //{field:'unit', title:'单位', align:'center'},
					{field:'knum', title:'库存数量', align:'center'},
                    {field:'bnum', title:'补发数量', align:'center', templet: function (row){
						return '<span style="color: blue">'+row.bnum+'</span>';
					}},
					
                    //{fixed:'right', title:'操作', toolbar: '#rowToolbar2', width: 80, align:'center'}
                ]]
        
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
            });
        });
  /*     	//监听行工具事件
        table.on('tool(tableList1)', function(obj){
        	var data = obj.data;
        	$.ajax({
				type: "get",
                url: "${pageContext.request.contextPath }/loadingProcess/queryOutStockOrderByOutStockOrderNum.do",
                data: {out_stock_order_num:data.out_stock_order_num},
                dataType: 'json',
                async: false,
                success: function(data){
                	//console.log(data + "," +data[0].customer + "," + data[0].address);
                	if(obj.event === 'loadingProcess1'){
                    	var timestamp = (new Date()).getTime();
                    	layer.open({
                            type: 1 				//Page层类型
                            ,area: ['670px', ''] 	//宽  高 
                            ,title: '装车处理'
                            ,shade: 0.6 			//遮罩透明度
                            ,maxmin: true 			//允许全屏最小化
                            ,anim: 1 				//0-6的动画形式，-1不开启
                            ,content:
                                '<div id="addDivID">'+
        							'<form class="layui-form" id="addFormID1">'+
        								'<xblock>'+
        									'<table>'+
	        									'<tr>'+
													'<td><input class="layui-hide" id="out_order_id" name="out_order_id" value="'+data.id+'"></td>'+
												'</tr>'+
												'<tr style="height: 10px"></tr>'+
        										'<tr>'+
	    											'<td><label class="layui-form-label">装车单号</label></td>'+
	    											'<td>'+
	    												'<input readonly class="layui-input" id="loading_process_num" name="loading_process_num" value="ZCDH-'+timestamp+'" style="color: gray; width: 190px">'+
	    											'</td>'+
	    											'<td><label class="layui-form-label">装车类型</label></td>'+
	    											'<td>'+
	    												'<div style="width: 190px; float: left;">' +
	    			                                        '<select id="loading_process_type" name="loading_process_type" lay-verify="loading_process_type">' +
	    			                                            '<option value="">请选择</option>' +
	    			                                            '<option value="0">一般装车</option>' +
	    			                                            '<option value="1">补货装车</option>' +
	    			                                        '</select>'+
	    			                                    '</div>'+
	    			                                    '<div style="margin-top: 12px;">'+
	    			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	    			                                    '</div>'+
	    											'</td>'+
	    										'</tr>'+
	    	                       				'<tr style="height: 10px"></tr>'+
	    										'<tr>'+
		    										'<td><label class="layui-form-label">车牌号</label></td>'+
	    											'<td>'+
	    												'<div style="width: 190px; float: left;">' +
	    			                                        '<select id="car_id" name="car_id" lay-verify="car_id"lay-filter="car_id">' +
	    			                                            '<option value="">请选择</option>' +
	    			                                        '</select>'+
	    			                                    '</div>'+
	    			                                    '<div style="margin-top: 12px;">'+
	    			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	    			                                    '</div>'+
	    											'</td>'+
	    											'<td><label class="layui-form-label">司机姓名</label></td>'+
	    											'<td>'+
	    												'<div style="width: 190px; float: left;">' +
	    												'<input class="layui-input" id="driver_id" name="driver_id" lay-verify="driver_id" style="width: 190px; display:inline" placeholder="司机姓名">'+
	    			                                    '</div>'+
	    			                                    '<div style="margin-top: 12px;">'+
	    			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	    			                                    '</div>'+
	    											'</td>'+
	    										'</tr>'+
	    										'<tr style="height: 10px"></tr>'+
	    										'<tr>'+
	    											'<td><label class="layui-form-label">装车时间</label></td>'+
	    											'<td>'+
	    												'<input class="layui-input" id="loading_process_date" name="loading_process_date" lay-verify="loading_process_date" style="width: 190px; display:inline" placeholder="请选择装车时间">'+
	    												'<div class="layui-inline" style="margin-top: 10px;">'+
	    		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	    		                                        '</div>'+
	    											'</td>'+
	    											'<td><label class="layui-form-label">客户</label></td>'+
	    											'<td>'+
	    												'<input readonly class="layui-input" id="customer_name" name="customer_name" value="'+data[0].customer+'" style="width: 190px">'+
	    											'</td>'+
	    										'</tr>'+
	    										'<tr style="height:20px"></tr>'+
	    										'<tr>'+
		    										'<td><label class="layui-form-label">客户地址</label></td>'+
	    											'<td>'+
	    												'<input readonly class="layui-input" id="customer_adress" name="customer_adress" value="'+data[0].address+'" style="width: 190px">'+
	    											'</td>'+
	    											'<td><label class="layui-form-label">装车备注</label></td>'+
	    											'<td>'+
	    												'<input class="layui-input" name="remark" style="width: 190px">'+
	    											'</td>'+
	    										'</tr>'+
        										'<tr style="height:20px"></tr>'+
        										'<tr align="center">'+
        											'<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn1" lay-submit lay-filter="addForm1" style="margin-left:160px; margin-bottom: 20px">提交</button></td>'+
        											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button type="reset" class="layui-btn layui-btn-primary" style="margin-bottom: 20px">重置</button></td>'+
        										'</tr>'+
        									'</table>'+
        								'<xblock>'+
        							'</form>'+
                            	'</div>'
                            ,success: function(){
                            	$.ajax({
                                    type: 'POST',
                                    url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCar.do',
                                    dataType: 'json',
                                    async: false,
                                    success: function (datas){
                                        for (var i = 0; i < datas.length; i++) {
                                        	$("#car_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].plate +"</option>");
                                        }
                                    }
                                });
                                
                                $.ajax({
                                    type: 'POST',
                                    url: '${pageContext.request.contextPath}/stockOutOrder/queryAllStaff.do',
                                    dataType: 'json',
                                    async: false,
                                    success: function (datas){
                                        for (var i = 0; i < datas.length; i++) {
                                        	$("#driver_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].name +"</option>");
                                        }
                                    }
                                });
                                
                                laydate.render({
                                    elem: '#loading_process_date'
                                });
                                form.render();
                            }
                        });
                    }
	             }
            });
        }); */
      	//监听行工具事件
        table.on('tool(tableList1)', function(obj){
        	var data = obj.data;
            if(obj.event === 'loadingProcess1'){
            	var timestamp = (new Date()).getTime();
            	layer.open({
                    type: 1 					//Page层类型
                    ,area: ['670px', ''] 	//宽  高
                    ,title: '装车处理'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="addDivID">'+
							'<form class="layui-form" id="addFormID1">'+
								'<xblock>'+
									'<table>'+
										'<tr>'+
											'<td><input class="layui-hide" id="out_order_id" name="out_order_id" value="'+data.id+'"></td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">装车单号</label></td>'+
											'<td>'+
												'<input readonly class="layui-input" id="loading_process_num" name="loading_process_num" value="ZCDH-'+timestamp+'" style="color: gray; width: 190px">'+
											'</td>'+
											'<td><label class="layui-form-label">装车类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
			                                        '<select id="loading_process_type" name="loading_process_type" lay-verify="loading_process_type">' +
			                                            '<option value="">请选择</option>' +
			                                            '<option value="0">一般装车</option>' +
			                                            '<option value="1">补货装车</option>' +
			                                        '</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
										'</tr>'+
	                       				'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">车牌号</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
			                                        '<select id="car_id" name="car_id" lay-verify="car_id"lay-filter="car_id">' +
			                                            '<option value="">请选择</option>' +
			                                        '</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+	
											'<td><label class="layui-form-label">司机姓名</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
												'<input class="layui-input" id="driver_id" name="driver_id" lay-verify="driver_id" style="width: 190px; display:inline" placeholder="司机姓名">'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">装车时间</label></td>'+
											'<td>'+
												'<input class="layui-input" id="loading_process_date" name="loading_process_date" lay-verify="loading_process_date" style="width: 190px; display:inline" placeholder="请选择装车时间">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
											
											'<td><label class="layui-form-label">装车备注</label></td>'+
											'<td>'+
												'<input class="layui-input" name="remark" style="width: 190px">'+
											'</td>'+
										'</tr>'+
										'<tr style="height:20px"></tr>'+
										'<tr align="center">'+
											'<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm1" style="margin-left:160px; margin-bottom: 20px">提交</button></td>'+
											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button type="reset" class="layui-btn layui-btn-primary" style="margin-bottom: 20px">重置</button></td>'+
										'</tr>'+
									'</table>'+
								'<xblock>'+
							'</form>'+
                    	'</div>'
                    ,success: function(){
                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCar.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#car_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].plate +"</option>");
                                	//$("#driver_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].owner +"</option>");
                                	//$("#driver_id").val(datas[i].owner)
                                }
                            }
                        });

            			
                        
                      /*   $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllStaff.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#driver_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].name +"</option>");
                                }
                            }
                        }); */
                        
                        laydate.render({
                            elem: '#loading_process_date'
                        });
                        form.render();
                    }
                });
            }
        });
        
        //监听复选框事件
        table.on('checkbox(tableList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })
        
        laydate.render({
			elem: '#keyword01',
			
		});
        
        laydate.render({
			elem: '#keyword001',
			
		});
        
        /**
		 * 通用表单提交(AJAX方式)(一般出库单新增)
		 */
		form.on('submit(addForm)', function (data) {
			$.ajax({
				url : '${pageContext.request.contextPath}/loadingProcess/addLoadingProcess.do',
				data: $("#addFormID").serialize(),
				cache : false,
				type : "post",
			}).done(
                function(res) {
                    if (res > 0) {
                    	toastrStyle();
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                	toastrStyle();
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
		});
        
		/**
		 * 通用表单提交(AJAX方式)(异常补货单新增)
		 */
		form.on('submit(addForm1)', function (data) {
			$.ajax({
				url : '${pageContext.request.contextPath}/loadingProcess/addLoadingProcess.do',
				data: $("#addFormID1").serialize(),
				cache : false,
				type : "post",
			}).done(
                function(res) {
                    if (res > 0) {
                    	toastrStyle();
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                	toastrStyle();
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
		});
    });
</script>
</body>
</html>