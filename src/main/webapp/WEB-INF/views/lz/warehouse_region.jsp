<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>库区信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>库区信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入库区名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
    <!-- <a class="layui-btn layui-btn-xs" lay-event="bindingMateriel"><i class="layui-icon layui-icon-edit"></i>绑定物料</a> -->
    <div id="formDiv2" hidden="hidden">
        <div class="box">
            <form class="layui-form layui-card-body" id="addform2">
                <input hidden id="id2" name="id">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">物料</label>
                        <div class="layui-input-inline" style="width: 285px">
                            <select style="width: 285px" class="" name="ware" id="ware" lay-filter="ware" xm-select="select2"></select>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" id="subform2">确定</button>
                        <button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword = $("#keyword").val();
                var strWhere = " region_name like '%"+keyword+"%' and  ";
                table.reload('contenttable',{
                    method:'get',
                    where:{"strWhere":strWhere}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/warehouse/queryWarehouseRegionCriteria.do'
            ,title: '库区列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,toolbar: '#toolbar'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'warehouse_name', title:'仓库名称'}
                ,{field:'region_name', title:'库区名称'}
                /* ,{field:'storageRack_name', title:'货架名称'} */
                ,{field:'region_num', title:'库区编号'}
                ,{field:'region_type', title:'库区类型', templet: function (row){
                    if (row.region_type == '0') {
                        return "货架";
                    } else if (row.region_type == '1') {
                        return "平地";
                    } else if (row.region_type == '-1') {
                        return "不良品库";
                    }
                }}
                ,{field:'region_frequency', title:'库区频率', templet: function (row){
                    if (row.region_frequency == '0') {
                        return "低频";
                    } else if (row.region_frequency == '1') {
                        return "中频";
                    } else {
                        return "高频";
                    }
                }}
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width: 200, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            warehouse_id: function(value, item){
                if(value == ''){
                    return '仓库名称不能为空';
                }
            },
            region_name: function(value, item){
                if(value == ''){
                    return '库区名称不能为空';
                }
            },
            region_num: function(value, item){
                if(value == ''){
                    return '库区编号不能为空';
                }
            },
            region_type: function(value, item){
                if(value == ''){
                    return '库区类型不能为空';
                }
            },
            region_frequency: function(value, item){
                if(value == ''){
                    return '库区频率不能为空';
                }
            }/* ,
            region_status: function(value, item){
                if(value == ''){
                    return '库区状态不能为空';
                }
            } */
        });
        
      	//新增
        $("#add").click(function(){
            layer.open({
                type: 1 				//Page层类型
                ,area: ['450px', ''] 	//宽  高
                ,title: '新增'
                ,shade: 0.6 			//遮罩透明度
                ,maxmin: true 			//允许全屏最小化
                ,anim: 1 				//0-6的动画形式，-1不开启
                ,content:
                    '<div id="addDivID">'+
                        '<form class="layui-form" id="addFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">仓库名称</label>'+
	                                        '<div style="width: 285px; float: left;">' +
		                                        '<select id="warehouse_id" name="warehouse_id" lay-verify="warehouse_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                        '<td><label class="layui-form-label">库区名称</label>'+
                                            '<input class="layui-input" id="region_name" name="region_name" lay-verify="region_name" style="width: 285px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        /* 
                                        '<td><label class="layui-form-label">货架名称</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="storageRack_name" name="storageRack_name" lay-verify="storageRack_name" style="width: 285px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+ 
                                        */
                                        '<td><label class="layui-form-label">库区类型</label>'+
		                                    '<div style="width: 285px; float: left;">' +
		                                        '<select id="region_type" name="region_type" lay-verify="region_type">' +
		                                            '<option value="">请选择</option>' +
		                                            '<option value="0">货架</option>' +
		                                            '<option value="1">平地</option>' +
		                                            '<option value="-1">不良品库</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                        '<td><label class="layui-form-label">库区编号</label>'+
                                            '<input class="layui-input" id="region_num" name="region_num" lay-verify="region_num" style="width: 285px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">库区频率</label>'+
                                            '<div style="width: 285px; float: left;">' +
                                                '<select id="region_frequency" name="region_frequency" lay-verify="region_frequency">' +
                                                	'<option value="">请选择</option>' +
                                                    '<option value="0">低频</option>' +
                                                    '<option value="1">中频</option>' +
                                                    '<option value="2">高频</option>' +
                                                '</select>'+
                                            '</div>'+
                                            '<div style="margin-top: 12px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
	                                    '<td><label class="layui-form-label">备注</label>'+
                                            '<input class="layui-input" id="remark" name="remark" style="width: 285px">'+
                                        '</td>'+
	                                    /* '<td><label class="layui-form-label">库区状态</label></td>'+
                                        '<td>'+
                                            '<div style="width: 285px; float: left;">' +
                                                '<select id="region_status" name="region_status" lay-verify="region_status">' +
                                                    '<option value="">请选择仓库状态</option>' +
                                                    '<option value="0">可用</option>' +
                                                    '<option value="1">不可用</option>' +
                                                '</select>'+
                                            '</div>'+
                                            '<div style="margin-top: 12px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+ */
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr align="center">'+
                                        '<td><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:70px; margin-bottom: 10px">提交</button>'+
                                        '&emsp;&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                	//下拉框查找所有仓库
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/warehouse/queryAllWarehouse.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#warehouse_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].warehouse_name +"</option>");
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    form.render();
                }
            });
        });

        //编辑
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            if (obj.event === 'update') {
                layer.open({
                    type: 1 					//Page层类型
                    ,area: ['450px', ''] 	//宽  高
                    ,title: '编辑'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="updateDivID">'+
                            '<form class="layui-form" id="updateFormID">'+
                                '<xblock>'+
                                    '<table>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td>'+
                                                '<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">仓库名称</label>'+
                                                '<div style="width: 285px; float: left;">' +
                                                    '<select id="warehouse_id" name="warehouse_id" lay-verify="warehouse_id">' +
                                                        '<option value="">请选择</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                            '<td><label class="layui-form-label">库区名称</label>'+
                                                '<input class="layui-input" id="region_name" name="region_name" value="'+data.region_name+'" lay-verify="region_name" style="width: 285px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            /*
                                            '<td><label class="layui-form-label">货架名称</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="storageRack_name" name="storageRack_name" value="'+data.storageRack_name+'" lay-verify="storageRack_name" style="width: 285px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                            */
                                            '<td><label class="layui-form-label">库区类型</label>'+
                                                '<div style="width: 285px; float: left;">' +
                                                    '<select id="region_type" name="region_type" lay-verify="region_type">' +
                                                    	'<option value="">请选择</option>' +
                                                        '<option value="0">货架</option>' +
                                                        '<option value="1">平地</option>' +
                                                        '<option value="-1">不良品库</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                            '<td><label class="layui-form-label">库区编号</label>'+
                                                '<input class="layui-input" id="region_num" name="region_num" value="'+data.region_num+'" lay-verify="region_num" style="width: 285px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">库区频率</label>'+
                                                '<div style="width: 285px; float: left;">' +
                                                    '<select id="region_frequency" name="region_frequency" lay-verify="region_frequency">' +
                                                    	'<option value="">请选择</option>' +
                                                        '<option value="0">低频</option>' +
                                                        '<option value="1">中频</option>' +
                                                        '<option value="2">高频</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                            '<td><label class="layui-form-label">备注</label>'+
                                                '<input class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 285px">'+
                                            '</td>'+
                                            /* '<td><label class="layui-form-label">库区状态</label></td>'+
                                            '<td>'+
                                                '<div style="width: 285px; float: left;">' +
                                                    '<select id="region_status" name="region_status" lay-verify="region_status">' +
                                                        '<option value="">请选择仓库状态</option>' +
                                                        '<option value="0">可用</option>' +
                                                        '<option value="1">不可用</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+ */
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr align="center">'+
                                            '<td><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:70px; margin-bottom: 10px">提交</button>'+
                                            '&emsp;&emsp;&emsp;&emsp;&emsp;<button  class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                        '</tr>'+
                                    '</table>'+
                                '</xblock>'+
                            '</form>'+
                        '</div>'
                    ,success: function(){
                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/warehouse/queryAllWarehouse.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                    $("#warehouse_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].warehouse_name +"</option>");
                                    if(datas[i].id == data.warehouse_id){
                                        $("#warehouse_id").val(data.warehouse_id);
                                    }
                                }
                            }
                        });
                        //库区类型
                        $("#region_type").val(data.region_type);
                        //出库频率
                        $("#region_frequency").val(data.region_frequency);
                        laydate.render({
                            elem: '#create_time'
                        });
                        form.render();
                    }
                });
            } else if (obj.event === 'bindingMateriel'){
                layer.open({
                    type: 1 				//Page层类型
                    ,area: ['500px', ''] 	//宽  高
                    ,title: '绑定物料'
                    ,shade: 0.6 			//遮罩透明度
                    ,maxmin: true 			//允许全屏最小化
                    ,anim: 1 				//0-6的动画形式，-1不开启
                    ,content:$('#formDiv2')
                    ,success:function(){
                        $("#id2").val(data.id);
                        $.ajax({
                            type:'post'
                            ,url:'${pageContext.request.contextPath }/materiel/wareTree.do?regionType='+data.placement_type
                            ,dataType:'json'
                            ,success:function(data){
                                formSelects.data('select2', 'local', {
                                    arr: data.rows,
                                    linkage: true
                                });
                            }
                        })

                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomer.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                    $("#client2").append("<option value='"+ datas[i].customer_name +"'>"+ datas[i].customer_name +"</option>");
                                    if(datas[i].customer_name == data.client){
                                        $("#client2").val(data.client);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });

        //批量删除
        $("#del").click(function(){
            var rowData = table.checkStatus('contenttable');
            var data = rowData.data;
            var idArr = new Array();
            if(data.length == 0){
                toastrStyle();
                toastr.warning("请至少选择一条记录！");
            } else {
                for(var i=0;i < data.length;i++){
                    idArr[i] = data[i].id;
                }
                layer.confirm('确定删除吗？', function(){
                $.ajax({
                    type:'post',
                    url:'${pageContext.request.contextPath }/warehouse/deleteWarehouseRegionById.do',
                    data:{"idArr" : idArr},
                    success:function(data){
                        
                            if(data > 0){
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                                setTimeout(function(){
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                                location.reload();
                            }
                        
	                    }
	                });
                });
            }
        });

        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(addForm)', function () {
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/warehouse/addWarehouseRegion.do',
                data:$("#addFormID").serialize(),
                cache:false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })

        /**
         * 通用表单编辑(AJAX方式)
         */
        form.on('submit(updateForm)', function () {
            $.ajax({
                type : 'post',
                url : '${pageContext.request.contextPath }/warehouse/updateWarehouseRegion.do',
                data : $("#updateFormID").serialize(),
                cache : false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('修改成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('修改失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function dateToStr(date) {
        var time = new Date(date.time);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        M = M < 10 ? ("0" + M) : M;
        var d = time.getDate();
        d = d < 10 ? ("0" + d) : d;
        var str = y + "-" + M + "-" + d;
        return str;
    }
</script>
</body>

</html>
