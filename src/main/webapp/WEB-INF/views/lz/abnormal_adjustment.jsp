<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>异常调整</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
    <div class="x-nav">
      <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
          <cite>异常处理</cite>
        </a>
      </span>
    </div>
    <div class="x-body">
      <div style="margin-bottom: 18px">
		<div class="layui-inline">
			<table>
				<tr>
					<td>
						<input class="layui-input" id="keyword01" style="width: 210px; margin-left: 10px" placeholder="请选择年月">
					</td>
				</tr>
			</table>
		</div>
		<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon">&#xe615;</i>检索</button>
	  </div>
      <xblock>
			<button id="add" class="layui-btn layui-btn-warm"><i class="layui-icon"></i>新增补发订单</button>
			<button id="dels" class="layui-btn layui-btn-danger"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
			<%--<button class="layui-btn layui-btn-blue" onclick="uploadExcel()"><i class="layui-icon layui-icon-down"></i>出库单导入</button>--%>
      </xblock> 
	  <table class="layui-hide" id="tableList" lay-filter="tableList"></table>
	  <table class="layui-hide" id="tableListSub" lay-filter="tableListSub"></table>
	</div>
	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-xs" lay-event="detail"><i class="layui-icon layui-icon-edit"></i>补发订单详细</a>
	</script>
	<script type="text/html" id="rowToolbar2">
		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del2">删除</a>
	</script>
	<!-- 导入框 -->
	<div id="importDiv" hidden="hidden">
		<form class="layui-form" id="uploadform">
			<div style="margin: 20px 0 20px 140px;">
				<input id="fileInput" name="file" type="file" accept=".xls,.xlsx"/>
			</div>
		</form>
		<button class="layui-btn layui-btn-blue" lay-filter="uplodeform" style="margin-left: 80px">导入数据</button>&emsp;&emsp;&emsp;&emsp;
		<button type="button" class="layui-btn" id="downloadFile">下载模板</button>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
	
		
	});
        var table;
		layui.use(['table','layer','upload','form','laydate'], function(){
			table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword01 = $("#keyword01").val();
                    //var keyword02 = $("#keyword2").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword01":keyword01}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#tableList'
				,url:'${pageContext.request.contextPath }/abnormalAdjustment/queryAbnormalAdjustmentCriteria.do'
				,toolbar: '#toolbar'
				,title: 'machineList'
				,id :'contenttable'
				,limits:[10,20,30]
                //,defaultToolbar:['filter', 'print']
                ,cols: [[
                    {type: 'checkbox', fixed: 'left', },
                    {field:'', title:'序号', width: 50, /* sort: true,  */type:'numbers', align:'center'},
                    {field:'out_stock_order_num', title:'出库单号', align:'center'},
                    /* {field:'business_order_num', title:'业务单号', align:'center'}, */
                    {field:'replenish_time', title:'补发日期', align:'center', templet: function (row){
                    	return '<a style="color: black" class="timepicker" id="replenish_time" name="replenish_time">'+row.replenish_time+'</a>';
					}},
                    {field:'replenish_man', title:'补发人', align:'center', edit: true, templet: function (row){
						return '<span style="color: black">'+row.replenish_man+'</span>';
					}},
                    {field:'replenish_reason', title:'补发原因', align:'center', edit: true, templet: function (row){
						return '<span style="color: black">'+row.replenish_reason+'</span>';
					}}
					//{field:'remark', title:'补发状态', align:'center', templet: function (row){
					//	if(row.remark == '0'){
					//		return '未完成';
					//	} else {
						//	return '已完成';
					//	}
					//}}
                ]]
                ,page: true
                ,done : function(){
                    $(".timepicker").each(function () {
                        laydate.render({
                            elem: this,
                            format: "yyyy-MM-dd"
                        });
                    });
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
			});

			//行单击事件
            table.on('row(tableList)', function(obj) {
                var data = obj.data;
                console.log(data.id);
                table.render({
                    elem: '#tableListSub'
                    ,url:'${pageContext.request.contextPath }/abnormalAdjustment/queryAbnormalDetail.do?aid='+data.id
                    ,toolbar: '#toolbar'
                    ,title: 'machineList'
                    //,id :'contenttable'
                    
                    //,defaultToolbar:['filter', 'print']
                    ,cols: [[
                        //{type: 'checkbox', fixed: 'left'},
                        {field:'id1', title:'序号', width: 100, fixed: 'left', unresize: true, sort: true, type:'numbers'},
                        {field:'id', title:'ID', align:'center', hide:true}, //隐藏不显示
                        //{field:'customer_id', title:'供应商ID', align:'center', hide:true}, //隐藏不显示
                        {field:'customer_name', title:'供应商名称', align:'center'}, //隐藏不显示
                        {field:'region_name', title:'库区名称', align:'center'}, //隐藏不显
                        //{field:'customer_name', title:'供应商名称', align:'center'},
                        {field:'materiel_id', title:'物料ID', align:'center', hide:true}, //隐藏不显示
                        {field:'materiel_name', title:'物料名称', align:'center'},
                        {field:'materiel_num', title:'物料编码', align:'center'},
                        {field:'materiel_size', title:'物料规格', align:'center'},
                        {field:'materiel_properties', title:'物料型号', align:'center'},
                        //{field:'unit', title:'单位', align:'center'},
						{field:'knum', title:'库存数量', align:'center'},
                        {field:'bnum', title:'补发数量', align:'center', edit: true, templet: function (row){
                        	if(row.bnum == "" || row.bnum == null){
                        		return '<span style="color: blue">0</span>';
                        	} else {
                        		return '<span style="color: blue">'+row.bnum+'</span>';
                        	}
						}},
                        {fixed:'right', title:'操作', toolbar: '#rowToolbar2', width: 80, align:'center'}
                    ]]
            
                    ,done : function(){
                        $('th').css({
                            'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        })
                    }
                });
            });
            //"盘点单详细"的行内编辑事件
            table.on('edit(tableListSub)', function(obj){
                var fieldValue = obj.value, 	//得到修改后的值
                    rowData = obj.data, 		//得到所在行所有键值
                    fieldName = obj.field; 
			     //console.log("***********"+obj.data.knum)
			     var k = rowData.knum
			     if(fieldValue > k) {
			    	 toastr.warning("补发数量不能大于库存量！");
			     } else {
				     //得到字段
				      layui.use('jquery',function(){
				          var $=layui.$;
				          $.ajax({
				              type: "post",
				              url: "${pageContext.request.contextPath }/abnormalAdjustment/updateAbnormaldetailById.do",
				              data: { id:rowData.id, fieldName:fieldName, fieldValue:fieldValue},
				              success: function(data){
				              	toastr.success("修改成功！");
				                  location.reload();
				              }
				          });
				      });
			    }
			});
            
            //监听复选框事件
            table.on('checkbox(tableList)',function(obj){
                if(obj.checked == true && obj.type == 'all'){
                    //点击全选
                    $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
                }else if(obj.checked == false && obj.type == 'all'){
                    //点击全不选
                    $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
                }else if(obj.checked == true && obj.type == 'one'){
                    //点击单行
                    if(obj.checked == true){
                        obj.tr.addClass('layui-table-click');
                    }else{
                        obj.tr.removeClass('layui-table-click');
                    }
                }else if(obj.checked == false && obj.type == 'one'){
                    //点击全选之后点击单行
                    if(obj.tr.hasClass('layui-table-click')){
                        obj.tr.removeClass('layui-table-click');
                    }
                }
            })
			
            laydate.render({
				elem: '#keyword01',
			});
            
			//"出库单"的行内编辑事件
			table.on('edit(tableList)', function(obj){ 
				var fieldValue = obj.value, 	//得到修改后的值 
				rowData = obj.data, 			//得到所在行所有键值 
				fieldName = obj.field; 			//得到字段
				layui.use('jquery',function(){ 
					var $=layui.$; 
					$.ajax({ 
						type: "post", 
						url: "${pageContext.request.contextPath }/abnormalAdjustment/updateAbnormalAdjustmentById.do", 
						data: { id:rowData.id, fieldName:fieldName, fieldValue:fieldValue }, 
						success: function(data){ 
							layer.msg('修改成功'); 
						} 
					}); 
				});
			});

		/* 	//"出库单详细"的行内编辑事件
            table.on('edit(tableListSub)', function(obj){
                var fieldValue = obj.value, 	//得到修改后的值
                    rowData = obj.data, 		//得到所在行所有键值
                    fieldName = obj.field; 		//得到字段
                layui.use('jquery',function(){
                    var $=layui.$;
                    $.ajax({
                        type: "post",
                        url: "${pageContext.request.contextPath }/stockOutOrder/updateOutStockOrderDetailById.do",
                        data: {id:rowData.id, fieldName:fieldName, fieldValue:fieldValue },
                        success: function(zcInventoryInfoId){
                        	zcInventoryInfoId=zcInventoryInfoId.replace(/\s+/g,"");
                        	if(zcInventoryInfoId > 0){
                        		//现在的数量=原来的数量-需要出库的数量
                        		var finalFieldValue = rowData.mNum - fieldValue;
                        		$.ajax({
                                    type: "post",
                                    url: "${pageContext.request.contextPath }/stockOutOrder/updateZcInventoryInfoById.do",
                                    data: { zcInventoryInfoId:zcInventoryInfoId, finalFieldValue:finalFieldValue},
                                    success: function(res){
                                    	if(res > 0){
                                    		layer.msg('修改成功');
                                    	}
                                    }
                                });
                        	}
                        }
                    });
                });
            });
 		*/
            //"出库单详细"的行内删除事件
            table.on('tool(tableListSub)', function(obj2){
                layer.confirm('确定删除吗？', function(index){
                    var id = obj2.data.id;
                    var idArr = new Array();
    					idArr[0] = id;
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/stockOutOrder/deleteOutStockOrderDetailById.do',
                        data:{ "idArr" : idArr },
                        success:function(data){
                            if(data > 0){
                                toastrStyle();
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }else{
                                toastrStyle();
                                toastr.warning("删除失败！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                            }
                        }
                    });
                    layer.close(index);
                });
            });

            /**
			 * 获取选中产品列表,将数据添加到弹框表格
			 */
			var rowDatas = null;
			table.on('checkbox(materielTable)', function(obj){
				rowDatas = obj.data;
				
				var val02 = rowDatas.materiel_name;
				var val03 = rowDatas.materiel_num;
				var val04 = rowDatas.materiel_size;
				var val05 = rowDatas.packing_quantity;
				var val06 = rowDatas.region_name;
				var val01 = rowDatas.customer_name;
				
				console.log();
				if(obj.checked){
					//将库存表的id信息存入数组
					$("#myTable tbody").append('<tr id="'+obj.tr.get(0).rowIndex+'">'+
						'<td>'+rowDatas.id+'</td>'+
						'<td>'+val01+'</td>'+
						'<td>'+val02+'</td>'+
						'<td>'+val03+'</td>'+
						'<td>'+val04+'</td>'+
						'<td>'+val05+'</td>'+
						'<td>'+val06+'</td>'+
						
					'</tr>');
				} else {
					$("#"+obj.tr.get(0).rowIndex).remove();
				}
			});
		
            /**
			 * 通用表单提交(AJAX方式)(新增)
			 */
			form.on('submit(addForm)', function (data) {
				$.ajax({
					url : '${pageContext.request.contextPath}/abnormalAdjustment/addAbnormalAdjustment.do',
					data: $("#addFormID").serialize(),
					cache : false,
					type : "post",
				}).done(
					function(res) {
						 console.log(res);
						if (res > 0) {
							//如果父表新增成功再执行新增子表的操作
							$("#tbody tr").each(function(i,dom){
	                            var tr = $(dom).closest("tr");
	                            var tds = tr.find("td");
	                            //主表插入行ID
                                var aid = res;
                                var mid = tds.eq(0).html();
                                
	                            $.ajax({
	                                type:'post'
	                                ,url:'${pageContext.request.contextPath}/abnormalAdjustment/addAbnormalDetail.do'
	                                ,data:{aid:aid, mid:mid}
	                                ,dataType:'json'
	                                ,success:function(data){
	                                    console.log(data);
	                                }
	                            })
                        	});
							/* —————————————— */
							toastrStyle();
							toastr.success('新增成功！');
							setTimeout(function(){
								location.reload();
							},1000);
							/* —————————————— */
						}
					}
				).fail(
					function(res) {
						toastrStyle();
						toastr.error('新增失败！');
						setTimeout(function(){
							location.reload();
						},1000);
					}
				)
				return false;
			});

			/**
			 * 新增表单校验
			 */
			form.verify({
				//value：表单的值item：表单的DOM对象
				out_stock_order_num: function(value, item){
					if(value == ''){
						return '出库单号不能为空';
					}
				},
				out_stock_type: function(value, item){
					if(value == ''){
						return '出库类型不能为空';
					}
				},
				customer: function(value, item){
					if(value == ''){
						return '客户名称不能为空';
					}
				},
				address: function(value, item){
					if(value == ''){
						return '客户地址不能为空';
					}
				},
				creater_date: function(value, item){
					if(value == ''){
						return '出库日期不能为空';
					}
				},
				replenish_time: function(value, item){
					if(value == ''){
						return '补发日期不能为空';
					}
				},
				replenish_man: function(value, item){
					if(value == ''){
						return '补发人员不能为空';
					}
				},
				replenish_reason: function(value, item){
					if(value == ''){
						return '补发原因不能为空';
					}
				}
			});
			
			form.on('select(out_stock_order_num)', function(data) {
				$.ajax({
                    type: 'POST',
                    url: '${pageContext.request.contextPath}/abnormalAdjustment/queryOutStockOrderByNum.do',
                    data: {out_order_num:data.value},
                    dataType: 'json',
                    async: false,
                    success: function (datas){
                        /* if(datas[0].out_stock_type == '0'){
                            datas[0].out_stock_type = '一般出库'
						} else {
                            datas[0].out_stock_type = '紧急出库'
						}
                    	$("#out_stock_type").val(datas[0].out_stock_type);
                    	$("#customer").val(datas[0].customer);
                    	$("#address").val(datas[0].address); */
                    	console.log(datas[0])
                    	$("#creater_date").val(datas[0].create_date);
                    }
                });
			})
			
			//新增
            $("#add").click(function(){
                layer.open({
                    type: 1 					//Page层类型
                    ,area: ['1000px', '500px'] 	//宽  高
                    ,title: '新增'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="addDivID">'+
							'<form class="layui-form" id="addFormID">'+
								'<xblock>'+
									'<table>'+
										'<tr>'+
											'<td><label class="layui-form-label">出库单号</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="out_stock_order_num" name="out_stock_order_num" lay-verify="out_stock_order_num" lay-filter="out_stock_order_num">' +
														'<c:forEach items="${allOutStockOrder}" var="outStockOrder">' +
															'<option value="${outStockOrder.out_stock_num}">${outStockOrder.out_stock_num}</option>' +
														'</c:forEach>'+
													'</select>'+
			                                    '</div>'+
			                                    '<div style="margin-top: 12px;">'+
			                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
			                                    '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">出库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="out_type" name="out_type" lay-verify="out_type" lay-filter="out_type">' +
														'<option value="">请选择</option>' +
														'<option value="0">一般出库</option>' +
														'<option value="1">特殊出库</option>' +
													'</select>'+
												'</div>'+
		                                    '</td>'+
										
											'<td><label class="layui-form-label">出库日期</label></td>'+
											'<td><input readonly class="layui-input" id="creater_date" name="creater_date" lay-verify="creater_date" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
										'</tr>'+
	                        			'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">补发日期</label></td>'+
											'<td><input class="layui-input" id="replenish_time1" name="replenish_time" lay-verify="replenish_time" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">补发人员</label></td>'+
											'<td><input class="layui-input" id="replenish_man" name="replenish_man" lay-verify="replenish_man" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
											'<td><label class="layui-form-label">供应商</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="customer_id" name="customer_id" lay-verify="customer_id" lay-filter="customer_id" style="width: 190px">' +
														'<option value="">请选择供应商</option>' +
														'<c:forEach items="${allcustomers}" var="customer">' +
															'<option value="${customer.id}">${customer.customer_name}</option>' +
														'</c:forEach>'+
													'</select>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">补发原因</label></td>'+
											'<td colspan="5"><input class="layui-input" id="replenish_reason" name="replenish_reason" lay-verify="replenish_reason" style="width: 818px; display:inline" placeholder="请填写异常补发原因">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
		                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                        '</div>'+
											'</td>'+
										'</tr>'+
										/*'<tr align="center">'+
											'<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 20px">提交</button></td>'+
											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button type="reset" class="layui-btn layui-btn-primary" style="margin-bottom: 20px">重置</button></td>'+
										'</tr>'+*/
									'</table>'+
								'</xblock>'+
								
								'<table class="layui-table" id="myTable">' +
									'<thead style="text-align: center">'+
										'<tr style="background: #009688; color: white">'+
                        					'<th>序号</th>'+
                        					'<th>供应商名称</th>'+
											'<th>物料名称</th>'+
											'<th>物料编码</th>'+
											'<th>物料规格</th>'+
											'<th>物料型号</th>'+
											'<th>库区</th>'+
										'</tr>'+
									'</thead>'+
									'<tbody id="tbody" style="text-align: center;">'+
	                    			'</tbody>'+
								'</table>'+
								'<xblock>'+
									'<button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:360px; margin-bottom: 10px">提交</button>'+
									'&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button>'+
								'</xblock>'+
								'</table>'+
							'</form>'+
                    	'</div>'
                    ,success: function(){
                    	$.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllOutStockOrder.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#out_stock_order_num").append("<option value='"+ datas[i].out_stock_num +"'>"+ datas[i].out_stock_num +"</option>");
                                }
                            }
                        });
                    	$.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomerByType.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                	$("#customer_id").append("<option value='"+datas[i].id+"'>"+datas[i].customer_name+"</option>");
                                }
                            }
                        });
           			
                        laydate.render({
                            elem: '#replenish_time1'
                        });
                        form.render();
                    }
                });
			});
			form.on('select(customer_id)', function(data){
				 var customerId = $('#customer_id').val();
					layer.open({
		                type: 1 					//Page层类型
		                ,area: ['900px', '450px'] 	//宽  高
		                ,title: '物料清单'
		                ,shade: 0.6 				//遮罩透明度
		                ,maxmin: true 				//允许全屏最小化
		                ,anim: 1 					//0-6的动画形式，-1不开启
		                ,content: '<form class="layui-form">' +
									   	'<table id="materielTable" lay-filter="materielTable"></table>' +
									   	
									   	'</form>'
		                ,success: function () {
		                    table.render({
		                        elem: '#materielTable'
		                        ,url:'${pageContext.request.contextPath }/abnormalAdjustment/queryMateriel.do?cid='+customerId
		                        
		                        ,defaultToolbar:['filter', 'print']
		                        ,cols: [[
		           					{type: 'checkbox', fixed: 'left'}
		           					,{field:'xuhao', title:'序号', sort: true, type:'numbers', width: 60}
		           					//,{field:'id', title:'物料ID', align:'center', hide:true} //隐藏不显示
		           					,{field:'customer_id', title:'供应商ID', align:'center',hide:true} //隐藏不显示
		           					,{field:'customer_name', title:'供应商名称',templet:function(row){
		           						if(row.customer_name!=null){
		           							return row.customer_name;
		           						}else{
		           							return "";
		           						}
		           					}}
		           					,{field:'materiel_name', title:'物料名称',templet:function(row){
		           						if(row.materiel_name!=null){
		           							return row.materiel_name;
		           						}else{
		           							return "";
		           						}
		           					}}
		           					,{field:'materiel_num', title:'物料编号',templet:function(row){
		           						if(row.materiel_num!=null){
		           							return row.materiel_num;
		           						}else{
		           							return "";
		           						}
		           					}}
		           					,{field:'materiel_size', title:'物料规格',templet:function(row){
		           						if(row.materiel_size!=null){
		           							return row.materiel_size;
		           						}else{
		           							return "";
		           						}
		           					}}
		           					,{field:'materiel_properties', title:'物料型号',templet:function(row){
		           						if(row.materiel_properties!=null){
		           							return row.materiel_properties;
		           						}else{
		           							return "";
		           						}
		           					}}
		           					,{field:'region_name', title:'库区名称', align:'center'}
		           					
		           				]]
		                       
		                        ,done : function(){
		                            $('th').css({
		                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
		                            })
		                        }
		                    });
		                }
		            }); 
				})
			
			//批量删除
			$("#dels").click(function(){
				var rowData = table.checkStatus('contenttable');
				var data = rowData.data;
				var idArr = new Array();
				if(data.length == 0){
					toastrStyle();
					toastr.warning("请至少选择一条记录！");
				} else {
					for(var i=0;i < data.length;i++){
						idArr[i] = data[i].id;
					}
					$.ajax({
						type:'post',
						url:'${pageContext.request.contextPath }/abnormalAdjustment/deleteAbnormalAdjustmentById.do',
						data:{"idArr" : idArr},
						success:function(data){
							layer.confirm('确定删除吗？', function(index){
								if(data > 0){
									toastrStyle();
									toastr.success("删除成功！");
									setTimeout(function(){
										location.reload();
									},1000);
									setTimeout(function(){
										window.location.reload();
									},2000);
								}else{
									toastrStyle();
									toastr.warning("删除失败！");
									location.reload();
								}
							});
						}
					});
				}
			});
		});

		//获取选中产品清单
		function addProduct (){
		     var onum = $('#out_stock_order_num').val();
            layer.open({
                type: 1 					//Page层类型
                ,area: ['900px', '450px'] 	//宽  高
                ,title: '产品清单'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content: '<form class="layui-form">' +
							   	'<table id="productTable" lay-filter="productTable"></table>' +
						  '</form>'
                ,success: function () {
                    table.render({
                        elem: '#productTable'
                        ,url:'${pageContext.request.contextPath }/stockOutOrder/selectmList.do?outnum='+onum
                        //,toolbar: '#toolbar'
                        //,title: 'machineList'
                        //,id :'contenttable'
                        
                        ,defaultToolbar:['filter', 'print']
                        ,cols: [[
           					{type: 'checkbox', fixed: 'left'}
           					,{field:'xuhao', title:'序号', sort: true, type:'numbers', width: 60}
           					,{field:'materiel_name', title:'物料名称'/* ,templet:function(row){
           						if(row.materiel!=null){
           							return row.materiel.materiel_name;
           						}else{
           							return "";
           						}
           					} */}
           					,{field:'materiel_num', title:'产品码'/* ,templet:function(row){
           						if(row.materiel!=null){
           							return row.materiel.materiel_num;
           						}else{
           							return "";
           						}
           					} */}
           					,{field:'brevity_num', title:'简码'/* ,templet:function(row){
           						if(row.materiel!=null){
           							return row.materiel.brevity_num;
           						}else{
           							return "";
           						}
           					} */}
           					,{field:'packing_quantity', title:'包装数量'/* ,templet:function(row){
           						if(row.materiel!=null){
           							return row.materiel.packing_quantity;
           						}else{
           							return "";
           						}
           					} */}
           					,{field:'unit', title:'单位'/* ,templet:function(row){
           						if(row.materiel!=null){
           							return row.materiel.unit;
           						}else{
           							return "";
           						}
           					} */}/* 
           					,{field:'mBatch', title:'批次'}
           					,{field:'mNum', title:'数量'}
           					
           					,{field:'moveDate', title:'拆箱日期',templet:function(row){
								if(row.moveDate != null){
									return row.moveDate;
								}else{
									return "";
								}
           					}}
           					,{field:'enterPerson', title:'拆箱人'} */
           					/* ,{field:'moveDate', title:'最后移动日期',templet:function(row){
           						if(row.moveDate!=null){
           							return date2String(row.moveDate.time);
           						}else{
           							return "";
           						}
           					}} */
           					//,{field:'movePerson', title:'最后移动人'}
           				]]
                     
                        ,done : function(){
                            $('th').css({
                                'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                            })
                        }
                    });
                }
            });
		}

        //日期格式转换
        function dateToStr(date) {
            var time = new Date(date.time);
            var y = time.getFullYear();
            var M = time.getMonth() + 1;
            M = M < 10 ? ("0" + M) : M;
            var d = time.getDate();
            d = d < 10 ? ("0" + d) : d;
            var str = y + "-" + M + "-" + d;
            return str;
        }
        
      	//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
			return date;
		}
      	
		function formatDate(time) { 
			let d = new Date(time);
        	let dformat = [ d.getFullYear(), 
        	checkTime(d.getMonth() + 1), checkTime(d.getDate()) ].join('-');
        	return dformat;
    	};
    	
   	  	//修改时间戳查询时间
		function checkTime(n) {
			return n < 10 ? '0' + n : n;
   	  	};
	</script>
</body>
</html>
