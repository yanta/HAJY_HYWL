<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>岗位信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>岗位信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入岗位名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
</div>
<script type="text/javascript">
    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/post/queryPostCriteria.do'
            ,title: '岗位列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,toolbar: '#toolbar'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'dept_name', title:'部门名称'}
                ,{field:'post_num', title:'岗位编号'}
                ,{field:'post_name', title:'岗位名称'}
                /* ,{field:'status', title:'岗位状态', templet: function (row){
                    if (row.status == '0') {
                        return "可用";
                    } else {
                        return "不可用";
                    }
                }} */
                ,{field:'create_time', title:'创建日期'}
                //,{field:'updatedTime', title:'修改时间'}
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            dept_id: function(value, item){
                if(value == ''){
                    return '部门名称不能为空';
                }
            },
            post_num: function(value, item){
                if(value == ''){
                    return '岗位编号不能为空';
                }
            },
            post_name: function(value, item){
                if(value == ''){
                    return '岗位名称不能为空';
                }
            },
            /* status: function(value, item){
                if(value == ''){
                    return '岗位状态不能为空';
                }
            }, */
            create_time: function(value, item){
                if(value == ''){
                    return '创建日期不能为空';
                }
            }
        });

        //新增
        $("#add").click(function(){
            layer.open({
                type: 1 					//Page层类型
                ,area: ['450px', ''] 	//宽  高
                ,title: '新增'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content:
                    '<div id="addDivID">'+
                        '<form class="layui-form" id="addFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">所属部门</label>'+
	                                        '<div style="width: 285px; float: left;">' +
		                                        '<select id="dept_id" name="dept_id" lay-verify="dept_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                        '<td><label class="layui-form-label">岗位编号</label>'+
                                            '<input class="layui-input" id="post_num" name="post_num" lay-verify="post_num" style="width: 285px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">岗位名称</label>'+
                                            '<input class="layui-input" id="post_name" name="post_name" lay-verify="post_name" style="width: 285px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                     '</tr>'+
                                     '<tr style="height: 10px"></tr>'+
                                        '<td><label class="layui-form-label">创建日期</label>'+
	                                        '<input class="layui-input" id="create_time" name="create_time" lay-verify="create_time" style="width: 285px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                        /* '<td><label class="layui-form-label">岗位状态</label></td>'+
                                        '<td>'+
                                            '<div style="width: 285px; float: left;">' +
                                                '<select name="status" lay-verify="status">' +
                                                    '<option value="">请选择岗位状态</option>' +
                                                    '<option>可用</option>' +
                                                    '<option>不可用</option>' +
                                                '</select>'+
                                            '</div>'+
                                            '<div style="margin-top: 12px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+ */
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">备注</label>'+
                                            '<input class="layui-input" id="remark" name="remark" style="width: 285px">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 20px"></tr>'+
                                    '<tr align="center">'+
                                        '<td><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:70PX; margin-bottom: 10px">提交</button>'+
                                        '&emsp;&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                	//下拉框查找所有仓库
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllDept.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#dept_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].dept_name +"</option>");
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    form.render();
                }
            });
        });

        //编辑
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            layer.open({
                type: 1 				//Page层类型
                ,area: ['450px', ''] 	//宽  高
                ,title: '编辑'
                ,shade: 0.6 			//遮罩透明度
                ,maxmin: true 			//允许全屏最小化
                ,anim: 1 				//0-6的动画形式，-1不开启
                ,content:
                    '<div id="updateDivID">'+
                        '<form class="layui-form" id="updateFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td>'+
                                            '<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">所属部门</label>'+
	                                        '<div style="width: 285px; float: left;">' +
		                                        '<select id="dept_id" name="dept_id" lay-verify="dept_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
	                                    '</td>'+
                                    '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                    '<td><label class="layui-form-label">岗位编号</label>'+
	                                        '<input class="layui-input" id="post_num" name="post_num" value="'+data.post_num+'" lay-verify="post_num" style="width: 285px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                '<tr>'+
	                                    '<td><label class="layui-form-label">岗位名称</label>'+
	                                        '<input class="layui-input" id="post_name" name="post_name" value="'+data.post_name+'" lay-verify="post_name" style="width: 285px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                    '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                    '<td><label class="layui-form-label">创建日期</label>'+
	                                        '<input class="layui-input" id="create_time" name="create_time" value="'+data.create_time+'" lay-verify="create_time" style="width: 285px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                    /* '<td><label class="layui-form-label">岗位状态</label></td>'+
	                                    '<td>'+
	                                        '<div style="width: 285px; float: left;">' +
	                                            '<select name="status" lay-verify="status">' +
	                                                '<option value="">请选择岗位状态</option>'+
	                                                '<option>可用</option>' +
	                                                '<option>不可用</option>' +
	                                            '</select>'+
	                                        '</div>'+
	                                        '<div style="margin-top: 12px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+ */
	                                '</tr>'+
	                                '<tr style="height: 10px"></tr>'+
	                                '<tr>'+
	                                    '<td><label class="layui-form-label">备注</label>'+
	                                        '<input class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 285px">'+
	                                    '</td>'+
	                                '</tr>'+
	                                '<tr style="height: 20px"></tr>'+
	                                '<tr align="center">'+
	                                    '<td><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:70PX; margin-bottom: 10px">提交</button>'+
	                                    '&emsp;&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
	                                '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                	//下拉框查找所有仓库
                    $.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/post/queryAllDept.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                                $("#dept_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].dept_name +"</option>");
                                if(datas[i].id == data.dept_id){
                                    $("#dept_id").val(data.dept_id);
                                }
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    form.render();
                }
            });
        });

        //批量删除
        $("#del").click(function(){
            var rowData = table.checkStatus('contenttable');
            var data = rowData.data;
            var idArr = new Array();
            if(data.length == 0){
                toastrStyle();
                toastr.warning("请至少选择一条记录！");
            } else {
                for(var i=0;i < data.length;i++){
                    idArr[i] = data[i].id;
                }
                $.ajax({
                    type:'post',
                    url:'${pageContext.request.contextPath }/post/deletePostById.do',
                    data:{"idArr" : idArr},
                    success:function(data){
                        layer.confirm('确定删除吗？', function(){
                            if(data > 0){
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                                setTimeout(function(){
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                                location.reload();
                            }
                        });
                    }
                });
            }
        });

        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(addForm)', function () {
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/post/addPost.do',
                data:$("#addFormID").serialize(),
                cache:false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })

        /**
         * 通用表单编辑(AJAX方式)
         */
        form.on('submit(updateForm)', function () {
            $.ajax({
                type : 'post',
                url : '${pageContext.request.contextPath }/post/updatePost.do',
                data : $("#updateFormID").serialize(),
                cache : false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('修改成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('修改失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function dateToStr(date) {
        var time = new Date(date.time);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        M = M < 10 ? ("0" + M) : M;
        var d = time.getDate();
        d = d < 10 ? ("0" + d) : d;
        var str = y + "-" + M + "-" + d;
        return str;
    }
</script>
</body>
</html>
