<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>用户信息</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style type="text/css">
		#tab td {
			border:none;
		} 
	</style>
</head>
<body class="layui-anim layui-anim-up">
	<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>用户信息</cite>
			</a>
		</span>
		<!-- <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" href="javascript:location.replace(location.href);" title="刷新">
		<i class="layui-icon" style="line-height:30px">ဂ</i></a> -->
    </div>
	<div class="x-body">
		<div class="layui-row">
			<form class="layui-form layui-col-md12 x-so">
				<!-- <input class="layui-input" placeholder="开始日" name="start" id="start">
				<input class="layui-input" placeholder="截止日" name="end" id="end"> -->
				<input type="text" name="keyword" id="keyword"  placeholder="请输入用户账号" autocomplete="off" class="layui-input">
				<button class="layui-btn"  lay-submit="" lay-filter="sreach"><i class="layui-icon layui-icon-search"></i>检索</button>
			</form>
		</div>
		<xblock>
      		<button class="layui-btn layui-btn-blue" id="add"><i class="layui-icon"></i>新增</button>
			<button class="layui-btn layui-btn-blue" id="delete"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
		</xblock>
		<table class="layui-hide" id="userList" lay-filter="userList"></table>
		
		<script type="text/html" id="rowToolbar">
			<a class="layui-btn layui-btn-xs" lay-event="accredit">授权</a>
			<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
			<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
		</script>
	</div>
	<div id="formDiv" hidden>
		<form class="layui-form layui-card-body" id="addform" onsubmit="return false;">
			<input class="layui-hide" name="id" id="id">
			<table class="layui-table" id="tab">
				<tbody>
					<tr>
						<td>
							<div class="layui-inline">
								<label class="layui-form-label">员工姓名</label>
								<input class="layui-input" readonly lay-verify="name" id="name" name="name" type="text" value="" placeholder="请选择员工" style="width:200px;">
								<input type="text" value="" class="layui-hide" id="staffId" name="staffId">
							</div>
							<div class="layui-inline" style="margin-top: 8px;">
								<font style="color:red; font-size: 24px;">*</font>
							</div>
						</td>
					</tr>
					<tr>
						<td>
							<div class="layui-inline">
								<label class="layui-form-label">登录账号</label>
								<input class="layui-input" lay-verify="username" id="username" name="username" type="text" value="" style="width:200px;">
							</div>
							<div class="layui-inline" style="margin-top: 8px;">
								<font style="color:red; font-size: 24px;">*</font>
							</div>
						</td>
					</tr>	
					<tr>
						<td>
							<div class="layui-inline">
								<label class="layui-form-label">登录密码</label>
								<input class="layui-input" lay-verify="password" id="password" name="password" type="text" value="" style="width:200px;">
							</div>
							<div class="layui-inline" style="margin-top: 8px;">
								<font style="color:red; font-size: 24px;">*</font>
							</div>
						</td>
					</tr>	
				</tbody>
			</table>
			<div style="text-align: center;margin-top: 20px">
				<button id="subBtn" class="layui-btn layui-btn-blue" lay-submit lay-filter="submitBtn">提交</button>
				<button type="reset" id="addReset" class="layui-btn layui-btn-primary" style="margin-left:200px">重置</button>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		//日期格式转换
		function date2String(timestamp){
			var d = new Date(timestamp);
			var date = (d.getFullYear()) + "-" + 
		       (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
		       (d.getDate()<10?"0"+d.getDate():d.getDate());
		       return date;
		}
		layui.use(['table','layer','upload','form','laydate'], function(){
			var table = layui.table;
			var layer = layui.layer;
			var form = layui.form;
			var laydate = layui.laydate;
			//日期
			laydate.render({
			   elem: '#birthdate'
			   ,type: 'date'
			});
			var $ = layui.jquery, active = {
				reload:function () {
					var keyword = $("#keyword").val();
					table.reload('contenttable',{
						method:'get',
						where:{"keyword":keyword}
					});
				}
			}
			$('.layui-btn').on('click', function(){
				var type = $(this).data('type');
				active[type] ? active[type].call(this) : '';
			});
			table.render({
				elem: '#userList'
				,url:'${pageContext.request.contextPath }/user/selectUserList.do'
				,title: '用户列表'
				,id :'contenttable'
				,limits:[10,20,30]
				,cols: [[
					{type: 'checkbox', fixed: 'left'}
					,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
					,{field:'name', title:'员工姓名'}
					,{field:'number', title:'登录账号'}
					,{field:'status', title:'启用状态'}
					,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:200}
				]]
				,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                        
                    })
                }
			});
			$("#name").click(function(){
				layer.open({
			        type: 1 					//Page层类型
			        ,area: ['95%', '600px']   //宽  高
			        ,title: '员工列表'
			        ,shade: 0.6 				//遮罩透明度
			        ,maxmin: true			    //允许全屏最小化
			        ,anim: 1 					//0-6的动画形式，-1不开启
			        ,content: '<table class="layui-hide" id="staffList" lay-filter="staffList"></table>'
			        ,success:function(){
		                table.render({
		                    elem: '#staffList'
		                    ,url:'${pageContext.request.contextPath }/staff/selectStaffList4User.do'
		                    ,title: '员工列表'
		                    ,limits:[10,20,30]
		                    ,done:function(res, curr, count) { //表格数据加载完后的事件
		                        //调用示例
		                    }
		                    ,cols: [[
		                        {field:'', title:'序号', sort: true, type:'numbers', width: 60}
		                        ,{field:'number', title:'员工姓名'}
		                        ,{field:'lhCode', title:'员工编号'}
		                        ,{field:'facilityNumber', title:'员工部门'}
		                        ,{field:'aDlNumBig', title:'员工岗位'}
		                    ]]
		                    ,page: true
                            ,done : function(){
                                $('th').css({
                                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                                    
                                })
                            }
		                });
			        }
				})
			})
			//头事件
			$("#add").click(function(){
		    	layer.open({
					type: 1 		//Page层类型
					,area: ['450px', '350px'] //宽  高
					,title: '新增用户'
					,shade: 0.6 	//遮罩透明度
					,shadeClose: true //点击遮罩关闭
					,maxmin: true //允许全屏最小化
					,anim: 1 		//0-6的动画形式，-1不开启
					,content: $('#formDiv')
					,success: function () {
						$("option").removeAttr("selected");
						document.getElementById("addform").reset();
					}
				});
		    })
			$("#delete").click(function(){
				var checkStatus = table.checkStatus("contenttable");
				var data = checkStatus.data;
	        	var str = "";
	        	if(data.length==0){
	        		toastr.warning("请至少选择一条记录！");
	        	}else{
					layer.confirm('确定删除吗？', function(index){
						var idArr = new Array();
					    var id = data.id;
					    idArr[0] = id;
						$.ajax({
					    	type:'post',
					    	url:'${pageContext.request.contextPath }/user/deleteUser.do',
					    	data:{"idArr":idArr},
					    	success:function(affact){
					    		if(affact>0){
					    			toastr.success("删除成功！");
					    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
										//关闭模态框
										// 父页面刷新
										window.location.reload();  
									},2000);
					    		}else{
					    			toastr.warning("删除失败！");
					    		}
					    	}
					    		
					    })
					    layer.close(index);
					});
	        	}
		    })
			//监听行工具事件
			table.on('tool(userList)', function(obj){
				var data = obj.data;
				if(obj.event === 'edit'){
					layer.open({
						type: 1 		//Page层类型
						,area: ['450px', '350px'] //宽  高
						,title: '编辑用户'
						,shade: 0.6 	//遮罩透明度
						,shadeClose: true //点击遮罩关闭
						,maxmin: true //允许全屏最小化
						,anim: 1 		//0-6的动画形式，-1不开启
						,content: $('#formDiv')
						,success: function () {
							document.getElementById("addform").reset();
							$("#id").val(data.id);
							$("#name").val(data.name);
							$("#usernumber").val(data.usernumber);
							$("#password").val(data.password);
						}
						,end: function () {
							var formDiv = document.getElementById('formDiv');
							formDiv.style.display = '';
							layer.closeAll();
						}
					});
				}else if(obj.event === 'del'){
					layer.confirm('确定删除吗？', function(index){
						var idArr = new Array();
					    var id = data.id;
					    idArr[0] = id;
						$.ajax({
					    	type:'post',
					    	url:'${pageContext.request.contextPath }/user/deleteUser.do',
					    	data:{"idArr":idArr},
					    	success:function(affact){
					    		if(affact>0){
					    			toastr.success("删除成功！");
					    			setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
										//关闭模态框
										// 父页面刷新
										window.location.reload();  
									},2000);
					    		}else{
					    			toastr.warning("删除失败！");
					    		}
					    	}
					    		
					    })
					    layer.close(index);
					});
				}
			})
		    /**
		     * 表单校验
		     */
		    form.verify({
		    	//value：表单的值、item：表单的DOM对象
		    	name: function(value, item){
		    		if(value == ''){
		    			return '员工姓名不能为空';
		    		}
		    	}
		    	,username: function(value, item){
		    		if(value == ''){
		    			return '登录账号不能为空';
		    		}
		    	}
		    	,password: function(value, item){
		    		if(value == ''){
		    			return '登录密码不能为空';
		    		}
		    	}
		    });
			/**
		     * 通用表单提交(AJAX方式)
		     */
		    form.on('submit(submitBtn)', function (data) {
				$.ajax({
					type:'post'
					,url:'${pageContext.request.contextPath }/user/saveUser.do'
					,data:$("#addform").serialize()
					,success:function(res){
						var list = res.split('_');
			  			if (parseInt(list[0]) > 0) {
			  				if(list[1].indexOf('add') != -1){
			  					layer.closeAll();
			  					toastr.success('新增成功！');
			  				}
			  				if(list[1].indexOf('edit') != -1){
			  					layer.closeAll();
			  					toastr.success('修改成功！');
			  				}
			  				setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
			  					//关闭模态框
			  					// 父页面刷新
			  					window.location.reload();  
			  				},2000);
			  			}else{
			  				if(list[1].indexOf('add') != -1){
			  					toastr.error('新增失败！');
			  				}
			  				if(list[1].indexOf('edit') != -1){
			  					toastr.error('修改失败！');
			  				}
			  			}
					}
					,error:function(){
						
					}
				})
			})
		});
		toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
	</script>
</body>

</html>