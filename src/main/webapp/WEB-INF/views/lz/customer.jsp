<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>客户信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
    <link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.css">
    <link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.js">
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>客户信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入客户名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <!-- 如果是供应商才可以绑定仓库，主机厂不能绑定仓库 -->
    <script type="text/html" id="rowToolbar">
		{{#  if(d.customer_type == 0){ }}
        	<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
        	<!--<a class="layui-btn layui-btn-xs" lay-event="bindingWarehouse"><i class="layui-icon layui-icon-edit"></i>绑定仓库</a>-->
		{{#  } }}  
		{{#  if(d.customer_type == 1){ }}
        	<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
		{{#  } }}  
	</script>
    <div id="formDiv2" hidden>
        <div class="box">
            <form class="layui-form layui-card-body" id="addform2">
                <input hidden id="id2" name="id">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">仓库</label>
                        <div class="layui-input-inline" style="width: 285px">
                            <select name="ware" id="ware" lay-filter="ware" xm-select="select2"></select>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" id="subform2">确定</button>
                        <button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    //全局定义一次, 加载formSelects
    layui.config({
        base: '${pageContext.request.contextPath }/assets/formSelects/' //此处路径请自行处理, 可以使用绝对路径
    }).extend({
        formSelects: 'formSelects-v4'
    });
    layui.use(['table','layer','upload','form','laydate','formSelects'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;
        var formSelects = layui.formSelects;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/customer/queryCustomerCriteria.do'
            ,title: '客户列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,toolbar: '#toolbar'
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'contacts_name', title:'联系人'}
                ,{field:'customer_name', title:'客户名称'}
                ,{field:'customer_num', title:'客户编号'}
                ,{field:'customer_type', title:'客户类型', templet: function (row){
                    if (row.customer_type == 0) {
                        return "供应商";
                    } else {
                        return "主机厂";
                    }
                }}
                ,{field:'customer_region', title:'客户所在地区'}
                ,{field:'customer_adress', title:'客户地址'}
                ,{field:'customer_email', title:'客户邮箱'}
                ,{field:'customer_zipcode', title:'客户邮编'}
                ,{field:'barcode', title:'是否有码', templet: function (row){
                    if (row.barcode == 0) {
                        return "无码";
                    } else {
                        return "有码";
                    }
                }}
                ,{field:'photo', title:'是否拍照', templet: function (row){
                    if (row.photo == 0) {
                        return "否";
                    } else {
                        return "是";
                    }
                }}
                ,{field:'wareName', title:'拥有库区', width: 250}
                ,{field:'create_time', title:'创建日期'}
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            contacts_id: function(value, item){
                if(value == ''){
                    return '联系人不能为空';
                }
            },
            customer_name: function(value, item){
                if(value == ''){
                    return '客户名称不能为空';
                }
            },
            customer_num: function(value, item){
                if(value == ''){
                    return '客户编号不能为空';
                }
            },
            customer_type: function(value, item){
                if(value == ''){
                    return '客户类型不能为空';
                }
            },
            customer_region: function(value, item){
                if(value == ''){
                    return '客户所在地区不能为空';
                }
            },
            create_time: function(value, item){
            	if(value == ''){
            		return '日期不能为空'
            	}
            }
        });

        //新增
        $("#add").click(function(){
            layer.open({
                type: 1 					//Page层类型
                ,area: ['670px', ''] 	//宽  高
                ,title: '新增'
                ,shade: 0.6 				//遮罩透明度
                ,maxmin: true 				//允许全屏最小化
                ,anim: 1 					//0-6的动画形式，-1不开启
                ,content:
                    '<div id="addDivID">'+
                        '<form class="layui-form" id="addFormID">'+
                            '<xblock>'+
                                '<table>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">联系人</label></td>'+
	                                    '<td>'+
	                                        '<div style="width: 190px; float: left;">' +
		                                        '<select id="contacts_id" name="contacts_id" lay-verify="contacts_id">' +
		                                            '<option value="">请选择</option>' +
		                                        '</select>'+
		                                    '</div>'+
		                                    '<div style="margin-top: 12px;">'+
		                                        '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
		                                    '</div>'+
	                                    '</td>'+
                                        '<td><label class="layui-form-label">客户名称</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="customer_name" name="customer_name" lay-verify="customer_name" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
                                        '<td><label class="layui-form-label">客户编号</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="customer_num" name="customer_num" lay-verify="customer_num" style="width: 190px; display:inline">'+
                                            '<div class="layui-inline" style="margin-top: 10px;">'+
                                                '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                            '</div>'+
                                        '</td>'+
                                        '<td><label class="layui-form-label">所在地区</label></td>'+
	                                    '<td>'+
	                                        '<input class="layui-input" id="customer_region" name="customer_region" lay-verify="customer_region" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
                                    '</tr>'+
                                    '<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">客户类型</label></td>'+
	                                    '<td>'+
	                                        '<div style="width: 190px; float: left;">' +
	                                            '<select name="customer_type" lay-verify="customer_type">' +
	                                                '<option value="">请选择客户类型</option>'+
	                                                '<option value="0">供应商</option>' +
	                                                '<option value="1">主机厂</option>' +
	                                            '</select>'+
	                                        '</div>'+
	                                        '<div style="margin-top: 12px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                    '<td><label class="layui-form-label">客户地址</label></td>'+
	                                    '<td>'+
	                                        '<input class="layui-input" id="customer_adress" name="customer_adress" style="width: 190px;">'+
	                                    '</td>'+
                                	'</tr>'+
                                	'<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    '<td><label class="layui-form-label">客户邮箱</label></td>'+
	                                    '<td>'+
	                                        '<input class="layui-input" id="customer_email" name="customer_email" style="width: 190px;">'+
	                                    '</td>'+
	                                    '<td><label class="layui-form-label">客户邮编</label></td>'+
	                                    '<td>'+
	                                        '<input class="layui-input" id="customer_zipcode" name="customer_zipcode" style="width: 190px;">'+
	                                    '</td>'+
                                	'</tr>'+
                                	'<tr style="height: 10px"></tr>'+
                                	'<tr>'+
	                                    '<td><label class="layui-form-label">是否有码</label></td>'+
	                                    '<td>'+
		                                    '<div style="width: 190px; float: left;">' +
		                                        '<select name="barcode" lay-verify="barcode">' +
		                                            '<option value="0">无码</option>' +
		                                            '<option value="1">有码</option>' +
		                                        '</select>'+
		                                    '</div>'+
	                                    '</td>'+
	                                    '<td><label class="layui-form-label">是否拍照</label></td>'+
	                                    '<td>'+
		                                    '<div style="width: 190px; float: left;">' +
		                                        '<select name="photo" lay-verify="photo">' +
		                                            '<option value="0">否</option>' +
		                                            '<option value="1">是</option>' +
		                                        '</select>'+
		                                    '</div>'+
	                                    '</td>'+
	                            	'</tr>'+
	                            	'<tr style="height: 10px"></tr>'+
                                    '<tr>'+
	                                    /* '<td><label class="layui-form-label">客户类型</label></td>'+
	                                    '<td>'+
	                                        '<div style="width: 190px; float: left;">' +
	                                            '<select name="status">' +
	                                                '<option value="">请选择客户类型</option>' +
	                                                '<option>可用</option>' +
	                                                '<option>不可用</option>' +
	                                            '</select>'+
	                                        '</div>'+
	                                        '<div style="margin-top: 12px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+ */
	                                    '<td><label class="layui-form-label">创建日期</label></td>'+
	                                    '<td>'+
	                                        '<input class="layui-input" id="create_time" name="create_time" lay-verify="create_time" style="width: 190px; display:inline">'+
	                                        '<div class="layui-inline" style="margin-top: 10px;">'+
	                                            '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
	                                        '</div>'+
	                                    '</td>'+
	                                    '<td><label class="layui-form-label">备注</label></td>'+
                                        '<td>'+
                                            '<input class="layui-input" id="remark" name="remark" style="width: 190px">'+
                                        '</td>'+
                                	'</tr>'+
                                    '<tr style="height: 20px"></tr>'+
                                    '<tr align="center">'+
                                        '<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                        '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button  class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                    '</tr>'+
                                '</table>'+
                            '</xblock>'+
                        '</form>'+
                    '</div>'
                ,success: function(){
                	$.ajax({
                        type: 'POST',
                        url: '${pageContext.request.contextPath}/customer/queryAllContacts.do',
                        dataType: 'json',
                        async: false,
                        success: function (datas){
                            for (var i = 0; i < datas.length; i++) {
                            	$("#contacts_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].contacts_name +"</option>");
                            }
                        }
                    });
                    laydate.render({
                        elem: '#create_time'
                    });
                    form.render();
                }
            });
        });

        //编辑
        table.on('tool(staffList)', function(obj){
            var data = obj.data;
            if (obj.event === 'update') {
                layer.open({
                    type: 1 					//Page层类型
                    ,area: ['670px', ''] 	//宽  高
                    ,title: '编辑'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="updateDivID">'+
                            '<form class="layui-form" id="updateFormID">'+
                                '<xblock>'+
                                    '<table>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td>'+
                                                '<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">联系人</label></td>'+
                                            '<td>'+
                                                '<div style="width: 190px; float: left;">' +
                                                    '<select id="contacts_id2" name="contacts_id" lay-verify="contacts_id">' +
                                                        '<option value="">请选择</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td><label class="layui-form-label">客户名称</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_name" name="customer_name" value="'+data.customer_name+'" lay-verify="customer_name" style="width: 190px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">客户编号</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_num" name="customer_num" value="'+data.customer_num+'" lay-verify="customer_num" style="width: 190px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td><label class="layui-form-label">客户类型</label></td>'+
                                            '<td>'+
                                                '<div style="width: 190px; float: left;">' +
                                                    '<select name="customer_type" id="customer_type2"  lay-verify="customer_type" value="'+data.customer_type+'">' +
                                                        '<option value="">请选择客户类型</option>' +
                                                        '<option value="0">供应商</option>' +
                                                        '<option value="1">主机厂</option>' +
                                                    '</select>'+
                                                '</div>'+
                                                '<div style="margin-top: 12px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">客户所在地区</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_region" name="customer_region" value="'+data.customer_region+'" lay-verify="customer_region" style="width: 190px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td><label class="layui-form-label">客户地址</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_adress" name="customer_adress" value="'+data.customer_adress+'" style="width: 190px;">'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">客户邮箱</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_email" name="customer_email" value="'+data.customer_email+'" style="width: 190px;">'+
                                            '</td>'+
                                            '<td><label class="layui-form-label">客户邮编</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="customer_zipcode" name="customer_zipcode" value="'+data.customer_zipcode+'" style="width: 190px;">'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 10px"></tr>'+
                                        '<tr>'+
		                                    '<td><label class="layui-form-label">是否有码</label></td>'+
		                                    '<td>'+
			                                    '<div style="width: 190px; float: left;">' +
			                                        '<select name="barcode" id="barcode2" lay-verify="barcode" value="'+data.barcode+'">' +
			                                            '<option value="0">无码</option>' +
			                                            '<option value="1">有码</option>' +
			                                        '</select>'+
			                                    '</div>'+
		                                    '</td>'+
		                                    '<td><label class="layui-form-label">是否拍照</label></td>'+
		                                    '<td>'+
			                                    '<div style="width: 190px; float: left;">' +
			                                        '<select name="photo" id = "photo2" lay-verify="photo" value="'+data.photo+'">' +
			                                            '<option value="0">否</option>' +
			                                            '<option value="1">是</option>' +
			                                        '</select>'+
			                                    '</div>'+
		                                    '</td>'+
		                            	'</tr>'+
		                            	'<tr style="height: 10px"></tr>'+
                                        '<tr>'+
                                            '<td><label class="layui-form-label">创建日期</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="create_time" name="create_time" value="'+data.create_time+'" lay-verify="create_time" style="width: 190px; display:inline">'+
                                                '<div class="layui-inline" style="margin-top: 10px;">'+
                                                    '<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                                '</div>'+
                                            '</td>'+
                                            '<td><label class="layui-form-label">备注</label></td>'+
                                            '<td>'+
                                                '<input class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 190px">'+
                                            '</td>'+
                                        '</tr>'+
                                        '<tr style="height: 20px"></tr>'+
                                        '<tr align="center">'+
                                            '<td colspan="2"><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
                                            '<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
                                        '</tr>'+
                                    '</table>'+
                                '</xblock>'+
                            '</form>'+
                        '</div>'
                    ,success: function(){
                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/customer/queryAllContacts.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                    $("#contacts_id2").append("<option value='"+ datas[i].id +"'>"+ datas[i].contacts_name +"</option>");
                                    
                                    if(datas[i].id == data.contacts_id){
                                        $("#contacts_id2").val(data.contacts_id);
                                    }
                                }
                            }
                        });
                        laydate.render({
                            elem: '#create_time'
                        });
                        
                        $("#photo2").val(data.photo); 
                        $("#customer_type2").val(data.customer_type); 
                        $("#photo2").val(data.photo); 
                        $("#barcode2").val(data.barcode); 
                        
                        
                        
                        
                        form.render();
                    }
                });
            } else if (obj.event === 'bindingWarehouse'){
                layer.open({
                    type: 1 				//Page层类型
                    ,area: ['500px', '400px'] 	//宽  高
                    ,title: '绑定仓库'
                    ,shade: 0.6 			//遮罩透明度
                    ,maxmin: true 			//允许全屏最小化
                    ,anim: 1 				//0-6的动画形式，-1不开启
                    ,content:$('#formDiv2')
                    ,success:function(){
                        $("#id2").val(data.id);
                        $.ajax({
                            type:'post'
                            ,url:'${pageContext.request.contextPath }/materiel/wareTree.do?regionType='+data.placement_type
                            ,dataType:'json'
                            ,success:function(data){
                                formSelects.data('select2', 'local', {
                                    arr: data.rows,
                                    linkage: true
                                });
                            }
                        })

                        $.ajax({
                            type: 'POST',
                            url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomer.do',
                            dataType: 'json',
                            async: false,
                            success: function (datas){
                                for (var i = 0; i < datas.length; i++) {
                                    $("#client2").append("<option value='"+ datas[i].customer_name +"'>"+ datas[i].customer_name +"</option>");
                                    if(datas[i].customer_name == data.client){
                                        $("#client2").val(data.client);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        });

        //批量删除
        $("#del").click(function(){
            var rowData = table.checkStatus('contenttable');
            var data = rowData.data;
            var idArr = new Array();
            if(data.length == 0){
                toastrStyle();
                toastr.warning("请至少选择一条记录！");
            } else {
                for(var i=0;i < data.length;i++){
                    idArr[i] = data[i].id;
                }
                $.ajax({
                    type:'post',
                    url:'${pageContext.request.contextPath }/customer/deleteCustomerById.do',
                    data:{"idArr" : idArr},
                    dataType:"json",
                    success:function(data){
                        layer.confirm('确定删除吗？', function(){
                            if(data > 0){
                                toastr.success("删除成功！");
                                setTimeout(function(){
                                    location.reload();
                                },1000);
                                setTimeout(function(){
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                                location.reload();
                            }
                        });
                    }
                });
            }
        });

        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(addForm)', function () {
            $.ajax({
                type:'post',
                url:'${pageContext.request.contextPath }/customer/addCustomer.do',
                data:$("#addFormID").serialize(),
                cache:false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('新增成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('新增失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        })

        /**
         * 通用表单编辑(AJAX方式)
         */
        form.on('submit(updateForm)', function () {
            $.ajax({
                type : 'post',
                url : '${pageContext.request.contextPath }/customer/updateCustomer.do',
                data : $("#updateFormID").serialize(),
                cache : false,
            }).done(
                function(res) {
                    if (res > 0) {
                        toastr.success('修改成功！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                }
            ).fail(
                function() {
                    toastr.error('修改失败！');
                    setTimeout(function(){
                        location.reload();
                    },1000);
                }
            )
            return false;
        });

        //绑定仓库
        form.on('submit(addform2)', function (data) {
            $("#subform2").attr("disabled" , true);
            $.ajax({
                url : '${pageContext.request.contextPath }/materiel/warehouseBindingWarehouse.do',
                data : $('#addform2').serialize(),
                type : "post",
                //dataType : "json",
            }).done(
                function(res) {
                    if(res>0){
                        toastr.success('绑定成功！');
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }else if(res==-2){
                    	toastr.error('仓库已被绑定！');
                    	$("#subform2").attr("disabled" , false);
                    }else{
                        toastr.success('清空绑定！');
                        setTimeout(function(){
                            location.reload();
                        },2000);

                    }
                }
            )
            return false;
        });
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function dateToStr(date) {
        var time = new Date(date.time);
        var y = time.getFullYear();
        var M = time.getMonth() + 1;
        M = M < 10 ? ("0" + M) : M;
        var d = time.getDate();
        d = d < 10 ? ("0" + d) : d;
        var str = y + "-" + M + "-" + d;
        return str;
    }
</script>
</body>
</html>
