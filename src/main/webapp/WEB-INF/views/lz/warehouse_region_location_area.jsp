<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>货架区、平地区信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
		<span class="layui-breadcrumb">
  			<a href="">首页</a>
			<a>
				<cite>货架区、平地区信息</cite>
			</a>
		</span>
</div>
<div class="x-body">
    <div class="layui-row">
        <form class="layui-form layui-col-md12 x-so" onsubmit="return false">
            <input type="text" name="keyword" id="keyword"  placeholder="请输入货架区、平地区名称" autocomplete="off" class="layui-input">
            <button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
        </form>
    </div>
    <xblock>
        <button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
        <button class="layui-btn layui-btn-danger" id="delete"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
    </xblock>
    <table class="layui-hide" id="staffList" lay-filter="staffList"></table>
    <script type="text/html" id="rowToolbar">
        <a class="layui-btn layui-btn-xs" lay-event="edit"><i class="layui-icon layui-icon-edit"></i>编辑</a>
    </script>
</div>
<script type="text/javascript">

    layui.use(['table','layer','upload','form','laydate'], function(){
        var table = layui.table;
        var layer = layui.layer;
        var form = layui.form;
        var laydate = layui.laydate;

        var $ = layui.jquery, active = {
            reload:function () {
                var keyword01 = $("#keyword").val();
                table.reload('contenttable',{
                    method:'get',
                    where:{"keyword01":keyword01}
                });
            }
        }

        $('.layui-btn').on('click', function(){
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        table.render({
            elem: '#staffList'
            ,url:'${pageContext.request.contextPath }/json/warehouse_region_location_area.json'
            ,title: '货架区、平地区列表'
            ,id :'contenttable'
            ,limits:[10,20,30]
            ,cols: [[
                {type: 'checkbox', fixed: 'left'}
                ,{field:'', title:'序号', sort: true, type:'numbers', width: 60}
                ,{field:'location_id', title:'库位名称'}
                ,{field:'area_name', title:'货架区、平地区名称'}
                ,{field:'area_num', title:'货架区、平地区编号'}
                ,{field:'area_floor', title:'货架区层编号'}
                ,{field:'area_floor_cell', title:'货架区单元格编号'}
                ,{field:'createTime', title:'创建时间'}
                //,{field:'updatedTime', title:'修改时间'}
                ,{field:'remark', title:'备注'}
                ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
            ]]
            ,page: true
            ,done : function(){
                $('th').css({
                    'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    
                })
            }
        });

        //监听复选框事件
        table.on('checkbox(staffList)',function(obj){
            if(obj.checked == true && obj.type == 'all'){
                //点击全选
                $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
            }else if(obj.checked == false && obj.type == 'all'){
                //点击全不选
                $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
            }else if(obj.checked == true && obj.type == 'one'){
                //点击单行
                if(obj.checked == true){
                    obj.tr.addClass('layui-table-click');
                }else{
                    obj.tr.removeClass('layui-table-click');
                }
            }else if(obj.checked == false && obj.type == 'one'){
                //点击全选之后点击单行
                if(obj.tr.hasClass('layui-table-click')){
                    obj.tr.removeClass('layui-table-click');
                }
            }
        })

        $("#delete").click(function(){
            var checkStatus = table.checkStatus("contenttable");
            var data = checkStatus.data;
            var str = "";
            if(data.length==0){
                toastr.warning("请至少选择一条记录！");
            }else{
                layer.confirm('确定删除吗？', function(index){
                    var idArr = new Array();
                    var id = data.id;
                    idArr[0] = id;
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/staff/deleteStaff.do',
                        data:{"idArr":idArr},
                        success:function(affact){
                            if(affact>0){
                                toastr.success("删除成功！");
                                setTimeout(function(){  //使用  setTimeout（）方法设定定时2000毫秒
                                    //关闭模态框
                                    // 父页面刷新
                                    window.location.reload();
                                },2000);
                            }else{
                                toastr.warning("删除失败！");
                            }
                        }

                    })
                    layer.close(index);
                });
            }
        })

        /**
         * 表单校验
         */
        form.verify({
            //value：表单的值、item：表单的DOM对象
            warehouse_name: function(value, item){
                if(value == ''){
                    return '货架区、平地区名称不能为空';
                }
            }
            ,warehouse_num: function(value, item){
                if(value == '-1'){
                    return '货架区、平地区编号不能为空';
                }
            }
            ,warehouse_properties: function(value, item){
                if(value == ''){
                    return '货架区、平地区属性不能为空';
                }
            }
            ,warehouse_admin: function(value, item){
                if(value == ''){
                    return '货架区、平地区管理员不能为空';
                }
            }
        });
        /**
         * 通用表单提交(AJAX方式)
         */
        form.on('submit(submitBtn)', function (data) {
            $.ajax({
                type:'post'
                ,url:'${pageContext.request.contextPath }/staff/saveStaff.do'
                ,data:$("#addform").serialize()
                ,success:function(res){
                    var list = res.split('_');
                    if (parseInt(list[0]) > 0) {
                        if(list[1].indexOf('add') != -1){
                            layer.closeAll();
                            toastr.success('新增成功！');
                        }
                        if(list[1].indexOf('edit') != -1){
                            layer.closeAll();
                            toastr.success('修改成功！');
                        }
                        setTimeout(function(){
                            //使用  setTimeout（）方法设定定时2000毫秒
                            //关闭模态框
                            // 父页面刷新
                            window.location.reload();
                        },2000);
                    }else{
                        if(list[1].indexOf('add') != -1){
                            toastr.error('新增失败！');
                        }
                        if(list[1].indexOf('edit') != -1){
                            toastr.error('修改失败！');
                        }
                    }
                }
                ,error:function(){

                }
            })
        })
    });
    toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
    //日期格式转换
    function date2String(timestamp){
        var d = new Date(timestamp);
        var date = (d.getFullYear()) + "-" +
            (d.getMonth() + 1<10?"0"+(d.getMonth() + 1):d.getMonth() + 1) + "-" +
            (d.getDate()<10?"0"+d.getDate():d.getDate());
        return date;
    }
</script>
</body>

</html>