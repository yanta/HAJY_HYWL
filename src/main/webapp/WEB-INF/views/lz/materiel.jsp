<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>物料信息</title>
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.css">
	<link rel="stylesheet" media="all" href="${pageContext.request.contextPath }/assets/formSelects/formSelects-v4.js">
	<style type="text/css">
		.tmdyTable td{border:1px solid #00000080;} 
		.tmdyTable td input{
			width:100%;
			height:100%;
			border:0px;
			padding:0px;
			margin:0px;
		}
	</style>
</head>
<body class="layui-anim layui-anim-up">
<div class="x-nav">
    <span class="layui-breadcrumb">
        <a href="">首页</a>
        <a>
            <cite>物料信息</cite>
        </a>
    </span>
</div>
<div class="x-body">
	<div class="layui-row">
		<form class="layui-form layui-col-md12 x-so" onsubmit="return false">
			<input type="text" name="keyword1" id="keyword1" placeholder="请输入供应商名称" autocomplete="off" class="layui-input">
			<input type="text" name="keyword2" id="keyword2" placeholder="请输入备注" autocomplete="off" class="layui-input">
			<input type="text" name="keyword3" id="keyword3" placeholder="请输入SAP/QAD" autocomplete="off" class="layui-input">
			<input type="text" name="keyword4" id="keyword4" placeholder="请输入中文名称" autocomplete="off" class="layui-input">
			<button class="layui-btn layui-btn-normal" data-type="reload"><i class="layui-icon layui-icon-search"></i>检索</button>
		</form>
	</div>
	<xblock>
		<button class="layui-btn layui-btn-warm" id="add"><i class="layui-icon layui-icon-add-circle-fine"></i>新增</button>
		<button class="layui-btn layui-btn-danger" id="del"><i class="layui-icon layui-icon-delete"></i>批量删除</button>
		<button class="layui-btn layui-btn-danger" id="import"><i class="layui-icon layui-icon-download-circle"></i>导入</button>
		<button class="layui-btn layui-btn-danger" id="export"><i class="layui-icon layui-icon-upload"></i>导出</button> 
		<button class="layui-btn layui-btn-danger" id="dybq"><i class="layui-icon layui-icon-upload-drag"></i>打印标签</button>
	</xblock>
	<table class="layui-hide" id="staffList" lay-filter="staffList"></table>
	<script type="text/html" id="rowToolbar">
		<a class="layui-btn layui-btn-xs" lay-event="update"><i class="layui-icon layui-icon-edit"></i>编辑</a>
		<%--<a class="layui-btn layui-btn-xs" lay-event="bindingCustomer"><i class="layui-icon layui-icon-edit"></i>绑定客户</a>--%>
		<%--<a class="layui-btn layui-btn-xs" lay-event="bindingWarehouse"><i class="layui-icon layui-icon-edit"></i>绑定仓库</a>--%>
	</script>
	<div id="formDiv" hidden="hidden">
		<div class="box">
			<form class="layui-form layui-card-body" id="addform">
				<input hidden id="id" name="id">
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">客户</label>
						<div class="layui-input-inline" style="width: 285px">
							<select style="width: 285px" class="" name="client" id="client" lay-filter="client" xm-select="select1">
								<c:forEach items="${customerList }" var="c">
									<option value="${c.id }">${c.customer_name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform" id="subform">确定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<%--导入模板--%>
	<div class="modal fade" id="importDiv" hidden="hidden" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<div style="text-align: center; padding: 5px">
						<a href="${pageContext.request.contextPath }/exceltemplate/materiel.xls">
							<img alt="导入客户信息-模板" src="${pageContext.request.contextPath }/images/excel_alt_1.png" style="width: 20px;"/>
							<span style="font-size: 15px; font-weight:200;color:red">materiel-模板.xls</span>
						</a>
					</div>
					<form id="importForm" class="layui-form" enctype="multipart/form-data">
						<div title="Excel导入操作" style="margin-left: 105px; padding: 5px">

							<input id="file" name="file" type="file" style="margin-bottom: 20px" accept=".xls,.xlsx"/>
						</div>
					</form>
					<div align="center">
						<button class="layui-btn layui-btn-blue" onclick="uploadBtn()">导入</button>
						&emsp;&emsp;
						<button class="layui-btn layui-btn-primary" onclick="qx()">取消</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="formDiv2" hidden="hidden">
		<div class="box">
			<form class="layui-form layui-card-body" id="addform2">
				<input hidden="hidden" id="id2" name="id">
				<!-- _______________________________________________ -->
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">客户</label>
						<div class="layui-input-inline" style="width: 285px">
							<select style="width: 285px" class="" name="remark" id="client2" lay-filter="client2" xm-select="select1">
								<c:forEach items="${customerList }" var="c">
									<option value="${c.id }">${c.customer_name }</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</div>
				<!-- _______________________________________________ -->
				<div class="layui-form-item">
					<div class="layui-inline">
						<label class="layui-form-label">仓库</label>
						<div class="layui-input-inline" style="width: 285px">
							<select style="width: 285px" class="" name="ware" id="ware" lay-filter="ware" xm-select="select2"></select>
						</div>
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn  layui-btn-blue" lay-submit lay-filter="addform2" id="subform2">确定</button>
						<button class="layui-btn layui-btn-primary" style="margin-left:120px">取消</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<!-- 打印标签信息输入 -->
	<div id="dyXXDivID" hidden="hidden">
		<form class="layui-form layui-card-body">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">批次号</label>
					<div class="layui-input-inline">
						<input class="layui-input" type="text" id="dy_pch" style="width: 300px">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">包装数量</label>
					<div class="layui-input-inline">
						<input class="layui-input" type="text" id="dy_bznum" style="width: 300px">
					</div>
				</div>
			</div>
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label">标签数量</label>
					<div class="layui-input-inline">
						<input class="layui-input" type="text" id="dy_num" style="width: 300px">
					</div>
				</div>
			</div>
		</form>
		<xblock>
			<div align="right" style="margin-bottom: 0px;">
				<button type="button" id="dytm" class="layui-btn" style="margin-left: 400px;background-color: #57575799;"><i class="layui-icon">&#xe66e;</i>打印</button>
			</div>
		</xblock>
	</div>
	<!-- 打印标签框 -->
	<div id="dytmDivID" hidden="hidden">
		<div id="tmdyDiv" style="width: 100%;overflow-y:scroll;height: 499px">
		</div>
		<xblock>
			<div align="right" style="margin-bottom: 0px;">
				<button type="button" id="dayingTmdyTable" class="layui-btn" style="margin-left: 450px;background-color: #57575799;"><i class="layui-icon">&#xe66e;</i>打印</button>
			</div>
		</xblock>
	</div>
	<script type="text/javascript">
        var opts  = document.getElementById("client");
        function check(id){
            for(var i = 0;i<opts.options.length;i++){
                if(opts.options[i].value == id){
                    opts.options[i].setAttribute("selected", "selected");
                }
            }
        }
        //全局定义一次, 加载formSelects
        layui.config({
            base: '${pageContext.request.contextPath }/assets/formSelects/' //此处路径请自行处理, 可以使用绝对路径
        }).extend({
            formSelects: 'formSelects-v4'
        });
        layui.use(['table','layer','upload','form','laydate','formSelects'], function(){
            var table = layui.table;
            var layer = layui.layer;
            var form = layui.form;
            var laydate = layui.laydate;
            var formSelects = layui.formSelects;
            var $ = layui.jquery, active = {
                reload:function () {
                    var keyword01 = $("#keyword1").val();
                    var keyword02 = $("#keyword2").val();
                    var keyword03 = $("#keyword3").val();
                    var keyword04 = $("#keyword4").val();
                    table.reload('contenttable',{
                        method:'get',
                        where:{"keyword01":keyword01, "keyword02":keyword02, "keyword03":keyword03, "keyword04":keyword04}
                    });
                }
            }

            $('.layui-btn').on('click', function(){
                var type = $(this).data('type');
                active[type] ? active[type].call(this) : '';
            });
            table.render({
                elem: '#staffList'
                ,url:'${pageContext.request.contextPath }/materiel/queryMaterielCriteria.do'
                ,title: '物料列表'
                ,id :'contenttable'
                ,limits:[10,20,30]
                ,toolbar: '#toolbar'
                ,cols: [[
                    {type: 'checkbox', fixed: 'left'}
                    ,{field:'', title:'序号', sort: true, type:'numbers', width: 60, align:'center'}
                    ,{field:'customer_name', title:'供应商名称', width: 110, align:'center'}
                    ,{field:'container_name', title:'容器名称', width: 90, align:'center'}
                    ,{field:'region_name', title:'库区名称', width: 90, align:'center'}
                    ,{field:'placement_type', title:'放置类型', width: 90, align:'center', templet: function (row){
                        if(row.placement_type == '0'){
                            return "货架";
                        } else {
                            return "平地";
                        }
                    }}
                    ,{field:'materiel_num', title:'SAP/QAD', width: 140, align:'center'}
                    ,{field:'materiel_name', title:'中文名称', width: 140, align:'center'}
                    ,{field:'brevity_num', title:'零件号', width: 140, align:'center'}
                    ,{field:'unit', title:'单位', width: 80, align:'center'}
                    ,{field:'packing_quantity', title:'包装数量', width: 90, align:'center'}
                    ,{field:'upper_value', title:'物料上限值', width: 100, align:'center'}
                    ,{field:'lower_value', title:'物料下限值', width: 100, align:'center'}
                    ,{field:'remark', title:'备注', width: 120, align:'center'}
                    ,{field:'is_bindtray', title:'是否绑定托盘', width: 100, align:'center', templet: function (row){
                        if(row.is_bindtray == 0){
                            return "否";
                        } else {
                            return "是";
                        }
                    }}
                    ,{field:'tray_type', title:'托盘类型', width: 100, align:'center', templet: function (row){
                        if(row.tray_type == '0'){
                            return "物理托盘";
                        } else {
                            return "逻辑托盘";
                        }
                    }}
                    ,{field:'is_check', title:'是否质检', width: 100, align:'center', templet: function (row){
                        if(row.is_check == 0){
                            return "是";
                        } else {
                            return "否";
                        }
                    }}
                    ,{field:'volume', title:'体积', width: 80, align:'center'}
                    ,{field:'weight', title:'重量', width: 80, align:'center'}
                    ,{field:'delivery_address', title:'收货地址', width: 90, align:'center'}
                    ,{field:'outgoing_frequency', title:'出库频率', width: 90, align:'center'}
                    ,{field:'materiel_size', title:'物料规格', width: 90, align:'center'}
                    ,{field:'materiel_properties', title:'物料型号', width: 90, align:'center'}
                    ,{field:'out_date', title:'出厂日期', width: 110, align:'center', templet: function (row){
                        if(row.out_date != null) {
                            return row.out_date;
                        } else {
                            return "";
                        }
                    }}
                    ,{fixed:'right',unresize: true, title:'操作', toolbar: '#rowToolbar',width:100, align: 'center'}
                  	//,{field:'clientName', title:'所属客户', width: 200}
                    //,{field:'wareName', title:'存放仓库', width: 200}
                    //,{field:'quality_guarantee_period', title:'保质期', width: 80}
                    //,{field:'allowable_storage_days', title:'库龄', width: 120}
                    //,{field:'code_rule', title:'编码规则', width: 90}
                    /* ,{field:'is_devanning', title:'是否拆箱', width: 90, templet: function (row){
                            if(row.is_devanning == '0') {
                                return "是";
                            } else {
                                return "否";
                            }
                        }}
                    ,{field:'devanning_num', title:'拆箱数', width: 80}
                    ,{field:'storage_type', title:'入库类型', width: 90, templet: function (row){
                        if(row.storage_type == '0'){
                            return "普通入库";
                        } else {
                            return "精准入库";
                        }
                    }}
                    ,{field:'barcode_type', title:'条码类型', width: 90, templet: function (row){
                        if(row.barcode_type == 0){
                            return "精准码";
                        } else if (row.barcode_type == 1) {
                            return "产品码";
                        } else {
                            return "简码";
                        }
                    }}
                    ,{field:'sequence_number', title:'顺序号', width: 80}
                    ,{field:'container_name', title:'拆箱后容器', width: 100} */
                ]]
                ,page: true
                ,done : function(){
                    $('th').css({
                        'background-color': '#009688', 'color': '#fff','font-weight':'bold',
                    })
                }
            });

            //监听复选框事件
            table.on('checkbox(staffList)',function(obj){
                if(obj.checked == true && obj.type == 'all'){
                    //点击全选
                    $('.layui-table-body table.layui-table tbody tr').addClass('layui-table-click');
                }else if(obj.checked == false && obj.type == 'all'){
                    //点击全不选
                    $('.layui-table-body table.layui-table tbody tr').removeClass('layui-table-click');
                }else if(obj.checked == true && obj.type == 'one'){
                    //点击单行
                    if(obj.checked == true){
                        obj.tr.addClass('layui-table-click');
                    }else{
                        obj.tr.removeClass('layui-table-click');
                    }
                }else if(obj.checked == false && obj.type == 'one'){
                    //点击全选之后点击单行
                    if(obj.tr.hasClass('layui-table-click')){
                        obj.tr.removeClass('layui-table-click');
                    }
                }
            })

            /**
             * 表单校验
             */
            form.verify({
                //value：表单的值、item：表单的DOM对象
                customer_id: function(value, item){
                    if(value == ''){
                        return '供应商名称不能为空';
                    }
                },
                region_id: function(value, item){
                    if(value == ''){
                        return '库区名称不能为空';
                    }
                },
                container_id: function(value, item){
                    if(value == ''){
                        return '容器名称不能为空';
                    }
                },
                materiel_name: function(value, item){
                    if(value == ''){
                        return '中文名称不能为空';
                    }
                },
                materiel_num: function(value, item){
                    if(value == ''){
                        return 'SAP/QAD不能为空';
                    }
                },
                brevity_num: function(value, item){
                    if(value == ''){
                        return '零件号不能为空';
                    }
                },
                is_check: function(value, item){
                    if(value == ''){
                        return '质检不能为空';
                    }
                },
                materiel_size: function(value, item){
                    if(value == ''){
                        return '物料规格不能为空';
                    }
                },
                materiel_properties: function(value, item){
                    if(value == ''){
                        return '物料型号不能为空';
                    }
                },
                out_date: function(value, item){
                    if(value == ''){
                        return '出厂日期不能为空';
                    }
                },
                delivery_address: function(value, item){
                    if(value == ''){
                        return '收货地址不能为空';
                    }
                },
                upper_value: function(value, item){
                    if(value == ''){
                        return '物料上限值不能为空';
                    }
                },
                lower_value: function(value, item){
                    if(value == ''){
                        return '物料下限值不能为空';
                    }
                },
                packing_quantity: function(value, item){
                    if(value == ''){
                        return '包装数量不能为空';
                    }
                },
                /*is_devanning: function(value, item){
                    if(value == ''){
                        return '包装数量不能为空';
                    }
                },
                removal_container: function(value, item){
                    if(value == ''){
                        return '拆箱后容器不能为空';
                    }
                },*/
                storage_type: function(value, item){
                    if(value == ''){
                        return '入库类型不能为空';
                    }
                },
                is_bindtray: function(value, item){
                    if(value == ''){
                        return '是否绑定托盘不能为空';
                    }
                },
                tray_type: function(value, item){
                    if(value == ''){
                        return '托盘类型不能为空';
                    }
                }
            });

            //新增
            $("#add").click(function(){
            	var timestamp = (new Date()).getTime();
                layer.open({
                    type: 1 				//Page层类型
                    ,area: ['670px', ''] 	//宽  高
                    ,title: '新增'
                    ,shade: 0.6 			//遮罩透明度
                    ,maxmin: true 			//允许全屏最小化
                    ,anim: 1 				//0-6的动画形式，-1不开启
                    ,content:
                        '<div id="addDivID">'+
							'<form class="layui-form" id="addFormID">'+
								'<input name="materiel_size" lay-verify="materiel_size" type="hidden" value="暂无">'+
								'<input name="materiel_properties" lay-verify="materiel_properties" type="hidden" value="暂无">'+
								'<xblock>'+
									'<table>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">供应商名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="customer_id" name="customer_id" lay-verify="customer_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">容器名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="container_id" name="container_id" lay-verify="container_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">库区名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="region_id" name="region_id" lay-verify="region_id" lay-filter="region_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">放置类型</label></td>'+
											'<td>'+
												/* 
												'<input readonly class="layui-input" id="placement_type" name="placement_type" style="width: 190px; color:gray; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+ 
												*/
												'<div style="width: 190px; float: left;">' +
													'<select id="placement_type" name="placement_type" lay-verify="placement_type">' +
														'<option value="0">货架</option>' +
														'<option value="1">平地</option>' +
														'<option value="-1">不良品库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">中文名称</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_name" name="materiel_name" lay-verify="materiel_name" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">SAP/QAD</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_num" name="materiel_num" lay-verify="materiel_num" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">零件号</label></td>'+
											'<td>'+
												'<input class="layui-input" id="brevity_num" name="brevity_num"  lay-verify="brevity_num" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+	
											'</td>'+
											'<td><label class="layui-form-label">质检</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="is_check" name="is_check" lay-verify="is_check">' +
														'<option value="">请选择</option>' +
														'<option value="0">是</option>' +
														'<option value="1">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
												/* '<input class="layui-input" id="is_check" name="is_check" lay-verify="is_check" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+ */
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										/* '<tr>'+
											'<td><label class="layui-form-label">物料规格</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_size" name="materiel_size" lay-verify="materiel_size" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">物料型号</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_properties" name="materiel_properties" lay-verify="materiel_properties" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+ */
										'<tr>'+
											'<td><label class="layui-form-label">出厂日期</label></td>'+
											'<td>'+
												'<input class="layui-input" id="out_date" name="out_date" lay-verify="out_date" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">单位</label></td>'+
											'<td>'+
												'<input class="layui-input" id="unit" name="unit" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">体积</label></td>'+
											'<td>'+
												'<input class="layui-input" id="volume" name="volume" style="width: 190px;">'+
											'</td>'+
											'<td><label class="layui-form-label">重量</label></td>'+
											'<td>'+
												'<input class="layui-input" id="weight" name="weight" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">收货地址</label></td>'+
											'<td>'+
												'<input class="layui-input" id="delivery_address" name="delivery_address" lay-verify="delivery_address" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">出库频率</label></td>'+
											'<td>'+
												'<input class="layui-input" id="outgoing_frequency" name="outgoing_frequency" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">包装数量</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="packing_quantity" name="packing_quantity" lay-verify="packing_quantity" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">物料上限值</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="upper_value" name="upper_value" lay-verify="upper_value" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											/* '<td><label class="layui-form-label">入库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="storage_type" name="storage_type" lay-verify="storage_type">' +
														'<option value="0">普通入库</option>' +
														'<option value="1">精准入库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+ */
											/*'<td><label class="layui-form-label">是否拆箱</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select name="is_devanning" lay-verify="is_devanning">' +
														'<option value="0">是</option>' +
														'<option value="1">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+*/
										'</tr>'+
										/*'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">拆箱后容器</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
												'<select id="removal_container" name="removal_container" lay-verify="removal_container">' +
													'<option value="">请选择</option>' +
												'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">入库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="storage_type" name="storage_type" lay-verify="storage_type">' +
														'<option value="0">普通入库</option>' +
														'<option value="1">精准入库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+*/
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">物料下限值</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="lower_value" name="lower_value" lay-verify="lower_value" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">绑定托盘</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="is_bindtray" name="is_bindtray" lay-verify="is_bindtray" lay-filter="is_bindtray">' +
														'<option value="">请选择</option>' +	
														'<option value="1">是</option>' +
														'<option value="0">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">托盘类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select readonly id="tray_type" name="tray_type" lay-verify="tray_type" disabled="disabled">' +
														'<option value="">请选择</option>' +
														'<option value="0">物理托盘</option>' +
														'<option value="1">逻辑托盘</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+	
											'<td><label class="layui-form-label">备注</label></td>'+
											'<td>'+
												'<input class="layui-input" id="remark" name="remark" style="width: 190px">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 20px">'+
										'<tr align="center">'+
											'<td colspan="2"><button class="layui-btn layui-btn-blue" id="subBtn" lay-submit lay-filter="addForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
										'</tr>'+
									'</table>'+
									'</xblock>'+
								'</form>'+
							'</div>'
						,success: function(){
							//下拉框查找所有容器
							$.ajax({
								type: 'POST',
								url: '${pageContext.request.contextPath}/materiel/queryAllContainer.do',
								dataType: 'json',
								async: false,
								success: function (datas){
									for (var i = 0; i < datas.length; i++) {
										$("#container_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].container_name +"</option>");
										$("#removal_container").append("<option value='"+ datas[i].id +"'>"+ datas[i].container_name +"</option>");
									}
								}
							});
							//下拉框查找所有客户
							$.ajax({
								type: 'POST',
								url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomerByType.do',
								dataType: 'json',
								async: false,
								success: function (datas){
									for (var i = 0; i < datas.length; i++) {
										$("#customer_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].customer_name +"</option>");
									}
								}
							});
							//选择供应商添加联动
							/*查询没有被绑定的库位（华缘物流使用）*/
							//form.on('select(customer_id)', function(data){
							//$("#region_id").empty();
							$.ajax({
								type: 'POST',
								url: '${pageContext.request.contextPath}/materiel/queryRegionByCustomerId1.do',
								//data: {customer_id:data.value},
								dataType: 'json',
								async: false,
								success: function (datas){
									for (var i = 0; i < datas.length; i++) {
										$("#region_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].region_name +"</option>");
									}
								}
							});
							form.render();
							//});
							/*查询没有被绑定的库位（恒安捷运使用）*/
							/*form.on('select(customer_id)', function(data){
								$("#region_id").empty();
								$.ajax({
									type: 'POST',
									url: '${pageContext.request.contextPath}/materiel/queryRegionByCustomerId.do',
									data: {customer_id:data.value},
									dataType: 'json',
									async: false,
									success: function (datas){
										for (var i = 0; i < datas.length; i++) {
											$("#region_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].region_name +"</option>");
										}
									}
								});
								form.render();
							});*/
							//库区名称联动
							form.on('select(region_id)', function(data){
								$.ajax({
									type: 'POST',
									url: '${pageContext.request.contextPath}/materiel/queryRegionTypeById.do',
									data: {region_id:data.value},
									dataType: 'json',
									async: false,
									success: function (datas){
										var obj = document.getElementById("placement_type");
										var option = obj.options;
										if (datas[0].region_type == 0) {
											option[0].selected = true;
										} else if (datas[0].region_type == 1) {
											option[1].selected = true;
										} else {
											option[2].selected = true;
										}
										form.render('select');
									}
								});
							});
							//托盘绑定联动
							form.on('select(is_bindtray)', function(data){
								var obj = document.getElementById("tray_type");
								var option = obj.options;
								if (data.value != 0) {
									option[1].selected = true;
								} else {
									option[2].selected = true;
								}
								form.render('select');
							});
							laydate.render({
								elem: '#out_date'
							});
							form.render();
						}
					});
				});

				//编辑
				table.on('tool(staffList)', function(obj){
					var data = obj.data;
					var mid = data.id;
					if(obj.event === 'update'){
						layer.open({
							type: 1 				//Page层类型
							,area: ['670px', ''] 	//宽  高
							,title: '编辑'
							,shade: 0.6 			//遮罩透明度
							,maxmin: true 			//允许全屏最小化
							,anim: 1 				//0-6的动画形式，-1不开启
							,content:
							'<div id="updateDivID">'+
								'<form class="layui-form" id="updateFormID">'+
								'<input name="materiel_size" lay-verify="materiel_size" type="hidden" value="暂无">'+
								'<input name="materiel_properties" lay-verify="materiel_properties" type="hidden" value="暂无">'+
								'<xblock>'+
									'<table>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td>'+
												'<input class="layui-hide" id="id" name="id" value="'+data.id+'">'+
											'</td>'+
										'</tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">供应商名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="customer_id" name="customer_id" lay-verify="customer_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">容器名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="container_id" name="container_id" lay-verify="container_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">库区名称</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="region_id" name="region_id" lay-verify="region_id" lay-filter="region_id">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">放置类型</label></td>'+
											'<td>'+
												/* 
												'<input readonly class="layui-input" id="placement_type" name="placement_type" style="width: 190px; color:gray; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+ 
												*/
												'<div style="width: 190px; float: left;">' +
													'<select id="placement_type" name="placement_type" lay-verify="placement_type">' +
														'<option value="0">货架</option>' +
														'<option value="1">平地</option>' +
														'<option value="-1">不良品库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">中文名称</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_name" name="materiel_name" value="'+data.materiel_name+'" lay-verify="materiel_name" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">SAP/QAD</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_num" name="materiel_num" value="'+data.materiel_num+'" lay-verify="materiel_num" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">零件号</label></td>'+
											'<td>'+
												'<input class="layui-input" id="brevity_num" name="brevity_num" value="'+data.brevity_num+'" lay-verify="brevity_num" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">质检</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="is_check" name="is_check" lay-verify="is_check">' +
														'<option value="">请选择</option>' +
														'<option value="0">是</option>' +
														'<option value="1">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
												/* '<input class="layui-input" id="is_check" name="is_check" value="'+data.is_check+'" lay-verify="is_check" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+ */
											'</td>'+
										'</tr>'+
										
										/* '<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">物料规格</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_size" name="materiel_size" value="'+data.materiel_size+'" lay-verify="materiel_size" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
												'</td>'+
											'<td><label class="layui-form-label">物料型号</label></td>'+
											'<td>'+
												'<input class="layui-input" id="materiel_properties" name="materiel_properties" value="'+data.materiel_properties+'" lay-verify="materiel_properties" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+ */
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">出厂日期</label></td>'+
											'<td>'+
												'<input class="layui-input" id="out_date" name="out_date" lay-verify="out_date" value="'+data.out_date+'" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
                                					'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
                                				'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">单位</label></td>'+
											'<td>'+
												'<input class="layui-input" id="unit" name="unit" value="'+data.unit+'" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">体积</label></td>'+
											'<td>'+
												'<input class="layui-input" id="volume" name="volume" value="'+data.volume+'" style="width: 190px;">'+
											'</td>'+
											'<td><label class="layui-form-label">重量</label></td>'+
											'<td>'+
												'<input class="layui-input" id="weight" name="weight" value="'+data.weight+'" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">收货地址</label></td>'+
											'<td>'+
												'<input class="layui-input" id="delivery_address" name="delivery_address" value="'+data.delivery_address+'" style="width: 190px;">'+
											'</td>'+
											'<td><label class="layui-form-label">出库频率</label></td>'+
											'<td>'+
												'<input class="layui-input" id="outgoing_frequency" name="outgoing_frequency" value="'+data.outgoing_frequency+'" style="width: 190px;">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">包装数量</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="packing_quantity" name="packing_quantity" value="'+data.packing_quantity+'" style="width: 190px;">'+
											'</td>'+
											'<td><label class="layui-form-label">物料上限值</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="upper_value" name="upper_value" value="'+data.upper_value+'" lay-verify="upper_value" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											/* '<td><label class="layui-form-label">入库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="storage_type" name="storage_type" lay-verify="storage_type">' +
														'<option value="0">普通入库</option>' +
														'<option value="1">精准入库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+ */
											/*'<td><label class="layui-form-label">是否拆箱</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="is_devanning" name="is_devanning" lay-verify="is_devanning">' +
														'<option value="0">是</option>' +
														'<option value="1">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+*/
										'</tr>'+
										/*'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">拆箱后容器</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="removal_container" name="removal_container" value="'+data.removal_container+'" lay-verify="removal_container">' +
														'<option value="">请选择</option>' +
													'</select>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">入库类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="storage_type" name="storage_type" lay-verify="storage_type">' +
														'<option value="0">普通入库</option>' +
														'<option value="1">精准入库</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+*/
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">物料下限值</label></td>'+
											'<td>'+
												'<input class="layui-input" type="number" id="lower_value" name="lower_value" value="'+data.lower_value+'" lay-verify="lower_value" style="width: 190px; display:inline">'+
												'<div class="layui-inline" style="margin-top: 10px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
											'<td><label class="layui-form-label">绑定托盘</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select id="is_bindtray" name="is_bindtray" lay-verify="is_bindtray" lay-filter="is_bindtray">' +
														'<option value="">请选择</option>' +
														'<option value="1">是</option>' +
														'<option value="0">否</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 10px"></tr>'+
										'<tr>'+
											'<td><label class="layui-form-label">托盘类型</label></td>'+
											'<td>'+
												'<div style="width: 190px; float: left;">' +
													'<select readonly id="tray_type" name="tray_type" lay-verify="tray_type" disabled="disabled">' +
														'<option value="">请选择</option>' +
														'<option value="0">物理托盘</option>' +
														'<option value="1">逻辑托盘</option>' +
													'</select>'+
												'</div>'+
												'<div style="margin-top: 12px;">'+
													'<span style="color:red; font-size: 24px; margin-left: 4px">*</span>'+
												'</div>'+
												//'<input readonly class="layui-input" id="tray_type" name="tray_type" style="width: 190px; display:inline; color: gray">'+
											'</td>'+
											'<td><label class="layui-form-label">备注</label></td>'+
											'<td>'+
												'<input class="layui-input" id="remark" name="remark" value="'+data.remark+'" style="width: 190px">'+
											'</td>'+
										'</tr>'+
										'<tr style="height: 20px">'+
										'<tr align="center">'+
											'<td colspan="2"><button class="layui-btn layui-btn-blue" lay-submit lay-filter="updateForm" style="margin-left:160px; margin-bottom: 10px">提交</button></td>'+
											'<td colspan="2">&emsp;&emsp;&emsp;&emsp;<button class="layui-btn layui-btn-primary" style="margin-bottom: 10px">取消</button></td>'+
										'</tr>'+
									'</table>'+
								'</xblock>'+
                            '</form>'+
                         '</div>'
                        ,success: function(){
                            //下拉框查找所有容器
                            $.ajax({
                                type: 'POST',
                                url: '${pageContext.request.contextPath}/materiel/queryAllContainer.do',
                                dataType: 'json',
                                async: false,
                                success: function (datas){
                                    for (var i = 0; i < datas.length; i++) {
                                        $("#container_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].container_name +"</option>");
                                        if(datas[i].id == data.container_id){
                                            $("#container_id").val(data.container_id);
                                        }
                                        $("#removal_container").append("<option value='"+ datas[i].id +"'>"+ datas[i].container_name +"</option>");
                                        if(datas[i].id == data.removal_container){
                                            $("#removal_container").val(data.removal_container);
                                        }
                                    }
                                }
                            });
                            //下拉框查找所有客户
                            $.ajax({
                                type: 'POST',
                                url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomerByType.do',
                                dataType: 'json',
                                async: false,
                                success: function (datas){
                                    for (var i = 0; i < datas.length; i++) {
                                        $("#customer_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].customer_name +"</option>");
                                        if(datas[i].id == data.customer_id){
                                            $("#customer_id").val(data.customer_id);
                                        }
                                    }
                                }
                            });
                            //下拉框查找所有库区
                            $.ajax({
                                type: 'POST',
                                url: '${pageContext.request.contextPath}/stockOutOrder/queryAllRegion.do',
                                dataType: 'json',
                                async: false,
                                success: function (datas){
                                    for (var i = 0; i < datas.length; i++) {
                                        $("#region_id").append("<option value='"+ datas[i].id +"'>"+ datas[i].region_name +"</option>");
                                        if(datas[i].id == data.region_id){
                                            $("#region_id").val(data.region_id);
                                        }
                                    }
                                }
                            });
                            laydate.render({
                                elem: '#out_date'
                            });
                            //条码类型
                            $("#barcode_type").val(data.barcode_type);
                            //放置类型
                            //库区名称联动
							form.on('select(region_id)', function(data){
								$.ajax({
									type: 'POST',
									url: '${pageContext.request.contextPath}/materiel/queryRegionTypeById.do',
									data: {region_id:data.value},
									dataType: 'json',
									async: false,
									success: function (datas){
										var obj = document.getElementById("placement_type");
										var option = obj.options;
										if (datas[0].region_type == 0) {
											option[0].selected = true;
										} else if (datas[0].region_type == 1) {
											option[1].selected = true;
										} else {
											option[2].selected = true;
										}
										form.render('select');
									}
								});
							});
							//放置类型
							$("#placement_type").val(data.placement_type);
                            //入库类型
                            $("#storage_type").val(data.storage_type);
                            //是否拆箱
                            $("#is_devanning").val(data.is_devanning);
                            //是否绑定托盘
                            $("#is_bindtray").val(data.is_bindtray);
							//托盘类型
							$("#tray_type").val(data.tray_type);
							//托盘绑定联动
							form.on('select(is_bindtray)', function(data){
								var obj = document.getElementById("tray_type");
								var option = obj.options;
								if (data.value != 0) {
									option[1].selected = true;
								} else {
									option[2].selected = true;
								}
								form.render('select');
							});
							$("#tray_type").val(data.tray_type);
							//是否质检
							$("#is_check").val(data.is_check);
                            form.render();
                        }
                    });
                }else if(obj.event === 'bindingCustomer'){
                    layer.open({
                        type: 1 				//Page层类型
                        ,area: ['500px', ''] 	//宽  高
                        ,title: '绑定客户'
                        ,shade: 0.6 			//遮罩透明度
                        ,maxmin: true 			//允许全屏最小化
                        ,anim: 1 				//0-6的动画形式，-1不开启
                        ,content:$('#formDiv')
                        ,success:function(){
                            $("#id").val(data.id);
                            var client = data.client;
                            var c = client.split(",");
                            for(var i = 0;i<c.length;i++){
                                if(c[i]!=""){
                                    check(c[i])
                                }
                            }
                            form.render();
                        }
                        ,end: function () {

                        }
                    });
                } else if (obj.event === 'bindingWarehouse'){
                    layer.open({
                        type: 1 				//Page层类型
                        ,area: ['500px', ''] 	//宽  高
                        ,title: '绑定仓库'
                        ,shade: 0.6 			//遮罩透明度
                        ,maxmin: true 			//允许全屏最小化
                        ,anim: 1 				//0-6的动画形式，-1不开启
                        ,content:$('#formDiv2')
                        ,success:function(){
                            $("#id2").val(data.id);
                            $.ajax({
                                type:'post'
                                ,url:'${pageContext.request.contextPath }/materiel/wareTree.do?regionType='+data.placement_type
                                ,dataType:'json'
                                ,success:function(data){
                                    formSelects.data('select2', 'local', {
                                        arr: data.rows,
                                        linkage: true
                                    });
                                }
                            })

                            $.ajax({
                                type: 'POST',
                                url: '${pageContext.request.contextPath}/stockOutOrder/queryAllCustomer.do',
                                dataType: 'json',
                                async: false,
                                success: function (datas){
                                    for (var i = 0; i < datas.length; i++) {
                                        $("#client2").append("<option value='"+ datas[i].customer_name +"'>"+ datas[i].customer_name +"</option>");
                                        if(datas[i].customer_name == data.client){
                                            $("#client2").val(data.client);
                                        }
                                    }
                                }
                            });
                        }
                    });
                }
            });

            /*==============导出===============*/
            $("#export").click(function(){
                window.location.href="${pageContext.request.contextPath}/materiel/doExportall.do";
            })
            /*==============导入===============*/
            $("#import").click(function(){
                layer.open({
                    type: 1 					//Page层类型
                    ,area: ['380px', '200px'] 	//宽  高
                    ,title: '导入&nbsp;<span style="color: green">注：请点击红色链接下载模板</span>'
                    ,shade: 0.6 				//遮罩透明度
                    ,maxmin: true 				//允许全屏最小化
                    ,anim: 1 					//0-6的动画形式，-1不开启
                    ,content:$('#importDiv')
                });
            });

            //批量删除
            $("#del").click(function(){
                var rowData = table.checkStatus('contenttable');
                var data = rowData.data;
                var idArr = new Array();
                if(data.length == 0){
                    toastrStyle();
                    toastr.warning("请至少选择一条记录！");
                } else {
                    for(var i=0;i < data.length;i++){
                        idArr[i] = data[i].id;
                    }
                    $.ajax({
                        type:'post',
                        url:'${pageContext.request.contextPath }/materiel/deleteMaterielById.do',
                        data:{"idArr" : idArr},
                        success:function(data){
                            layer.confirm('确定删除吗？', function(){
                                if(data > 0){
                                    toastr.success("删除成功！");
                                    setTimeout(function(){
                                        location.reload();
                                    },1000);
                                    setTimeout(function(){
                                        window.location.reload();
                                    },2000);
                                }else{
                                    toastr.warning("删除失败！");
                                    location.reload();
                                }
                            });
                        }
                    });
                }
            });

            /**
             * 通用表单提交(AJAX方式)
             */
            form.on('submit(addForm)', function () {
                $.ajax({
                    type:'post',
                    url:'${pageContext.request.contextPath }/materiel/addMateriel.do',
                    data:$("#addFormID").serialize(),
                    cache:false,
                }).done(
                    function(res) {
                        if (res > 0) {
                            toastr.success('新增成功！');
                            setTimeout(function(){
                                location.reload();
                            },1000);
                        }
                    }
                ).fail(
                    function() {
                        toastr.error('新增失败！');
                        setTimeout(function(){
                            location.reload();
                        },1000);
                    }
                )
                return false;
            })
            form.on('submit(uplodeform)', function (data) {
                var fileName = $("#files").val();
                if (fileName.length > 0) {
                    var formData = new FormData($("#importForm")[0]);
                    $.ajax({
                        url: '${pageContext.request.contextPath}/materiel/uplodeFile.do' ,
                        type: 'post',
                        data: formData,

                        processData: false,
                        success: function (data) {
                            if (data == 1) {
                                toastr.success("文件导入成功！（客户名称重复的直接过滤）");
                                setTimeout(function() {//使用setTimeout()方法设定定时2000毫秒

                                },1000);
                            }else if(data == -1){
                                toastr.warning("导入的数据不符合导入条件（客户名称不能重复）！");
                            }else{
                                toastr.warning("文件格式异常，文件导入失败！");
                            }
                        },
                        error: function (data) {
                            toastr.error("文件格式异常，文件导入失败！");
                        }
                    });
                } else {
                    toastr.warning("上传文件为空，请选择要导入的文件！");
                }
            })
            /**
             * 通用表单编辑(AJAX方式)
             */
            form.on('submit(updateForm)', function () {
                $.ajax({
                    type : 'post',
                    url : '${pageContext.request.contextPath }/materiel/updateMateriel.do',
                    data : $("#updateFormID").serialize(),
                    cache : false,
                }).done(
                    function(res) {
                        if (res > 0) {
                            toastr.success('修改成功！');
                            setTimeout(function(){
                            	layer.closeAll();
                                //location.reload();
                            	var keyword01 = $("#keyword1").val();
                                var keyword02 = $("#keyword2").val();
                                var keyword03 = $("#keyword3").val();
                                var keyword04 = $("#keyword4").val();
                                table.reload('contenttable',{
                                    method:'get',
                                    where:{"keyword01":keyword01, "keyword02":keyword02, "keyword03":keyword03, "keyword04":keyword04}
                                });
                            },1000);
                        }
                    }
                ).fail(
                    function() {
                        toastr.error('修改失败！');
                        setTimeout(function(){
                        	layer.closeAll();
                            //location.reload();
                        	var keyword01 = $("#keyword1").val();
                            var keyword02 = $("#keyword2").val();
                            var keyword03 = $("#keyword3").val();
                            var keyword04 = $("#keyword4").val();
                            table.reload('contenttable',{
                                method:'get',
                                where:{"keyword01":keyword01, "keyword02":keyword02, "keyword03":keyword03, "keyword04":keyword04}
                            });
                        },1000);
                    }
                )
                return false;
            })
            //绑定客户
            form.on('submit(addform)', function (data) {
                $("#subform").attr("disabled",true);
                $.ajax({
                    url : '${pageContext.request.contextPath }/materiel/bindingCustomer.do',
                    data : $('#addform').serialize(),
                    type : "post",
                    //dataType : "json",
                }).done(
                    function(res) {
                        if(res>0){
                            toastr.success('绑定成功！');
                            setTimeout(function(){
                                location.reload();
                            },2000);
                        }
                    }
                )
                return false;
            });
            //绑定仓库
            form.on('submit(addform2)', function (data) {
                $("#subform2").attr("disabled",true);
                $.ajax({
                    url : '${pageContext.request.contextPath }/materiel/bindingWarehouse.do',
                    data : $('#addform2').serialize(),
                    type : "post",
                    //dataType : "json",
                }).done(
                    function(res) {
                        if(res>0){
                            toastr.success('绑定成功！');
                            setTimeout(function(){
                                location.reload();
                            },2000);
                        }else{
                            toastr.success('清空绑定！');
                            setTimeout(function(){
                                location.reload();
                            },2000);

                        }
                    }
                )
                return false;
            });
            
            //选择库区名称带回放置类型
            /* form.on('select(region_id)', function(data){
				$.ajax({
					type: 'POST',
					url: '${pageContext.request.contextPath}/materiel/queryRegionTypeById.do',
					data: {region_id:data.value},
					dataType: 'json',
					async: false,
					success: function (datas){
						if (datas[0].region_type == 0) {
							$("#placement_type").val("货架");
						} else if (datas[0].region_type == 1) {
							$("#placement_type").val("平地");
						} else {
							$("#placement_type").val("不良品");
						}
					}
				});
				form.render();
			}); */
			//打印标签信息录入
            $("#dybq").click(function(){
            	var checkStatus = table.checkStatus('contenttable')
                var datas = checkStatus.data;
                if(datas.length!=1){
                    toastr.warning("请选择一条记录！");
                }else{
                	var data = datas[0]
    	            layer.open({
    	                type: 1 					//Page层类型
    	                ,area: ['510px', ''] 			//宽  高
    	                ,title: '打印标签信息'
    	                ,shade: 0.6 				//遮罩透明度
    	                ,maxmin: true 				//允许全屏最小化
    	                ,anim: 1 					//0-6的动画形式，-1不开启
    	                ,content: $('#dyXXDivID')
    	                ,end: function () {
    	                    var formDiv = document.getElementById('dyXXDivID');
    	                    formDiv.style.display = '';
    	                }
    	                ,success: function(){
    	                	$("#dy_pch").val("")
    	                	$("#dy_bznum").val(data.packing_quantity)
    	                	$("#dy_num").val(1)
    	                }
    	            });
                }
            });
			
          //打印标签
            $("#dytm").click(function(){
            	var checkStatus = table.checkStatus('contenttable')
                var datas = checkStatus.data;
                if(datas.length!=1){
                    toastr.warning("请选择一条记录详情！");
                }else{
                	layer.closeAll();
               	  	var num = $.trim($("#dy_num").val());
               	  	var dy_bznum = $.trim($("#dy_bznum").val());
               	  	var dy_pch = $.trim($("#dy_pch").val());
               		var re = /^[1-9]\d*$/;
               	  	if(!re.test(num) || !re.test(dy_bznum)){
               	  		toastr.warning("请输入正整数！");
   	                }else{
   	            	  	var hight = '600px'
   	            	  	/* if(num > 2){
   		            	  	hight = '90%'
   	            	  	} */
   	            	  	var data = datas[0]
   	    	            layer.open({
   	    	                type: 1 					//Page层类型
   	    	                ,area: ['1200px', hight] 			//宽  高
   	    	                ,title: '打印标签'
   	    	                ,shade: 0.6 				//遮罩透明度
   	    	                ,maxmin: true 				//允许全屏最小化
   	    	                ,anim: 1 					//0-6的动画形式，-1不开启
   	    	                ,content: $('#dytmDivID')
   	    	                ,end: function () {
   	    	                    var formDiv = document.getElementById('dytmDivID');
   	    	                    formDiv.style.display = '';
   	    	                }
   	    	                ,success: function(){
   	    	                	var materiel_name = data.materiel_name;
   	    	                	var materiel_num = data.materiel_num;
   	    	                	var packing_quantity = data.packing_quantity;
   	    	                	var time = curentTime();
   	    	                	var pici = dy_pch;
   	    	                	var bznum = dy_bznum;
   	    	                	var customer_name = data.customer_name;
   	    	                	var tm = "";
   	    	                	var tmwmimg = '<%=request.getContextPath()%>/';
   	    	                	var tm2wmimg = "";
   	    	                	var tm1wmimg = "";
   	    	                	var code = new Array();
   	    	                	$.ajax({
   	                                type:'post',
   	                                data:'id='+data.id+"&num="+num,
   	                                url:'${pageContext.request.contextPath}/label/dyByMid.do',
   	                                dataType: 'JSON',
   	                                async: false,
   	                                success:function (data) {
   	                                	code = data.code;
   	                                }
   	                            });
   	    	                	
       	                		var html = '';
   	    	                	for (var i = 0; i < num; i++) {
   	    	                		tm = code[i][0];
                                   	tm2wmimg = tmwmimg+code[i][1];
                                   	tm1wmimg = tmwmimg+code[i][2];
                                   	
                                  //分页
                                    if(i == 0){
        	                            html += '<div class="A4">'
                                    /* }else{
        	                            if(i%6 == 0){
        		                            html += '</div>'
        		                            html += '<div class="A4">'
        	                            } */
                                    }
                                   	
                                   	
   	    	                		html += '<table class="tmdyTable" style="border: 2px solid #222;width: 540px;height: 260px;margin:0 auto;margin-bottom: 20px;display:inline-table;/*display:inline-block; float:left;*/margin-left: 48px;">'
   	    	                			html += '<tr>'
   	    	                				html += '<td width="100" align="center">'
   	    	                					html += '零件名称：'
   	    	                				html += '</td>'
   	    	                				html += '<td width="220">'
   	    	                				html += materiel_name
   	    	                				html += '</td>'
   	    	                				html += '<td rowspan="8" width="200">'
   	    	                					html += '<img src="'+tm2wmimg+'" width="100%" height="100%"/>'
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '零件号：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += materiel_num
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '供应商：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += customer_name
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '包装信息：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += packing_quantity
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '包装数量：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += bznum
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '日期时间：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += time
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '批次信息：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += pici
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td align="center">'
   	    	                					html += '条码信息：'
   	    	                				html += '</td>'
   	    	                				html += '<td>'
   	    	                					html += tm
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr>'
   	    	                				html += '<td colspan="3">'
   	    	                					html += '<img src="'+tm1wmimg+'" width="100%" height="50px" id="dpddremark_dy"/>'
   	    	                				html += '</td>'
   	    	                			html += '</tr>'
   	    	                			html += '<tr style="height:5px"></tr>'
   	    	                		html += '</table>'
   								}
   	    	                	
   	    	               		//分页
   	    	                	html += '</div>'
   	    	                	
   	    	                	var tmdyDiv = document.getElementById("tmdyDiv");
       	                		tmdyDiv.innerHTML= html;
   	    	                }
   	    	            });
   	                } 
                }
            });
            $("#dayingTmdyTable").click(function(){
            	//layer.closeAll();
    		    var obj =document.getElementById('tmdyDiv')
    			var css='<style>'
    				+".A4 {page-break-before: auto;page-break-after: always;}"
                    +".tmdyTable td{border:1px solid #00000080;}"
                    +'.tmdyTable td input{width:100%;height:100%;border:0px;padding:0px;margin:0px;}'
                    +'.tmdyTable{border:1px solid #F00;border-collapse:collapse;}'
                    +'.tmdyTable td{border:1px solid #222;}'
                    +'</style>'; 
    			var docStr =  css+ obj.innerHTML;
    			var newWindow=window.open("打印窗口","_blank");//打印窗口要换成页面的url
    			newWindow.document.write(docStr);
    		    newWindow.document.close();
    		    newWindow.print(); 
    		    //newWindow.close(); 
    		    printpage(docStr)
    		});
          	//获取当前时间
            function curentTime() {
                var now=new Date();
                var year = now.getFullYear();       //年
                var month = now.getMonth() + 1;     //月
                var day = now.getDate();            //日
                var time=year+"-"+add0(month)+"-"+add0(day);
                return time;
            }
            
            function add0(m){
                return m<10?'0'+m:m
            }
        });
        
        toastr.options.positionClass = 'toast-top-center'; //提示框的位置设置为中上
        
        //日期格式转换
        function dateToStr(date) {
            var time = new Date(date.time);
            var y = time.getFullYear();
            var M = time.getMonth() + 1;
            M = M < 10 ? ("0" + M) : M;
            var d = time.getDate();
            d = d < 10 ? ("0" + d) : d;
            var str = y + "-" + M + "-" + d;
            return str;
        }
        function qx(){

            location.reload();
        }
        function uploadBtn() {
            var fileName = $("#file").val();
            var formData = new FormData($("#importForm")[0]);

            $.ajax({
                url: '${pageContext.request.contextPath}/materiel/uplodeFile.do' ,
                type: 'post',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data == 1) {
                        toastr.success("文件导入成功！");
                        setTimeout(function() {//使用setTimeout()方法设定定时2000毫秒
                            location.reload();
                        },1000);
                    }else if(data == -1){
                        toastr.warning("导入的数据不符合导入条件！");
                    }else{
                        toastr.warning("文件格式异常，文件导入失败！");
                    }
                },
                error: function (data) {
                    toastr.error("文件格式异常，文件导入失败！");
                }
            });

        }
	</script>
</body>
</html>