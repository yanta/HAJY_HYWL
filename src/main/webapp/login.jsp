<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>北京恒安系统登录</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
	<jsp:include page="${pageContext.request.contextPath }/resource_path.jsp" flush="true"/>
	<style>
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
    .code {
        width: 400px;
        margin: 0 auto;
    }
    .input-val {
        width: 295px;
        background: #ffffff;
        height: 2.8rem;
        padding: 0 2%;
        border-radius: 5px;
        border: none;
        border: 1px solid rgba(0,0,0,.2);
        font-size: 1.0625rem;
    }
    #canvas {
        float: right;
        display: inline-block;
        border: 1px solid #ccc;
        border-radius: 5px;
        cursor: pointer;
    }
  
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
  </style>

	
</head>
<body class="login-bg">
    
    <div class="login layui-anim layui-anim-up">
        <div class="message">北京恒安系统登录</div>
        <div id="darkbannerwrap">
        </div>
        <from class="layui-form" action="" method="post">
            <input name="userAccount" id="accountName"  placeholder="用户名"  type="text" lay-verify="accountName" class="layui-input" >
            <hr class="hr15">
            <input name="password" id="password" lay-verify="password" placeholder="密码"  type="password" class="layui-input">
            <hr class="hr15">
            
           
            <input type="text" value="" placeholder="请输入验证码（不区分大小写）"style="width:230px"  class="input-val">
            <canvas id="canvas" width="100" height="48"></canvas>
        	
        	
            <hr class="hr15">
            <input lay-submit lay-filter="addform" type="submit" value="登录" <%--lay-submit lay-filter="login" --%>style="width:100%;" >
            <div id="hhang" style="width: 100%; height: 30px"></div>
            <input type="button" value="注册" <%--lay-submit lay-filter="login" --%>style="width:100%;" onclick="toregister()">
            <hr class="hr20" >
            <div class='login_fields__submit' style="padding-top: 35px;">
            	<h4 id="message" style="color: red;text-align:center;"></h4>
		    </div>
        </from>

    </div>
    
    <script>
    var show_num = [];
    $(function(){
       
        draw(show_num);
 
        $("#canvas").on('click',function(){
            draw(show_num);
        })
      
    })
    var mess = '${message }';
	if(mess != "" && mess != null){
		$("#message").html(mess);
	}
    
    layui.use(['form'], function(){
    	var form = layui.form;
    	 form.verify({
    		 accountName:function (value,item){
                 if(""==value.replace(/\s+/g,"")){
                     return '账号不能为空';
                 }
             },
             password:function(value,item){
                 if(""==value.replace(/\s+/g,"")){
                     return '密码不能为空';
                 }
             }
         });
    	 
    	 form.on('submit(addform)', function (data) {
    		 var val = $(".input-val").val().toLowerCase();
             var num = show_num.join("");
             if(val==''){
                 layer.msg('请输入验证码！');
             }else if(val == num){
            	 var i = 0;
        		 $.ajax({
       	                type: "post",
       	                url: "${pageContext.request.contextPath }/login/pclogin.do",
       	                data: {accountName:$("#accountName").val(), password:$("#password").val()},
       	                dataType: "json",
       	                success: function(data){
       	                	console.log(data)
    	                	if(data.data) {
    	                		window.location.href = "${pageContext.request.contextPath }";
    	                	}else {
    	                       layer.msg(data.message);
    	                   	}
       	                }
        	      });

  
             }else{
                 layer.msg('验证码错误！请重新输入！');
                 $(".input-val").val('');
                 draw(show_num);
             }
    	
    	 });
    	
    });
    
       
        function toregister() {
            window.location.href = "${pageContext.request.contextPath }/register.jsp"
        }
       
        
        
        function draw(show_num) {
            var canvas_width=$('#canvas').width();
            var canvas_height=$('#canvas').height();
            var canvas = document.getElementById("canvas");//获取到canvas的对象，演员
            var context = canvas.getContext("2d");//获取到canvas画图的环境，演员表演的舞台
            canvas.width = canvas_width;
            canvas.height = canvas_height;
            var sCode = "A,B,C,E,F,G,H,J,K,L,M,N,P,Q,R,S,T,W,X,Y,Z,1,2,3,4,5,6,7,8,9,0";
            var aCode = sCode.split(",");
            var aLength = aCode.length;//获取到数组的长度
            
            for (var i = 0; i <= 3; i++) {
                var j = Math.floor(Math.random() * aLength);//获取到随机的索引值
                var deg = Math.random() * 30 * Math.PI / 180;//产生0~30之间的随机弧度
                var txt = aCode[j];//得到随机的一个内容
                show_num[i] = txt.toLowerCase();
                var x = 10 + i * 20;//文字在canvas上的x坐标
                var y = 20 + Math.random() * 8;//文字在canvas上的y坐标
                context.font = "bold 23px 微软雅黑";
     
                context.translate(x, y);
                context.rotate(deg);
     
                context.fillStyle = randomColor();
                context.fillText(txt, 0, 0);
     
                context.rotate(-deg);
                context.translate(-x, -y);
            }
            for (var i = 0; i <= 5; i++) { //验证码上显示线条
                context.strokeStyle = randomColor();
                context.beginPath();
                context.moveTo(Math.random() * canvas_width, Math.random() * canvas_height);
                context.lineTo(Math.random() * canvas_width, Math.random() * canvas_height);
                context.stroke();
            }
            for (var i = 0; i <= 30; i++) { //验证码上显示小点
                context.strokeStyle = randomColor();
                context.beginPath();
                var x = Math.random() * canvas_width;
                var y = Math.random() * canvas_height;
                context.moveTo(x, y);
                context.lineTo(x + 1, y + 1);
                context.stroke();
            }
        }
     
        function randomColor() {//得到随机的颜色值
            var r = Math.floor(Math.random() * 256);
            var g = Math.floor(Math.random() * 256);
            var b = Math.floor(Math.random() * 256);
            return "rgb(" + r + "," + g + "," + b + ")";
        }
    </script>
</body>
</html>