package com.sdry.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.lz.Customer;
import com.sdry.service.tdd.OthersService;

/**
 * 
 * @ClassName StockOutController
 * @Description 入库单跳转菜单
 * @Author lz
 * @Date 2019年3月5日 10:39:13
 * @Version 1.0
 */
@Controller
public class Menu {
	@RequestMapping("/bb1")
	public String bb1(Model model){
		return "/bb/1";
	}
	@RequestMapping("/bb2")
	public String bb2(Model model){
		return "/bb/2";
	}
	@RequestMapping("/bb3")
	public String bb3(Model model){
		return "/bb/3";
	}
	@RequestMapping("/bb4")
	public String bb4(Model model){
		return "/bb/4";
	}
	@RequestMapping("/bb5")
	public String bb5(Model model){
		return "/bb/5";
	}
	@RequestMapping("/bb6")
	public String bb6(Model model){
		return "/bb/6";
	}
	@RequestMapping("/bb7")
	public String bb7(Model model){
		return "/bb/7";
	}
	@RequestMapping("/bb8")
	public String bb8(Model model){
		return "/bb/8";
	}
	@RequestMapping("/bb9")
	public String bb9(Model model){
		return "/bb/9";
	}
}
