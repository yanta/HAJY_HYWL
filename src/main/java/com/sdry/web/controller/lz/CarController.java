package com.sdry.web.controller.lz;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.llm.BarCode;
import com.sdry.model.lz.Car;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.service.llm.CancellingStockService;
import com.sdry.service.lz.CarService;
import com.sdry.service.lz.InventoryService;
import com.sdry.service.lz.MaterielService;
import com.sdry.utils.ResponseUtil;

/**
 * @ClassName CarCOntroller
 * @Description 车辆信息
 * @Author lz
 * @Date 2019年4月16日 10:02:27
 * @Version 1.0
 */
@Controller
@RequestMapping("/car")
public class CarController {
    public static final Integer AFFECT_ROW = 0;

    @Resource
    CarService carService;
    @Resource
    ReceiveDetailService receiveDetailService;

    /**
     * 车辆新增
     * @param response
     * @param car
     */
    @RequestMapping("/addCar")
    public void addCar(HttpServletResponse response, Car car){
        Long affectRow = carService.addCar(car);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteCarById")
    public void deleteCarById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = carService.deleteCarById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑车辆表
     * @param response
     */
    @RequestMapping("/updateCar")
    public void updateCar(HttpServletResponse response, Car Car) {
        Long affectRow = carService.updateCar(Car);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询车辆表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryCarCriteria")
    public void queryCarCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Car> CarList = carService.queryCarCriteria(criteria);
        int count = carService.countCarCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(CarList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Resource
	private CancellingStockService cancellingStockService;
    @Resource
	private InventoryService inventoryService;
    @Resource MaterielService materielService;
    /**
     * 根据条码查询该条码是否存在
     * @param response
     * @param beforeCode 原条码
     * @param mcode 产品码 SAP/QAD
     * @param location 库位
     */
    @RequestMapping("/queryCodeExist")
    public void queryCodeExist(HttpServletResponse response, String beforeCode,String mcode,String location){
    	int flg = 0;
    	//根据条码查询该条码是否存在
    	CodeMark codeMark = carService.queryCodeExist(beforeCode);
        if (codeMark != null) {
        	CodeMark code = new CodeMark();
        	code.setCode(beforeCode);
        	//库位
        	BarCode bar = cancellingStockService.getLocationByCode(code);
        	//根据条码找到物料ID和批次
        	ReceiveDetailQuality byCode = inventoryService.queryReceiveByCode(beforeCode);
        	if(bar != null && byCode != null && location.equals(bar.getRemark02())){
        		//根据id查物料
        		Materiel materiel = materielService.queryMaterielById(byCode.getMid());
        		if(materiel != null && mcode.equals(materiel.getMateriel_num())){
        			flg = 1;
        		}
        	}
        } 
        try {
        	ResponseUtil.write(response, flg);
        } catch (Exception e) {
        	e.printStackTrace();
        }
    }
    
    
    /**
     * 修改条码
     * @param response
     * @param beforeCode 原条码
     * @param newCode    新条码
     */
    @RequestMapping("/editCode")
    public void editCode(HttpServletResponse response, String beforeCode, String newCode){
    	Long affact = 0L;
    	int aff = 0;
    	int result = 0;
    	//校验新条码是否已存在
    	int count = carService.checkNewCode(newCode);
    	
    	if(count > 0) {
    		result = -1;
    	}else {
        	//1.修改lz_codeMark表中的条码
            affact = carService.editCode(beforeCode, newCode);
            //根据原条码查询绑定托盘的信息
            ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = receiveDetailService.queryMaterielTrayByCode(beforeCode);
            //原条码（借用一下字段）
            zcMaterielAndTrayEntity.setSrcRegion(beforeCode);
            //新条码
            zcMaterielAndTrayEntity.setTargetRegion(newCode);
            
            //2.修改zc_materiel_tray表中的条码
            aff = carService.updateCodeByid(zcMaterielAndTrayEntity);
            
            if (affact > 0 && aff > 0) {
            	result = 1;
            } else {
            	result = 0;
            }
    	}
        
        try {
			ResponseUtil.write(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
