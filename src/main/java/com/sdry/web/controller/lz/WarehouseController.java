package com.sdry.web.controller.lz;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.WarehouseService;
import com.sdry.utils.QRCodeUtil;
import com.sdry.utils.ResponseUtil;
import com.sdry.utils.RootPath;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * @ClassName WarehouseController
 * @Description 仓库信息
 * @Author lz
 * @Date 2019年4月9日 10:12:19
 * @Version 1.0
 */
@Controller
@RequestMapping("/warehouse")
public class WarehouseController {
    public static final Integer AFFECT_ROW = 0;
    @Resource
    WarehouseService warehouseService;

    @RequestMapping("/addWarehouse")
    public void addWarehouse(HttpServletResponse response, Warehouse warehouse,HttpSession httpSession){
    	ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
    	warehouse.setCreate_person(su.getId());
        Long affectRow = warehouseService.addWarehouse(warehouse);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteWarehouseById")
    public void deleteWarehouseById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0){
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = warehouseService.deleteWarehouseById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑仓库表
     * @param response
     */
    @RequestMapping("/updateWarehouse")
    public void updateWarehouseById(HttpServletResponse response, Warehouse warehouse){
        Long affectRow = warehouseService.updateWarehouse(warehouse);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询仓库表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryWarehouseCriteria")
    public void queryWarehouseCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Warehouse> warehouseList = warehouseService.queryWarehouseCriteria(criteria);
        int count = warehouseService.countWarehouseCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(warehouseList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*——————————————————库区信息————————————————————*/
    @RequestMapping("/addWarehouseRegion")
    public void addWarehouseRegion(HttpServletResponse response, WarehouseRegion warehouseRegion){
        Long affectRow = warehouseService.addWarehouseRegion(warehouseRegion);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteWarehouseRegionById")
    public void deleteWarehouseRegionById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0){
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = warehouseService.deleteWarehouseRegionById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑仓库表
     * @param response
     */
    @RequestMapping("/updateWarehouseRegion")
    public void updateWarehouseRegion(HttpServletResponse response, WarehouseRegion warehouseRegion){
        Long affectRow = warehouseService.updateWarehouseRegion(warehouseRegion);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询库区表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryWarehouseRegionCriteria")
    public void queryWarehouseRegionCriteria(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
        if(zcGeneralQueryEntity.getStrWhere() == null){
        	zcGeneralQueryEntity.setStrWhere("");
        }
        List<WarehouseRegion> warehouseRegionList = warehouseService.queryWarehouseRegionCriteria(zcGeneralQueryEntity);
        int count = warehouseService.countWarehouseRegionCriteria(zcGeneralQueryEntity);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(warehouseRegionList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*——————————————————库位信息————————————————————*/
    @RequestMapping("/addWarehouseRegionLocation")
    public void addWarehouseRegionLocation(HttpServletResponse response, WarehouseRegionLocation warehouseRegionLocation){
        /*================================根据库位编号生成二维码================================*/
        String dir = RootPath.getRootPath("LOCATION_RCODE_PATH");
        String rcodeName = "location_" + System.currentTimeMillis() + ".png";
        String logoImgPath = dir + rcodeName;
        warehouseRegionLocation.setRcode(rcodeName);
        File file = new File(logoImgPath);
        Long affectRow = warehouseService.addWarehouseRegionLocation(warehouseRegionLocation);
        try {
            QRCodeUtil.encode(warehouseRegionLocation.getLocation_num(), null, new FileOutputStream(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteWarehouseRegionLocationById")
    public void deleteWarehouseRegionLocationById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0){
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = warehouseService.deleteWarehouseRegionLocationById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑仓库表
     * @param response
     */
    @RequestMapping("/updateWarehouseRegionLocation")
    public void updateWarehouseRegionLocation(HttpServletResponse response, WarehouseRegionLocation warehouseRegionLocation){
        if("可用".equals(warehouseRegionLocation.getLocation_status())){
            warehouseRegionLocation.setLocation_status(0);
        } else {
            warehouseRegionLocation.setLocation_status(1);
        }
        /*================================根据库位编号修改生成二维码================================*/
        String dir = RootPath.getRootPath("LOCATION_RCODE_PATH");
        String rcodeName = "location_" + System.currentTimeMillis() + ".png";
        String logoImgPath = dir + rcodeName;
        warehouseRegionLocation.setRcode(rcodeName);
        File file = new File(logoImgPath);
        Long affectRow = warehouseService.updateWarehouseRegionLocation(warehouseRegionLocation);
        try {
            QRCodeUtil.encode(warehouseRegionLocation.getLocation_num(), null, new FileOutputStream(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询库区表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryWarehouseRegionLocationCriteria")
    public void queryWarehouseRegionLocationCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<WarehouseRegionLocation> warehouseRegionList = warehouseService.queryWarehouseRegionLocationCriteria(criteria);
        int count = warehouseService.countWarehouseRegionLocationCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(warehouseRegionList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
	 * 下拉框查找所有仓库
     * @throws Exception 
	 */
	@RequestMapping("/queryAllWarehouse")
	public void queryAllWarehouse(HttpServletResponse response) throws Exception{
		List<Warehouse> warehouseList = warehouseService.queryAllWarehouse();
		JSONArray jsonArray = JSONArray.fromObject(warehouseList);
		if (warehouseList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
	
	/**
	 * 下拉框查找所有库区
     * @throws Exception 
	 */
	@RequestMapping("/queryAllWarehouseRegion")
	public void queryAllWarehouseRegion(HttpServletResponse response) throws Exception{
		List<WarehouseRegion> warehouseRegionList = warehouseService.queryAllWarehouseRegion();
		JSONArray jsonArray = JSONArray.fromObject(warehouseRegionList);
		if (warehouseRegionList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
}
