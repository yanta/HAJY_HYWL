package com.sdry.web.controller.lz;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdry.model.lz.Contacts;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WaitNumAndTotalNum;
import com.sdry.model.lz.WaitSendArea;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.service.lz.CustomerService;
import com.sdry.utils.ResponseUtil;

/**
 * @ClassName CustomerController
 * @Description 客户信息
 * @Author lz
 * @Date 2019年4月16日 09:21:18
 * @Version 1.0
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {
    public static final Integer AFFECT_ROW = 0;
    @Resource
    CustomerService customerService;
    @Resource 
    ReceiveService ReceiveService;
    /**
     * 客户新增
     * @param response
     * @param customer
     */
    @RequestMapping("/addCustomer")
    public void addCustomer(HttpServletResponse response, Customer customer){
        Long affectRow = customerService.addCustomer(customer);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteCustomerById")
    public void deleteCustomerById(HttpServletResponse response, @RequestParam("idArr[]")long[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = customerService.deleteCustomerById(idArr[i]);
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑客户表
     * @param response
     */
    @RequestMapping("/updateCustomer")
    public void updateCustomer(HttpServletResponse response, Customer Customer) {
        Long affectRow = customerService.updateCustomer(Customer);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询客户表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryCustomerCriteria")
    public void queryCustomerCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Customer> customerList = customerService.queryCustomerCriteria(criteria);
        int count = customerService.countCustomerCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(customerList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 条件查询所有供应商
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryCustomerCriteriaByType")
    public void queryCustomerCriteriaByType(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Customer> customerList = customerService.queryCustomerCriteriaByType(criteria);
        int count = customerService.countCustomerCriteriaByType(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(customerList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 联系人新增
     * @param response
     * @param customer
     */
    @RequestMapping("/addContacts")
    public void addContacts(HttpServletResponse response, Contacts contacts){
        Long affectRow = customerService.addContacts(contacts);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteContactsById")
    public void deleteContactsById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = customerService.deleteContactsById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑联系人表
     * @param response
     */
    @RequestMapping("/updateContacts")
    public void updateContacts(HttpServletResponse response, Contacts contacts) {
        Long affectRow = customerService.updateContacts(contacts);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询联系人表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryContactsCriteria")
    public void queryContactsCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Contacts> contactsList = customerService.queryContactsCriteria(criteria);
        int count = customerService.countContactsCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(contactsList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 查找所有联系人
     * @param response
     * @throws Exception
     */
	@RequestMapping("/queryAllContacts")
	public void queryAllContacts(HttpServletResponse response) throws Exception{
		List<Contacts> contactsList = customerService.queryAllContacts();
		JSONArray jsonArray = JSONArray.fromObject(contactsList);
		if (contactsList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
	
	/**
	 * 根据客户id查询库存里的物料
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/queryInventoryInfoByCustomerId")
	public void queryInventoryInfoByCustomerId(HttpServletResponse response, Materiel param) throws Exception{
		int sum = 0;
		//List<Materiel> materielList = customerService.queryInventoryInfoByCustomerId(Long.parseLong(id));
		List<Materiel> materielList = ReceiveService.queryAllMaterielByParam(param);
		/*for (Materiel materiel : materielList) {
			//根据客户id和物料id查询待发货区数量
			List<WaitSendArea> waitSendArea = customerService.queryWaitSendAreaCountByCustomerIdAndMaterielId(materiel.getId());
			if(waitSendArea.size() > 0){
				//将待发货区数量存入
				materiel.setSequence_number(waitSendArea.get(0).getMateriel_num() + "");
			} else {
				materiel.setSequence_number(0 + "");
			}
			//根据物料id查询库存中小箱的数量
			List<ZcInventoryInfoEntity> zcInventoryInfoEntity = customerService.queryInventorySmallNum(materiel.getId());
			if(zcInventoryInfoEntity.size() > 0){
				//将库存中所有小箱的数量存入
				for (int i = 0; i < zcInventoryInfoEntity.size(); i++) {
					sum += zcInventoryInfoEntity.get(i).getSmallNum();
				}
				materiel.setPlacement_type(sum + "");
			} else {
				materiel.setPlacement_type(0 + "");
			}
			sum = 0;
		}*/
		
		JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(materielList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 修改待发货区数量
	 * @param response
	 * @param materiel_id 物料id
	 * @param materiel_num 物料数量
	 */
	@RequestMapping("/updateWaitSendAreaValue")
	public void updateWaitSendAreaValue(HttpServletResponse response, String materiel_id, String materiel_num) {
        customerService.updateWaitSendAreaValue(materiel_id, materiel_num);
        try {
			ResponseUtil.write(response, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * 删除待发货区数量为0的记录
	 * @param response
	 * @param materiel_id 物料id
	 */
	@RequestMapping("/deleteWaitSendAreaValue")
	public void deleteWaitSendAreaValue(HttpServletResponse response, String materiel_id) {
        customerService.deleteWaitSendAreaValue(materiel_id);
        try {
			ResponseUtil.write(response, 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * 出库单详细的行内编辑事件
	 * @param response
	 * @param id 出库单详细内的物料id
	 * @throws Exception
	 */
	@RequestMapping("/queryWaitNumAndTotalNumByMaterialId")
	public void queryWaitNumAndTotalNumByMaterialId(HttpServletResponse response, String materiel_id) throws Exception{
		WaitNumAndTotalNum waitNumAndTotalNum = new WaitNumAndTotalNum();
		int sum = 0;
		//根据物料id查询待发货区数量
		List<WaitSendArea> waitSendArea = customerService.queryWaitSendAreaCountByCustomerIdAndMaterielId(Long.parseLong(materiel_id));
		if(waitSendArea.size() > 0){
			//将待发货区数量存入
			waitNumAndTotalNum.setWait_num(waitSendArea.get(0).getMateriel_num() + "");
		} else {
			waitNumAndTotalNum.setWait_num(0 + "");
		}
		//根据物料id查询库存中小箱的数量
		List<ZcInventoryInfoEntity> zcInventoryInfoEntity = customerService.queryInventorySmallNum(Long.parseLong(materiel_id));
		if(zcInventoryInfoEntity.size() > 0){
			//将库存中所有小箱的数量存入
			for (int i = 0; i < zcInventoryInfoEntity.size(); i++) {
				sum += zcInventoryInfoEntity.get(i).getSmallNum();
			}
			waitNumAndTotalNum.setTotal_num(sum + "");
		} else {
			waitNumAndTotalNum.setTotal_num(0 + "");
		}
		JSONArray jsonArray = JSONArray.fromObject(waitNumAndTotalNum);
		ResponseUtil.write(response, jsonArray);
	}
}
