package com.sdry.web.controller.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.lz.Customer;
import com.sdry.model.lz.InventoryOrder;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.InventoryService;
import com.sdry.service.lz.StockOutOrderService;
import com.sdry.service.zc.ZcBindAndUnbindService;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.service.zc.ZcSystemService;

/**
 * 
 * @ClassName StockOutController
 * @Description 入库单跳转菜单
 * @Author lz
 * @Date 2019年3月5日 10:39:13
 * @Version 1.0
 */
@Controller
@RequestMapping("/stockOutMenu")
public class StockOutMenu {
	@Resource
	InventoryService inventoryService;
	@Resource
	ZcSystemService zcSystemService;
	@Resource
	StockOutOrderService stockOutService;
	@Resource
	private ZcInventoryManagementService zcInventoryManagementService;
	
	//-------------------jyy-start-----------------------
	//物料绑定与解绑业务层接口（为了调用查询所有库区接口）
	@Resource
	private ZcBindAndUnbindService zcBindAndUnbindService;
	//-------------------jyy-end------------------------

	//出库单
	@RequestMapping("/stockOutExport")
	public String stockOutExport(Model model){
		List<Customer> customer_type1 = stockOutService.queryAllCustomerByType1();
		model.addAttribute("customer_type1", customer_type1);
		return "/lz/stock_out_export";
	}
	
	//出库单2(去精简出库单页面)
	@RequestMapping("/stockOutExport2")
	public String stockOutExport2(Model model){
		List<Customer> customer_type1 = stockOutService.queryAllCustomerByType1();
		model.addAttribute("customer_type1", customer_type1);
		return "/lz/stock_out_export_only";
	}
	//紧急出库单
	@RequestMapping("/stockOutExport3")
	public String stockOutExport3(Model model){
		List<Customer> customer_type1 = stockOutService.queryAllCustomerByType1();
		model.addAttribute("customer_type1", customer_type1);
		return "/lz/stock_out_jinji";
	}
	//出库
	@RequestMapping("/stockOut")
	public String stockOut(Model model){
		List<Customer> customer_type1 = stockOutService.queryAllCustomerByType1();
		model.addAttribute("customer_type1", customer_type1);
		return "/lz/stock_out";
	}

	//异常调整
	@RequestMapping("/abnormalAdjustment")
	public String abnormalAdjustment(Model model){
		List<OutStockOrder> allOutStockOrder = stockOutService.queryAllOutStockOrder();
		model.addAttribute("allOutStockOrder", allOutStockOrder);
		List<Customer> allcustomer = stockOutService.queryAllCustomerByType();
		model.addAttribute("allcustomers", allcustomer);
		return "/lz/abnormal_adjustment";
	}
	
	//装车处理
	@RequestMapping("/loadingProcess")
	public String loadingProcess(){
		return "/lz/loading_process";
	}
	
	//盘点管理
	@RequestMapping("/inventoryManagement")
	public String inventoryManagement(Model model){
		List<ZcSysUserEntity> userList = inventoryService.queryAllUser();
		model.addAttribute("userList", userList);
		List<Warehouse> warehouseList = inventoryService.queryAllWarehouseAndroid();
		model.addAttribute("warehouseList", warehouseList);
		List<Customer> allcustomer = stockOutService.queryAllCustomerByType();
		model.addAttribute("allcustomer", allcustomer);
		
		//----------------jyy-stat-----------------
		//查询所有库区
		List<WarehouseRegion> allRegion = zcBindAndUnbindService.selectAllRegion();
		model.addAttribute("allRegion",allRegion);
		//----------------jyy-end------------------
		
		//查询所有供应商
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<Customer> allCustomerList = zcInventoryManagementService.selectAllCustomerList(zcGeneralQueryEntity);
		model.addAttribute("allCustomerList",allCustomerList);
		return "/lz/inventory_management";
	}
		
	
	//损溢管理
	@RequestMapping("/lossSpilloverManagement")
	public String lossSpilloverManagement(Model model){
		List<InventoryOrder> list = inventoryService.queryAllInventoryOrder1();
		model.addAttribute("allInventory", list);
		return "/lz/loss_spillover_management";
	}

	//地区信息
	@RequestMapping("/region")
	public String region(){
		return "/lz/region";
	}

	//仓库信息
	@RequestMapping("/warehouse")
	public ModelAndView warehouse(){
		ModelAndView mav = new ModelAndView();
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<ZcSysUserEntity> allUser = zcSystemService.selectAllSysUserList(zcGeneralQueryEntity);
		mav.addObject("allUser",allUser);
		mav.setViewName("/lz/warehouse");
		return mav;
	}

	//库区信息
	@RequestMapping("/warehouseRegion")
	public String warehouseRegion(){
		return "/lz/warehouse_region";
	}

	//库位信息
	@RequestMapping("/warehouseRegionLocation")
	public String warehouseRegionLocation(){
		return "/lz/warehouse_region_location";
	}

	//部门信息
	@RequestMapping("/dept")
	public ModelAndView dept(){
		ModelAndView mav = new ModelAndView();
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<ZcSysUserEntity> allUser = zcSystemService.selectAllSysUserList(zcGeneralQueryEntity);
		mav.addObject("allUser",allUser);
		mav.setViewName("/lz/dept");
		return mav;
	}

	//员工信息
	@RequestMapping("/staff")
	public String staff(){
		return "/lz/staff";
	}
	
	//车辆信息
	@RequestMapping("/car")
	public ModelAndView car(){
		ModelAndView mav = new ModelAndView();
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<ZcSysUserEntity> allUser = zcSystemService.selectAllSysUserList(zcGeneralQueryEntity);
		mav.addObject("allUser",allUser);
		mav.setViewName("/lz/car");
		return mav;
	}

	//岗位信息
	@RequestMapping("/post")
	public String post(){
		return "/lz/post";
	}

	//客户信息
	@RequestMapping("/customer")
	public String customer(){
		return "/lz/customer";
	}

	//客户信息
	@RequestMapping("/contacts")
	public String contacts(){
		return "/lz/contacts";
	}
	
	//物料信息
	@RequestMapping("/materiel")
	public ModelAndView materiel(){
		ModelAndView mav = new ModelAndView();
		List<Customer> customerList = stockOutService.queryAllCustomer();
		mav.setViewName("/lz/materiel");
		mav.addObject("customerList",customerList);
		return mav;
	}

	//物料明细
	@RequestMapping("/materiel_detail")
	public String materiel_detail(){
		return "/lz/materiel_detail";
	}

	//容器信息
	@RequestMapping("/container")
	public String container(){
		return "/lz/container";
	}

}
