package com.sdry.web.controller.lz;

import com.sdry.model.lz.Department;
import com.sdry.model.lz.Post;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.PostService;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import java.util.List;

/**
 * @ClassName PostController
 * @Description 岗位信息
 * @Author lz
 * @Date 2019年4月16日 09:43:28
 * @Version 1.0
 */
@Controller
@RequestMapping("/post")
public class PostController {

    public static final Integer AFFECT_ROW = 0;

    @Resource
    PostService postService;

    /**
     * 岗位新增
     * @param response
     * @param post
     */
    @RequestMapping("/addPost")
    public void addPost(HttpServletResponse response, Post post){
        Long affectRow = postService.addPost(post);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deletePostById")
    public void deletePostById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = postService.deletePostById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑岗位表
     * @param response
     */
    @RequestMapping("/updatePost")
    public void updatePost(HttpServletResponse response, Post post) {
        Long affectRow = postService.updatePost(post);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询岗位表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryPostCriteria")
    public void queryPostCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Post> postList = postService.queryPostCriteria(criteria);
        int count = postService.countPostCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(postList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * 查找所有部门
     * @param response
     * @throws Exception
     */
	@RequestMapping("/queryAllDept")
	public void queryAllDept(HttpServletResponse response) throws Exception{
		List<Department> departmentList = postService.queryAllDept();
		JSONArray jsonArray = JSONArray.fromObject(departmentList);
		if (departmentList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
	
	/**
     * 查找所有岗位
     * @param response
     * @throws Exception
     */
	@RequestMapping("/queryAllPost")
	public void queryAllPost(HttpServletResponse response) throws Exception{
		List<Post> postList = postService.queryAllPost();
		JSONArray jsonArray = JSONArray.fromObject(postList);
		if (postList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
}
