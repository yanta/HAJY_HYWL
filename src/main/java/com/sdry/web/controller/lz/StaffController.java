package com.sdry.web.controller.lz;

import com.sdry.model.lz.Staff;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.StaffService;
import com.sdry.utils.ResponseUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName StaffController
 * @Description 员工信息
 * @Author lz
 * @Date 2019年4月16日 10:44:12
 * @Version 1.0
 */
@Controller
@RequestMapping("/staff")
public class StaffController {
    
    public static final Integer AFFECT_ROW = 0;

    @Resource
    StaffService staffService;

    /**
     * 员工新增
     * @param response
     * @param staff
     */
    @RequestMapping("/addStaff")
    public void addStaff(HttpServletResponse response, Staff staff){
        Long affectRow = staffService.addStaff(staff);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteStaffById")
    public void deleteStaffById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = staffService.deleteStaffById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑员工表
     * @param response
     */
    @RequestMapping("/updateStaff")
    public void updateStaff(HttpServletResponse response, Staff staff) {
        Long affectRow = staffService.updateStaff(staff);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询员工表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryStaffCriteria")
    public void queryStaffCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Staff> StaffList = staffService.queryStaffCriteria(criteria);
        int count = staffService.countStaffCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(StaffList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
