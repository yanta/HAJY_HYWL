package com.sdry.web.controller.lz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdry.model.barCodeOperation.BarCodeOperation;
import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.lz.InventoryDetail;
import com.sdry.model.lz.InventoryDetailCode;
import com.sdry.model.lz.InventoryOrder;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;
import com.sdry.service.barCodeOperation.BarCodeOperationService;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.service.lz.CarService;
import com.sdry.service.lz.InventoryService;
import com.sdry.service.lz.MaterielService;
import com.sdry.service.zc.ZcBindAndUnbindService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

/**
 * @ClassName InventoryController
 * @Description 盘点管理
 * @Author lz
 * @Date 2019年3月26日 14:17:05
 * @Version 1.0
 */
@Controller
@RequestMapping("/inventory")
public class InventoryController {

	public static final Integer AFFECT_ROW = 0;
	
	@Resource
	/**盘点业务层接口*/
	InventoryService inventoryService;
	@Resource
	/**物料业务层接口*/
	MaterielService materielService;
	@Resource
	BarCodeOperationService barCodeOperationService;
	@Resource
	ZcBindAndUnbindService zcBindAndUnbindService;
	@Resource
	ReceiveDetailService receiveDetailService;
	@Resource
	CarService carService;
	
	/**
	 * 盘点单确认
	 * @author jyy
	 * @param inventoryOrder 盘点单实体
	 * @param response 响应
	 * @param session 会话信息
	 */
	@RequestMapping("/confirm")
	public void confirm(InventoryOrder inventoryOrder, HttpServletResponse response, HttpSession session) {
		
		int aff = 0;
		String addStr = "";
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//确认时间
		inventoryOrder.setConfirmed_time(sdf.format(new Date()));
		//盘点单状态：3：已确认
		inventoryOrder.setTab("3");
		//获取session信息
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		//确认人id
		inventoryOrder.setConfirmed_man(user.getId());
		//一：修改盘点单的确认人、时间、状态等信息
		aff = inventoryService.update(inventoryOrder);
		
		
		//二 ：把有差异的条码移到损益区
		//1、查询所有有差异的条码
		List<InventoryDetailCode> list = inventoryService.queryCodeByDiff(inventoryOrder.getId());
		//2、把有差异的条码转移到损益区
		for(InventoryDetailCode code : list){
			//（1）.通过条码，在物料托盘绑定表（zc_materiel_tray）中找到该条码所在记录的id
			 //根据原条码查询绑定托盘的信息
            ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = receiveDetailService.queryMaterielTrayByCode(code.getCode());
			//（2）.通过id用  空字符串   替换 条码+“，”
            zcMaterielAndTrayEntity.setSrcRegion(code.getCode()+",");
            zcMaterielAndTrayEntity.setTargetRegion("");
            aff = carService.updateCodeByid(zcMaterielAndTrayEntity);
            
            //(3).生成一个逻辑托盘码
    		String tray_code = System.currentTimeMillis()+"";
    		//（4）.在物料-托盘绑定表中新增一条记录
	    		ZcMaterielAndTrayEntity zcMaterielAndTrayEntity2 = new ZcMaterielAndTrayEntity();
	    		//物料id
	    		zcMaterielAndTrayEntity2.setMid(zcMaterielAndTrayEntity.getMid());
	    		//pici
	    		zcMaterielAndTrayEntity2.setmBatch(zcMaterielAndTrayEntity.getmBatch());
	    		//所有条码
	    		zcMaterielAndTrayEntity2.setMateriel_code(addStr);
	    		//托盘码
	    		zcMaterielAndTrayEntity2.setTray_code(tray_code);
	    		//条码的账面数量
	    		zcMaterielAndTrayEntity2.setmNum(code.getTheory_num());
	    		//绑定人
	    		zcMaterielAndTrayEntity2.setBinding_person(user.getId());
	    		//绑定时间
	    		zcMaterielAndTrayEntity2.setBinding_date(sdf.format(new Date()));
	    		
	    		aff = inventoryService.MaterielTrayInsert(zcMaterielAndTrayEntity);
    		
    		 addStr += tray_code + ",";
		}
		
		//（5）.在托盘-库位绑定表（zc_tray_location）中找是否存在 损溢区“SY”的记录
		ZcTrayAndLocationEntity zcTrayAndLocationEntity = inventoryService.selectTrayLocationByLocationCode();
		if(zcTrayAndLocationEntity != null) {
			//如果有，在此记录的tray_code(托盘码)字段后追加我在（4）生成的逻辑托盘码
			String tray_code = zcTrayAndLocationEntity.getTray_code();
			tray_code = tray_code + addStr;
			aff = inventoryService.updateTrayLocationByID(zcTrayAndLocationEntity);
		}else {
			//如果没有，则新增一条记录
			ZcTrayAndLocationEntity zcTrayAndLocationEntity2 = new ZcTrayAndLocationEntity();
			//托盘码
			zcTrayAndLocationEntity2.setTray_code(addStr);
			//库区编码
			zcTrayAndLocationEntity2.setLocation_code("SY");
			//绑定人
			zcTrayAndLocationEntity2.setBinding_person(user.getId());
    		//绑定时间
			zcTrayAndLocationEntity2.setBinding_date(sdf.format(new Date()));
			aff = inventoryService.trayLocationInsert(zcTrayAndLocationEntity2);
		}
		
		/*Date date = new Date();
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = new ZcMaterielAndTrayEntity();
		String tray_code = System.currentTimeMillis()+"";
		zcMaterielAndTrayEntity.setMateriel_code(addStr);
		zcMaterielAndTrayEntity.setBinding_person(user.getId());
		zcMaterielAndTrayEntity.setBinding_date(sdf.format(date));
		zcMaterielAndTrayEntity.setTargetRegion("损溢区");
		zcMaterielAndTrayEntity.setTray_code(tray_code);
		//zcMaterielAndTrayEntity.setLocation_code(code.getLocation_num());
		zcMaterielAndTrayEntity.setDownType(Integer.parseInt("2"));
		int i = zcBindAndUnbindService.bindAndUp2SpecialArea(zcMaterielAndTrayEntity);*/
		
		try {
			ResponseUtil.write(response, aff);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据盘点详情id查询复盘条码
	 * @author jyy
	 * @param invenId 盘点详情id
	 * @param response
	 */
	@RequestMapping("/queryCodeListByinvenId")
	public void queryCodeListByinvenId(Long invenId, HttpServletResponse response) {
		List<InventoryDetailCode> list = inventoryService.queryCodeListByinvenId(invenId);
		
		JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(list);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", list.size());
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	//--------------------------------------------------------------------------------------------------------------------------
	
	
	/**
	 * 新增盘点单
	 * @param response
	 * @param inventoryOrder
	 */
	@RequestMapping("/addInventoryOrder")
	public void addInventoryOrder(HttpServletResponse response, InventoryOrder inventoryOrder, HttpSession session){
		
		//修改为：在盘点完提交的时候才保存盘点人盘点时间
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//盘点时间
		//inventoryOrder.setInventory_time(sdf.format(new Date()));
		//盘点状态：0未盘点
		inventoryOrder.setTab("0");

		//获取session信息
		//ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		//盘点人id
		//inventoryOrder.setInventory_man(user.getId());
		//添加盘点单
		Long affectRow = inventoryService.addInventoryOrder(inventoryOrder);
		try {
			ResponseUtil.write(response, affectRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增盘点单详细
	 * @param response
	 * @param detail
	 */
	@RequestMapping("/addInventoryOrderDetail")
	public void addInventoryOrderDetail(HttpServletResponse response, InventoryDetail detail){
		detail.setTab(0);
		Long affectRow = inventoryService.addInventoryOrderDetail(detail);
		try {
			ResponseUtil.write(response, affectRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

 	/**
 	 * 根据ID删除该行数据
 	 * @param response
 	 * @param idArr
 	 */
	@RequestMapping("/deleteInventoryOrderById")
	public void deleteInventoryOrderById(HttpSession httpSession,HttpServletResponse response, @RequestParam("idArr[]") String[] idArr) throws Exception{
 		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		if(idArr.length > 0){
			for (int i = 0; i < idArr.length; i++) {
				
				 Map map = new HashMap();
				 map.put("id",Long.parseLong(idArr[i]));
				map.put("cancellation_time", df.format(new Date()));
				map.put("cancellation", userName);
				Integer affact = inventoryService.cacelpd(map);
				ResponseUtil.write(response, affact);
			}
		}
	}
	@RequestMapping("/restor")
	public void delet(HttpSession httpSession,HttpServletResponse response, @RequestParam("idArr[]") String[] idArr) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
	       
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		if(idArr.length > 0){
			for (int i = 0; i < idArr.length; i++) {
				
				 Map map = new HashMap();
				 map.put("id", Long.parseLong(idArr[i]));
					map.put("restorer_time", df.format(new Date()));
					map.put("restorer", userName);
				Integer affact = inventoryService.restorpd(map);
				ResponseUtil.write(response, affact);
				
			}
		}
        
	}
	
	
 	/**
 	 * 根据盘点单详细ID删除订单信息
 	 * @param response
 	 * @param id 订单详细ID
 	 */

 	@RequestMapping("/deleteInventoryOrderDetailById")
	public void deleteInventoryOrderDetailById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
 		if(idArr.length > 0){
 			for (int i = 0; i < idArr.length; i++) {
 	            Long affectRow = inventoryService.deleteInventoryOrderDetailById(Long.parseLong(idArr[i]));
 	            if (affectRow > AFFECT_ROW) {
 	                try {
						ResponseUtil.write(response, affectRow);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	            } else {
 	                try {
						ResponseUtil.write(response, AFFECT_ROW);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	            }
 	        }
 		}
	}
 	/**
 	 * 行内编辑盘点订单表
 	 * @param response
 	 * @param id 订单ID
 	 * @param fieldName 修改的字段名称
 	 * @param fieldValue 修改的字段的值
 	 */
 	@RequestMapping("/updateInventoryOrderById")
	public void updateInventoryOrderById(HttpServletResponse response, String id, String fieldName, String fieldValue){
		Long affectRow = inventoryService.updateInventoryOrderById(id, fieldName, fieldValue);
		if(affectRow > AFFECT_ROW){
			try {
				ResponseUtil.write(response, affectRow);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				ResponseUtil.write(response, "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
 	 * 行内编辑盘点订单表详细
 	 * @param response
 	 * @param id 订单ID
 	 * @param fieldName 修改的字段名称
 	 * @param fieldValue 修改的字段的值
 	 */
 	@RequestMapping("/updateInventoryOrderDetailById")
	public void updateInventoryOrderDetailById(HttpServletResponse response, String mid, String fieldName, String fieldValue,String cid,String detailid){
		//System.out.println(mid+"**********gys"+cid);
 		Long affectRow = inventoryService.updateInventoryOrderDetailById(mid, fieldName, fieldValue,cid,detailid);
		if(affectRow > AFFECT_ROW){
			try {
				ResponseUtil.write(response, affectRow);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			try {
				ResponseUtil.write(response, "");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
 	
	/**
	 * 分页条件查询盘点单
	 * @param response
	 * @param criteria
	 */
	@RequestMapping("/queryInventoryOrderCriteria")
	public void queryInventoryOrderCriteria(HttpServletResponse response, LzQueryCriteria criteria){
		String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        
        List<InventoryOrder> inventoryOrderList = inventoryService.queryInventoryOrderCriteria(criteria);
        int count = inventoryService.countInventoryOrderCriteria(criteria);
        
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(inventoryOrderList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	/**
 	 * 根据订单编号查询订单详细
 	 * @param response
 	 * @param criteria 订单ID
 	 * @throws Exception
 	 */
 	@RequestMapping("/queryInventoryDetailCriteriaById")
	public void queryInventoryDetailCriteriaById(HttpServletResponse response, LzQueryCriteria criteria) throws Exception{
 		String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<InventoryDetail> inventoryDetailList = inventoryService.queryInventoryDetailCriteriaById(criteria);
        int count = inventoryService.countInventoryDetailCriteriaById(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(inventoryDetailList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
 	}
 	
 	/**
	 * 分页条件查询盘点单(Android)
	 * @param response
	 * @param criteria
	 */
	@RequestMapping("/queryInventoryOrderCriteriaAndroid")
	public void queryInventoryOrderCriteriaAndroid(HttpServletResponse response){
        List<InventoryOrder> inventoryOrderList = inventoryService.queryInventoryOrderCriteriaAndroid();
        JSONObject jsonObject = new JSONObject();
        if(inventoryOrderList.size() > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", inventoryOrderList);
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "查询数据为空");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	/**
 	 * 根据盘点单号查询盘点详细(Android)
 	 * @param response
 	 * @param criteria 订单ID
 	 * @throws Exception
 	 */
 	@RequestMapping("/queryInventoryDetailCriteriaByIdAndroid")
	public void queryInventoryDetailCriteriaByIdAndroid(HttpServletResponse response, String order_id){
        List<InventoryDetail> inventoryDetailList = inventoryService.queryInventoryDetailCriteriaByIdAndroid(order_id);
        JSONObject jsonObject = new JSONObject();
        if(inventoryDetailList.size() > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", inventoryDetailList);
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "查询数据为空");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
 	}
 	
 	/**
 	 * 复盘查询
 	 * 根据盘点单号查询盘点详细(Android)
 	 * @param response
 	 * @param criteria 订单ID
 	 * @throws Exception
 	 */
 	@RequestMapping("/queryInventoryDetailCriteriaByIdAndroid2")
	public void queryInventoryDetailCriteriaByIdAndroid2(HttpServletResponse response, String order_id){
        List<InventoryDetail> inventoryDetailList = inventoryService.queryInventoryDetailCriteriaByIdAndroid2(order_id);
        JSONObject jsonObject = new JSONObject();
        if(inventoryDetailList.size() > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", inventoryDetailList);
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "查询数据为空");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
 	}
 	
 	
 	/**
 	 * 修改盘点单详细(Android)--盘点提交接口
 	 * @param response 响应
 	 * @param jsonValue  InventoryDetail 盘点详情实体集合
 	 * @param person 操作人
 	 * @param state 状态->1：初盘，2：复盘
 	 */
 	@RequestMapping("/updateInventoryDetailAndroid")
	public void updateInventoryDetailAndroid(HttpServletResponse response, String jsonValue, String person, String state) {
 		Gson gson = new Gson();
 		String tab = "0";
 		String status = "0";
 		int num = 0;
        List<InventoryDetail> inventoryDetailList = gson.fromJson(jsonValue,
                new TypeToken<List<InventoryDetail>>() {
                }.getType());
 		JSONObject jsonObject = new JSONObject();
 		if(inventoryDetailList.size() > AFFECT_ROW){
        	for (int i = 0; i < inventoryDetailList.size(); i++) {
        		/*获取传来的codeMark
        		List<CodeMark> codeList = inventoryDetailList.get(i).getReceiveCode();
        		int actual_quantity = 0;
        		for (CodeMark codeMark : codeList) {
        			actual_quantity+=codeMark.getNum();
				}
        		inventoryDetailList.get(i).setActual_quantity(Long.parseLong(actual_quantity+""));*/
        		
        		//二级详情账面库存
        		Long theoretical_quantity = inventoryDetailList.get(i).getTheoretical_quantity();
        		//TODO
        		//复盘数量
        		int check_num = inventoryDetailList.get(i).getCheck_num();
        		//复盘差异
        		inventoryDetailList.get(i).setCheck_diff(check_num - Integer.valueOf(theoretical_quantity+"")  );
        		//条码集合
        		List<InventoryDetailCode> inventoryDetailCodes = inventoryDetailList.get(i).getInventoryDetailCode();
        		
        		if("2".equals(state)) {
        			num = 2;
        			//复盘,把条码存入数据库
        			inventoryService.insertCodeInInventory(inventoryDetailCodes, inventoryDetailList.get(i).getOrder_id(), inventoryDetailList.get(i).getId());
        			/*for (InventoryDetailCode inventoryDetailCode : inventoryDetailCodes) {
        				//账面库存
        				int theory_num = inventoryDetailCode.getTheory_num();
        				//复盘数量
        				int reality_num = inventoryDetailCode.getReality_num();
        				//复盘差异
        				inventoryDetailCode.setDiff_num(theory_num - reality_num);
        				inventoryService.insert(inventoryDetailCode);
    				}*/
        			tab = "2";
        		}else {
        			num = 1;
        			//初盘
        			Long actual_quantity = inventoryDetailList.get(i).getActual_quantity();
        			//理论数量
        			//int count =inventoryService.selectKwCount(inventoryDetailList.get(i).getLocation_num());
        			InventoryDetail detail = inventoryService.queryInventoryDetailById2(inventoryDetailList.get(i).getId());
        			Long first = detail.getTheoretical_quantity();
            		inventoryDetailList.get(i).setTheoretical_quantity(first);
            		//出盘差异
            		Long difference = actual_quantity - first;
            		inventoryDetailList.get(i).setDifference_quantity(difference);
            		
            		//如果初盘差异为0，复盘差异也设置为0，解决处盘无差异，但复盘有差异数据的问题
            		if(inventoryDetailList.get(i).getDifference_quantity() == 0) {
            			inventoryDetailList.get(i).setCheck_diff(0);
            		}
            		
            		if(difference != 0){
            			//如果有差异
            			tab = "1";
            			status = "1";
            			inventoryDetailList.get(i).setTab(0);
            		}else{
            			tab = "3";
            			inventoryDetailList.get(i).setTab(1);
            		}
        		}
        		inventoryService.updateInventoryDetailAndroid(inventoryDetailList.get(i));
        		
        		//--------------给历史记录表插入数据--------------
        		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        		String time = format.format(new Date());
        		BarCodeOperation bar = new BarCodeOperation();
    			bar.setOperator(Long.parseLong(person));
    			bar.setMaterielId(inventoryDetailList.get(i).getMateriel_id());
    			bar.setOperatingTime(time);
    			bar.setType("盘点");
    			bar.setResult("成功");
    			for(InventoryDetailCode cm : inventoryDetailList.get(i).getInventoryDetailCode()){
    				ReceiveDetailQuality byCode = inventoryService.queryReceiveByCode(cm.getCode());
    				bar.setBatch(byCode.getPici());
    				bar.setBarCode(cm.getCode());
    				barCodeOperationService.insert(bar);
    			}
        		//--------------给历史记录表插入数据--------------
			}
//    		inventoryService.updateInventoryOrderById(inventoryDetailList.get(i).getOrder_id()+"", "tab", tab);
			InventoryOrder inventoryOrder = new InventoryOrder();
			if(num == 2) {
				//复盘人
				inventoryOrder.setChecked_man(Long.valueOf(person));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				//复盘时间
				inventoryOrder.setChecked_time(sdf.format(new Date()));
			}else {
				//初盘人
				inventoryOrder.setInventory_man(Long.valueOf(person));
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				//初盘时间
				inventoryOrder.setInventory_time(sdf.format(new Date()));
			}
			if("1".equals(status)){
				tab = "1";
			}
			inventoryOrder.setTab(tab);
			inventoryOrder.setId(inventoryDetailList.get(0).getOrder_id());
			inventoryService.update(inventoryOrder);
			try {
				jsonObject.put("status", "1");
	            jsonObject.put("data", "提交成功");
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	try {
				jsonObject.put("status", "0");
	            jsonObject.put("message", "提交失败");
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
 	}
 	
 	/**
	 * 查询所有仓库
     * @throws Exception 
	 */
	@RequestMapping("/queryAllWarehouseAndroid")
	public void queryAllWarehouseAndroid(HttpServletResponse response) throws Exception{
		List<Warehouse> warehouseList = inventoryService.queryAllWarehouseAndroid();
		JSONObject jsonObject = new JSONObject();
        if(warehouseList.size() > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", warehouseList);
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "查询数据为空");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
	
	/**
 	 * 根据盘点单号查询盘点详情
 	 * @param orderId
 	 * @param response
 	 */
	@RequestMapping("/queryInventoryDetailByOrder")
 	public void queryInventoryDetailByOrder(String orderId,HttpServletResponse response) {
		if (!"".equals(orderId)) {
			List<InventoryDetail> inventoryDetailList = inventoryService.queryInventoryDetailByOrder(orderId);
	        JSONArray jsonArray = JSONArray.fromObject(inventoryDetailList);
	        JSONObject jsonObject = new JSONObject();
	        jsonObject.put("code", AFFECT_ROW);
	        jsonObject.put("data", jsonArray);
	        try {
	            ResponseUtil.write(response, jsonObject);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		} else {
			try {
				JSONObject jsonObject = new JSONObject();
		        jsonObject.put("code", AFFECT_ROW);
		        jsonObject.put("data", null);
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	@ResponseBody
	@RequestMapping("/queryMidByOrder")
	public void queryOrder(String orderId,HttpServletResponse response) {
		if (!"".equals(orderId)) {
			List<InventoryDetail> inventoryDetailList = inventoryService.queryMaterielId(Long.parseLong(orderId));
	        JSONArray jsonArray = JSONArray.fromObject(inventoryDetailList);
	       
	        try {
		            ResponseUtil.write(response, jsonArray);
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		}
	}
	/**
	 * 分页查询物料
	 * @Title:listPageMaterielByCustomerIdAndWarehouseId
	 * @param page 
	 * @param warehouseId 仓库ID
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/findAllMateriel")
	public void listPageMaterielByCustomerIdAndWarehouseId(Page page,  Materiel materiel, HttpServletResponse response){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if(materiel.getCustomer_name()!=null&&!"".equals(materiel.getCustomer_name())) {
			map.put("gys", materiel.getCustomer_name());
		}else {
			map.put("gys", "");
		}
		if(materiel.getMateriel_name()!=null&&!"".equals(materiel.getMateriel_name())) {
			map.put("wlmc", materiel.getMateriel_name());
		}else {
			map.put("wlmc", "");
		}
		if(materiel.getMateriel_num()!=null&&!"".equals(materiel.getMateriel_num())) {
			map.put("wlbm", materiel.getMateriel_num());
		}else {
			map.put("wlbm", "");
		}
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(null, jsonConfig);
		int count = 0;
		List<Materiel> list = new ArrayList<Materiel>();

		count = materielService.countAllMC(map);
		list = materielService.queryAllMC(map);

		jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//System.out.println(jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
 	 * 根据盘点详情单号查询盘点详情
 	 * @param id
 	 * @param response
 	 */
	@RequestMapping("/queryInventoryDetailById")
 	public void queryInventoryDetailById(String id,HttpServletResponse response) {
		 InventoryDetail inventoryDetail = inventoryService.queryInventoryDetailById(Long.parseLong(id));
	     JSONArray jsonArray = JSONArray.fromObject(inventoryDetail);
	     JSONObject jsonObject = new JSONObject();
	     jsonObject.put("data", jsonArray);
	        try {
	            ResponseUtil.write(response, jsonObject);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	}
}
