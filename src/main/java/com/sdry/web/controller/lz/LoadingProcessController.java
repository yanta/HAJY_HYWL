package com.sdry.web.controller.lz;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdry.model.lz.LoadingProcess;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.service.lz.LoadingProcessService;
import com.sdry.utils.ResponseUtil;
/**
 * @ClassName LoadingProcessController
 * @Description 装车处理
 * @Author lz
 * @Date 2019年4月25日 11:25:26
 * @Version 1.0
 */
@Controller
@RequestMapping("/loadingProcess")
public class LoadingProcessController {
	public static final Integer AFFECT_ROW = 0;
	@Resource
	LoadingProcessService loadingProcessService;
	
	/**
	 * 新增装车单
	 * @param response
	 * @param LoadingProcess
	 */
	@RequestMapping("/addLoadingProcess")
	public void addLoadingProcess(HttpServletResponse response, LoadingProcess loadingProcess){
		Long affectRow = loadingProcessService.addLoadingProcess(loadingProcess);
		try {
			ResponseUtil.write(response, affectRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
 	 * 根据ID删除该行数据
 	 * @param response
 	 * @param idArr
 	 */
 	@RequestMapping("/deleteLoadingProcessById")
	public void deleteLoadingProcessById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
 		if(idArr.length > 0){
 			for (int i = 0; i < idArr.length; i++) {
 	            Long affectRow = loadingProcessService.deleteLoadingProcessById(Long.parseLong(idArr[i]));
 	            if (affectRow > AFFECT_ROW) {
 	                try {
						ResponseUtil.write(response, affectRow);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	            } else {
 	                try {
						ResponseUtil.write(response, AFFECT_ROW);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	            }
 	        }
 		}
	}
	
	/**
     * 根据出库单号查询出库单
     * @param response
     * @throws Exception
     */
 	@RequestMapping("/queryOutStockOrderByOutStockOrderNum")
	public void queryOutStockOrderByOutStockOrderNum(HttpServletResponse response, String out_stock_order_num) throws Exception{
		List<OutStockOrder> outStockOrderList = loadingProcessService.queryOutStockOrderByOutStockOrderNum(out_stock_order_num);
		JSONArray jsonArray = JSONArray.fromObject(outStockOrderList.get(0));
		if (outStockOrderList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
	}
}
