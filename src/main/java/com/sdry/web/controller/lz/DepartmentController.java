package com.sdry.web.controller.lz;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sdry.model.ljq.XtreeUtil;
import com.sdry.model.lz.Department;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Staff;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.DepartmentService;
import com.sdry.service.lz.StaffService;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * @ClassName DepartmentController
 * @Description 部门信息
 * @Author lz
 * @Date 2019年4月16日 10:16:31
 * @Version 1.0
 */
@Controller
@RequestMapping("/dept")
public class DepartmentController {

    public static final Integer AFFECT_ROW = 0;
    @Resource
    DepartmentService departmentService;
    @Resource 
    StaffService staffService;

    /**
     * 部门新增
     * @param response
     * @param dept
     */
    @RequestMapping("/addDept")
    public void addDept(HttpServletResponse response, Department dept){
        Long affectRow = departmentService.addDept(dept);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteDeptById")
    public void deleteDeptById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = departmentService.deleteDeptById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑部门表
     * @param response
     */
    @RequestMapping("/updateDept")
    public void updateDept(HttpServletResponse response, Department dept) {
        Long affectRow = departmentService.updateDept(dept);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询部门表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryDeptCriteria")
    public void queryDeptCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Department> deptList = departmentService.queryDeptCriteria(criteria);
        int count = departmentService.countDeptCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(deptList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    /**
	 * 获取部门员工的树状结构
	 * @param response
     * @throws Exception 
	 */
    @RequestMapping("/getDeptXtree")
	public void getDeptXtree(HttpServletResponse response) throws Exception {
		List<XtreeUtil> xtreeList = new ArrayList<>();
		List<Department> dList = departmentService.queryAllDept();
		for (Department department : dList) {
			XtreeUtil xtree = new XtreeUtil();
			List<XtreeUtil> xList = new ArrayList<>();
			List<ZcSysUserEntity> sList=staffService.queryStaffByDept(department.getId());
			if(sList.size()!=0) {
				
				for (ZcSysUserEntity staff : sList) {
					XtreeUtil data = new XtreeUtil();
					data.setTitle(staff.getUserName());
					data.setValue(staff.getId().toString());
					data.setData(null);
					xList.add(data);
				}
				xtree.setTitle(department.getDept_name());
				xtree.setValue("-1");
				xtree.setData(xList);
				xtreeList.add(xtree);
			}
			
		}
		
		JSONArray json = JSONArray.fromObject(xtreeList);
		ResponseUtil.write(response, json);
	}
}