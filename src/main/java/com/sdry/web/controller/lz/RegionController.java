package com.sdry.web.controller.lz;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Region;
import com.sdry.service.lz.RegionService;
import com.sdry.utils.ResponseUtil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @ClassName RegionController
 * @Description 地区信息
 * @Author lz
 * @Date 2019年5月27日 11:15:09
 * @Version 1.0
 */
@Controller
@RequestMapping("/region")
public class RegionController {
    public static final Integer AFFECT_ROW = 0;

    @Resource
    RegionService regionService;

    /**
     * 地区新增
     * @param response
     * @param region
     */
    @RequestMapping("/addRegion")
    public void addRegion(HttpServletResponse response, Region region){
        Long affectRow = regionService.addRegion(region);
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteRegionById")
    public void deleteRegionById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = regionService.deleteRegionById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑地区表
     * @param response
     */
    @RequestMapping("/updateRegion")
    public void updateRegion(HttpServletResponse response, Region region) {
        Long affectRow = regionService.updateRegion(region);
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询地区表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryRegionCriteria")
    public void queryRegionCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Region> regionList = regionService.queryRegionCriteria(criteria);
        int count = regionService.countRegionCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(regionList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 下拉框查询所有地区
     * @param response
     * @throws Exception
     */
    @RequestMapping("/queryAllRegion")
    public void queryAllRegion(HttpServletResponse response) throws Exception{
        List<Region> regionList = regionService.queryAllRegion();
        JSONArray jsonArray = JSONArray.fromObject(regionList);
        if (regionList.size() > 0) {
            ResponseUtil.write(response, jsonArray);
        } else {
            ResponseUtil.write(response, null);
        }
    }
}
