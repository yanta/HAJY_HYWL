package com.sdry.web.controller.lz;

import com.sdry.model.lz.Container;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.ContainerService;
import com.sdry.utils.QRCodeUtil;
import com.sdry.utils.ResponseUtil;
import com.sdry.utils.RootPath;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/**
 * @ClassName ContainerController
 * @Description 容器信息
 * @Author lz
 * @Date 2019年4月16日 11:23:25
 * @Version 1.0
 */
@Controller
@RequestMapping("/container")
public class ContainerController {
    public static final Integer AFFECT_ROW = 0;

    @Resource
    ContainerService containerService;

    /**
     * 容器新增
     * @param response
     * @param container
     */
    @RequestMapping("/addContainer")
    public void addContainer(HttpServletResponse response, Container container){
        /*================================根据容器编号生成二维码================================*/
        String dir = RootPath.getRootPath("CONTAINER_RCODE_PATH");
        String rcodeName = "container_" + System.currentTimeMillis() + ".png";
        String logoImgPath = dir + rcodeName;
        container.setRcode(rcodeName);
        File file = new File(logoImgPath);
        Long affectRow = containerService.addContainer(container);
        try {
            QRCodeUtil.encode(container.getContainer_num(), null, new FileOutputStream(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            ResponseUtil.write(response, affectRow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据ID删除该行数据
     * @param response
     * @param idArr
     */
    @RequestMapping("/deleteContainerById")
    public void deleteContainerById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
        if(idArr.length > 0) {
            for (int i = 0; i < idArr.length; i++) {
                Long affectRow = containerService.deleteContainerById(Long.parseLong(idArr[i]));
                if (affectRow > AFFECT_ROW) {
                    try {
                        ResponseUtil.write(response, affectRow);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        ResponseUtil.write(response, AFFECT_ROW);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * 行内编辑容器表
     * @param response
     */
    @RequestMapping("/updateContainer")
    public void updateContainer(HttpServletResponse response, Container container) {
        String dir = RootPath.getRootPath("CONTAINER_RCODE_PATH");
        String rcodeName = "container_" + System.currentTimeMillis() + ".png";
        String logoImgPath = dir + rcodeName;
        container.setRcode(rcodeName);
        File file = new File(logoImgPath);
        Long affectRow = containerService.updateContainer(container);
        try {
            QRCodeUtil.encode(container.getContainer_num(), null, new FileOutputStream(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(affectRow > AFFECT_ROW){
            try {
                ResponseUtil.write(response, affectRow);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ResponseUtil.write(response, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 分页查询容器表
     * @param response
     * @param criteria
     */
    @RequestMapping("/queryContainerCriteria")
    public void queryContainerCriteria(HttpServletResponse response, LzQueryCriteria criteria){
        String keyword01 = criteria.getKeyword01();
        String keyword02 = criteria.getKeyword02();
        if(keyword01 == null){
            keyword01 = "";
        }
        if(keyword02 == null){
            keyword02 = "";
        }
        criteria.setKeyword01(keyword01);
        criteria.setKeyword02(keyword02);
        List<Container> ContainerList = containerService.queryContainerCriteria(criteria);
        int count = containerService.countContainerCriteria(criteria);
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = JSONArray.fromObject(ContainerList);
        jsonObject.put("code", AFFECT_ROW);
        jsonObject.put("data", jsonArray);
        jsonObject.put("count", count);
        try {
            ResponseUtil.write(response, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
