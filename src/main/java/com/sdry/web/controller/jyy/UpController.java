package com.sdry.web.controller.jyy;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import com.sdry.model.jyy.ReceiveDetailInstock;
import com.sdry.model.lz.Car;
import com.sdry.model.lz.LzQueryCriteria;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.jyy.Up;
import com.sdry.service.jyy.UpService;
import com.sdry.utils.DateJsonValueProcessor;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: UpController
 *@Description: 入库记录
 *@Author jyy
 *@Date 2019-05-17 17:20:06
 *@version 1.0
*/
@Controller
@RequestMapping("/up")
public class UpController{
	@Resource UpService UpService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageUp (Model model) {
		return "/td/Up";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public Up queryById(Long id) {
		Up param = UpService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<Up> queryAllByMution(Up param) {
		List<Up> list = UpService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(Up param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<Up> list = UpService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = UpService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(Up param, HttpServletResponse response) throws Exception{
		Long id = UpService.insert(param);
		response.getWriter().print(id);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(Up param,HttpServletResponse response) throws Exception{
		Integer count = UpService.update(param);
		response.getWriter().print(count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = UpService.delete(ids);
		response.getWriter().print(count);
	}

	/*===============================Lebron_Irving修改=================================*/
	/**
	 * 分页查询已收货未入库
	 * @param response
	 * @param criteria
	 */
	@RequestMapping("/queryReceiveDetailInstockCriteria")
	public void queryReceiveDetailInstockCriteria(HttpServletResponse response, LzQueryCriteria criteria){
		String keyword01 = criteria.getKeyword01();
		String keyword02 = criteria.getKeyword02();
		if(keyword01 == null){
			keyword01 = "";
		}
		if(keyword02 == null){
			keyword02 = "";
		}
		criteria.setKeyword01(keyword01);
		criteria.setKeyword02(keyword02);
		List<ReceiveDetailInstock> receiveDetailInstockList = UpService.queryReceiveDetailInstockCriteria(criteria);
		int count = UpService.countReceiveDetailInstockCriteria(criteria);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(receiveDetailInstockList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
