package com.sdry.web.controller.jyy;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.jyy.Receive;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.utils.DateJsonValueProcessor;
import com.sdry.utils.DateUtil;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: ReceiveController
 *@Description: 收货计划
 *@Author jyy
 *@Date 2019-04-18 09:35:01
 *@version 1.0
*/
@Controller
@RequestMapping("/receive")
public class ReceiveController{
	@Resource ReceiveService ReceiveService;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public Receive queryById(Long id) {
		Receive param = ReceiveService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<Receive> queryAllByMution(Receive param) {
		List<Receive> list = ReceiveService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(Receive param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	
	/** 
	 * 条件分页查询  查询所有“状态：已收货”的单子
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/list2")
	public void list2(Receive param,HttpServletResponse response) throws Exception{
		param.setState(1);
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	
	/** 
	 * 条件分页查询  查询所有“状态：未收货”的单子
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/list3")
	public void list3(Receive param,HttpServletResponse response) throws Exception{
		param.setState(0);
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	
	/** 
	 * 条件分页查询  查询所有“状态：已质检”的单子
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/list4")
	public void list4(Receive param,HttpServletResponse response) throws Exception{
		param.setState(2);
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	/** 
	 * 条件分页查询  查询所有“状态：已入库”的单子
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/list5")
	public void list5(Receive param,HttpServletResponse response) throws Exception{
		param.setState(3);
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	/** 
	 * 条件分页查询  查询所有“状态：已上架”的单子
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/list6")
	public void list6(Receive param,HttpServletResponse response) throws Exception{
		param.setState(4);
		/*
		 * 条件分页查询内容
		 */
		List<Receive> list = ReceiveService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert( HttpSession httpSession, Receive param, HttpServletResponse response) throws Exception{
		
		//获取当前操作人
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		param.setCreateDate(DateUtil.dateFormat1());
		param.setCreateName(userName);
		param.setState(0);
		Long id = ReceiveService.insert(param);
		response.getWriter().print(id);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(Receive param,HttpServletResponse response) throws Exception{
		Integer count = ReceiveService.update(param);
		response.getWriter().print(count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(HttpSession httpSession,String ids,HttpServletResponse response) throws Exception{
		/*ids=ids.replace("'",""); 
		Integer count = ReceiveService.delete(ids);
		response.getWriter().print(count);*/
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        //System.out.println(df.format(new Date()));
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		String[] arr = ids.split(",");
		
		for (String s : arr) {
			 Map map = new HashMap();
			 map.put("id", Long.valueOf(s));
			map.put("cancellation_time", df.format(new Date()));
			map.put("cancellation", userName);
			Integer affact = ReceiveService.cacel(map);
			ResponseUtil.write(response, affact);
		}
        
	}
	
	/** 
	 * 单据合并
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	 * @throws Exception 
	*/
	@RequestMapping("/merge")
	public void merge(HttpSession httpSession, String ids, String receiveNumber, HttpServletResponse response){
		int i = ReceiveService.merge(httpSession, ids, receiveNumber);
		try {
			ResponseUtil.write(response, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
        
	}
	
	@RequestMapping("/restor")
	public void delet(HttpSession httpSession,String ids,HttpServletResponse response) throws Exception{
		/*ids=ids.replace("'",""); 
		Integer count = ReceiveService.delete(ids);
		response.getWriter().print(count);*/
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        //System.out.println(df.format(new Date()));
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		String[] arr = ids.split(",");
		
		for (String s : arr) {
			 Map map = new HashMap();
			 map.put("id", Long.valueOf(s));
			map.put("restorer_time", df.format(new Date()));
			map.put("restorer", userName);
			Integer affact = ReceiveService.restor(map);
			ResponseUtil.write(response, affact);
		}
        
	}
	/**
	 * 收货完成 确认操作
	 * @param ids
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping("/doReceive")
	public void doReceive(HttpSession httpSession, String ids,HttpServletResponse response) throws Exception {
		
		//获取当前操作人
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		String[] arr = ids.split(",");
		
		for (String s : arr) {
			Receive param = new Receive();
			
			param.setId(Long.valueOf(s));
			param.setState(3);
			//获取当前时间
			param.setSureDate(DateUtil.dateFormat1());
			//获取当前操作人
			param.setSureName(userName);
			
			Integer affact = ReceiveService.update(param);
			
			ResponseUtil.write(response, affact);
		}
	}
	
	/**
	 * 更新状态
	 * @param ids
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/updateState")
	public void updateState(HttpSession httpSession, String ids,HttpServletResponse response) throws Exception{
		//获取当前操作人
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		Integer count = 0;
		String[] split = ids.split(",");
		
		for (String s : split) {
			long id = Long.parseLong(s);
			Receive param = new Receive();
			param.setId(id);
			param.setState(1);
			
			//获取当前时间
			param.setReceiveDate(DateUtil.dateFormat1());
			//获取当前操作人
			param.setReceiveName(userName);
			
			count = ReceiveService.update(param);
		}
		response.getWriter().print(count);
	}
	
	//-------------------------------------------------app---------------------------------------------------------------
	
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	 * @throws Exception 
	*/
	@RequestMapping("/receiving")
	@ResponseBody
	public void receiving(Receive param,HttpServletResponse response) throws Exception {
		param.setState(0);
		List<Receive> list = ReceiveService.queryAllByMution(param);
		JSONObject jsonObject = new JSONObject();
		int count = list.size();
		if (count>0) {
			//传递数据到页面
		    jsonObject.put("status", "1");
		    jsonObject.put("message", "");
		    jsonObject.put("data", list);
		}else {
			//传递数据到页面
		    jsonObject.put("status", "0");
		    jsonObject.put("message", "查询数据为空");
		    jsonObject.put("data", list);
		}
		ResponseUtil.write(response, jsonObject);
	}
	
	/**
	 * 修改收货单状态
	 * @param id
	 * @param response
	 */
	@RequestMapping("/updateStatus")
	public void updateStatus(Long id ,HttpServletResponse response, HttpSession httpSession){
		Receive queryById = ReceiveService.queryById(id);
		int i = 0;
		if(queryById != null){
			//获取当前操作人
			ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
			String userName = su.getUserName();
			queryById.setConfirmorDate(DateUtil.dateFormat1());
			queryById.setConfirmor(userName);
			queryById.setState(1);
			i = ReceiveService.update(queryById);
		}
		try {
			ResponseUtil.write(response, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
