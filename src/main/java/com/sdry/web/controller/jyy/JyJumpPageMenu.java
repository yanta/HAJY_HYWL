package com.sdry.web.controller.jyy;

import java.util.List;

import javax.annotation.Resource;

import com.sdry.service.lz.StockOutOrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.service.lz.CustomerService;
import com.sdry.service.tdd.OthersService;
import com.sdry.service.zc.ZcInventoryManagementService;

/**
 * 
 * @ClassName StockOutController
 * @Description 入库单跳转菜单
 * @Author lz
 * @Date 2019年3月5日 10:39:13
 * @Version 1.0
 */
@Controller
@RequestMapping("/jyJumpPageMenu")
public class JyJumpPageMenu {
	
	@Resource
	StockOutOrderService stockOutService;

	//收货
	@RequestMapping("/receivingNote")
	public String receivingNote(Model model){
		List<Customer> customer_type0 = stockOutService.queryAllCustomerByType();
		model.addAttribute("customer_type0", customer_type0);
		//Customer param = new Customer();
		//param.setCustomer_type("0");
		//List<Customer> customers = othersService.customerQueryAllByMution(param );
		//model.addAttribute("customers",customers);
		return "/jy/receiving/receivingNote";
	}
	
	/**
	 * 去上架页面
	 * @return
	 */
	@RequestMapping("/toUp")
	public String toUp(Model model) {
		/*ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<Warehouse> allWarehouseList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		model.addAttribute("allWarehouseList", allWarehouseList);*/
		return "jy/up/up";
	}

	/**
	 * 质检管理
	 * @return
	 */
	@RequestMapping("/receiveDetailQuality")
	public String receiveDetailQuality () {
		return "jy/receiveDetailQuality/receiveDetailQuality";
	}
	
	/*=======调账管理=======*/
}
