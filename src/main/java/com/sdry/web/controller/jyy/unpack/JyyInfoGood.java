package com.sdry.web.controller.jyy.unpack;

/**
 * 良品库存实体（仅本人使用）
 * @author jyy
 * @2019-08-21 16:08:31
 */
public class JyyInfoGood {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/**id*/
	private Long id;
	/**供应商id*/
	private Long customerId;
	
	/**物料id*/
	private Long mid;
	/**库存数量*/
	private int mNum;
	/**批次*/
	private String mBatch;

	//物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //零件号
    private String brevity_num;
    //包装数量
  	private String packing_quantity;
  	
  	//查询条件（5个物料编号同时模糊查询）
  	private String materielNum1;
  	private String materielNum2;
  	private String materielNum3;
  	private String materielNum4;
  	private String materielNum5;
  	
  	
	public String getMaterielNum1() {
		return materielNum1;
	}
	public void setMaterielNum1(String materielNum1) {
		this.materielNum1 = materielNum1;
	}
	public String getMaterielNum2() {
		return materielNum2;
	}
	public void setMaterielNum2(String materielNum2) {
		this.materielNum2 = materielNum2;
	}
	public String getMaterielNum3() {
		return materielNum3;
	}
	public void setMaterielNum3(String materielNum3) {
		this.materielNum3 = materielNum3;
	}
	public String getMaterielNum4() {
		return materielNum4;
	}
	public void setMaterielNum4(String materielNum4) {
		this.materielNum4 = materielNum4;
	}
	public String getMaterielNum5() {
		return materielNum5;
	}
	public void setMaterielNum5(String materielNum5) {
		this.materielNum5 = materielNum5;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public int getmNum() {
		return mNum;
	}
	public void setmNum(int mNum) {
		this.mNum = mNum;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
}
