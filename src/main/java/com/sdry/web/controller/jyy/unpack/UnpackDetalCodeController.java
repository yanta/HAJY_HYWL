package	com.sdry.web.controller.jyy.unpack;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.jyy.unpack.UnpackDetalCode;
import com.sdry.service.jyy.unpack.UnpackDetalCodeService;
import com.sdry.utils.DateJsonValueProcessor;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: UnpackDetalCodeController
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:31
 *@version 1.0
*/
@Controller
@RequestMapping("/unpackDetalCode")
public class UnpackDetalCodeController{
	@Resource UnpackDetalCodeService UnpackDetalCodeService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageUnpackDetalCode (Model model) {
		return "/td/UnpackDetalCode";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public UnpackDetalCode queryById(Long id) {
		UnpackDetalCode param = UnpackDetalCodeService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	public void queryAllByMution(UnpackDetalCode param, HttpServletResponse response) {
		List<UnpackDetalCode> list = UnpackDetalCodeService.queryAllByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", list.size());
		try {
			ResponseUtil.write(response, result);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(UnpackDetalCode param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<UnpackDetalCode> list = UnpackDetalCodeService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = UnpackDetalCodeService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(UnpackDetalCode param, HttpServletResponse response) throws Exception{
		Long id = UnpackDetalCodeService.insert(param);
		response.getWriter().print(id);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(UnpackDetalCode param,HttpServletResponse response) throws Exception{
		Integer count = UnpackDetalCodeService.update(param);
		response.getWriter().print(count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = UnpackDetalCodeService.delete(ids);
		response.getWriter().print(count);
	}
	
	//-------------------------------------------------------------app--------------------------------------------------------
	/**
	 * 拆箱后条码查询
	 * @param mcode 条码
	 * @param did 拆箱详细 id
	 * @param response 响应
	 */
	@RequestMapping("/selectDevanningByCodeApp")
	public void selectDevanningByCodeApp(String mcode, String did, HttpServletResponse response ){
		UnpackDetalCode param = new UnpackDetalCode();
		param.setCode(mcode);
		param.setDid(Long.valueOf(did));
		List<UnpackDetalCode> list = UnpackDetalCodeService.queryAllByMution(param);
		
		JSONObject jsonObject = new JSONObject();
		int count = list.size();
		if (count>0) {
			//传递数据到页面
		    jsonObject.put("status", "1");
		    jsonObject.put("message", "");
		    jsonObject.put("data", list.get(0));
		}else {
			//传递数据到页面
		    jsonObject.put("status", "0");
		    jsonObject.put("message", "查询数据为空");
		    //jsonObject.put("data", list);
		}
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
