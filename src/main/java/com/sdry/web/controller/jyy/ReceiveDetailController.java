package com.sdry.web.controller.jyy;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.jyy.Workload;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.ReceiveMark;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.service.jyy.UpService;
import com.sdry.service.lz.MaterielService;
import com.sdry.service.tdd.OthersService;
import com.sdry.utils.BarCodeUtils;
import com.sdry.utils.DateJsonValueProcessor;
import com.sdry.utils.DateUtil;
import com.sdry.utils.QRCodeUtil;
import com.sdry.utils.ResponseUtil;
import com.sdry.utils.RootPath;
import com.sdry.utils.StringUtil;
import com.sdry.utils.codeUtil;

/**
 *
 *@ClassName: ReceiveDetailController
 *@Description: 收货计划详情
 *@Author jyy
 *@Date 2019-04-19 14:30:17
 *@version 1.0
*/
@Controller
@RequestMapping("/receiveDetail")
public class ReceiveDetailController{
	@Resource
	ReceiveService ReceiveService;

	@Resource
	ReceiveDetailService ReceiveDetailService;

	@Resource
	UpService upService;
	
	@Resource 
	MaterielService materielService;
	
	@Resource 
	OthersService othersService;
	
	ReceiveMark receiveMarkSrc = new ReceiveMark();
	
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public ReceiveDetail queryById(Long id) {
		ReceiveDetail param = ReceiveDetailService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<ReceiveDetail> queryAllByMution(ReceiveDetail param) {
		List<ReceiveDetail> list = ReceiveDetailService.queryAllByMution(param);
		return list;
	}

	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(ReceiveDetail param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<ReceiveDetail> list = ReceiveDetailService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = ReceiveDetailService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("data", jsonArray);
		result.put("count", total);
		ResponseUtil.write(response, result);
	}
	
	/*=======================条码工具类===========================*/
	public void Label(Long id, int num) {
		ReceiveDetail receiveDetail = ReceiveDetailService.queryById(id);
		Materiel materiel = materielService.queryMaterielById(receiveDetail.getMid());
		//Customer customer = othersService.selectCustomerByReceiveNumber(receiveDetail.getReceiveNumber());
		//根据物料id查询客户的remark
		Customer customer = materielService.queryCustomerByCustomerId(materiel.getCustomer_id());
		String s = materiel.getSequence_number();
		String snum = "";
		String slast = "A";
		if(null != s && !"".equals(s)){
			snum = s.substring(0,s.length() - 1);
			slast = s.substring(s.length() - 1,s.length());
		}
		int sequence_number = StringUtil.strNum(snum);
		List<List<String>> list = new ArrayList<>();
		//生成条码及二维码
		for (int i = 0; i < num; i++) {
			List<String> codelist = new ArrayList<>();
			if((sequence_number + i) > 999999){
				sequence_number = 0;
				i = 1;
				num = num - i;
				slast = codeUtil.getNextUpEn(slast);
			}
			try {
				codelist = getcode(customer.getRemark(),materiel.getMateriel_num(),receiveDetail.getPici(),(sequence_number+i)+slast);
			}catch (Exception e) {
				e.printStackTrace();
			}
			list.add(codelist);
			codelist = null;
		}
		updateNumber(materiel.getId(),sequence_number+num+slast);
	}
	
	public List<String> getcode(String client, String Materiel_num, String Batch,String Sequence_number) throws FileNotFoundException, Exception{
		List<String> list = new ArrayList<>();
		//materiel.getClient(),materiel.getMateriel_num(),param.getBatch(),materiel.getSequence_number()
		String code =  codeUtil.getCodes(client, Materiel_num, Batch, Sequence_number);
		System.out.println("生成的条码：" + code);
		list.add(code);
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");// 可以方便地修改日期格式
		String datetime = dateFormat.format(now);
		//String str = request.getSession().getServletContext().getRealPath("/");
		//String path = "INDEX\\img\\"+"code"+datetime+Sequence_number+".png";
		//String path1 = "INDEX\\img\\"+"codebar"+datetime+Sequence_number+".png";
		String twoCodeDir = RootPath.getRootPath("RECEIVE_TWO_RCODE_PATH");
		String barCodeDir = RootPath.getRootPath("RECEIVE_BAR_RCODE_PATH");
		String rcodeTwoCodName = "rcode//receive_two_rcode//" + "receive_two_"+Materiel_num + datetime + Sequence_number + ".png";
		String rcodeBarCodName = "rcode//receive_bar_rcode//" + "receive_bar_"+Materiel_num + datetime + Sequence_number + ".png";
		String path = twoCodeDir + rcodeTwoCodName;
		String path1 = barCodeDir + rcodeBarCodName;
        File file = new File(path);
        //生成二维码
        QRCodeUtil.encode(code, null, new FileOutputStream(file), true);
        //生成条形码
        BarCodeUtils.BarCodeImage(code, path1);
        list.add(path);
        list.add(path1);
        receiveMarkSrc.setCode(code);
        receiveMarkSrc.setTwoCodePath(rcodeTwoCodName);
        receiveMarkSrc.setBarCodePath(rcodeBarCodName);
		return list;
	}
	/**
	 * 根据物料主键更新物料顺序号
	 * @param id 物料主键
	 * @param num 顺序号
	 * @return
	 */
	public int updateNumber(Long id,String num){
		int count = othersService.updateMateriel(id,num);
		return count;
	}
	/*=======================条码工具类===========================*/
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(ReceiveDetail param, HttpServletResponse response, HttpServletRequest request) {
		String ids = "";
		ReceiveDetail receiveDetail = new ReceiveDetail();
		receiveDetail.setReceiveNumber(param.getReceiveNumber());

		if (param.getMid() != null) {
			receiveDetail.setMid(param.getMid());
			List<ReceiveDetail> details = ReceiveDetailService.queryAllByMution(receiveDetail);
			if (details.size() > 0) {
				for (int i = 0; i < details.size(); i++) {
					if (i == 0) {
						ids += details.get(i).getId();
					} else {
						ids += "," + details.get(i).getId();
					}
					ReceiveDetailService.delete(ids);
				}
			}
		} else {
			param.setMid(param.getId());
		}
		/*____________________新增二维码rcode_______________________*/
		/**
		 * 根据物料id查询物料实体
		 */
		Materiel materiel = ReceiveDetailService.queryMaterielByReceiveDetailId(param.getMid());
		/*String dir = RootPath.getRootPath("RECEIVE_RCODE_PATH");
		String rcodeName = "receive_" + System.currentTimeMillis() + ".png";
		String logoImgPath = dir + rcodeName;
		param.setRcode(rcodeName);
		File file = new File(logoImgPath);
		try {
			QRCodeUtil.encode(materiel.getMateriel_num(), null, new FileOutputStream(file), true);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		/*___________________________________________*/
		if ("0".equals(materiel.getBarcode_type())) {
			param.setTotalNum(param.getSendNum() * Integer.parseInt(materiel.getPacking_quantity()) + param.getSingleNum());
		} else {
			param.setTotalNum(param.getSendNum() * Integer.parseInt(materiel.getPacking_quantity()) + param.getSingleNum());
		}
		Long identity = ReceiveDetailService.insert(param);
		//生成条码存入ReceiveMark
		ReceiveMark receiveMark = new ReceiveMark();
		if (param.getSingleNum() > 0) {
			for (int i = 0; i < param.getSendNum() + 1; i++) {
				//收货单详细的自增主键
				receiveMark.setReceive_detail_id(identity);
				if (i == param.getSendNum()) {
					receiveMark.setNum(param.getSingleNum());
				} else {
					receiveMark.setNum(Integer.parseInt(materiel.getPacking_quantity()));
				}
				receiveMark.setSAP_QAD(materiel.getMateriel_num());
				//=========按规则生成的条码=========
				try {
					Label(identity, 1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				//============================
				receiveMark.setCode(receiveMarkSrc.getCode());
				receiveMark.setTwoCodePath(receiveMarkSrc.getTwoCodePath());
				receiveMark.setBarCodePath(receiveMarkSrc.getBarCodePath());
				ReceiveDetailService.addReceiveMark(receiveMark);
			}
		} else {
			for (int i = 0; i < param.getSendNum(); i++) {
				//收货单详细的自增主键
				receiveMark.setReceive_detail_id(identity);
				receiveMark.setNum(Integer.parseInt(materiel.getPacking_quantity()));
				receiveMark.setSAP_QAD(materiel.getMateriel_num());
				//=========按规则生成的条码=========
				try {
					Label(identity, 1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				//============================
				receiveMark.setCode(receiveMarkSrc.getCode());
				receiveMark.setTwoCodePath(receiveMarkSrc.getTwoCodePath());
				receiveMark.setBarCodePath(receiveMarkSrc.getBarCodePath());
				ReceiveDetailService.addReceiveMark(receiveMark);
			}
		}
		try {
			response.getWriter().print(identity);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(ReceiveDetail param,HttpServletResponse response) throws Exception{
		Integer count = ReceiveDetailService.update(param);
		response.getWriter().print(count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = ReceiveDetailService.delete(ids);
		response.getWriter().print(count);
	}
	
	/**
	 * 根据供应商 名称 查询相关物料信息
	 * @throws Exception
	 */
	/*@RequestMapping("/materielList")
	public void materielList(String id, HttpServletResponse response) throws Exception {
		//转json数据
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		if(StringUtil.isNotEmpty(id) && !"null".equals(id)) {
			List<Materiel> list = ReceiveService.materielList(Long.parseLong(id));
			JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
			//添加入result
			result.put("code", 0);
			result.put("data", jsonArray);
			result.put("count", list.size());
			ResponseUtil.write(response, result);
		} else {
			//添加入result
			result.put("code", 0);
			result.put("data", "");
			result.put("count", 0);
			ResponseUtil.write(response, result);
		}
	}*/
	
	/**
	 * llm新增，对上面方法的修改
	 * @param materiel
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/materielList")
	public void materielList(Materiel materiel, HttpServletResponse response) throws Exception {
		//转json数据
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		String id = materiel.getCustomer_id().toString();
		if(StringUtil.isNotEmpty(id) && !"null".equals(id)) {
			List<Materiel> list = ReceiveService.queryAllMaterielByParam(materiel);
			JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
			//添加入result
			result.put("code", 0);
			result.put("data", jsonArray);
			result.put("count", list.size());
			ResponseUtil.write(response, result);
		} else {
			//添加入result
			result.put("code", 0);
			result.put("data", "");
			result.put("count", 0);
			ResponseUtil.write(response, result);
		}
	}
	
	//-------------------------------------------------app---------------------------------------------------------------
	
	/**
	 * 根据收货单号查询收货详情
	 * @param receiveNumber 收货单号
	 * @param response 响应
	 * @throws Exception 
	 */
	@RequestMapping("/selectDtailByNumber")
	public void selectDtailByNumber(String receiveNumber,HttpServletResponse response) throws Exception {
		
		ReceiveDetail param = new ReceiveDetail();
		param.setReceiveNumber(receiveNumber);
		List<ReceiveDetail> list = ReceiveDetailService.queryAllByMution(param );
		
		JSONObject jsonObject = new JSONObject();
		int count = list.size();
		if (count>0) {
			//传递数据到页面
		    jsonObject.put("status", "1");
		    jsonObject.put("message", "");
		    jsonObject.put("data", list);
		}else {
			//传递数据到页面
		    jsonObject.put("status", "0");
		    jsonObject.put("message", "查询数据为空");
		    jsonObject.put("data", list);
		}
		ResponseUtil.write(response, jsonObject);
	}
	
	/**
	 * 根据id修改
	 * @param json
	 * @param response
	 * @throws Exception 
	 */
	@RequestMapping("/updateById")
	public void  updateById(String json, HttpServletResponse response) throws Exception {
		
		int affact = 0;
		Gson gson = new Gson();
		List<ReceiveDetail> list= gson.fromJson(json,new TypeToken<List<ReceiveDetail>>() {}.getType());
		
		for (ReceiveDetail receiveDetail : list) {
			affact = ReceiveDetailService.update(receiveDetail);
		}
		
		ReceiveDetail receiveDetail = list.get(0);
		//判断一级中的二级是否都已收货
		int count =  ReceiveDetailService.countState(receiveDetail.getReceiveNumber());
		//更改一级状态
		if(count == 0) {
			Receive receive = new Receive();
			receive.setReceiveNumber(receiveDetail.getReceiveNumber());
			List<Receive> receives = ReceiveService.queryAllByMution(receive);
			Receive receive2 = receives.get(0);
			receive2.setState(1);
			receive2.setCreateDate(DateUtil.dateFormat1());
			receive2.setCreateName("app");
			ReceiveService.update(receive2);
		}
		
		JSONObject jsonObject = new JSONObject();
		if (affact > 0) {
			//传递数据到页面
		    jsonObject.put("status", "1");
		    jsonObject.put("message", "");
		    jsonObject.put("data", "提交成功");
		}else {
			//传递数据到页面
		    jsonObject.put("status", "0");
		    jsonObject.put("message", "提交失败");
		    jsonObject.put("data", "0");
		}
		ResponseUtil.write(response, jsonObject);
	}

	/**
	 * 根据收货单详细表id查询对应的条码
	 * @param receive_detail_id 收货单详细表id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/queryCodemarkByReceiveDetailId")
	public void queryCodemarkByReceiveDetailId(String receive_detail_id,HttpServletResponse response) {
		List<CodeMark> codeMarkList = ReceiveDetailService.queryCodemarkByReceiveDetailId(receive_detail_id);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(codeMarkList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 条件分页查询质检单
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/queryReceiveDetailQualityCriteria")
	public void queryReceiveDetailQualityCriteria(LzQueryCriteria criteria, HttpServletResponse response) {
		String keyword01 = criteria.getKeyword01();
		String keyword02 = criteria.getKeyword02();
		if(keyword01 == null){
			keyword01 = "";
		}
		if(keyword02 == null){
			keyword02 = "";
		}
		criteria.setKeyword01(keyword01);
		criteria.setKeyword02(keyword02);
		List<ReceiveDetailQuality> receiveDetailQualityList = ReceiveDetailService.queryReceiveDetailQualityCriteria(criteria);
		int count = ReceiveDetailService.countReceiveDetailQualityCriteria(criteria);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(receiveDetailQualityList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据质检单详细表id查询对应的工作量
	 * @param receiveDetailQuality_id 质检单详细表id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/queryWorkloadByreceiveDetailQualityId")
	public void queryWorkloadByreceiveDetailQualityId(String receiveDetailQuality_id, HttpServletResponse response) {
		List<Workload> workloadList = ReceiveDetailService.queryWorkloadByreceiveDetailQualityId(receiveDetailQuality_id);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(workloadList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据收货单详情id查询收货绑定实体
	 * @param receiveDetailId 收货单详情id
	 * @param response
	 */
	@RequestMapping("/queryReceiveMarkByReceiveId")
	public void queryReceiveMarkByReceiveId(String receiveDetailId, HttpServletResponse response) {
		List<ReceiveMark> receiveMarkList = ReceiveDetailService.queryReceiveMarkByReceiveId(receiveDetailId);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(receiveMarkList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 根据收货单id查询条码
	 * @param receiveId 收货单id
	 * @param response
	 */
	@RequestMapping("/selectReceiveMarkByReceiveId")
	public void selectReceiveMarkByReceiveId(Long receiveId, HttpServletResponse response) {
		List<ReceiveMark> receiveMarkList = ReceiveDetailService.selectReceiveMarkByReceiveId(receiveId);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(receiveMarkList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 收货单详细添加
	 * @param receiveDetail 收货单详细
	 * @param response
	 * @param request
	 */
	@RequestMapping("/insertReceiveDetail")
	public void insertReceiveDetail(ReceiveDetail receiveDetail, HttpServletResponse response, HttpServletRequest request) {
		if (receiveDetail != null) {
			Materiel materiel = ReceiveDetailService.queryMaterielByReceiveDetailId(receiveDetail.getMid());
			//先根据物料id查询是否存在
			List<ReceiveDetail> details = ReceiveDetailService.queryAllByMution(receiveDetail);
			//如果存在就不能再添加
			if (details.size() > 0) {
				try {
					ResponseUtil.write(response, 0);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				if ("0".equals(materiel.getBarcode_type())) {
					receiveDetail.setTotalNum(receiveDetail.getSendNum() * Integer.parseInt(materiel.getPacking_quantity()) + receiveDetail.getSingleNum());
				} else {
					receiveDetail.setTotalNum(receiveDetail.getSendNum() * Integer.parseInt(materiel.getPacking_quantity()) + receiveDetail.getSingleNum());
				}
				//如果不存在就再添加
				Long identity = ReceiveDetailService.insert(receiveDetail);
				//生成条码存入ReceiveMark
				ReceiveMark receiveMark = new ReceiveMark();
				if (receiveDetail.getSingleNum() > 0) {
					for (int i = 0; i < receiveDetail.getSendNum() + 1; i++) {
						//收货单详细的自增主键
						receiveMark.setReceive_detail_id(identity);
						if (i == receiveDetail.getSendNum()) {
							receiveMark.setNum(receiveDetail.getSingleNum());
						} else {
							receiveMark.setNum(Integer.parseInt(materiel.getPacking_quantity()));
						}
						receiveMark.setSAP_QAD(materiel.getMateriel_num());
						//=========按规则生成的条码=========
						try {
							Label(identity, 1);
						} catch (Exception e) {
							e.printStackTrace();
						}
						//============================
						receiveMark.setCode(receiveMarkSrc.getCode());
						receiveMark.setTwoCodePath(receiveMarkSrc.getTwoCodePath());
						receiveMark.setBarCodePath(receiveMarkSrc.getBarCodePath());
						ReceiveDetailService.addReceiveMark(receiveMark);
					}
				} else {
					for (int i = 0; i < receiveDetail.getSendNum(); i++) {
						//收货单详细的自增主键
						receiveMark.setReceive_detail_id(identity);
						receiveMark.setNum(Integer.parseInt(materiel.getPacking_quantity()));
						receiveMark.setSAP_QAD(materiel.getMateriel_num());
						//=========按规则生成的条码=========
						try {
							Label(identity, 1);
						} catch (Exception e) {
							e.printStackTrace();
						}
						//============================
						receiveMark.setCode(receiveMarkSrc.getCode());
						receiveMark.setTwoCodePath(receiveMarkSrc.getTwoCodePath());
						receiveMark.setBarCodePath(receiveMarkSrc.getBarCodePath());
						ReceiveDetailService.addReceiveMark(receiveMark);
					}
				}
				
				try {
					ResponseUtil.write(response, identity);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
