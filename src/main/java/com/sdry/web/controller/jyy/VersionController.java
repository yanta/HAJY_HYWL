package com.sdry.web.controller.jyy;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.jyy.Version;
import com.sdry.service.jyy.VersionService;
import com.sdry.utils.ResponseUtil;

/**
 * 
 * @Title:VersionController
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年3月26日
 * @version 1.0
 */

@Controller
@RequestMapping("/version")
public class VersionController {

	@Resource
	private VersionService versionService;
	
	/**
	 * 获取最新版本
	 * @Title:getLatestVersion   
	 * @param response
	 */
	@RequestMapping("/getLatestVersion")
	public void getLatestVersion(HttpServletResponse response){
		Version version = versionService.getLatestVersion();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("status", "1");
		jsonObject.put("data", version);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
