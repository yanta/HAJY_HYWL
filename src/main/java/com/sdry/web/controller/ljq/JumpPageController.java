package com.sdry.web.controller.ljq;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/JumpPage")
public class JumpPageController {
	
	@RequestMapping("/toIsubmited")
	public String toIsubmited(Model model) {
		return "/ljq/I_submited";
	}
	@RequestMapping("/toIapprovaled")
	public String toIapprovaled(Model model) {
		return "/ljq/I_approvaled";
	}
	
}
