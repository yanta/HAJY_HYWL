package com.sdry.web.controller.prepare;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.prepare.PrepareDetail;
import com.sdry.model.prepare.PrepareDetailCode;
import com.sdry.model.prepare.PrepareOne;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.service.prepare.PrepareDetailCodeService;
import com.sdry.service.prepare.PrepareDetailService;
import com.sdry.service.prepare.PrepareOneService;
import com.sdry.service.zc.ZcBindAndUnbindService;
import com.sdry.utils.ResponseUtil;

/**
 *
 * @ClassName: AppPrepareController
 * @Description: app
 * @Author tdd
 * @Date 2019-03-13 15:43:32
 * @version 1.0
 */
@Controller
@RequestMapping("/appPrepare")
public class AppPrepareController {
	@Resource PrepareOneService prepareOneService;
	@Resource PrepareDetailService prepareDetailService;
	@Resource PrepareDetailCodeService PrepareDetailCodeService;
	/**
	 * 根据配货单查询配货单
	 * @param prepareNumber 配料单号 回传  List<PrepareOnel>
	 * @param response 
	 * @throws Exception
	 */
	@RequestMapping("/selectPrepareOneByNumber")
	public void selectPrepareOneByNumber(String prepareNumber,HttpServletResponse response)
			throws Exception {
		List<PrepareOne> list = new ArrayList<>();
		List<PrepareOne> list1 = new ArrayList<>();
		List<PrepareOne> list2 = new ArrayList<>();
		PrepareOne param = new PrepareOne();
		param.setNum(prepareNumber);
		param.setState("0");
		list1 = prepareOneService.queryAllByMution(param);
		param.setState("2");
		list2 = prepareOneService.queryAllByMution(param);
		list.addAll(list1);
		list.addAll(list2);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("data", list);
		if(list.size() > 0){
			jsonObject.put("status", "1");
		}else{
			jsonObject.put("status", "0");
			jsonObject.put("message", "无数据");
		}
		ResponseUtil.write(response, jsonObject);
	}
	/**
	 * 根据配货单id查询配货单详情
	 * @param prepareNumber 配料单id 回传  List<PrepareDetail>
	 * @param response 
	 * @throws Exception
	 */
	@RequestMapping("/selectPrepareDtailByNumber")
	public void selectPrepareDtailByNumber(Long prepareNumber,HttpServletResponse response)
			throws Exception {
		List<PrepareDetail> list = new ArrayList<>();
		PrepareDetail param = new PrepareDetail();
		param.setDid(prepareNumber);
		list = prepareDetailService.queryAllByMution(param);
		List<PrepareDetail> list1 = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			PrepareDetail prepareDetail = list.get(i);
			List<String> location = prepareDetailService.selectLocationByMid(prepareDetail.getMid());
			prepareDetail.setLocation(location);
			list1.add(prepareDetail);
			prepareDetail = null;
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("data", list1);
		if(list.size() > 0){
			jsonObject.put("status", "1");
		}else{
			jsonObject.put("status", "0");
			jsonObject.put("message", "无数据");
		}
		ResponseUtil.write(response, jsonObject);
	}
	
	/**
	 * 配货提交
	 * @param prepareDetailCodes 配货单详情条码集合
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/peihou")
	public void appUnpackingTaskXj(String prepareDetailCodes, HttpServletResponse response) throws Exception{
		int affact = 0;
		JSONObject jsonObject = new JSONObject();
		Gson gson = new Gson();
		PrepareDetailCode prepareDetailCode = gson.fromJson(prepareDetailCodes, new TypeToken<PrepareDetailCode>() {}.getType());
		List<CodeMark> codeMarks = prepareDetailCode.getCodeMarks();
		int subnum = 0;
		for (int j = 0; j < codeMarks.size(); j++) {
			String code = codeMarks.get(j).getCode();
			int num = codeMarks.get(j).getNum();
			subnum = subnum+num;
			prepareDetailCode.setCodeMark(code);
			prepareDetailCode.setPreNum(num);
			//根据条码查询库位 
			//WarehouseRegionLocation warehouseRegionLocation = PrepareDetailCodeService.selectWarehouseRegionLocationByCodeDown(code);
			//prepareDetailCode.setLocations(String.valueOf(warehouseRegionLocation.getId()));
			PrepareDetailCodeService.insert(prepareDetailCode);
		}
		PrepareDetail prepareDetail = new PrepareDetail();
		prepareDetail.setId(prepareDetailCode.getDdid());
		List<PrepareDetail> prepareDetails = prepareDetailService.queryAllByMution(prepareDetail);
		if(prepareDetails.size() > 0){
			PrepareDetail prepareDetail2 = prepareDetails.get(0);
			int plannum = prepareDetail2.getPlannum();
			int subnum2 = prepareDetail2.getSubnum();
			int subnum3= subnum2+subnum;
			if (subnum3 >= plannum) {
				prepareDetail2.setState("1");
			}else if(subnum2 == 0){
				prepareDetail2.setState("2");
			}
			prepareDetail2.setSubnum(subnum3);
			Integer updateCount = prepareDetailService.update(prepareDetail2);
			if(updateCount>0){
				//根据配货单id查询配货单
				PrepareOne prepareOne = new PrepareOne();
				prepareOne.setId(prepareDetail2.getDid());
				List<PrepareOne> prepareOnes = prepareOneService.queryAllByMution(prepareOne);
				if(prepareOnes.size() > 0){
					PrepareOne prepareOne2 = prepareOnes.get(0);
					//默认配货中
					prepareOne2.setState("2");
					if (subnum3 >= plannum) {
						//根据配货单id查询未配货信息
						PrepareDetail prepareDetail3 = new PrepareDetail();
						prepareDetail3.setDid(prepareDetail2.getDid());
						prepareDetail3.setState("0");
						List<PrepareDetail> prepareDetails3 = prepareDetailService.queryAllByMution(prepareDetail3);
						//如果没有未配货信息
						if(prepareDetails3.size() == 0){
							//根据配货单id查询配货中信息
							prepareDetail3.setState("2");
							List<PrepareDetail> prepareDetails4 = prepareDetailService.queryAllByMution(prepareDetail3);
							//如果没有配货中信息
							if(prepareDetails4.size() == 0){
								//赋值配货完成
								prepareOne2.setState("1");
							}
						}
					}
					prepareOneService.update(prepareOne2);
				}
				affact = 1;
				jsonObject.put("data", "操作成功");
			}else{
				affact = 0;
				jsonObject.put("message", "操作失败");
			}
		}else{
			affact = 0;
			jsonObject.put("message", "该数据不存在");
		}
		jsonObject.put("status", affact);
		
		ResponseUtil.write(response, jsonObject);
	}
}
