package	com.sdry.web.controller.prepare;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.lz.Customer;
import com.sdry.model.prepare.PrepareDetail;
import com.sdry.model.prepare.PrepareOne;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.StockOutOrderService;
import com.sdry.service.prepare.PrepareDetailService;
import com.sdry.service.prepare.PrepareOneService;
import com.sdry.utils.DateUtil;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: PrepareOneController
 *@Description: 配货单
 *@Author tdd
 *@Date 2019-08-24 16:48:02
 *@version 1.0
*/
@Controller
@RequestMapping("/PrepareOne")
public class PrepareOneController{
	@Resource PrepareOneService PrepareOneService;
	@Resource StockOutOrderService stockOutService;
	@Resource PrepareDetailService prepareDetailService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pagePrepareOne (Model model) {
		List<Customer> customer_type0 = stockOutService.queryAllCustomerByType();
		model.addAttribute("customer_type0", customer_type0);
		return "/td/prepareOne";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public PrepareOne queryById(Long id) {
		PrepareOne param = PrepareOneService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<PrepareOne> queryAllByMution(PrepareOne param) {
		List<PrepareOne> list = PrepareOneService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(PrepareOne param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<PrepareOne> list = PrepareOneService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = PrepareOneService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		//创建result
		JSONObject jsonObject = new JSONObject();
		//添加入jsonObject
		jsonObject.put("code", 0);
		jsonObject.put("rows", list);
		jsonObject.put("total", total);
		ResponseUtil.write(response, jsonObject);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param mids 物料id集合
	 * @param plannums 物料id对应数量集合
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(PrepareOne param,Long[] mids,int[] plannums,HttpSession session, HttpServletResponse response) throws Exception{
		int flg = 0;
		PrepareOne prepareOne = new PrepareOne();
		prepareOne.setNum(param.getNum());
		List<PrepareOne> prepareOnes = PrepareOneService.queryAllByMution(prepareOne);
		if(prepareOnes.size()>0){
			flg = -1;
		}else{
			//获取当前操作人
			ZcSysUserEntity su = (ZcSysUserEntity)session.getAttribute("user");
			String userName = su.getUserName();
			param.setCreateTime(DateUtil.dateFormat3());
			param.setPerson(userName);
			param.setState("0");
			Long id = PrepareOneService.insert(param);
			if(null != id){
				PrepareDetail prepareDetail = new PrepareDetail();
				prepareDetail.setDid(id);
				prepareDetail.setState("0");
				for (int i = 0; i < mids.length; i++) {
					prepareDetail.setMid(mids[i]);
					prepareDetail.setPlannum(plannums[i]);
					prepareDetailService.insert(prepareDetail);
				}
				flg = 1;
			}
		}
		ResponseUtil.write(response,flg);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param mids 物料id集合
	 * @param plannums 物料id对应数量集合
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(PrepareOne param,Long[] mids,int[] plannums,HttpServletResponse response) throws Exception{
		int flg = 0;
		Integer count = PrepareOneService.update(param);
		if(count > 0){
			prepareDetailService.deleteByDids(String.valueOf(param.getId()));
			PrepareDetail prepareDetail = new PrepareDetail();
			prepareDetail.setDid(param.getId());
			prepareDetail.setState("0");
			for (int i = 0; i < mids.length; i++) {
				prepareDetail.setMid(mids[i]);
				prepareDetail.setPlannum(plannums[i]);
				prepareDetailService.insert(prepareDetail);
			}
			flg = 1;
		}
		ResponseUtil.write(response,flg);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = PrepareOneService.delete(ids);
		if(count > 0){
			prepareDetailService.deleteByDids(ids);
		}
		ResponseUtil.write(response,count);
	}
}
