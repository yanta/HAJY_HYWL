package	com.sdry.web.controller.prepare;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.prepare.PrepareDetailCode;
import com.sdry.service.prepare.PrepareDetailCodeService;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: PrepareDetailCodeController
 *@Description: 配货单详情条码
 *@Author tdd
 *@Date 2019-08-24 16:49:04
 *@version 1.0
*/
@Controller
@RequestMapping("/PrepareDetailCode")
public class PrepareDetailCodeController{
	@Resource PrepareDetailCodeService PrepareDetailCodeService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pagePrepareDetailCode (Model model) {
		return "/td/PrepareDetailCode";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public PrepareDetailCode queryById(Long id) {
		PrepareDetailCode param = PrepareDetailCodeService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<PrepareDetailCode> queryAllByMution(PrepareDetailCode param) {
		List<PrepareDetailCode> list = PrepareDetailCodeService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(PrepareDetailCode param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<PrepareDetailCode> list = PrepareDetailCodeService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = PrepareDetailCodeService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		//创建result
		JSONObject jsonObject = new JSONObject();
		//添加入jsonObject
		jsonObject.put("code", 0);
		jsonObject.put("rows", list);
		jsonObject.put("total", total);
		ResponseUtil.write(response, jsonObject);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(PrepareDetailCode param, HttpServletResponse response) throws Exception{
		Long id = PrepareDetailCodeService.insert(param);
		ResponseUtil.write(response,id);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(PrepareDetailCode param,HttpServletResponse response) throws Exception{
		Integer count = PrepareDetailCodeService.update(param);
		ResponseUtil.write(response,count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = PrepareDetailCodeService.delete(ids);
		ResponseUtil.write(response,count);
	}
}
