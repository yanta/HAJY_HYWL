package	com.sdry.web.controller.prepare;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.prepare.PrepareDetail;
import com.sdry.service.prepare.PrepareDetailService;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: PrepareDetailController
 *@Description: 配货单详情
 *@Author tdd
 *@Date 2019-08-24 16:47:15
 *@version 1.0
*/
@Controller
@RequestMapping("/PrepareDetail")
public class PrepareDetailController{
	@Resource PrepareDetailService PrepareDetailService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pagePrepareDetail (Model model) {
		return "/td/PrepareDetail";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public PrepareDetail queryById(Long id) {
		PrepareDetail param = PrepareDetailService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<PrepareDetail> queryAllByMution(PrepareDetail param) {
		List<PrepareDetail> list = PrepareDetailService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(PrepareDetail param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<PrepareDetail> list = PrepareDetailService.findPageByMution(param);
		List<PrepareDetail> list1 = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			PrepareDetail prepareDetail = list.get(i);
			List<String> location = PrepareDetailService.selectLocationByMid(prepareDetail.getMid());
			prepareDetail.setLocation(location);
			list1.add(prepareDetail);
			prepareDetail = null;
		}
		/*
		 * 条件查询总条数
		 */
		Integer total = PrepareDetailService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		//创建result
		JSONObject jsonObject = new JSONObject();
		//添加入jsonObject
		jsonObject.put("code", 0);
		jsonObject.put("rows", list1);
		jsonObject.put("total", total);
		ResponseUtil.write(response, jsonObject);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(PrepareDetail param, HttpServletResponse response) throws Exception{
		int flg = 0;
		PrepareDetail prepareDetail = new PrepareDetail();
		prepareDetail.setDid(param.getDid());
		prepareDetail.setMid(param.getMid());
		List<PrepareDetail> prepareDetails = PrepareDetailService.queryAllByMution(prepareDetail);
		if(prepareDetails.size() > 0){
			flg = -1;
		}else{
			param.setState("0");
			Long id = PrepareDetailService.insert(param);
			flg = 1;
		}
		ResponseUtil.write(response,flg);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(PrepareDetail param,HttpServletResponse response) throws Exception{
		Integer count = PrepareDetailService.update(param);
		ResponseUtil.write(response,count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = PrepareDetailService.delete(ids);
		ResponseUtil.write(response,count);
	}
}
