package com.sdry.web.controller.tdd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.jyy.unpack.UnpackDetalCode;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.service.lz.MaterielService;
import com.sdry.service.tdd.OthersService;
import com.sdry.utils.BarCodeUtils;
import com.sdry.utils.QRCodeUtil;
import com.sdry.utils.ResponseUtil;
import com.sdry.utils.StringUtil;
import com.sdry.utils.codeUtil;

/**
 * 
 * @ClassName:LabelController
 * @Description:标签打印
 * @Author tdd
 * @Date 2019年8月9日上午9:51:20
 * @version 1.0
 */
@Controller
@RequestMapping("/label")
public class LabelController {
	//收货计划单
	@Resource ReceiveService receiveService;
	//收货计划单详情
	@Resource ReceiveDetailService receiveDetailService;
	//收货计划单详情
	@Resource MaterielService materielService;
	@Resource OthersService othersService;
	/**
	 *根据发货单详情生成并打印条码
	 * @param id 发货单详情
	 * @param num 标签数量
	 * @param request
	 * @param response
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	@RequestMapping("/dy")
	public void Label(Long id,int num,HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException, Exception {
		ReceiveDetail receiveDetail = receiveDetailService.queryById(id);
		Materiel materiel = materielService.queryMaterielById(receiveDetail.getMid());
		Customer customer = othersService.selectCustomerByReceiveNumber(receiveDetail.getReceiveNumber());
		String s = materiel.getSequence_number();
		String snum = "";
		String slast = "A";
		if(null != s && !"".equals(s)){
			snum = s.substring(0,s.length() - 1);
			slast = s.substring(s.length() - 1,s.length());
		}
		int sequence_number = StringUtil.strNum(snum);
		List<List<String>> list = new ArrayList<>();
		//生成条码及二维码
		for (int i = 1; i <= num; i++) {
			List<String> codelist = new ArrayList<>();
			if((sequence_number+i) > 999999){
				sequence_number = 0;
				i = 1;
				num = num - i;
				slast = codeUtil.getNextUpEn(slast);
			}
			codelist = getcode(customer.getRemark(),materiel.getMateriel_num(),receiveDetail.getPici(),(sequence_number+i)+slast,request);
			list.add(codelist);
			codelist = null;
		}
		updateNumber(materiel.getId(),sequence_number+num+slast);
		/*
		 * 转为json数据传至页面
		 */
		JSONObject result = new JSONObject();
		//添加入result
		result.put("customer_name", customer.getCustomer_name());
		result.put("code", list);
		try {
			ResponseUtil.write(response, result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *根据物料id生成并打印条码
	 * @param id 物料id
	 * @param num 标签数量
	 * @param request
	 * @param response
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	@RequestMapping("/dyByMid")
	public void dyByMid(Long id,int num,HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException, Exception {
		Materiel materiel = materielService.queryMaterielById(id);
		Customer customer = new Customer();
		customer.setId(materiel.getCustomer_id());
		List<Customer> customers = othersService.customerQueryAllByMution(customer);
		customer = customers.get(0);
		String s = materiel.getSequence_number();
		String snum = "";
		String slast = "A";
		if(null != s && !"".equals(s)){
			snum = s.substring(0,s.length() - 1);
			slast = s.substring(s.length() - 1,s.length());
		}
		int sequence_number = StringUtil.strNum(snum);
		List<List<String>> list = new ArrayList<>();
		//生成条码及二维码
		for (int i = 1; i <= num; i++) {
			List<String> codelist = new ArrayList<>();
			if((sequence_number+i) > 999999){
				sequence_number = 0;
				i = 1;
				num = num - i;
				slast = codeUtil.getNextUpEn(slast);
			}
			codelist = getcode(customer.getRemark(),materiel.getMateriel_num(),null,(sequence_number+i)+slast,request);
			list.add(codelist);
			codelist = null;
		}
		updateNumber(materiel.getId(),sequence_number+num+slast);
		/*
		 * 转为json数据传至页面
		 */
		JSONObject result = new JSONObject();
		//添加入result
		result.put("code", list);
		try {
			ResponseUtil.write(response, result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 *根据物料id生成并打印条码
	 * @param code 码
	 * @param request
	 * @param response
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	@RequestMapping("/erWeiMa")
	public void erWeiMa(String code,HttpServletRequest request,HttpServletResponse response) throws FileNotFoundException, Exception {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");// 可以方便地修改日期格式
		String datetime = dateFormat.format(now);
		String str = request.getSession().getServletContext()
				.getRealPath("/");
		String path = "INDEX\\img\\"+"danju"+code+datetime+".png";
		String dir = str+path;
        //String logoImgPath = str+"/images/logo.png";
        File file = new File(dir);
        QRCodeUtil.encode(code, null, new FileOutputStream(file), true);
		/*
		 * 转为json数据传至页面
		 */
		JSONObject result = new JSONObject();
		//添加入result
		result.put("path", path);
		try {
			ResponseUtil.write(response, result);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 根据信息生成条码及二维码
	 * @param client码 客户
	 * @param Materiel_num 产品码
	 * @param Batch 批次
	 * @param Sequence_number 顺序号
	 * @param slast 顺序号最后一位
	 * @param request
	 * @return 
	 * @throws FileNotFoundException
	 * @throws Exception
	 */
	public List<String> getcode(String client,String Materiel_num,String Batch,String Sequence_number,HttpServletRequest request) throws FileNotFoundException, Exception{
		List<String> list = new ArrayList<>();
		//materiel.getClient(),materiel.getMateriel_num(),param.getBatch(),materiel.getSequence_number()
		String code =  codeUtil.getCodes(client,Materiel_num,Batch,Sequence_number);
		list.add(code);
		
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");// 可以方便地修改日期格式
		String datetime = dateFormat.format(now);
		String str = request.getSession().getServletContext()
				.getRealPath("/");
		String path = "INDEX\\img\\"+"code"+datetime+Sequence_number+".png";
		String path1 = "INDEX\\img\\"+"codebar"+datetime+Sequence_number+".png";
		String dir = str+path;
		String dir1 = str+path1;
        //String logoImgPath = str+"/images/logo.png";
        File file = new File(dir);
        QRCodeUtil.encode(code, null, new FileOutputStream(file), true);
        BarCodeUtils.BarCodeImage(code, dir1);
        list.add(path);
        list.add(path1);
		return list;
	}
	/**
	 * 根据物料主键更新物料顺序号
	 * @param id 物料主键
	 * @param num 顺序号
	 * @return
	 */
	public int updateNumber(Long id,String num){
		int count = othersService.updateMateriel(id,num);
		return count;
	}
	/**
	 * 根据拆箱单id查询条码
	 * @param unpackId 拆箱单id
	 * @param response
	 */
	@RequestMapping("/queryUnpackDetalCodeByUnpackId")
	public void queryUnpackDetalCodeByUnpackId(Long unpackId, HttpServletResponse response) {
		List<UnpackDetalCode> receiveMarkList = othersService.queryUnpackDetalCodeByUnpackId(unpackId);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(receiveMarkList);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		//jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
