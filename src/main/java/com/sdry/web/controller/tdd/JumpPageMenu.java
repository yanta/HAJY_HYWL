package com.sdry.web.controller.tdd;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.lz.Customer;
import com.sdry.service.tdd.OthersService;

/**
 * 
 * @ClassName StockOutController
 * @Description 入库单跳转菜单
 * @Author lz
 * @Date 2019年3月5日 10:39:13
 * @Version 1.0
 */
@Controller
public class JumpPageMenu {
	//拆箱作业——》任务导入
	@Resource OthersService othersService;
	@RequestMapping("/jumpPageMenu/unpackRule")
	public String unpackRule(Model model){
		//供应商
		Customer param = new Customer();
		param.setCustomer_type("0");
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		model.addAttribute("customers",customers);
		return "/td/unpack/unpackRule";
	}
}
