package	com.sdry.web.controller.tdd;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.lz.Customer;
import com.sdry.model.tdd.ReportUtil;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.tdd.OthersService;
import com.sdry.service.tdd.ReportService;
import com.sdry.utils.DateJsonValueProcessor;
/**
 *
 *@ClassName: ReportController
 *@Description: 报表
 *@Author tdd
 *@Date 2019-05-27 13:47:25
 *@version 1.0
*/
@Controller
@RequestMapping("/report")
public class ReportController{
	@Resource ReportService reportService;
	
	@Resource OthersService othersService;
	
	@RequestMapping("/rkReportZPage")
	public String rkReportZPage(Model model,HttpSession session){
		//供应商
		Customer param = new Customer();
		param.setCustomer_type("0");
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		model.addAttribute("customers",customers);
		
		//获取当前操作人
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customer_id = user.getCustomer_id();
		model.addAttribute("customer_id",customer_id);
		return "/td/report/rkReportZ";
	}
	@RequestMapping("/rkReportYPage")
	public String rkReportYPage(Model model,HttpSession session){
		//供应商
		Customer param = new Customer();
		param.setCustomer_type("0");
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		model.addAttribute("customers",customers);
		//获取当前操作人
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customer_id = user.getCustomer_id();
		model.addAttribute("customer_id",customer_id);
		return "/td/report/rkReportY";
	}
	@RequestMapping("/blpReportZPage")
	public String blpReportZPage(Model model,HttpSession session){
		//供应商
		Customer param = new Customer();
		param.setCustomer_type("0");
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		model.addAttribute("customers",customers);
		//获取当前操作人
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customer_id = user.getCustomer_id();
		model.addAttribute("customer_id",customer_id);
		return "/td/report/blpReportZ";
	}
	@RequestMapping("/blpReportYPage")
	public String blpReportYPage(Model model,HttpSession session){
		//供应商
		Customer param = new Customer();
		param.setCustomer_type("0");
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		model.addAttribute("customers",customers);
		//获取当前操作人
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customer_id = user.getCustomer_id();
		model.addAttribute("customer_id",customer_id);
		return "/td/report/blpReportY";
	}
	
	/**
	 * 查询入库报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @param response 响应
	 * @throws Exception 异常
	 */
	@RequestMapping("/selectRkReport")
	public void selectRkReport(String date,String type,Long customer_id,HttpServletResponse response) throws Exception{
		
		 /** 条件分页查询内容*/
		List<ReportUtil> list = reportService.selectRkReport(date, type, customer_id);
		
		 /** 条件查询总条数*/
		Integer total = list.size();
		
		 /** 转为json数据传至页面*/
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("rows", jsonArray);
		result.put("total", total);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		out.println(result.toString());
		out.flush();
		out.close();
	}
	/**
	 * 查询入库报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @param response 响应
	 * @throws Exception 异常
	 */
	@RequestMapping("/selectBlpReport")
	public void selectBlpReport(String date,String type,Long customer_id,HttpServletResponse response) throws Exception{
		
		/** 条件分页查询内容*/
		List<ReportUtil> list = reportService.selectBlpReport(date, type, customer_id);
		
		/** 条件查询总条数*/
		Integer total = list.size();
		
		/** 转为json数据传至页面*/
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("rows", jsonArray);
		result.put("total", total);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		out.println(result.toString());
		out.flush();
		out.close();
	}
}
