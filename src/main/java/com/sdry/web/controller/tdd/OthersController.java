package	com.sdry.web.controller.tdd;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.lz.Customer;
import com.sdry.service.tdd.OthersService;
/**
 *
 *@ClassName: OthersController
 *@Description: 其他信息表
 *@Author tdd
 *@Date 2019-04-17 15:47:25
 *@version 1.0
*/
@Controller
@RequestMapping("/others")
public class OthersController{
	@Resource OthersService othersService;
	//@Resource UnpackingDetailService unpackingDetailService;
	
	
	/** 
	 * 条件查询所有物料
	 * @param param 物料实体条件
	 * @return 实体集合
	*//*
	@RequestMapping("/materielQueryAllByMution")
	@ResponseBody
	public List<Materiel> materielQueryAllByMution(Materiel param) {
		List<Materiel> list = othersService.materielQueryAllByMution(param);
		return list;
	}
	*//** 
	 * 条件分页查询物料
	 * @param param 物料实体条件
	 * @param response
	 * @throws Exception
	*//*
	@RequestMapping("/materielList")
	public void materielList(Materiel param,HttpServletResponse response) throws Exception{
		
		 * 条件分页查询内容
		 
		List<Materiel> list = othersService.materielFindPageByMution(param);
		
		 * 条件查询总条数
		 
		Integer total = othersService.materielSelectCountByMution(param);
		
		 * 转为json数据传至页面
		 
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("rows", jsonArray);
		result.put("total", total);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		out.println(result.toString());
		out.flush();
		out.close();
	}*/
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/customerByName")
	@ResponseBody
	public Customer customerByName(String id) {
		Customer param = new Customer();
		//param.setCustomer_name(customer_id);
		param.setId(Long.parseLong(id));
		List<Customer> customers = othersService.customerQueryAllByMution(param);
		if(customers.size() > 0){
			return customers.get(0);
		}else{
			return null;
		}
	}
	/*@Resource
	private ZcInventoryManagementService zcInventoryManagementService;*/
	/** 
	 * 插入库存返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*//*
	@RequestMapping("/insertZcInventoryInfoEntity")
	public void insertZcInventoryInfoEntity(ZcInventoryInfoEntity param,Long udid, HttpServletResponse response) throws Exception{
		Long id = othersService.insertZcInventoryInfoEntity(param);
		UnpackingDetail unpackingDetail = new UnpackingDetail();
		unpackingDetail.setId(udid);
		unpackingDetail.setUdstate("2");
		unpackingDetailService.update(unpackingDetail);
		response.getWriter().print(id);
	}
	*//** 
	 * 插入代发货区返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 *//*
	@RequestMapping("/insertLjqWaitSendArea")
	public void insertLjqWaitSendArea(LjqWaitSendArea param,Long udid, HttpServletResponse response) throws Exception{
		Long id = othersService.insertLjqWaitSendArea(param);
		UnpackingDetail unpackingDetail = new UnpackingDetail();
		unpackingDetail.setId(udid);
		unpackingDetail.setUdstate("2");
		unpackingDetailService.update(unpackingDetail);
		response.getWriter().print(id);
	}*/
	
}
