package com.sdry.web.controller.tdd;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.jyy.Up;
import com.sdry.model.llm.LlmRegionStock;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WaitSendArea;
import com.sdry.model.tdd.UnpackingTask;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.service.tdd.OthersService;
import com.sdry.service.tdd.UnpackingService;
import com.sdry.service.tdd.UnpackingTaskService;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.utils.DateUtil;
import com.sdry.utils.ResponseUtil;

/**
 *
 * @ClassName: AppController
 * @Description: app
 * @Author tdd
 * @Date 2019-03-13 15:43:32
 * @version 1.0
 */
@Controller
public class AppController {
	@Resource UnpackingService unpackingService;
	@Resource UnpackingTaskService unpackingTaskService;
	@Resource OthersService othersService;
	@Resource ZcInventoryManagementService zcInventoryManagementService;
	@Resource ReceiveService receiveService;
	/**
	 * 根据状态查询拆箱任务单
	 * @param state1 状态 0待下架 1待拆箱 2待上架 3拆箱完成
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/appUnpackingTask")
	public void appUnpacking(String state1,HttpServletResponse response)
			throws Exception {
		List<UnpackingTask> list = new ArrayList<>();
		UnpackingTask param = new UnpackingTask();
		if(null == state1 || "".equals(state1)){
			param.setState1("0");
			list = unpackingTaskService.queryAllByMution(param);
			param.setState1("2");
			List<UnpackingTask> list1 = unpackingTaskService.queryAllByMution(param);
			list.addAll(list1);
		}else{
			param.setState1(state1);
			list = unpackingTaskService.queryAllByMution(param);
		}
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("data", list);
		if(list.size() > 0){
			jsonObject.put("status", "1");
		}else{
			jsonObject.put("status", "0");
			jsonObject.put("message", "无数据");
		}
		ResponseUtil.write(response, jsonObject);
	}
	/**
	 * 根据拆箱任务单id查对应库区
	 * @param id 拆箱任务单id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/appFindKqByUid")
	public void appFindKqByUid(Long id,HttpServletResponse response)
			throws Exception {
		UnpackingTask param = new UnpackingTask();
		param.setUtid(id);
		List<UnpackingTask> list = unpackingTaskService.queryAllByMution(param);
		JSONObject jsonObject = new JSONObject();
		if(list.size() > 0){
			param = list.get(0);
			//根据拆箱任务单id查对应库区
			List<String> kq = new ArrayList<>();
			/*Up up = new Up();
			up.setMid(param.getUnpacking().getMid());
			up.setCustomerId(param.getCustomer_id());*/
			//根据物料id查询库区位置
			Materiel materiel = receiveService.queryMaterielLocationById(param.getUnpacking().getMid());
			kq.add(materiel.getRegion_name());
			/*List<LlmWarehouseStock> list2 = receiveService.selectWarehouseByMid(up);
			for (int i = 0; i < list2.size(); i++) {
				String warehouse_name = list2.get(i).getWarehouse_name();
				List<LlmRegionStock> regionList = list2.get(i).getRegionList();
				for (int j = 0; j < regionList.size(); j++) {
					String region_name = regionList.get(j).getRegion_name();
					kq.add(warehouse_name+","+region_name);
					region_name = null;
				}
				warehouse_name = null;
				regionList = null;
			}*/
			
			if (kq.size() > 0) {
				jsonObject.put("data", kq);
				jsonObject.put("status", "1");
			}else{
				jsonObject.put("status", "0");
				jsonObject.put("message", "无数据");
			}	
		}else{
			jsonObject.put("status", "0");
			jsonObject.put("message", "无数据");
		}
		ResponseUtil.write(response, jsonObject);
	}
	/**
	 * 根据拆箱任务下架及拆箱
	 * @param utid 任务单id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/appUnpackingTaskXj")
	public void appUnpackingTaskXj(Long utid, HttpServletResponse response) throws Exception{
		int affact = 0;
		JSONObject jsonObject = new JSONObject();
		
		UnpackingTask param = new UnpackingTask();
		param.setUtid(utid);
		List<UnpackingTask> list = unpackingTaskService.queryAllByMution(param);
		if(list.size() > 0){
			param = list.get(0);
			param.setState1("2");
			//修改状态为待上架
			Integer count = unpackingTaskService.update(param);
			if(count > 0){
				//根据物料id和数量按先进先出减库存
				//othersService.zcInventoryInfoFIFOReduceStock(param.getUnpacking().getMid(),param.getUtnum(),param.getCustomer_id());
				affact = 1;
				jsonObject.put("data", "操作成功");
			}else{
				affact = 0;
				jsonObject.put("message", "操作失败");
			}
		}else{
			affact = 0;
			jsonObject.put("message", "操作失败");
		}
		jsonObject.put("status", affact);
		
		ResponseUtil.write(response, jsonObject);
	}
	/**
	 * 根据拆箱任务上架
	 * @param utid 任务单id
	 * @param enterPerson 用户id
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/appUnpackingTaskSj")
	public void appUnpackingTaskSj(Long utid,Long enterPerson, HttpServletResponse response) throws Exception{
		int affact = 0;
		JSONObject jsonObject = new JSONObject();
		
		UnpackingTask param = new UnpackingTask();
		param.setUtid(utid);
		List<UnpackingTask> list = unpackingTaskService.queryAllByMution(param);
		if(list.size() > 0){
			param = list.get(0);
			param.setState("1");
			param.setState1("3");
			//修改状态为拆箱完成
			Integer count = unpackingTaskService.update(param);
			if(count > 0){
				/*if(param.getUpperNum() > 0){
					//加入库数量至库存
					ZcInventoryInfoEntity inventoryInfo = new ZcInventoryInfoEntity();
					inventoryInfo.setMid(param.getUnpacking().getMid());
					inventoryInfo.setmNum(param.getUpperNum());
					inventoryInfo.setEnterPerson(enterPerson);
					inventoryInfo.setEnterDate(DateUtil.dateFormat1());
					//inventoryInfo.setCustomerId(param.getUnpacking().getCustomer_id());
					othersService.zcInventoryInfoAddStock(inventoryInfo);
				}*/
				//根据物料id和数量按先进先出大箱改小箱
				othersService.zcInventoryInfoFIFODaGaiXiaoStock(param.getUnpacking().getMid(),param.getUtnum(),param.getCustomer_id());
				if(param.getSendNum() > 0) {
					//加发货数量至待发货区
					WaitSendArea waitSendArea = new WaitSendArea();
					waitSendArea.setMateriel_id(param.getUnpacking().getMid());
					waitSendArea.setMateriel_num(param.getSendNum());
					waitSendArea.setMove_man(enterPerson+"");
					waitSendArea.setMove_date(DateUtil.dateFormat1());
					waitSendArea.setStatus(0);
					waitSendArea.setCustomer_id(param.getUnpacking().getCustomer_id());
					othersService.insertLjqWaitSendArea(waitSendArea);
				} 
				affact = 1;
				jsonObject.put("data", "操作成功");
			}else{
				affact = 0;
				jsonObject.put("message", "操作失败");
			}
		}else{
			affact = 0;
			jsonObject.put("message", "操作失败");
		}
		jsonObject.put("status", affact);
		
		ResponseUtil.write(response, jsonObject);
	}
}
