package	com.sdry.web.controller.tdd;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.tdd.Unpacking;
import com.sdry.model.tdd.UnpackingTask;
import com.sdry.service.tdd.UnpackingService;
import com.sdry.service.tdd.UnpackingTaskService;
import com.sdry.utils.DateJsonValueProcessor;
/**
 *
 *@ClassName: UnpackingTaskController
 *@Description: 拆箱任务表
 *@Author tdd
 *@Date 2019-05-14 10:58:34
 *@version 1.0
*/
@Controller
@RequestMapping("/unpackingTask")
public class UnpackingTaskController{
	@Resource UnpackingTaskService unpackingTaskService;
	@Resource UnpackingService unpackingService;
	
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<UnpackingTask> queryAllByMution(UnpackingTask param) {
		List<UnpackingTask> list = unpackingTaskService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(UnpackingTask param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<UnpackingTask> list = unpackingTaskService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = unpackingTaskService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("rows", jsonArray);
		result.put("total", total);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		out.println(result.toString());
		out.flush();
		out.close();
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	/**
	 * @param param
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/insert")
	public void insert(UnpackingTask param, HttpServletResponse response) throws Exception{
		param.setState("0");
		param.setState1("0");
		Long id = unpackingTaskService.insert(param);
		int flg = 0;
		if(null != id){
			Unpacking unpacking = unpackingService.queryById(param.getUid());
			unpacking.setUnum(unpacking.getUnum() - param.getUtnum());
			unpackingService.update(unpacking);
			flg = 1;
		}
		response.getWriter().print(flg);
	}
	
	/** 
	 * 根据主键更新状态 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/updateState")
	public void updateState(Long utid,String state,HttpServletResponse response) throws Exception{
		UnpackingTask param = new UnpackingTask();
		param.setUtid(utid);
		param.setState(state);
		Integer count = unpackingTaskService.updateState(param);
		response.getWriter().print(count);
	}
	
}
