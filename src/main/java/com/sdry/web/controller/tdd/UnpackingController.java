package	com.sdry.web.controller.tdd;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.tdd.Unpacking;
import com.sdry.service.tdd.UnpackingService;
import com.sdry.utils.DateJsonValueProcessor;
/**
 *
 *@ClassName: UnpackingController
 *@Description: 拆箱表
 *@Author tdd
 *@Date 2019-05-14 10:56:27
 *@version 1.0
*/
@Controller
@RequestMapping("/unpacking")
public class UnpackingController{
	@Resource UnpackingService unpackingService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageUnpacking (Model model) {
		return "/td/unpacking";
	}
	
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(Unpacking param,HttpServletResponse response) throws Exception{
		/*
		 * 条件分页查询内容
		 */
		List<Unpacking> list = unpackingService.findPageByMution(param);
		/*
		 * 条件查询总条数
		 */
		Integer total = unpackingService.selectCountByMution(param);
		/*
		 * 转为json数据传至页面
		 */
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor("yyyy-MM-dd HH:mm:ss"));
		//创建result
		JSONObject result = new JSONObject();
		//转json数据
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		//添加入result
		result.put("code", 0);
		result.put("rows", jsonArray);
		result.put("total", total);
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		out.println(result.toString());
		out.flush();
		out.close();
	}
	
}
