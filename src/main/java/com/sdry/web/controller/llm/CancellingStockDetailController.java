package com.sdry.web.controller.llm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.LlmCodemarkReturn;
import com.sdry.model.llm.LlmCancellingStocksDetail;
import com.sdry.model.lz.Materiel;
import com.sdry.service.llm.CancellingStockDetailService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 退库单详情Controller
 * @Title:CancellingStockManager
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年5月15日
 * @version 2.0
 */

@Controller
@RequestMapping("/cancellingStockDetailManager")
public class CancellingStockDetailController {

	@Resource
	private CancellingStockDetailService cancellingStockDetailService;
	
	/**
	 * 分页查询退库单
	 * @Title:listPageCancellingStock  
	 * @param page 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageCancellingStockDetail")
	public void listPageCancellingStockDetail(Page page, LlmCancellingStocksDetail cancellingStockDetail,HttpServletResponse response, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if (cancellingStockDetail.getCancellingNumber() != null && !"".equals(cancellingStockDetail.getCancellingNumber())) {
			map.put("cancellingNumber", cancellingStockDetail.getCancellingNumber());
		} else {
			map.put("cancellingNumber", "");
			cancellingStockDetail.setCancellingNumber("");
		}
		int count = cancellingStockDetailService.getCountCancellingStockDetail(cancellingStockDetail);
		List<LlmCancellingStocksDetail> productList = cancellingStockDetailService.listPageCancellingStockDetail(map);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(productList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace(); 
		}
	}
	
	/**
	 * 新增退库单详情
	 * @param response
	 * @param cancellingNumber 退库单号
	 * @param ids 物料ID拼接字符串
	 * @param source 1：良品库，0：不良品库
	 */
	@RequestMapping("/saveCancellingStockDetail")
	public void saveCancellingStockDetail(HttpServletResponse response, String ids, String cancellingNumber, String source, String warehouseId, String customerId){
		Integer row = cancellingStockDetailService.saveCancellingStockDetail(ids, cancellingNumber, source, warehouseId, customerId);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改退库单
	 * @param response
	 */
	@RequestMapping("/updateCancellingStockDetail")
	public void updateCancellingStockDetail(HttpServletResponse response, LlmCancellingStocksDetail cancellingStockDetail){
		Integer row = cancellingStockDetailService.updateCancellingStockDetail(cancellingStockDetail);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改退库物料数量
	 * @param response
	 */
	@RequestMapping("/updateCancellingStockDetail4")
	public void updateCancellingStockDetail4(HttpServletResponse response, String id, String fieldName, String fieldValue){
		int i = 0;
		if(id != null && !"".equals(id) && fieldName != null && !"".equals(fieldName)  && fieldValue != null && !"".equals(fieldValue)){
			Map<String, Object> map = new HashMap<>(); 
			map.put("id", Long.parseLong(id));
			map.put("fieldName", fieldName);
			map.put("fieldValue", Integer.parseInt(fieldValue));
			i = cancellingStockDetailService.updateCancellingStockDetail4(map);
		}
		try {
			ResponseUtil.write(response, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除退库单
	 * @param response
	 * @param ids 退库单ID拼接的字符串
	 */
	@RequestMapping("/deleteCancellingStockDetail")
	public void deleteCancellingStockDetail(HttpServletResponse response, String ids){
		Integer row = cancellingStockDetailService.deleteCancellingStockDetail(ids);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据物料ID获取库存数量
	 * @param response
	 * @param id 物料ID
	 */
	/*@RequestMapping("/getQuantityByMid")
	public void getQuantityByMid(HttpServletResponse response, String id){
		Map<String, Object> map = new HashMap<>();
		map.put("mid", Long.parseLong(id));
		ZcInventoryInfoEntity info = cancellingStockDetailService.getQuantityByMid(map);
		try {
			ResponseUtil.write(response, info);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * 分页查询物料
	 * @Title:listPageMaterielByCustomerIdAndWarehouseId
	 * @param page 
	 * @param customerId 客户ID
	 * @param source 退库类型
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageMaterielByCustomerIdAndWarehouseId")
	public void listPageMaterielByCustomerIdAndWarehouseId(Page page, String customerId, String source, String materielName, String materielNum, HttpServletResponse response){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if(null != materielName){
			map.put("materielName", materielName);
		}else{
			map.put("materielName", "");
		}
		if(null != materielNum){
			String [] split = materielNum.split(",");
			for(int i = 0; i < split.length; i ++){
				map.put("key"+i, split[i].trim());
			}
		}else{
			map.put("key0", "");
		}
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(null, jsonConfig);
		int count = 0;
		List<Materiel> list = new ArrayList<Materiel>();
		if (customerId != null && !"".equals(customerId) && source != null && !"".equals(source)) {
			map.put("customerId", Long.parseLong(customerId));
			//map.put("warehouseId", Long.parseLong(warehouseId));
			map.put("source", Integer.parseInt(source));
			count = cancellingStockDetailService.getCountMaterielByCustomerIdAndWarehouseId(map);
			list = cancellingStockDetailService.listPageMaterielByCustomerIdAndWarehouseId(map);
		}
		jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据仓库ID分页查询物料
	 * @Title:listPageMaterielByCustomerIdAndWarehouseId
	 * @param page 
	 * @param warehouseId 仓库ID
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageMaterielByWarehouseId")
	public void listPageMaterielByWarehouseId(Page page, String warehouseId, String customerId, HttpServletResponse response){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(null, jsonConfig);
		int count = 0;
		List<Materiel> list = new ArrayList<Materiel>();
		if (warehouseId != null && !"".equals(warehouseId)) {
			map.put("warehouseId", Long.parseLong(warehouseId));
			if(customerId != null && !"".equals(customerId)){
				map.put("customerId", Long.parseLong(customerId));
			}else{
				map.put("customerId", "");
			}
			count = cancellingStockDetailService.getCountMaterielByWarehouseId(map);
			list = cancellingStockDetailService.listPageMaterielByWarehouseId(map);
		}
		jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增条码
	 * @Title:insert   
	 * @param codemark
	 * @param response
	 */
	@RequestMapping("/insert")
	public void insert(LlmCodemarkReturn codemark, HttpServletResponse response){
		Integer row = cancellingStockDetailService.insert(codemark);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据退库单详情ID删除条码
	 * @Title:insert   
	 * @param detailId
	 * @param response
	 */
	@RequestMapping("/deleteByDetailId")
	public void deleteByDetailId(Long detailId, HttpServletResponse response){
		Integer row = cancellingStockDetailService.deleteByDetailId(detailId);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据退库单详情ID查询条码
	 * @param detailId
	 * @param response
	 */
	@RequestMapping("/queryCodemarkById")
	public void queryCodemarkById(Long detailId, HttpServletResponse response){
		List<LlmCodemarkReturn> list = cancellingStockDetailService.queryCodemarkById(detailId);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(list);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
