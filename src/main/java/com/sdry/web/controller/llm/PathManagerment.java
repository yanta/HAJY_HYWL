package com.sdry.web.controller.llm;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.llm.LlmCancellingStockReason;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Warehouse;
import com.sdry.service.llm.CancellingStockReasonService;
import com.sdry.service.llm.CancellingStockService;

/**
 * 路径管理
 * @Title:PathManagerment
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年5月14日
 * @version 1.0
 */
@Controller
@RequestMapping("/path")
public class PathManagerment {

	@Resource
	private CancellingStockService cancellingStockService;
	@Resource
	private CancellingStockReasonService cancellingStockReasonService;
	
	/**
	 * 进入退库单管理
	 * @return
	 */
	@RequestMapping(value="/toCancellingStocksManager")
	public ModelAndView toCancellingStocksManager(){
		ModelAndView mv = new ModelAndView();
		List<Customer> customerList = cancellingStockService.getAllCustomer();
		mv.addObject("customerList", customerList);
		List<LlmCancellingStockReason> reasonList = cancellingStockReasonService.getAllCancellingStockReason();
		mv.addObject("reasonList", reasonList);
		List<Warehouse> allWarehouse = cancellingStockService.getAllWarehouse();
		mv.addObject("warehouseList", allWarehouse);
		mv.setViewName("/llm/CancellingStocksManager4");
		return mv;
	}
	
	/**
	 * 跳转退库原因页面
	 * @return
	 */
	@RequestMapping(value="/toCancellingStockReasonManager")
	public ModelAndView toCancellingStockReasonManager(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/cancellingStockReason");
		return mv;
	}
	
	/**
	 * 跳转退库月报表页面
	 * @return
	 */
	@RequestMapping(value="/toCancellingStockMonthReport")
	public ModelAndView toCancellingStockMonthReport(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/cancellingStockMonthReport");
		return mv;
	}
	
	/**
	 * 跳转退库周报表页面
	 * @return
	 */
	@RequestMapping(value="/toCancellingStockWeekReport")
	public ModelAndView toCancellingStockWeekReport(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/cancellingStockWeekReport");
		return mv;
	}
	
	/**
	 * 跳转出库月报表页面
	 * @return
	 */
	@RequestMapping(value="/toOutStockMonthReport")
	public ModelAndView toOutStockMonthReport(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/outStockMonthReport");
		return mv;
	}
	
	/**
	 * 跳转出库周报表页面
	 * @return
	 */
	@RequestMapping(value="/toOutStockWeekReport")
	public ModelAndView toOutStockWeekReport(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/outStockWeekReport");
		return mv;
	}
	
	/**
	 * 跳转绑码记录
	 * @return
	 */
	@RequestMapping(value="/toCodemark")
	public ModelAndView toCodemark(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/queryCodemark");
		return mv;
	}
	
	/**
	 * 跳转拆箱记录
	 * @return
	 */
	@RequestMapping(value="/toDevaning")
	public ModelAndView toDevaning(){
		ModelAndView mv = new ModelAndView();
		mv.setViewName("/llm/devaning");
		return mv;
	}
}
