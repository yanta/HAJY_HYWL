package com.sdry.web.controller.llm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.DevaningLog;
import com.sdry.model.lz.Materiel;
import com.sdry.service.llm.CancellingStockDetailService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 拆箱记录
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年7月29日
 * @version 1.0
 */

@Controller
@RequestMapping("/devaning")
public class DevaningLogController {

	@Resource
	private CancellingStockDetailService cancellingStockDetailService;
	
	/**
	 * 分页查询拆箱记录
	 * @param page 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/list")
	public void listPageCancellingStockDetail(Page page, String materile_name, String work_time, HttpServletResponse response, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if (materile_name != null && !"".equals(materile_name)) {
			map.put("materiel_name", materile_name);
		} else {
			map.put("materiel_name", "");
		}
		if (work_time != null && !"".equals(work_time)) {
			map.put("work_time", work_time);
		} else {
			map.put("work_time", "");
		}
		int count = cancellingStockDetailService.getCountDevaning(map);
		List<Materiel> list = cancellingStockDetailService.listPageDevaning(map);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据入库单详情ID和拆箱前条码查拆箱后条码
	 * @param detailId
	 * @param response
	 */
	@RequestMapping("/queryCodemarkByNum1")
	public void queryCodemarkByNum1(String num, HttpServletResponse response){
		int count = cancellingStockDetailService.getCountByNum1(num);
		List<DevaningLog> list = cancellingStockDetailService.queryCodemarkByNum1(num);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(list);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据入库单详情ID和拆箱前条码查拆箱后条码
	 * @param detailId
	 * @param response
	 */
	@RequestMapping("/queryCodemarkByNum2")
	public void queryCodemarkByNum2(String num, HttpServletResponse response){
		int count = cancellingStockDetailService.getCountByNum2(num);
		List<DevaningLog> list = cancellingStockDetailService.queryCodemarkByNum2(num);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(list);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增拆箱记录
	 * @param devaning
	 * @param response
	 */
	@RequestMapping("/insert")
	public void insert(DevaningLog devaningLog, HttpServletResponse response){
		int i = cancellingStockDetailService.insertDevaning(devaningLog);
		try {
			ResponseUtil.write(response, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
