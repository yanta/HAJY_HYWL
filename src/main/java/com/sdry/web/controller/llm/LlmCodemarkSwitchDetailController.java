package	com.sdry.web.controller.llm;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import java.io.IOException;
import com.sdry.model.llm.LlmCodemarkSwitchDetail;
import com.sdry.service.llm.LlmCodemarkSwitchDetailService;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: LlmCodemarkSwitchDetailController
 *@Description: 条码类型转换详情
 *@Author llm
 *@Date 2019-08-07 18:53:08
 *@version 1.0
*/
@Controller
@RequestMapping("/LlmCodemarkSwitchDetail")
public class LlmCodemarkSwitchDetailController{
	@Resource LlmCodemarkSwitchDetailService LlmCodemarkSwitchDetailService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageLlmCodemarkSwitchDetail (Model model) {
		return "/llm/LlmCodemarkSwitchDetail";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public LlmCodemarkSwitchDetail queryById(Long id) {
		LlmCodemarkSwitchDetail param = LlmCodemarkSwitchDetailService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<LlmCodemarkSwitchDetail> queryAllByMution(LlmCodemarkSwitchDetail param) {
		List<LlmCodemarkSwitchDetail> list = LlmCodemarkSwitchDetailService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(LlmCodemarkSwitchDetail param,HttpServletResponse response){
		List<LlmCodemarkSwitchDetail> list = LlmCodemarkSwitchDetailService.findPageByMution(param);
		Integer total = LlmCodemarkSwitchDetailService.selectCountByMution(param);
		//转为json数据传至页面
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", total);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(LlmCodemarkSwitchDetail param, HttpServletResponse response){
		Long id = LlmCodemarkSwitchDetailService.insert(param);
		try {
			response.getWriter().print(id);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(LlmCodemarkSwitchDetail param,HttpServletResponse response){
		Integer count = LlmCodemarkSwitchDetailService.update(param);
		try {
			response.getWriter().print(count);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response){
		ids=ids.replace("'",""); 
		Integer count = LlmCodemarkSwitchDetailService.delete(ids);
		try {
			response.getWriter().print(count);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
