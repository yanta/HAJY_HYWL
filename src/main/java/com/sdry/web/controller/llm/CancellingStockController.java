package com.sdry.web.controller.llm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.LlmCancellingStocks;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.llm.CancellingStockService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 退库管理
 * @Title:CancellingStockManager
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年5月14日
 * @version 2.0
 */

@Controller
@RequestMapping("/cancellingStockManager")
public class CancellingStockController {

	@Resource
	private CancellingStockService cancellingStockService;
	
	/**
	 * 分页查询退库单
	 * @Title:listPageCancellingStock  
	 * @param page 
	 * @param cancellingStock 退库单实体类
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageCancellingStock")
	public void listPageCancellingStock(Page page, LlmCancellingStocks cancellingStock,HttpServletResponse response, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if (cancellingStock.getCancellingReason() != null && !"".equals(cancellingStock.getCancellingReason())) {
			map.put("cancellingReason", cancellingStock.getCancellingReason());
		} else {
			map.put("cancellingReason", "");
			cancellingStock.setCancellingReason("");
		}
		int count = cancellingStockService.getCountConcellingStock(cancellingStock);
		List<LlmCancellingStocks> productList = cancellingStockService.listPageConcellingStock(map);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(productList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增退库单
	 * @param response
	 * @param order
	 */
	@RequestMapping("/saveCancellingStock")
	public void saveCancellingStock(HttpServletResponse response, HttpSession session, LlmCancellingStocks cancellingStock){
		cancellingStock.setSource(1);
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		cancellingStock.setUserId(user.getId());
		cancellingStock.setStatus(0);
		Integer row = cancellingStockService.saveConcellingStock(cancellingStock);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改退库单
	 * @param response
	 * @param order
	 */
	@RequestMapping("/updateCancellingStock")
	public void updateCancellingStock(HttpServletResponse response, LlmCancellingStocks cancellingStock){
		cancellingStock.setSource(1);
		Integer row = cancellingStockService.updateConcellingStock(cancellingStock);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除退库单
	 * @param response
	 * @param ids 退库单ID拼接的字符串
	 */
	@RequestMapping("/deleteCancellingStock")
	public void deleteCancellingStock(HttpSession httpSession,String ids,HttpServletResponse response) throws Exception{
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
 
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		String[] arr = ids.split(",");
		
		for (String s : arr) {
			 Map map = new HashMap();
			 map.put("id", Long.valueOf(s));
			map.put("cancellation_time", df.format(new Date()));
			map.put("cancellation", userName);
			Integer affact = cancellingStockService.caceltk(map);
			ResponseUtil.write(response, affact);
		}
	}
	@RequestMapping("/restor")
	public void delet(HttpSession httpSession,String ids,HttpServletResponse response) throws Exception{
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
       
        ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String userName = su.getUserName();
		
		String[] arr = ids.split(",");
		
		for (String s : arr) {
			 Map map = new HashMap();
			 map.put("id", Long.valueOf(s));
			map.put("restorer_time", df.format(new Date()));
			map.put("restorer", userName);
			Integer affact = cancellingStockService.restortk(map);
			ResponseUtil.write(response, affact);
		}
        
	}
	/**
	 * 发送退库单
	 * @param response
	 * @param order
	 */
	@RequestMapping("/sendCancellingStock")
	public void sendCancellingStock(HttpServletResponse response, String roder){
		Integer row = cancellingStockService.sendConcellingStock(roder);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取所有仓库
	 * @param response
	 */
	@RequestMapping("/getAllWarehouse")
	public void getAllWarehouse(HttpServletResponse response){
		List<Warehouse> allWarehouse = cancellingStockService.getAllWarehouse();
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(allWarehouse);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 获取所有不良品库
	 * @param response
	 */
	@RequestMapping("/getAllBadnessWarehouse")
	public void getAllBadnessWarehouse(HttpServletResponse response){
		List<Warehouse> badnessWarehouse = cancellingStockService.getAllBadnessWarehouse();
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(badnessWarehouse);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 根据仓库ID获取管理员信息
	 * @param response
	 */
	@RequestMapping("/getWarehouseById")
	public void getWarehouseById(HttpServletResponse response, String id){
		Warehouse warehouse = cancellingStockService.getWarehouseById(Long.parseLong(id));
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(warehouse);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据客户ID获取联系人信息
	 * @param response
	 */
	@RequestMapping("/getClientById")
	public void getClientById(HttpServletResponse response, String id){
		Customer customer = cancellingStockService.getClientById(Long.parseLong(id));
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(customer);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据仓库ID获取物料信息
	 * @param response
	 */
	@RequestMapping("/getMaterielByWarehouseId")
	public void getMaterielByWarehouseId(HttpServletResponse response, String id){
		List<Materiel> list = cancellingStockService.getMaterielByWarehouseId(Long.parseLong(id));
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(list);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据仓库ID获取供应商信息
	 * @param response
	 */
	@RequestMapping("/getCustomerByWarehouseId")
	public void getCustomerByWarehouseId(HttpServletResponse response, String id){
		List<Customer> list = cancellingStockService.getCustomerByWarehouseId(Long.parseLong(id));
		//将对象转化为json
        JSONArray json = JSONArray.fromObject(list);
		try {
			ResponseUtil.write(response, json);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
