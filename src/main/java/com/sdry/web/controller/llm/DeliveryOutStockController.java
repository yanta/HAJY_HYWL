package	com.sdry.web.controller.llm;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import com.sdry.model.llm.DeliveryOutStock;
import com.sdry.service.llm.DeliveryNodeService;
import com.sdry.service.llm.DeliveryOutStockService;
import com.sdry.utils.ResponseUtil;

/**
 *
 *@ClassName: DeliveryOutStockController
 *@Description: 发货单和出库单的中间表
 *@Author llm
 *@Date 2019-07-17 16:39:44
 *@version 1.0
*/
@Controller
@RequestMapping("/DeliveryOutStock")
public class DeliveryOutStockController{
	
	@Resource 
	DeliveryOutStockService deliveryOutStockService;
	@Resource
	DeliveryNodeService deliveryNodeService;
	
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageDeliveryOutStock (Model model) {
		return "/llm/deliveryOutStock";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public DeliveryOutStock queryById(Long id) {
		DeliveryOutStock param = deliveryOutStockService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<DeliveryOutStock> queryAllByMution(DeliveryOutStock param) {
		List<DeliveryOutStock> list = deliveryOutStockService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(DeliveryOutStock param,HttpServletResponse response){
		//条件分页查询内容
		List<DeliveryOutStock> list = deliveryOutStockService.findPageByMution(param);
		//条件查询总条数
		Integer total = deliveryOutStockService.selectCountByMution(param);
		//转为json数据传至页面
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", total);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(DeliveryOutStock param, String ids, HttpServletResponse response){
		String[] split = ids.split(",");
		int isSuccess = 1;
		//1、根据送货单ID查询出库单ID
		List<DeliveryOutStock> list = deliveryOutStockService.queryAllByMution(param);
		//2、把出库单ID存入set集合，以去重
		Set<Long> set = new HashSet<>();
		for(DeliveryOutStock dos : list){
			set.add(dos.getOutStockId());
		}
		for(String sp : split){
			set.add(Long.parseLong(sp));
		}
		//3、把送货单ID和出库单ID存入中间表
		for(Long id : set){
			param.setOutStockId(id);
			Long i = deliveryOutStockService.insert(param);
			if(i <= 0){
				isSuccess = 0;
				break;
			}
		}
		try {
			ResponseUtil.write(response, isSuccess);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(DeliveryOutStock param,HttpServletResponse response){
		Integer count = deliveryOutStockService.update(param);
		try {
			ResponseUtil.write(response, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids, String deliveryId,HttpServletResponse response){
		ids=ids.replace("'",""); 
		Integer count = deliveryOutStockService.delete(ids, deliveryId);
		try {
			ResponseUtil.write(response, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
