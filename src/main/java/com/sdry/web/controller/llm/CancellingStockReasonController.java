package com.sdry.web.controller.llm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.LlmCancellingStockReason;
import com.sdry.service.llm.CancellingStockReasonService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 退库原因
 * @Title:CancellingReasonManager
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年5月15日
 * @version 1.0
 */

@Controller
@RequestMapping("/cancellingStockReasonManager")
public class CancellingStockReasonController {

	@Resource
	private CancellingStockReasonService cancellingStockReasonService;
	
	/**
	 * 分页查询退库原因
	 * @Title:listPageCancellingStockReason  
	 * @param page 
	 * @param cancellingStockReason 退库原因实体类
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageCancellingStockReason")
	public void listPageCancellingStock(Page page, LlmCancellingStockReason cancellingStockReason,HttpServletResponse response, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if (cancellingStockReason.getCancellingReason() != null && !"".equals(cancellingStockReason.getCancellingReason())) {
			map.put("cancellingReason", cancellingStockReason.getCancellingReason());
		} else {
			map.put("cancellingReason", "");
			cancellingStockReason.setCancellingReason("");
		}
		int count = cancellingStockReasonService.getCountCancellingStockReason(cancellingStockReason);
		List<LlmCancellingStockReason> productList = cancellingStockReasonService.listPageCancellingStockReason(map);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(productList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 新增退库原因
	 * @param response
	 * @param order
	 */
	@RequestMapping("/saveCancellingStockReason")
	public void saveCancellingStockReason(HttpServletResponse response, LlmCancellingStockReason cancellingStockReason){
		Integer row = cancellingStockReasonService.saveCancellingStockReason(cancellingStockReason);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 修改退库原因
	 * @param response
	 * @param order
	 */
	@RequestMapping("/updateCancellingStockReason")
	public void updateCancellingStockReason(HttpServletResponse response, LlmCancellingStockReason cancellingStockReason){
		Integer row = cancellingStockReasonService.updateCancellingStockReason(cancellingStockReason);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除退库原因
	 * @param response
	 * @param ids 退库原因ID拼接的字符串
	 */
	@RequestMapping("/deleteCancellingStockReason")
	public void deleteCancellingStockReason(HttpServletResponse response, String ids){
		Integer row = cancellingStockReasonService.deleteCancellingStockReason(ids);
		try {
			ResponseUtil.write(response, row);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
