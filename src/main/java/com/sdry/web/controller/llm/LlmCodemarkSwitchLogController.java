package	com.sdry.web.controller.llm;
import java.util.List;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.Model;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import com.sdry.model.llm.LlmCodemarkSwitchLog;
import com.sdry.service.llm.LlmCodemarkSwitchLogService;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: LlmCodemarkSwitchLogController
 *@Description: 条码类型转换记录
 *@Author llm
 *@Date 2019-08-08 09:23:39
 *@version 1.0
*/
@Controller
@RequestMapping("/LlmCodemarkSwitchLog")
public class LlmCodemarkSwitchLogController{
	@Resource LlmCodemarkSwitchLogService llmCodemarkSwitchLogService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageLlmCodemarkSwitchLog (Model model) {
		return "/llm/LlmCodemarkSwitchLog";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public LlmCodemarkSwitchLog queryById(Long id) {
		LlmCodemarkSwitchLog param = llmCodemarkSwitchLogService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<LlmCodemarkSwitchLog> queryAllByMution(LlmCodemarkSwitchLog param) {
		List<LlmCodemarkSwitchLog> list = llmCodemarkSwitchLogService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(LlmCodemarkSwitchLog param,HttpServletResponse response){
		List<LlmCodemarkSwitchLog> list = llmCodemarkSwitchLogService.findPageByMution(param);
		Integer total = llmCodemarkSwitchLogService.selectCountByMution(param);
		//转为json数据传至页面
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", total);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(LlmCodemarkSwitchLog param, HttpServletResponse response){
		Long id = llmCodemarkSwitchLogService.insert(param);
		try {
			ResponseUtil.write(response, id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(LlmCodemarkSwitchLog param,HttpServletResponse response){
		Integer count = llmCodemarkSwitchLogService.update(param);
		try {
			ResponseUtil.write(response, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response){
		ids=ids.replace("'",""); 
		Integer count = llmCodemarkSwitchLogService.delete(ids);
		try {
			ResponseUtil.write(response, count);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
