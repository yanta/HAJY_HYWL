package com.sdry.web.controller.llm;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.DevaningWork;
import com.sdry.service.llm.DevaningWorkService;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * 拆箱工作量
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年7月31日
 * @version 1.0
 */

@Controller
@RequestMapping("/devaningWork")
public class DevaningWorkController {

	@Resource DevaningWorkService devaningWorkService;
	
	/**
	 * 根据拆箱记录编号查工作量
	 * @param num	拆箱记录编号
	 * @param response
	 */
	@RequestMapping("/queryWorkByNum")
	public void queryWorkByNum(String num, HttpServletResponse response){
		List<DevaningWork> list = devaningWorkService.queryWorkByNum(num);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(list);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 保存
	 * @param work	拆箱工作记录实体类
	 * @param response
	 */
	@RequestMapping("/insert")
	public void insert(DevaningWork work, HttpServletResponse response){
		Long i = devaningWorkService.insert(work);
		try {
			ResponseUtil.write(response, i);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
