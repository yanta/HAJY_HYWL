package com.sdry.web.controller.llm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.llm.LlmCancellingStocksDetail;
import com.sdry.model.llm.LlmCodemarkReturn;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Materiel;
import com.sdry.service.llm.CancellingStockDetailService;
import com.sdry.utils.Page;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 精准码展示
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年7月26日
 * @version 1.0
 */
@Controller
@RequestMapping("/codemark")
public class CodeMarkController {

	@Resource
	private CancellingStockDetailService cancellingStockDetailService;
	
	/**
	 * 分页查询精准码
	 * @Title:listPageCancellingStock  
	 * @param page 
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value="/listPageCodemark")
	public void listPageCancellingStockDetail(Page page, Materiel materiel, HttpServletResponse response, HttpSession session){
		Map<String, Object> map = new HashMap<>();
		map.put("limit", page.getLimit());
		map.put("page", page.getPage());
		if (materiel.getMateriel_name() != null && !"".equals(materiel.getMateriel_name())) {
			map.put("materiel_name", materiel.getMateriel_name());
		} else {
			map.put("materiel_name", "");
			materiel.setMateriel_name("");
		}
		int count = cancellingStockDetailService.getCountCodemark(materiel);
		List<Materiel> list = cancellingStockDetailService.listPageCodemark(map);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据入库单详情ID查询条码
	 * @param detailId
	 * @param response
	 */
	@RequestMapping("/queryCodemarkById2")
	public void queryCodemarkById(Long detailId, HttpServletResponse response){
		List<CodeMark> list = cancellingStockDetailService.queryCodemarkById2(detailId);
		JSONObject jsonObject = new JSONObject();
		JSONArray jsonArray = JSONArray.fromObject(list);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
