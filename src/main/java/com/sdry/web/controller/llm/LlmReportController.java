package com.sdry.web.controller.llm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sdry.model.lz.Customer;
import com.sdry.model.tdd.ReportUtil;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.llm.CancellingStockDetailService;
import com.sdry.service.llm.LlmReportService;
import com.sdry.utils.ResponseUtil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 报表
 * @Title:ReportController
 * @Package com.sdry.web.controller.llm
 * @author llm
 * @date 2019年5月27日
 * @version 1.0
 */

@Controller
@RequestMapping("/llmReport")
public class LlmReportController {

	@Resource
	private LlmReportService llmReportService;
	@Resource
	private CancellingStockDetailService cancellingStockDetailService;
	
	/**
	 * 退库报表
	 * @Title:getMonthCancellingStockReportByCustomer   
	 * @param response
	 * @param session
	 * @param dateMonth
	 */
	@RequestMapping("/getMonthCancellingStockReportByCustomer")
	public void getMonthCancellingStockReportByCustomer(HttpServletResponse response, HttpSession session, String date, String type){
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customerId = user.getCustomer_id();
		List<ReportUtil> list = new ArrayList<>();
		if(customerId != null && !"".equals(customerId) && type != null && !"".equals(type)){
			//1、判断是否是供应商
			Customer customer = cancellingStockDetailService.getCustomerById(customerId);
			if(customer != null){
				if(date == null || "".equals(date)){
					if(type.equals("month")){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
						date = sdf.format(new Date()) + "-01";
					}else{
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						date = sdf.format(new Date());
					}
				}
				Map<String, Object> map = new HashMap<>();
				map.put("type", type);
				map.put("customer_id", customer.getId());
				map.put("date", date);
				list = llmReportService.getMonthCancellingStockReportByCustomer(map);
			}
		}
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", 0);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 出库报表
	 * @Title:getMonthCancellingStockReportByCustomer   
	 * @param response
	 * @param session
	 * @param dateMonth
	 */
	@RequestMapping("/getOutStockReportByCustomer")
	public void getOutStockReportByCustomer(HttpServletResponse response, HttpSession session, String date, String type){
		ZcSysUserEntity user = (ZcSysUserEntity)session.getAttribute("user");
		Long customerId = user.getCustomer_id();
		List<ReportUtil> list = new ArrayList<>();
		if(customerId != null && !"".equals(customerId) && type != null && !"".equals(type)){
			//1、判断是否是供应商
			Customer customer = cancellingStockDetailService.getCustomerById(customerId);
			if(customer != null){
				if(date == null || "".equals(date)){
					if(type.equals("month")){
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
						date = sdf.format(new Date()) + "-01";
					}else{
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						date = sdf.format(new Date());
					}
				}
				Map<String, Object> map = new HashMap<>();
				map.put("type", type);
				map.put("customer_id", customer.getId());
				map.put("date", date);
				list = llmReportService.getOutStockReportByCustomer(map);
			}
		}
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", 0);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
