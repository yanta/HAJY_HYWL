package	com.sdry.web.controller.barCodeOperation;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sdry.model.barCodeOperation.BarCodeOperation;
import com.sdry.service.barCodeOperation.BarCodeOperationService;
import com.sdry.utils.ResponseUtil;
/**
 *
 *@ClassName: BarCodeOperationController
 *@Description: 条码操作记录
 *@Author tdd
 *@Date 2019-08-07 17:03:56
 *@version 1.0
*/
@Controller
@RequestMapping("/barCodeOperation")
public class BarCodeOperationController{
	@Resource BarCodeOperationService barCodeOperationService;
	/** 
	 * 进入页面
	 * @param model
	 * @return 页面路径
	*/
	@RequestMapping("/page")
	public String pageBarCodeOperation (Model model) {
		return "/td/barCodeOperation";
	}
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	@RequestMapping("/queryById")
	@ResponseBody
	public BarCodeOperation queryById(Long id) {
		BarCodeOperation param = barCodeOperationService.queryById(id);
		return param;
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	@RequestMapping("/queryAllByMution")
	@ResponseBody
	public List<BarCodeOperation> queryAllByMution(BarCodeOperation param) {
		List<BarCodeOperation> list = barCodeOperationService.queryAllByMution(param);
		return list;
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/list")
	public void list(BarCodeOperation param,HttpServletResponse response) throws Exception{
		JSONObject jsonObject = barCodeOperationService.findPageByMution(param);
		ResponseUtil.write(response, jsonObject);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/insert")
	public void insert(BarCodeOperation param, HttpServletResponse response) throws Exception{
		Long id = barCodeOperationService.insert(param);
		ResponseUtil.write(response,id);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @param response
	 * @throws Exception
	*/
	@RequestMapping("/update")
	public void update(BarCodeOperation param,HttpServletResponse response) throws Exception{
		Integer count = barCodeOperationService.update(param);
		ResponseUtil.write(response,count);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	@RequestMapping("/delete")
	public void delete(String ids,HttpServletResponse response) throws Exception{
		ids=ids.replace("'",""); 
		Integer count = barCodeOperationService.delete(ids);
		ResponseUtil.write(response,count);
	}
}
