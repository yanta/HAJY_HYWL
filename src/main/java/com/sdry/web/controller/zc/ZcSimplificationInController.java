package com.sdry.web.controller.zc;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.lz.Customer;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.service.jyy.ReceiveService;
import com.sdry.service.jyy.UpService;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.service.zc.ZcSimplificationInService;
import com.sdry.utils.DateUtil;
import com.sdry.utils.ResponseUtil;

/**
 * 精简入库
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年8月6日 上午11:20:33
 * @Version 1.0
 */
@Controller
@RequestMapping("simplificationIn")
public class ZcSimplificationInController {
	@Resource
	private ZcInventoryManagementService zcInventoryManagementService;
	@Resource
	private ZcSimplificationInService zcSimplificationInService;
	@Resource
	private UpService upService;
	@Resource
	private ReceiveDetailService receiveDetailService;
	@Resource
	private ReceiveService receiveService;
	
	/**
	 * 进入精简入库页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterSimplificationInPage")
	public ModelAndView enterSimplificationInPage(){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		//查询所有供应商
		List<Customer> allCustomerList = zcInventoryManagementService.selectAllCustomerList(zcGeneralQueryEntity);
		ModelAndView mav = new ModelAndView();
		mav.addObject("allCustomerList",allCustomerList);
		mav.setViewName("/zc/warehouse/simplificationIn");
		return mav;
	}
	/**
	 * 新增入库单
	 * @param receive
	 * @param httpServletResponse
	 */
	@RequestMapping("insertSimplificationIn")
	public void insertSimplificationIn(Receive receive, HttpServletResponse response,HttpSession httpSession){
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		receive.setCreateName(su.getId()+"");
		
		receive.setSendDate(receive.getCreateDate());
		
		//此字段已被占用(标识为精简入库)
		//receive.setRemark("-1");
		receive.setIsSimple(1);
		int affact = zcSimplificationInService.insertSimplificationIn(receive);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 新增详情
	 * @param receiveDetail
	 * @param response
	 */
	@RequestMapping("insertDetails")
	public void insertDetails(ReceiveDetail receiveDetail, HttpServletResponse response){
		int affact = zcSimplificationInService.insertDetails(receiveDetail);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 条件查询入库单列表
	 * @param zcGeneralQueryEntity
	 * @param response
	 */
	@RequestMapping("selectSimplificationInList")
	public void selectSimplificationInList(ZcGeneralQueryEntity zcGeneralQueryEntity,HttpServletResponse response){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere(" r.isSimple = 1 and ");
		}
		List<Receive> simplificationInList = zcSimplificationInService.selectSimplificationInList(zcGeneralQueryEntity);
		int count = zcSimplificationInService.countSimplificationInList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(simplificationInList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 批量删除
	 * @param response
	 * @param idArr
	 */
	@RequestMapping("deleteSimplificationInByNumber")
	public void deleteSimplificationInByNumber(HttpServletResponse response, @RequestParam("receiveNumber[]") String[] receiveNumber){
		int affectRow = 0;
		for (int i = 0; i < receiveNumber.length; i++) {
			ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
			String receiveNumberNew  = "'"+receiveNumber[i]+"'";
			zcGeneralQueryEntity.setStrWhere(" receive_number = "+receiveNumberNew);
            affectRow = zcSimplificationInService.deleteSimplificationInByNumber(zcGeneralQueryEntity);
        }
		try {
			ResponseUtil.write(response, affectRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//----------------------------------------------------jyy------------------------------------------------------------
	
	/**
	 * 收货提交(计数)
	 * @param receiveNumber 收货单号
	 * @param response 响应
	 * @throws Exception 异常
	 */
	@RequestMapping("/submitReceipt")
	public void submitReceipt(String receiveNumber, HttpServletResponse response, HttpSession httpSession) throws Exception{
		//初始化影响行数
		int affact1 = 0;
		Long affact = 0L;
		//获取session中的用户信息
		ZcSysUserEntity user = (ZcSysUserEntity)httpSession.getAttribute("user");
		
		//根据收货单号查询收货单明细
		ReceiveDetail param = new ReceiveDetail();
		param.setReceiveNumber(receiveNumber);
		List<ReceiveDetail> receiveDetailList = receiveDetailService.queryAllByMution(param);
		
		//遍历修改
		for (ReceiveDetail receiveDetail : receiveDetailList) {
			/*==================修改收货详细表信息==========*/
			receiveDetail.setCodeNum(receiveDetail.getTotalNum());
			affact1 = receiveDetailService.update(receiveDetail);
			/*====================添加库存=======================*/
			int number = receiveDetail.getCodeNum();
			ZcInventoryInfoEntity zcInventoryInfoEntity = upService.selectByMid2(receiveDetail.getMid() + "", receiveDetail.getPici());
			//1.如果查到
			if (zcInventoryInfoEntity != null) {
				int mNum = zcInventoryInfoEntity.getmNum();
				//1.如果是精准码,只存入总数
				mNum = number + mNum;
				//总数量（个数）
				zcInventoryInfoEntity.setmNum(mNum);
				affact = upService.editStockOnlyCount(zcInventoryInfoEntity);
			} else {
				ZcInventoryInfoEntity zcInventoryInfoEntity1 = new ZcInventoryInfoEntity();
				zcInventoryInfoEntity1.setMid(receiveDetail.getMid());
				zcInventoryInfoEntity1.setmNum(receiveDetail.getCodeNum());
				zcInventoryInfoEntity1.setmBatch(receiveDetail.getPici());
				zcInventoryInfoEntity1.setEnterPerson(user.getId());
				affact = upService.insertStockOnlyCount(zcInventoryInfoEntity1);
			}
		}

		//更改一级状态  --为已收货
		Receive receive = new Receive();
		receive.setReceiveNumber(receiveNumber);
		receive.setIsSimple(1);
		List<Receive> receives = receiveService.allByMution(receive);
		
		if(receives.size()  > 0) {
			Receive receive2 = receives.get(0);
			receive2.setState(1);
			receive2.setReceiveDate(DateUtil.dateFormat1());
			receive2.setReceiveName(user.getId()+"");
			receive2.setSendCompany(null);
			//回收容器数量
			//receive2.setSureName(sure_name);
			receiveService.update(receive2);
		}
		ResponseUtil.write(response, affact);
	}
}
