package com.sdry.web.controller.zc;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.Awarehouse;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.service.zc.ZcRejectsWarehouseService;
import com.sdry.utils.ResponseUtil;

/**
 * 
 * @ClassName:    ZcRejectsWarehouseController   
 * @Description:  不良品库
 * @Author:       zc   
 * @CreateDate:   2019年4月3日 下午2:31:55   
 * @Version:      v1.0
 */
@RequestMapping("rejectsWarehouse")
@Controller
public class ZcRejectsWarehouseController {
	public static final Integer AFFECT_ROW = 0;
	@Resource
	private ZcRejectsWarehouseService zcRejectsWarehouseService;
	@Resource
	private ZcInventoryManagementService zcInventoryManagementService;
	/**
	 * 进入不良品库管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterRejectsWarehousePage")
	public ModelAndView enterRejectsWarehousePage(HttpSession httpSession){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<Customer> allCustomerList = zcInventoryManagementService.selectAllCustomerList(zcGeneralQueryEntity);
		List<Warehouse> allWarehouseList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		ModelAndView mav = new ModelAndView();
		mav.addObject("allWarehouseList",allWarehouseList);
		mav.addObject("allCustomerList",allCustomerList);
		mav.setViewName("/zc/warehouse/rejectsWarehouse");
		return mav;
	}
	/**
	 * 查询不良库列表
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectRejectsWarehouseList")
	public void selectRejectsWarehouseList(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<ZcRejectsWarehouseEntity> rejectsWarehouseList = zcRejectsWarehouseService.selectRejectsWarehouseList(zcGeneralQueryEntity);
		int count = zcRejectsWarehouseService.countRejectsWarehouseList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(rejectsWarehouseList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 不良品入库
	 * @param httpSession
	 * @param response
	 * @param zcRejectsWarehouseEntity
	 */
	@RequestMapping("enterRejects")
	public void enterRejects(HttpSession httpSession,HttpServletResponse response,ZcRejectsWarehouseEntity zcRejectsWarehouseEntity){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		zcRejectsWarehouseEntity.setEnterPerson(su.getId());
		zcRejectsWarehouseEntity.setEnterDate(sdf.format(date));
		int affact = zcRejectsWarehouseService.enterRejects(zcRejectsWarehouseEntity);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/******************************************APP*********************************************/
	/**
	 * 查询不良库列表
	 * @param response
	 * @param mcode
	 */
	@RequestMapping("selectRejectsWarehouseListApp")
	public void selectRejectsWarehouseListApp(HttpServletResponse response){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		zcGeneralQueryEntity.setStrWhere("");
		zcGeneralQueryEntity.setPage(1);
		zcGeneralQueryEntity.setLimit(999999999);
		List<ZcRejectsWarehouseEntity> rejectsWarehouseList = zcRejectsWarehouseService.selectRejectsWarehouseList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
        if(rejectsWarehouseList.size() > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", rejectsWarehouseList);
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "查询数据为空");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
	/**
	 * 查询不良品仓库
	 */
	@RequestMapping("selectRejectWareListApp")
	public void selectRejectWareListApp(HttpServletResponse response,String customer_id){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<Warehouse> zcWareTreeList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		for (Warehouse warehouse : zcWareTreeList) {
			String strWhere = "r.warehouse_id = "+ warehouse.getId() + " and cw.cid = "+customer_id+" and r.region_type=-1 and ";
			zcGeneralQueryEntity.setStrWhere(strWhere);
			List<WarehouseRegion> warehouseRegionList = zcInventoryManagementService.selectWarehouseRegionListByWarehouseId(zcGeneralQueryEntity);
			warehouse.setWarehouseRegion(warehouseRegionList);
		}
		JSONObject jsonObject = new JSONObject();
		if(zcWareTreeList.size() > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", zcWareTreeList);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * 不良品入库
	 * @param response
	 * @param json
	 */
	@RequestMapping("enterRejectsApp")
	public void enterRejectsApp(HttpServletResponse response,String mid,String mBatch,String totality,String describe,String regionId,String type){
		ZcRejectsWarehouseEntity zcRejectsWarehouseEntity= new ZcRejectsWarehouseEntity();
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		zcRejectsWarehouseEntity.setEnterDate(sdf.format(date));
		zcRejectsWarehouseEntity.setMid(Long.parseLong(mid));
		zcRejectsWarehouseEntity.setmBatch(mBatch);
		zcRejectsWarehouseEntity.setTotality(Integer.parseInt(totality));
		zcRejectsWarehouseEntity.setDescribe(describe);
		zcRejectsWarehouseEntity.setRegionId(Long.parseLong(regionId));
		zcRejectsWarehouseEntity.setType(Integer.parseInt(type));
		int affact = zcRejectsWarehouseService.enterRejects(zcRejectsWarehouseEntity);
		JSONObject jsonObject = new JSONObject();
		if(affact > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", "提交成功");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "提交失败");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	@RequestMapping("sponsorRejects2GoodApprove")
	public void sponsorRejects2GoodApprove(HttpServletResponse response,String materiel_code){}
}