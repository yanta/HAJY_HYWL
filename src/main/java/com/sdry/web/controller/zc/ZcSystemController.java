package com.sdry.web.controller.zc;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Department;
import com.sdry.model.lz.Post;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcPermissionEntity;
import com.sdry.model.zc.ZcSysRoleEntity;
import com.sdry.model.zc.ZcSysRolePermissionEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.StockOutOrderService;
import com.sdry.service.zc.ZcSystemService;
import com.sdry.utils.MD5;
import com.sdry.utils.ResponseUtil;

/**
 * 
 * @ClassName:    ZcSystemController   
 * @Description:  系统管理
 * @Author:       zc   
 * @CreateDate:   2019年4月4日 上午9:14:36   
 * @Version:      v1.0
 */
@Controller
@RequestMapping("system")
public class ZcSystemController {

	@Resource
	public ZcSystemService zcSystemService;
	@Resource
	public StockOutOrderService stockOutService;
	/**
	 * 进入Index
	 * @param httpSession
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	@RequestMapping("toIndex")
	public ModelAndView toIndex(HttpSession httpSession,ZcGeneralQueryEntity zcGeneralQueryEntity){
		ModelAndView mav = new ModelAndView();
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		if(su == null){
			mav.setViewName("/zc/login");
		}else{
			//查看用户信息
			ZcSysUserEntity sysUser= zcSystemService.selectSysUserByAccountName(su);
			//查询用户的角色
			List<ZcSysRoleEntity> roleList = zcSystemService.selectRoleListByuserId(sysUser.getId());
			String permissionId ="";
			for (ZcSysRoleEntity zcSysRoleEntity : roleList) {
				//查询每个角色的权限
				List<ZcSysRolePermissionEntity> selectRoleListByuserId = zcSystemService.selectAllRoleOperation2Index(zcSysRoleEntity.getId()+"");
				for (ZcSysRolePermissionEntity zcSysRolePermissionEntity : selectRoleListByuserId) {
					permissionId += zcSysRolePermissionEntity.getPermissionId()+",";
				}
			}
			String strWhere="";
			if(permissionId.length() !=0){
				permissionId = permissionId.substring(0,permissionId.length() - 1);
				//System.out.println(permissionId);
				strWhere = "status = 0 and isdel = 0 and id in ("+ permissionId+") and ";
			}else{
				strWhere = "status = 0 and isdel = 0 and id in ("+ 0+") and ";
			}
			zcGeneralQueryEntity.setStrWhere(strWhere);
			List<ZcPermissionEntity> selectAllMenuList = zcSystemService.selectAllMenuList2Index(zcGeneralQueryEntity);
			if(selectAllMenuList.size()>0){
				httpSession.setAttribute("menuList", selectAllMenuList);
				mav.setViewName("/zc/index");
			}else{
				mav.setViewName("/zc/login");
			}
		}
		
		return mav;
	}
	/**
	 * 进入用户管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterUserPage")
	public ModelAndView enterUserPage(HttpSession httpSession){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<ZcSysRoleEntity> selectAllSysRoleList = zcSystemService.selectAllSysRoleList(zcGeneralQueryEntity);
		List<Department> selectAllDeptList = zcSystemService.selectAllDeptList(zcGeneralQueryEntity);
		List<Customer> customerList = stockOutService.queryAllCustomer();
		ModelAndView mav = new ModelAndView();
		mav.addObject("rList", selectAllSysRoleList);
		mav.addObject("deptList", selectAllDeptList);
		mav.addObject("customerList", customerList);
		mav.setViewName("/zc/system/userList");
		return mav;
	}
	/**
	 * 通过deptID查询岗位
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectAllPostList")
	public void selectAllPostList(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		List<Post> selectAllPostList = zcSystemService.selectAllPostList(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(selectAllPostList, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 进入角色管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterRolePage")
	public ModelAndView enterRolePage(HttpSession httpSession){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/zc/system/roleList");
		return mav;
	}
	/**
	 * 进入权限管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterAuthorityPage")
	public ModelAndView enterAuthorityPage(HttpSession httpSession){
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/zc/system/menuList");
		return mav;
	}
	/**
	 * 查找全部菜单列表
	 * @param response
	 */
	@RequestMapping("selectAllMenuList")
	public void selectAllMenuList(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		List<ZcPermissionEntity> selectAllMenuList = zcSystemService.selectAllMenuList(zcGeneralQueryEntity);
		for (ZcPermissionEntity menu : selectAllMenuList) {
			zcGeneralQueryEntity.setStrWhere("status = 0 and isdel = 0 and pid = "+menu.getId()+" and ");
			List<ZcPermissionEntity> mList = zcSystemService.selectAllMenuList(zcGeneralQueryEntity);
			menu.setmList(mList);
		}
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(selectAllMenuList, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查找菜单是否存在
	 * @param response
	 */
	@RequestMapping("findPermissionByID")
	public void findPermissionByID(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		List<ZcPermissionEntity> selectAllMenuList = zcSystemService.selectAllMenuList(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(selectAllMenuList, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存权限
	 * @param response
	 * @param zcPermissionEntity
	 */
	@RequestMapping("savePermission")
	public void savePermission(HttpServletResponse response,ZcPermissionEntity zcPermissionEntity){
		int isSuccess = 0;
		String type = "";
		if(zcPermissionEntity.getPid() == null){
			zcPermissionEntity.setPid(0L);
		}
		if (zcPermissionEntity.getId() == null || "".equals(zcPermissionEntity.getId())) {
			isSuccess = zcSystemService.insertPermission(zcPermissionEntity);
			type = "_add";
		} else {
			isSuccess = zcSystemService.updatePermission(zcPermissionEntity);
			type = "_edit";
		}
		try {
			ResponseUtil.write(response, isSuccess+type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 条件删除权限
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("deletePermissionById")
	public void deletePermissionById(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		int affact = zcSystemService.deletePermissionById(zcGeneralQueryEntity);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 条件查询用户列表
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectSysUserList")
	public void selectSysUserList(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		
		
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("[user].status = 0 and [user].isdel = 0 and ");
		}
		List<ZcSysUserEntity> selectSysUserList = zcSystemService.selectSysUserList(zcGeneralQueryEntity);
		int count = zcSystemService.countSysUserList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(selectSysUserList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存用户信息
	 * @param response
	 * @param zcSysUserEntity
	 */
	@RequestMapping("saveSysUser")
	public void saveSysUser(HttpServletResponse response,ZcSysUserEntity zcSysUserEntity){
		int count = zcSystemService.selectCountByNumber(zcSysUserEntity.getJobNumber());
		int isSuccess = 0;
		String type = "";
		if (zcSysUserEntity.getId() == null || "".equals(zcSysUserEntity.getId())) {
			if(count > 0){
				isSuccess = -2;
			}else{
				isSuccess = zcSystemService.insertSysUser(zcSysUserEntity);
			}
			type = "_add";
		} else {
			isSuccess = zcSystemService.updateSysUser(zcSysUserEntity);
			type = "_edit";
		}
		try {
			ResponseUtil.write(response, isSuccess+type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除用户
	 * @param response
	 * @param idArr
	 */
	@RequestMapping("deleteUserById")
	public void deleteUserById(HttpServletResponse response,@RequestParam("idArr[]") String[] idArr){
		int affact = 0;
		for (String idstr : idArr) {
			ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
			zcGeneralQueryEntity.setStrWhere("id = " + idstr );
			affact += zcSystemService.deleteUserById(zcGeneralQueryEntity);
		}
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 条件查询角色列表
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectSysRoleList")
	public void selectSysRoleList(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("status = 0 and isdel = 0 and ");
		}
		List<ZcSysRoleEntity> selectSysRoleList = zcSystemService.selectSysRoleList(zcGeneralQueryEntity);
		int count = zcSystemService.countSysRoleList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(selectSysRoleList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存角色信息
	 * @param response
	 * @param zcSysRoleEntity
	 */
	@RequestMapping("saveSysRole")
	public void saveSysRole(HttpServletResponse response,ZcSysRoleEntity zcSysRoleEntity){
		int isSuccess = 0;
		String type = "";
		if (zcSysRoleEntity.getId() == null || "".equals(zcSysRoleEntity.getId())) {
			isSuccess = zcSystemService.insertSysRole(zcSysRoleEntity);
			type = "_add";
		} else {
			isSuccess = zcSystemService.updateSysRole(zcSysRoleEntity);
			type = "_edit";
		}
		try {
			ResponseUtil.write(response, isSuccess+type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 保存权限
	 * @param response
	 * @param roleId
	 * @param ztree
	 */
	@RequestMapping("saveRoleOperation")
	public void saveRoleOperation(HttpServletResponse response,String roleId,String ztree){
		int affact = zcSystemService.saveRoleOperation(roleId,ztree);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询角色的权限
	 * @param roleId
	 */
	@RequestMapping("selectAllRoleOperation")
	public void selectAllRoleOperation(HttpServletResponse response,String roleId){
		List<ZcSysRolePermissionEntity> selectAllRoleOperation = zcSystemService.selectAllRoleOperation(roleId);
		JSONArray jsonArray = JSONArray.fromObject(selectAllRoleOperation);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 核对原密码
	 * @param httpSession
	 * @param response
	 * @param roleId
	 */
	@RequestMapping("checkPsd")
	public void checkPsd(HttpSession httpSession,HttpServletResponse response,String srcPsd){
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String accountName = su.getAccountName();
		int success = zcSystemService.checkPsd(accountName,MD5.md5Encode(srcPsd));
		try {
			ResponseUtil.write(response, success);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 修改
	 * @param httpSession
	 * @param response
	 * @param roleId
	 */
	@RequestMapping("editPsd")
	public void editPsd(HttpSession httpSession,HttpServletResponse response,String newPsd){
		ZcSysUserEntity su = (ZcSysUserEntity)httpSession.getAttribute("user");
		String accountName = su.getAccountName();
		int success = zcSystemService.editPsd(accountName,MD5.md5Encode(newPsd));
		try {
			ResponseUtil.write(response, success);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询用户是否存在
	 */
	@RequestMapping("countUser")
	public void countUser(HttpServletResponse response,String accountName){
		int success = zcSystemService.countUser(accountName);
		try {
			ResponseUtil.write(response, success);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 根据条件查用户
	 * @param response
	 * @param zcSysUserEntity 条件
	 */
	@RequestMapping("selectSysUserByMution")
	public void selectSysUserByMution(HttpServletResponse response,ZcSysUserEntity zcSysUserEntity){
		List<ZcSysUserEntity> list = zcSystemService.selectSysUserByMution(zcSysUserEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(list, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}