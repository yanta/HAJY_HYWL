package com.sdry.web.controller.zc;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcStorehouseEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.zc.ZcBaseService;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.service.zc.ZcSystemService;
import com.sdry.utils.ResponseUtil;

/**
 * 基础模块Controller（zc）
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月31日 上午10:23:10
 * @Version 1.0
 */
@Controller
@RequestMapping("base")
public class ZcBaseController {
	@Resource
	private ZcInventoryManagementService zcInventoryManagementService;
	@Resource
	private ZcSystemService zcSystemService;
	@Resource
	private ZcBaseService zcBaseService;
	/**
	 * 进入库房管理页面
	 * @return
	 */
	@RequestMapping("enterStorehousePage")
	public ModelAndView enterStorehousePage(){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		// 查询所有仓库
		List<Warehouse> allWarehouseList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		// 查询所有人员
		List<ZcSysUserEntity> allSysUserList = zcSystemService.selectAllSysUserList(zcGeneralQueryEntity);
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/zc/base/storehouse");
		mav.addObject("allWarehouseList",allWarehouseList);
		mav.addObject("allSysUserList",allSysUserList);
		return mav;
	}
	/**
	 * 条件查询库房列表
	 * @param zcGeneralQueryEntity
	 * @param response
	 */
	@RequestMapping("selectStorehouseList")
	public void selectStorehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity,HttpServletResponse response){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("s.status = 0 and s.isdel = 0 and ");
		}
		List<ZcStorehouseEntity> storehouseList = zcBaseService.selectStorehouseList(zcGeneralQueryEntity);
		int count = zcBaseService.countStorehouseList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(storehouseList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 保存库房
	 * @param response
	 * @param zcStorehouseEntity
	 */
	@RequestMapping("saveStorehouse")
	public void saveStorehouse(HttpServletResponse response,ZcStorehouseEntity zcStorehouseEntity){
		int isSuccess = 0;
		int isExist = 0;
		String type = "";
		if (zcStorehouseEntity.getId() == null || "".equals(zcStorehouseEntity.getId())) {
			isExist = zcBaseService.selectCodeIsExistByCode(zcStorehouseEntity.getHouse_code());
			if(isExist>0){
				//存在
				isSuccess = -2;
			}else{
				isSuccess = zcBaseService.insertStorehouse(zcStorehouseEntity);
			}
			type = "_add";
		} else {
			isSuccess = zcBaseService.updateStorehouse(zcStorehouseEntity);
			type = "_edit";
		}
		try {
			ResponseUtil.write(response, isSuccess+type);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 批量删除
	 * @param response
	 * @param idArr
	 */
	@RequestMapping("deleteStorehouseById")
	public void deleteStorehouseById(HttpServletResponse response, @RequestParam("idArr[]") String[] idArr){
		int affectRow = 0;
		for (int i = 0; i < idArr.length; i++) {
			ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
			zcGeneralQueryEntity.setStrWhere(" id = "+idArr[i]);
            affectRow = zcBaseService.deleteStorehouseById(zcGeneralQueryEntity);
        }
		try {
			ResponseUtil.write(response, affectRow);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询全部库房
	 * @param response
	 */
	@RequestMapping("selectAllStorehouse")
	public void selectAllStorehouse(HttpServletResponse response){
		List<ZcStorehouseEntity> allStorehouse = zcBaseService.selectAllStorehouse();
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(allStorehouse, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
