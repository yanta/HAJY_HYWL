package com.sdry.web.controller.zc;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.sdry.model.zc.ResponseResult;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.zc.ZcLoginService;

/**
 * 
 * @ClassName:    ZcLoginController   
 * @Description:  登录
 * @Author:       zc   
 * @CreateDate:   2019年4月17日 下午3:30:30   
 * @Version:      v1.0
 */
@ResponseBody
@RequestMapping("login")
@Controller
public class ZcLoginController {
	@Resource
	public ZcLoginService ZcLoginService;
	/**
	 * pc登录
	 * @param session
	 * @param zcSysUserEntity
	 * @return
	 */
	@RequestMapping("pclogin")
	public ResponseResult<Boolean> pclogin(HttpSession session,ZcSysUserEntity zcSysUserEntity){
	    ResponseResult<Boolean> login = ZcLoginService.pclogin(session,zcSysUserEntity);
	    return login;
	}

	/**
	 * pc登出
	 * @param request
	 * @return
	 */
	@RequestMapping("pcloginOut")
	public ModelAndView pcloginOut(HttpServletRequest request){
		Enumeration em = request.getSession().getAttributeNames();
		  while(em.hasMoreElements()){
		   request.getSession().removeAttribute(em.nextElement().toString());
		  }
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/zc/login");
		return mav;
	}
	/**
	 * app登录
	 * @param zcSysUserEntity
	 * @return
	 */
	@RequestMapping("/appLogin")
    public Object appLogin(ZcSysUserEntity zcSysUserEntity){
        Map<String,Object> map =new HashMap<>() ;
        
        map=ZcLoginService.appLogin(zcSysUserEntity);
        
        return map;
    }

}
