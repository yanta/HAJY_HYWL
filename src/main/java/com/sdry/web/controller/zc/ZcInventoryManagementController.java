package com.sdry.web.controller.zc;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.MaterielInStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.service.lz.MaterielService;
import com.sdry.service.zc.ZcInventoryManagementService;
import com.sdry.utils.ResponseUtil;

/**
 * 
 * @ClassName:    ZcWarehouseController   
 * @Description:  库存管理
 * @Author:       zc   
 * @CreateDate:   2019年4月2日 下午5:55:37   
 * @Version:      v1.0
 */
@RequestMapping("inventoryManagement")
@Controller
public class ZcInventoryManagementController {
	public static final Integer AFFECT_ROW = 0;
	@Resource
	private ZcInventoryManagementService zcInventoryManagementService;
	@Resource
	private MaterielService materielService;
	/**
	 * 进入库存管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterInventoryManagement")
	public ModelAndView enterInventoryManagement(HttpSession httpSession){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		//查询所有供应商
		List<Customer> allCustomerList = zcInventoryManagementService.selectAllCustomerList(zcGeneralQueryEntity);
		List<Warehouse> allWarehouseList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		ModelAndView mav = new ModelAndView();
		mav.addObject("allCustomerList",allCustomerList);
		mav.addObject("allWarehouseList",allWarehouseList);
		mav.setViewName("/zc/warehouse/inventoryManagement");
		return mav;
	}
	/**
	 * 进入精简库存管理页面
	 * @param httpSession
	 * @return
	 */
	@RequestMapping("enterInventoryManagementOnlyCount")
	public ModelAndView enterInventoryManagementOnlyCount(HttpSession httpSession){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		//查询所有供应商
		List<Customer> allCustomerList = zcInventoryManagementService.selectAllCustomerList(zcGeneralQueryEntity);
		List<Warehouse> allWarehouseList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		ModelAndView mav = new ModelAndView();
		mav.addObject("allCustomerList",allCustomerList);
		mav.addObject("allWarehouseList",allWarehouseList);
		mav.setViewName("/zc/warehouse/inventoryManagementOnlyCount");
		return mav;
	}
	/**
	 * 查询库存信息
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectInventoryInfoList")
	public void selectInventoryInfoList(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<ZcInventoryInfoEntity> inventoryInfoList = zcInventoryManagementService.selectInventoryInfoList(zcGeneralQueryEntity);
		int count = zcInventoryManagementService.countInventoryInfoList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(inventoryInfoList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询精简库存信息
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectInventoryInfoOnlyCountList")
	public void selectInventoryInfoOnlyCountList(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<ZcInventoryInfoEntity> inventoryInfoList = zcInventoryManagementService.selectInventoryInfoOnlyCountList(zcGeneralQueryEntity);
		int count = zcInventoryManagementService.countInventoryInfoOnlyCountList(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(inventoryInfoList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 查询库存信息详情
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectInventoryInfoListDetails")
	public void selectInventoryInfoListDetails(HttpServletResponse response,ZcGeneralQueryEntity zcGeneralQueryEntity){
		List<ZcMaterielAndTrayEntity> inventoryInfoList = zcInventoryManagementService.selectInventoryInfoListDetails(zcGeneralQueryEntity);
		int count = zcInventoryManagementService.countInventoryInfoListDetails(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(inventoryInfoList, jsonConfig);
		jsonObject.put("code", 0);
		jsonObject.put("data", jsonArray);
		jsonObject.put("count", count);
		try {
			ResponseUtil.write(response, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过仓库id查库区
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectWarehouseRegionListByWarehouseId")
	public void selectWarehouseRegionListByWarehouseId(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<WarehouseRegion> warehouseRegionList = zcInventoryManagementService.selectWarehouseRegionListByWarehouseId(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(warehouseRegionList, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过库区id查库位
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectWarehouseRegionLocationListByRegionId")
	public void selectWarehouseRegionLocationListByRegionId(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<WarehouseRegionLocation> warehouseRegionLocationList = zcInventoryManagementService.selectWarehouseRegionLocationListByRegionId(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(warehouseRegionLocationList, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过产品码或简码查询物料规格型号信息
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectMaterielSizeByCode")
	public void selectMaterielSizeByCode(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<Materiel> materiel = zcInventoryManagementService.selectMaterielSizeByCode(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(materiel, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 通过产品码或简码查询库存信息
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectInventoryInfoByCode")
	public void selectInventoryInfoByCode(HttpServletResponse response, ZcGeneralQueryEntity zcGeneralQueryEntity){
		if(zcGeneralQueryEntity.getStrWhere() == null){
			zcGeneralQueryEntity.setStrWhere("");
		}
		List<ZcInventoryInfoEntity> inventoryInfo = zcInventoryManagementService.selectInventoryInfoByCode(zcGeneralQueryEntity);
		JsonConfig jsonConfig = new JsonConfig();
		JSONArray jsonArray = JSONArray.fromObject(inventoryInfo, jsonConfig);
		try {
			ResponseUtil.write(response, jsonArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 调整
	 * @param response
	 * @param inventId
	 * @param mNum
	 */
	@RequestMapping("adjust")
	public void adjust(HttpServletResponse response, String inventId, String mNum){
		int affact = zcInventoryManagementService.adjust(inventId,mNum);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 库存预警
	 * @param response
	 * @param zcInventoryInfoEntity
	 */
	@RequestMapping("stockWarning")
	public void stockWarning(HttpServletResponse response,ZcInventoryInfoEntity zcInventoryInfoEntity){
		int affact = zcInventoryManagementService.stockWarning(zcInventoryInfoEntity);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * 移库
	 * @param response
	 */
	@RequestMapping("bindingWarehouse")
	public void bindingWarehouse(HttpServletResponse response,Materiel materiel){
		int affact = zcInventoryManagementService.bindingWarehouse(materiel);
		try {
			ResponseUtil.write(response, affact);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/*******************************************APP***********************************************/

	/**
	 * 通过产品码或简码查询物料规格型号信息
	 * @param response
	 * @param mcode 产品码或简码
	 */
	@RequestMapping("selectMaterielSizeByCodeApp")
	public void selectMaterielSizeByCodeApp(HttpServletResponse response, String mcode){
		mcode = "'"+mcode+"'";
		String empty = "''";
		String strWhere = "";
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		strWhere = "(m.materiel_num = "+mcode+" or brevity_num = "+mcode+" or m.devanning_code = "+mcode+") and ";
		zcGeneralQueryEntity.setStrWhere(strWhere);
		List<Materiel> materiel = zcInventoryManagementService.selectMaterielSizeByCode(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		if(materiel.size() > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", materiel);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 通过产品码或简码查询物料规格型号信息2
	 * @param response
	 * @param mcode 产品码或简码
	 * @param specification 物料规格型号
	 */
	@RequestMapping("selectMaterielSizeByCodeApp2")
	public void selectMaterielSizeByCodeApp2(HttpServletResponse response, String mcode, String specification){
		String[] sizeAndProperties = specification.split("/");
		List<MaterielInStock> materiel = zcInventoryManagementService.queryMaterielBatchByMcodeSpecification(mcode, sizeAndProperties[0], sizeAndProperties[1]);
		if (materiel.size() > 0) {
			//根据物料ID去收货详细表查询
			List<ReceiveDetail> receiveDetail = zcInventoryManagementService.queryReceiveDetailByMaterielId(materiel.get(0).getId());
			for (int i = 0; i < receiveDetail.size(); i++) {
				System.out.println(materiel.get(0).getBatch());	
				System.out.println(receiveDetail.get(i).getPici());
				materiel.get(0).getBatch().add(receiveDetail.get(i).getPici()); 
			}
		}
		JSONObject jsonObject = new JSONObject();
		if(materiel.size() > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", materiel);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 通过产品码或简码、规格、型号查询物料信息
	 */
	@RequestMapping("selectMaterielInfoByCodeApp")
	public void selectMaterielInfoByCodeApp(HttpServletResponse response, String json){
		Gson gson = new Gson();
		Materiel materiel= gson.fromJson(json,new TypeToken<Materiel>() {}.getType());
		String materiel_num = "'"+materiel.getMateriel_num()+"'";
		String materiel_size = "'"+materiel.getMateriel_size().split("/")[0]+"'";
		String materiel_properties = "'"+materiel.getMateriel_size().split("/")[1]+"'";
		String empty = "''";
		String strWhere = "";
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		strWhere = "(m.materiel_num = "+materiel_num+" or m.brevity_num = "+materiel_num+" or m.devanning_code = "+materiel_num+") and m.materiel_size = "+materiel_size+" and m.materiel_properties = "+materiel_properties+" and ";
		zcGeneralQueryEntity.setStrWhere(strWhere);
		Materiel materielInfo = zcInventoryManagementService.selectMaterielInfoByCodeApp(zcGeneralQueryEntity);
		JSONObject jsonObject = new JSONObject();
		if(materielInfo!=null){
			jsonObject.put("status", "1");
			jsonObject.put("data", materielInfo);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	/**
	 * 查仓库库区
	 * @param response
	 * @param zcGeneralQueryEntity
	 */
	@RequestMapping("selectWarehouseRegionListApp")
	public void selectWarehouseRegionListApp(HttpServletResponse response, String customer_id){
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		List<Warehouse> zcWareTreeList = zcInventoryManagementService.selectAllWarehouseList(zcGeneralQueryEntity);
		for (Warehouse warehouse : zcWareTreeList) {
			String strWhere = "r.warehouse_id = "+ warehouse.getId() + " and cw.cid = "+customer_id+" and cw.is_empty = 0 and ";
			zcGeneralQueryEntity.setStrWhere(strWhere);
			List<WarehouseRegion> warehouseRegionList = zcInventoryManagementService.selectWarehouseRegionListByWarehouseId(zcGeneralQueryEntity);
			warehouse.setWarehouseRegion(warehouseRegionList);
		}
		JSONObject jsonObject = new JSONObject();
		if(zcWareTreeList.size() > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", zcWareTreeList);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	/**
	 * 移库
	 * @param response
	 * @param json
	 */
	@RequestMapping("appMove")
	public void appMove(HttpServletResponse response,String id,String src_region_id,String region_id){
		Materiel materiel= new Materiel();
		materiel.setId(Long.parseLong(id));
		materiel.setSrc_region_id(Long.parseLong(src_region_id));
		materiel.setRegion_id(Long.parseLong(region_id));
		int affact = zcInventoryManagementService.bindingWarehouse(materiel);
		JSONObject jsonObject = new JSONObject();
		if(affact > AFFECT_ROW){
            jsonObject.put("status", "1");
            jsonObject.put("data", "提交成功");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        } else {
        	jsonObject.put("status", "0");
            jsonObject.put("message", "提交失败");
            try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
	}
	/**
	 * 查询库存预警列表
	 * @param response
	 */
	@RequestMapping("selectStockWarningList")
	public void selectStockWarningList(HttpServletResponse response){
		List<ZcInventoryInfoEntity> selectStockWarningList = zcInventoryManagementService.selectStockWarningList();
		JSONObject jsonObject = new JSONObject();
		if(selectStockWarningList.size() > AFFECT_ROW){
			jsonObject.put("status", "1");
			jsonObject.put("data", selectStockWarningList);
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			jsonObject.put("status", "0");
			jsonObject.put("message", "查询数据为空");
			try {
				ResponseUtil.write(response, jsonObject);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
