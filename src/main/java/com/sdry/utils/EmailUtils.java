package com.sdry.utils;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * 邮件发送
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月9日 下午2:54:37
 * @Version 1.0
 */
public class EmailUtils {

	/**
	 * 发送qq邮件
	 * @param receiveMailbox 收件人邮箱
	 * @param headline 标题
	 * @param content 内容
	 * @throws AddressException
	 * @throws MessagingException
	 */
	public static void sendQQMail(String receiveMailbox, String headline, String content) throws AddressException, MessagingException{
		Properties properties = new Properties();
		properties.put("mail.transport.protocol", "smtp");// 连接协议
		properties.put("mail.smtp.host", "smtp.qq.com");// 主机名
		properties.put("mail.smtp.port", 465);// 端口号
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
		properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
		
		Properties prop = new Properties();
		String mailbox = "";
		String code = "";
		try{
			//读取属性文件sendMailbox.properties
			InputStream in = new BufferedInputStream (new FileInputStream("src/main/resources/sendMailbox.properties"));
			prop.load(in);     ///加载属性列表
			mailbox = prop.getProperty("mailbox");
			code = prop.getProperty("code");
			in.close();
		}catch(Exception e){
			System.out.println(e);
		}
		// 得到回话对象
		Session session = Session.getInstance(properties);
		// 获取邮件对象
		Message message = new MimeMessage(session);
		// 设置发件人邮箱地址
		message.setFrom(new InternetAddress(mailbox));
		// 设置收件人邮箱地址 
		//message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress("xxxxx@qq.com"),new InternetAddress("xxxxx@qq.com"),new InternetAddress("xxxxx@qq.com")});
		message.setRecipient(Message.RecipientType.TO, new InternetAddress(receiveMailbox));//一个收件人
		// 设置邮件标题
		message.setSubject(headline);
		// 设置邮件内容
		message.setText(content);
		// 得到邮差对象
		Transport transport = session.getTransport();
		// 连接自己的邮箱账户
		transport.connect(mailbox, code);// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码
		// 发送邮件
		transport.sendMessage(message, message.getAllRecipients());
		transport.close();
	} 
}
