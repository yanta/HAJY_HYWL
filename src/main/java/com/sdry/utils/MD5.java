package com.sdry.utils;
import java.security.MessageDigest;
/**
 * 
 * @ClassName MD5
 * @Description MD5工具加密
 * @Author lz
 * @Date 2018年9月10日 15:29:30
 * @Version 1.0
 */
public class MD5 {

	public static String md5Encode(String inStr) {

		 MessageDigest md5 = null;
	        try{
	            md5 = MessageDigest.getInstance("MD5");
	        }catch (Exception e){
	            System.out.println(e.toString());
	            e.printStackTrace();
	            return "";
	        }
	        char[] charArray = inStr.toCharArray();
	        byte[] byteArray = new byte[charArray.length];
	 
	        for (int i = 0; i < charArray.length; i++)
	            byteArray[i] = (byte) charArray[i];
	        byte[] md5Bytes = md5.digest(byteArray);
	        StringBuffer hexValue = new StringBuffer();
	        for (int i = 0; i < md5Bytes.length; i++){
	            int val = ((int) md5Bytes[i]) & 0xff;
	            if (val < 16)
	                hexValue.append("0");
	            hexValue.append(Integer.toHexString(val));
	        }
	        return hexValue.toString();

	}

	/**
	 * 测试主函数
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) {
		String strs = new String("123");
		System.out.println("原始：" + strs);
		System.out.println("MD5后：" + md5Encode(strs));
	}
}
