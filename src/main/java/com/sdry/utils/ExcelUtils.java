package com.sdry.utils;

import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	/**
	 * 导出模板
	 * 
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static void exportFile(String title, List<String> list,
			HttpServletResponse response) throws UnsupportedEncodingException {
		Date now = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");// 可以方便地修改日期格式
		String nowtime = dateFormat.format(now);
		// 第一步，创建一个webbook，对应一个Excel文件
		HSSFWorkbook wb = new HSSFWorkbook();
		// 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet
		HSSFSheet sheet = wb.createSheet(title + nowtime);
		// 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short
		HSSFRow row = sheet.createRow((int) 0);
		// 第四步，创建单元格，并设置值表头 设置表头居中
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式
		HSSFCell cell;

		if (list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				cell = row.createCell((short) i);
				cell.setCellValue(list.get(i));
				cell.setCellStyle(style);
			}

		}
		list = null;
		// 第六步，将文件存到指定位置
		String fileName = title + nowtime + ".xls";
		fileName = new String(fileName.getBytes("GBK"), "iso8859-1");
		try {

			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ fileName);// 指定下载的文件名
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			response.setDateHeader("Expires", 0);
			OutputStream output = response.getOutputStream();
			BufferedOutputStream bufferedOutPut = new BufferedOutputStream(
					output);
			bufferedOutPut.flush();
			wb.write(bufferedOutPut);
			bufferedOutPut.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 读取内容
	public static List<List<String>> readXls(String filePath,
			HttpServletRequest request, int num) throws IOException {
		List<List<String>> lists = new ArrayList();
		// 创建 Excel 文件的输入流对象
		FileInputStream excelFileInputStream = new FileInputStream(filePath);
		// XSSFWorkbook 就代表一个 Excel 文件
		// 创建其对象，就打开这个 Excel 文件
		HSSFWorkbook workbook = new HSSFWorkbook(excelFileInputStream);
		// 输入流使用后，及时关闭！这是文件流操作中极好的一个习惯！
		excelFileInputStream.close();
		// XSSFSheet 代表 Excel 文件中的一张表格
		// 我们通过 getSheetAt(0) 指定表格索引来获取对应表格
		// 注意表格索引从 0 开始！
		HSSFSheet sheet = workbook.getSheetAt(0);
		// 步骤三 ： 循环读取表格并输出其内容
		// 开始循环表格数据,表格的行索引从 0 开始
		// employees.xlsx 第一行是标题行，我们从第二行开始, 对应的行索引是 1
		// sheet.getLastRowNum() : 获取当前表格中最后一行数据对应的行索引
		// 获得总列数 int coloumNum=sheet.getRow(0).getPhysicalNumberOfCells();
		// 获得总行数 int rowNum=sheet.getLastRowNum();
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			// XSSFRow 代表一行数据
			HSSFRow row = sheet.getRow(rowIndex);
			if (row == null) {
				continue;
			}
			List<String> list = new ArrayList<String>();
			for (int i = 0; i < num; i++) {
				String cellStr = null;
				if (null != row.getCell(i)) {
					int dataFormat = row.getCell(i).getCellStyle()
							.getDataFormat();
					if (dataFormat == 14 || dataFormat == 176
							|| dataFormat == 178 || dataFormat == 180
							|| dataFormat == 181 || dataFormat == 182) {
						/*
						 * cellStr =
						 * ReadExcleUtils.getDateValue2007(row.getCell(i));
						 */
						Date theDate = row.getCell(i).getDateCellValue();
						SimpleDateFormat dff = new SimpleDateFormat(
								"yyyy-MM-dd");
						if(theDate != null) {
							cellStr = dff.format(theDate);
						}
					} else {
						switch (row.getCell(i).getCellType()) {
						case HSSFCell.CELL_TYPE_NUMERIC:// 数值
							BigDecimal db = new BigDecimal(row.getCell(i)
									.getNumericCellValue());
							
							if (db.toString().indexOf(".") != -1) {
								java.text.DecimalFormat dfomat = new java.text.DecimalFormat(
										"0.000000");
								cellStr = dfomat.format(db);
							} else {
								cellStr = db.toPlainString();
							}
							break;
						case HSSFCell.CELL_TYPE_STRING:// 字符串
							cellStr = row.getCell(i).getStringCellValue();
							break;
						case HSSFCell.CELL_TYPE_BOOLEAN:// 布尔
							cellStr = String.valueOf(row.getCell(i)
									.getBooleanCellValue());
							break;
						case HSSFCell.CELL_TYPE_BLANK:// 空值
							cellStr = "";
							break;
						default:
							//jyy
							row.getCell(i).setCellType(Cell.CELL_TYPE_STRING);
							//jyy
							cellStr = row.getCell(i).getStringCellValue();
							break;
						}
					}
				} else {
					cellStr = null;
				}
				list.add(i, cellStr);
			}
			lists.add(list);
			list = null;
		}
		// 操作完毕后，记得要将打开的 XSSFWorkbook 关闭
		workbook.close();
		return lists;
	}

	// 读取内容
	public static List<List<String>> readXlsx(String filePath,
			HttpServletRequest request, int num) throws IOException {
		List<List<String>> lists = new ArrayList();
		// 创建 Excel 文件的输入流对象
		FileInputStream excelFileInputStream = new FileInputStream(filePath);
		// XSSFWorkbook 就代表一个 Excel 文件
		// 创建其对象，就打开这个 Excel 文件
		XSSFWorkbook workbook = new XSSFWorkbook(excelFileInputStream);
		// 输入流使用后，及时关闭！这是文件流操作中极好的一个习惯！
		excelFileInputStream.close();
		// XSSFSheet 代表 Excel 文件中的一张表格
		// 我们通过 getSheetAt(0) 指定表格索引来获取对应表格
		// 注意表格索引从 0 开始！
		XSSFSheet sheet = workbook.getSheetAt(0);
		// 步骤三 ： 循环读取表格并输出其内容
		// 开始循环表格数据,表格的行索引从 0 开始
		// employees.xlsx 第一行是标题行，我们从第二行开始, 对应的行索引是 1
		// sheet.getLastRowNum() : 获取当前表格中最后一行数据对应的行索引
		// 获得总列数 int coloumNum=sheet.getRow(0).getPhysicalNumberOfCells();
		// 获得总行数 int rowNum=sheet.getLastRowNum();
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			// XSSFRow 代表一行数据
			XSSFRow row = sheet.getRow(rowIndex);
			if (row == null) {
				continue;
			}
			List<String> list = new ArrayList<String>();
			/*************************************************************/
			for (int i = 0; i < num; i++) {
				/*************************************************************/
				String cellStr = null;
				if (null != row.getCell(i)) {
					switch (row.getCell(i).getCellType()) {
					case HSSFCell.CELL_TYPE_NUMERIC:// 数值
						int dataFormat = row.getCell(i).getCellStyle()
								.getDataFormat();
						if (dataFormat == 14 || dataFormat == 176
								|| dataFormat == 178 || dataFormat == 180
								|| dataFormat == 181 || dataFormat == 182) {
							/*
							 * Date theDate = row.getCell(i).getDateCellValue();
							 * cellStr = dff.format(theDate);
							 */
							/*
							 * SimpleDateFormat dff = new
							 * SimpleDateFormat("yyyy-MM-dd"); double value =
							 * row.getCell(i).getNumericCellValue(); Date date =
							 * org
							 * .apache.poi.ss.usermodel.DateUtil.getJavaDate(value
							 * ); cellStr = dff.format(date);
							 */
							cellStr = Double.toString(row.getCell(i)
									.getNumericCellValue());
						} else {
							BigDecimal db = new BigDecimal(row.getCell(i)
									.getNumericCellValue());
							if (db.toString().indexOf(".") != -1) {
								java.text.DecimalFormat dfomat = new java.text.DecimalFormat(
										"0.000000");
								cellStr = dfomat.format(db);
							} else {
								cellStr = db.toPlainString();
							}
						}
						break;
					case HSSFCell.CELL_TYPE_STRING:// 字符串
						cellStr = row.getCell(i).getStringCellValue();
						break;
					case HSSFCell.CELL_TYPE_BOOLEAN:// 布尔
						cellStr = String.valueOf(row.getCell(i)
								.getBooleanCellValue());
						break;
					case HSSFCell.CELL_TYPE_BLANK:// 空值
						cellStr = "";
						break;
					default:
						cellStr = row.getCell(i).getStringCellValue();
						break;
					}
				} else {
					cellStr = null;
				}
				list.add(i, cellStr);
			}
			lists.add(list);
			list = null;
		}
		// 操作完毕后，记得要将打开的 XSSFWorkbook 关闭
		workbook.close();
		return lists;
	}
	

 	public static void export(String title, List<String> list,
			HttpServletResponse response,List<List<String>> lists) throws UnsupportedEncodingException{
 		// 第一步，创建一个webbook，对应一个Excel文件  
         HSSFWorkbook wb = new HSSFWorkbook();  
         // 第二步，在webbook中添加一个sheet,对应Excel文件中的sheet  
          HSSFSheet sheet = wb.createSheet(title);  
         // 第三步，在sheet中添加表头第0行,注意老版本poi对Excel的行数列数有限制short  
         HSSFRow row = sheet.createRow((int) 0);  
         // 第四步，创建单元格，并设置值表头 设置表头居中  
         HSSFCellStyle style = wb.createCellStyle();  
         style.setAlignment(HSSFCellStyle.ALIGN_CENTER); // 创建一个居中格式 
           
         HSSFCell cell;
         if (list.size() > 0) {
 			for (int i = 0; i < list.size(); i++) {
 				cell = row.createCell((short) i);
 				cell.setCellValue(list.get(i));
 				cell.setCellStyle(style);
 			}

 		} 
         
        for (int i = 0; i < lists.size(); i++)
         {	 
        	 row = sheet.createRow((int) i + 1);
        	 List<String> listob = lists.get(i);
        	 for (int j = 0; j < listob.size(); j++) {
        		 row.createCell((short) j).setCellValue((listob.get(j)));
			}
            
         }
         
         list = null;
         lists = null;
      // 第六步，将文件存到指定位置  
         Date now = new Date(); 
         SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");//可以方便地修改日期格式 
       String nowtime = dateFormat.format(now); 
      
       String fileName = title+nowtime +".xls";
       fileName = new String(fileName.getBytes("GBK"),"iso8859-1");
       try  
         { 
 	        response.reset();   
 	        response.setHeader("Content-Disposition","attachment;filename="+fileName);//指定下载的文件名   
 	        response.setContentType("application/vnd.ms-excel");   
 	        response.setHeader("Pragma", "no-cache");   
 	        response.setHeader("Cache-Control", "no-cache");   
 	        response.setDateHeader("Expires", 0);   
 	        OutputStream output = response.getOutputStream();   
 	        BufferedOutputStream bufferedOutPut = new BufferedOutputStream(output);   
 	        bufferedOutPut.flush();   
             wb.write(bufferedOutPut);   
             bufferedOutPut.close();   
         }
         catch (Exception e)
         {
             e.printStackTrace();  
         } 
 	}	
}
