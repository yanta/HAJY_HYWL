package com.sdry.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
/**
 * 
 * @ClassName:BeanReflexUtil
 * @Description:实体类反射
 * @Author tdd
 * @Date 2019年1月10日下午2:58:33
 * @version 1.0
 */
public class BeanReflexUtil {
	public static Object getBean(String className) throws Exception {
		Class cls = null;
		try {
			cls = Class.forName(className);// 对应Spring ->bean -->class
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new Exception("类错误！");
		}

		Constructor[] cons = null;// 得到所有构造器
		try {
			cons = cls.getConstructors();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("构造器错误！");
		}
		if (cons == null || cons.length < 1) {
			throw new Exception("没有默认构造方法！");
		}
		// 如果上面没错，就有构造方法

		Constructor defCon = cons[0];// 得到默认构造器,第0个是默认构造器，无参构造方法
		Object obj = defCon.newInstance();// 实例化，得到一个对象 //Spring - bean -id
		return obj;
	}

	public static void setProperty(Object bean, String propertyName,
			Object propertyValue) throws Exception {
		Class cls = bean.getClass();
		Method[] methods = cls.getMethods();// 得到所有方法
		// cls.getFields();//所有公开字段属性
		// 注入属性 用户名：admin setUsername();
		// obj username admin
		// String propertyName = "username";//对应 Spring配置文件- property ->name
		// String propertyValue = "admin";//对应：Spring -- property -->ref/value
		for (Method m : methods) {
			if (m.getName().equalsIgnoreCase("set" + propertyName)) {
				// 找到方法就注入
				m.invoke(bean, propertyValue);
				break;
			}
		}
	}
}
