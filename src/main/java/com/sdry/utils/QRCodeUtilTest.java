package com.sdry.utils;

import java.io.File;
import java.io.FileOutputStream;

public class QRCodeUtilTest {

    public static void getRcode(String content) {
        String dir = RootPath.getRootPath("ATTACH_ROOT");
        String logoImgPath = dir + "40.png";
        File file = new File(logoImgPath);
        try {
            QRCodeUtil.encode(content, null, new FileOutputStream(file), true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}