package com.sdry.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @ClassName RootPath
 * @Description 获取配置文件路径
 * @Author lz
 * @Date 2018年7月12日 09:47:04
 * @Version 1.0
 */
public class RootPath {
    /**
     * 获取项目根目录
     * @param key
     * @return
     */
    public static String getRootPath(String key){
        Properties prop = new Properties();
        try {
            InputStream inputStream = new RootPath().getClass().getClassLoader().getResourceAsStream("pathconfig.properties");
            // 装载配置文件
            prop.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 返回获取的值
        return prop.getProperty(key);
    }
}
