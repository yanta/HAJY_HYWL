package com.sdry.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.service.llm.CancellingStockDetailService;

/**
 * 减库存
 * @Title:InventoryReduction
 * @Package com.sdry.utils
 * @author llm
 * @date 2019年6月1日
 * @version 1.0
 */
public class InventoryReduction {

	@Resource
	CancellingStockDetailService cancellingStockDetailService;
	
	/**
	 * 减物料良品库的库存
	 * @param materielId 物料ID
	 * @param materielQuantity	物料数量
	 */
	public void inventoryReduction(Long materielId, Integer materielQuantity){
		//根据物料ID查良品库的库存
		List<ZcInventoryInfoEntity> list = cancellingStockDetailService.getInventoryByMid(materielId);
		//遍历减库存（1个物料ID多条库存记录，减后为0的，删除该记录）
		for(ZcInventoryInfoEntity info : list){
			System.out.println("物料ID="+materielQuantity+":  库存数量="+info.getmNum());
			materielQuantity = materielQuantity - info.getmNum();
			//如果物料数量大于库存
			if(materielQuantity > 0){
				//删除这条数据
				cancellingStockDetailService.deleteInventoryById(info.getId());
			}else{
				//修改库存数量
				Map<String, Object> map = new HashMap<>();
				map.put("id", info.getId());
				map.put("fieldName", "mNum");
				map.put("fieldValue", Math.abs(materielQuantity));
				cancellingStockDetailService.updateInventory(map);
				break;
			}
		}
	}
	
	/**
	 * 减不良品库存
	 * @Title:rejectsStockReduction   
	 * @param materielId
	 * @param materielQuantity
	 */
	public void rejectsStockReduction(Long materielId, Integer materielQuantity){
		List<ZcRejectsWarehouseEntity> list = cancellingStockDetailService.queryRejectsByMaterielId(materielId);
		for(ZcRejectsWarehouseEntity rejects : list){
			materielQuantity = materielQuantity - rejects.getTotality();
			//如果物料数量大于库存
			if(materielQuantity > 0){
				//删除这条数据
				cancellingStockDetailService.deleteRejectsWarehouse(rejects.getId());
			}else{
				//修改库存数量
				Map<String, Object> map = new HashMap<>();
				map.put("id", rejects.getId());
				map.put("fieldName", "totality");
				map.put("fieldValue", Math.abs(materielQuantity));
				cancellingStockDetailService.updateRejectsWarehouse(map);
				break;
			}
		}
	}
	
}
