package com.sdry.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @ClassName RequestResult
 * @Description Request工具类
 * @Author lz
 * @Date 2018年9月10日 15:27:14
 * @Version 1.0
 */
public class RequestResult {

	
	public static Map<String, Object> requestResultLong(long effect) {
 		Map<String,Object> map = new HashMap<String, Object>(16);
         
         if (effect > 0) {
             map.put("success",true);

         }else {
             map.put("success",false);

         }
 		return map;
 	}
	
	
	public static Map<String, Object> requestResultInt(int effect) {
 		Map<String,Object> map = new HashMap<String, Object>(16);
         
         if (effect > 0) {
             map.put("success",true);

         }else {
             map.put("success",false);

         }
 		return map;
 	}
	
	/**
	 * 手持端
	 * @param <T>
	 * @param insert
	 * @return
	 */
	public static <T> Map<String, Object> responsePhone(int insert,T t) {
		
		Map<String, Object> map = new HashMap();
		
		if (insert > 0) {
			
			map.put("status", 1);
			map.put("message", 13);
			map.put("data", t);
			
		}else {
			map.put("status", 0);
			map.put("message", 13);
			map.put("data", t);
		}
		
		return map;
	}
	
	
}
