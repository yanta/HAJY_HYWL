package com.sdry.utils;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @ClassName ResponseUtil
 * @Description Response工具类
 * @Author lz
 * @Date 2018年9月10日 15:27:14
 * @Version 1.0
 */
public class ResponseUtil {

	public static void write(HttpServletResponse response,Object o)throws Exception{
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		if(o != null) {
			out.println(o.toString());
		}else {
			out.println("");
		}
		out.flush();
		out.close();
	}
}
