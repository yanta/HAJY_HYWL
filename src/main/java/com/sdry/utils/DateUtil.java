package com.sdry.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @ClassName:    [DateUtil]   
 * @Description:  [日期工具类]   
 * @Author:       [jyy]   
 * @CreateDate:   [2018年12月15日 下午4:26:18]    
 * @Version:      [v1.0]
 */
public class DateUtil {
	
	
	/**
	 * 获取当前时间， 并格式化
	 * @return yyyy-MM-dd
	 */
	public static String dateFormat1() {
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String format = dateFormat.format(date);
		return format;
	}
	
	
	/**
	 * 获取当前时间， 并格式化
	 * @return yyyy-MM-dd HH:mm
	 */
	public static String dateFormat2() {
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String format = dateFormat.format(date);
		return format;
	}
	/**
	 * 获取当前时间， 并格式化
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String dateFormat3() {
		
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = dateFormat.format(date);
		return format;
	}

	/**
	 * 获取当前时间，格式化 
	 * @return 格式化后的日期  String类型    yyyy年MM月dd日 HH:mm:ss
	 */
	public static String format() {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");
		String format = dateFormat.format(date);
		return format;
	}
	
	/**
	 * 获取当前时间，格式化
	 * @return 格式化后的日期  Date 类型   yyyy-MM-dd HH:mm:ss
	 * @throws ParseException 
	 */
	public static Date format2() throws ParseException {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String format = dateFormat.format(date);
		Date parse = dateFormat.parse(format);
		return parse;
	}
	
	/**
	 * 计算两个时间的差
	 * @param now 后一个时间
	 * @param starttime 前一个时间
	 * @return 两个时间相差  天、小时、分钟
	 * @throws ParseException
	 */
	public static String diff(String now, String starttime) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Date now2 = format.parse(now);
		Date starttime2 = format.parse(starttime);
		long nd = 1000 * 24 * 60 * 60;
	    long nh = 1000 * 60 * 60;
	    long nm = 1000 * 60;
	    // long ns = 1000;
	    // 获得两个时间的毫秒时间差异
	    long diff = now2.getTime() - starttime2.getTime();
	    // 计算差多少天
	    long day = diff / nd;
	    // 计算差多少小时
	    long hour = diff % nd / nh;
	    // 计算差多少分钟
	    long min = diff % nd % nh / nm;
	    // 计算差多少秒//输出结果
	    // long sec = diff % nd % nh % nm / ns;
	    return day + "天" + hour + "小时" + min + "分钟";
	}
	
	
	/**
	 * 计算两个时间的差
	 * @param now 后一个时间
	 * @param starttime 前一个时间
	 * @return 两个时间相差小时
	 * @throws ParseException
	 */
	public static long diff2(String now, String starttime) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		Date now2 = format.parse(now);
		Date starttime2 = format.parse(starttime);
		long nh = 1000 * 60 * 60;
		// long ns = 1000;
		// 获得两个时间的毫秒时间差异
		long diff = now2.getTime() - starttime2.getTime();
		// 计算差多少小时
		long hour = diff/ nh;
		return  hour;
	}
	
	public static int getDays(String str){
		int days = 0;
		String month = str.substring(5, 7);
		Integer year = Integer.parseInt(str.substring(0, 4));
		switch(month){
		case "01":
			days = 31;
			break;
		case "02":
			if((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0 && year % 100 == 0)){
				days = 29;
				break;
			}else{
				days = 28;
				break;
			}
		case "03":
			days = 31;
			break;
		case "04":
			days = 30;
			break;
		case "05":
			days = 31;
			break;
		case "06":
			days = 30;
			break;
		case "07":
			days = 31;
			break;
		case "08":
			days = 31;
			break;
		case "09":
			days = 30;
			break;
		case "10":
			days = 31;
			break;
		case "11":
			days = 30;
			break;
		case "12":
			days = 31;
			break;
		}
		return days;
	}
	
	/**
	 * 计算时间差
	 * @throws ParseException 
	 */
	public static Long workHourse(String startTime, String endTime) throws ParseException{
		if(null != startTime && null != endTime && !"".equals(startTime) && !"".equals(endTime)){
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date login = sdf.parse(startTime);
			Date logout = sdf.parse(endTime);
			return logout.getTime() - login.getTime();
		}else{
			return 0L;
		}
	}
	
	/**
	 * 根据毫秒计算时分秒
	 */
	public static String time(Long number){
		Long hourse = number/1000/3600;
		Long minute = (number%(3600*1000))/1000/60;
		Long second = (number%(3600*1000*60))%(1000*60)/1000;
		return hourse + "时" + minute + "分" + second + "秒";
	}
	
	/**
	 * 时分秒转换毫秒 格式  hourse + "时" + minute + "分" + second + "秒";
	 * @param time
	 * @return
	 */
	public static Long sss(String time){
		int i = time.indexOf("时");
		int i2 = time.indexOf("分");
		int i3 = time.indexOf("秒");
		String hourse = time.substring(0, i);
		String minute = time.substring(i+1, i2);
		String second = time.substring(i2+1, i3);
		Long h = Long.parseLong(hourse)*3600000;
		Long m = Long.parseLong(minute)*60000;
		Long s = Long.parseLong(second)*1000;
		return h+m+s; 
	}
	
	/**
     *  java 获取 获取某年某月 所有日期（yyyy-mm-dd格式字符串）
     * @param year
     * @param month
     * @return
     */
    public static List<String> getMonthFullDayByYearAndMonth(int year , int month){
        SimpleDateFormat dateFormatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        List<String> fullDayList = new ArrayList<>(32);
        // 获得当前日期对象
        Calendar cal = Calendar.getInstance();
        cal.clear();// 清除信息
        cal.set(Calendar.YEAR, year);
        // 1月从0开始
        cal.set(Calendar.MONTH, month-1 );
        // 当月1号
        cal.set(Calendar.DAY_OF_MONTH,1);
        int count = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int j = 1; j <= count ; j++) {
            fullDayList.add(dateFormatYYYYMMDD.format(cal.getTime()));
            cal.add(Calendar.DAY_OF_MONTH,1);
        }
        return fullDayList;
    }
    
    /**
	 * 获得指定日期的后一天
	 * 
	 * @param specifiedDay
	 * @return
	 */
	public static String getSpecifiedDayAfter(String specifiedDay) {
		Calendar c = Calendar.getInstance();
		Date date = null;
		try {
			date = new SimpleDateFormat("yy-MM-dd").parse(specifiedDay);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		c.setTime(date);
		int day = c.get(Calendar.DATE);
		c.set(Calendar.DATE, day + 1);

		String dayAfter = new SimpleDateFormat("yyyy-MM-dd")
				.format(c.getTime());
		return dayAfter;
	}
}
