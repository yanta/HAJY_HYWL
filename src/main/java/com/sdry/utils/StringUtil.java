package com.sdry.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @ClassName StringUtil
 * @Description 字符串工具类
 * @Author lz
 * @Date 2018年9月10日 15:27:41
 * @Version 1.0
 */
public class StringUtil {

	/**
	 * 判断是否是空
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		if(str==null||"".equals(str.trim())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 判断是否不是空
	 * @param str
	 * @return
	 */
	public static boolean isNotEmpty(String str){
		if((str!=null)&&!"".equals(str.trim())){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * 格式化模糊查询
	 * @param str
	 * @return
	 */
	public static String formatLike(String str){
		if(isNotEmpty(str)){
			return "%"+str+"%";
		}else{
			return null;
		}
	}
	
	/**
	 * 过滤掉集合里的空格
	 * @param list
	 * @return
	 */
	public static List<String> filterWhite(List<String> list){
		List<String> resultList=new ArrayList<String>();
		for(String l:list){
			if(isNotEmpty(l)){
				resultList.add(l);
			}
		}
		return resultList;
	}
	/**
	 * 根据分隔符分割字符串并去重成Long集合
	 * @param list
	 * @return
	 */
	public static List<Long> splitListL(String splits,String s){
		String[] split = splits.split(s);
		 List<Long> listPId = new ArrayList<Long>();
		 for (int i = 0; i < split.length; i++) {
			if ("".equals(split[i].trim()) || null == split[i].trim()) {
				if(!listPId.contains(null)) 
					listPId.add(null);
			} else {
				if(!listPId.contains(Long.valueOf(split[i].trim()))) 
				listPId.add(Long.valueOf(split[i].trim()));
			}
		}
		return listPId;
	}
	/**
	 * 根据分隔符分割字符串并去重成String集合
	 * @param list
	 * @return
	 */
	public static List<String> splitListS(String splits,String s){
		String[] split = splits.split(s);
		List<String> listPId = new ArrayList<String>();
		for (int i = 0; i < split.length; i++) {
			if ("".equals(split[i].trim()) || null == split[i].trim()) {
				if(!listPId.contains(null)) 
					listPId.add(null);
			} else {
				if(!listPId.contains(split[i].trim())) 
					listPId.add(split[i].trim());
			}
		}
		return listPId;
	}
	
	/** 
	* 查看一个字符串是否可以转换为数字 
	* @param str 字符串 
	* @return true 可以; false 不可以 
	*/ 
	public static boolean isStr2Num(String str) {  
	    try {  
	        Integer.parseInt(str);  
	        return true;  
	    } catch (NumberFormatException e) {  
	        return false;  
	    }  
	}
	/**
	 * 将String转化为int（转化不成返回0）
	 * @param str
	 * @return
	 */
	public static int strNum(String str) {  
		int a = 0;
		if(null != str){
			str= str.trim();
			try {
			    a = Integer.parseInt(str);
			} catch (NumberFormatException e) {
				a = 0;
			}
		}
		return a;
	}
	
	/**
	 * float类型的加减乘除，避免Java 直接计算错误
	 * @param t1
	 * @param t2
	 * @param type 加：add   减：  subtract  乘：multiply 除：divide
	 * @return
	 */
	public static float calcFloatValue(float t1,float t2,String type){
		BigDecimal a = new BigDecimal(String.valueOf(t1));
		BigDecimal b = new BigDecimal(String.valueOf(t2));
		float retValue=0f;
		switch (type){
			case "add":
				retValue=a.add(b).floatValue();
				break;
			case "subtract":
				retValue=a.subtract(b).floatValue();
				break;
			case "multiply":
				retValue=a.multiply(b).floatValue();
				break;
			case "divide":
				retValue=a.divide(b).floatValue();
				break;
		}
		return retValue;
	}
	
	public static void main(String[] args) {
		List<Long> splitListL = splitListL("1，，1，1，2，3，4，5", "，");
		List<String> splitListS = splitListS("a，，a，1，v，3，4，5", "，");
		
		System.out.println(splitListL);
		System.out.println(splitListS);
		System.out.println(filterWhite(splitListS));
		 
		 //生成0001
		System.out.println("生成0001"+String.format("%04d", 1));
		//修改字符串中的所有与指定字段为指定字符串
		System.out.println("修改字符串中的所有与指定字段为指定字符串"+"e,fdsibfgd,jgymffgbgth".replace("b","*"));
	}
}
