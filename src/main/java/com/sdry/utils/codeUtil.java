package com.sdry.utils;

public class codeUtil {
	
	 public static String getCodes(String strChinese,String numbering,String batch,String sequence_number){
		 //供应商（3位）
		 //String gys = formatStr(PinyinUtil.getPYIndexStr(strChinese, true),3);
		 String gys = strChinese;
		 //品编（11位）
		 String pb = formatStr(numbering,11); 
		 //批次号（10位）
		 //String pc = formatStr(batch,10); 
		 String pc = "A"; 
		 //顺序号（8位）
		 //String sx = getSubstringInt(sequence_number,8); 
		 String sx = addZeroForNum(sequence_number, 7);
		 return gys+pb+pc+sx;
	 }
	 public static String getSubstringInt(String str,int num){
		 if(str.length() > num){
			 str = str.substring(0,num);
		 }else{
			 
			 str = String.format("%0"+num+"d", StringUtil.strNum(str));     
		 }
		return str;
	 }
	 /**
	 * 生成指定长度字符串，不足位右补空格
	 * @param str
	 * @param length
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String formatStr(String str, int length) {
		int strLen;
		if (str == null) {
			strLen = 0;
		}else{
			strLen= str.length();
		}
		
		if (strLen == length) {
			return str;
		} else if (strLen < length) {
			int temp = length - strLen;
			String tem = "";
			for (int i = 0; i < temp; i++) {
				tem = tem + "0";
			}
			return  tem + str;
		}else{
			return str.substring(0,length);
		}
	}
	
	
	public static String getNextUpEn(String en){  
        char lastE = 'a';  
        char st = en.toCharArray()[0];
        if(Character.isUpperCase(st)){
            if(en.equals("Z")){
                return "A";
            }
            if(en==null || en.equals("")){ 
                return "A";  
            }
            lastE = 'Z';  
        }else{
            if(en.equals("z")){
                return "a";
            }
            if(en==null || en.equals("")){ 
                return "a";  
            }
            lastE = 'z';  
        }
        int lastEnglish = (int)lastE;      
        char[] c = en.toCharArray();  
        if(c.length>1){  
            return null;  
        }else{  
            int now = (int)c[0];  
            if(now >= lastEnglish)  
                return null;  
            char uppercase = (char)(now+1);  
            return String.valueOf(uppercase);  
        }  
    }
	

	public static String addZeroForNum(String str, int strLength) {
	    int strLen = str.length();
	    if (strLen < strLength) {
	        while (strLen < strLength) {
	            StringBuffer sb = new StringBuffer();
	            sb.append("0").append(str);// 左补0
	            // sb.append(str).append("0");//右补0
	            str = sb.toString();
	            strLen = str.length();
	        }
	    }
	
	    return str;
	}

	 public static void main(String[] args) {
		System.out.println(getCodes("ss方法", "55ss55", "55ss55", "5555"));
		//System.out.println(getSubstringInt("555", 8));
		//System.out.println(formatStr("555",8)+"xx");
	}
}
