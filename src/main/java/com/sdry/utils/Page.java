package com.sdry.utils;

/**
 * 
 * @Title:Page
 * @Package com.sdry.utils
 * @author llm
 * @date 2019年4月17日
 * @version 1.0
 */
public class Page {

	private Integer page;
	
	private Integer limit;

	public Page() {
		super();
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	@Override
	public String toString() {
		return "Page [page=" + page + ", limit=" + limit + "]";
	}
	
}
