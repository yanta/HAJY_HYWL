package com.sdry.service.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.llm.LlmInventoryInfo;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.llm.LocationStock;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;

/**
 * 业务查询Service
 * @Title:InquiryBusinessService
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年4月23日
 * @version 1.0
 */
public interface InquiryBusinessService {

	/**
	 * APP 根据物料名称查库存信息	
	 * @Title:appGetInventoryInfoByMateriel   
	 * @param materielName
	 * @return
	 */
	List<LlmInventoryInfo> appGetInventoryInfoByMateriel(String materielName);
	
	/**
	 * 分页查库存信息	
	 * @Title:appGetInventoryInfoByMateriel   
	 * @param map
	 * @return
	 */
	List<LlmInventoryInfo> listPageInventoryInfo(Map<String, Object> map);
	
	/**
	 * 根据条件获取库存信息总行数
	 * @Title:getCountInventoryInfo   
	 * @param llmInventoryInfo
	 * @return
	 */
	Integer getCountInventoryInfo(LlmInventoryInfo llmInventoryInfo);

	/**   
	 * 查询所有库区
	 * @Title:getAllWarehouseRegion   
	 * @return             
	 */
	List<WarehouseRegion> getAllWarehouseRegion();

	/**   
	 * 查询所有库位
	 * @Title:getAllWarehouseRegionLocation   
	 * @return             
	 */
	List<WarehouseRegionLocation> getAllWarehouseRegionLocation();

	/**   
	 * 查询数量统计总行数
	 * @Title:materielQuantityStatistics   
	 * @param materielName
	 * @return             
	 */
	int getCountMaterielQuantityStatistics(String materielName);

	/**   
	 * 根据退库单ID查上架记录总条数
	 * @Title:getPutawayByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getPutawayByCancellingStockId(Long id);


	/**   
	 * 根据退库单ID查收货入库记录总条数
	 * @Title:getPutStockByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getPutStockByCancellingStockId(Long id);


	/**   
	 * 根据退库单ID查验收记录总条数
	 * @Title:getCheckStockByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getCheckStockByCancellingStockId(Long id);

	/**   
	 * 获取仓库库位总行数
	 * @Title:getCountLocationStock   
	 * @return             
	 */
	int getCountLocationStock();

	/**   
	 * 获取仓库库位物料信息
	 * @Title:getAllLocationStock   
	 * @return             
	 */
	List<LocationStock> getAllLocationStock();

	/**   
	 * app端获取所有仓库
	 * @Title:appGetStock   
	 * @return             
	 */
	List<LlmWarehouseStock> appGetStock();
	
}
