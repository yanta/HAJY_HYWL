package com.sdry.service.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.jyy.Receive;
import com.sdry.model.llm.BarCode;
import com.sdry.model.llm.LlmCancellingStocks;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;

/**
 * 
 * @Title:CancellingStockService
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年5月14日
 * @version 2.0
 */
public interface CancellingStockService {

	/**   
	 * 根据条件获取退库单总行数
	 * @Title:getCountConcellingStock   
	 * @param cancellingStock
	 * @return             
	 */
	int getCountConcellingStock(LlmCancellingStocks cancellingStock);

	/**   
	 * 根据条件分页查询退库单
	 * @Title:listPageConcellingStock   
	 * @param map
	 * @return             
	 */
	List<LlmCancellingStocks> listPageConcellingStock(Map<String, Object> map);

	/**   
	 * 新增退库单
	 * @Title:saveConcellingStock   
	 * @param cancellingStock
	 * @return             
	 */
	Integer saveConcellingStock(LlmCancellingStocks cancellingStock);

	/**   
	 * 修改退库单
	 * @Title:updateConcellingStock   
	 * @param cancellingStock
	 * @return             
	 */
	Integer updateConcellingStock(LlmCancellingStocks cancellingStock);
	
	/**   
	 * 修改退库单2
	 * @Title:updateConcellingStock   
	 * @param map 
	 * @return             
	 */
	Integer updateConcellingStock2(Map<String, Object> map);

	/**   
	 * 批量删除退库单
	 * @Title:deleteConcellingStock   
	 * @param ids
	 * @return             
	 */
	Integer deleteConcellingStock(String ids);

	/**   
	 * 发送退库单
	 * @Title:sendConcellingStock   
	 * @param roder
	 * @return             
	 */
	Integer sendConcellingStock(String roder);
	
	/**
	 * 获取所有供应商
	 * @Title:getAllWarehouse   
	 * @return
	 */
	List<Customer> getAllCustomer();
	
	/**
	 * 获取所有仓库
	 * @Title:getAllWarehouse   
	 * @return
	 */
	List<Warehouse> getAllWarehouse();
	
	/**
	 * 获取所有退库单
	 * @Title:getAllCancellingStock   
	 * @return
	 */
	List<LlmCancellingStocks> getAllCancellingStock();
	
	/**
	 * 根据物料ID查询物料表
	 * @Title:getMaterielByMid   
	 * @param id
	 * @return
	 */
	Materiel getMaterielByMid(Long id);
	
	/**
	 * 根据客户ID获取供应商联系人信息
	 * @Title:getCustomerById   
	 * @param id
	 * @return
	 */
	Customer getClientById(Long id);
	
	/**
	 * 根据仓库ID获取管理员信息
	 * @Title:getWarehouseById   
	 * @return
	 */
	Warehouse getWarehouseById(Long id);

	/**   
	 * 获取所有不良品库
	 * @Title:getAllBadnessWarehouse   
	 * @return             
	 */
	List<Warehouse> getAllBadnessWarehouse();

	/**   
	 * 根据仓库ID获取物料信息
	 * @Title:getMaterielByWarehouseId   
	 * @param id
	 * @return             
	 */
	List<Materiel> getMaterielByWarehouseId(Long parseLong);

	/**   
	 * 根据退库单编号查询退库单
	 * @Title:getCancellingStockByNumber   
	 * @param cancellingNumber
	 * @return             
	 */
	LlmCancellingStocks getCancellingStockByNumber(String cancellingNumber);

	/**   
	 * 根据仓库ID获取供应商信息
	 * @Title:getCustomerByWarehouseId   
	 * @param id
	 * @return             
	 */
	List<Customer> getCustomerByWarehouseId(Long id);

	/**   
	 * 根据供应商和SAP查询所有物料   
	 * @param map
	 * @return             
	 */
	List<Materiel> appGetMaterielByCustomerAndSAP(Map<String, Object> map);

	/**   
	 * 根据收货单号查收货单  
	 * @param receiveNumber
	 * @return             
	 */
	Receive getReceiveByNumber(String receiveNumber);

	/**   
	 * 根据SAP模糊搜索所有条码
	 * @param number
	 * @return             
	 */
	List<CodeMark> getCodeByMaterielNum(String number);

	/**   
	 * 根据条码查库存位置   
	 * @param code
	 * @return             
	 */
	BarCode getLocationByCode(CodeMark code);
	
int caceltk(Map map);
	
	int restortk(Map map);

}
