package	com.sdry.service.llm;
import com.sdry.model.llm.LlmCodemarkSwitchDetail;
import java.util.List;
/**
 *
 *@ClassName: LlmCodemarkSwitchDetailService
 *@Description: 条码类型转换详情
 *@Author llm
 *@Date 2019-08-07 18:53:08
 *@version 1.0
*/
public interface LlmCodemarkSwitchDetailService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public LlmCodemarkSwitchDetail queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchDetail> queryAllByMution(LlmCodemarkSwitchDetail param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchDetail> findPageByMution(LlmCodemarkSwitchDetail param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(LlmCodemarkSwitchDetail param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(LlmCodemarkSwitchDetail param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(LlmCodemarkSwitchDetail param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
}
