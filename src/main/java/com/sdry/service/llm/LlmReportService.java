package com.sdry.service.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.tdd.ReportUtil;

/**
 * 报表
 * @Title:ReportService
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年5月27日
 * @version 1.0
 */
public interface LlmReportService {

	/**   
	 * 退库月报表
	 * @Title:getMonthCancellingStockReportByCustomer   
	 * @param map
	 * @return             
	 */
	List<ReportUtil> getMonthCancellingStockReportByCustomer(Map<String, Object> map);

	/**   
	 * 出库报表
	 * @Title:getOutStockReportByCustomer   
	 * @param map
	 * @return             
	 */
	List<ReportUtil> getOutStockReportByCustomer(Map<String, Object> map);

}
