package com.sdry.service.ljq;

import java.util.List;
import java.util.Map;

import com.sdry.model.ljq.LjqWaitSendArea;

/**
  * @ClassName LjqWaitSendAreaSevices.java
  * @Description 
  * @author LJQ
  * @Date 2019年5月7日 上午9:45:03
  * @Version 1.0
  *
  */
public interface LjqWaitSendAreaService {

	//分页查询待发货区信息
	public List<LjqWaitSendArea> listPageWaitSendArea(Map<String,Object> map);
	
	
	//获取待发货区总数
	public int listCountWaitSendArea();
	
	
}

