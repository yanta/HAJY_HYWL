package	com.sdry.service.impl.tdd;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.mapper.tdd.ReportMapper;
import com.sdry.model.tdd.ReportUtil;
import com.sdry.service.tdd.ReportService;
/**
 *
 *@ClassName: ReportService
 *@Description: 报表
 *@Author tdd
 *@Date 2019-05-27 13:47:25
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class ReportServiceImpl implements ReportService {
	@Resource ReportMapper ReportMapper;

	/**
	 * 查询入库报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @return 报表
	 */
	public List<ReportUtil> selectRkReport(String date, String type,
			Long customer_id) {
		return ReportMapper.selectRkReport(date, type, customer_id);
	}

	/**
	 * 查询不良品报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @return 报表
	 */
	public List<ReportUtil> selectBlpReport(String date, String type,
			Long customer_id) {
		return ReportMapper.selectBlpReport(date, type, customer_id);
	}
}
