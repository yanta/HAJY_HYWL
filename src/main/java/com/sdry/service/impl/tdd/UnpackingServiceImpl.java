package	com.sdry.service.impl.tdd;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.tdd.Unpacking;
import com.sdry.service.tdd.UnpackingService;
import com.sdry.mapper.tdd.UnpackingMapper;
/**
 *
 *@ClassName: UnpackingService
 *@Description: 拆箱表
 *@Author tdd
 *@Date 2019-05-14 10:56:27
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class UnpackingServiceImpl implements UnpackingService {
	@Resource UnpackingMapper unpackingMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public Unpacking queryById(Long id) {
		return unpackingMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Unpacking> queryAllByMution(Unpacking param) {
		return unpackingMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Unpacking> findPageByMution(Unpacking param) {
		return unpackingMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(Unpacking param) {
		return unpackingMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(Unpacking param) {
		return unpackingMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(Unpacking param) {
		return unpackingMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return unpackingMapper.delete(ids);
	}
}
