package	com.sdry.service.impl.tdd;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.model.tdd.UnpackingTask;
import com.sdry.service.tdd.UnpackingTaskService;
import com.sdry.mapper.tdd.UnpackingTaskMapper;
/**
 *
 *@ClassName: UnpackingTaskService
 *@Description: 拆箱任务表
 *@Author tdd
 *@Date 2019-05-14 10:58:34
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class UnpackingTaskServiceImpl implements UnpackingTaskService {
	@Resource UnpackingTaskMapper unpackingTaskMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public UnpackingTask queryById(Long id) {
		return unpackingTaskMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackingTask> queryAllByMution(UnpackingTask param) {
		return unpackingTaskMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackingTask> findPageByMution(UnpackingTask param) {
		return unpackingTaskMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(UnpackingTask param) {
		return unpackingTaskMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(UnpackingTask param) {
		return unpackingTaskMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(UnpackingTask param) {
		return unpackingTaskMapper.update(param);
	}
	/** 
	 * 根据主键更新状态 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	 */
	public Integer updateState(UnpackingTask param){
		return unpackingTaskMapper.updateState(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return unpackingTaskMapper.delete(ids);
	}
}
