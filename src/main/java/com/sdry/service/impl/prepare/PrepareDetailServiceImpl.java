package	com.sdry.service.impl.prepare;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.model.prepare.PrepareDetail;
import com.sdry.service.prepare.PrepareDetailService;
import com.sdry.mapper.prepare.PrepareDetailMapper;
/**
 *
 *@ClassName: PrepareDetailService
 *@Description: 配货单详情
 *@Author tdd
 *@Date 2019-08-24 16:47:15
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class PrepareDetailServiceImpl implements PrepareDetailService {
	@Resource PrepareDetailMapper PrepareDetailMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public PrepareDetail queryById(Long id) {
		return PrepareDetailMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetail> queryAllByMution(PrepareDetail param) {
		return PrepareDetailMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetail> findPageByMution(PrepareDetail param) {
		return PrepareDetailMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(PrepareDetail param) {
		return PrepareDetailMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(PrepareDetail param) {
		return PrepareDetailMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(PrepareDetail param) {
		return PrepareDetailMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return PrepareDetailMapper.delete(ids);
	}
	/** 
	 * 根据配货单id拼接字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	 */
	public Integer deleteByDids(String ids){
		return PrepareDetailMapper.deleteByDids(ids);
	}
	/**
	 * 根据物料id查询推荐库位编号
	 * @param mid 物料id
	 * @return
	 */
	public List<String> selectLocationByMid(Long mid){
		return PrepareDetailMapper.selectLocationByMid(mid);
	}
}
