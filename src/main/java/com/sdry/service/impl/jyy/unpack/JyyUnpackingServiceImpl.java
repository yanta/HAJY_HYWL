package	com.sdry.service.impl.jyy.unpack;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.mapper.jyy.unpack.JyyUnpackingMapper;
import com.sdry.model.jyy.unpack.JyyUnpacking;
import com.sdry.service.jyy.unpack.JyyUnpackingService;
import com.sdry.web.controller.jyy.unpack.JyyInfoGood;
/**
 *
 *@ClassName: UnpackingService
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:57:31
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class JyyUnpackingServiceImpl implements JyyUnpackingService {
	@Resource JyyUnpackingMapper jyyUnpackingMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public JyyUnpacking queryById(Long id) {
		return jyyUnpackingMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<JyyUnpacking> queryAllByMution(JyyUnpacking param) {
		return jyyUnpackingMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<JyyUnpacking> findPageByMution(JyyUnpacking param) {
		return jyyUnpackingMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(JyyUnpacking param) {
		return jyyUnpackingMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(JyyUnpacking param) {
		return jyyUnpackingMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(JyyUnpacking param) {
		return jyyUnpackingMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return jyyUnpackingMapper.delete(ids);
	}
	
	/**
	 * 条件查询良品库存
	 * @param customerId
	 * @return
	 */
	@Override
	public List<JyyInfoGood> selectPageGood(JyyInfoGood jyyInfoGood) {
		return jyyUnpackingMapper.selectPageGood(jyyInfoGood);
	}
	/**
	 * 条件统计良品库存
	 * @param customerId
	 * @return
	 */
	@Override
	public int selectCountGood(JyyInfoGood jyyInfoGood) {
		return jyyUnpackingMapper.selectCountGood(jyyInfoGood);
	}
	/**
	 * 更改单子状态为已完成
	 * @param id 
	 * @return
	 */
	@Override
	public Integer updateState(Long id) {
		return jyyUnpackingMapper.updateState(id);
	}
}
