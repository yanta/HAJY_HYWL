package	com.sdry.service.impl.jyy.unpack;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.jyy.unpack.UnpackDetalCode;
import com.sdry.service.jyy.unpack.UnpackDetalCodeService;
import com.sdry.mapper.jyy.unpack.UnpackDetalCodeMapper;
/**
 *
 *@ClassName: UnpackDetalCodeService
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:31
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class UnpackDetalCodeServiceImpl implements UnpackDetalCodeService {
	@Resource UnpackDetalCodeMapper UnpackDetalCodeMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public UnpackDetalCode queryById(Long id) {
		return UnpackDetalCodeMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetalCode> queryAllByMution(UnpackDetalCode param) {
		return UnpackDetalCodeMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetalCode> findPageByMution(UnpackDetalCode param) {
		return UnpackDetalCodeMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(UnpackDetalCode param) {
		return UnpackDetalCodeMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(UnpackDetalCode param) {
		return UnpackDetalCodeMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(UnpackDetalCode param) {
		return UnpackDetalCodeMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return UnpackDetalCodeMapper.delete(ids);
	}
}
