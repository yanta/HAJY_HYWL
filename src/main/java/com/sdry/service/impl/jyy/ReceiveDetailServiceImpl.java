package com.sdry.service.impl.jyy;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.jyy.Workload;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.ReceiveMark;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.service.jyy.ReceiveDetailService;
import com.sdry.mapper.jyy.ReceiveDetailMapper;
/**
 *
 *@ClassName: ReceiveDetailService
 *@Description: 收货计划详情
 *@Author jyy
 *@Date 2019-04-19 14:30:17
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class ReceiveDetailServiceImpl implements ReceiveDetailService {
	@Resource ReceiveDetailMapper ReceiveDetailMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public ReceiveDetail queryById(Long id) {
		return ReceiveDetailMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<ReceiveDetail> queryAllByMution(ReceiveDetail param) {
		
		
		return ReceiveDetailMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<ReceiveDetail> findPageByMution(ReceiveDetail param) {
		return ReceiveDetailMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(ReceiveDetail param) {
		return ReceiveDetailMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(ReceiveDetail param) {
		return ReceiveDetailMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(ReceiveDetail param) {
		return ReceiveDetailMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return ReceiveDetailMapper.delete(ids);
	}
	
	/**
	 * 统计收货详情中有几条未收货的
	 */
	@Override
	public int countState(String receiveNumber) {
		return ReceiveDetailMapper.countState(receiveNumber);
	}

	/**
	 * 根据物料id查询物料实体
	 */
	@Override
	public Materiel queryMaterielByReceiveDetailId(Long mid) {
		return ReceiveDetailMapper.queryMaterielByReceiveDetailId(mid);
	}

	/**
	 * 添加每种物料对应的多个条码
	 * @param codemark
	 */
	@Override
	public void addCodeMark(CodeMark codeMark) {
		ReceiveDetailMapper.addCodeMark(codeMark);
	}

	/**
	 * 根据收货单详细表id查询对应的条码
	 * @param receive_detail_id 收货单详细表id
	 * @throws Exception
	 */
	@Override
	public List<CodeMark> queryCodemarkByReceiveDetailId(String receive_detail_id) {
		return ReceiveDetailMapper.queryCodemarkByReceiveDetailId(receive_detail_id);
	}

	/**
	 * 根据mcode查询收货单详细
	 * @param mcode
	 * @return
	 */
	@Override
	public CodeMark queryReceiveDetailIdByMcode(String mcode) {
		return ReceiveDetailMapper.queryReceiveDetailIdByMcode(mcode);
	}

	/**
	 * 查询状态为1的收货单详细
	 * @throws Exception
	 */
	@Override
	public List<ReceiveDetail> selectAccurateByCodeApp(Long id) {
		return ReceiveDetailMapper.selectAccurateByCodeApp(id);
	}

	/**
	 * 新表添加数据
	 * @param receiveDetail
	 */
	@Override
	public void addReceiveDetailInstock(ReceiveDetail receiveDetail) {
		ReceiveDetailMapper.addReceiveDetailInstock(receiveDetail);
	}

	/**
	 * 质检表添加信息
	 * @param receiveDetail
	 */
	@Override
	public Long addReceiveDetailQuality(ReceiveDetail receiveDetail) {
		return ReceiveDetailMapper.addReceiveDetailQuality(receiveDetail);
	}

	/**
	 * 质检表修改信息
	 * @param receiveDetail
	 */
	@Override
	public void updateReceiveDetailQuality(ReceiveDetail receiveDetail) {
		ReceiveDetailMapper.updateReceiveDetailQuality(receiveDetail);
	}

	/**
	 * 分页查询质检单
	 * @param criteria
	 * @return
	 */
	@Override
	public List<ReceiveDetailQuality> queryReceiveDetailQualityCriteria(LzQueryCriteria criteria) {
		return ReceiveDetailMapper.queryReceiveDetailQualityCriteria(criteria);
	}

	/**
	 * 分页查询质检单数量
	 * @param criteria
	 * @return
	 */
	@Override
	public int countReceiveDetailQualityCriteria(LzQueryCriteria criteria) {
		return ReceiveDetailMapper.countReceiveDetailQualityCriteria(criteria);
	}

	/**
	 * 查询状态为0的质检表
	 * @return
	 */
	@Override
	public List<ReceiveDetailQuality> queryReceiveDetailQualityAPP() {
		return ReceiveDetailMapper.queryReceiveDetailQualityAPP();
	}

	/**
	 * 查询状态为1的质检表
	 * @return
	 */
	@Override
	public List<ReceiveDetailQuality> queryReceiveDetailQualityAPP1() {
		return ReceiveDetailMapper.queryReceiveDetailQualityAPP1();
	}

	/**
	 * 修改良品数和不良品数
	 * @param receiveDetailQuality
	 * @return
	 */
	@Override
	public Long updateReceiveDetailQualityAPP(ReceiveDetailQuality receiveDetailQuality) {
		return ReceiveDetailMapper.updateReceiveDetailQualityAPP(receiveDetailQuality);
	}

	/**
	 * 添加每种物料对应的工作量
	 * @param workload
	 */
	@Override
	public void addWorkload(Workload workload) {
		ReceiveDetailMapper.addWorkload(workload);
	}

	/**
	 * 绑定后修改质检的状态为2
	 * @param receiveDetailId
	 */
	@Override
	public void updateReceiveDetailQuality2(String receiveDetailId) {
		ReceiveDetailMapper.updateReceiveDetailQuality2(receiveDetailId);
	}

	/**
	 * 拆箱
	 * @param code
	 */
	@Override
	public void uncomeku(String code) {
		ReceiveDetailMapper.uncomeku(code);
	}
	
	/**
	 * 给不良品库存添加数据
	 * @param receiveDetailQuality
	 */
	@Override
	public void addZcRejectsWarehouseEntity(
			ReceiveDetailQuality receiveDetailQuality) {
		ReceiveDetailMapper.addZcRejectsWarehouseEntity(receiveDetailQuality);
	}
	
	/**
	 * 将库存表中的不良品数量减掉
	 * @param receiveDetailQuality
	 */
	@Override
	public void updateInventoryInfoNum(ReceiveDetailQuality receiveDetailQuality) {
		ReceiveDetailMapper.updateInventoryInfoNum(receiveDetailQuality);
	}
	
	/**
	 * 根据质检单详细表id查询对应的工作量
	 * @param receiveDetailQuality_id 质检单详细表id
	 * @param response
	 * @throws Exception
	 */
	@Override
	public List<Workload> queryWorkloadByreceiveDetailQualityId(
			String receiveDetailQuality_id) {
		return ReceiveDetailMapper.queryWorkloadByreceiveDetailQualityId(receiveDetailQuality_id);
	}
	
	/**
	 * 根据批次和物料id查询不良品库是否存在该物料
	 */
	@Override
	public ZcRejectsWarehouseEntity queryZcRejectsWarehouseEntityByMbatchAngMid(
			ReceiveDetailQuality receiveDetailQuality) {
		return ReceiveDetailMapper.queryZcRejectsWarehouseEntityByMbatchAngMid(receiveDetailQuality);
	}
	
	/**
	 * 如果不良品库存在该物料就累加
	 */
	@Override
	public void updateZcRejectsWarehouseEntity(
			ReceiveDetailQuality receiveDetailQuality) {
		ReceiveDetailMapper.updateZcRejectsWarehouseEntity(receiveDetailQuality);
	}
	
	/**
	 * 根据精准码查询是否存在
	 * @param mcode
	 * @param response
	 */
	@Override
	public CodeMark queryRepeatAccurateByCodeApp(String mcode) {
		return ReceiveDetailMapper.queryRepeatAccurateByCodeApp(mcode);
	}
	
	/**
	 * 查询有仓库操作员权限的人员
	 * @param mcode
	 * @param response
	 */
	@Override
	public List<String> queryRoleUser() {
		return ReceiveDetailMapper.queryRoleUser();
	}
	
	/**
	 * 质检表添加信息2
	 * @param receiveDetail
	 */
	@Override
	public Long addReceiveDetailQuality1(ReceiveDetail receiveDetail) {
		return ReceiveDetailMapper.addReceiveDetailQuality1(receiveDetail);
	}
	
	/**
	 * 根据条码修改条码状态
	 * @param code
	 */
	@Override
	public Long updateIsngStatus(String code) {
		return ReceiveDetailMapper.updateIsngStatus(code);
	}
	
	/**
	 * 根据条码查询物料和批次
	 * @param code
	 * @return
	 */
	@Override
	public ReceiveDetailQuality queryReceiveDetailQualityByCode(String code) {
		return ReceiveDetailMapper.queryReceiveDetailQualityByCode(code);
	}
	
	/**
	 * 修改CodeMark
	 * @param codeMark
	 * @return
	 */
	@Override
	public Long updateCodeMark(CodeMark codeMark) {
		return ReceiveDetailMapper.updateCodeMark(codeMark);
	}
	
	/**
	 * 根据CodeMark查询是否存在
	 * @param receiveCode
	 * @return
	 */
	@Override
	public CodeMark queryCodeMarkByCode(CodeMark codemark) {
		return ReceiveDetailMapper.queryCodeMarkByCode(codemark);
	}
	
	/**
	 * 根据收货单id查询CodeMark
	 * @param receive_detail_id
	 * @return
	 */
	@Override
	public List<CodeMark> queryCodeMarkById(String receive_detail_id) {
		return ReceiveDetailMapper.queryCodeMarkById(receive_detail_id);
	}
	
	/**
	 * 根据精准码修改质检区库存的数量
	 * @param mid 物料id
	 * @param pici 批次
	 * @param 差值数
	 */
	@Override
	public void updateInventoryInfoQuality(String mid, String pici,
			String afterVal) {
		ReceiveDetailMapper.updateInventoryInfoQuality(mid, pici, afterVal);
	}
	
	/**
	 * 绑定条码
	 * @param receiveMark
	 */
	@Override
	public void addReceiveMark(ReceiveMark receiveMark) {
		ReceiveDetailMapper.addReceiveMark(receiveMark);
	}
	
	/**
	 * 根据收货单id查询收货绑定实体
	 * @param receiveId 收货单id
	 * @param response
	 */
	@Override
	public List<ReceiveMark> queryReceiveMarkByReceiveId(String receiveDetailId) {
		return ReceiveDetailMapper.queryReceiveMarkByReceiveId(receiveDetailId);
	}
	
	/**
	 * 根据收货单id查询所有ReceiveMark实体
	 * @param receiveId 收货单id
	 * @param mcode
	 * @param response
	 */
	@Override
	public ReceiveMark queryReceiveCodeByReceiveId(String receiveId,
			String mcode) {
		return ReceiveDetailMapper.queryReceiveCodeByReceiveId(receiveId, mcode);
	}
	
	/**
	 * 根据收货单详细id查询所有ReceiveMark实体
	 * @param receiveDetailId 收货单详细id
	 * @param mcode
	 * @param response
	 */
	@Override
	public List<ReceiveMark> queryReceiveCodeByReceiveDetailId(String receiveDetailId) {
		return ReceiveDetailMapper.queryReceiveCodeByReceiveDetailId(receiveDetailId);
	}
	/**
	 * 根据收货单id查询条码
	 * @param receiveId 收货单id
	 * @param response
	 */
	public List<ReceiveMark> selectReceiveMarkByReceiveId(Long receiveId) {
		return ReceiveDetailMapper.selectReceiveMarkByReceiveId(receiveId);
	}
	
	/**
	 * 根据条码查询是否包含
	 * @param code
	 * @return
	 */
	@Override
	public ZcMaterielAndTrayEntity queryMaterielTrayByCode(String code) {
		return ReceiveDetailMapper.queryMaterielTrayByCode(code);
	}
	
	/**
	 * 根据id替换条码
	 * @param indentity
	 * @param code
	 */
	@Override
	public void updateMaterielTrayByCode(String indentity, String code) {
		ReceiveDetailMapper.updateMaterielTrayByCode(indentity, code);
	}
	
	/**
	 * 根据物料id查询该质检表批次的该物料是否存在
	 * @param mid 物料id
	 * @param pici 批次
	 * @return
	 */
	@Override
	public ReceiveDetail queryReceiveDetaiQualityExsit(String mid, String pici, String receiveNumber) {
		return ReceiveDetailMapper.queryReceiveDetaiQualityExsit(mid, pici, receiveNumber);
	}
	
	/**     
	 * 根据ID删除记录   
	 * @param id    
	 */
	@Override
	public void deleteMaterielTrayById(Long id) {
		ReceiveDetailMapper.deleteMaterielTrayById(id);
	}
	/**       
	 * @param tray
	 * @return    
	 */
	@Override
	public ZcTrayAndLocationEntity queryTrayLocationByTray(String tray) {
		return ReceiveDetailMapper.queryTrayLocationByTray(tray);
	}
	/**       
	 * @param id    
	 */
	@Override
	public void deleteTrayLocationById(Long id) {
		ReceiveDetailMapper.deleteTrayLocationById(id);
	}
	/**       
	 * @param map    
	 */
	@Override
	public void updateQuantityByMidAndPici(Map<String, Object> map) {
		ReceiveDetailMapper.updateQuantityByMidAndPici(map);
	}
	
	/**
	 * 根据物料id和批次修改不质检的数量
	 * @param receiveDetail
	 * @return
	 */
	@Override
	public Long updateReceiveDetailQuality1(ReceiveDetail receiveDetail) {
		return ReceiveDetailMapper.updateReceiveDetailQuality1(receiveDetail);
	}
	
	/**
	 * 根据物料id和批次修改质检的数量
	 * @param receiveDetail
	 * @return
	 */
	@Override
	public Long updateReceiveDetailQuality3(ReceiveDetail receiveDetail) {
		return ReceiveDetailMapper.updateReceiveDetailQuality3(receiveDetail);
	}
	
	/**
	 * 根据物料id和批次查询修改的数据的主键
	 * @param receiveDetail
	 * @return
	 */
	@Override
	public Long queryIndentityByMidAndPici(ReceiveDetail receiveDetail) {
		return ReceiveDetailMapper.queryIndentityByMidAndPici(receiveDetail);
	}
	
	/**
	 * 根据托盘码查询物料托盘信息
	 * @param tray_code
	 * @return
	 */
	@Override
	public List<ZcMaterielAndTrayEntity> selectZcMaterielAndTrayEntityByTray(
			String tray_code) {
		return ReceiveDetailMapper.selectZcMaterielAndTrayEntityByTray(tray_code);
	}
}
