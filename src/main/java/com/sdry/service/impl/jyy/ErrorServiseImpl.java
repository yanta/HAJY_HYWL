package com.sdry.service.impl.jyy;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.mapper.jyy.ErrorMapper;
import com.sdry.model.jyy.ErrorJyy;
import com.sdry.service.jyy.ErrorServise;

@Service
@Transactional
public class ErrorServiseImpl implements ErrorServise{
	
	@Resource ErrorMapper errorMapper;

	/**
	 * 添加错误信息
	 * @param errorJyy 错误实体
	 * @return 返回影响行数
	 */
	@Override
	@Transactional
	public int addError(ErrorJyy errorJyy) {
		
		int affact = 0;
		int addError = errorMapper.addError(errorJyy);
		//int addTest = errorMapper.addTest(errorJyy);
		
		if(addError > 0 ){
			affact = 1;
		}
		
		return affact;
	}

	
}
