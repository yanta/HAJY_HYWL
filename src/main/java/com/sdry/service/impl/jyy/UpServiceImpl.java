package com.sdry.service.impl.jyy;
import java.util.List;

import javax.annotation.Resource;

import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.jyy.ReceiveDetailInstock;
import com.sdry.model.lz.LzQueryCriteria;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.model.jyy.Up;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.service.jyy.UpService;
import com.sdry.mapper.jyy.UpMapper;
/**
 *
 *@ClassName: UpService
 *@Description: 入库记录
 *@Author jyy
 *@Date 2019-05-17 17:20:06
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class UpServiceImpl implements UpService {
	@Resource UpMapper UpMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public Up queryById(Long id) {
		return UpMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Up> queryAllByMution(Up param) {
		return UpMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Up> findPageByMution(Up param) {
		return UpMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(Up param) {
		return UpMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(Up param) {
		return UpMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(Up param) {
		return UpMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return UpMapper.delete(ids);
	}

	/**
	 * 新增物料库存对应关系信息
	 */
	@Override
	public Long insertMw(Up up) {
		return UpMapper.insertMw(up);
	}
	/**
	 * 根据仓库名称查询仓库id
	 * @param cqname 仓库名称
	 * @return 仓库id
	 */
	@Override
	public Long getwidByWname(String cqname) {
		return UpMapper.getwidByWname(cqname);
	}
	/**
	 * 根据物料id查询库存信息
	 * @param mid 料id
	 * @return
	 */
	@Override
	public ZcInventoryInfoEntity selectByMid(String mid, String batch_num) {
		return UpMapper.selectByMid(mid, batch_num);
	}
	@Override
	public ZcInventoryInfoEntity selectByMid2(String mid, String batch_num) {
		return UpMapper.selectByMid2(mid, batch_num);
	}
	/**
	 * 修改库存信息
	 * @return
	 */
	@Override
	public Long editStock(ZcInventoryInfoEntity zcInventoryInfoEntity) {
		return UpMapper.editStock(zcInventoryInfoEntity);
	}
	/**
	 * 修改库存信息(计数)
	 * @return
	 */
	@Override
	public Long editStockOnlyCount(ZcInventoryInfoEntity zcInventoryInfoEntity) {
		return UpMapper.editStockOnlyCount(zcInventoryInfoEntity);
	}
	
	/**
	 * 根据产品码和规格型号查询物料
	 * @return
	 */
	@Override
	public Materiel queryMaterielByRemarkAndSpecification(String remark, String materiel_size, String materiel_properties) {
		return UpMapper.queryMaterielByRemarkAndSpecification(remark, materiel_size, materiel_properties);
	}
	
	/**
	 * 根据库区id查找库区名称
	 * @param region_id
	 * @return
	 */
	@Override
	public WarehouseRegion queryRegionNameById(Long region_id) {
		return UpMapper.queryRegionNameById(region_id);
	}

	/**
	 * 分页查询已收货未入库
	 * @param criteria
	 */
	@Override
	public List<ReceiveDetailInstock> queryReceiveDetailInstockCriteria(LzQueryCriteria criteria) {
		return UpMapper.queryReceiveDetailInstockCriteria(criteria);
	}

	/**
	 * 查询已收货未入库的数量
	 * @param criteria
	 * @return
	 */
	@Override
	public int countReceiveDetailInstockCriteria(LzQueryCriteria criteria) {
		return UpMapper.countReceiveDetailInstockCriteria(criteria);
	}

	/**
	 * 添加库存信息
	 * @param zcInventoryInfoEntity
	 */
	@Override
	public Long insertStock(ZcInventoryInfoEntity zcInventoryInfoEntity) {
		return UpMapper.insertStock(zcInventoryInfoEntity);
	}
	/**
	 * 添加库存信息（计数）
	 * @param zcInventoryInfoEntity
	 */
	@Override
	public Long insertStockOnlyCount(ZcInventoryInfoEntity zcInventoryInfoEntity) {
		return UpMapper.insertStockOnlyCount(zcInventoryInfoEntity);
	}

	/**
	 * 给不良品绑定条码
	 * @param receiveDetail
	 */
	@Override
	public void bingdNGCode(ReceiveDetail receiveDetail) {
		UpMapper.bingdNGCode(receiveDetail);
	}

	/**
	 * 减去已收货未入库数量
	 * @param final_value
	 */
	@Override
	public void updateReceiveDetailInstockCodeNum(Long id, Long final_value) {
		UpMapper.updateReceiveDetailInstockCodeNum(id, final_value);
	}
	
	/**
	 * 根据供应商id查询供应商的邮箱
	 * @param customer_id
	 * @return
	 */
	@Override
	public Customer queryCustomerEmailById(Long customer_id) {
		return UpMapper.queryCustomerEmailById(customer_id);
	}
}