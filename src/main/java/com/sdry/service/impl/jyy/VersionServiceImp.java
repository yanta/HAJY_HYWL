package com.sdry.service.impl.jyy;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.jyy.VersionMapper;
import com.sdry.model.jyy.Version;
import com.sdry.service.jyy.VersionService;

/**
 * 版本管理ServiceImp
 * @Title:VersionServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年3月26日
 * @version 1.0
 */

@Service
public class VersionServiceImp implements VersionService {

	@Resource
	private VersionMapper versionMapper;
	
	/**     
	 * 获取最新版本  
	 * @return    
	 */
	@Override
	public Version getLatestVersion() {
		return versionMapper.getLatestVersion();
	}

}
