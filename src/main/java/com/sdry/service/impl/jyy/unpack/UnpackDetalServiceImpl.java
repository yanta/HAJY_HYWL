package	com.sdry.service.impl.jyy.unpack;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sdry.model.jyy.unpack.UnpackDetal;
import com.sdry.service.jyy.unpack.UnpackDetalService;
import com.sdry.mapper.jyy.unpack.UnpackDetalMapper;
/**
 *
 *@ClassName: UnpackDetalService
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:14
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class UnpackDetalServiceImpl implements UnpackDetalService {
	@Resource UnpackDetalMapper UnpackDetalMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public UnpackDetal queryById(Long id) {
		return UnpackDetalMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetal> queryAllByMution(UnpackDetal param) {
		return UnpackDetalMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetal> findPageByMution(UnpackDetal param) {
		return UnpackDetalMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(UnpackDetal param) {
		return UnpackDetalMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(UnpackDetal param) {
		return UnpackDetalMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	@Transactional
	public Integer update(UnpackDetal param) {
		return UnpackDetalMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return UnpackDetalMapper.delete(ids);
	}
	/**
	 * 根据员工姓名 查询员工id
	 * @param workman 员工姓名 
	 * @return 员工id
	 */
	@Override
	public Long selectUidByUname(String workman) {
		return UnpackDetalMapper.selectUidByUname(workman);
	}
}
