package com.sdry.service.impl.zc;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.MaterielMapper;
import com.sdry.mapper.zc.ZcInventoryManagementMapper;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.MaterielInStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcAdjustEntity;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMaterielAndWarehouseEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;
import com.sdry.service.zc.ZcInventoryManagementService;
/**
 * 
 * @ClassName:    ZcInventoryManagementServiceImpl   
 * @Description:  库存管理
 * @Author:       zc   
 * @CreateDate:   2019年4月19日 下午7:22:32   
 * @Version:      v1.0
 */
@Service
public class ZcInventoryManagementServiceImpl implements ZcInventoryManagementService {

	@Resource
	private ZcInventoryManagementMapper zcInventoryManagementMapper;
	@Resource
	private MaterielMapper materielMapper;
	/**
	 * 查询库存列表
	 */
	@Override
	public List<ZcInventoryInfoEntity> selectInventoryInfoList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_inventory_info info left join lz_materiel m on m.id = info.mid left join lz_customer c on c.id = m.customer_id left join lz_warehouse_region r on r.id = m.region_id left join lz_warehouse w on w.id = r.warehouse_id left join zc_system_user u on u.id = info.enterPerson ";
		//查询字段
		String strFld = "(select sum(mNum) from zc_inventory_info i left join lz_materiel ma on ma.id = i.mid where ma.materiel_num = m.materiel_num group by ma.materiel_num) as countAll,c.customer_name,info.*,m.materiel_name,m.materiel_num,m.brevity_num,m.devanning_code,m.packing_quantity,m.materiel_size,m.materiel_properties,m.devanning_num,m.unit,m.placement_type,m.upper_value,m.lower_value,m.customer_id,m.region_id,u.userName,(w.warehouse_name+'-'+r.region_name)as region_name ";
		//排序
		String sort = "m.customer_id,m.materiel_num,m.materiel_size,materiel_properties,info.mBatch";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		List<ZcInventoryInfoEntity> selectInventoryInfoList = zcInventoryManagementMapper.selectInventoryInfoList(zcGeneralQueryEntity);
		return selectInventoryInfoList;
	}
	/**
	 * 查询库存列表数目
	 */
	@Override
	public int countInventoryInfoList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_inventory_info info left join lz_materiel m on info.mid = m.id left join zc_system_user u on u.id = info.enterPerson ";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.countInventoryInfoList(zcGeneralQueryEntity);
	}
	/**
	 * 查询精简库存列表
	 */
	@Override
	public List<ZcInventoryInfoEntity> selectInventoryInfoOnlyCountList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_inventory_info_only_count info left join lz_materiel m on m.id = info.mid left join lz_customer c on c.id = m.customer_id left join lz_warehouse_region r on r.id = m.region_id left join lz_warehouse w on w.id = r.warehouse_id left join zc_system_user u on u.id = info.enterPerson ";
		//查询字段
		String strFld = "(select sum(mNum) from zc_inventory_info_only_count i left join lz_materiel ma on ma.id = i.mid where ma.materiel_num = m.materiel_num group by ma.materiel_num) as countAll,c.customer_name,info.*,m.materiel_name,m.materiel_num,m.brevity_num,m.devanning_code,m.packing_quantity,m.materiel_size,m.materiel_properties,m.devanning_num,m.unit,m.placement_type,m.upper_value,m.lower_value,m.customer_id,m.region_id,u.userName,(w.warehouse_name+'-'+r.region_name)as region_name ";
		//排序
		String sort = "m.customer_id,m.materiel_num,m.materiel_size,materiel_properties,info.mBatch";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		List<ZcInventoryInfoEntity> selectInventoryInfoList = zcInventoryManagementMapper.selectInventoryInfoOnlyCountList(zcGeneralQueryEntity);
		return selectInventoryInfoList;
	}
	/**
	 * 查询精简库存列表数目
	 */
	@Override
	public int countInventoryInfoOnlyCountList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_inventory_info_only_count info left join lz_materiel m on info.mid = m.id left join zc_system_user u on u.id = info.enterPerson ";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.countInventoryInfoOnlyCountList(zcGeneralQueryEntity);
	}
	/**
	 * 查询仓库列表
	 */
	@Override
	public List<Warehouse> selectAllWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_warehouse";
		//查询条件
		String strWhere = "";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcInventoryManagementMapper.selectAllWarehouseList(zcGeneralQueryEntity);
	}
	/**
	 * 通过仓库id查库区
	 */
	@Override
	public List<WarehouseRegion> selectWarehouseRegionListByWarehouseId(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_warehouse_region r left join lz_customer_ware cw on cw.region_id = r.id";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.selectWarehouseRegionListByWarehouseId(zcGeneralQueryEntity);
	}
	/**
	 * 通过库区id查库位
	 */
	@Override
	public List<WarehouseRegionLocation> selectWarehouseRegionLocationListByRegionId(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_warehouse_region_location";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.selectWarehouseRegionLocationListByRegionId(zcGeneralQueryEntity);
	}
	/**
	 * 通过产品码或简码查询库存信息
	 */
	@Override
	public List<ZcInventoryInfoEntity> selectInventoryInfoByCode(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_inventory_info info left join lz_materiel m on m.id = info.mid left join lz_customer c on c.id = m.customer_id";
		//排序
		String sort = "info.id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		List<ZcInventoryInfoEntity> infoList =  zcInventoryManagementMapper.selectInventoryInfoByCode(zcGeneralQueryEntity);
		return infoList;
	}
	/**
	 * 调整
	 */
	@Override
	public int adjust(String inventId, String mNum) {
		ZcAdjustEntity zcAdjustEntity = new ZcAdjustEntity();
		zcAdjustEntity.setInventId(Long.parseLong(inventId));
		zcAdjustEntity.setmNum(Integer.parseInt(mNum));
		return zcInventoryManagementMapper.adjust(zcAdjustEntity);
	}
	/**
	 * 库存预警
	 */
	@Override
	public int stockWarning(ZcInventoryInfoEntity zcInventoryInfoEntity) {
		return zcInventoryManagementMapper.stockWarning(zcInventoryInfoEntity);
	}
	/**
	 * 查询所有供应商
	 */
	public List<Customer> selectAllCustomerList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_customer";
		//条件
		String strWhere = "customer_type = 0 and ";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcInventoryManagementMapper.selectAllCustomerList(zcGeneralQueryEntity);
	}
	/**
	 * 移库
	 */
	@Override
	public int bindingWarehouse(Materiel materiel) {
		//查询这个物料是否绑定了库位
		if(zcInventoryManagementMapper.selectBindByMid(materiel.getId())!=0){
			return -1;
		}
		return zcInventoryManagementMapper.bindingWarehouse(materiel);
	}
	/**
	 * 通过产品码或简码查询规格型号
	 */
	@Override
	public List<Materiel> selectMaterielSizeByCode(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_materiel m left join lz_warehouse_region r on r.id = m.region_id";
		//排序
		String sort = "m.id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.selectMaterielSizeByCode(zcGeneralQueryEntity);
	}
	/********************************************APP****************************************/
	/**
	 * 通过仓库和库区名称查询库区id
	 */
	@Override
	public Long selectSrcRegionIdByName(String ware,String srcRegion) {
		return zcInventoryManagementMapper.selectSrcRegionIdByName(ware, srcRegion);
	}
	/**
	 * 通过库区和库位名称查询库位id
	 */
	@Override
	public Long selectSrcLocationIdByName(String srcRegion, String srcLocation) {
		return zcInventoryManagementMapper.selectSrcLocationIdByName(srcRegion, srcLocation);
	}
	/**
	 * 查询库存预警列表
	 */
	@Override
	public List<ZcInventoryInfoEntity> selectStockWarningList() {
		return zcInventoryManagementMapper.selectStockWarningList();
	}
	/**
	 * 通过仓库名称查询id
	 * @param ware
	 * @return
	 */
	@Override
	public Long selectSrcWareIdByName(String ware) {
		return zcInventoryManagementMapper.selectSrcWareIdByName(ware);
	}
	/**
	 * 通过产品码、规格型号查询物料信息
	 */
	@Override
	public Materiel selectMaterielInfoByCodeApp(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_materiel m";
		//排序
		String sort = "m.id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.selectMaterielInfoByCodeApp(zcGeneralQueryEntity);
	}
	/**
	 * 查询物料信息详情
	 */
	@Override
	public List<ZcMaterielAndTrayEntity> selectInventoryInfoListDetails(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_codemark cm left join zc_materiel_tray mt on CHARINDEX(','+cm.code,','+mt.materiel_code)>0 left join lz_materiel m on m.id = mt.mid left join zc_system_user u on u.id = mt.binding_person";
		//查询字段
		String strFld = "mt.*,u.userName,cm.code,cm.num";
		//排序
		String sort = " mt.tray_code ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		//条件
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		List<ZcMaterielAndTrayEntity> inventoryInfoListDetails = zcInventoryManagementMapper.selectInventoryInfoListDetails(zcGeneralQueryEntity);
		for (ZcMaterielAndTrayEntity zcMaterielAndTrayEntity : inventoryInfoListDetails) {
			ZcTrayAndLocationEntity zcTrayAndLocationEntity = new ZcTrayAndLocationEntity();
			String location_code = zcInventoryManagementMapper.selectLocationCode(zcMaterielAndTrayEntity.getTray_code());
			WarehouseRegion warehouseRegion = zcInventoryManagementMapper.selectRegionByLocationCode(location_code);
			zcTrayAndLocationEntity.setLocation_code(location_code);
			if(warehouseRegion!=null){
				zcTrayAndLocationEntity.setRegion_name(warehouseRegion.getRegion_name());
				zcTrayAndLocationEntity.setRegion_type(warehouseRegion.getRegion_type());
			}
			zcMaterielAndTrayEntity.setZcTrayAndLocation(zcTrayAndLocationEntity);
		}
		return inventoryInfoListDetails;
	}
	/**
	 * 查询物料信息详情数量
	 */
	@Override
	public int countInventoryInfoListDetails(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = " lz_codemark cm left join zc_materiel_tray mt on CHARINDEX(','+cm.code+',',','+mt.materiel_code)>0 left join lz_materiel m on m.id = mt.mid left join zc_system_user u on u.id = mt.binding_person";
		byte isGetCount = 1;
		//条件
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcInventoryManagementMapper.countInventoryInfoListDetails(zcGeneralQueryEntity);
	}
	
	/**
	 * 通过产品码或简码查询物料规格型号信息2
	 * @param response
	 * @param mcode 产品码或简码
	 * @param specification 物料规格型号
	 */
	@Override
	public List<MaterielInStock> queryMaterielBatchByMcodeSpecification(String mcode, String materiel_size, String materiel_properties) {
		return zcInventoryManagementMapper.queryMaterielBatchByMcodeSpecification(mcode, materiel_size, materiel_properties);
	}
	
	/**
	 * 根据物料ID去收货详细表查询
	 * @param id 物料id
	 * @return
	 */
	@Override
	public List<ReceiveDetail> queryReceiveDetailByMaterielId(Long mid) {
		return zcInventoryManagementMapper.queryReceiveDetailByMaterielId(mid);
	}
}