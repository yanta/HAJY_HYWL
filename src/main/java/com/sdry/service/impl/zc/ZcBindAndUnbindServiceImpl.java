package com.sdry.service.impl.zc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcBindAndUnbindMapper;
import com.sdry.mapper.zc.ZcInventoryManagementMapper;
import com.sdry.mapper.zc.ZcUpAndDownMapper;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMaterielNameAndCode;
import com.sdry.model.zc.ZcMaterileMoveInfoEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;
import com.sdry.service.zc.ZcBindAndUnbindService;

/**
 * 物料托盘绑定与解绑
 * @ClassName:    ZcBindAndUnbindServiceImpl   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年6月3日 上午10:57:42   
 * @Version:      v1.0
 */
@Service
public class ZcBindAndUnbindServiceImpl implements ZcBindAndUnbindService{

	@Resource
	private ZcBindAndUnbindMapper zcBindAndUnbindMapper;
	@Resource
	private ZcUpAndDownMapper zcUpAndDownMapper;
	@Resource
	private ZcInventoryManagementMapper zcInventoryManagementMapper;
	/**
	 * 通过物料条码（精准码）查询物料信息
	 */
	@Override
	public Materiel selectMaterielInfoByCode(String materiel_code_list) {
		String[] materiel_code_arr = materiel_code_list.split(",");
		Materiel materiel = new Materiel();
		for (String materiel_code : materiel_code_arr) {
			//查询物料条码是否已经绑定
			int isBind = zcBindAndUnbindMapper.selectBindByMaterielCode(materiel_code);
			if(isBind>0){
				materiel.setId(-1L);
				return materiel;
			}
		}
		for (int i = 0; i < materiel_code_arr.length; i++) {
			if(!"".equals(materiel_code_arr[i])){
				materiel = zcBindAndUnbindMapper.selectMaterielInfoByCode(materiel_code_arr[i]);
			}
			if(i>0 && materiel !=null){
				//后一个值
				long id1 = materiel.getId();
				String pici1 = materiel.getPici();
				//前一个值
				long id2 = zcBindAndUnbindMapper.selectMaterielInfoByCode(materiel_code_arr[i-1]).getId();
				String pici2 = zcBindAndUnbindMapper.selectMaterielInfoByCode(materiel_code_arr[i-1]).getPici();
				if(id1 != id2 || !pici1.equals(pici2)){
					materiel.setId(-2L);
					return materiel;
				}
			}
		}
		if(materiel==null){
			return null;
		}
		//良品推荐库位
		WarehouseRegionLocation warehouseRegionLocation = zcBindAndUnbindMapper.selectTopOneLocation(materiel.getRegion_id());
		//不良品推荐库位
		WarehouseRegionLocation warehouseRegionLocationBad = zcBindAndUnbindMapper.selectTopOneLocationBad();
		if(warehouseRegionLocation!=null && materiel.getIs_ng() == 0){
			materiel.setBetter_location(warehouseRegionLocation.getLocation_num());
		}
		if(warehouseRegionLocationBad!=null && materiel.getIs_ng() == 1){
			//materiel.setBetter_location("不良品"+warehouseRegionLocationBad.getLocation_num());
			materiel.setBetter_location("不良品区");
		}
		return materiel;
	}
	/**
	 * 物料托盘绑定并上架
	 */
	@Override
	public int bindingAndUp(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		String[] materiel_code_arr = zcMaterielAndTrayEntity.getMateriel_code().split(",");
		int num = 0;
		for (String string : materiel_code_arr) {
			num += zcBindAndUnbindMapper.selectNumByCode(string);
		}
		zcMaterielAndTrayEntity.setNum(num);
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
		int affact = 0;
		int affact_res =0;
		if(zcMaterielAndTrayEntityOld == null){
			affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
		}else{
			affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
		}
		//绑定托盘完成
		if(affact > 0){
			//绑定库位
			//查询上架的托盘是否已上架
			int yetOn = zcUpAndDownMapper.selectYetOnByTrayCode(zcMaterielAndTrayEntity.getTray_code());
			if(yetOn>0){
				//如果上架失败，回退
				unbindMaterAndTray(zcMaterielAndTrayEntity);
				return -3;
			}
			//查询这个物料的所属库区id
			long region_id = zcUpAndDownMapper.selectRegionIdByMid(zcUpAndDownMapper.selectMid(zcMaterielAndTrayEntity.getTray_code()));
			String storage_type = zcUpAndDownMapper.selectStorageTypeByMid(zcMaterielAndTrayEntity.getMid());
			//普通入库不让上架
			if("0".equals(storage_type)){
				//如果上架失败，回退
				unbindMaterAndTray(zcMaterielAndTrayEntity);
				return -4;
			}
			//上架前查询库位是否存在
			WarehouseRegionLocation location = new WarehouseRegionLocation();
			location.setLocation_num(zcMaterielAndTrayEntity.getLocation_code());
			location.setRegion_id(region_id);
			WarehouseRegionLocation locationIsExist = zcUpAndDownMapper.selectLocationIsExist(location);
			if(locationIsExist==null && zcMaterielAndTrayEntity.getIs_ng() ==0){
				//如果上架失败，回退
				unbindMaterAndTray(zcMaterielAndTrayEntity);
				return -5;
			}
			//物料是不良品
			if(zcMaterielAndTrayEntity.getIs_ng() ==1){
				//查询库位码的库区是否是不良区
				int isBad = zcBindAndUnbindMapper.selectBadRegionByLocation(zcMaterielAndTrayEntity.getLocation_code());
				//库区是良品区
				if(isBad == 0){
					//如果上架失败，回退
					unbindMaterAndTray(zcMaterielAndTrayEntity);
					return -6;
				}
			}
			int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
			zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
			if(count == 0){
				//库位为空新增
				affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
			}else{
				//库位不为空时查询托盘是否存在
				int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
				if(countByTray>0){
					affact_res=1;
				}else{
					affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
				}
			}
		}
		if(affact_res==0){
			unbindMaterAndTray(zcMaterielAndTrayEntity);
		}
		return affact_res;
	}
	/**
	 * 通过物料条码查询物料所在位置
	 */
	@Override
	public ZcMaterielAndTrayEntity selectMaterielLocationByCode(String materiel_code_list) {
		String[] materiel_code_arr = materiel_code_list.split(",");
		String materiel_code = materiel_code_arr[0];
		List<ZcMaterielAndTrayEntity> zcMaterielAndTrayEntityList = zcBindAndUnbindMapper.selectMaterielLocationByCodeNew(materiel_code);
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = new ZcMaterielAndTrayEntity();
		String materiel_code1="";
		String location_code="";
		for (ZcMaterielAndTrayEntity zcMaterielAndTrayEntity2 : zcMaterielAndTrayEntityList) {
			materiel_code1+=zcMaterielAndTrayEntity2.getMateriel_code();
			location_code = zcMaterielAndTrayEntity2.getLocation_code();
		}
		zcMaterielAndTrayEntity.setMateriel_code(materiel_code1);
		zcMaterielAndTrayEntity.setLocation_code(location_code);
		return zcMaterielAndTrayEntity;
	}
	/**
	 * 解绑
	 */
	@Override
	public int downAndUnbind(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
		String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
		int num = 0;
		for (String string : materiel_code_old) {
			num += zcBindAndUnbindMapper.selectNumByCode(string);
		}
		String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
		String new_materiel_code_list="";
		/*for(int i = 0; i < materiel_code_old.length; i++){
			for(int j = 0; j < materiel_code_new.length; j++){
				if(materiel_code_new[j].equals(materiel_code_old[i])){
					materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
				}
			}
		}
		for (String string : materiel_code_old) {
			new_materiel_code_list += string+",";
		}*/
		
		//---------------------jyy-----------------------
		List<String> listX19 = new ArrayList<>();
		
		for(int i = 0; i < materiel_code_old.length; i++){
			int fag = 0;
			for(int j = 0; j < materiel_code_new.length; j++){
				if(materiel_code_new[j].equals(materiel_code_old[i])){
					fag = 1;
				}
			}
			
			if(fag == 0) {
				listX19.add(materiel_code_old[i]);
			}
		}
		
		
		for (String s : listX19) {
			new_materiel_code_list += s+",";
		}
		
		//---------------------jyy---------------------
		
		zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
		ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
		a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
		ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
		String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
		String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
		String new_tray_code_list="";
		//需下架的托盘码
		/*for(int i = 0; i < tray_code_old.length; i++){
			for(int j = 0; j < tray_code_new.length; j++){
				if(tray_code_new[j].equals(tray_code_old[i])){
					tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
				}
			}
		}
		for (String string : tray_code_old) {
			new_tray_code_list += string+",";
		}*/
		
		//---------------------jyy-----------------------
		List<String> listX20 = new ArrayList<>();
		
		for(int i = 0; i < tray_code_old.length; i++){
			int fag = 0;
			for(int j = 0; j < tray_code_new.length; j++){
				if(tray_code_new[j].equals(tray_code_old[i])){
					fag = 1;
				}
			}
			
			if(fag == 0) {
				listX20.add(tray_code_old[i]);
			}
		}
		
		for (String s : listX20) {
			new_tray_code_list += s+",";
		}
		
		//---------------------jyy---------------------
		
		
		zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
		zcMaterielAndTrayEntity.setNum(num);
		int affact = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
		//删除空库位空托盘
		zcBindAndUnbindMapper.deleteEmpty();
		if(affact>0){
			//记录下架记录
			zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
		}
		return affact;
	}
	/**
	 * 解绑托盘物料
	 * @param zcMaterielAndTrayEntity
	 */
	public void unbindMaterAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity){
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld1 = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
		String[] materiel_code_old = zcMaterielAndTrayEntityOld1.getMateriel_code().split(",");
		String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
		String new_materiel_code_list="";
		/*for(int i = 0; i < materiel_code_old.length; i++){
			for(int j = 0; j < materiel_code_new.length; j++){
				if(materiel_code_new[j].equals(materiel_code_old[i])){
					materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
				}
			}
		}
		for (String string : materiel_code_old) {
			new_materiel_code_list += string+",";
		}*/
		
		//---------------------jyy-----------------------
		List<String> listX21 = new ArrayList<>();
		
		for(int i = 0; i < materiel_code_old.length; i++){
			int fag = 0;
			for(int j = 0; j < materiel_code_new.length; j++){
				if(materiel_code_new[j].equals(materiel_code_old[i])){
					fag = 1;
				}
			}
			
			if(fag == 0) {
				listX21.add(materiel_code_old[i]);
			}
		}
		
		for (String s : listX21) {
			new_materiel_code_list += s+",";
		}
		
		//---------------------jyy---------------------
		
		zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
		zcBindAndUnbindMapper.unbindMaterielAndTray(zcMaterielAndTrayEntity);
		//删除空库位空托盘
		zcBindAndUnbindMapper.deleteEmpty();
	}
	/**
	 * 条件查询库位信息
	 */
	@Override
	public List<WarehouseRegionLocation> selectTopLocation(Materiel materiel) {
		List<WarehouseRegionLocation> warehouseRegionLocation  = null;
		if(materiel.getRegion_id() == null&& "".equals(materiel.getMateriel_num())&& "".equals(materiel.getMateriel_size()) && "".equals(materiel.getMateriel_properties())){
			//全查
			warehouseRegionLocation = zcUpAndDownMapper.selectTopLocationAllRegion();
		}else if(materiel.getRegion_id() != null && "".equals(materiel.getMateriel_num())&& "".equals(materiel.getMateriel_size()) && "".equals(materiel.getMateriel_properties())){
			//通过库区id查询
			warehouseRegionLocation = zcUpAndDownMapper.selectTopLocation(materiel.getRegion_id());
		}else{
			//通过物料产品码，规格型号查询库区id
			Long region_id = zcUpAndDownMapper.selectRegionId(materiel);
			//通过库区id查询这个库区下最优的库位
			warehouseRegionLocation = zcUpAndDownMapper.selectTopLocation(region_id);
		}
		for (WarehouseRegionLocation warehouseRegionLocation2 : warehouseRegionLocation) {
			List<String> materiel_code_list = zcUpAndDownMapper.selectCodeByLocationCode(warehouseRegionLocation2.getLocation_num());
			String materiel_code_arr="";
			int count = 0;
			for (String string : materiel_code_list) {
				materiel_code_arr+=string;
				//通过code查询物料名称和数量
				String[] x = string.split(",");
				for (String string2 : x) {
					ZcMaterielNameAndCode zcMaterielNameAndCode1 =  zcBindAndUnbindMapper.selectNameByCode(string2);
					if(zcMaterielNameAndCode1!=null){
						count += zcMaterielNameAndCode1.getNum();
					}
				}
			}
			warehouseRegionLocation2.setMateriel_code(materiel_code_arr);
			warehouseRegionLocation2.setTotal_capacity(count+"");
			String[] materiel_code_a = materiel_code_arr.split(",");
			String materiel_name = "";
			List<ZcMaterielNameAndCode> zcMaterielNameAndCode =  new ArrayList<ZcMaterielNameAndCode>();
			for (String materiel_code : materiel_code_a) {
				//通过code查询物料名称和数量
				ZcMaterielNameAndCode zcMaterielNameAndCode1 =  zcBindAndUnbindMapper.selectNameByCode(materiel_code);
				if(zcMaterielNameAndCode1!= null){
					zcMaterielNameAndCode1.setMateriel_code(materiel_code);
				}
				zcMaterielNameAndCode.add(zcMaterielNameAndCode1);
			}
			warehouseRegionLocation2.setZcMaterielNameAndCode(zcMaterielNameAndCode);
		}
		return warehouseRegionLocation;
	}
	/**
	 * 查询全部库区
	 * @return
	 */
	@Override
	public List<WarehouseRegion> selectAllRegion() {
		return zcUpAndDownMapper.selectAllRegion();
	}
	/**
	 * 下架库位推荐（先进先出）
	 */
	@Override
	public String selectTopLocationDown(String materiel_num) {
		//通过产品码查询物料所在最早库位
		String location_name = "";
		WarehouseRegionLocation warehouseRegionLocation = zcBindAndUnbindMapper.selectTopLocationDown(materiel_num);
		if(warehouseRegionLocation!=null){
			location_name = warehouseRegionLocation.getLocation_name();
		}
		return location_name;
	}
	/**
	 * 区域库存的移动
	 */
	@Override
	public int bindAndUp2SpecialArea(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		//条码集合
		String[] materiel_code_arr = zcMaterielAndTrayEntity.getMateriel_code().split(",");
		List<ZcMaterileMoveInfoEntity> zcMaterileMoveInfoList = new ArrayList<ZcMaterileMoveInfoEntity>();
		List<ZcMaterileMoveInfoEntity> zcMaterileMoveInfoList1 = new ArrayList<ZcMaterileMoveInfoEntity>();
		
		int num =0;
		String materiel_code="";
		//物料原区域
		String srcRegion="";
		for (int i = 0; i < materiel_code_arr.length; i++) {
			//查询条码的库位
			ZcMaterielAndTrayEntity z = zcBindAndUnbindMapper.selectMaterielLocationByCode(materiel_code_arr[0]);
			//查询这个库位所在的库区区域和库区类型
			long region_type = 0L;
			if (z != null) {
				WarehouseRegion warehouseRegion = zcInventoryManagementMapper.selectRegionByLocationCode(z.getLocation_code());
				//原库区
				srcRegion = warehouseRegion.getRegion_name();
				//库区类型
				region_type = warehouseRegion.getRegion_type();
			}
			
			if(region_type == -1){
				srcRegion = "不良品区";
			}else if(srcRegion==null){
				srcRegion="";
			}else if(!"".equals(srcRegion) &&!"待检区".equals(srcRegion) && !"损溢区".equals(srcRegion) && !"缓存区".equals(srcRegion)){
				srcRegion = "良品区";
			}
			//入库时没原区域的
			if("".equals(srcRegion)){
				ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity = new ZcMaterileMoveInfoEntity();
				if(i==0){
					zcMaterileMoveInfoEntity = zcBindAndUnbindMapper.selectMaterileMoveInfoByCode(materiel_code_arr[i]);
					if(null != zcMaterileMoveInfoEntity){
						num = zcMaterileMoveInfoEntity.getNum();
					}else{
						zcMaterileMoveInfoEntity = new ZcMaterileMoveInfoEntity();
					}
					zcMaterileMoveInfoEntity.setNum(num);
					materiel_code = materiel_code_arr[i]+",";
					zcMaterileMoveInfoEntity.setMateriel_code(materiel_code);
					if(materiel_code_arr.length==1){
						zcMaterileMoveInfoList.add(zcMaterileMoveInfoEntity);
					}
				}else{
					ZcMaterileMoveInfoEntity last = zcBindAndUnbindMapper.selectMaterileMoveInfoByCode(materiel_code_arr[i-1]);
					ZcMaterileMoveInfoEntity now = zcBindAndUnbindMapper.selectMaterileMoveInfoByCode(materiel_code_arr[i]);
					if(last.getMid().equals(now.getMid())  && last.getPici().equals(now.getPici())){
						num += now.getNum();
						materiel_code+=materiel_code_arr[i]+",";
					}else{
						last.setNum(num);
						last.setMateriel_code(materiel_code);
						zcMaterileMoveInfoList.add(last);
						num = now.getNum();
						materiel_code = materiel_code_arr[i]+",";
					}
					if(materiel_code_arr.length==i+1){
						now.setNum(num);
						now.setMateriel_code(materiel_code);
						zcMaterileMoveInfoList.add(now);
					}
				}
			}else{
				//有原区域的
				ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity1 = new ZcMaterileMoveInfoEntity();
				if(i==0){
					//根据条码查询原托盘信息
					zcMaterileMoveInfoEntity1 = zcBindAndUnbindMapper.selectMaterileMoveInfoByCodeSrc(materiel_code_arr[i]);
					if(null != zcMaterileMoveInfoEntity1){
						num = zcMaterileMoveInfoEntity1.getNum();
					}else{
						zcMaterileMoveInfoEntity1 = new ZcMaterileMoveInfoEntity();
					}
					num = zcMaterileMoveInfoEntity1.getNum();
					zcMaterileMoveInfoEntity1.setNum(num);
					materiel_code = materiel_code_arr[i]+",";
					zcMaterileMoveInfoEntity1.setMateriel_code(materiel_code);
					//--------------------------------------------------------
					if(materiel_code_arr.length==1){
						zcMaterileMoveInfoList1.add(zcMaterileMoveInfoEntity1);
					}
					//-------------------------------------------------------------
				}else{
					ZcMaterileMoveInfoEntity last = zcBindAndUnbindMapper.selectMaterileMoveInfoByCodeSrc(materiel_code_arr[i-1]);
					ZcMaterileMoveInfoEntity now = zcBindAndUnbindMapper.selectMaterileMoveInfoByCodeSrc(materiel_code_arr[i]);
					if(last.getMid().equals(now.getMid())  && last.getPici().equals(now.getPici())&& last.getTray_code().equals(now.getTray_code())){
						num += now.getNum();
						materiel_code+=materiel_code_arr[i]+",";
					}else{
						last.setNum(num);
						last.setMateriel_code(materiel_code);
						zcMaterileMoveInfoList1.add(last);
						num = now.getNum();
						materiel_code = materiel_code_arr[i]+",";
					}
					if(materiel_code_arr.length==i+1){
						now.setNum(num);
						now.setMateriel_code(materiel_code);
						zcMaterileMoveInfoList1.add(now);
					}
				}
			}
		}
		//现在zcMaterileMoveInfoList里面有物料的id和批次，总数量，开始移动
		int affact_res=0;
		if("".equals(srcRegion)){
			for (ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity : zcMaterileMoveInfoList) {
				zcMaterileMoveInfoEntity.setEnterDate(zcMaterielAndTrayEntity.getBinding_date());
				zcMaterileMoveInfoEntity.setEnterPerson(zcMaterielAndTrayEntity.getBinding_person());
				zcMaterielAndTrayEntity.setMid(zcMaterileMoveInfoEntity.getMid());
				zcMaterielAndTrayEntity.setmBatch(zcMaterileMoveInfoEntity.getPici());
				zcMaterielAndTrayEntity.setMateriel_code(zcMaterileMoveInfoEntity.getMateriel_code());
				int numCode = 0;
				String[] newMateriel_code_arr = zcMaterileMoveInfoEntity.getMateriel_code().split(",");
				if("".equals(srcRegion)&&"待检区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					zcMaterielAndTrayEntity.setLocation_code("DJ");
					for (String string : newMateriel_code_arr) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					zcMaterielAndTrayEntity.setNum(numCode);
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
					//待检区入库
					int affact_in = zcBindAndUnbindMapper.insertQualityWare(zcMaterileMoveInfoEntity);
					if(affact_in>0){
						int affact = 0;
						if(zcMaterielAndTrayEntityOld == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}
			}
		}else{
			for (ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity : zcMaterileMoveInfoList1) {
				zcMaterileMoveInfoEntity.setEnterDate(zcMaterielAndTrayEntity.getBinding_date());
				zcMaterileMoveInfoEntity.setEnterPerson(zcMaterielAndTrayEntity.getBinding_person());
				zcMaterielAndTrayEntity.setMid(zcMaterileMoveInfoEntity.getMid());
				zcMaterielAndTrayEntity.setmBatch(zcMaterileMoveInfoEntity.getPici());
				zcMaterielAndTrayEntity.setMateriel_code(zcMaterileMoveInfoEntity.getMateriel_code());
				int numCode = 0;
				String[] newMateriel_code_arr = zcMaterileMoveInfoEntity.getMateriel_code().split(",");
				if("待检区".equals(srcRegion)&&"良品区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//保存目标库位条码
					String location_code = zcMaterielAndTrayEntity.getLocation_code();
					//从待检区解绑
					zcMaterielAndTrayEntity.setLocation_code("DJ");
					int unbindQuality = 0;
					
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
								i=i-1;
							}
						}
						
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}
					*/
					
					//---------------------jyy-----------------------
					List<String> listX1 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX1.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX1) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindQuality = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindQuality>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从待检区出库
					int outQuality = 0;
					if(unbindQuality>0){
						outQuality = zcBindAndUnbindMapper.outQuality(zcMaterileMoveInfoEntity);
					}
					//入良品区域
					int inGood=0;
					if(outQuality>0){
						inGood = zcBindAndUnbindMapper.inGood(zcMaterileMoveInfoEntity); 
					}
					//绑定良品区
					if(inGood>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code(location_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setNum(numBind);
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldBind = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldBind == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						//绑定托盘完成
						if(affact > 0){
							//绑定库位
							//查询上架的托盘是否已上架
							int yetOn = zcUpAndDownMapper.selectYetOnByTrayCode(zcMaterielAndTrayEntity.getTray_code());
							if(yetOn>0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -3;
							}
							//查询这个物料的所属库区id
							long region_id = zcUpAndDownMapper.selectRegionIdByMid(zcUpAndDownMapper.selectMid(zcMaterielAndTrayEntity.getTray_code()));
							String storage_type = zcUpAndDownMapper.selectStorageTypeByMid(zcMaterielAndTrayEntity.getMid());
							//普通入库不让上架
							if("0".equals(storage_type)){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -4;
							}
							//上架前查询库位是否存在
							WarehouseRegionLocation location = new WarehouseRegionLocation();
							location.setLocation_num(zcMaterielAndTrayEntity.getLocation_code());
							location.setRegion_id(region_id);
							WarehouseRegionLocation locationIsExist = zcUpAndDownMapper.selectLocationIsExist(location);
							if(locationIsExist==null && zcMaterielAndTrayEntity.getIs_ng() ==0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -5;
							}
							//物料是不良品
							if(zcMaterielAndTrayEntity.getIs_ng() ==1){
								//查询库位码的库区是否是不良区
								int isBad = zcBindAndUnbindMapper.selectBadRegionByLocation(zcMaterielAndTrayEntity.getLocation_code());
								//库区是良品区
								if(isBad == 0){
									//如果上架失败，回退
									unbindMaterAndTray(zcMaterielAndTrayEntity);
									return -6;
								}
							}
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
					
				}else if("待检区".equals(srcRegion)&&"不良品区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					zcMaterielAndTrayEntity.setIs_ng(1);
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//保存目标库位条码
					String location_code = zcMaterielAndTrayEntity.getLocation_code();
					//从待检区解绑
					zcMaterielAndTrayEntity.setLocation_code("DJ");
					int unbindQuality = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
				/*	for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX22 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX22.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX22) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX2 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX2.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX2) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindQuality = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindQuality>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从待检区出库
					int outQuality = 0;
					if(unbindQuality>0){
						outQuality = zcBindAndUnbindMapper.outQuality(zcMaterileMoveInfoEntity);
					}
					//入不良品区域
					int inBad=0;
					if(outQuality>0){
						inBad = zcBindAndUnbindMapper.inBad(zcMaterileMoveInfoEntity); 
					}
					//绑定不良品区
					if(inBad>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code(location_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setNum(numBind);
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldBind = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldBind == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						//绑定托盘完成
						if(affact > 0){
							//绑定库位
							//查询上架的托盘是否已上架
							int yetOn = zcUpAndDownMapper.selectYetOnByTrayCode(zcMaterielAndTrayEntity.getTray_code());
							if(yetOn>0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -3;
							}
							//查询这个物料的所属库区id
							long region_id = zcUpAndDownMapper.selectRegionIdByMid(zcUpAndDownMapper.selectMid(zcMaterielAndTrayEntity.getTray_code()));
							String storage_type = zcUpAndDownMapper.selectStorageTypeByMid(zcMaterielAndTrayEntity.getMid());
							//普通入库不让上架
							if("0".equals(storage_type)){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -4;
							}
							//上架前查询库位是否存在
							WarehouseRegionLocation location = new WarehouseRegionLocation();
							location.setLocation_num(zcMaterielAndTrayEntity.getLocation_code());
							location.setRegion_id(region_id);
							WarehouseRegionLocation locationIsExist = zcUpAndDownMapper.selectLocationIsExist(location);
							if(locationIsExist==null && zcMaterielAndTrayEntity.getIs_ng() ==0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -5;
							}
							//物料是不良品
							if(zcMaterielAndTrayEntity.getIs_ng() ==1){
								//查询库位码的库区是否是不良区
								int isBad = zcBindAndUnbindMapper.selectBadRegionByLocation(zcMaterielAndTrayEntity.getLocation_code());
								//库区是良品区
								if(isBad == 0){
									//如果上架失败，回退
									unbindMaterAndTray(zcMaterielAndTrayEntity);
									return -6;
								}
							}
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("良品区".equals(srcRegion)&&"缓存区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//从良品区解绑
					int unbindGood = 0;
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX3 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX3.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX3) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX4 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX4.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX4) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindGood = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindGood>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从良品库出库
					int outGood=0;
					if(unbindGood>0){
						outGood=zcBindAndUnbindMapper.outGood(zcMaterileMoveInfoEntity);
					}
					//入缓存区库存
					int inCache = 0;
					if(outGood>0){
						inCache = zcBindAndUnbindMapper.inCache(zcMaterileMoveInfoEntity);
					}
					//绑定缓存区
					if(inCache>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code("HC");
						String tray_code = zcMaterielAndTrayEntity.getTray_code();
						zcMaterielAndTrayEntity.setTray_code(tray_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						zcMaterielAndTrayEntity.setNum(numBind);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldCache = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldCache == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
					
				}else if("不良品区".equals(srcRegion)&&"缓存区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//从不良品区解绑
					int unbindBad = 0;
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					
					//---------------------jyy-----------------------
					List<String> listX5 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX5.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX5) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					

					//---------------------jyy-----------------------
					List<String> listX6 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX6.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX6) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindBad = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindBad>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从不良品库出库
					int outBad=0;
					if(unbindBad>0){
						outBad=zcBindAndUnbindMapper.outBad(zcMaterileMoveInfoEntity);
					}
					//入缓存区库存
					int inCache = 0;
					if(outBad>0){
						inCache = zcBindAndUnbindMapper.inCache(zcMaterileMoveInfoEntity);
					}
					//绑定缓存区
					if(inCache>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code("HC");
						String tray_code = zcMaterielAndTrayEntity.getTray_code();
						zcMaterielAndTrayEntity.setTray_code(tray_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						zcMaterielAndTrayEntity.setNum(numBind);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldCache = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldCache == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("缓存区".equals(srcRegion)&&"良品区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//保存目标库位条码
					String location_code = zcMaterielAndTrayEntity.getLocation_code();
					//从缓存区解绑
					zcMaterielAndTrayEntity.setLocation_code("HC");
					int unbindCache = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					
					//---------------------jyy-----------------------
					List<String> listX7 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX7.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX7) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX8 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX8.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX8) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindCache = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindCache>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从缓存区出库
					int outCache = 0;
					if(unbindCache>0){
						outCache = zcBindAndUnbindMapper.outCache(zcMaterileMoveInfoEntity);
					}
					//入良品区域
					int inGood=0;
					if(outCache>0){
						inGood = zcBindAndUnbindMapper.inGood(zcMaterileMoveInfoEntity); 
					}
					//绑定良品区
					if(inGood>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code(location_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setNum(numBind);
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldBind = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldBind == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						//绑定托盘完成
						if(affact > 0){
							//绑定库位
							//查询上架的托盘是否已上架
							int yetOn = zcUpAndDownMapper.selectYetOnByTrayCode(zcMaterielAndTrayEntity.getTray_code());
							if(yetOn>0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -3;
							}
							//查询这个物料的所属库区id
							long region_id = zcUpAndDownMapper.selectRegionIdByMid(zcUpAndDownMapper.selectMid(zcMaterielAndTrayEntity.getTray_code()));
							String storage_type = zcUpAndDownMapper.selectStorageTypeByMid(zcMaterielAndTrayEntity.getMid());
							//普通入库不让上架
							if("0".equals(storage_type)){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -4;
							}
							//上架前查询库位是否存在
							WarehouseRegionLocation location = new WarehouseRegionLocation();
							location.setLocation_num(zcMaterielAndTrayEntity.getLocation_code());
							location.setRegion_id(region_id);
							WarehouseRegionLocation locationIsExist = zcUpAndDownMapper.selectLocationIsExist(location);
							if(locationIsExist==null && zcMaterielAndTrayEntity.getIs_ng() ==0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -5;
							}
							//物料是不良品
							if(zcMaterielAndTrayEntity.getIs_ng() ==1){
								//查询库位码的库区是否是不良区
								int isBad = zcBindAndUnbindMapper.selectBadRegionByLocation(zcMaterielAndTrayEntity.getLocation_code());
								//库区是良品区
								if(isBad == 0){
									//如果上架失败，回退
									unbindMaterAndTray(zcMaterielAndTrayEntity);
									return -6;
								}
							}
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("缓存区".equals(srcRegion)&&"不良品区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					zcMaterielAndTrayEntity.setIs_ng(1);
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//保存目标库位条码
					String location_code = zcMaterielAndTrayEntity.getLocation_code();
					//从缓存区解绑
					zcMaterielAndTrayEntity.setLocation_code("HC");
					int unbindCache = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					
					//---------------------jyy-----------------------
					List<String> listX9 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX9.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX9) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX10 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX10.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX10) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindCache = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindCache>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从缓存区出库
					int outCache = 0;
					if(unbindCache>0){
						outCache = zcBindAndUnbindMapper.outCache(zcMaterileMoveInfoEntity);
					}
					//入不良品区域
					int inBad=0;
					if(outCache>0){
						inBad = zcBindAndUnbindMapper.inBad(zcMaterileMoveInfoEntity); 
					}
					//绑定不良品区
					if(inBad>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code(location_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setNum(numBind);
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldBind = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldBind == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						//绑定托盘完成
						if(affact > 0){
							//绑定库位
							//查询上架的托盘是否已上架
							int yetOn = zcUpAndDownMapper.selectYetOnByTrayCode(zcMaterielAndTrayEntity.getTray_code());
							if(yetOn>0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -3;
							}
							//查询这个物料的所属库区id
							long region_id = zcUpAndDownMapper.selectRegionIdByMid(zcUpAndDownMapper.selectMid(zcMaterielAndTrayEntity.getTray_code()));
							String storage_type = zcUpAndDownMapper.selectStorageTypeByMid(zcMaterielAndTrayEntity.getMid());
							//普通入库不让上架
							if("0".equals(storage_type)){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -4;
							}
							//上架前查询库位是否存在
							WarehouseRegionLocation location = new WarehouseRegionLocation();
							location.setLocation_num(zcMaterielAndTrayEntity.getLocation_code());
							location.setRegion_id(region_id);
							WarehouseRegionLocation locationIsExist = zcUpAndDownMapper.selectLocationIsExist(location);
							if(locationIsExist==null && zcMaterielAndTrayEntity.getIs_ng() ==0){
								//如果上架失败，回退
								unbindMaterAndTray(zcMaterielAndTrayEntity);
								return -5;
							}
							//物料是不良品
							if(zcMaterielAndTrayEntity.getIs_ng() ==1){
								//查询库位码的库区是否是不良区
								int isBad = zcBindAndUnbindMapper.selectBadRegionByLocation(zcMaterielAndTrayEntity.getLocation_code());
								//库区是良品区
								if(isBad == 0){
									//如果上架失败，回退
									unbindMaterAndTray(zcMaterielAndTrayEntity);
									return -6;
								}
							}
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("缓存区".equals(srcRegion)&&"损溢区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//从缓存区解绑
					zcMaterielAndTrayEntity.setLocation_code("HC");
					int unbindCache = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX11 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX11.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX11) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy----------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX12 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX12.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX12) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindCache = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindCache>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从缓存区出库
					int outCache = 0;
					if(unbindCache>0){
						outCache = zcBindAndUnbindMapper.outCache(zcMaterileMoveInfoEntity);
					}
					//入损益区库存
					int inProfitAndLoss = 0;
					if(outCache>0){
						inProfitAndLoss = zcBindAndUnbindMapper.inProfitAndLoss(zcMaterileMoveInfoEntity);
					}
					//绑定损益区
					if(inProfitAndLoss>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code("SY");
						String tray_code = zcMaterielAndTrayEntity.getTray_code();
						zcMaterielAndTrayEntity.setTray_code(tray_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						zcMaterielAndTrayEntity.setNum(numBind);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldCache = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldCache == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("损溢区".equals(srcRegion)&&"缓存区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//从损益区解绑
					zcMaterielAndTrayEntity.setLocation_code("SY");
					int unbindProfitAndLoss = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX13 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX13.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX13) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX14 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX14.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX14) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindProfitAndLoss = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindProfitAndLoss>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从损益区出库
					int outProfitAndLoss = 0;
					if(unbindProfitAndLoss>0){
						outProfitAndLoss = zcBindAndUnbindMapper.outProfitAndLoss(zcMaterileMoveInfoEntity);
					}
					//入缓存区库存
					int inCache = 0;
					if(outProfitAndLoss>0){
						inCache = zcBindAndUnbindMapper.inCache(zcMaterileMoveInfoEntity);
					}
					//绑定缓存区
					if(inCache>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code("HC");
						String tray_code = zcMaterielAndTrayEntity.getTray_code();
						zcMaterielAndTrayEntity.setTray_code(tray_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						zcMaterielAndTrayEntity.setNum(numBind);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldCache = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldCache == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("待检区".equals(srcRegion)&&"缓存区".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//上架托盘条码
					String upTrayCode = zcMaterielAndTrayEntity.getTray_code();
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//从待检区解绑
					zcMaterielAndTrayEntity.setLocation_code("DJ");
					int unbindQuality = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX15 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX15.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX15) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX16 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX16.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX16) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindQuality = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindQuality>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从待检区出库
					int outQuality = 0;
					if(unbindQuality>0){
						outQuality = zcBindAndUnbindMapper.outQuality(zcMaterileMoveInfoEntity);
					}
					//入缓存区库存
					int inCache = 0;
					if(outQuality>0){
						inCache = zcBindAndUnbindMapper.inCache(zcMaterileMoveInfoEntity);
					}
					//绑定缓存区
					if(inCache>0){
						zcMaterielAndTrayEntity.setTray_code(upTrayCode);
						zcMaterielAndTrayEntity.setLocation_code("HC");
						String tray_code = zcMaterielAndTrayEntity.getTray_code();
						zcMaterielAndTrayEntity.setTray_code(tray_code);
						int numBind=0;
						String newCode="";
						for (String string : newMateriel_code_arr) {
							numBind += zcBindAndUnbindMapper.selectNumByCode(string);
							newCode+=string+",";
						}
						zcMaterielAndTrayEntity.setMateriel_code(newCode);
						zcMaterielAndTrayEntity.setNum(numBind);
						ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOldCache = zcBindAndUnbindMapper.selectMaterielAndTray(zcMaterielAndTrayEntity);
						int affact = 0;
						if(zcMaterielAndTrayEntityOldCache == null){
							affact = zcBindAndUnbindMapper.bindingMaterielAndTray(zcMaterielAndTrayEntity);
						}else{
							affact = zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
						}
						if(affact>0){
							//绑定库位
							int count = zcUpAndDownMapper.countTrayAndLocation(zcMaterielAndTrayEntity);
							zcMaterielAndTrayEntity.setTray_code(zcMaterielAndTrayEntity.getTray_code());
							if(count == 0){
								//库位为空新增
								affact_res = zcUpAndDownMapper.bindingTrayAndLocation(zcMaterielAndTrayEntity);
							}else{
								//库位不为空时查询托盘是否存在
								int countByTray = zcBindAndUnbindMapper.selectCountByTray(zcMaterielAndTrayEntity.getTray_code());
								if(countByTray>0){
									affact_res=1;
								}else{
									affact_res = zcUpAndDownMapper.addTrayAndLocation(zcMaterielAndTrayEntity);
								}
							}
						}
						if(affact_res==0){
							unbindMaterAndTray(zcMaterielAndTrayEntity);
						}
					}
				}else if("缓存区".equals(srcRegion)&&"".equals(zcMaterielAndTrayEntity.getTargetRegion())){
					//获取原托盘条码
					String srcTrayCode = zcBindAndUnbindMapper.selectTrayCodeByMcode(zcMaterielAndTrayEntity.getMateriel_code().split(",")[0]);
					zcMaterielAndTrayEntity.setTray_code(srcTrayCode);
					//从缓存区解绑
					zcMaterielAndTrayEntity.setLocation_code("HC");
					int unbindCache = 0;
					ZcMaterielAndTrayEntity zcMaterielAndTrayEntityOld = zcBindAndUnbindMapper.selectMaterielCodeByTrayCode(zcMaterielAndTrayEntity);
					String[] materiel_code_old = zcMaterielAndTrayEntityOld.getMateriel_code().split(",");
					String[] materiel_code_new = zcMaterielAndTrayEntity.getMateriel_code().split(",");
					for (String string : materiel_code_new) {
						numCode += zcBindAndUnbindMapper.selectNumByCode(string);
					}
					String new_materiel_code_list="";
					/*for(int i = 0; i < materiel_code_old.length; i++){
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								materiel_code_old = org.apache.commons.lang3.ArrayUtils.remove(materiel_code_old, i);
							}
						}
					}
					for (String string : materiel_code_old) {
						new_materiel_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX17 = new ArrayList<>();
					
					for(int i = 0; i < materiel_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < materiel_code_new.length; j++){
							if(materiel_code_new[j].equals(materiel_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX17.add(materiel_code_old[i]);
						}
					}
					
					
					for (String s : listX17) {
						new_materiel_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setMateriel_code(new_materiel_code_list);
					ZcTrayAndLocationEntity a = new ZcTrayAndLocationEntity();
					a.setLocation_code(zcMaterielAndTrayEntity.getLocation_code());
					ZcTrayAndLocationEntity zcTrayAndLocationEntityOld = zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
					String[] tray_code_old = zcTrayAndLocationEntityOld.getTray_code().split(",");
					String[] tray_code_new = zcMaterielAndTrayEntity.getTray_code().split(",");
					String new_tray_code_list="";
					//需下架的托盘码
					/*for(int i = 0; i < tray_code_old.length; i++){
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								tray_code_old = org.apache.commons.lang3.ArrayUtils.remove(tray_code_old, i);
							}
						}
					}
					for (String string : tray_code_old) {
						new_tray_code_list += string+",";
					}*/
					
					//---------------------jyy-----------------------
					List<String> listX18 = new ArrayList<>();
					
					for(int i = 0; i < tray_code_old.length; i++){
						int fag = 0;
						for(int j = 0; j < tray_code_new.length; j++){
							if(tray_code_new[j].equals(tray_code_old[i])){
								fag = 1;
							}
						}
						
						if(fag == 0) {
							listX18.add(tray_code_old[i]);
						}
					}
					
					
					for (String s : listX18) {
						new_tray_code_list += s+",";
					}
					
					//---------------------jyy---------------------
					
					zcMaterielAndTrayEntity.setNew_materiel_code_list(new_tray_code_list);
					zcMaterielAndTrayEntity.setNum(numCode);
					unbindCache = zcBindAndUnbindMapper.downAndUnbind(zcMaterielAndTrayEntity);
					//删除空库位空托盘
					zcBindAndUnbindMapper.deleteEmpty();
					if(unbindCache>0){
						//记录下架记录
						zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
					}
					//从缓存区出库
					int outCache = 0;
					if(unbindCache>0){
						outCache = zcBindAndUnbindMapper.outCache(zcMaterileMoveInfoEntity);
					}
					affact_res=outCache;
				}
			}
		}
		return affact_res;
	}
	/**
	 * 扫码查询位置（下架）
	 */
	@Override
	public ZcMaterielAndTrayEntity selectMaterielLocationByCodeDown(String materiel_code_list) {
		String[] materiel_code_arr = materiel_code_list.split(",");
		String materiel_code = materiel_code_arr[0];
		List<ZcMaterielAndTrayEntity> zcMaterielAndTrayEntityList = zcBindAndUnbindMapper.selectMaterielLocationByCodeDown(materiel_code);
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = new ZcMaterielAndTrayEntity();
		String materiel_code1="";
		String location_code="";
		//丹丹新增
		int location_count=0;
		String location_type="";
		for (ZcMaterielAndTrayEntity zcMaterielAndTrayEntity2 : zcMaterielAndTrayEntityList) {
			materiel_code1+=zcMaterielAndTrayEntity2.getMateriel_code();
			location_code = zcMaterielAndTrayEntity2.getLocation_code();
			//丹丹新增
			//通过库位查询物料条码
			List<String> materiel_code_list1 = zcUpAndDownMapper.selectCodeByLocationCode(location_code);
			//库位数量
			int count = 0;
			int sfcf = 0;
			Long mid = 0L;
			for (String string : materiel_code_list1) {
				//通过code查询物料名称和数量
				String[] x = string.split(",");
				for (String string2 : x) {
					Materiel materiel = zcBindAndUnbindMapper.selectMaterielInfoByCode(string2);
					if(materiel!=null){
						count += materiel.getNum();
						if(!mid.equals(0L) && !mid.equals(materiel.getId())){
							sfcf =1;
						}
						mid = materiel.getId();
					}
				}
			}
			location_count = count;
			location_type = sfcf+"";
		}
		zcMaterielAndTrayEntity.setMateriel_code(materiel_code1);
		zcMaterielAndTrayEntity.setLocation_code(location_code);
		//丹丹新增
		zcMaterielAndTrayEntity.setLocation_count(location_count);
		zcMaterielAndTrayEntity.setLocation_type(location_type);
		return zcMaterielAndTrayEntity;
	}
	/**
	 * 通过物料条码查询物料是否在对应位置
	 * @param materiel_code
	 * @param location_code1 库区编号
	 * @return
	 */
	@Override
	public ZcMaterielAndTrayEntity selectMaterielLocationByCodeDownLocation(String materiel_code_list,String location_code1) {
		String[] materiel_code_arr = materiel_code_list.split(",");
		String materiel_code = materiel_code_arr[0];
		List<ZcMaterielAndTrayEntity> zcMaterielAndTrayEntityList = zcBindAndUnbindMapper.selectMaterielLocationByCodeDownLocation(materiel_code,location_code1);
		ZcMaterielAndTrayEntity zcMaterielAndTrayEntity = new ZcMaterielAndTrayEntity();
		String materiel_code1="";
		String location_code="";
		for (ZcMaterielAndTrayEntity zcMaterielAndTrayEntity2 : zcMaterielAndTrayEntityList) {
			materiel_code1+=zcMaterielAndTrayEntity2.getMateriel_code();
			location_code = zcMaterielAndTrayEntity2.getLocation_code();
		}
		zcMaterielAndTrayEntity.setMateriel_code(materiel_code1);
		zcMaterielAndTrayEntity.setLocation_code(location_code);
		return zcMaterielAndTrayEntity;
	}
	@Override
	public String selectByMaterielCode(String materiel_code) {
		return zcBindAndUnbindMapper.selectByMaterielCode(materiel_code);
	}
	/**
	 * 根据精准码查询库位集合
	 * @param code
	 * @return
	 */
	@Override
	public List<WarehouseRegionLocation> queryLocationByCode(String code) {
		return zcBindAndUnbindMapper.queryLocationByCode(code);
	}
	/**
	 * 查询库位码是否在良品区
	 */
	@Override
	public int selectCountGoodByLocationCode(String location_code) {
		return zcBindAndUnbindMapper.selectCountGoodByLocationCode(location_code);
	}
	/**
	 * 查询库位码是否在不良品区
	 */
	@Override
	public int selectCountBadByLocationCode(String location_code) {
		return zcBindAndUnbindMapper.selectCountBadByLocationCode(location_code);
	}
	/**       
	 * @param zcMaterielAndTrayEntity    
	 */
	@Override
	public void addMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		zcBindAndUnbindMapper.addMaterielAndTray(zcMaterielAndTrayEntity);
	}
	/**       
	 * @param a
	 * @return    
	 */
	@Override
	public ZcTrayAndLocationEntity selectTrayCodeByLocationCode(ZcTrayAndLocationEntity a) {
		return zcUpAndDownMapper.selectTrayCodeByLocationCode(a);
	}
	/**       
	 * @param newLocation    
	 */
	@Override
	public void addTrayAndLocation(ZcMaterielAndTrayEntity newLocation) {
		zcUpAndDownMapper.addTrayAndLocation(newLocation);
	}
	
	/**     
	 * 缓冲区添加数据  
	 * @param zcMaterileMoveInfoEntity    
	 */
	@Override
	public Integer inCache(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity) {
		return zcBindAndUnbindMapper.inCache(zcMaterileMoveInfoEntity);
	}
	/**     
	 * 从良品库出库
	 * @param zcMaterileMoveInfoEntity    
	 */
	@Override
	public int outGood(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity) {
		// TODO Auto-generated method stub
		return zcBindAndUnbindMapper.outGood(zcMaterileMoveInfoEntity);
	}
	/**     
	 * 记录下架记录
	 * @param ZcMaterielAndTrayEntity    
	 */
	@Override
	public void insertUnbindleRecord(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		// TODO Auto-generated method stub
		zcUpAndDownMapper.insertUnbindleRecord(zcMaterielAndTrayEntity);
	}
	/**
	 * 通过物料条码查询物料所在位置 以托盘为主
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielTrayLocationByCode(String materiel_code) {
		return  zcBindAndUnbindMapper.selectMaterielLocationByCode(materiel_code);
	}
		
}
