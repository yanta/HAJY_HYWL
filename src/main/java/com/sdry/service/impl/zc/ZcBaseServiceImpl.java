package com.sdry.service.impl.zc;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcBaseMapper;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcStorehouseEntity;
import com.sdry.service.zc.ZcBaseService;

/**
 * 基础模块逻辑实现类
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月31日 上午10:55:39
 * @Version 1.0
 */
@Service
public class ZcBaseServiceImpl implements ZcBaseService {

	@Resource
	private ZcBaseMapper zcBaseMapper;
	/**
	 * 条件查询库房列表
	 */
	@Override
	public List<ZcStorehouseEntity> selectStorehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_storehouse s left join lz_warehouse w on w.id = s.warehouse_id left join zc_system_user u on u.id = s.house_clerk ";
		//查询字段
		String strFld = "s.*,w.warehouse_name,u.userName as user_name";
		//排序
		String sort = " s.id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		return zcBaseMapper.selectStorehouseList(zcGeneralQueryEntity);
	}

	/**
	 * 条件查询库房数量
	 */
	@Override
	public int countStorehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_storehouse s ";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcBaseMapper.countStorehouseList(zcGeneralQueryEntity);
	}
	
	/**
	 * 新增库房
	 * @param zcStorehouseEntity
	 * @return
	 */
	@Override
	public int insertStorehouse(ZcStorehouseEntity zcStorehouseEntity) {
		return zcBaseMapper.insertStorehouse(zcStorehouseEntity);
	}
	
	/**
	 * 修改库房
	 * @param zcStorehouseEntity
	 * @return
	 */
	@Override
	public int updateStorehouse(ZcStorehouseEntity zcStorehouseEntity) {
		return zcBaseMapper.updateStorehouse(zcStorehouseEntity);
	}

	@Override
	public int deleteStorehouseById(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_storehouse";
		zcGeneralQueryEntity.setTab(tab);
		return zcBaseMapper.deleteStorehouseById(zcGeneralQueryEntity);
	}

	/**
	 * 查询编号是否存在
	 */
	@Override
	public int selectCodeIsExistByCode(String house_code) {
		return zcBaseMapper.selectCodeIsExistByCode(house_code);
	}

	/**
	 * 查询全部库房
	 */
	@Override
	public List<ZcStorehouseEntity> selectAllStorehouse() {
		ZcGeneralQueryEntity zcGeneralQueryEntity = new ZcGeneralQueryEntity();
		//表名
		String tab = "zc_storehouse ";
		//排序
		String sort = " id ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere("status = 0 and isdel = 0 and ");
		return zcBaseMapper.selectAllStorehouse(zcGeneralQueryEntity);
	}

}
