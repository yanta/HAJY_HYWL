package com.sdry.service.impl.zc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcSystemMapper;
import com.sdry.model.lz.Department;
import com.sdry.model.lz.Post;
import com.sdry.model.zc.ZcPermissionEntity;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcSysRoleEntity;
import com.sdry.model.zc.ZcSysRolePermissionEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.model.zc.ZcSysUserRoleEntity;
import com.sdry.service.zc.ZcSystemService;
import com.sdry.utils.MD5;

/**
 * 
 * @ClassName:    ZcSystemServiceImpl   
 * @Description:  系统管理
 * @Author:       zc   
 * @CreateDate:   2019年4月11日 上午9:59:27   
 * @Version:      v1.0
 */
@Service
public class ZcSystemServiceImpl implements ZcSystemService {

	@Resource
	public ZcSystemMapper zcSystemMapper;
	/**
	 * 查询用户信息
	 */
	@Override
	public ZcSysUserEntity selectSysUserByAccountName(ZcSysUserEntity zcSysUserEntity) {
		return zcSystemMapper.selectSysUserByAccountName(zcSysUserEntity);
	}
	/**
	 * 给首页查询菜单
	 */
	@Override
	public List<ZcPermissionEntity> selectAllMenuList2Index(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_permission";
		//排序
		String sort = "sort";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		//条件
		//String strWhere = "status = 0 and isdel = 0 and";
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		//zcGeneralQueryEntity.setStrWhere(strWhere);
		List<ZcPermissionEntity> ZcPermissionEntityList = zcSystemMapper.selectAllMenuList(zcGeneralQueryEntity);
		Map<Long, ZcPermissionEntity> map = new HashMap<>();
		for (ZcPermissionEntity ZcPermissionEntity : ZcPermissionEntityList) {
			if(ZcPermissionEntity.getPid() == 0){
				ZcPermissionEntity newMenu = new ZcPermissionEntity();
				newMenu.setId(ZcPermissionEntity.getId());
				newMenu.setPermissionName(ZcPermissionEntity.getPermissionName());
				newMenu.setUrl(ZcPermissionEntity.getUrl());
				newMenu.setPermissionType(ZcPermissionEntity.getPermissionType());
				newMenu.setPid(ZcPermissionEntity.getPid());
				newMenu.setSort(ZcPermissionEntity.getSort());
				map.put(ZcPermissionEntity.getId(), newMenu);
			}
		}
		ZcPermissionEntity parent = null;
		ZcPermissionEntity child = null;
		for (ZcPermissionEntity ZcPermissionEntity : ZcPermissionEntityList) {
			if(ZcPermissionEntity.getPid() > 0){
				parent = map.get(ZcPermissionEntity.getPid());
				child = new ZcPermissionEntity();
				child.setId(ZcPermissionEntity.getId());
				child.setPermissionName(ZcPermissionEntity.getPermissionName());
				child.setUrl(ZcPermissionEntity.getUrl());
				child.setPermissionType(ZcPermissionEntity.getPermissionType());
				child.setPid(ZcPermissionEntity.getPid());
				child.setSort(ZcPermissionEntity.getSort());
				
				//将二级菜单放入对应的一级菜单下
				parent.getmList().add(child);
			}
		}
		//将map转为list并排序
		List<ZcPermissionEntity> menus = new ArrayList<>();
		for(Iterator<ZcPermissionEntity> iter = map.values().iterator(); iter.hasNext();){
			ZcPermissionEntity ZcPermissionEntity = iter.next();
			Collections.sort(ZcPermissionEntity.getmList());
			menus.add(ZcPermissionEntity);
		}
		Collections.sort(menus);
		return menus;
	}
	/**
	 * 查询全部权限列表
	 */
	@Override
	public List<ZcPermissionEntity> selectAllMenuList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_permission";
		//排序
		String sort = "sort";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcSystemMapper.selectAllMenuList(zcGeneralQueryEntity);
	}
	/**
	 * 新增权限
	 */
	@Override
	public int insertPermission(ZcPermissionEntity zcPermissionEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcPermissionEntity.setCreatedTime(sdf.format(date));
		zcPermissionEntity.setUpdatedTime(sdf.format(date));
		return zcSystemMapper.insertPermission(zcPermissionEntity);
	}
	/**
	 * 修改权限
	 */
	@Override
	public int updatePermission(ZcPermissionEntity zcPermissionEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcPermissionEntity.setUpdatedTime(sdf.format(date));
		return zcSystemMapper.updatePermission(zcPermissionEntity);
	}
	/**
	 * 条件删除权限
	 */
	@Override
	public int deletePermissionById(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_permission";
		zcGeneralQueryEntity.setTab(tab);
		zcSystemMapper.deleteDetails(zcGeneralQueryEntity.getId());
		return zcSystemMapper.deletePermissionById(zcGeneralQueryEntity);
	}
	/**
	 * 条件查询用户列表
	 */
	@Override
	public List<ZcSysUserEntity> selectSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_user [user] left join lz_dept d on d.id = [user].deptId left join lz_post p on p.id = [user].postId left join lz_customer c on c.id = [user].customer_id";
		//查询字段
		String strFld = "[user].*,d.dept_name,p.post_name,c.customer_name";
		//排序
		String sort = " [user].id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		List<ZcSysUserEntity> userList = zcSystemMapper.selectSysUserList(zcGeneralQueryEntity);
		for (ZcSysUserEntity zcSysUserEntity : userList) {
			String roleId="";
			String roleName="";
			List<ZcSysRoleEntity> roleListByuserId = zcSystemMapper.selectRoleListByuserId(zcSysUserEntity.getId());
			for (ZcSysRoleEntity zcSysRoleEntity : roleListByuserId) {
				roleId += zcSysRoleEntity.getId()+",";
				roleName += zcSysRoleEntity.getRoleName()+",";
			}
			zcSysUserEntity.setRoleId(roleId);
			zcSysUserEntity.setRoleName(roleName);
		}
		return userList;
	}
	/**
	 * 条件查询用户列表数量
	 */
	@Override
	public int countSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_user [user]";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcSystemMapper.countSysUserList(zcGeneralQueryEntity);
	}
	/**
	 * 新增用户
	 */
	@Override
	public int insertSysUser(ZcSysUserEntity zcSysUserEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcSysUserEntity.setCreatedTime(sdf.format(date));
		zcSysUserEntity.setUpdatedTime(sdf.format(date));
		zcSysUserEntity.setPassword(MD5.md5Encode("123"));
		int affact;
		affact = zcSystemMapper.insertSysUser(zcSysUserEntity);
		if(affact>0){
			String roleId = zcSysUserEntity.getRoleId();
			String roleIds[] = roleId.split(",");
			ZcSysUserRoleEntity zcSysUserRoleEntity = new ZcSysUserRoleEntity();
			for (String rId : roleIds) {
				zcSysUserRoleEntity.setRoleId(Long.parseLong(rId));
				zcSysUserRoleEntity.setUserId(Long.parseLong(affact+""));
				int details = zcSystemMapper.insertSysUserRole(zcSysUserRoleEntity);
				if(details <= 0){
					//删除用户
					zcSystemMapper.deleteSysUserByUserId(Long.parseLong(affact+""));
					affact = 0;
				}
			}
		}
		return affact;
	}
	/**
	 * 修改用户
	 */
	@Override
	public int updateSysUser(ZcSysUserEntity zcSysUserEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcSysUserEntity.setUpdatedTime(sdf.format(date));
		int affact;
		affact = zcSystemMapper.updateSysUser(zcSysUserEntity);
		if(affact>0){
			//先删除用户角色
			zcSystemMapper.deleteSysUserRoleByUserId(zcSysUserEntity.getId());
			String roleId = zcSysUserEntity.getRoleId();
			String roleIds[] = roleId.split(",");
			ZcSysUserRoleEntity zcSysUserRoleEntity = new ZcSysUserRoleEntity();
			for (String rId : roleIds) {
				zcSysUserRoleEntity.setRoleId(Long.parseLong(rId));
				zcSysUserRoleEntity.setUserId(zcSysUserEntity.getId());
				//新添加用户角色
				zcSystemMapper.insertSysUserRole(zcSysUserRoleEntity);
			}
		}
		return affact;
	}
	/**
	 * 删除用户
	 */
	@Override
	public int deleteUserById(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_user";
		zcGeneralQueryEntity.setTab(tab);
		return zcSystemMapper.deleteUserById(zcGeneralQueryEntity);
	}
	/**
	 * 条件查询角色列表
	 */
	@Override
	public List<ZcSysRoleEntity> selectSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_role";
		//查询字段
		String strFld = "*";
		//排序
		String sort = "id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		return zcSystemMapper.selectSysRoleList(zcGeneralQueryEntity);
	}
	/**
	 * 条件查询角色列表数量
	 */
	@Override
	public int countSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_role";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcSystemMapper.countSysRoleList(zcGeneralQueryEntity);
	}
	/**
	 * 全查角色列表
	 */
	@Override
	public List<ZcSysRoleEntity> selectAllSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_role";
		//条件
		String strWhere = "";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcSystemMapper.selectAllSysRoleList(zcGeneralQueryEntity);
	}
	/**
	 * 新增角色
	 */
	@Override
	public int insertSysRole(ZcSysRoleEntity zcSysRoleEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcSysRoleEntity.setCreatedTime(sdf.format(date));
		zcSysRoleEntity.setUpdatedTime(sdf.format(date));
		return zcSystemMapper.insertSysRole(zcSysRoleEntity);
	}
	/**
	 * 修改角色
	 */
	@Override
	public int updateSysRole(ZcSysRoleEntity zcSysRoleEntity) {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		zcSysRoleEntity.setUpdatedTime(sdf.format(date));
		return zcSystemMapper.updateSysRole(zcSysRoleEntity);
	}
	/**
	 * 保存权限
	 */
	@Override
	public int saveRoleOperation(String roleId, String ztree) {
		//删除权限
		zcSystemMapper.deleteRoleOperation(Long.parseLong(roleId));
		int affact=0;
		String mIds[] = ztree.split(",");
		ZcSysRolePermissionEntity zcSysRolePermissionEntity = new ZcSysRolePermissionEntity();
		for (String mId : mIds) {
			if(!"".equals(mId)){
				zcSysRolePermissionEntity.setRoleId(Long.parseLong(roleId));
				zcSysRolePermissionEntity.setPermissionId(Long.parseLong(mId));
				affact = zcSystemMapper.saveRoleOperation(zcSysRolePermissionEntity);
			}
		}
		return affact;
	}
	/**
	 * 查询角色的权限
	 */
	@Override
	public List<ZcSysRolePermissionEntity> selectAllRoleOperation(String roleId) {
		return zcSystemMapper.selectAllRoleOperation(Long.parseLong(roleId));
	}
	/**
	 * 通过用户id查询他的角色
	 */
	@Override
	public List<ZcSysRoleEntity> selectRoleListByuserId(Long userId) {
		return zcSystemMapper.selectRoleListByuserId(userId);
	}
	/**
	 * 查询全部部门列表
	 */
	@Override
	public List<Department> selectAllDeptList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_dept";
		//条件
		String strWhere = "";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcSystemMapper.selectAllDeptList(zcGeneralQueryEntity);
	}
	/**
	 * 查询岗位列表通过部门id
	 */
	@Override
	public List<Post> selectAllPostList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "lz_post p left join lz_dept d on d.id = p.dept_id";
		//排序
		String sort = "p.id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcSystemMapper.selectAllPostList(zcGeneralQueryEntity);
	}
	/**
	 * 全查用户列表
	 */
	@Override
	public List<ZcSysUserEntity> selectAllSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_system_user";
		//条件
		String strWhere = "";
		//排序
		String sort = "id";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcSystemMapper.selectAllSysUserList(zcGeneralQueryEntity);
	}
	/**
	 * 核对原密码
	 */
	@Override
	public int checkPsd(String accountName, String srcPsd) {
		return zcSystemMapper.checkPsd(accountName, srcPsd);
	}
	/**
	 * 修改密码
	 */
	@Override
	public int editPsd(String accountName, String newPsd) {
		return zcSystemMapper.editPsd(accountName, newPsd);
	}
	/**
	 * 查看用户是否存在
	 */
	@Override
	public int countUser(String accountName) {
		return zcSystemMapper.countUser(accountName);
	}
	/**
	 * 通过角色权限查菜单
	 */
	@Override
	public List<ZcSysRolePermissionEntity> selectAllRoleOperation2Index(
			String id) {
		return zcSystemMapper.selectAllRoleOperation2Index(id);
	}
	/**
	 * 查询工号是否重复
	 * @param jobNumber
	 * @return
	 */
	@Override
	public int selectCountByNumber(String jobNumber) {
		return zcSystemMapper.selectCountByNumber(jobNumber);
	}
	/**
	 * 根据条件查用户
	 * @param zcSysUserEntity 条件
	 * @return
	 */
	public List<ZcSysUserEntity> selectSysUserByMution(ZcSysUserEntity zcSysUserEntity){
		return zcSystemMapper.selectSysUserByMution(zcSysUserEntity);
	}
}
