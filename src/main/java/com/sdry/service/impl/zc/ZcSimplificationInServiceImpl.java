package com.sdry.service.impl.zc;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcSimplificationInMapper;
import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.service.zc.ZcSimplificationInService;

/**
 * 精简入库
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年8月7日 上午9:37:30
 * @Version 1.0
 */
@Service
public class ZcSimplificationInServiceImpl implements ZcSimplificationInService {

	@Resource
	public ZcSimplificationInMapper zcSimplificationInMapper;
	/**
	 * 新增入库单
	 */
	@Override
	public int insertSimplificationIn(Receive receive) {
		return zcSimplificationInMapper.insertSimplificationIn(receive);
	}
	/**
	 * 新增详情
	 */
	@Override
	public int insertDetails(ReceiveDetail receiveDetail) {
		return zcSimplificationInMapper.insertDetails(receiveDetail);
	}
	/**
	 * 条件查询入库单列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	@Override
	public List<Receive> selectSimplificationInList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "jy_receive r left join zc_system_user u on cast(u.id as varchar) = r.create_name left join lz_customer c on c.id = r.send_company ";
		//查询字段
		String strFld = "r.*,r.receive_number as receiveNumber, r.send_name as sendName, r.create_date as createDate,u.userName as createName,c.customer_name as sendCompany";
		//排序
		String sort = " r.id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		return zcSimplificationInMapper.selectSimplificationInList(zcGeneralQueryEntity);
	}
	/**
	 * 条件查询入库单数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	@Override
	public int countSimplificationInList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "jy_receive r left join zc_system_user u on u.id = r.create_name left join lz_customer c on c.id = r.send_company  ";
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcSimplificationInMapper.countSimplificationInList(zcGeneralQueryEntity);
	}
	/**
	 * 批量删除
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	@Override
	public int deleteSimplificationInByNumber(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab1 = "jy_receive_detail";
		String tab2 = "jy_receive";
		zcGeneralQueryEntity.setTab(tab1);
		int aff1 = zcSimplificationInMapper.deleteSimplificationInDetailsByNumber(zcGeneralQueryEntity);
		int aff2=0;
		zcGeneralQueryEntity.setTab(tab2);
		if(aff1>0){
			aff2 = zcSimplificationInMapper.deleteSimplificationInByNumber(zcGeneralQueryEntity);
		}
		return aff2;
	}
	/**
	 * 查询状态为0 的入库单
	 */
	@Override
	public List<Receive> selectSimplificationInListAll(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "jy_receive r left join zc_system_user u on cast(u.id AS varchar(10)) = r.create_name left join lz_customer c on cast(c.id AS varchar(10)) = r.send_company ";
		//查询条件
		String strWhere = "r.state = 0 and r.isSimple = 1 and ";
		//排序
		String sort = " r.id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrWhere(strWhere);
		return zcSimplificationInMapper.selectSimplificationInListAll(zcGeneralQueryEntity);
	}
}
