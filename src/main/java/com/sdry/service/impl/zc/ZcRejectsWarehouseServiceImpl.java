package com.sdry.service.impl.zc;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcInventoryManagementMapper;
import com.sdry.mapper.zc.ZcRejectsWarehouseMapper;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.service.zc.ZcRejectsWarehouseService;

/**
 * 不良库管理
 * @ClassName:    ZcRejectsWarehouseServiceImpl   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年4月25日 下午5:19:32   
 * @Version:      v1.0
 */
@Service
public class ZcRejectsWarehouseServiceImpl implements ZcRejectsWarehouseService {

	@Resource
	private ZcRejectsWarehouseMapper zcRejectsWarehouseMapper;
	@Resource
	private ZcInventoryManagementMapper zcInventoryManagementMapper;
	/**
	 * 查询不良库列表
	 */
	@Override
	public List<ZcRejectsWarehouseEntity> selectRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_rejects_warehouse info left join lz_materiel m on m.id = info.mid left join lz_customer c on c.id = m.customer_id left join zc_system_user u on u.id = info.enterPerson";
		//查询字段
		String strFld = "(select sum(totality) from zc_rejects_warehouse i left join lz_materiel ma on ma.id = i.mid where ma.materiel_num = m.materiel_num group by ma.materiel_num) as countAll,c.customer_name,info.*,m.materiel_name,m.materiel_num,m.brevity_num,m.devanning_code,m.packing_quantity,m.devanning_num,m.unit,m.placement_type,m.upper_value,m.lower_value,m.customer_id,u.userName ";
		//排序
		String sort = "m.customer_id,m.materiel_num";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
		return zcRejectsWarehouseMapper.selectRejectsWarehouseList(zcGeneralQueryEntity);
	}
	/**
	 * 查询不良库数量
	 */
	@Override
	public int countRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity) {
		//表名
		String tab = "zc_rejects_warehouse info left join lz_materiel m on m.id = info.mid";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		return zcRejectsWarehouseMapper.countRejectsWarehouseList(zcGeneralQueryEntity);
	}
	/**
	 * 不良品入库
	 */
	@Override
	public int enterRejects(ZcRejectsWarehouseEntity zcRejectsWarehouseEntity) {
		int res = 0;
		//判断有无目标库绑定
		if(zcRejectsWarehouseMapper.countInventoryInfoByRegionId(zcRejectsWarehouseEntity)>0){
			res = zcRejectsWarehouseMapper.addNum(zcRejectsWarehouseEntity);
		}else{
			res = zcRejectsWarehouseMapper.bindingNewRejectsWare(zcRejectsWarehouseEntity);
		}
		return res;
	}
}
