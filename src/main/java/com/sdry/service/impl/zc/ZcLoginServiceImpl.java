package com.sdry.service.impl.zc;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.sdry.mapper.zc.ZcSystemMapper;
import com.sdry.model.zc.ResponseResult;
import com.sdry.model.zc.ZcSysRoleEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.zc.ZcLoginService;
import com.sdry.utils.MD5;

/**
 * 
 * @ClassName:    ZcLoginServiceImpl   
 * @Description:  登录
 * @Author:       zc   
 * @CreateDate:   2019年4月17日 下午3:41:57   
 * @Version:      v1.0
 */
@Service
public class ZcLoginServiceImpl implements ZcLoginService {
	@Resource
	public ZcSystemMapper zcSystemMapper;
	/**
	 * pc登录
	 */
	@Override
	public ResponseResult<Boolean> pclogin(HttpSession httpSession,ZcSysUserEntity zcSysUserEntity) {
		ResponseResult<Boolean> result = new ResponseResult<>();
		//加密
		zcSysUserEntity.setPassword(MD5.md5Encode(zcSysUserEntity.getPassword()));
		if(zcSystemMapper.selectSysUserByAccountName(zcSysUserEntity) == null){
			result.setData(false);
            result.setMessage("用户不存在");
		}else{
			ZcSysUserEntity user = zcSystemMapper.selectSysUserByAccountName(zcSysUserEntity);
			String roleId="";
			String roleName="";
			List<ZcSysRoleEntity> roleListByuserId = zcSystemMapper.selectRoleListByuserId(user.getId());
			int per=0;
			for (ZcSysRoleEntity zcSysRoleEntity : roleListByuserId) {
				per+=zcSystemMapper.countPermissionByroleId(zcSysRoleEntity.getId());
				roleId += zcSysRoleEntity.getId()+",";
				roleName += zcSysRoleEntity.getRoleName()+",";
			}
			if(per==0){
				result.setData(false);
                result.setMessage("用户无权限,请联系管理员");
			}else{
				user.setRoleId(roleId);
				user.setRoleName(roleName);
				String s = user.getPassword();
	            String encode = zcSysUserEntity.getPassword();
	            if(s.equals(encode)){
	                httpSession.setAttribute("user",user);
	                result.setData(true);
	                result.setMessage("登陆成功");
	            }else {
	                result.setData(false);
	                result.setMessage("密码不正确");
	            }
			}
		}
		return result;
	}
	/**
	 * app登录
	 */
	@Override
	public Map<String, Object> appLogin(ZcSysUserEntity zcSysUserEntity) {
		// TODO Auto-generated method stub
		Map<String,Object> result=new HashMap<>();
		//加密
		zcSysUserEntity.setPassword(MD5.md5Encode(zcSysUserEntity.getPassword()));
		if(zcSystemMapper.selectSysUserByAccountName(zcSysUserEntity) == null){
			result.put("status", 0);
            result.put("message","用户不存在");
		}else{
			ZcSysUserEntity user = zcSystemMapper.selectSysUserByAccountName(zcSysUserEntity);
			String roleId="";
			String roleName="";
			List<ZcSysRoleEntity> roleListByuserId = zcSystemMapper.selectRoleListByuserId(user.getId());
			for (ZcSysRoleEntity zcSysRoleEntity : roleListByuserId) {
				roleId += zcSysRoleEntity.getId()+",";
				roleName += zcSysRoleEntity.getRoleName()+",";
			}
			user.setRoleId(roleId);
			user.setRoleName(roleName);
			String s = user.getPassword();
            String encode = zcSysUserEntity.getPassword();
            if(s.equals(encode)){
            	result.put("data",user);
	            result.put("status", 1);
	            result.put("message","登陆成功");
            }else {
            	result.put("status", 0);
	             result.put("message","密码不正确");
            }
		}
	    return result;
	}
}