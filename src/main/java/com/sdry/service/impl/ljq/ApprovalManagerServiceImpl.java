package com.sdry.service.impl.ljq;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.ljq.ApprovalManagerMapper;
import com.sdry.model.lz.LossSpillover;
import com.sdry.service.ljq.ApprovalManagerService;

@Service
public class ApprovalManagerServiceImpl implements ApprovalManagerService {

	@Resource
	ApprovalManagerMapper amMapper;
	
	@Override
	public List<LossSpillover> ICommitedQueryPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return amMapper.ICommitedQueryPage(map);
	}

	@Override
	public int ICommitedQueryCount(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return amMapper.ICommitedQueryCount(map);
	}

	@Override
	public int CompleteApproval(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return amMapper.CompleteApproval(map);
	}

	@Override
	public List<LossSpillover> IApprovaledQueryPage(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return amMapper.IApprovaledQueryPage(map);
	}

	@Override
	public int IApprovaledQueryCount(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return amMapper.IApprovaledQueryCount(map);
	}

}
