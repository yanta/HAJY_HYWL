package com.sdry.service.impl.ljq;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.ljq.LjqWaitSendAreaMapper;
import com.sdry.model.ljq.LjqWaitSendArea;
import com.sdry.service.ljq.LjqWaitSendAreaService;

/**
  * @ClassName LjqWaitSendAreaServicesImpl.java
  * @Description 
  * @author LJQ
  * @Date 2019年5月7日 上午9:46:04
  * @Version 1.0
  *
  */
@Service
public class LjqWaitSendAreaServiceImpl implements LjqWaitSendAreaService {

	@Resource
	LjqWaitSendAreaMapper lwsaMapper;
	
	@Override
	public List<LjqWaitSendArea> listPageWaitSendArea(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return lwsaMapper.listPageWaitSendArea(map);
	}

	@Override
	public int listCountWaitSendArea() {
		// TODO Auto-generated method stub
		return lwsaMapper.listCountWaitSendArea();
	}

}

