package	com.sdry.service.impl.barCodeOperation;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import javax.annotation.Resource;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.sdry.model.cb.TypeEnum;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.barCodeOperation.BarCodeOperation;
import com.sdry.model.lz.CodeMark;
import com.sdry.service.barCodeOperation.BarCodeOperationService;
import com.sdry.mapper.barCodeOperation.BarCodeOperationMapper;

/**
 *
 *@ClassName: BarCodeOperationService
 *@Description: 条码操作记录
 *@Author tdd
 *@Date 2019-08-07 17:03:56
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class BarCodeOperationServiceImpl implements BarCodeOperationService {
	@Resource BarCodeOperationMapper barCodeOperationMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public BarCodeOperation queryById(Long id) {
		return barCodeOperationMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<BarCodeOperation> queryAllByMution(BarCodeOperation param) {
		return barCodeOperationMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public JSONObject findPageByMution(BarCodeOperation param) {
        param.setType(TypeEnum.valueOf(param.getType()).getValue());
        param.setResult(TypeEnum.valueOf(param.getResult()).getValue());
        Integer total = 0;
		if ("2".equals(param.getType()) ) {
			param.setType(null);
		}
		if ("2".equals(param.getResult()) ) {
			param.setResult(null);
		}
		List<BarCodeOperation> listAll = new ArrayList<>();
		// 1. 条码
		List<BarCodeOperation> list1 = barCodeOperationMapper.findPageByMution(param);
        Integer count1 = barCodeOperationMapper.selectCountByMution(param);
        // 2. 零件号
		String barCode = param.getBarCode();
		param.setBatch(barCode);
		param.setBarCode(null);
		List<BarCodeOperation> list2 = barCodeOperationMapper.findPageByMution(param);
        Integer count2 = barCodeOperationMapper.selectCountByMution(param);
		// 3. 人员
		param.setParam1(barCode);
		param.setBatch(null);
		List<BarCodeOperation> list3 = barCodeOperationMapper.findPageByMution(param);
        Integer count3 = barCodeOperationMapper.selectCountByMution(param);
        int max = Math.max(list1.size(), Math.max(list2.size(), list3.size()));
        if (max == list1.size()) {
            listAll.addAll(list1);
            total = count1;
        }else if (max == list2.size()) {
            listAll.addAll(list2);
            total = count2;
        }else if (max == list3.size()) {
            listAll.addAll(list3);
            total = count3;
        }
        JSONObject jsonObject = new JSONObject();
        //添加入jsonObject
        jsonObject.put("code", 0);
        jsonObject.put("rows", listAll);
        jsonObject.put("total", total);
		return jsonObject;
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(BarCodeOperation param) {
		//barCodeOperationMapper.selectCountByMution(param);
		return null;
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(BarCodeOperation param) {
		return barCodeOperationMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(BarCodeOperation param) {
		return barCodeOperationMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return barCodeOperationMapper.delete(ids);
	}
	/**       
	 * @param userId
	 * @return    
	 */
	@Override
	public Long queryIdByName(String userId) {
		return barCodeOperationMapper.queryIdByName(userId);
	}
	/**
	 * 查询同一物料同一批次的所有条码       
	 * @param materiel_code
	 * @return    
	 */
	@Override
	public List<CodeMark> selectCountOnSameCodeByCode(String materiel_code) {
		return barCodeOperationMapper.selectCountOnSameCodeByCode(materiel_code);
	}
	/** 
	 * 根据条码查是否质检      
	 * @param code
	 * @return    
	 */
	@Override
	public Integer queryStatusByCode(String code) {
		return barCodeOperationMapper.queryStatusByCode(code);
	}
}
