package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.CarMapper;
import com.sdry.model.lz.Car;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.service.lz.CarService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * @ClassName CarServiceImpl
 * @Description 车辆信息
 * @Author lz
 * @Date 2019年4月16日 10:23:46
 * @Version 1.0
 */
@Service
public class CarServiceImpl implements CarService {

    @Resource
    CarMapper carMapper;

    @Override
    public Long addCar(Car car) {
        return carMapper.addCar(car);
    }

    @Override
    public Long deleteCarById(long id) {
        return carMapper.deleteCarById(id);
    }

    @Override
    public Long updateCar(Car car) {
        return carMapper.updateCar(car);
    }

    @Override
    public List<Car> queryCarCriteria(LzQueryCriteria criteria) {
        return carMapper.queryCarCriteria(criteria);
    }

    @Override
    public int countCarCriteria(LzQueryCriteria criteria) {
        return carMapper.countCarCriteria(criteria);
    }

    /**
     * 根据条码查询该条码是否存在
     * @param response
     * @param beforeCode 原条码
     */
	@Override
	public CodeMark queryCodeExist(String beforeCode) {
		return carMapper.queryCodeExist(beforeCode);
	}

	/**
	 * 修改条码
	 * @param beforeCode 原条码
	 * @param newCode 新条码
	 * @return
	 */
	@Override
	public Long editCode(String beforeCode, String newCode) {
		return carMapper.editCode(beforeCode, newCode);
	}

	@Override
	public int updateCodeByid(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		return carMapper.updateCodeByid(zcMaterielAndTrayEntity);
	}

	/**
	 * 验证是否已存在
	 * @param newCode 新条码
	 * @return
	 */
	@Override
	public int checkNewCode(String newCode) {
		return carMapper.checkNewCode(newCode);
	}
}
