package com.sdry.service.impl.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.AbnormalAdjustmentMapper;
import com.sdry.model.lz.AbnormalAdjustment;
import com.sdry.model.lz.AbnormalDetail;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.service.lz.AbnormalAdjustmentService;

/**
 * @ClassName 异常调整
 * @Description AbnormalAdjustmentServiceImpl
 * @Author lz
 * @Date 2019年4月23日 11:03:15
 * @Version 1.0
 */
@Service
public class AbnormalAdjustmentServiceImpl implements AbnormalAdjustmentService{

	@Resource
	AbnormalAdjustmentMapper abnormalAdjustmentMapper;
	
	@Override
	public Long addAbnormalAdjustment(AbnormalAdjustment abnormalAdjustment) {
		return abnormalAdjustmentMapper.addAbnormalAdjustment(abnormalAdjustment);
	}

	@Override
	public Long deleteAbnormalAdjustmentById(long id) {
		return abnormalAdjustmentMapper.deleteAbnormalAdjustmentById(id);
	}

	@Override
	public Long updateAbnormalAdjustmentById(String id, String fieldName,
			String fieldValue) {
		return abnormalAdjustmentMapper.updateAbnormalAdjustmentById(id, fieldName, fieldValue);
	}

	@Override
	public List<AbnormalAdjustment> queryAbnormalAdjustmentCriteria(
			LzQueryCriteria criteria) {
		return abnormalAdjustmentMapper.queryAbnormalAdjustmentCriteria(criteria);
	}

	@Override
	public int countAbnormalAdjustmentCriteria(LzQueryCriteria criteria) {
		return abnormalAdjustmentMapper.countAbnormalAdjustmentCriteria(criteria);
	}

	@Override
	public List<OutStockOrder> queryOutStockOrderByNum(String out_order_num) {
		return abnormalAdjustmentMapper.queryOutStockOrderByNum(out_order_num);
	}

	@Override
	public List<Materiel> queryMaCuRe(long cid) {
		return abnormalAdjustmentMapper.queryMaCuRe(cid);
	}

	@Override
	public Long addAbnormalDetail(AbnormalDetail AbnormalDetail) {
		return abnormalAdjustmentMapper.addAbnormalDetail(AbnormalDetail);
	}

	@Override
	public List<Materiel> queryAbnormalDetail(long aid) {
		return abnormalAdjustmentMapper.queryAbnormalDetail(aid);
	}

	@Override
	public Long updateAbnormalDetailById(String id, String fieldName, String fieldValue) {
		AbnormalDetail abnormalDetail= abnormalAdjustmentMapper.queryAbnormalDetailByid(Long.parseLong(id));
		abnormalDetail.setBnum(fieldValue);
		return abnormalAdjustmentMapper.updateAbnormalDetailById(id, fieldName, fieldValue);
		
	}

	@Override
	public AbnormalDetail queryAbnormalDetailByid(long id) {
		return abnormalAdjustmentMapper.queryAbnormalDetailByid(id);
	}

	@Override
	public List<AbnormalDetail> queryMateridByAbnormalid(long aid) {
		return abnormalAdjustmentMapper.queryMateridByAbnormalid(aid);
	}

}
