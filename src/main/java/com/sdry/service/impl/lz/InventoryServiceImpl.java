package com.sdry.service.impl.lz;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.llm.CancellingStockDetailMapper;
import com.sdry.mapper.llm.CancellingStockMapper;
import com.sdry.mapper.lz.InventoryMapper;
import com.sdry.mapper.zc.ZcBindAndUnbindMapper;
import com.sdry.mapper.zc.ZcUpAndDownMapper;
import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.InventoryDetail;
import com.sdry.model.lz.InventoryDetailCode;
import com.sdry.model.lz.InventoryOrder;
import com.sdry.model.lz.LzInventoryDetailsCodeEntity;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMaterielNameAndCode;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;
import com.sdry.service.lz.InventoryService;

/**
 * @ClassName InventoryServiceImpl
 * @Description 盘点管理
 * @Author lz
 * @Date 2019年3月26日 14:18:37
 * @Version 1.0
 */
@Service
public class InventoryServiceImpl implements InventoryService{

	@Resource
	InventoryMapper inventoryMapper;
	@Resource
	CancellingStockDetailMapper cancellingStockDetailMapper;
	@Resource
	CancellingStockMapper cancellingStockMapper;
	@Resource
	ZcUpAndDownMapper zcUpAndDownMapper;
	@Resource
	ZcBindAndUnbindMapper zcBindAndUnbindMapper;
	
	@Override
	public Long addInventoryOrder(InventoryOrder inventoryOrder) {
		return inventoryMapper.addInventoryOrder(inventoryOrder);
	}

	@Override
	public Long addInventoryOrderDetail(InventoryDetail detail) {
		Long l = 0L;
		Long id = detail.getMateriel_id();
		//根据物料查供应商
		//if(id != null && !"".equals(id)){
		//Customer customer = cancellingStockDetailMapper.getCustomerByMid(id);
		//if(customer != null){
		Map<String, Object> map = new HashMap<>();
		map.put("mid", id);
		//map.put("customerId", customer.getId());
		//根据物料id查库存
		List<ZcInventoryInfoEntity> infoEntity = cancellingStockDetailMapper.getQuantityByMid(map);
		long sum=0;
		for (ZcInventoryInfoEntity zcInventoryInfoEntity : infoEntity) {
			sum += zcInventoryInfoEntity.getmNum();
		}
		detail.setTheoretical_quantity(sum + 0L);
		//System.out.println("***************"+sum);
		//l = inventoryMapper.addInventoryOrderDetail(detail);*/
		//Map<String, Object> map = new HashMap<>();
		//map.put("mid", id);
		//map.put("customerId", customer.getId());
		//根据物料id查库存
		//List<ZcInventoryInfoEntity> infoEntity = cancellingStockDetailMapper.getQuantityByMid(map);
		//long sum=0;
		//for (ZcInventoryInfoEntity zcInventoryInfoEntity : infoEntity) {
		//	sum+=zcInventoryInfoEntity.getmNum();
		//}
		//detail.setTheoretical_quantity(sum+0L);
		//System.out.println("***************"+sum);
		int sumCode = 0;
		//根据库位编号查托盘编号
		String tray = inventoryMapper.trayByLocation(detail.getLocation_num());
		
		if(!"".equals(tray) && tray !=null) {
			String [] trays = tray.split(",");
			if(trays.length  > 0){
				for(String trayNum : trays){
					//根据托盘编号查物料条码
					String codes = inventoryMapper.queryCodeByTray(trayNum);
					if(!"".equals(codes) && codes != null) {
						String [] codess = codes.split(",");
						for(String code : codess){
							//根据条码查物料数量
							CodeMark codeMark = cancellingStockDetailMapper.queryCodeByCode(code);
							if(codeMark != null){
								sumCode += codeMark.getNum();
							}
						}
					}
				}
				detail.setTheoretical_quantity(sumCode + 0L);
			}else{
				detail.setTheoretical_quantity(0L);
			}
		}else {
			detail.setTheoretical_quantity(0L);
		}
		
		l = inventoryMapper.addInventoryOrderDetail(detail);
			//}
		//}
		return l;
	}
	
	@Override
	public Long deleteInventoryOrderById(long id) {
		return inventoryMapper.deleteInventoryOrderById(id);
	}

	@Override
	public Long deleteInventoryOrderDetailById(long id) {
		return inventoryMapper.deleteInventoryOrderDetailById(id);
	}

	@Override
	public Long updateInventoryOrderById(String id, String fieldName,
			String fieldValue) {
		return inventoryMapper.updateInventoryOrderById(id, fieldName, fieldValue);
	}

	/*@Override
	public Long updateInventoryOrderDetailById(String id, String fieldName,
			String fieldValue) {
		InventoryDetail detail = inventoryMapper.queryInventoryDetailById(Long.parseLong(id));
		detail.setActual_quantity(Long.parseLong(fieldValue));
		detail.setDifference_quantity(detail.getTheoretical_quantity()-Long.parseLong(fieldValue));
		return inventoryMapper.updateInventoryDetailAndroid(detail);
	}*/

	@Override
	public List<InventoryOrder> queryInventoryOrderCriteria(
			LzQueryCriteria criteria) {
		return inventoryMapper.queryInventoryOrderCriteria(criteria);
	}

	@Override
	public int countInventoryOrderCriteria(LzQueryCriteria criteria) {
		return inventoryMapper.countInventoryOrderCriteria(criteria);
	}
	
	@Override
	public List<InventoryDetail> queryInventoryDetailCriteriaById(
			LzQueryCriteria criteria) {
		return inventoryMapper.queryInventoryDetailCriteriaById(criteria);
	}

	@Override
	public int countInventoryDetailCriteriaById(LzQueryCriteria criteria) {
		return inventoryMapper.countInventoryDetailCriteriaById(criteria);
	}

	@Override
	public List<InventoryOrder> queryInventoryOrderCriteriaAndroid() {
		return inventoryMapper.queryInventoryOrderCriteriaAndroid();
	}

	@Override
	public List<InventoryDetail> queryInventoryDetailCriteriaByIdAndroid(
			String order_id) {
		return inventoryMapper.queryInventoryDetailCriteriaByIdAndroid(order_id);
	}

	@Override
	public Long updateInventoryDetailAndroid(InventoryDetail inventoryDetail) {
		Long affact = inventoryMapper.updateInventoryDetailAndroid(inventoryDetail);
		/*LzInventoryDetailsCodeEntity lzInventoryDetailsCodeEntity = new LzInventoryDetailsCodeEntity();
		Long res = 0L;
		if(affact>0){
			List<CodeMark> codeList = inventoryDetail.getReceiveCode();
			//往盘点code表插条码
    		for (CodeMark codeMark : codeList) {
    			//通过条码查询原数量
    			int theory_num = inventoryMapper.selectTheorynumByCode(codeMark.getCode());
    			if(codeMark.getCode() != null && !"".equals(codeMark.getCode())){
    				lzInventoryDetailsCodeEntity.setCode(codeMark.getCode());
    			}else{
    				break;
    			}
    			lzInventoryDetailsCodeEntity.setInvenId(inventoryDetail.getId());
    			lzInventoryDetailsCodeEntity.setTheory_num(theory_num);
    			lzInventoryDetailsCodeEntity.setReality_num(codeMark.getNum());
    			lzInventoryDetailsCodeEntity.setDiff_num(codeMark.getNum()-theory_num);
    			res += inventoryMapper.insertCodeList(lzInventoryDetailsCodeEntity);
			}
		}*/
		return affact;
	}

	@Override
	public List<Warehouse> queryAllWarehouseAndroid() {
		return inventoryMapper.queryAllWarehouseAndroid();
	}
	
	@Override
	public List<InventoryDetail> queryInventoryDetailByOrder(String orderId) {
		/*List<InventoryDetail> list = inventoryMapper.queryInventoryDetailByOrder(orderId);
		if(list.size() > 0){
			for(InventoryDetail detail : list){
				if(detail.getMateriel_id() != null && !"".equals(detail.getMateriel_id())){
					Materiel materiel = cancellingStockMapper.getMaterielByMid(detail.getMateriel_id());
					detail.setMateriel_name(materiel.getMateriel_name());
					detail.setMateriel_num(materiel.getMateriel_num());
					detail.setBrevity_num(materiel.getBrevity_num());
				}
			}
		}
		return list;*/
		long oid = queryInventorynum(orderId);
		return queryDetailByid(oid);
	}

	@Override
	public InventoryDetail queryInventoryDetailById(long id) {
		InventoryDetail detail = inventoryMapper.queryInventoryDetailById(id);
		if(detail.getMateriel_id() != null && !"".equals(detail.getMateriel_id())){
			Materiel materiel = cancellingStockMapper.getMaterielByMid(detail.getMateriel_id());
			detail.setMateriel_name(materiel.getMateriel_name());
			detail.setMateriel_num(materiel.getMateriel_num());
			detail.setBrevity_num(materiel.getBrevity_num());
		}
		return detail;
	}

	public List<InventoryOrder> queryAllInventoryOrder() {
		List<InventoryOrder> list = inventoryMapper.queryAllInventoryOrder();
		return list;
	}

	/**       
	 * @return    
	 */
	@Override
	public List<ZcSysUserEntity> queryAllUser() {
		return inventoryMapper.queryAllUser();
	}

	@Override
	public Long updateInventoryOrderDetailById(String mid, String fieldName,
			String fieldValue, String cid, String detailid) {
		InventoryDetail detail = inventoryMapper.queryInventoryDetailByIdcid(Long.parseLong(mid),Long.parseLong(cid),Long.parseLong(detailid));
		detail.setActual_quantity(Long.parseLong(fieldValue));
		detail.setDifference_quantity(Long.parseLong(fieldValue) - detail.getTheoretical_quantity());
		return inventoryMapper.updateInventoryDetailAndroid(detail);
	}

	@Override
	public Long queryInventorynum(String num) {
		// TODO Auto-generated method stub
		return inventoryMapper.queryInventorynum(num);
	}

	@Override
	public List<InventoryDetail> queryDetailByid(long id) {
		// TODO Auto-generated method stub
		return inventoryMapper.queryDetailByid(id);
	}

	@Override
	public List<InventoryOrder> queryAllInventoryOrder1() {
		// TODO Auto-generated method stub
		return inventoryMapper.queryAllInventoryOrder1();
	}

	@Override
	public List<InventoryDetail> queryMaterielId(long id) {
		// TODO Auto-generated method stub
		return inventoryMapper.queryMaterielId( id);
	}

	/**       
	 * @param inventory_order
	 * @return    
	 */
	@Override
	public InventoryOrder queryInventoryOrderByNumber(String inventory_order) {
		return inventoryMapper.queryInventoryOrderByNumber(inventory_order);
	}

	/**       
	 * @param id
	 * @param materielId
	 * @return    
	 */
	@Override
	public Long queryInventoryDetailByOrderIdAndMid(Long id, Long materielId) {
		return inventoryMapper.queryInventoryDetailByOrderIdAndMid(id, materielId);
	}

	/**       
	 * @param id
	 * @param fieldName
	 * @param fieldValue
	 * @param tableName
	 * @return    
	 */
	@Override
	public int updateById(String id, String fieldName, String fieldValue, String tableName) {
		return inventoryMapper.updateById(id, fieldName, fieldValue, tableName);
	}

	/**       
	 * @param detailId
	 * @return    
	 */
	@Override
	public List<LzInventoryDetailsCodeEntity> queryCodeById(Long detailId) {
		return inventoryMapper.queryCodeById(detailId);
	}

	/**       
	 * @param code
	 * @return    
	 */
	@Override
	public ReceiveDetailQuality queryReceiveByCode(String code) {
		return inventoryMapper.queryReceiveByCode(code);
	}

	/**       
	 * @param string
	 * @param string2
	 * @param string3    
	 */
	@Override
	public void updateInventoryQuantity(Map<String, Object> map) {
		inventoryMapper.updateInventoryQuantity(map);
	}

	/**       
	 * @param map2    
	 */
	@Override
	public void updateCodeMarkQuantity(Map<String, Object> map2) {
		inventoryMapper.updateCodeMarkQuantity(map2);
	}

	/**
	 * 修改盘点单
	 * @author jyy
	 * 2019-08-06 14:56:01
	 */
	@Override
	public int update(InventoryOrder inventoryOrder) {
		return inventoryMapper.update(inventoryOrder);
	}

	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<InventoryDetailCode> findPageByMution(InventoryDetailCode param) {
		return inventoryMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(InventoryDetailCode param) {
		return inventoryMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(InventoryDetailCode param) {
		return inventoryMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer updateCode(InventoryDetailCode param) {
		return inventoryMapper.updateCode(param);
	}

	/**
	 * 根据盘点详情id查询复盘条码
	 * @author jyy
	 * @param invenId 盘点详情id
	 * @return 复盘条码集合
	 */
	@Override
	public List<InventoryDetailCode> queryCodeListByinvenId(Long invenId) {
		return inventoryMapper.queryCodeListByinvenId(invenId);
	}

	@Override
	public int selectKwCount(String location_num) {
		//查询库位下的所有物料条码
		List<String> materiel_code_list = zcUpAndDownMapper.selectCodeByLocationCode(location_num);
		int count = 0;
		for (String materiel_code : materiel_code_list) {
			//通过code查询物料名称和数量
			ZcMaterielNameAndCode zcMaterielNameAndCode1 =  zcBindAndUnbindMapper.selectNameByCode(materiel_code);
			int num = zcMaterielNameAndCode1.getNum();
			count = num + count;
			System.out.println(num);
		}
		return count;
	}

	@Override
	public List<InventoryDetail> queryInventoryDetailCriteriaByIdAndroid2(String order_id) {
		return inventoryMapper.queryInventoryDetailCriteriaByIdAndroid2(order_id);
	}

	/**       
	 * @param id
	 * @return    
	 */
	@Override
	public InventoryDetail queryInventoryDetailById2(Long id) {
		return inventoryMapper.queryInventoryDetailById2(id);
	}

	/**       
	 * @param inventoryDetailCodes    
	 */
	@Override
	public void insertCodeInInventory(List<InventoryDetailCode> inventoryDetailCodes, Long orderId, Long detailId) {
		List<InventoryDetail> detailList = inventoryMapper.queryInventoryDetailByOrderId(orderId);
		//1、查询出有差异的库位系统中的所有条码
		List<CodeMark> locationCodeList = new ArrayList<CodeMark>();
		for(InventoryDetail detail : detailList){
			//根据库位编号查托盘编号
			String tray = inventoryMapper.trayByLocation(detail.getLocation_num());
			if(!"".equals(tray) && null != tray){
				String [] trays = tray.split(",");
				if(trays.length  > 0){
					for(String trayNum : trays){
						//根据托盘编号查物料条码
						String codes = inventoryMapper.queryCodeByTray(trayNum);
						if(!"".equals(codes) && null != codes){
							String [] codess = codes.split(",");
							for(String code : codess){
								//根据条码查物料数量
								CodeMark codeMark = cancellingStockDetailMapper.queryCodeByCode(code);
								if(codeMark != null){
									locationCodeList.add(codeMark);
								}
							}
						}
					}
				}
			}
		}
		//2、进行比对，找出不一致的条码
		List<InventoryDetailCode> temp2 = new ArrayList<>();
		//存已匹配过的理论条码实体
		List<CodeMark> tempList2 = new ArrayList<>();
		//存已匹配过的实际条码实体
		List<InventoryDetailCode> tempList3 = new ArrayList<>();
		//找出理论上有这个条码，但是实际没有的
		for(int j = 0; j < locationCodeList.size(); j ++){
			for(int i = 0; i < inventoryDetailCodes.size(); i ++){
				if(locationCodeList.get(j).getCode().equals(inventoryDetailCodes.get(i).getCode())){
					inventoryDetailCodes.get(i).setDiff_num(inventoryDetailCodes.get(i).getReality_num() - locationCodeList.get(j).getNum());
					temp2.add(inventoryDetailCodes.get(i));
					tempList2.add(locationCodeList.get(j));
					tempList3.add(inventoryDetailCodes.get(i));
				}
			}
		}
		//差集（用原集合减去已匹配的集合）这是理论条码比实际条码多   比如理论 1,2,3  实际 2,3,4， 这里得出 1 存入新集合
		locationCodeList.removeAll(tempList2);
		//把集合的类进行转换存入数据库
		for(CodeMark cm : locationCodeList){
			InventoryDetailCode temp = new InventoryDetailCode();
			temp.setCode(cm.getCode());
			temp.setInvenId(detailId);
			temp.setTheory_num(cm.getNum());
			temp.setReality_num(0);
			temp.setDiff_num(0 - cm.getNum());
			temp2.add(temp);
		}
		//差集（用原集合减去已匹配的集合）这是实际条码比理论条码多 比如理论 1,2,3  实际 2,3,4， 这里得出 4 存入新集合
		inventoryDetailCodes.removeAll(tempList3);
		for(InventoryDetailCode c3 : inventoryDetailCodes){
			c3.setTheory_num(0);
			c3.setDiff_num(c3.getReality_num());
			temp2.add(c3);
		}
		for(InventoryDetailCode c : temp2){
			inventoryMapper.insert(c);
		}
	}

	/**       
	 * @param id
	 * @return    
	 */
	@Override
	public List<InventoryDetailCode> queryCodeByDiff(Long id) {
		return inventoryMapper.queryCodeByDiff(id);
	}

	@Override
	public int cacelpd(Map map) {
		// TODO Auto-generated method stub
		return inventoryMapper.cacelpd(map);
	}

	@Override
	public int restorpd(Map map) {
		// TODO Auto-generated method stub
		return inventoryMapper.restorpd(map);
	}

	/**
	 * 在物料托盘绑定表中新增一条数据
	 * @param zcMaterielAndTrayEntity
	 * @return 影响行数
	 */
	@Override
	public int MaterielTrayInsert(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity) {
		return inventoryMapper.MaterielTrayInsert(zcMaterielAndTrayEntity);
	}

	/**
	 * 查询托盘-库位绑定表中有没有损溢区的记录
	 * @return 托盘-库位绑定实体
	 */
	@Override
	public ZcTrayAndLocationEntity selectTrayLocationByLocationCode() {
		return inventoryMapper.selectTrayLocationByLocationCode();
	}

	/**
	 * 在托盘-库位绑定表（zc_tray_location）中追加托盘码
	 * @param zcTrayAndLocationEntity 托盘-库位绑定实体
	 * @return 影响行数
	 */
	@Override
	public int updateTrayLocationByID(ZcTrayAndLocationEntity zcTrayAndLocationEntity) {
		return inventoryMapper.updateTrayLocationByID(zcTrayAndLocationEntity);
	}

	/**
	 * 在托盘-库位绑定表中新增一条记录
	 * @param zcTrayAndLocationEntity2
	 * @return 影响行数
	 */
	@Override
	public int trayLocationInsert(ZcTrayAndLocationEntity zcTrayAndLocationEntity2) {
		return inventoryMapper.trayLocationInsert(zcTrayAndLocationEntity2);
	}

	/**       
	 * @param id
	 * @param table    
	 */
	@Override
	public void deleteById(String id, String table) {
		inventoryMapper.deleteById(id, table);
	}

	/**     
	 * 根据条件删除  
	 * @param pram
	 * @param string    
	 */
	@Override
	public void deleteByCode(String fileName, String pram, String string) {
		inventoryMapper.deleteByCode(fileName, pram, string);
	}

}
