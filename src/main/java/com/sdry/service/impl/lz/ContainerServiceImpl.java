package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.ContainerMapper;
import com.sdry.model.lz.Container;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.ContainerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName ContainerServiceImpl
 * @Description 容器信息
 * @Author lz
 * @Date 2019年4月16日 11:29:11
 * @Version 1.0
 */
@Service
public class ContainerServiceImpl implements ContainerService {

    @Resource
    ContainerMapper containerMapper;

    @Override
    public Long addContainer(Container container) {
        return containerMapper.addContainer(container);
    }

    @Override
    public Long deleteContainerById(long id) {
        return containerMapper.deleteContainerById(id);
    }

    @Override
    public Long updateContainer(Container container) {
        return containerMapper.updateContainer(container);
    }

    @Override
    public List<Container> queryContainerCriteria(LzQueryCriteria criteria) {
        return containerMapper.queryContainerCriteria(criteria);
    }

    @Override
    public int countContainerCriteria(LzQueryCriteria criteria) {
        return containerMapper.countContainerCriteria(criteria);
    }
}
