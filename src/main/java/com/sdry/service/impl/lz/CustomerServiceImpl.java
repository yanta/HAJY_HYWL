package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.CustomerMapper;
import com.sdry.model.lz.Contacts;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WaitSendArea;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.service.lz.CustomerService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * @ClassName CustomerServiceImpl
 * @Description 客户信息
 * @Author lz
 * @Date 2019年4月16日 09:28:49
 * @Version 1.0
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Resource
    CustomerMapper customerMapper;

    @Override
    public Long addCustomer(Customer customer) {
        return customerMapper.addCustomer(customer);
    }

    @Override
    public Long deleteCustomerById(long id) {
        return customerMapper.deleteCustomerById(id);
    }

    @Override
    public Long updateCustomer(Customer customer) {
        return customerMapper.updateCustomer(customer);
    }

    @Override
    public List<Customer> queryCustomerCriteria(LzQueryCriteria criteria) {
    	List<Customer> clist = customerMapper.queryCustomerCriteria(criteria);
    	//通过客户id查询他的所有仓库
    	for (Customer customer : clist) {
    		List<WarehouseRegion> wareList = customerMapper.selectWareListByCid(customer.getId());
    		for (WarehouseRegion warehouseRegion : wareList) {
    			customer.setRegion_name(warehouseRegion.getWarehouse_name()+"/"+warehouseRegion.getRegion_name()+",");
			}
		}
        return clist;
    }

    @Override
    public int countCustomerCriteria(LzQueryCriteria criteria) {
        return customerMapper.countCustomerCriteria(criteria);
    }

	@Override
	public Long addContacts(Contacts contacts) {
		return customerMapper.addContacts(contacts);
	}

	@Override
	public Long deleteContactsById(long id) {
		return customerMapper.deleteContactsById(id);
	}

	@Override
	public Long updateContacts(Contacts contacts) {
		return customerMapper.updateContacts(contacts);
	}

	@Override
	public List<Contacts> queryContactsCriteria(LzQueryCriteria criteria) {
		return customerMapper.queryContactsCriteria(criteria);
	}

	@Override
	public int countContactsCriteria(LzQueryCriteria criteria) {
		return customerMapper.countContactsCriteria(criteria);
	}

	@Override
	public List<Contacts> queryAllContacts() {
		return customerMapper.queryAllContacts();
	}

	@Override
	public List<Customer> queryCustomerCriteriaByType(LzQueryCriteria criteria) {
		return customerMapper.queryCustomerCriteriaByType(criteria);
	}

	@Override
	public int countCustomerCriteriaByType(LzQueryCriteria criteria) {
		return customerMapper.countCustomerCriteriaByType(criteria);
	}

	@Override
	public List<Materiel> queryInventoryInfoByCustomerId(long id) {
		return customerMapper.queryInventoryInfoByCustomerId(id);
	}

	/**
	 * 根据客户id和物料id查询待发货区数量
	 */
	@Override
	public List<WaitSendArea> queryWaitSendAreaCountByCustomerIdAndMaterielId(long materiel_id) {
		return customerMapper.queryWaitSendAreaCountByCustomerIdAndMaterielId(materiel_id);
	}

	@Override
	public List<ZcInventoryInfoEntity> queryInventorySmallNum(Long materiel_id) {
		return customerMapper.queryInventorySmallNum(materiel_id);
	}

	/**
	 * 修改待发货区数量
	 * @param response
	 * @param materiel_id 物料id
	 * @param customer_id 客户id
	 * @param materiel_num 物料数量
	 */
	@Override
	public void updateWaitSendAreaValue(String materiel_id, String materiel_num) {
		customerMapper.updateWaitSendAreaValue(materiel_id, materiel_num);
	}

	/**
	 * 删除待发货区数量为0的记录
	 * @param response
	 * @param materiel_id 物料id
	 */
	@Override
	public void deleteWaitSendAreaValue(String materiel_id) {
		customerMapper.deleteWaitSendAreaValue(materiel_id);
	}

	@Override
	public List<Customer> queryAllCustomer() {
		return customerMapper.queryAllCustomer();
	}
}