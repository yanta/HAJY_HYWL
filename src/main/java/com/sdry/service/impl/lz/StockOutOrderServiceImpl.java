package com.sdry.service.impl.lz;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.jyy.ReceiveMapper;
import com.sdry.mapper.llm.CancellingStockDetailMapper;
import com.sdry.mapper.llm.CancellingStockMapper;
import com.sdry.mapper.lz.StockOutOrderMapper;
import com.sdry.mapper.zc.ZcBindAndUnbindMapper;
import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.llm.BarCode;
import com.sdry.model.lz.Car;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Contacts;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.model.lz.OutStockOrderDetail;
import com.sdry.model.lz.OutStockOrderDetailPDAEntity;
import com.sdry.model.lz.OutStockOrderPDAEntity;
import com.sdry.model.lz.Staff;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.CustomerService;
import com.sdry.service.lz.StockOutOrderService;
/**
 * @ClassName StockOutOrderServiceImpl
 * @Description 出库管理
 * @Author lz
 * @Date 2019年3月26日 14:22:40
 * @Version 1.0
 */
@Service
public class StockOutOrderServiceImpl implements StockOutOrderService {

	@Resource
	StockOutOrderMapper stockOutMapper;
	
	@Resource
	CancellingStockMapper cancellingStockMapper;
	
	@Resource
	ReceiveMapper receiveMapper;
	
	@Resource
	CustomerService customerService;
	
	@Resource
	CancellingStockDetailMapper cancellingStockDetailMapper;
	
	@Resource
	ZcBindAndUnbindMapper zcBindAndUnbindMapper;
	
	@Override
	public List<OutStockOrder> queryOutStockOrderCriteria(LzQueryCriteria criteria) {
		List<OutStockOrder> outStockOrderList = stockOutMapper.queryOutStockOrderCriteria(criteria);
		/*if(outStockOrderList.size() > 0) {
			for(OutStockOrder orderList : outStockOrderList){
				if(orderList.getWarehouse_id() != null && !"".equals(orderList.getWarehouse_id())){
					Warehouse warehouse = stockOutMapper.getWarehouseById(orderList.getWarehouse_id());
					if(warehouse != null){
						orderList.setWarehouse_name(warehouse.getWarehouse_name());
						orderList.setWarehouse_address(warehouse.getWarehouse_address());
						orderList.setUserName(warehouse.getUserName());
						orderList.setPhone(warehouse.getPhone());
					}
				}
				if(orderList.getCustomer_id() != null && !"".equals(orderList.getCustomer_id())){
					Customer customer = cancellingStockMapper.getClientById(orderList.getCustomer_id());
					if(customer != null){
						orderList.setCustomer_name(customer.getCustomer_name());
						orderList.setCustomer_adress(customer.getCustomer_adress());
						orderList.setContacts_name(customer.getContacts_name());
						orderList.setContacts_tel(customer.getContacts_tel());
					}
				}
			}
		}*/
		return outStockOrderList;
	}
	
	@Override
	public int countOutStockOrderCriteria(LzQueryCriteria criteria) {
		return stockOutMapper.countOutStockOrderCriteria(criteria);
	}

	/**
	 * 查询已出库的出库单
	 * @param criteria
	 * @return
	 */
	@Override
	public List<OutStockOrder> queryOutStockOrderCriteria2(LzQueryCriteria criteria) {
		return stockOutMapper.queryOutStockOrderCriteria2(criteria);
	}

	/**
	 * 查询已出库的出库单数量
	 * @param criteria
	 * @return
	 */
	@Override
	public int countOutStockOrderCriteria2(LzQueryCriteria criteria) {
		return stockOutMapper.countOutStockOrderCriteria2(criteria);
	}

	@Override
	public Long addStockOutOrder(OutStockOrder order) {
		return stockOutMapper.addStockOutOrder(order);
	}

	@Override
	public Long addStockOutDetailList(OutStockOrderDetail detail) {
		return stockOutMapper.addStockOutDetailList(detail);
	}

	/**
	 * 查询推荐库位和库存
	 * TODO
	 */
	@Override
	public List<OutStockOrderDetail> queryOutStockOrderCriteriaById(
			LzQueryCriteria criteria) {
		
		List<OutStockOrderDetail> outStockOrderDetailList = stockOutMapper.queryOutStockOrderCriteriaById(criteria);
		/*for (int i = 0; i < outStockOrderDetailList.size(); i++) {
			//根据物料ID查库存
			Integer good  = cancellingStockDetailMapper.getGoodQuantityInInventoryInfo(outStockOrderDetailList.get(i).getMaterial_id());
			Integer bad = cancellingStockDetailMapper.getBadQuantityInRejectsWarehouse(outStockOrderDetailList.get(i).getMaterial_id());
			if(good == null){
				good = 0;
			}
			if(bad == null){
				bad = 0;
			}
			//通过产品码查询物料所在最早库位
			String location_name = "";
			WarehouseRegionLocation warehouseRegionLocation = zcBindAndUnbindMapper.selectTopLocationDown(outStockOrderDetailList.get(i).getMateriel_num());
			if(warehouseRegionLocation != null){
				location_name = warehouseRegionLocation.getLocation_name();
				//库位推荐
				outStockOrderDetailList.get(i).setRecommend_stock(location_name);
			}
			//库存总数量
			outStockOrderDetailList.get(i).setmNum_sum(good + bad);
			//良品数
			outStockOrderDetailList.get(i).setGood_num(good);
		}*/
		return outStockOrderDetailList;
	}
	
	/**
	 * 查询推荐库位和库存
	 * TODO
	 */
	@Override
	public List<OutStockOrderDetail> queryOutStockOrderCriteriaById2(LzQueryCriteria criteria) {
		
		List<OutStockOrderDetail> outStockOrderDetailList = stockOutMapper.queryOutStockOrderCriteriaById(criteria);
		for (int i = 0; i < outStockOrderDetailList.size(); i++) {
			//根据物料ID查库存
			Integer sum  = cancellingStockDetailMapper.getSumQuantityInInventoryInfo(outStockOrderDetailList.get(i).getMaterial_id());
			if(sum == null){
				sum = 0;
			}
			//库存总数量
			outStockOrderDetailList.get(i).setmNum_sum(sum);
		}
		return outStockOrderDetailList;
	}
	/**
	 * 查询推荐库位和库存
	 * TODO
	 */
	@Override
	public List<OutStockOrderDetail> queryOutStockOrderCriteriaById3(LzQueryCriteria criteria) {
		
		List<OutStockOrderDetail> outStockOrderDetailList = stockOutMapper.queryOutStockOrderCriteriaById(criteria);
		for (int i = 0; i < outStockOrderDetailList.size(); i++) {
			//根据物料ID查库存
			Integer sum  = cancellingStockDetailMapper.getSumQuantityInInventoryInfoCache(outStockOrderDetailList.get(i).getMaterial_id());
			if(sum == null){
				sum = 0;
			}
			//库存总数量
			outStockOrderDetailList.get(i).setmNum_sum(sum);
		}
		return outStockOrderDetailList;
	}

	@Override
	public int countOutStockOrderCriteriaById(LzQueryCriteria criteria) {
		return stockOutMapper.countOutStockOrderCriteriaById(criteria);
	}

	@Override
	public Long updateOutStockOrderById(String id, String fieldName,
			String fieldValue) {
		return stockOutMapper.updateOutStockOrderById(id, fieldName, fieldValue);
	}

	@Override
	public Long updateOutStockOrderDetailById(String id, String fieldName,
			String fieldValue) {
		return stockOutMapper.updateOutStockOrderDetailById(id, fieldName, fieldValue);
	}

	@Override
	public Long deleteOutStockOrderById(long id) {
		return stockOutMapper.deleteOutStockOrderById(id);
	}

	@Override
	public Long deleteOutStockOrderDetailById1(long id) {
		return stockOutMapper.deleteOutStockOrderDetailById1(id);
	}

	@Override
	public List<OutStockOrder> queryOutStockOrderCriteriaPDA(
			LzQueryCriteria criteria) {
		return stockOutMapper.queryOutStockOrderCriteriaPDA(criteria);
	}

	@Override
	public int countOutStockOrderCriteriaPDA(LzQueryCriteria criteria) {
		return stockOutMapper.countOutStockOrderCriteriaPDA(criteria);
	}

	@Override
	public List<Customer> queryAllCustomer() {
		return stockOutMapper.queryAllCustomer();
	}

	@Override
	public List<Staff> queryAllStaff() {
		return stockOutMapper.queryAllStaff();
	}

	@Override
	public List<Car> queryAllCar() {
		return stockOutMapper.queryAllCar();
	}

	@Override
	public List<OutStockOrder> queryAllOutStockOrder() {
		return stockOutMapper.queryAllOutStockOrder();
	}

	@Override
	public List<Customer> queryCustomerByName(String customer_name) {
		return stockOutMapper.queryCustomerByName(customer_name);
	}

	@Override
	public Long updateZcInventoryInfoById(String materiel_id, String res3) {
		List<ZcInventoryInfoEntity> inventoryInfoEntityList = customerService.queryInventorySmallNum(Long.parseLong(materiel_id));
		for (int i = 0; i < inventoryInfoEntityList.size(); i++) {
			//减去待发货区数量后的值与库存中的每个进行比较
			
			/*int tempValue = Integer.parseInt(res3) - inventoryInfoEntityList.get(i).getSmallNum();
			if(tempValue >= 0){
				//将第一条信息改为0
				//updateZcInventoryInfoById(inventoryInfoEntityList.get(i).getId());
				//将
			} else {
				
			}*/
		}
		return stockOutMapper.updateZcInventoryInfoById(materiel_id, res3);
	}

	@Override
	public List<OutStockOrder> queryOutStockOrderAndroid() {
		return stockOutMapper.queryOutStockOrderAndroid();
	}

	@Override
	public List<OutStockOrderDetail> queryOutStockOrderDetailByIdAndroid(String order_id) {
		return stockOutMapper.queryOutStockOrderDetailByIdAndroid(order_id);
	}

	@Override
	public ZcInventoryInfoEntity triggerWarning(String zcInventoryInfoId) {
		return stockOutMapper.triggerWarning(zcInventoryInfoId);
	}

	@Override
	public Long updateStockOutOrderDetailStatusAndroid(BarCode barCode) {
		return stockOutMapper.updateStockOutOrderDetailStatusAndroid(barCode);
	}

	@Override
	public Warehouse queryWarehouseById(long id) {
		return stockOutMapper.queryWarehouseById(id);
	}

	@Override
	public ZcSysUserEntity queryUserById(long id) {
		return stockOutMapper.queryUserById(id);
	}

	@Override
	public Customer queryCustomerById(long id) {
		return stockOutMapper.queryCustomerById(id);
	}

	@Override
	public Contacts queryContactsById(long id) {
		return stockOutMapper.queryContactsById(id);
	}

	@Override
	public Long updateStockOutOrder(OutStockOrder outStockOrder) {
		return stockOutMapper.updateStockOutOrder(outStockOrder);
	}

	@Override
	public List<Materiel> queryWaitSendAreaAndStock(LzQueryCriteria criteria) {
		return stockOutMapper.queryWaitSendAreaAndStock(criteria);
	}

	@Override
	public int countWaitSendAreaAndStock(LzQueryCriteria criteria) {
		return stockOutMapper.countWaitSendAreaAndStock(criteria);
	}

	@Override
	public List<ZcInventoryInfoEntity> queryInventoryInfo(
			LzQueryCriteria criteria) {
		return stockOutMapper.queryInventoryInfo(criteria);
	}

	@Override
	public int countInventoryInfo(LzQueryCriteria criteria) {
		return stockOutMapper.countInventoryInfo(criteria);
	}

	/**
	 * PDA查询所有出库单接口
	 * @return
	 */
	@Override
	public List<OutStockOrderPDAEntity> queryOutStockOrderForPDA() {
		List list = new ArrayList();
		List<OutStockOrder> outStockOrderList = stockOutMapper.queryOutStockOrderForPDA();
		if(outStockOrderList.size() > 0){
			for(OutStockOrder orderList : outStockOrderList){
				OutStockOrderPDAEntity outStockOrderPDAEntity = new OutStockOrderPDAEntity();
				if(orderList.getWarehouse_id() != null && !"".equals(orderList.getWarehouse_id())){
					Warehouse warehouse = stockOutMapper.getWarehouseById(orderList.getWarehouse_id());
					if(warehouse != null){
						//插入出库单id
						outStockOrderPDAEntity.setId(orderList.getId());
						//插入出库单单号
						outStockOrderPDAEntity.setOut_stock_num(orderList.getOut_stock_num());
						//插入仓库名称
						outStockOrderPDAEntity.setWarehouse_name(warehouse.getWarehouse_name());
					}
				}
				if(orderList.getCustomer_id() != null && !"".equals(orderList.getCustomer_id())){
					Customer customer = cancellingStockMapper.getClientById(orderList.getCustomer_id());
					if(customer != null){
						//插入客户名称
						outStockOrderPDAEntity.setCustomer_name(customer.getCustomer_name());
					}
				}
				list.add(outStockOrderPDAEntity);
			}
		}
		return list;
	}
	/**
	 * PDA查询所有紧急出库单接口
	 * @return
	 */
	@Override
	public List<OutStockOrderPDAEntity> queryUrgencyOrderForPDA() {
		List list = new ArrayList();
		List<OutStockOrder> outStockOrderList = stockOutMapper.queryUrgencyOrderForPDA();
		if(outStockOrderList.size() > 0){
			for(OutStockOrder orderList : outStockOrderList){
				OutStockOrderPDAEntity outStockOrderPDAEntity = new OutStockOrderPDAEntity();
				if(orderList.getWarehouse_id() != null && !"".equals(orderList.getWarehouse_id())){
					Warehouse warehouse = stockOutMapper.getWarehouseById(orderList.getWarehouse_id());
					if(warehouse != null){
						//插入出库单id
						outStockOrderPDAEntity.setId(orderList.getId());
						//插入出库单单号
						outStockOrderPDAEntity.setOut_stock_num(orderList.getOut_stock_num());
						//插入仓库名称
						outStockOrderPDAEntity.setWarehouse_name(warehouse.getWarehouse_name());
					}
				}
				if(orderList.getCustomer_id() != null && !"".equals(orderList.getCustomer_id())){
					Customer customer = cancellingStockMapper.getClientById(orderList.getCustomer_id());
					if(customer != null){
						//插入客户名称
						outStockOrderPDAEntity.setCustomer_name(customer.getCustomer_name());
					}
				}
				list.add(outStockOrderPDAEntity);
			}
		}
		return list;
	}

	/**
	 * PDA根据出库单ID查询出库单详细接口
	 * @return
	 */
	@Override
	public List<OutStockOrderDetailPDAEntity> queryOutStockOrderDetailByIdForPDA(long out_stock_num_id) {
		List list = new ArrayList();
		List<OutStockOrderDetail> outStockOrderDetailList = stockOutMapper.queryOutStockOrderDetailByIdForPDA(out_stock_num_id);
		if(outStockOrderDetailList.size() > 0){
			for (OutStockOrderDetail outStockOrderDetail : outStockOrderDetailList) {
				OutStockOrderDetailPDAEntity outStockOrderDetailPDAEntity = new OutStockOrderDetailPDAEntity();
				//1.插入出库单ID
				outStockOrderDetailPDAEntity.setOut_stock_num_id(outStockOrderDetail.getOut_stock_num_id());
				//2.根据物料id查询物料实体
				Materiel materiel = stockOutMapper.queryMaterielById(outStockOrderDetail.getMaterial_id());
				outStockOrderDetailPDAEntity.setMateriel(materiel);
				//3.预计数量
				outStockOrderDetailPDAEntity.setPre_num(outStockOrderDetail.getMaterial_quantity());
				//4.实际数量
				outStockOrderDetailPDAEntity.setAct_num(outStockOrderDetail.getActual_quantity());
				//Customer customer = stockOutMapper.queryCustomerNameById(outStockOrderDetail.getCustomer_id());
				//5.供应商名称
				outStockOrderDetailPDAEntity.setCustomer_name(materiel.getCustomer_name());
				//6.物料位置
				outStockOrderDetailPDAEntity.setMateriel_location(materiel.getRegion_name());
				//7.规格型号
				outStockOrderDetailPDAEntity.setMateriel_size_properties(materiel.getMateriel_size() + "/" + materiel.getMateriel_properties());
				//8.出库单详细id
				outStockOrderDetailPDAEntity.setId(outStockOrderDetail.getId());
				list.add(outStockOrderDetailPDAEntity);
			}
		}
		return list;
	}

	/**
	 * 根据出库单详细修改出库单的实际值
	 */
	@Override
	public void updateOutStockOrderDetailActNum(String id, String act_num, String single_num, String big_num, String small_num) {
		stockOutMapper.updateOutStockOrderDetailActNum(id, act_num, single_num, big_num, small_num);
	}

	/**
	 * 根据出库单id修改出库单状态
	 * @param outStockOrderDetail
	 */
	@Override
	public void updateOutStockOrderStatus(OutStockOrderDetailPDAEntity outStockOrderDetail) {
		stockOutMapper.updateOutStockOrderStatus(outStockOrderDetail);
	}

	/**
	 * 提交待发货区和库存物料
	 * @param outStockOrderDetail
	 * @return
	 */
	@Override
	public Long addStockOutOrderDetail(OutStockOrderDetail outStockOrderDetail) {
		Integer cache = stockOutMapper.queryCacheQuantity(outStockOrderDetail.getMaterial_id());
		if(cache == null) {
			cache = 0;
		}
		Integer good = stockOutMapper.queryGoodQuantity(outStockOrderDetail.getMaterial_id());
		if(good == null) {
			good = 0;
		}
		Integer unCheck = stockOutMapper.queryUnCheckQuantity(outStockOrderDetail.getMaterial_id());
		if(unCheck == null) {
			unCheck = 0;
		}
		int i = stockOutMapper.queryOutStockOrderByNumber(outStockOrderDetail.getOut_stock_num_id());
		if(i == 1){
			outStockOrderDetail.setSingle_num(cache);
		}else if(i == 2){
			outStockOrderDetail.setSingle_num(unCheck);
		}
		outStockOrderDetail.setSmall_num(cache + good + unCheck);
		return stockOutMapper.addStockOutOrderDetail(outStockOrderDetail);
	}

	/**
     * 查找所有供应商
     * @return
     */
	@Override
	public List<Customer> queryAllCustomerByType() {
		return stockOutMapper.queryAllCustomerByType();
	}

	/**
	 * 查找所有库区
	 * @return
	 */
	@Override
	public List<WarehouseRegion> queryAllRegion() {
		return stockOutMapper.queryAllRegion();
	}

	/**
	 * 查询所有主机厂
	 * @throws Exception
	 */
	@Override
	public List<Customer> queryAllCustomerByType1() {
		return stockOutMapper.queryAllCustomerByType1();
	}

	@Override
	public Car queryDriver(long id) {
		return stockOutMapper.queryDriver(id);
	}

	/**
	 * 查询该出库单详细是否存在该物料
	 * @param outStockOrderDetail 出库单详细
	 * @throws Exception
	 */
	@Override
	public List<OutStockOrderDetail> queryExistMaterial(
			OutStockOrderDetail outStockOrderDetail) {
		return stockOutMapper.queryExistMaterial(outStockOrderDetail);
	}

	@Override
	public List<OutStockOrder> queryOutStockOrderStatusByOutStockNumId(
			long out_stock_num_id) {
		return stockOutMapper.queryOutStockOrderStatusByOutStockNumId(out_stock_num_id);
	}

	@Override
	public int addCodeMarkOut(CodeMark codeMark) {
		return stockOutMapper.addCodeMarkOut(codeMark);
	}

	@Override
	public CodeMark queryCodeMarkOutByReciveid(long rid) {
		return stockOutMapper.queryCodeMarkOutByReciveid(rid);
	}

	/**
	 * 根据质检单详情id查询质检单
	 * @param receive_detail_id
	 * @return
	 */
	@Override
	public ReceiveDetailQuality queryReceiveDetailQualityById(Long receive_detail_id) {
		return stockOutMapper.queryReceiveDetailQualityById(receive_detail_id);
	}

	/**
	 * 根据质检单中的物料id和批次减库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	@Override
	public void reductionInventory(String mid, String pici, String num) {
		stockOutMapper.reductionInventory(mid, pici, num);
	}
	/**
	 * 根据质检单中的物料id和批次减缓存库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	@Override
	public void reductionInventoryCache(String mid, String pici, String num) {
		stockOutMapper.reductionInventoryCache(mid, pici, num);
	}

	/**
	 * 根据精准码删除已出库的精准码
	 * @param code
	 */
	@Override
	public void deleteCodemarkOutByCode(String code) {
		stockOutMapper.deleteCodemarkOutByCode(code);
	}

	/**
	 * 根据质检单中的物料id和批次减不良品库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	@Override
	public void reductionRejectsWarehouse(String mid, String pici, String num) {
		stockOutMapper.reductionRejectsWarehouse(mid, pici, num);
	}

	/**       
	 * @return    
	 */
	@Override
	public List<OutStockOrderPDAEntity> queryDeliveryOrderForPDA() {
		List list = new ArrayList();
		List<OutStockOrder> outStockOrderList = stockOutMapper.queryDeliveryOrderForPDA();
		if(outStockOrderList.size() > 0){
			for(OutStockOrder orderList : outStockOrderList){
				OutStockOrderPDAEntity outStockOrderPDAEntity = new OutStockOrderPDAEntity();
				if(orderList.getWarehouse_id() != null && !"".equals(orderList.getWarehouse_id())){
					Warehouse warehouse = stockOutMapper.getWarehouseById(orderList.getWarehouse_id());
					if(warehouse != null){
						//插入出库单id
						outStockOrderPDAEntity.setId(orderList.getId());
						//插入出库单单号
						outStockOrderPDAEntity.setOut_stock_num(orderList.getOut_stock_num());
						//插入仓库名称
						outStockOrderPDAEntity.setWarehouse_name(warehouse.getWarehouse_name());
					}
				}
				if(orderList.getCustomer_id() != null && !"".equals(orderList.getCustomer_id())){
					Customer customer = cancellingStockMapper.getClientById(orderList.getCustomer_id());
					if(customer != null){
						//插入客户名称
						outStockOrderPDAEntity.setCustomer_name(customer.getCustomer_name());
					}
				}
				list.add(outStockOrderPDAEntity);
			}
		}
		return list;
	}

	@Override
	public int cancelout(Map map) {
		// TODO Auto-generated method stub
		return stockOutMapper.cancelout(map);
	}

	@Override
	public int restorout(Map map) {
		// TODO Auto-generated method stub
		return stockOutMapper.restorout(map);
	}
}
