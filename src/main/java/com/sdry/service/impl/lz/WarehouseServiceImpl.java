package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.WarehouseMapper;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.service.lz.WarehouseService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * @ClassName WarehouseServiceImpl
 * @Description 仓库信息
 * @Author lz
 * @Date 2019年4月9日 10:19:45
 * @Version 1.0
 */
@Service
public class WarehouseServiceImpl implements WarehouseService {

    @Resource
    WarehouseMapper warehouseMapper;

    @Override
    public Long addWarehouse(Warehouse warehouse) {
        return warehouseMapper.addWarehouse(warehouse);
    }

    @Override
    public Long deleteWarehouseById(long id) {
        return warehouseMapper.deleteWarehouseById(id);
    }

    @Override
    public Long updateWarehouse(Warehouse warehouse) {
        return warehouseMapper.updateWarehouse(warehouse);
    }

    @Override
    public List<Warehouse> queryWarehouseCriteria(LzQueryCriteria criteria) {
        return warehouseMapper.queryWarehouseCriteria(criteria);
    }

    @Override
    public int countWarehouseCriteria(LzQueryCriteria criteria) {
        return warehouseMapper.countWarehouseCriteria(criteria);
    }


    @Override
    public Long addWarehouseRegion(WarehouseRegion warehouseRegion) {
        return warehouseMapper.addWarehouseRegion(warehouseRegion);
    }

    @Override
    public Long deleteWarehouseRegionById(long id) {
        return warehouseMapper.deleteWarehouseRegionById(id);
    }

    @Override
    public Long updateWarehouseRegion(WarehouseRegion warehouseRegion) {
        return warehouseMapper.updateWarehouseRegion(warehouseRegion);
    }

    @Override
    public List<WarehouseRegion> queryWarehouseRegionCriteria(ZcGeneralQueryEntity zcGeneralQueryEntity) {
    	//表名
		String tab = "lz_warehouse_region r left join lz_warehouse w on w.id = r.warehouse_id";
		//查询字段
		String strFld = "r.*,w.warehouse_name";
		//排序
		String sort = " r.id DESC ";
		//0返回结果集，1返回count数
		byte isGetCount = 0;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setSort(sort);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
		zcGeneralQueryEntity.setStrFld(strFld);
        return warehouseMapper.queryWarehouseRegionCriteria(zcGeneralQueryEntity);
    }

    @Override
    public int countWarehouseRegionCriteria(ZcGeneralQueryEntity zcGeneralQueryEntity) {
    	//表名
		String tab = "lz_warehouse_region ";
		//0返回结果集，1返回count数
		byte isGetCount = 1;
		zcGeneralQueryEntity.setTab(tab);
		zcGeneralQueryEntity.setIsGetCount(isGetCount);
        return warehouseMapper.countWarehouseRegionCriteria(zcGeneralQueryEntity);
    }



    @Override
    public Long addWarehouseRegionLocation(WarehouseRegionLocation warehouseRegionLocation) {
        return warehouseMapper.addWarehouseRegionLocation(warehouseRegionLocation);
    }

    @Override
    public Long deleteWarehouseRegionLocationById(long id) {
        return warehouseMapper.deleteWarehouseRegionLocationById(id);
    }

    @Override
    public Long updateWarehouseRegionLocation(WarehouseRegionLocation warehouseRegionLocation) {
        return warehouseMapper.updateWarehouseRegionLocation(warehouseRegionLocation);
    }

    @Override
    public List<WarehouseRegionLocation> queryWarehouseRegionLocationCriteria(LzQueryCriteria criteria) {
        return warehouseMapper.queryWarehouseRegionLocationCriteria(criteria);
    }

    @Override
    public int countWarehouseRegionLocationCriteria(LzQueryCriteria criteria) {
        return warehouseMapper.countWarehouseRegionLocationCriteria(criteria);
    }

	@Override
	public List<Warehouse> queryAllWarehouse() {
		return warehouseMapper.queryAllWarehouse();
	}

	@Override
	public List<WarehouseRegion> queryAllWarehouseRegion() {
		return warehouseMapper.queryAllWarehouseRegion();
	}

}