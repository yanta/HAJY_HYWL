package com.sdry.service.impl.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.LoadingProcessMapper;
import com.sdry.model.lz.LoadingProcess;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.service.lz.LoadingProcessService;

/**
 * @ClassName LoadingProcessServiceImpl
 * @Description 装车处理
 * @Author lz
 * @Date 2019年4月25日 11:27:23
 * @Version 1.0
 */
@Service
public class LoadingProcessServiceImpl implements LoadingProcessService{

	@Resource
	LoadingProcessMapper loadingProcessMapper;

	@Override
	public List<OutStockOrder> queryOutStockOrderByOutStockOrderNum(
			String out_stock_order_num) {
		return loadingProcessMapper.queryOutStockOrderByOutStockOrderNum(out_stock_order_num);
	}

	@Override
	public Long addLoadingProcess(LoadingProcess loadingProcess) {
		return loadingProcessMapper.addLoadingProcess(loadingProcess);
	}

	@Override
	public Long deleteLoadingProcessById(long id) {
		return loadingProcessMapper.deleteLoadingProcessById(id);
	}
	
}
