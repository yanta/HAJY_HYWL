package com.sdry.service.impl.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.StaffMapper;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Staff;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.service.lz.StaffService;

/**
 * @ClassName StaffServiceImpl
 * @Description 员工信息
 * @Author lz
 * @Date 2019年4月16日 10:51:25
 * @Version 1.0
 */
@Service
public class StaffServiceImpl implements StaffService {

    @Resource
    StaffMapper staffMapper;

    @Override
    public Long addStaff(Staff staff) {
        return staffMapper.addStaff(staff);
    }

    @Override
    public Long deleteStaffById(long id) {
        return staffMapper.deleteStaffById(id);
    }

    @Override
    public Long updateStaff(Staff staff) {
        return staffMapper.updateStaff(staff);
    }

    @Override
    public List<Staff> queryStaffCriteria(LzQueryCriteria criteria) {
        return staffMapper.queryStaffCriteria(criteria);
    }

    @Override
    public int countStaffCriteria(LzQueryCriteria criteria) {
        return staffMapper.countStaffCriteria(criteria);
    }
    
    @Override
	public List<ZcSysUserEntity> queryStaffByDept(long id) {
		return staffMapper.queryStaffByDept(id);
	}

	@Override
	public ZcSysUserEntity queryStaffById(long id) {
		return staffMapper.queryStaffById(id);
	}
}