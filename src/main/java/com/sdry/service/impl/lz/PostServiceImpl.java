package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.PostMapper;
import com.sdry.model.lz.Department;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Post;
import com.sdry.service.lz.PostService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.List;

/**
 * @ClassName PostServiceImpl
 * @Description 岗位信息
 * @Author lz
 * @Date 2019年4月16日 09:50:08
 * @Version 1.0
 */
@Service
public class PostServiceImpl implements PostService {

    @Resource
    PostMapper postMapper;

    @Override
    public Long addPost(Post post) {
        return postMapper.addPost(post);
    }

    @Override
    public Long deletePostById(long id) {
        return postMapper.deletePostById(id);
    }

    @Override
    public Long updatePost(Post post) {
        return postMapper.updatePost(post);
    }

    @Override
    public List<Post> queryPostCriteria(LzQueryCriteria criteria) {
        return postMapper.queryPostCriteria(criteria);
    }

    @Override
    public int countPostCriteria(LzQueryCriteria criteria) {
        return postMapper.countPostCriteria(criteria);
    }

	@Override
	public List<Department> queryAllDept() {
		return postMapper.queryAllDept();
	}

	@Override
	public List<Post> queryAllPost() {
		return postMapper.queryAllPost();
	}
}
