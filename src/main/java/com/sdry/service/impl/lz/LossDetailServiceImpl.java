package com.sdry.service.impl.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.LossDetailMapper;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LossDetail;
import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzInventoryDetailsCodeEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.service.lz.LossDetailService;

/**
 * @ClassName LossDetailServiceImpl
 * @Description 损益单详细
 * @Author zy
 * @Date 2019年4月16日 10:23:46
 * @Version 1.0
 */
@Service
public class LossDetailServiceImpl implements LossDetailService {
	 @Resource
	 LossDetailMapper lossDetailMapper;
	@Override
	public Long addLossDetail(LossDetail lossDetail) {
		// TODO Auto-generated method stub
		return lossDetailMapper.addLossDetail(lossDetail);
	}
	@Override
	public List<LossDetail> queryLossDetail(Long lossid) {
		// TODO Auto-generated method stub
		return lossDetailMapper.queryLossDetail(lossid);
	}
	@Override
	public int countLossDetail(Long lossid) {
		// TODO Auto-generated method stub
		return lossDetailMapper.countLossDetail(lossid);
	}
	@Override
	public List<LossDetail> queryMidByLossId(Long lossid) {
		// TODO Auto-generated method stub
		return lossDetailMapper.queryMidByLossId(lossid);
	}
	@Override
	public List<ZcInventoryInfoEntity> queryMnumByMId(Long mid) {
		// TODO Auto-generated method stub
		return lossDetailMapper.queryMnumByMId(mid);
	}
	@Override
	public LossSpillover queryLossById(Long lossid) {
		// TODO Auto-generated method stub
		return lossDetailMapper.queryLossById(lossid);
	}
	@Override
	public Long updateLossDetail(LossDetail lossDetail) {
		// TODO Auto-generated method stub
		return lossDetailMapper.updateLossDetail(lossDetail);
	}
	/**       
	 * @param mid
	 * @return    
	 */
	@Override
	public List<CodeMark> queryCodeById(Long mid) {
		return lossDetailMapper.queryCodeById(mid);
	}
	/**       
	 * @param lossId
	 * @return    
	 */
	@Override
	public List<LzInventoryDetailsCodeEntity> queryCodeById2(Long lossId, Long mid) {
		return lossDetailMapper.queryCodeById2(lossId, mid);
	}

   
    
}
