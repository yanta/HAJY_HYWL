package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.DepartmentMapper;
import com.sdry.model.lz.Department;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.DepartmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName DepartmentServiceImpl
 * @Description 部门信息
 * @Author lz
 * @Date 2019年4月16日 10:25:30
 * @Version 1.0
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Resource
    DepartmentMapper departmentMapper;

    @Override
    public Long addDept(Department dept) {
        return departmentMapper.addDept(dept);
    }

    @Override
    public Long deleteDeptById(long id) {
        return departmentMapper.deleteDeptById(id);
    }

    @Override
    public Long updateDept(Department dept) {
        return departmentMapper.updateDept(dept);
    }

    @Override
    public List<Department> queryDeptCriteria(LzQueryCriteria criteria) {
        return departmentMapper.queryDeptCriteria(criteria);
    }

    @Override
    public int countDeptCriteria(LzQueryCriteria criteria) {
        return departmentMapper.countDeptCriteria(criteria);
    }
    @Override
   	public List<Department> queryAllDept() {
   		// TODO Auto-generated method stub
   		return departmentMapper.queryAllDept();
   	}
}