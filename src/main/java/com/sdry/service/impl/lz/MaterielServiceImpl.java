package com.sdry.service.impl.lz;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.MaterielMapper;
import com.sdry.model.lz.Container;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzCustomerAndWarehouse;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.service.lz.MaterielService;

/**
 * @ClassName MaterielServiceImpl
 * @Description 物料信息
 * @Author lz
 * @Date 2019年4月15日 17:52:06
 * @Version 1.0
 */
@Service
public class MaterielServiceImpl implements MaterielService {

    @Resource
    MaterielMapper materielMapper;


    @Override
    public Long addMateriel(Materiel materiel) {
        return materielMapper.addMateriel(materiel);
    }

    @Override
    public Long deleteMaterielById(long id) {
        return materielMapper.deleteMaterielById(id);
    }

    @Override
    public Long updateMateriel(Materiel materiel) {
        return materielMapper.updateMateriel(materiel);
    }

    @Override
    public List<Materiel> queryMaterielCriteria(LzQueryCriteria criteria) {
    	/*List<Materiel> materielLists =  materielMapper.queryMaterielCriteria(criteria);
    	 for (Materiel materiel : materielLists) {
 			String client="";
 			String clientName="";
 			String ware="";
 			String wareName="";
 			List<Customer> customerList = materielMapper.selectCustomerByMid(materiel.getId());
 			List<ZcMaterielAndWarehouseEntity> bundList = materielMapper.selectBundListByMid(materiel.getId());
 			for (ZcMaterielAndWarehouseEntity zcMaterielAndWarehouseEntity : bundList) {
 				String location = zcMaterielAndWarehouseEntity.getLocationName();
				if(location == null){
					location ="";
				}
				ware += zcMaterielAndWarehouseEntity.getWareId()+"/"+zcMaterielAndWarehouseEntity.getRegionId()+"/"+zcMaterielAndWarehouseEntity.getLocationId()+",";
				wareName += zcMaterielAndWarehouseEntity.getWareName()+"/"+zcMaterielAndWarehouseEntity.getRegionName()+"/"+location+"，";
			}
 			for (Customer customer : customerList) {
 				client += customer.getId()+",";
 				clientName += customer.getCustomer_name()+"，";
			}
 			materiel.setClient(client);
 			materiel.setClientName(clientName);
 			materiel.setWare(ware);
 			materiel.setWareName(wareName);
		}*/
    	 return materielMapper.queryMaterielCriteria(criteria);
    }

    @Override
    public int countMaterielCriteria(LzQueryCriteria criteria) {
        return materielMapper.countMaterielCriteria(criteria);
    }

	@Override
	public List<Container> queryAllContainer() {
		return materielMapper.queryAllContainer();
	}
	@Override
	public Materiel queryMaterielById(long id) {
		// TODO Auto-generated method stub
		return materielMapper.queryMaterielById(id);
	}

	/*@Override
	public int bindingCustomer(Materiel materiel) {
		ZcMaterielAndCustomerEntity zcMaterielAndCustomerEntity = new ZcMaterielAndCustomerEntity();
		zcMaterielAndCustomerEntity.setMid(materiel.getId());
		//删除之前的绑定
		materielMapper.deleteBindingCustomer(materiel.getId());
		String client = materiel.getClient();
		String cids[] = client.split(",");
		int affact=0;
		for (String cid : cids) {
			zcMaterielAndCustomerEntity.setCid(Long.parseLong(cid));
			affact = materielMapper.bindingCustomer(zcMaterielAndCustomerEntity);
		}
		return affact;
	}*/
	//仓库列表
	@Override
	public List<Warehouse> selectAllWarehouse() {
		return materielMapper.selectAllWarehouse();
	}
	//通过仓库id查库区
	@Override
	public List<WarehouseRegion> selectRegionByWareid(Long id) {
		return materielMapper.selectRegionByWareid(id);
	}

	@Override
	public List<WarehouseRegionLocation> selectLocationByRegionid(Long id) {
		return materielMapper.selectLocationByRegionid(id);
	}

	//绑定仓库
	@Override
	public int warehouseBindingWarehouse(Customer customer) {
		//删除之前的绑定
		materielMapper.deleteWarehouseBindingWarehouse(customer.getId());
		//客户id
		Long cid = customer.getId();
		//客户表里的仓库id字符串数组
		String wares[] = customer.getRegion_idArr().split(",");
		int affact = 0;
		Long wId = 0L;
		Long rId = 0L;
		//Long lId = 0L;
		
		for (int i = 0; i < wares.length; i++) {
			if(wares[i].split("/").length == 3){
				wId = Long.parseLong(wares[i].split("/")[0]);
				rId = Long.parseLong(wares[i].split("/")[1]);
				//lId = Long.parseLong(wares[i].split("/")[2]);
			}else if(wares[i].split("/").length == 2){
				wId = Long.parseLong(wares[i].split("/")[0]);
				rId = Long.parseLong(wares[i].split("/")[1]);
			}
			if(wId != 0){
				LzCustomerAndWarehouse lzCustomerAndWarehouse = new LzCustomerAndWarehouse();
				lzCustomerAndWarehouse.setCid(cid);
				lzCustomerAndWarehouse.setWarehouse_id(wId);
				lzCustomerAndWarehouse.setRegion_id(rId);
				//lzCustomerAndWarehouse.setLocalhost_id(lId);
				int status = materielMapper.selectStatusByRid(rId);
				if(status == 1){
					return -2;
				}
				affact = materielMapper.warehouseBindingWarehouse(lzCustomerAndWarehouse);
				//改变库区表的状态为1(表示已经被供应商绑定)
				//materielMapper.updateWerehouseRegionStatus(lzCustomerAndWarehouse);
			}
		}
		return affact;
	}

	/**
	 * 根据客户id查询关联表中的库区
	 * @param customer_id 客户id
	 * @throws Exception
	 */
	@Override
	public List<WarehouseRegion> queryRegionByCustomerId(String customer_id) {
		return materielMapper.queryRegionByCustomerId(customer_id);
	}

	@Override
	public List<Materiel> querymaterielcustomerbyid(Map<String, Object> map) {
		return materielMapper.querymaterielcustomerbyid(map);
	}

	@Override
	public int countmaterielcustomerbyid(Map<String, Object> map) {
		return materielMapper.countmaterielcustomerbyid(map);
	}

	@Override
	public void updateCustomerWarehouseStatus(Materiel materiel) {
		materielMapper.updateCustomerWarehouseStatus(materiel);
	}

	@Override
	public List<Materiel> queryMeterailByOutid(String outnum) {
		return materielMapper.queryMeterailByOutid(outnum);
	}

	@Override
	public List<Materiel> queryAllMC(Map<String, Object> map) {
		return materielMapper.queryAllMC(map);
	}

	@Override
	public int countAllMC(Map<String, Object> map) {
		return materielMapper.countAllMC(map);
	}

	@Override
	public Long queryCustnameByname(String name) {
		return materielMapper.queryCustnameByname(name);
	}

	@Override
	public Long queryRegionByname(String name) {
		return materielMapper.queryRegionByname(name);
	}

	@Override
	public Long queryContainerByName(String name) {
		return materielMapper.queryContainerByName(name);
	}

	@Override
	public List<Materiel> queryAllMateriel() {
		return materielMapper.queryAllMateriel();
	}

	/**
	 * 查询所有库区
	 * @param customer_id
	 * @return
	 */
	@Override
	public List<WarehouseRegion> queryRegionByCustomerId1(String customer_id) {
		return materielMapper.queryRegionByCustomerId1(customer_id);
	}

	/**
     * 根据库区id查询库区类型
     * @param region_id 库区id
     * @param response
     * @throws Exception
     */
	@Override
	public WarehouseRegion queryRegionTypeById(String region_id) {
		return materielMapper.queryRegionTypeById(region_id);
	}

	/**
	 * 根据物料id查询客户
	 * @param mid
	 * @return
	 */
	@Override
	public Customer queryCustomerByCustomerId(Long customerId) {
		return materielMapper.queryCustomerByCustomerId(customerId);
	}
}
