package com.sdry.service.impl.lz;

import com.sdry.mapper.lz.RegionMapper;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Region;
import com.sdry.service.lz.RegionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName RegionServiceImpl
 * @Description 地区信息
 * @Author lz
 * @Date 2019年5月27日 11:25:05
 * @Version 1.0
 */
@Service
public class RegionServiceImpl implements RegionService {

    @Resource
    RegionMapper regionMapper;

    @Override
    public Long addRegion(Region region) {
        return regionMapper.addRegion(region);
    }

    @Override
    public Long deleteRegionById(long id) {
        return regionMapper.deleteRegionById(id);
    }

    @Override
    public Long updateRegion(Region region) {
        return regionMapper.updateRegion(region);
    }

    @Override
    public List<Region> queryRegionCriteria(LzQueryCriteria criteria) {
        return regionMapper.queryRegionCriteria(criteria);
    }

    @Override
    public int countRegionCriteria(LzQueryCriteria criteria) {
        return regionMapper.countRegionCriteria(criteria);
    }

    @Override
    public List<Region> queryAllRegion() {
        return regionMapper.queryAllRegion();
    }
}
