package com.sdry.service.impl.lz;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.lz.LossSpilloverMapper;
import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.service.lz.LossSpilloverService;

/**
 * @ClassName LossSpilloverService
 * @Description 损溢管理
 * @Author lz
 * @Date 2019年3月25日 11:07:30
 * @Version 1.0
 */
@Service
public class LossSpilloverServiceImpl implements LossSpilloverService{

	@Resource
	LossSpilloverMapper lossSpilloverMapper;

	@Override
	public Long addLossSpillover(LossSpillover lossSpillover) {
		return lossSpilloverMapper.addLossSpillover(lossSpillover);
	}

	@Override
	public Long deleteLossSpilloverById(long id) {
		return lossSpilloverMapper.deleteLossSpilloverById(id);
	}

	@Override
	public Long updateLossSpilloverById(String id, String fieldName,
			String fieldValue) {
		return lossSpilloverMapper.updateLossSpilloverById(id, fieldName, fieldValue);
	}

	@Override
	public List<LossSpillover> queryLossSpilloverCriteria(
			LzQueryCriteria criteria) {
		return lossSpilloverMapper.queryLossSpilloverCriteria(criteria);
	}

	@Override
	public int countLossSpilloverCriteria(LzQueryCriteria criteria) {
		return lossSpilloverMapper.countLossSpilloverCriteria(criteria);
	}

	@Override
	public Long updateLossTabByNum(String num) {
		// TODO Auto-generated method stub
		return lossSpilloverMapper.updateLossTabByNum(num);
	}
}
