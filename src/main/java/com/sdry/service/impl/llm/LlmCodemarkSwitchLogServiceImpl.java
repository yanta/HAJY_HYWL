package	com.sdry.service.impl.llm;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.llm.LlmCodemarkSwitchLog;
import com.sdry.service.llm.LlmCodemarkSwitchLogService;
import com.sdry.mapper.llm.LlmCodemarkSwitchLogMapper;
/**
 *
 *@ClassName: LlmCodemarkSwitchLogService
 *@Description: 条码类型转换记录
 *@Author llm
 *@Date 2019-08-08 09:23:39
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class LlmCodemarkSwitchLogServiceImpl implements LlmCodemarkSwitchLogService {
	@Resource LlmCodemarkSwitchLogMapper llmCodemarkSwitchLogMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public LlmCodemarkSwitchLog queryById(Long id) {
		return llmCodemarkSwitchLogMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchLog> queryAllByMution(LlmCodemarkSwitchLog param) {
		return llmCodemarkSwitchLogMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchLog> findPageByMution(LlmCodemarkSwitchLog param) {
		return llmCodemarkSwitchLogMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(LlmCodemarkSwitchLog param) {
		return llmCodemarkSwitchLogMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(LlmCodemarkSwitchLog param) {
		return llmCodemarkSwitchLogMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(LlmCodemarkSwitchLog param) {
		return llmCodemarkSwitchLogMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return llmCodemarkSwitchLogMapper.delete(ids);
	}
}
