package com.sdry.service.impl.llm;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.sdry.mapper.llm.CancellingStockMapper;
import com.sdry.model.jyy.Receive;
import com.sdry.model.llm.BarCode;
import com.sdry.model.llm.LlmCancellingStocks;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.service.llm.CancellingStockService;
import com.sdry.utils.PushExample;

/**
 * 退库单service实现类
 * @Title:CanacellingStockServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年5月14日
 * @version 2.0
 */
@Service
public class CanacellingStockServiceImp implements CancellingStockService{

	@Resource
	private CancellingStockMapper cancellingStockMapper;
	
	/**       
	 * 根据条件获取退库单总行数
	 * @param cancellingStock
	 * @return    
	 */
	@Override
	public int getCountConcellingStock(LlmCancellingStocks cancellingStock) {
		return cancellingStockMapper.getCountConcellingStock(cancellingStock);
	}

	/**      
	 * 根据条件分页查询退库单 
	 * @param map
	 * @return    
	 */
	@Override
	public List<LlmCancellingStocks> listPageConcellingStock(Map<String, Object> map) {
		List<LlmCancellingStocks> list = cancellingStockMapper.listPageConcellingStock(map);
		if(list.size() > 0){
			for(LlmCancellingStocks cs : list){
				if(cs.getWarehouseId() != null && !"".equals(cs.getWarehouseId())){
					Warehouse warehouse = cancellingStockMapper.getWarehouseById(cs.getWarehouseId());
					if(warehouse != null){
						cs.setWarehouse_address(warehouse.getWarehouse_address());
						cs.setUserName(warehouse.getUserName());
						cs.setPhone(warehouse.getPhone());
						cs.setWarehouse_name(warehouse.getWarehouse_name());
					}
				}
				if(cs.getClientId() != null && !"".equals(cs.getClientId())){
					Customer customer = cancellingStockMapper.getClientById(cs.getClientId());
					if(customer != null){
						cs.setContacts_name(customer.getContacts_name());
						cs.setContacts_tel(customer.getContacts_tel());
						cs.setCustomer_adress(customer.getCustomer_adress());
						cs.setCustomer_name(customer.getCustomer_name());
					}
				}
				if(cs.getStatus() == 1){
					cs.setCancellingStatus("已退库");
				}else{
					cs.setCancellingStatus("未退库");
				}
			}
		}
		return list;
	}

	/**       
	 * 新增退库单
	 * @param cancellingStock
	 * @return    
	 */
	@Override
	public Integer saveConcellingStock(LlmCancellingStocks cancellingStock) {
		return cancellingStockMapper.saveConcellingStock(cancellingStock);
	}

	/**   
	 * 修改退库单    
	 * @param cancellingStock
	 * @return    
	 */
	@Override
	public Integer updateConcellingStock(LlmCancellingStocks cancellingStock) {
		return cancellingStockMapper.updateConcellingStock(cancellingStock);
	}

	/**  
	 * 修改退库单2     
	 * @param map
	 * @return    
	 */
	@Override
	public Integer updateConcellingStock2(Map<String, Object> map) {
		return cancellingStockMapper.updateConcellingStock2(map);
	}
	
	/**    
	 * 批量删除退库单   
	 * @param ids
	 * @return    
	 */
	@Transactional(rollbackFor={Exception.class})
	public Integer deleteConcellingStock(String ids) {
		int isSuccess = 0;
		String[] split = ids.split(",");
		try {
			for (String id : split) {
				isSuccess = cancellingStockMapper.deleteConcellingStock(Long.parseLong(id));
			}
		}catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return isSuccess;
	}

	/**   
	 * 发送退库单    
	 * @param roder
	 * @return    
	 */
	@Override
	public Integer sendConcellingStock(String roder) {
		int isSuccess = 1;
		//发送消息
		PushExample.testSendPush("您有一个新的退库单，单号是："+roder, "退库通知", "1", "2");
		return isSuccess;
	}

	/**     
	 * 获取所有供应商  
	 * @return    
	 */
	@Override
	public List<Customer> getAllCustomer() {
		return cancellingStockMapper.getAllCustomer();
	}

	/** 
	 * 获取所有仓库      
	 * @return    
	 */
	@Override
	public List<Warehouse> getAllWarehouse() {
		return cancellingStockMapper.getAllWarehouse();
	}

	/**    
	 * 获取所有退库单   
	 * @return    
	 */
	@Override
	public List<LlmCancellingStocks> getAllCancellingStock() {
		return cancellingStockMapper.getAllCancellingStock();
	}

	/**  
	 * 根据物料ID查询物料表     
	 * @param id
	 * @return    
	 */
	@Override
	public Materiel getMaterielByMid(Long id) {
		return cancellingStockMapper.getMaterielByMid(id);
	}

	/**   
	 * 根据客户ID获取供应商联系人信息    
	 * @param id
	 * @return    
	 */
	@Override
	public Customer getClientById(Long id) {
		return cancellingStockMapper.getClientById(id);
	}

	/**     
	 * 根据仓库ID获取管理员信息  
	 * @param id
	 * @return    
	 */
	@Override
	public Warehouse getWarehouseById(Long id) {
		return cancellingStockMapper.getWarehouseById(id);
	}

	/**      
	 * 获取所有不良品库
	 * @return    
	 */
	@Override
	public List<Warehouse> getAllBadnessWarehouse() {
		return cancellingStockMapper.getAllBadnessWarehouse();
	}

	/**       
	 * 根据仓库ID获取物料信息
	 * @param id
	 * @return    
	 */
	@Override
	public List<Materiel> getMaterielByWarehouseId(Long id) {
		return cancellingStockMapper.getMaterielByWarehouseId(id);
	}

	/**     
	 * 根据退库单编号查询退库单  
	 * @param cancellingNumber
	 * @return    
	 */
	@Override
	public LlmCancellingStocks getCancellingStockByNumber(String cancellingNumber) {
		return cancellingStockMapper.getCancellingStockByNumber(cancellingNumber);
	}

	/**      
	 * 根据仓库ID获取供应商信息 
	 * @param id
	 * @return    
	 */
	@Override
	public List<Customer> getCustomerByWarehouseId(Long id) {
		return cancellingStockMapper.getCustomerByWarehouseId(id);
	}

	/**   
	 * 根据供应商和SAP查询所有物料      
	 * @param map
	 * @return    
	 */
	@Override
	public List<Materiel> appGetMaterielByCustomerAndSAP(Map<String, Object> map) {
		return cancellingStockMapper.appGetMaterielByCustomerAndSAP(map);
	}

	/**       
	 * @param receiveNumber
	 * @return    
	 */
	@Override
	public Receive getReceiveByNumber(String receiveNumber) {
		return cancellingStockMapper.getReceiveByNumber(receiveNumber);
	}

	/**       
	 * @param number
	 * @return    
	 */
	@Override
	public List<CodeMark> getCodeByMaterielNum(String number) {
		return cancellingStockMapper.getCodeByMaterielNum(number);
	}

	/**       
	 * @param code
	 * @return    
	 */
	@Override
	public BarCode getLocationByCode(CodeMark code) {
		return cancellingStockMapper.getLocationByCode(code);
	}

	@Override
	public int caceltk(Map map) {
		// TODO Auto-generated method stub
		return cancellingStockMapper.caceltk(map);
	}

	@Override
	public int restortk(Map map) {
		// TODO Auto-generated method stub
		return cancellingStockMapper.restortk(map);
	}

}
