package com.sdry.service.impl.llm;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.llm.CancellingStockMapper;
import com.sdry.mapper.llm.InquiryBusinessMapper;
import com.sdry.model.llm.LlmInventoryInfo;
import com.sdry.model.llm.LlmRegionStock;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.llm.LocationStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.service.llm.InquiryBusinessService;

/**
 * 业务查询Service实现类
 * @Title:InquiryBusinessServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年4月23日
 * @version 1.0
 */
@Service
public class InquiryBusinessServiceImp implements InquiryBusinessService{

	@Resource
	private InquiryBusinessMapper inquiryBusinessMapper;
	@Resource
	private CancellingStockMapper cancellingStockMapper;
	
	/**       
	 * APP 根据物料名称查库存信息	
	 * @param materielName
	 * @return    
	 */
	@Override
	public List<LlmInventoryInfo> appGetInventoryInfoByMateriel(String materielName) {
		return inquiryBusinessMapper.appGetInventoryInfoByMateriel(materielName);
	}

	/**       
	 * 分页查库存信息	
	 * @param map
	 * @return    
	 */
	@Override
	public List<LlmInventoryInfo> listPageInventoryInfo(Map<String, Object> map) {
		List<LlmInventoryInfo> list = inquiryBusinessMapper.listPageInventoryInfo(map);
		if(list.size() > 0){
			for(LlmInventoryInfo in : list){
				if(in.getWarehouseId() != null && !"".equals(in.getWarehouseId())){
					Warehouse w = cancellingStockMapper.getWarehouseById(in.getWarehouseId());
					if(w != null){
						in.setWarehouse_name(w.getWarehouse_name());
						in.setWarehouse_num(w.getWarehouse_num());
					}
				}
				if(in.getRegionId() != null && !"".equals(in.getRegionId())){
					WarehouseRegion region = inquiryBusinessMapper.getWarehouseRegionById(in.getRegionId());
					if(region != null){
						in.setRegion_name(region.getRegion_name());
						in.setRegion_num(region.getRegion_num());
					}
				}
				if(in.getLocationId() != null && !"".equals(in.getLocationId())){
					WarehouseRegionLocation loca = inquiryBusinessMapper.getWarehouseRegionLocationById(in.getLocationId());
					if(loca != null){
						in.setLocation_name(loca.getLocation_name());
						in.setLocation_num(loca.getLocation_num());
					}
				}
			}
		}
		return list;
	}

	/**   
	 * 根据条件获取库存信息总行数    
	 * @param llmInventoryInfo
	 * @return    
	 */
	@Override
	public Integer getCountInventoryInfo(LlmInventoryInfo llmInventoryInfo) {
		return inquiryBusinessMapper.getCountInventoryInfo(llmInventoryInfo);
	}

	/**       
	 * 查询所有库区
	 * @return    
	 */
	@Override
	public List<WarehouseRegion> getAllWarehouseRegion() {
		return inquiryBusinessMapper.getAllWarehouseRegion();
	}

	/**   
	 * 查询所有库位    
	 * @return    
	 */
	@Override
	public List<WarehouseRegionLocation> getAllWarehouseRegionLocation() {
		return inquiryBusinessMapper.getAllWarehouseRegionLocation();
	}

	/**       
	 * 查询数量统计总行数
	 * @param materielName
	 * @return    
	 */
	@Override
	public int getCountMaterielQuantityStatistics(String materielName) {
		return inquiryBusinessMapper.getCountMaterielQuantityStatistics(materielName);
	}

	
	
	/**    
	 * 根据退库单ID查上架记录总条数   
	 * @param id
	 * @return    
	 */
	@Override
	public int getPutawayByCancellingStockId(Long id) {
		return inquiryBusinessMapper.getPutawayByCancellingStockId(id);
	}


	/**      
	 * 根据退库单ID查收货入库记录总条数    
	 * @param id
	 * @return    
	 */
	@Override
	public int getPutStockByCancellingStockId(Long id) {
		return inquiryBusinessMapper.getPutStockByCancellingStockId(id);
	}


	/**   
	 * 根据退库单ID查验收记录总条数    
	 * @param id
	 * @return    
	 */
	@Override
	public int getCheckStockByCancellingStockId(Long id) {
		return inquiryBusinessMapper.getCheckStockByCancellingStockId(id);
	}


	/**       
	 * 获取仓库库位总行数
	 * @return    
	 */
	@Override
	public int getCountLocationStock() {
		return inquiryBusinessMapper.getCountLocationStock();
	}

	/**    
	 * 获取仓库库位物料信息   
	 * @return    
	 */
	@Override
	public List<LocationStock> getAllLocationStock() {
		List<LocationStock> list = inquiryBusinessMapper.getAllLocationStock();
		for(LocationStock ls : list){
			List<LocationStock> inventoryList = inquiryBusinessMapper.getMaterielByLocationId(ls.getLocationId());
			if(inventoryList.size() > 0){
				Integer quantity = 0;
				for(LocationStock s : inventoryList){
					if(s.getmNum() != null && !"".equals(s.getmNum())){
						quantity += s.getmNum();
					}
					if(s.getMaterielName() != null && !"".equals(s.getMaterielName())){
						ls.setMaterielName(s.getMaterielName());
					}
				}
				ls.setMaterielQuantity(quantity);
				ls.setFloor(inventoryList.get(0).getFloor());
				ls.setUpper(inventoryList.get(0).getUpper());
			}
		}
		
		return list;
	}

	/**   
	 * app端获取所有仓库    
	 * @return    
	 */
	@Override
	public List<LlmWarehouseStock> appGetStock() {
		List<LlmWarehouseStock> list = inquiryBusinessMapper.appGetStock();
		if(list.size() > 0){
			for(LlmWarehouseStock ws : list){
				List<LlmRegionStock> regionList = inquiryBusinessMapper.appGetRegionStockByWarehouseId(ws.getId());
				if(regionList.size() > 0){
					for(LlmRegionStock rs : regionList){
						List<WarehouseRegionLocation> locationList = inquiryBusinessMapper.appGetLocationStockByRegionId(rs.getId());
						rs.setLocationList(locationList);
					}
				}
				ws.setRegionList(regionList);
			}
		}
		return list;
	}
}
