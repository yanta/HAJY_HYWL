package com.sdry.service.impl.llm;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.llm.DevaningWorkMapper;
import com.sdry.model.llm.DevaningWork;
import com.sdry.service.llm.DevaningWorkService;

/**
 * 拆箱工作量
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年7月31日
 * @version 1.0
 */
@Service
public class DevaningWorkServiceImp implements DevaningWorkService{

	@Resource
	DevaningWorkMapper devaningWorkMapper;
	
	/**       
	 * @param work
	 * @return    
	 */
	@Override
	public Long insert(DevaningWork work) {
		return devaningWorkMapper.insert(work);
	}

	/**       
	 * @param num
	 * @return    
	 */
	@Override
	public List<DevaningWork> queryWorkByNum(String num) {
		return devaningWorkMapper.queryWorkByNum(num);
	}

}
