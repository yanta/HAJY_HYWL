package	com.sdry.service.impl.llm;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.llm.DeliveryNode;
import com.sdry.model.llm.DeliveryOutStock;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.model.lz.OutStockOrderDetail;
import com.sdry.service.llm.DeliveryNodeService;
import com.sdry.utils.Page;
import com.sdry.mapper.llm.DeliveryNodeMapper;
/**
 *
 *@ClassName: DeliveryNodeService
 *@Description: 发货单
 *@Author llm
 *@Date 2019-07-17 14:14:41
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class DeliveryNodeServiceImpl implements DeliveryNodeService {
	@Resource DeliveryNodeMapper deliveryNodeMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public DeliveryNode queryById(Long id) {
		return deliveryNodeMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryNode> queryAllByMution(DeliveryNode param) {
		return deliveryNodeMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryNode> findPageByMution(DeliveryNode param) {
		return deliveryNodeMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(DeliveryNode param) {
		return deliveryNodeMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(DeliveryNode param) {
		return deliveryNodeMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(DeliveryNode param) {
		return deliveryNodeMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return deliveryNodeMapper.delete(ids);
	}
	/**  
	 * 根据送货单ID分页查询出库单     
	 * @param parseLong
	 * @return    
	 */
	@Override
	public List<OutStockOrder> queryStockOutByDeliveryId(DeliveryOutStock param) {
		return deliveryNodeMapper.queryStockOutByDeliveryId(param);
	}
	/**   
	 * 根据送货单ID查询出库单总数    
	 * @param parseLong
	 * @return    
	 */
	@Override
	public Integer selectCountStockOutByDeliveryId(DeliveryOutStock param) {
		return deliveryNodeMapper.selectCountStockOutByDeliveryId(param);
	}
	/**       
	 * @return    
	 */
	@Override
	public List<OutStockOrder> queryAllStockOut(Page page) {
		return deliveryNodeMapper.queryAllStockOut(page);
	}
	/**       
	 * @return    
	 */
	@Override
	public Integer selectCountAllStockOut() {
		return deliveryNodeMapper.selectCountAllStockOut();
	}
	/**   
	 * 根据登录人查询所有送货单    
	 * @param parseLong
	 * @return    
	 */
	@Override
	public List<DeliveryNode> queryDeliveryByUid(long parseLong) {
		return deliveryNodeMapper.queryDeliveryByUid(parseLong);
	}
	/**      
	 * 根据送货单ID查询所有出库单  
	 * @param parseLong
	 * @return    
	 */
	@Override
	public List<OutStockOrder> queryStockOutByDeliveryId2(long parseLong) {
		return deliveryNodeMapper.queryStockOutByDeliveryId2(parseLong);
	}
	/**     
	 * 根据出库单ID查询所有出库单详情  
	 * @param parseLong
	 * @return    
	 */
	@Override
	public List<OutStockOrderDetail> queryOutStockDetailByOrderId(long parseLong) {
		return deliveryNodeMapper.queryOutStockDetailByOrderId(parseLong);
	}
	/**     
	 * 修改出库单详情    
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @param string6    
	 */
	@Override
	public int updateById2(String string, String string2, String string3, String string4, String string5,
			String string6) {
		return deliveryNodeMapper.updateById2(string, string2, string3, string4, string5, string6);
	}
	/**       
	 * @param userId
	 * @return    
	 */
	@Override
	public List<OutStockOrder> queryStockOutByUserId(long userId) {
		return deliveryNodeMapper.queryStockOutByUserId(userId);
	}
	/**       
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param time
	 * @param string5
	 * @param reason
	 * @param string6
	 * @return    
	 */
	@Override
	public int updateById3(String string, String string2, String string3, String string4, String string5, String string6,
			String string7, String string8) {
		return deliveryNodeMapper.updateById3(string, string2, string3, string4, string5, string6, string7, string8);
	}
	/**       
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @return    
	 */
	@Override
	public int updateById4(String string, String string2, String string3, String string4) {
		return deliveryNodeMapper.updateById4(string, string2, string3, string4);
	}
	/**       
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4    
	 */
	@Override
	public int updateInventoryByMid(String string, String string2, String string3, String string4) {
		return deliveryNodeMapper.updateInventoryByMid(string, string2, string3, string4);
	}
}
