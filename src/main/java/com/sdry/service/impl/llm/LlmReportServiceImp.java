package com.sdry.service.impl.llm;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.sdry.mapper.llm.LlmReportMapper;
import com.sdry.model.tdd.ReportUtil;
import com.sdry.service.llm.LlmReportService;

/**
 * 报表
 * @Title:ReportServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年5月27日
 * @version 1.0
 */

@Service
public class LlmReportServiceImp implements LlmReportService{

	@Resource
	private LlmReportMapper llmReportMapper;

	/**       
	 * 退库报表
	 * @param map
	 * @return    
	 */
	@Override
	public List<ReportUtil> getMonthCancellingStockReportByCustomer(Map<String, Object> map) {
		return llmReportMapper.getMonthCancellingStockReportByCustomer(map);
	}

	/**    
	 * 出库报表   
	 * @param map
	 * @return    
	 */
	@Override
	public List<ReportUtil> getOutStockReportByCustomer(Map<String, Object> map) {
		return llmReportMapper.getOutStockReportByCustomer(map);
	}

}
