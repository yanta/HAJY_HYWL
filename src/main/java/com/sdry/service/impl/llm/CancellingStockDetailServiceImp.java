package com.sdry.service.impl.llm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.sdry.mapper.llm.CancellingStockDetailMapper;
import com.sdry.mapper.llm.CancellingStockMapper;
import com.sdry.mapper.zc.ZcBindAndUnbindMapper;
import com.sdry.model.llm.LlmCodemarkReturn;
import com.sdry.model.llm.DevaningLog;
import com.sdry.model.llm.LlmCancellingStocksDetail;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.service.llm.CancellingStockDetailService;

/**
 * 退库单详情
 * @Title:CancellingStockDetailServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年5月15日
 * @version 1.0
 */

@Service
public class CancellingStockDetailServiceImp implements CancellingStockDetailService{

	@Resource
	private CancellingStockDetailMapper cancellingStockDetailMapper;
	@Resource
	private CancellingStockMapper cancellingStockMapper;
	@Resource
	private ZcBindAndUnbindMapper zcBindAndUnbindMapper;

	/**    
	 * 获取退库单详情总行数   
	 * @param cancellingStockDetail
	 * @return    
	 */
	@Override
	public Integer getCountCancellingStockDetail(LlmCancellingStocksDetail cancellingStockDetail) {
		return cancellingStockDetailMapper.getCountCancellingStockDetail(cancellingStockDetail);
	}

	/**   
	 * 分页查询退库单详情    
	 * @param map
	 * @return    
	 */
	@Override
	public List<LlmCancellingStocksDetail> listPageCancellingStockDetail(Map<String, Object> map) {
		List<LlmCancellingStocksDetail> list = cancellingStockDetailMapper.listPageCancellingStockDetail(map);
		if(list.size() > 0){
			for(LlmCancellingStocksDetail detail : list){
				if(detail.getMaterialId() != null && !"".equals(detail.getMaterialId())){
					Materiel materiel = cancellingStockMapper.getMaterielByMid(detail.getMaterialId());
					if(materiel != null){
						detail.setMateriel_name(materiel.getMateriel_name());
					}
					//通过产品码查询物料所在最早库位
					String location_name = "";
					WarehouseRegionLocation warehouseRegionLocation = zcBindAndUnbindMapper.selectTopLocationDown(materiel.getMateriel_num());
					if(warehouseRegionLocation != null){
						location_name = warehouseRegionLocation.getLocation_name();
					}
					detail.setStorageLocation(location_name);
				}
			}
		}
		return list;
	}

	/**   
	 * 新增退库单详情    
	 * @param cancellingNumber 退库单号
	 * @param ids 物料ID拼接字符串  
	 * @return  
	 */
	@Override
	public Integer saveCancellingStockDetail(String ids, String cancellingNumber, String source, String warehouseId, String customerId) {
		int isSuccess = 0;
		String[] split = ids.split(",");
		LlmCancellingStocksDetail cancellingStockDetail = new LlmCancellingStocksDetail();
		cancellingStockDetail.setCancellingNumber(cancellingNumber);
		for(String id : split){
			cancellingStockDetail.setMaterialId(Long.parseLong(id));
			cancellingStockDetail.setStatus(0);
			//根据物料ID和退库单号查详情
			/*List<LlmCancellingStocksDetail> detail = cancellingStockDetailMapper.getCancellingStockDetailByMidAndNumber(cancellingStockDetail);
			if(detail.size() > 0){
				Map<String, Object> map = new HashMap<>();
				map.put("id", Long.parseLong(id));
				map.put("warehouseId", Long.parseLong(warehouseId));
				map.put("customerId", Long.parseLong(customerId));
				//根据物料ID查库存
				if(source.equals("1")){
					ZcInventoryInfoEntity info  = cancellingStockDetailMapper.getGoodQuantityByMid(map);
					if(info != null){
						//默认退库数量是物料的库存数量
						cancellingStockDetail.setMaterialQuantity(info.getmNum());
					}
				} else {
					ZcRejectsWarehouseEntity rejects = cancellingStockDetailMapper.getBadQuantityByMid(map);
					if(rejects != null){
						//默认退库数量是物料的库存数量
						cancellingStockDetail.setMaterialQuantity(rejects.getTotality());
					}
				}
				isSuccess = cancellingStockDetailMapper.saveCancellingStockDetail(cancellingStockDetail);
			}*/
			//根据物料ID查库存
			Integer good = cancellingStockDetailMapper.getGoodQuantityInInventoryInfo(Long.parseLong(id));
			Integer bad = cancellingStockDetailMapper.getBadQuantityInRejectsWarehouse(Long.parseLong(id));
			if(good != null){
				cancellingStockDetail.setGoodQuantity(good);
			}else{
				cancellingStockDetail.setGoodQuantity(0);
			}
			if(bad != null){
				cancellingStockDetail.setBadQuantity(bad);
			}else{
				cancellingStockDetail.setBadQuantity(0);
			}
			cancellingStockDetail.setSumQuantity(cancellingStockDetail.getGoodQuantity() + cancellingStockDetail.getBadQuantity());
			cancellingStockDetail.setMaterialQuantity(0);
			isSuccess = cancellingStockDetailMapper.saveCancellingStockDetail(cancellingStockDetail);
		}
		return isSuccess;
	}

	/**      
	 * 修改退库单详情 
	 * @param cancellingStockDetail
	 * @return    
	 */
	@Override
	public Integer updateCancellingStockDetail(LlmCancellingStocksDetail cancellingStockDetail) {
		return cancellingStockDetailMapper.updateCancellingStockDetail(cancellingStockDetail);
	}

	/**   
	 * 删除退库单详情    
	 * @param ids
	 * @return    
	 */
	@Transactional(rollbackFor={Exception.class})
	public Integer deleteCancellingStockDetail(String ids) {
		int isSuccess = 0;
		String[] split = ids.split(",");
		try {
			for (String id : split) {
				isSuccess = cancellingStockDetailMapper.deleteCancellingStockDetail(Long.parseLong(id));
			}
		}catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return isSuccess;
	}

	/**      
	 * 根据退库单号查询详情 
	 * @param cancellingNumber
	 * @return    
	 */
	@Override
	public List<LlmCancellingStocksDetail> getCancellingStockDetailByNumber(String cancellingNumber) {
		List<LlmCancellingStocksDetail> list = cancellingStockDetailMapper.getCancellingStockDetailByNumber(cancellingNumber);
		if(list.size() > 0){
			for(LlmCancellingStocksDetail detail : list){
				if(detail.getMaterialId() != null && !"".equals(detail.getMaterialId())){
					Materiel materiel = cancellingStockMapper.getMaterielByMid(detail.getMaterialId());
					if(materiel != null){
						detail.setMateriel_name(materiel.getMateriel_name());
						detail.setMateriel(materiel);
					}
				}
			}
		}
		return list;
	}

	/**    
	 * 根据ID查询退库单详情   
	 * @param id
	 * @return    
	 */
	@Override
	public LlmCancellingStocksDetail getCancellingStockDetailByDetailId(Long id) {
		return cancellingStockDetailMapper.getCancellingStockDetailByDetailId(id);
	}

	/**   
	 * 根据物料ID获取库存数量     
	 * @return
	 */
	@Override
	public List<ZcInventoryInfoEntity> getQuantityByMid(Map<String, Object> map) {
		return cancellingStockDetailMapper.getQuantityByMid(map);
	}

	/**    
	 * 查物料总数   
	 * @param map
	 * @return    
	 */
	@Override
	public int getCountMaterielByCustomerIdAndWarehouseId(Map<String, Object> map) {
		Integer source = (Integer)map.get("source");
		Integer i = 0;
		if(source == 1){
			//查良品
			i = cancellingStockDetailMapper.getCountMaterielByGood(map);
		}else{
			//查不良
			i = cancellingStockDetailMapper.getCountMaterielByBad(map);
		}
		return i;
	}

	/**  
	 * 分页查物料     
	 * @param map
	 * @return    
	 */
	@Override
	public List<Materiel> listPageMaterielByCustomerIdAndWarehouseId(Map<String, Object> map) {
		Integer source = (Integer)map.get("source");
		List<Materiel> list = new ArrayList<>();
		if(source == 1){
			//查良品
			list = cancellingStockDetailMapper.listPageMaterielByGood(map);
		}else{
			//查不良
			list = cancellingStockDetailMapper.listPageMaterielByBad(map);
		}
		return list;
	}

	/**      
	 * 修改退库物料数量 
	 * @return
	 */
	@Override
	public int updateCancellingStockDetail2(String id, String actualQuantity, String single_num, String big_num, String small_num) {
		return cancellingStockDetailMapper.updateCancellingStockDetail2(id, actualQuantity, single_num, big_num, small_num);
	}

	/**      
	 * 修改库存数量 
	 * @param map    
	 */
	@Override
	public Integer updateInventory(Map<String, Object> map) {
		return cancellingStockDetailMapper.updateInventory(map);
	}

	/**    
	 * 根据仓库ID查询物料总行数   
	 * @param map
	 * @return    
	 */
	@Override
	public int getCountMaterielByWarehouseId(Map<String, Object> map) {
		return cancellingStockDetailMapper.getCountMaterielByWarehouseId(map);
	}

	/**   
	 * 根据仓库ID分页查询物料    
	 * @param map
	 * @return    
	 */
	@Override
	public List<Materiel> listPageMaterielByWarehouseId(Map<String, Object> map) {
		return cancellingStockDetailMapper.listPageMaterielByWarehouseId(map);
	}

	/**   
	 * 修改拆箱总数    
	 * @param map
	 * @return    
	 */
	@Override
	public Integer updateUnpacking(Map<String, Object> map) {
		return cancellingStockDetailMapper.updateUnpacking(map);
	}

	/**   
	 * 查询不良品数量    
	 * @param map
	 * @return    
	 */
	@Override
	public ZcRejectsWarehouseEntity getBadQuantityByMid(Map<String, Object> map) {
		return cancellingStockDetailMapper.getBadQuantityByMid(map);
	}

	/**    
	 * 查询良品数量   
	 * @param map
	 * @return    
	 */
	@Override
	public ZcInventoryInfoEntity getGoodQuantityByMid(Map<String, Object> map) {
		return cancellingStockDetailMapper.getGoodQuantityByMid(map);
	}

	/**      
	 * 删除不良品库存数据 
	 * @param id    
	 */
	@Override
	public Integer deleteRejectsWarehouse(Long id) {
		return cancellingStockDetailMapper.deleteRejectsWarehouse(id);
	}

	/**   
	 * 修改不良品库存数据    
	 * @param map    
	 */
	@Override
	public Integer updateRejectsWarehouse(Map<String, Object> map) {
		return cancellingStockDetailMapper.updateRejectsWarehouse(map);
	}

	/**
	 * 根据物料ID查库存
	 */
	@Override
	public List<ZcInventoryInfoEntity> getInventoryByMid(Long mid) {
		List<ZcInventoryInfoEntity> list = cancellingStockDetailMapper.getInventoryByMid(mid);
		return list;
	}

	@Override
	public Integer deleteInventoryById(Long id) {
		return cancellingStockDetailMapper.deleteInventoryById(id);
	}

	@Override
	public List<ZcRejectsWarehouseEntity> queryRejectsByMaterielId(Long materielId) {
		return cancellingStockDetailMapper.queryRejectsByMaterielId(materielId);
	}

	/**  
	 * 根据ID查询供应商     
	 * @param id
	 * @return    
	 */
	@Override
	public Customer getCustomerById(Long id) {
		return cancellingStockDetailMapper.getCustomerById(id);
	}

	/**
	 * 修改退库单详细的状态
	 * @param id
	 * @return
	 */
	@Override
	public int updateCancellingStockDetail3(Long id) {
		return cancellingStockDetailMapper.updateCancellingStockDetail3(id);
	}

	@Override
	public int updateCancellingStockDetail4(Map<String, Object> map) {
		return cancellingStockDetailMapper.updateCancellingStockDetail4(map);
	}

	/**       
	 * @param materiel_id
	 * @param act_num
	 * @param single_num
	 * @param big_num
	 * @param small_num
	 * @return    
	 */
	@Override
	public int updateCancellingStockDetailActNum(String materiel_id, String act_num, String single_num, String big_num,
			String small_num) {
		return cancellingStockDetailMapper.updateCancellingStockDetailActNum(materiel_id, act_num, single_num, big_num, small_num);
	}

	/**       
	 * @param codemark
	 * @return    
	 */
	@Override
	public Integer insert(LlmCodemarkReturn codemark) {
		return cancellingStockDetailMapper.insert(codemark);
	}

	/**       
	 * @param detailId
	 * @return    
	 */
	@Override
	public Integer deleteByDetailId(Long detailId) {
		return cancellingStockDetailMapper.deleteByDetailId(detailId);
	}

	/**       
	 * @param detailId
	 * @return    
	 */
	@Override
	public List<LlmCodemarkReturn> queryCodemarkById(Long detailId) {
		return cancellingStockDetailMapper.queryCodemarkById(detailId);
	}

	/**       
	 * @param code
	 * @return    
	 */
	@Override
	public CodeMark queryCodeByCode(String code) {
		return cancellingStockDetailMapper.queryCodeByCode(code);
	}

	/**       
	 * @param mid
	 * @param pici
	 * @param num
	 * @return    
	 */
	@Override
	public int reductionInventory(String mid, String pici, String num) {
		return cancellingStockDetailMapper.reductionInventory(mid, pici, num);
	}

	/**       
	 * @param materiel
	 * @return    
	 */
	@Override
	public int getCountCodemark(Materiel materiel) {
		return cancellingStockDetailMapper.getCountCodemark(materiel);
	}

	/**       
	 * @param map
	 * @return    
	 */
	@Override
	public List<Materiel> listPageCodemark(Map<String, Object> map) {
		return cancellingStockDetailMapper.listPageCodemark(map);
	}

	/**       
	 * @param detailId
	 * @return    
	 */
	@Override
	public List<CodeMark> queryCodemarkById2(Long detailId) {
		return cancellingStockDetailMapper.queryCodemarkById2(detailId);
	}

	/**       
	 * @param map
	 * @return    
	 */
	@Override
	public List<Materiel> listPageDevaning(Map<String, Object> map) {
		return cancellingStockDetailMapper.listPageDevaning(map);
	}

	/**       
	 * @param devaningLog
	 * @return    
	 */
	@Override
	public int insertDevaning(DevaningLog devaningLog) {
		return cancellingStockDetailMapper.insertDevaning(devaningLog);
	}

	/**       
	 * @param map
	 * @return    
	 */
	@Override
	public int getCountDevaning(Map<String, Object> map) {
		return cancellingStockDetailMapper.getCountDevaning(map);
	}

	/**       
	 * @param number
	 * @return    
	 */
	@Override
	public List<DevaningLog> queryCodemarkByNum1(String number) {
		return cancellingStockDetailMapper.queryCodemarkByNum1(number);
	}

	/**       
	 * @param number
	 * @return    
	 */
	@Override
	public List<DevaningLog> queryCodemarkByNum2(String number) {
		return cancellingStockDetailMapper.queryCodemarkByNum2(number);
	}

	/**       
	 * @param number
	 * @return    
	 */
	@Override
	public int getCountByNum1(String number) {
		return cancellingStockDetailMapper.getCountByNum1(number);
	}

	/**       
	 * @param number
	 * @return    
	 */
	@Override
	public int getCountByNum2(String number) {
		return cancellingStockDetailMapper.getCountByNum2(number);
	}

	/**
	 * 根据物料查询精简出库的库存(精简出库)
	 * @param materielId
	 * @return
	 */
	@Override
	public List<ZcInventoryInfoEntity> getInventoryOnlyCountByMid(
			Long materielId) {
		return cancellingStockDetailMapper.getInventoryOnlyCountByMid(materielId);
	}

	/**
	 * 如果这条数据为0删掉该数据(精简出库)
	 * @param id
	 */
	@Override
	public void deleteInventoryOnlyCountById(Long id) {
		cancellingStockDetailMapper.deleteInventoryOnlyCountById(id);
	}

	/**
	 * 修改该库存数量(精简出库)
	 * @param map
	 */
	@Override
	public void updateInventoryOnlyCount(Map<String, Object> map) {
		cancellingStockDetailMapper.updateInventoryOnlyCount(map);
	}

}
