package com.sdry.service.impl.llm;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import com.sdry.mapper.llm.CancellingStockReasonMapper;
import com.sdry.model.llm.LlmCancellingStockReason;
import com.sdry.service.llm.CancellingStockReasonService;

/**
 * 退库原因
 * @Title:CancellingStockReasonServiceImp
 * @Package com.sdry.service.impl.llm
 * @author llm
 * @date 2019年5月15日
 * @version 1.0
 */

@Service
public class CancellingStockReasonServiceImp implements CancellingStockReasonService{

	@Resource
	private CancellingStockReasonMapper cancellingStockReasonMapper;
	
	/**   
	 * 获取退库原因总行数    
	 * @param cancellingStockReason
	 * @return    
	 */
	@Override
	public Integer getCountCancellingStockReason(LlmCancellingStockReason cancellingStockReason) {
		return cancellingStockReasonMapper.getCountCancellingStockReason(cancellingStockReason);
	}

	/**    
	 * 分页查询退库原因   
	 * @param map
	 * @return    
	 */
	@Override
	public List<LlmCancellingStockReason> listPageCancellingStockReason(Map<String, Object> map) {
		return cancellingStockReasonMapper.listPageCancellingStockReason(map);
	}

	/**      
	 * 新增退库原因 
	 * @param cancellingStockReason
	 * @return    
	 */
	@Override
	public Integer saveCancellingStockReason(LlmCancellingStockReason cancellingStockReason) {
		return cancellingStockReasonMapper.saveCancellingStockReason(cancellingStockReason);
	}

	/**      
	 * 修改退库原因 
	 * @param cancellingStockReason
	 * @return    
	 */
	@Override
	public Integer updateCancellingStockReason(LlmCancellingStockReason cancellingStockReason) {
		return cancellingStockReasonMapper.updateCancellingStockReason(cancellingStockReason);
	}

	/**    
	 * 删除退库原因   
	 * @param ids
	 * @return    
	 */
	@Transactional(rollbackFor={Exception.class})
	public Integer deleteCancellingStockReason(String ids) {
		int isSuccess = 0;
		String[] split = ids.split(",");
		try {
			for (String id : split) {
				isSuccess = cancellingStockReasonMapper.deleteCancellingStockReason(Long.parseLong(id));
			}
		}catch (Exception e) {
			e.printStackTrace();
			TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
		}
		return isSuccess;
	}

	/**   
	 * 获取所有退库原因    
	 * @return    
	 */
	@Override
	public List<LlmCancellingStockReason> getAllCancellingStockReason() {
		return cancellingStockReasonMapper.getAllCancellingStockReason();
	}

}
