package	com.sdry.service.impl.llm;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.llm.LlmCodemarkSwitchDetail;
import com.sdry.service.llm.LlmCodemarkSwitchDetailService;
import com.sdry.mapper.llm.LlmCodemarkSwitchDetailMapper;
/**
 *
 *@ClassName: LlmCodemarkSwitchDetailService
 *@Description: 条码类型转换详情
 *@Author llm
 *@Date 2019-08-07 18:53:08
 *@version 1.0
*/
@Service
@Transactional(rollbackFor = Exception.class)
public class LlmCodemarkSwitchDetailServiceImpl implements LlmCodemarkSwitchDetailService {
	@Resource LlmCodemarkSwitchDetailMapper llmCodemarkSwitchDetailMapper;
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public LlmCodemarkSwitchDetail queryById(Long id) {
		return llmCodemarkSwitchDetailMapper.queryById(id);
	}
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchDetail> queryAllByMution(LlmCodemarkSwitchDetail param) {
		return llmCodemarkSwitchDetailMapper.queryAllByMution(param);
	}
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitchDetail> findPageByMution(LlmCodemarkSwitchDetail param) {
		return llmCodemarkSwitchDetailMapper.findPageByMution(param);
	}
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(LlmCodemarkSwitchDetail param) {
		return llmCodemarkSwitchDetailMapper.selectCountByMution(param);
	}
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(LlmCodemarkSwitchDetail param) {
		return llmCodemarkSwitchDetailMapper.insert(param);
	}
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(LlmCodemarkSwitchDetail param) {
		return llmCodemarkSwitchDetailMapper.update(param);
	}
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids) {
		return llmCodemarkSwitchDetailMapper.delete(ids);
	}
}
