package com.sdry.service.zc;

import java.util.List;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMaterileMoveInfoEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;

/**
 * 物料与托盘的绑定与解绑
 * @ClassName:    ZcBindAndUnbindService   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年6月3日 上午10:34:55   
 * @Version:      v1.0
 */
public interface ZcBindAndUnbindService {

	/**
	 * 通过物料条码查询物料信息
	 * @param materiel_code
	 * @return
	 */
	public Materiel selectMaterielInfoByCode(String materiel_code);
	/**
	 * 绑定
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int bindingAndUp(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);

	/**
	 * 通过物料条码查询物料所在位置
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielLocationByCode(String materiel_code);
	/**
	 * 解绑
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int downAndUnbind(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 条件查询库位信息
	 * @param tray_code
	 */
	public List<WarehouseRegionLocation> selectTopLocation(Materiel materiel);
	/**
	 * 查询全部库区
	 * @return
	 */
	public List<WarehouseRegion> selectAllRegion();
	/**
	 * 下架推荐库位
	 * @return
	 */
	public String selectTopLocationDown(String materiel_num);
	/**
	 * 区域库存的移动
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int bindAndUp2SpecialArea(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 扫码查询物料位置（下架）
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielLocationByCodeDown(String materiel_code);
	/**
	 * 通过物料条码查询物料是否在对应位置
	 * @param materiel_code
	 * @param location_code 库区编号
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielLocationByCodeDownLocation(String materiel_code,String location_code);
	public String selectByMaterielCode(String materiel_code);
	/**
	 * 根据精准码查询库位集合
	 * @param code
	 * @return
	 */
	public List<WarehouseRegionLocation> queryLocationByCode(String code);
	/**
	 * 查询库位码是否在良品区
	 * @param location_code
	 * @return
	 */
	public int selectCountGoodByLocationCode(String location_code);
	/**
	 * 查询库位码是否在不良品区
	 * @param location_code
	 * @return
	 */
	public int selectCountBadByLocationCode(String location_code);
	/**   
	 * 绑定物料托盘  
	 * @param zcMaterielAndTrayEntity             
	 */
	public void addMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**   
	 * 根据库位查询
	 * @param a
	 * @return             
	 */
	public ZcTrayAndLocationEntity selectTrayCodeByLocationCode(ZcTrayAndLocationEntity a);
	/**   
	 * 库位托盘绑定   
	 * @param newLocation             
	 */
	public void addTrayAndLocation(ZcMaterielAndTrayEntity newLocation);
	/**
	 * 缓冲区添加数据
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	Integer inCache(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 从良品库出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outGood(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**     
	 * 记录下架记录
	 * @param ZcMaterielAndTrayEntity    
	 */
	void insertUnbindleRecord(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);

	/**
	 * 通过物料条码查询物料所在位置 以托盘为主
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielTrayLocationByCode(String materiel_code);
}
