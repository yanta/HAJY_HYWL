package com.sdry.service.zc;

import java.util.List;

import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.MaterielInStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMoveRecordEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;

/**
 * 
 * @ClassName:    ZcInventoryManagementService   
 * @Description:  库存管理
 * @Author:       zc   
 * @CreateDate:   2019年4月19日 下午7:19:12   
 * @Version:      v1.0
 */
public interface ZcInventoryManagementService {

	/**
	 * 查询库存列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcInventoryInfoEntity> selectInventoryInfoList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询库存列表数目
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countInventoryInfoList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 *查询仓库列表 
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<Warehouse> selectAllWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 通过仓库id查库区
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<WarehouseRegion> selectWarehouseRegionListByWarehouseId(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 通过库区id查库位
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<WarehouseRegionLocation> selectWarehouseRegionLocationListByRegionId(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 通过产品码或简码查询库存信息
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcInventoryInfoEntity> selectInventoryInfoByCode(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 调整
	 * @param inventId
	 * @param mNum
	 * @return
	 */
	public int adjust(String inventId, String mNum);
	/**
	 * 库存预警
	 * @param zcInventoryInfoEntity
	 * @return
	 */
	public int stockWarning(ZcInventoryInfoEntity zcInventoryInfoEntity);
	/**
	 * 查询所有供应商
	 */
	public List<Customer> selectAllCustomerList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 移库
	 */
	public int bindingWarehouse(Materiel materiel);
	/**
	 * 通过产品码或简码查询规格型号
	 */
	public List<Materiel> selectMaterielSizeByCode(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/********************************************APP****************************************/
	/**
	 * 通过仓库和库区名称查询库区id
	 * @param ware
	 * @param srcRegion
	 * @return
	 */
	public Long selectSrcRegionIdByName(String ware, String srcRegion);
	/**
	 * 通过库区和库位名称查询库位id
	 * @param srcRegion
	 * @param srcLocation
	 * @return
	 */
	
	public Long selectSrcLocationIdByName(String srcRegion, String srcLocation);
	/**
	 * 查询库存预警列表
	 * @return
	 */
	public List<ZcInventoryInfoEntity> selectStockWarningList();
	/**
	 * 通过仓库名称查询id
	 * @param ware
	 * @return
	 */
	public Long selectSrcWareIdByName(String ware);
	/**
	 * 通过产品码及规格型号查询物料信息
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public Materiel selectMaterielInfoByCodeApp(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询物料信息详情
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public List<ZcMaterielAndTrayEntity> selectInventoryInfoListDetails(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询物料信息详情数量
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int countInventoryInfoListDetails(ZcGeneralQueryEntity zcGeneralQueryEntity);
	
	/**
	 * 通过产品码或简码查询物料规格型号信息2
	 * @param response
	 * @param mcode 产品码或简码
	 * @param specification 物料规格型号
	 */
	public List<MaterielInStock> queryMaterielBatchByMcodeSpecification(String mcode, String materiel_size, String materiel_properties);
	
	/**
	 * 根据物料ID去收货详细表查询
	 * @param id 物料id
	 * @return
	 */
	public List<ReceiveDetail> queryReceiveDetailByMaterielId(Long mid);
	/**
	 * 查询精简库存列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcInventoryInfoEntity> selectInventoryInfoOnlyCountList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询精简库存数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countInventoryInfoOnlyCountList(ZcGeneralQueryEntity zcGeneralQueryEntity);
}