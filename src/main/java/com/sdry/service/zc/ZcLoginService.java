package com.sdry.service.zc;

import java.util.Map;

import javax.servlet.http.HttpSession;

import com.sdry.model.zc.ResponseResult;
import com.sdry.model.zc.ZcSysUserEntity;

/**
 * 
 * @ClassName:    ZcLoginService   
 * @Description:  登录
 * @Author:       zc   
 * @CreateDate:   2019年4月17日 下午3:35:30   
 * @Version:      v1.0
 */
public interface ZcLoginService {

	/**
	 * pc登录
	 * @param zcSysUserEntity
	 * @return
	 */
	public ResponseResult<Boolean> pclogin(HttpSession httpSession,ZcSysUserEntity zcSysUserEntity);
	/**
	 * app登录
	 * @param zcSysUserEntity
	 * @return
	 */
	public Map<String ,Object> appLogin(ZcSysUserEntity zcSysUserEntity);
}
