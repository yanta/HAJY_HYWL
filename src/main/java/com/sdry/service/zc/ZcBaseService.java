package com.sdry.service.zc;

import java.util.List;

import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcStorehouseEntity;

/**
 * 基础模块逻辑层接口
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月31日 上午10:53:55
 * @Version 1.0
 */
public interface ZcBaseService {

	/**
	 * 条件查询库房列表
	 * @return
	 */
	public List<ZcStorehouseEntity> selectStorehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 条件查询库房数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countStorehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 新增库房
	 * @param zcStorehouseEntity
	 * @return
	 */
	public int insertStorehouse(ZcStorehouseEntity zcStorehouseEntity);
	
	/**
	 * 修改库房
	 * @param zcStorehouseEntity
	 * @return
	 */
	public int updateStorehouse(ZcStorehouseEntity zcStorehouseEntity);

	/**
	 * 删除库房
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int deleteStorehouseById(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 查询编号是否存在
	 * @param house_code
	 * @return
	 */
	public int selectCodeIsExistByCode(String house_code);

	/**
	 * 查询全部库房
	 * @return
	 */
	public List<ZcStorehouseEntity> selectAllStorehouse();
}
