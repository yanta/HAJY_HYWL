package com.sdry.service.zc;

import java.util.List;

import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.zc.ZcGeneralQueryEntity;

/**
 * 精简 入库
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年8月7日 上午9:37:06
 * @Version 1.0
 */
public interface ZcSimplificationInService {

	/**
	 * 新增入库单
	 * @param receive
	 * @return
	 */
	public int insertSimplificationIn(Receive receive);

	/**
	 * 新增详情
	 * @param receiveDetail
	 * @return
	 */
	public int insertDetails(ReceiveDetail receiveDetail);

	/**
	 * 条件查询入库单列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<Receive> selectSimplificationInList(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 条件查询入库单数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countSimplificationInList(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 批量删除
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int deleteSimplificationInByNumber(ZcGeneralQueryEntity zcGeneralQueryEntity);

	/**
	 * 查询状态为0的入库单
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<Receive> selectSimplificationInListAll(ZcGeneralQueryEntity zcGeneralQueryEntity);

}
