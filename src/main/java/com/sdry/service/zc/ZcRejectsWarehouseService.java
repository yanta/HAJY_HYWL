package com.sdry.service.zc;

import java.util.List;

import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;

/**
 * 不良库管理
 * @ClassName:    ZcRejectsWarehouseService   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年4月25日 下午5:18:54   
 * @Version:      v1.0
 */
public interface ZcRejectsWarehouseService {
	/**
	 * 查询不良库列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcRejectsWarehouseEntity> selectRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询不良库列表数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 不良品入库
	 * @param zcRejectsWarehouseEntity
	 * @return
	 */
	public int enterRejects(ZcRejectsWarehouseEntity zcRejectsWarehouseEntity);
}
