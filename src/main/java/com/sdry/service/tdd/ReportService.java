package	com.sdry.service.tdd;
import java.util.List;

import com.sdry.model.tdd.ReportUtil;
/**
 *
 *@ClassName: ReportMapper
 *@Description: 报表
 *@Author tdd
 *@Date 2019-05-27 13:47:25
 *@version 1.0
*/
public interface ReportService {
	
	/**
	 * 查询入库报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @return 报表
	 */
	public List<ReportUtil> selectRkReport(String date,String type,Long customer_id);
	/**
	 * 查询不良品报表
	 * @param date 日期
	 * @param type 报表类型
	 * @param customer_id 供应商id
	 * @return 报表
	 */
	public List<ReportUtil> selectBlpReport(String date,String type,Long customer_id);
	
}
