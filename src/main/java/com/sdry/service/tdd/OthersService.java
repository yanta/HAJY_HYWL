package	com.sdry.service.tdd;
import java.util.List;

import com.sdry.model.jyy.unpack.UnpackDetalCode;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.WaitSendArea;
import com.sdry.model.zc.ZcInventoryInfoEntity;
/**
 *
 *@ClassName: OthersService
 *@Description: 其他信息表
 *@Author tdd
 *@Date 2019-04-17 15:47:25
 *@version 1.0
*/
public interface OthersService {
	/**
	 * 根据物料id和数量按先进先出减库存
	 * @param mid 物料id
	 * @param utnum 数量
	 * @return 影响行数
	 */
	public Integer zcInventoryInfoFIFOReduceStock(Long mid,int utnum,Long customer_id);
	/**
	 * 插入库存返回id
	 * @param param 库存
	 * @return  id
	 */
	public Long zcInventoryInfoAddStock(ZcInventoryInfoEntity param);
	
	/**
	 * 插入待发货区返回id
	 * @param param 待发货区
	 * @return id
	 */
	public Long insertLjqWaitSendArea(WaitSendArea param);
	
	/** 
	 * 条件查询客户记录数
	 * @param param 客户实体条件
	 * @return 实体集合
	 */
	public List<Customer> customerQueryAllByMution(Customer param);
	/**
	 * 根据物料id和数量按先进先出大箱改小箱
	 * @param mid 物料id
	 * @param utnum 数量
	 * @return 影响行数
	 */
	public Integer zcInventoryInfoFIFODaGaiXiaoStock(Long mid, int utnum,
			Long customer_id);
	/**
	 * 根据收货计划单号查询供应商
	 * @param receiveNumber 收货计划单号
	 * @return 供应商
	 */
	public Customer selectCustomerByReceiveNumber(String receiveNumber);
	/**
	 * 根据物料主键更新物料顺序号
	 * @param id 物料主键
	 * @param num 顺序号
	 * @return
	 */
	public int updateMateriel(Long id, String num);
	/**
	 * 根据拆箱单id查询条码
	 * @param unpackId 拆箱单id
	 * @return
	 */
	public List<UnpackDetalCode> queryUnpackDetalCodeByUnpackId(Long unpackId);
}
