package	com.sdry.service.tdd;
import com.sdry.model.tdd.Unpacking;
import java.util.List;
/**
 *
 *@ClassName: UnpackingService
 *@Description: 拆箱表
 *@Author tdd
 *@Date 2019-05-14 10:56:27
 *@version 1.0
*/
public interface UnpackingService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public Unpacking queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Unpacking> queryAllByMution(Unpacking param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Unpacking> findPageByMution(Unpacking param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(Unpacking param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(Unpacking param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(Unpacking param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
}
