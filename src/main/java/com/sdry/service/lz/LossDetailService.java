package com.sdry.service.lz;

import java.util.List;

import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LossDetail;
import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzInventoryDetailsCodeEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;

/**
 * @ClassName LossDetailService
 * @Description 损益单详细
 * @Author zy
 * @Date 2019年4月16日 10:10:13
 * @Version 1.0 
 */
public interface LossDetailService {

    Long addLossDetail(LossDetail lossDetail);
    Long  updateLossDetail(LossDetail lossDetail);
    List<LossDetail>  queryLossDetail(Long lossid);
    int  countLossDetail(Long lossid);
    List<LossDetail>  queryMidByLossId(Long lossid);
    List<ZcInventoryInfoEntity>  queryMnumByMId(Long mid);
    LossSpillover queryLossById(Long lossid);
	/**   
	 * 根据物料ID找到所有的条码   
	 * @param mid
	 * @return             
	 */
	List<CodeMark> queryCodeById(Long mid);
	
	/**   
	 * 根据损益单ID找到所有盘点的条码   
	 * @param mid
	 * @return             
	 */
	List<LzInventoryDetailsCodeEntity> queryCodeById2(Long lossId, Long mid);
}
