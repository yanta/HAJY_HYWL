package com.sdry.service.lz;

import com.sdry.model.lz.*;

import java.util.List;
import java.util.Map;

/**
 * @ClassName MaterielService
 * @Description 物料信息
 * @Author lz
 * @Date 2019年4月15日 17:47:13
 * @Version 1.0
 */
public interface MaterielService {

    Long addMateriel(Materiel materiel);

    Long deleteMaterielById(long id);

    Long updateMateriel(Materiel materiel);

    List<Materiel> queryMaterielCriteria(LzQueryCriteria criteria);

    int countMaterielCriteria(LzQueryCriteria criteria);

	List<Container> queryAllContainer();
	
	//ljq 根据物料ID查询物料信息
    Materiel queryMaterielById(long id);

	//int bindingCustomer(Materiel materiel);
	//仓库列表
	List<Warehouse> selectAllWarehouse();
	//通过仓库id查库区
	List<WarehouseRegion> selectRegionByWareid(Long id);
	//通过库区id查库位
	List<WarehouseRegionLocation> selectLocationByRegionid(Long id);
	//绑定仓库
	int warehouseBindingWarehouse(Customer customer);

	List<Materiel> querymaterielcustomerbyid(Map<String, Object> map);
	
	int countmaterielcustomerbyid(Map<String, Object> map);
	
	/**
	 * 根据客户id查询关联表中的库区
	 * @param customer_id 客户id
	 * @throws Exception
	 */
	List<WarehouseRegion> queryRegionByCustomerId(String customer_id);

	/**
	 * 物料与仓库绑定时修改客户与仓库关联表的状态
	 * @param materiel
	 */
	void updateCustomerWarehouseStatus(Materiel materiel);
	
	List<Materiel> queryMeterailByOutid(String outnum);
	
	List<Materiel> queryAllMC(Map<String, Object> map);

	int countAllMC(Map<String, Object> map);

	Long queryCustnameByname(String name);

	Long queryRegionByname(String name);

	Long queryContainerByName(String name);

	List<Materiel> queryAllMateriel();

	/**
	 * 查询所有库区
	 * @param customer_id
	 * @return
	 */
    List<WarehouseRegion> queryRegionByCustomerId1(String customer_id);

    /**
     * 根据库区id查询库区类型
     * @param region_id 库区id
     * @param response
     * @throws Exception
     */
	WarehouseRegion queryRegionTypeById(String region_id);

	/**
	 * 根据物料id查询客户
	 * @param mid
	 * @return
	 */
	Customer queryCustomerByCustomerId(Long customerId);
}
