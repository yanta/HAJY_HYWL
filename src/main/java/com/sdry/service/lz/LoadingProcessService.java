package com.sdry.service.lz;

import java.util.List;

import com.sdry.model.lz.LoadingProcess;
import com.sdry.model.lz.OutStockOrder;

/**
 * @ClassName LoadingProcessService
 * @Description 装车处理
 * @Author lz
 * @Date 2019年4月25日 11:26:15
 * @Version 1.0
 */
public interface LoadingProcessService {

	Long addLoadingProcess(LoadingProcess loadingProcess);
	
	Long deleteLoadingProcessById(long id);
	
	List<OutStockOrder> queryOutStockOrderByOutStockOrderNum(
			String out_stock_order_num);
}
