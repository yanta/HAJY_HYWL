package com.sdry.service.lz;

import com.sdry.model.lz.Department;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Post;

import java.util.List;

/**
 * @ClassName PostService
 * @Description 岗位信息
 * @Author lz
 * @Date 2019年4月16日 09:46:12
 * @Version 1.0
 */
public interface PostService {

    Long addPost(Post post);

    Long deletePostById(long id);

    Long updatePost(Post post);

    List<Post> queryPostCriteria(LzQueryCriteria criteria);

    int countPostCriteria(LzQueryCriteria criteria);

	List<Department> queryAllDept();

	List<Post> queryAllPost();
}
