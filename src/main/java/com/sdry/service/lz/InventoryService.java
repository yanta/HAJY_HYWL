package com.sdry.service.lz;

import java.util.List;
import java.util.Map;

import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.lz.InventoryDetail;
import com.sdry.model.lz.InventoryDetailCode;
import com.sdry.model.lz.InventoryOrder;
import com.sdry.model.lz.LzInventoryDetailsCodeEntity;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;

/**
 * @ClassName InventoryService
 * @Description 盘点管理
 * @Author lz
 * @Date 2019年3月26日 14:19:23
 * @Version 1.0
 */
public interface InventoryService {

	Long addInventoryOrder(InventoryOrder inventoryOrder);

	Long addInventoryOrderDetail(InventoryDetail detail);
	
	Long deleteInventoryOrderById(long id);

	Long deleteInventoryOrderDetailById(long id);

	Long updateInventoryOrderById(String id, String fieldName, String fieldValue);

	Long updateInventoryOrderDetailById(String mid, String fieldName,
			String fieldValue,String cid,String detailid);

	List<InventoryOrder> queryInventoryOrderCriteria(LzQueryCriteria criteria);

	int countInventoryOrderCriteria(LzQueryCriteria criteria);
	
	List<InventoryDetail> queryInventoryDetailCriteriaById(
			LzQueryCriteria criteria);

	int countInventoryDetailCriteriaById(LzQueryCriteria criteria);

	
	List<InventoryOrder> queryInventoryOrderCriteriaAndroid();

	List<InventoryDetail> queryInventoryDetailCriteriaByIdAndroid(
			String order_id);

	Long updateInventoryDetailAndroid(InventoryDetail inventoryDetail);

	List<Warehouse> queryAllWarehouseAndroid();
	
	//ljq 查询所有盘点单
	List<InventoryOrder> queryAllInventoryOrder();
	
	List<InventoryOrder> queryAllInventoryOrder1();
	//ljq 根据盘点单查询所有详情
	List<InventoryDetail> queryInventoryDetailByOrder(String orderId);
	//ljq 根据盘点详情单ID查询
	InventoryDetail queryInventoryDetailById(long id);
	 Long queryInventorynum(String num);
	 
	 List<InventoryDetail>  queryDetailByid(long  id);
	 
	 List<InventoryDetail>  queryMaterielId(long  id);
	/**   
	 * 查询所有员工
	 * @Title:queryAllUser   
	 * @return             
	 */
	List<ZcSysUserEntity> queryAllUser();

	/**   
	 * llm 根据盘点单号查盘点单   
	 * @param inventory_order
	 * @return             
	 */
	InventoryOrder queryInventoryOrderByNumber(String inventory_order);

	/**   
	 * llm 根据物料ID和盘点单ID确定唯一的盘点单详情ID  
	 * @param id
	 * @param materielId
	 * @return             
	 */
	Long queryInventoryDetailByOrderIdAndMid(Long id, Long materielId);

	/**   
	 * llm
	 * @Title:updateById   
	 * @param id
	 * @param fieldName
	 * @param fieldValue
	 * @param tableName             
	 */
	int updateById(String id, String fieldName, String fieldValue, String tableName);

	/**   
	 * 根据盘点单详情ID查条码  
	 * @param detailId
	 * @return             
	 */
	List<LzInventoryDetailsCodeEntity> queryCodeById(Long detailId);

	/**   
	 * 根据条码找到物料ID和批次   
	 * @param code
	 * @return             
	 */
	ReceiveDetailQuality queryReceiveByCode(String code);

	/**   
	 * 修改库存数量
	 * @param string
	 * @param string2
	 * @param string3
	 */
	void updateInventoryQuantity(Map<String, Object> map);

	/**   
	 * 修改条码对应的数量 
	 * @param map2             
	 */
	void updateCodeMarkQuantity(Map<String, Object> map2);

	/**修改盘点单*/
	int update(InventoryOrder inventoryOrder);
	
	
	/** 
	 * @author jyy 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(InventoryDetailCode param);
	/** 
	 * @author jyy
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<InventoryDetailCode> findPageByMution(InventoryDetailCode param);
	/** 
	 * @author jyy
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(InventoryDetailCode param);
	/** 
	 * @author jyy
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer updateCode(InventoryDetailCode param);

	/**
	 * 根据盘点详情id查询复盘条码
	 * @author jyy
	 * @param invenId 盘点详情id
	 * @return 复盘条码集合
	 */
	List<InventoryDetailCode> queryCodeListByinvenId(Long invenId);

	/**
	 * 根据库位编号查询物料数量
	 * @param location_num
	 * @return
	 */
	int selectKwCount(String location_num);

	/**
	 * 查询需要复盘的详情
	 * @param order_id
	 * @return
	 */
	List<InventoryDetail> queryInventoryDetailCriteriaByIdAndroid2(String order_id);

	/**   
	 * 根据ID查盘点单详情   
	 * @param id
	 * @return             
	 */
	InventoryDetail queryInventoryDetailById2(Long id);

	/**   
	 * @param inventoryDetailCodes             
	 */
	void insertCodeInInventory(List<InventoryDetailCode> inventoryDetailCodes, Long orderId, Long detailId);

	/**   
	 * @param id
	 * @return             
	 */
	List<InventoryDetailCode> queryCodeByDiff(Long id);
	
	int cacelpd(Map map);
		
	int restorpd(Map map);

	/**
	 * 在物料托盘绑定表中新增一条数据
	 * @param zcMaterielAndTrayEntity
	 * @return 影响行数
	 */
	int MaterielTrayInsert(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);

	/**
	 * 查询托盘-库位绑定表中有没有损溢区的记录
	 * @return 托盘-库位绑定实体
	 */
	ZcTrayAndLocationEntity selectTrayLocationByLocationCode();

	/**
	 * 在托盘-库位绑定表（zc_tray_location）中追加托盘码
	 * @param zcTrayAndLocationEntity 托盘-库位绑定实体
	 * @return 影响行数
	 */
	int updateTrayLocationByID(ZcTrayAndLocationEntity zcTrayAndLocationEntity);

	/**
	 * 在托盘-库位绑定表中新增一条记录
	 * @param zcTrayAndLocationEntity2
	 * @return
	 */
	int trayLocationInsert(ZcTrayAndLocationEntity zcTrayAndLocationEntity2);

	/**   
	 * 根据ID删除数据   
	 * @param string
	 * @param string2             
	 */
	void deleteById(String id, String table);

	/**   
	 * 根据条件删除   
	 * @param tray
	 * @param string             
	 */
	void deleteByCode(String fileName, String pram, String string);
}