package com.sdry.service.lz;

import java.util.List;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Staff;
import com.sdry.model.zc.ZcSysUserEntity;

/**
 * @ClassName StaffService
 * @Description 员工信息
 * @Author lz
 * @Date 2019年4月16日 10:47:41
 * @Version 1.0
 */
public interface StaffService {

    Long addStaff(Staff staff);

    Long deleteStaffById(long id);

    Long updateStaff(Staff staff);

    List<Staff> queryStaffCriteria(LzQueryCriteria criteria);

    int countStaffCriteria(LzQueryCriteria criteria);
    
    //ljq 根据部门ID查询员工信息
    List<ZcSysUserEntity> queryStaffByDept(long id);
    //ljq 根据用户ID查询员工信息
    ZcSysUserEntity queryStaffById(long id);
}