package com.sdry.service.lz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.AbnormalAdjustment;
import com.sdry.model.lz.AbnormalDetail;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.OutStockOrder;

/**
 * @ClassName AbnormalAdjustmentService
 * @Description 异常调整
 * @Author lz
 * @Date 2019年4月23日 11:01:19
 * @Version 1.0
 */
public interface AbnormalAdjustmentService {

	Long addAbnormalAdjustment(AbnormalAdjustment abnormalAdjustment);

	Long deleteAbnormalAdjustmentById(long id);

	Long updateAbnormalAdjustmentById(String id, String fieldName,
			String fieldValue);
	Long updateAbnormalDetailById(String id, String fieldName,
			String fieldValue);
	List<AbnormalAdjustment> queryAbnormalAdjustmentCriteria(
			LzQueryCriteria criteria);

	int countAbnormalAdjustmentCriteria(LzQueryCriteria criteria);

	List<OutStockOrder> queryOutStockOrderByNum(String out_order_num);
	
	List<Materiel>  queryMaCuRe(long cid);
	Long addAbnormalDetail(AbnormalDetail AbnormalDetail);
	
	List<Materiel> queryAbnormalDetail(long aid);
	
	AbnormalDetail queryAbnormalDetailByid(long id);
	
	
	List<AbnormalDetail> queryMateridByAbnormalid(long aid);
}
