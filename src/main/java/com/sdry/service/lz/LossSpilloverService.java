package com.sdry.service.lz;

import java.util.List;

import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzQueryCriteria;

/**
 * @ClassName LossSpilloverService
 * @Description 损溢管理
 * @Author lz
 * @Date 2019年3月25日 11:06:45
 * @Version 1.0
 */
public interface LossSpilloverService {

	Long addLossSpillover(LossSpillover lossSpillover);

	Long deleteLossSpilloverById(long id);

	Long updateLossSpilloverById(String id, String fieldName, String fieldValue);

	List<LossSpillover> queryLossSpilloverCriteria(LzQueryCriteria criteria);

	int countLossSpilloverCriteria(LzQueryCriteria criteria);
	Long updateLossTabByNum(String num);
	
	
}
