package com.sdry.service.lz;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Region;

import java.util.List;

/**
 * @ClassName RegionService
 * @Description 地区信息
 * @Author lz
 * @Date
 * @Version 1.0
 */
public interface RegionService {
    
    Long addRegion(Region region);

    Long deleteRegionById(long id);

    Long updateRegion(Region region);

    List<Region> queryRegionCriteria(LzQueryCriteria criteria);

    int countRegionCriteria(LzQueryCriteria criteria);

    List<Region> queryAllRegion();
}
