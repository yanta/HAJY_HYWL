package com.sdry.service.lz;

import com.sdry.model.lz.Container;
import com.sdry.model.lz.LzQueryCriteria;

import java.util.List;

/**
 * @ClassName ContainerService
 * @Description 容器信息
 * @Author lz
 * @Date 2019年4月16日 11:24:44
 * @Version 1.0
 */
public interface ContainerService {

    Long addContainer(Container container);

    Long deleteContainerById(long id);

    Long updateContainer(Container container);

    List<Container> queryContainerCriteria(LzQueryCriteria criteria);

    int countContainerCriteria(LzQueryCriteria criteria);
}
