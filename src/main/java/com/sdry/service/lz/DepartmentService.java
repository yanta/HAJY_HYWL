package com.sdry.service.lz;

import com.sdry.model.lz.Department;
import com.sdry.model.lz.LzQueryCriteria;

import java.util.List;

/**
 * @ClassName DepartmentService
 * @Description 部门信息
 * @Author lz
 * @Date 2019年4月16日 10:21:43
 * @Version 1.0
 */
public interface DepartmentService {

    Long addDept(Department dept);

    Long deleteDeptById(long id);

    Long updateDept(Department dept);

    List<Department> queryDeptCriteria(LzQueryCriteria criteria);

    int countDeptCriteria(LzQueryCriteria criteria);
    
    //ljq  无条件查询所有部门信息
    List<Department> queryAllDept();
}