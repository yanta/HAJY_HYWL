package com.sdry.service.lz;

import com.sdry.model.lz.Car;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;

import java.util.List;

/**
 * @ClassName CarService
 * @Description 车辆信息
 * @Author lz
 * @Date 2019年4月16日 10:10:13
 * @Version 1.0 
 */
public interface CarService {

    Long addCar(Car car);

    Long deleteCarById(long id);

    Long updateCar(Car car);

    List<Car> queryCarCriteria(LzQueryCriteria criteria);

    int countCarCriteria(LzQueryCriteria criteria);

    /**
     * 根据条码查询该条码是否存在
     * @param response
     * @param beforeCode 原条码
     */
	CodeMark queryCodeExist(String beforeCode);

	/**
	 * 修改条码
	 * @param beforeCode 原条码
	 * @param newCode 新条码
	 * @return
	 */
	Long editCode(String beforeCode, String newCode);

	/**
	 * 用新条码替换旧条码
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	int updateCodeByid(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);

	/**
	 * 验证是否已存在
	 * @param newCode 新条码
	 * @return
	 */
	int checkNewCode(String newCode);
}
