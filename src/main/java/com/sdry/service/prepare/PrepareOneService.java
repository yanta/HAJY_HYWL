package	com.sdry.service.prepare;
import com.sdry.model.prepare.PrepareOne;

import java.util.List;
/**
 *
 *@ClassName: PrepareOneService
 *@Description: 配货单
 *@Author tdd
 *@Date 2019-08-24 16:48:02
 *@version 1.0
*/
public interface PrepareOneService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public PrepareOne queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareOne> queryAllByMution(PrepareOne param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareOne> findPageByMution(PrepareOne param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(PrepareOne param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(PrepareOne param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(PrepareOne param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**
	 * 根据条码在lz_codemark查询数量
	 * @param materiel_code 条码
	 * @return
	 */
	public Integer selectCodemarkNumbyCode(String materiel_code);
}
