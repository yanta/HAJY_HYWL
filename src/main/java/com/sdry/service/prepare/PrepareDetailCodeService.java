package	com.sdry.service.prepare;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.prepare.PrepareDetailCode;

import java.util.List;
/**
 *
 *@ClassName: PrepareDetailCodeService
 *@Description: 配货单详情条码
 *@Author tdd
 *@Date 2019-08-24 16:49:04
 *@version 1.0
*/
public interface PrepareDetailCodeService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public PrepareDetailCode queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetailCode> queryAllByMution(PrepareDetailCode param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetailCode> findPageByMution(PrepareDetailCode param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(PrepareDetailCode param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(PrepareDetailCode param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(PrepareDetailCode param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**
	 * 根据条码查询库位 下架 
	 * @param code 条码
	 * @return
	 */
	public WarehouseRegionLocation selectWarehouseRegionLocationByCodeDown(
			String code);
}
