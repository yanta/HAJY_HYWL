package	com.sdry.service.prepare;
import com.sdry.model.prepare.PrepareDetail;

import java.util.List;
/**
 *
 *@ClassName: PrepareDetailService
 *@Description: 配货单详情
 *@Author tdd
 *@Date 2019-08-24 16:47:15
 *@version 1.0
*/
public interface PrepareDetailService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public PrepareDetail queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetail> queryAllByMution(PrepareDetail param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<PrepareDetail> findPageByMution(PrepareDetail param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(PrepareDetail param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(PrepareDetail param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(PrepareDetail param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/** 
	 * 根据配货单id拼接字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	 */
	public Integer deleteByDids(String ids);
	/**
	 * 根据物料id查询推荐库位编号
	 * @param mid 物料id
	 * @return
	 */
	public List<String> selectLocationByMid(Long mid);
}
