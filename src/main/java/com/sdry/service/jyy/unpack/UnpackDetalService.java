package	com.sdry.service.jyy.unpack;
import com.sdry.model.jyy.unpack.UnpackDetal;

import java.util.List;
/**
 *
 *@ClassName: UnpackDetalService
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:14
 *@version 1.0
*/
public interface UnpackDetalService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public UnpackDetal queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetal> queryAllByMution(UnpackDetal param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackDetal> findPageByMution(UnpackDetal param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(UnpackDetal param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(UnpackDetal param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(UnpackDetal param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**
	 * 根据员工姓名 查询员工id
	 * @param workman 员工姓名 
	 * @return 员工id
	 */
	public Long selectUidByUname(String workman);
}
