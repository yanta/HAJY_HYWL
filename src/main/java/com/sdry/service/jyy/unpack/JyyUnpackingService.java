package	com.sdry.service.jyy.unpack;
import com.sdry.model.jyy.unpack.JyyUnpacking;
import com.sdry.web.controller.jyy.unpack.JyyInfoGood;

import java.util.List;
/**
 *
 *@ClassName: UnpackingService
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:57:31
 *@version 1.0
*/
public interface JyyUnpackingService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public JyyUnpacking queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<JyyUnpacking> queryAllByMution(JyyUnpacking param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<JyyUnpacking> findPageByMution(JyyUnpacking param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(JyyUnpacking param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(JyyUnpacking param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(JyyUnpacking param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**
	 * 条件查询良品库存
	 * @param customerId
	 * @return
	 */
	public List<JyyInfoGood> selectPageGood(JyyInfoGood jyyInfoGood);
	/**
	 * 条件统计良品库存
	 * @param customerId
	 * @return
	 */
	public int selectCountGood(JyyInfoGood jyyInfoGood);
	
	/**
	 * 更改单子状态为已完成
	 * @param id 
	 * @return
	 */
	public Integer updateState(Long id);
}
