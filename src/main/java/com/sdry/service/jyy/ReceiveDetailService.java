package com.sdry.service.jyy;
import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.jyy.Workload;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.ReceiveMark;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;

import java.util.List;
import java.util.Map;
/**
 *
 *@ClassName: ReceiveDetailService
 *@Description: 收货计划详情
 *@Author jyy
 *@Date 2019-04-19 14:30:17
 *@version 1.0
*/
public interface ReceiveDetailService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	ReceiveDetail queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	List<ReceiveDetail> queryAllByMution(ReceiveDetail param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	List<ReceiveDetail> findPageByMution(ReceiveDetail param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	Integer selectCountByMution(ReceiveDetail param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	Long insert(ReceiveDetail param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	Integer update(ReceiveDetail param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	Integer delete(String ids);
	
	/**
	 * 统计收货详情中有几条未收货的
	 * @param receiveNumber
	 * @return
	 */
	int countState(String receiveNumber);

	/**
	 * 根据物料id查询物料实体
	 */
	Materiel queryMaterielByReceiveDetailId(Long mid);

	/**
	 * 添加每种物料对应的多个条码
	 * @param codemark
	 */
    void addCodeMark(CodeMark codemark);

    /**
	 * 
	 * @param receiveDetail
	 */
	//public void addReceiveDetailTemp(ReceiveDetail receiveDetail);

	/**
	 * 根据收货单详细表id查询对应的条码
	 * @param receive_detail_id 收货单详细表id
	 * @throws Exception
	 */
	List<CodeMark> queryCodemarkByReceiveDetailId(String receive_detail_id);

	/**
	 * 查询状态为1的收货单详细
	 * @throws Exception
	 */
    List<ReceiveDetail> selectAccurateByCodeApp(Long id);

	/**
	 * 根据mcode查询收货单详细
	 * @param mcode
	 * @return
	 */
	CodeMark queryReceiveDetailIdByMcode(String mcode);

	/**
	 * 新表添加数据
	 * @param receiveDetail
	 */
    void addReceiveDetailInstock(ReceiveDetail receiveDetail);

	/**
	 * 质检表添加信息
	 * @param receiveDetail
	 */
	Long addReceiveDetailQuality(ReceiveDetail receiveDetail);

	/**
	 * 质检表修改信息
	 * @param receiveDetail
	 */
	void updateReceiveDetailQuality(ReceiveDetail receiveDetail);

	/**
	 * 分页查询质检单
	 * @param criteria
	 * @return
	 */
	List<ReceiveDetailQuality> queryReceiveDetailQualityCriteria(LzQueryCriteria criteria);

	/**
	 * 分页查询质检单数量
	 * @param criteria
	 * @return
	 */
	int countReceiveDetailQualityCriteria(LzQueryCriteria criteria);

	/**
	 * 查询状态为0的质检表
	 * @return
	 */
	List<ReceiveDetailQuality> queryReceiveDetailQualityAPP();

	/**
	 * 查询状态为1的质检表
	 * @return
	 */
	List<ReceiveDetailQuality> queryReceiveDetailQualityAPP1();

	/**
	 * 修改良品数和不良品数
	 * @param receiveDetailQuality
	 * @return
	 */
	Long updateReceiveDetailQualityAPP(ReceiveDetailQuality receiveDetailQuality);

	/**
	 * 添加每种物料对应的工作量
	 * @param workload
	 */
	void addWorkload(Workload workload);

	/**
	 * 绑定后修改质检的状态为2
	 * @param receiveDetailId
	 */
	void updateReceiveDetailQuality2(String receiveDetailId);

	/**
	 * 拆箱
	 * @param code
	 */
	void uncomeku(String code);
	
	/**
	 * 给不良品库存添加数据
	 * @param receiveDetailQuality
	 */
	void addZcRejectsWarehouseEntity(ReceiveDetailQuality receiveDetailQuality);
	
	/**
	 * 将库存表中的不良品数量减掉
	 * @param receiveDetailQuality
	 */
	void updateInventoryInfoNum(ReceiveDetailQuality receiveDetailQuality);
	
	/**
	 * 根据质检单详细表id查询对应的工作量
	 * @param receiveDetailQuality_id 质检单详细表id
	 * @param response
	 * @throws Exception
	 */
	List<Workload> queryWorkloadByreceiveDetailQualityId(
			String receiveDetailQuality_id);
	
	/**
	 * 首先根据物料id和批次查询是否存在
	 * @param receiveDetailQuality
	 * @return
	 */
	ZcRejectsWarehouseEntity queryZcRejectsWarehouseEntityByMbatchAngMid(
			ReceiveDetailQuality receiveDetailQuality);
	
	/**
	 * 如果不良品库存在该物料就累加
	 */
	void updateZcRejectsWarehouseEntity(
			ReceiveDetailQuality receiveDetailQuality);
	
	/**
	 * 根据精准码查询是否存在
	 * @param mcode
	 * @param response
	 */
	CodeMark queryRepeatAccurateByCodeApp(String mcode);
	
	/**
	 * 查询有仓库操作员权限的人员
	 * @param mcode
	 * @param response
	 */
	List<String> queryRoleUser();
	
	/**
	 * 质检表添加信息2
	 * @param receiveDetail
	 */
	Long addReceiveDetailQuality1(ReceiveDetail receiveDetail);
	
	/**
	 * 根据条码修改条码状态
	 * @param code
	 */
	Long updateIsngStatus(String code);
	
	/**
	 * 根据条码查询物料和批次
	 * @param code
	 * @return
	 */
	ReceiveDetailQuality queryReceiveDetailQualityByCode(String code);
	
	/**
	 * 修改CodeMark
	 * @param codeMark
	 * @return
	 */
	Long updateCodeMark(CodeMark codeMark);
	
	/**
	 * 根据CodeMark查询是否存在
	 * @param receiveCode
	 * @return
	 */
	CodeMark queryCodeMarkByCode(CodeMark codemark);
	
	/**
	 * 根据收货单id查询CodeMark
	 * @param receive_detail_id
	 * @return
	 */
	List<CodeMark> queryCodeMarkById(String receive_detail_id);
	
	/**
	 * 根据精准码修改质检区库存的数量
	 * @param mid 物料id
	 * @param pici 批次
	 * @param 差值数
	 */
	void updateInventoryInfoQuality(String mid, String pici, String afterVal);
	
	/**
	 * 绑定条码
	 * @param receiveMark
	 */
	void addReceiveMark(ReceiveMark receiveMark);
	
	/**
	 * 根据收货单id查询收货绑定实体
	 * @param receiveId 收货单id
	 * @param response
	 */
	List<ReceiveMark> queryReceiveMarkByReceiveId(String receiveDetailId);
	
	/**
	 * 根据收货单id查询所有ReceiveMark实体
	 * @param receiveId 收货单id
	 * @param mcode
	 * @param response
	 */
	ReceiveMark queryReceiveCodeByReceiveId(String receiveId, String mcode);
	
	/**
	 * 根据收货单详细id查询所有ReceiveMark实体
	 * @param receiveDetailId 收货单详细id
	 * @param mcode
	 * @param response
	 */
	List<ReceiveMark> queryReceiveCodeByReceiveDetailId(String receiveDetailId);
	
	/**
	 * 传入查询到的条码和提交的数量
	 * @param code 条码
	 * @param string 数量
	 * @return
	 */
	//Long updateCodeMark(String code, String num);
	/**
	 * 根据收货单id查询条码
	 * @param receiveId 收货单id
	 * @param response
	 */
	List<ReceiveMark> selectReceiveMarkByReceiveId(Long receiveId);
	
	/**
	 * 根据条码查询是否包含
	 * @param code
	 * @return
	 */
	ZcMaterielAndTrayEntity queryMaterielTrayByCode(String code);
	
	/**
	 * 根据id替换条码
	 * @param indentity
	 * @param code
	 */
	void updateMaterielTrayByCode(String indentity, String code);
	/**   
	 * 根据ID删除记录  
	 * @param id             
	 */
	void deleteMaterielTrayById(Long id);
	
	/**
	 * 根据物料id查询该质检表批次的该物料是否存在
	 * @param mid 物料id
	 * @param pici 批次
	 * @return
	 */
	ReceiveDetail queryReceiveDetaiQualityExsit(String mid, String pici, String receiveNumber);
	
	/**   
	 * 根据托盘号查询托盘和库位绑定记录
	 * @param tray 托盘号
	 * @return             
	 */
	ZcTrayAndLocationEntity queryTrayLocationByTray(String tray);
	
	/**   
	 * 根据ID删除 托盘和库位绑定记录
	 * @param id             
	 */
	void deleteTrayLocationById(Long id);
	
	/**   
	 * 根据物料ID和批次减待检区库存  
	 * @param map             
	 */
	void updateQuantityByMidAndPici(Map<String, Object> map);
	
	/**
	 * 根据物料id和批次修改不质检的数量
	 * @param receiveDetail
	 * @return
	 */
	Long updateReceiveDetailQuality1(ReceiveDetail receiveDetail);
	
	/**
	 * 根据物料id和批次修改质检的数量
	 * @param receiveDetail
	 * @return
	 */
	Long updateReceiveDetailQuality3(ReceiveDetail receiveDetail);
	
	/**
	 * 根据物料id和批次查询修改的数据的主键
	 * @param receiveDetail
	 * @return
	 */
	Long queryIndentityByMidAndPici(ReceiveDetail receiveDetail);
	
	/**
	 * 根据托盘码查询物料托盘信息
	 * @param tray_code
	 * @return
	 */
	List<ZcMaterielAndTrayEntity> selectZcMaterielAndTrayEntityByTray(
			String tray_code);
	
}
