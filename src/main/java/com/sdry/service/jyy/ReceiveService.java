package com.sdry.service.jyy;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.Up;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
/**
 *
 *@ClassName: ReceiveService
 *@Description: 收货计划
 *@Author jyy
 *@Date 2019-04-18 09:35:01
 *@version 1.0
*/
public interface ReceiveService {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	Receive queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	List<Receive> queryAllByMution(Receive param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	List<Receive> findPageByMution(Receive param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	Integer selectCountByMution(Receive param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	Long insert(Receive param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	Integer update(Receive param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	Integer delete(String ids);
	
	/**
	 * 根据物料id 和 供应商 名称 查询物料的仓库信息
	 * @param mid 物料id
	 * @return
	 */
	List<LlmWarehouseStock> selectWarehouseByMid(Up up);
	
	/**
	 * 根据物料产品码查询物料所有信息
	 * @param maNum 物料产品码
	 * @return
	 */
	Materiel selectMaByMaNum(String maNum);
	
	/**
	 * 根据供应商 名称 查询相关物料信息
	 * @param id
	 * @return
	 */
	List<Materiel> materielList(long id);
	
	/**
	 * 根据物料id查询供应商
	 * @param id 物料id
	 * @return
	 */
	List<Customer> selectCustomerByMid(Long id);
	
	/**
	 * 根据物料id查询库区位置
	 * @param mid 物料id
	 * @return
	 */
	Materiel queryMaterielLocationById(Long mid);
	
	/**   
	 * 根据条件查询物料 
	 * @param materiel 物料实体
	 * @return             
	 */
	List<Materiel> queryAllMaterielByParam(Materiel materiel);
	/**
	 * 根据条件查询单据（不区分发货单，精简发货）
	 * @param receive
	 * @return
	 */
	List<Receive> allByMution(Receive receive);
	
	public int restor(Map map);
	
	public int cacel(Map map);
	/**
	 * 根据id条件查询收货数量
	 * @param receive
	 * @return
	 */
	public int  queryCodeNumByid(int id);
	
	/**
	 * 单据合并
	 * @param httpSession
	 * @param ids 收货单ID拼接的字符串
	 * @param receiveNumber 新生成的收货单号
	 * @return
	 */
	int merge(HttpSession httpSession, String ids, String receiveNumber);
}
