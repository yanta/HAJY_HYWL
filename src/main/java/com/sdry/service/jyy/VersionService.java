package com.sdry.service.jyy;

import com.sdry.model.jyy.Version;



/**
 * 
 * @Title:VersionService
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年3月26日
 * @version 1.0
 */
public interface VersionService {

	/**   
	 * 获取最新版本
	 * @Title:getLatestVersion   
	 * @return             
	 */
	Version getLatestVersion();

}
