package com.sdry.model.llm;

import java.util.List;

/**
 * 
 * @Title:Stock
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年4月26日
 * @version 1.0
 */
public class LlmWarehouseStock {

	//id
    private Long id;
    //仓库管理员姓名
    private String staff_name;
    //仓库名称
    private String warehouse_name;
    //仓库编号
    private String warehouse_num;
    //仓库属性
    private String warehouse_properties;
    //仓库地址
    private String warehouse_address;
    //仓库拥有者(供应商公司名称)
    private String warehouse_owner;
    //仓库状态
    private String status;
    //创建日期
    private String create_time;
    //备注
    private String remark;
    //库区集合
    private List<LlmRegionStock> regionList;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStaff_name() {
		return staff_name;
	}
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getWarehouse_num() {
		return warehouse_num;
	}
	public void setWarehouse_num(String warehouse_num) {
		this.warehouse_num = warehouse_num;
	}
	public String getWarehouse_properties() {
		return warehouse_properties;
	}
	public void setWarehouse_properties(String warehouse_properties) {
		this.warehouse_properties = warehouse_properties;
	}
	public String getWarehouse_address() {
		return warehouse_address;
	}
	public void setWarehouse_address(String warehouse_address) {
		this.warehouse_address = warehouse_address;
	}
	public String getWarehouse_owner() {
		return warehouse_owner;
	}
	public void setWarehouse_owner(String warehouse_owner) {
		this.warehouse_owner = warehouse_owner;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<LlmRegionStock> getRegionList() {
		return regionList;
	}
	public void setRegionList(List<LlmRegionStock> regionList) {
		this.regionList = regionList;
	}
    
}
