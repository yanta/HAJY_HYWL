package	com.sdry.model.llm;

import com.sdry.utils.Page;

/**
 *
 *@ClassName: DeliveryNode
 *@Description: 发货单
 *@Author llm
 *@Date 2019-07-17 14:14:41
 *@version 1.0
*/
public class DeliveryNode extends Page{
	/** 主键ID */
	private Long id;
	/** 发货单号 */
	private String number;
	/** 车辆ID */
	private Long carId;
	/** 出发时间 */
	private String startDate;
	/** 到达时间 */
	private String endDate;
	/** 状态 */
	private int status;
	/** 备注 */
	private String remark;
	/** 车牌号 */
	private String carPlate;
	/** 司机 */
	private String carOwner;
	/** 联系方式 */
    private String phone;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Long getCarId() {
		return carId;
	}
	public void setCarId(Long carId) {
		this.carId = carId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getCarPlate() {
		return carPlate;
	}
	public void setCarPlate(String carPlate) {
		this.carPlate = carPlate;
	}
	public String getCarOwner() {
		return carOwner;
	}
	public void setCarOwner(String carOwner) {
		this.carOwner = carOwner;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
