package com.sdry.model.llm;

/**
 * 仓库盘点单
 * @Title:LlmStockCheck
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmStockCheck {

	private Long id;
	
	/**创建人ID*/
	private Long userId;
	
	/**创建日期*/
	private String checkDate;
	
	/**盘点单号*/
	private String checkNumber;
	
	/**盘点类型ID*/
	private Long checkTypeId;
	
	/**仓库ID*/
	private Long warehouseId;
	
	/**盘点状态(1:已完成，0:未完成)*/
	private Integer checkStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Long getCheckTypeId() {
		return checkTypeId;
	}

	public void setCheckTypeId(Long checkTypeId) {
		this.checkTypeId = checkTypeId;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(Integer checkStatus) {
		this.checkStatus = checkStatus;
	}

	@Override
	public String toString() {
		return "LlmStockCheck [id=" + id + ", userId=" + userId + ", checkDate=" + checkDate + ", checkNumber="
				+ checkNumber + ", checkTypeId=" + checkTypeId + ", warehouseId=" + warehouseId + ", checkStatus="
				+ checkStatus + "]";
	}
	
}
