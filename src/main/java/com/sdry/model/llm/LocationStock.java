package com.sdry.model.llm;

/**
 * 库位的物料信息
 * @Title:LocationStock
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年4月25日
 * @version 1.0
 */
public class LocationStock {

	//仓库名称
	private String warehouseName;
	
	//库区名称
	private String regionName;
	
	//库位名称
	private String locationName;
	
	//库位状态
	private String locationStatus;
	
	//备注
	private String remark;
	
	//物料名称
	private String materielName;
		
	//物料数量
	private Integer materielQuantity;
	
	//库存数量
	private Integer mNum;
	
	//库位ID
	private Long locationId;
	
	//库区ID
	private Long regionId;
	
	//仓库ID
	private Long warehouseId;
	
	//上限
	private Integer upper;
	
	//下限
	private Integer floor;
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMaterielName() {
		return materielName;
	}

	public void setMaterielName(String materielName) {
		this.materielName = materielName;
	}

	public String getWarehouseName() {
		return warehouseName;
	}

	public void setWarehouseName(String warehouseName) {
		this.warehouseName = warehouseName;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public String getLocationName() {
		return locationName;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public String getLocationStatus() {
		return locationStatus;
	}

	public void setLocationStatus(String locationStatus) {
		this.locationStatus = locationStatus;
	}

	public Integer getMaterielQuantity() {
		return materielQuantity;
	}

	public void setMaterielQuantity(Integer materielQuantity) {
		this.materielQuantity = materielQuantity;
	}

	public Integer getmNum() {
		return mNum;
	}

	public void setmNum(Integer mNum) {
		this.mNum = mNum;
	}

	public Long getLocationId() {
		return locationId;
	}

	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Integer getUpper() {
		return upper;
	}

	public void setUpper(Integer upper) {
		this.upper = upper;
	}

	public Integer getFloor() {
		return floor;
	}

	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	@Override
	public String toString() {
		return "LocationStock [warehouseName=" + warehouseName + ", regionName=" + regionName + ", locationName="
				+ locationName + ", locationStatus=" + locationStatus + ", remark=" + remark + ", materielName="
				+ materielName + ", materielQuantity=" + materielQuantity + ", mNum=" + mNum + ", locationId="
				+ locationId + ", regionId=" + regionId + ", warehouseId=" + warehouseId + ", upper=" + upper
				+ ", floor=" + floor + "]";
	}
	
}
