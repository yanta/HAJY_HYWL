package com.sdry.model.llm;

/**
 * 退库原因
 * @Title:LlmCancellingStockReason
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmCancellingStockReason {

	private Long id;
	
	/**退库原因*/
	private String cancellingReason;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCancellingReason() {
		return cancellingReason;
	}

	public void setCancellingReason(String cancellingReason) {
		this.cancellingReason = cancellingReason;
	}

	@Override
	public String toString() {
		return "LlmCancellingStockReason [id=" + id + ", cancellingReason=" + cancellingReason + "]";
	}
	
}
