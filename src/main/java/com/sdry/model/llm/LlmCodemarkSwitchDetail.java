package	com.sdry.model.llm;

import com.sdry.utils.Page;

/**
 *
 *@ClassName: LlmCodemarkSwitchDetail
 *@Description: 条码类型转换详情
 *@Author llm
 *@Date 2019-08-07 18:53:08
 *@version 1.0
*/
public class LlmCodemarkSwitchDetail extends Page{
	/** null*/
	private Long id;
	/** 上表ID */
	private Long switchId;
	/** 物料ID*/
	private Long mid;
	/** 批次*/
	private String pici;
	public void setId(Long id){
	}
	public Long getId(){
		return id;
	}
	public void setSwitchId(Long switchId){
	}
	public Long getSwitchId(){
		return switchId;
	}
	public void setMid(Long mid){
	}
	public Long getMid(){
		return mid;
	}
	public void setPici(String pici){
	}
	public String getPici(){
		return pici;
	}
}
