package com.sdry.model.llm;

/**
 * 拆箱记录
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年7月29日
 * @version 1.0
 */

public class DevaningLog {

	private Long id;
	//收货单详情数量ID
	private Long detail_id;
	//条码
	private String code;
	//条码对应的数量
	private int quantity;
	//拆箱时间
	private String work_time;
	//拆箱人
	private Long user_id;
	//状态	0：良品，1：不良
	private int is_ng;
	//分类 	0：拆箱前，1：拆箱后
	private int status;
	//拆箱单号（自动生成）
	private String number;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDetail_id() {
		return detail_id;
	}
	public void setDetail_id(Long detail_id) {
		this.detail_id = detail_id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public String getWork_time() {
		return work_time;
	}
	public void setWork_time(String work_time) {
		this.work_time = work_time;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public int getIs_ng() {
		return is_ng;
	}
	public void setIs_ng(int is_ng) {
		this.is_ng = is_ng;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	
}
