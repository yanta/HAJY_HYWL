package	com.sdry.model.llm;
/**
 *
 *@ClassName: LlmCodemarkSwitchLog
 *@Description: 条码类型转换记录
 *@Author llm
 *@Date 2019-08-08 09:23:39
 *@version 1.0
*/
public class LlmCodemarkSwitchLog {
	/** null*/
	private Long id;
	/** 条码转换下表ID */
	private Long detailId;
	/** 操作人*/
	private Long userId;
	/** 操作时间*/
	private String createDate;
	/** 条码*/
	private String code;
	/** 操作类型（上架/下架）*/
	private String operation;
	/** 库位*/
	private String location;
	public void setId(Long id){
	}
	public Long getId(){
		return id;
	}
	public void setDetailId(Long detailId){
	}
	public Long getDetailId(){
		return detailId;
	}
	public void setUserId(Long userId){
	}
	public Long getUserId(){
		return userId;
	}
	public void setCreateDate(String createDate){
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setCode(String code){
	}
	public String getCode(){
		return code;
	}
	public void setOperation(String operation){
	}
	public String getOperation(){
		return operation;
	}
	public void setLocation(String location){
	}
	public String getLocation(){
		return location;
	}
}
