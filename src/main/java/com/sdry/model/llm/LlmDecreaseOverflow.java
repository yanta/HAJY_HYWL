package com.sdry.model.llm;

/**
 * 损溢单
 * @Title:LlmDecreaseOverflow
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmDecreaseOverflow {

	private Long id;
	
	/**损溢单号*/
	private String decreaseOverflowNumber;
	
	/**创建人ID*/
	private Long userId;
	
	/**创建时间*/
	private String createDate;
	
	/**盘点单号*/
	private String stockCheckNumber;
	
	/**损溢类型ID*/
	private Long overflowTypeId;
	
	/**是否审批(-1:未审批，0:审批中，1:已审批)*/
	private Integer approvalStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDecreaseOverflowNumber() {
		return decreaseOverflowNumber;
	}

	public void setDecreaseOverflowNumber(String decreaseOverflowNumber) {
		this.decreaseOverflowNumber = decreaseOverflowNumber;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getStockCheckNumber() {
		return stockCheckNumber;
	}

	public void setStockCheckNumber(String stockCheckNumber) {
		this.stockCheckNumber = stockCheckNumber;
	}

	public Long getOverflowTypeId() {
		return overflowTypeId;
	}

	public void setOverflowTypeId(Long overflowTypeId) {
		this.overflowTypeId = overflowTypeId;
	}

	public Integer getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	@Override
	public String toString() {
		return "LlmDecreaseOverflow [id=" + id + ", decreaseOverflowNumber=" + decreaseOverflowNumber + ", userId="
				+ userId + ", createDate=" + createDate + ", stockCheckNumber=" + stockCheckNumber + ", overflowTypeId="
				+ overflowTypeId + ", approvalStatus=" + approvalStatus + "]";
	}
	
}
