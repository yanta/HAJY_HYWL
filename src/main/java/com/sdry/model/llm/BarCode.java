package com.sdry.model.llm;

/**
 * 退库单详情条码
 * @Title:CancellingStockDetailBarCode
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年4月17日
 * @version 1.0
 */
public class BarCode {

	/* 主键ID */
	private Long bcid;
	
	/*  */
	private Long csdid;
	
	/* 条码 */
	private String barCode;

	/*  */
	private String batchNumber;

	/* 条码绑定数量 */
	private Integer materielQuantity;
	
	/*  */
	private String status;

	/* 物料名称 */
	private String remarks;
	
	/* SAP/QAD */
	private String remark01;
	
	/* 库位名称 */
	private String remark02;
	
	/*  */
	private String remark03;
	
	/* 备注 */
	private String remark04;
	
	/* 备注 */
	private String remark05;

	public BarCode() {
		super();
	}

	public Long getBcid() {
		return bcid;
	}

	public void setBcid(Long bcid) {
		this.bcid = bcid;
	}

	public Long getCsdid() {
		return csdid;
	}

	public void setCsdid(Long csdid) {
		this.csdid = csdid;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getBatchNumber() {
		return batchNumber;
	}

	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}

	public Integer getMaterielQuantity() {
		return materielQuantity;
	}

	public void setMaterielQuantity(Integer materielQuantity) {
		this.materielQuantity = materielQuantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemark01() {
		return remark01;
	}

	public void setRemark01(String remark01) {
		this.remark01 = remark01;
	}

	public String getRemark02() {
		return remark02;
	}

	public void setRemark02(String remark02) {
		this.remark02 = remark02;
	}

	public String getRemark03() {
		return remark03;
	}

	public void setRemark03(String remark03) {
		this.remark03 = remark03;
	}

	public String getRemark04() {
		return remark04;
	}

	public void setRemark04(String remark04) {
		this.remark04 = remark04;
	}

	public String getRemark05() {
		return remark05;
	}

	public void setRemark05(String remark05) {
		this.remark05 = remark05;
	}

	@Override
	public String toString() {
		return "BarCode [bcid=" + bcid + ", csdid=" + csdid + ", barCode=" + barCode
				+ ", batchNumber=" + batchNumber + ", materielQuantity=" + materielQuantity + ", status=" + status
				+ ", remarks=" + remarks + ", remark01=" + remark01 + ", remark02=" + remark02 + ", remark03="
				+ remark03 + ", remark04=" + remark04 + ", remark05=" + remark05 + "]";
	}
	
}
