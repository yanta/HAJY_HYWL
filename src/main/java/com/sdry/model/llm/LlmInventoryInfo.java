package com.sdry.model.llm;

/**
 * 库存统计
 * @Title:LlmInventoryInfo
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年4月23日
 * @version 1.0
 */
public class LlmInventoryInfo {

	//库存ID
    private Long id;
    //物料ID
    private Long mid;
	//物料名称
    private String materiel_name;
    //容器名称
    private String container_name;
	//条码
	private String code;
	//批次
	private String mBatch;
	//数量
	private int mNum;
	//入库日期
	private String enterDate;
	//入库人
	private String enterPerson;
	//最后移动日期
	private String moveDate;
	//最后移动人
	private String movePerson;
	//仓库ID
	private Long warehouseId;
	//库区ID
	private Long regionId;
	//库位ID
	private Long locationId;
	//仓库名称
    private String warehouse_name;
    //库区名称
    private String region_name;
    //库位名称
    private String location_name;
    //仓库编号
    private String warehouse_num;
    //库区编号
    private String region_num;
    //库位编号
    private String location_num;
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getContainer_name() {
		return container_name;
	}
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public int getmNum() {
		return mNum;
	}
	public void setmNum(int mNum) {
		this.mNum = mNum;
	}
	public String getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}
	public String getEnterPerson() {
		return enterPerson;
	}
	public void setEnterPerson(String enterPerson) {
		this.enterPerson = enterPerson;
	}
	public String getMoveDate() {
		return moveDate;
	}
	public void setMoveDate(String moveDate) {
		this.moveDate = moveDate;
	}
	public String getMovePerson() {
		return movePerson;
	}
	public void setMovePerson(String movePerson) {
		this.movePerson = movePerson;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getWarehouse_num() {
		return warehouse_num;
	}
	public void setWarehouse_num(String warehouse_num) {
		this.warehouse_num = warehouse_num;
	}
	public String getRegion_num() {
		return region_num;
	}
	public void setRegion_num(String region_num) {
		this.region_num = region_num;
	}
	public String getLocation_num() {
		return location_num;
	}
	public void setLocation_num(String location_num) {
		this.location_num = location_num;
	}
	
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	@Override
	public String toString() {
		return "LlmInventoryInfo [id=" + id + ", mid=" + mid + ", materiel_name=" + materiel_name + ", container_name="
				+ container_name + ", code=" + code + ", mBatch=" + mBatch + ", mNum=" + mNum + ", enterDate="
				+ enterDate + ", enterPerson=" + enterPerson + ", moveDate=" + moveDate + ", movePerson=" + movePerson
				+ ", warehouseId=" + warehouseId + ", regionId=" + regionId + ", locationId=" + locationId
				+ ", warehouse_name=" + warehouse_name + ", region_name=" + region_name + ", location_name="
				+ location_name + ", warehouse_num=" + warehouse_num + ", region_num=" + region_num + ", location_num="
				+ location_num + "]";
	}
	
}
