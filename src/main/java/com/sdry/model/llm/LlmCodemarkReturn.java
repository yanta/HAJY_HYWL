package com.sdry.model.llm;

/**
 * 退库单条码
 * @Title:Codemark
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年7月22日
 * @version 1.0
 */
public class LlmCodemarkReturn {

	private Long id;
	
	private Long cancelling_stock_detail_id;
	
	private String code;
	
	private int num;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCancelling_stock_detail_id() {
		return cancelling_stock_detail_id;
	}

	public void setCancelling_stock_detail_id(Long cancelling_stock_detail_id) {
		this.cancelling_stock_detail_id = cancelling_stock_detail_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
}
