package com.sdry.model.llm;

/**
 * 拆箱工作量
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年7月31日
 * @version 1.0
 */
public class DevaningWork {

	private Long id;
	//拆箱记录编号
	private String logNumber;
	//拆箱人ID
	private Long userId;
	//拆箱后的箱数
	private int boxQuantity;
	//物料总数
	private int pcsQuantity;
	private String userName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogNumber() {
		return logNumber;
	}
	public void setLogNumber(String logNumber) {
		this.logNumber = logNumber;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public int getBoxQuantity() {
		return boxQuantity;
	}
	public void setBoxQuantity(int boxQuantity) {
		this.boxQuantity = boxQuantity;
	}
	public int getPcsQuantity() {
		return pcsQuantity;
	}
	public void setPcsQuantity(int pcsQuantity) {
		this.pcsQuantity = pcsQuantity;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
