package com.sdry.model.llm;

/**
 * 盘点详情
 * @Title:LlmStockCheckDetail
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月20日
 * @version 1.0
 */
public class LlmStockCheckDetail {

	private Long id;
	/** 盘点单号 */
	private String checkNumber;
	/** 盘点人ID */
	private Long userId;
	/** 盘点日期 */
	private String checkDate;
	/** 物料ID */
	private Long materielId;
	/** 理论数量 */
	private Integer theoryQuantity;
	/** 实际数量 */
	private Integer actualQuantity;
	/** 差异数量 */
	private Integer difference;
	
	private String userName;
	
	private String materiel_name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCheckDate() {
		return checkDate;
	}

	public void setCheckDate(String checkDate) {
		this.checkDate = checkDate;
	}

	public Long getMaterielId() {
		return materielId;
	}

	public void setMaterielId(Long materielId) {
		this.materielId = materielId;
	}

	public Integer getTheoryQuantity() {
		return theoryQuantity;
	}

	public void setTheoryQuantity(Integer theoryQuantity) {
		this.theoryQuantity = theoryQuantity;
	}

	public Integer getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(Integer actualQuantity) {
		this.actualQuantity = actualQuantity;
	}

	public Integer getDifference() {
		return difference;
	}

	public void setDifference(Integer difference) {
		this.difference = difference;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	
}
