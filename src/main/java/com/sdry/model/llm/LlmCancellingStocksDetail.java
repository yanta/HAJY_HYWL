package com.sdry.model.llm;

import java.util.ArrayList;
import java.util.List;

import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Materiel;

/**
 * 退库单详情
 * @Title:LlmCancellingStockDetail
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmCancellingStocksDetail {

	private Long id;
	
	/**退库单号*/
	private String cancellingNumber;
	
	/**物料ID*/
	private Long materialId;
	
	/**物料数量*/
	private Integer materialQuantity;
	
	/**实际数量*/
	private Integer actualQuantity;
	
	/**状态(1：已收货，0：未收货)*/
	private Integer status;
	
	//自定义精准码
	private List<CodeMark> receiveCode = new ArrayList<>();
	
	private Materiel materiel;
	//物料名称
	private String materiel_name;
	//产品码
	private String materiel_num;
	//物料简码
	private String brevity_num;
	//物料规格
	private String materiel_size;
	//物料型号
	private String materiel_properties;
	private String detailStatus;

	//单个数量
	private int single_num;
	//大箱数量
	private int big_num;
	//小箱数量
	private int small_num;
	private int goodQuantity;
	private int badQuantity;
	private int sumQuantity;
	private String storageLocation;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCancellingNumber() {
		return cancellingNumber;
	}

	public void setCancellingNumber(String cancellingNumber) {
		this.cancellingNumber = cancellingNumber;
	}

	public Long getMaterialId() {
		return materialId;
	}

	public void setMaterialId(Long materialId) {
		this.materialId = materialId;
	}

	public Integer getMaterialQuantity() {
		return materialQuantity;
	}

	public void setMaterialQuantity(Integer materialQuantity) {
		this.materialQuantity = materialQuantity;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Integer getActualQuantity() {
		return actualQuantity;
	}

	public void setActualQuantity(Integer actualQuantity) {
		this.actualQuantity = actualQuantity;
	}
	
	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	
	public String getDetailStatus() {
		return detailStatus;
	}

	public void setDetailStatus(String detailStatus) {
		this.detailStatus = detailStatus;
	}
	
	public Materiel getMateriel() {
		return materiel;
	}

	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}

	public String getMateriel_num() {
		return materiel_num;
	}

	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}

	public String getBrevity_num() {
		return brevity_num;
	}

	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}

	public String getMateriel_size() {
		return materiel_size;
	}

	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}

	public String getMateriel_properties() {
		return materiel_properties;
	}

	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}

	public int getSingle_num() {
		return single_num;
	}

	public void setSingle_num(int single_num) {
		this.single_num = single_num;
	}

	public int getBig_num() {
		return big_num;
	}

	public void setBig_num(int big_num) {
		this.big_num = big_num;
	}

	public int getSmall_num() {
		return small_num;
	}

	public void setSmall_num(int small_num) {
		this.small_num = small_num;
	}

	public List<CodeMark> getReceiveCode() {
		return receiveCode;
	}

	public void setReceiveCode(List<CodeMark> receiveCode) {
		this.receiveCode = receiveCode;
	}

	public int getGoodQuantity() {
		return goodQuantity;
	}

	public void setGoodQuantity(int goodQuantity) {
		this.goodQuantity = goodQuantity;
	}

	public int getBadQuantity() {
		return badQuantity;
	}

	public void setBadQuantity(int badQuantity) {
		this.badQuantity = badQuantity;
	}

	public int getSumQuantity() {
		return sumQuantity;
	}

	public void setSumQuantity(int sumQuantity) {
		this.sumQuantity = sumQuantity;
	}

	public String getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(String storageLocation) {
		this.storageLocation = storageLocation;
	}

	@Override
	public String toString() {
		return "LlmCancellingStocksDetail [id=" + id + ", cancellingNumber=" + cancellingNumber + ", materialId="
				+ materialId + ", materialQuantity=" + materialQuantity + ", actualQuantity=" + actualQuantity
				+ ", status=" + status + ", receiveCode=" + receiveCode + ", materiel=" + materiel + ", materiel_name="
				+ materiel_name + ", materiel_num=" + materiel_num + ", brevity_num=" + brevity_num + ", materiel_size="
				+ materiel_size + ", materiel_properties=" + materiel_properties + ", detailStatus=" + detailStatus
				+ ", single_num=" + single_num + ", big_num=" + big_num + ", small_num=" + small_num + ", goodQuantity="
				+ goodQuantity + ", badQuantity=" + badQuantity + ", sumQuantity=" + sumQuantity + "]";
	}
	
}
