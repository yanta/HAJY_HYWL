package com.sdry.model.llm;

/**
 * 损溢类型
 * @Title:LlmDecreaseOverflowType
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmDecreaseOverflowType {

	private Long id;
	
	/**损溢类型*/
	private String overflowType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOverflowType() {
		return overflowType;
	}

	public void setOverflowType(String overflowType) {
		this.overflowType = overflowType;
	}

	@Override
	public String toString() {
		return "LlmDecreaseOverflowType [id=" + id + ", overflowType=" + overflowType + "]";
	}
	
}
