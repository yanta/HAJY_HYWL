package	com.sdry.model.llm;

import com.sdry.utils.Page;

/**
 *@Description: 发货单和出库单的中间表
 *@Author llm
 *@Date 2019-07-17 16:39:44
 *@version 1.0
*/
public class DeliveryOutStock extends Page{
	/** 主键ID */
	private Long id;
	/** 送货单ID */
	private Long deliveryId;
	/** 出库单ID */
	private Long outStockId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	public Long getOutStockId() {
		return outStockId;
	}
	public void setOutStockId(Long outStockId) {
		this.outStockId = outStockId;
	}
	
}
