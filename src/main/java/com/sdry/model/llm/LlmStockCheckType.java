package com.sdry.model.llm;

/**
 * 仓库盘点类型
 * @Title:LlmStockCheckType
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmStockCheckType {

	private Long id;
	
	/**盘点类型*/
	private String checkType;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCheckType() {
		return checkType;
	}

	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}

	@Override
	public String toString() {
		return "LlmStockCheckType [id=" + id + ", checkType=" + checkType + "]";
	}
	
}
