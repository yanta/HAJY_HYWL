package com.sdry.model.llm;

/**
 * 退库单
 * @Title:LlmCancellingStock
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmCancellingStocks {

	private Long id;
	
	/**退库单号*/
	private String cancellingNumber;
	
	/**创建日期*/
	private String createDate;
	
	/**退库原因ID*/
	private Long reasonId;
	
	/**仓库类型（1：良品库，0：不良品库）*/
	private Integer source;
	
	/**仓库ID*/
	private Long warehouseId;
	
	/**供应商ID*/
	private Long clientId;
	
	/**创建人ID*/
	private Long userId;
	
	/**状态(1：已收货，0：未收货)*/
	private Integer status;

	//客户单号
	private String customer_order;
	private String warehouse_name;
	private String warehouse_address;
	private String userName;
	private String phone;
	private String contacts_name;
	private String contacts_tel;
	private String customer_adress;
	private String customer_name;
	private String cancellingStatus;
	private String cancellingReason;
	//erica
	private String remark;
	private String flag;
	private String cancellation;
	private String cancellation_time;
	private String restorer ;
	private String restorer_time;
	
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCancellation() {
		return cancellation;
	}

	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}

	public String getCancellation_time() {
		return cancellation_time;
	}

	public void setCancellation_time(String cancellation_time) {
		this.cancellation_time = cancellation_time;
	}

	public String getRestorer() {
		return restorer;
	}

	public void setRestorer(String restorer) {
		this.restorer = restorer;
	}

	public String getRestorer_time() {
		return restorer_time;
	}

	public void setRestorer_time(String restorer_time) {
		this.restorer_time = restorer_time;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCancellingNumber() {
		return cancellingNumber;
	}

	public void setCancellingNumber(String cancellingNumber) {
		this.cancellingNumber = cancellingNumber;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public Long getReasonId() {
		return reasonId;
	}

	public void setReasonId(Long reasonId) {
		this.reasonId = reasonId;
	}

	public Integer getSource() {
		return source;
	}

	public void setSource(Integer source) {
		this.source = source;
	}

	public Long getWarehouseId() {
		return warehouseId;
	}

	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public String getWarehouse_address() {
		return warehouse_address;
	}

	public void setWarehouse_address(String warehouse_address) {
		this.warehouse_address = warehouse_address;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getContacts_name() {
		return contacts_name;
	}

	public void setContacts_name(String contacts_name) {
		this.contacts_name = contacts_name;
	}

	public String getContacts_tel() {
		return contacts_tel;
	}

	public void setContacts_tel(String contacts_tel) {
		this.contacts_tel = contacts_tel;
	}
	
	public String getCustomer_adress() {
		return customer_adress;
	}

	public void setCustomer_adress(String customer_adress) {
		this.customer_adress = customer_adress;
	}
	
	public String getWarehouse_name() {
		return warehouse_name;
	}

	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	
	public String getCancellingStatus() {
		return cancellingStatus;
	}

	public void setCancellingStatus(String cancellingStatus) {
		this.cancellingStatus = cancellingStatus;
	}

	public String getCancellingReason() {
		return cancellingReason;
	}

	public void setCancellingReason(String cancellingReason) {
		this.cancellingReason = cancellingReason;
	}

	public String getCustomer_order() {
		return customer_order;
	}

	public void setCustomer_order(String customer_order) {
		this.customer_order = customer_order;
	}

	@Override
	public String toString() {
		return "LlmCancellingStocks [id=" + id + ", cancellingNumber=" + cancellingNumber + ", createDate=" + createDate
				+ ", reasonId=" + reasonId + ", source=" + source + ", warehouseId=" + warehouseId + ", clientId="
				+ clientId + ", userId=" + userId + ", status=" + status + "]";
	}
	
}
