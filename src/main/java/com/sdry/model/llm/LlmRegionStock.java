package com.sdry.model.llm;

import java.util.List;

import com.sdry.model.lz.WarehouseRegionLocation;

/**
 * 
 * @Title:LlmRegionStock
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年4月26日
 * @version 1.0
 */
public class LlmRegionStock {

	//id
    private Long id;
    //仓库id
    private Long warehouse_id;
    //仓库名称
    private String warehouse_name;
    //库区名称
    private String region_name;
    //库区编号
    private String region_num;
    //库区属性(1、货架 2、平地)
    private Long region_type;
    //库区频率
    private String region_frequency;
    //库区状态
    private String region_status;
    //备注
    private String remark;
    //库位集合
    private List<WarehouseRegionLocation> locationList;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getWarehouse_id() {
		return warehouse_id;
	}
	public void setWarehouse_id(Long warehouse_id) {
		this.warehouse_id = warehouse_id;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getRegion_num() {
		return region_num;
	}
	public void setRegion_num(String region_num) {
		this.region_num = region_num;
	}
	public Long getRegion_type() {
		return region_type;
	}
	public void setRegion_type(Long region_type) {
		this.region_type = region_type;
	}
	public String getRegion_frequency() {
		return region_frequency;
	}
	public void setRegion_frequency(String region_frequency) {
		this.region_frequency = region_frequency;
	}
	public String getRegion_status() {
		return region_status;
	}
	public void setRegion_status(String region_status) {
		this.region_status = region_status;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<WarehouseRegionLocation> getLocationList() {
		return locationList;
	}
	public void setLocationList(List<WarehouseRegionLocation> locationList) {
		this.locationList = locationList;
	}
    
}
