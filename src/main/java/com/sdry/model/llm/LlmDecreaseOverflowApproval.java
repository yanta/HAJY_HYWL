package com.sdry.model.llm;

/**
 * 损溢单审批
 * @Title:LlmDecreaseOverflowApproval
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年5月13日
 * @version 1.0
 */
public class LlmDecreaseOverflowApproval {

	private Long id;
	
	/**损溢单号*/
	private String overflowNumber;
	
	/**审批人ID*/
	private Long userId;
	
	/**审批日期*/
	private String approvalDate;
	
	/**审批意见*/
	private String approvalOpinion;
	
	/**审批状态(1:同意，0:驳回)*/
	private Integer approvalStatus;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOverflowNumber() {
		return overflowNumber;
	}

	public void setOverflowNumber(String overflowNumber) {
		this.overflowNumber = overflowNumber;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getApprovalDate() {
		return approvalDate;
	}

	public void setApprovalDate(String approvalDate) {
		this.approvalDate = approvalDate;
	}

	public String getApprovalOpinion() {
		return approvalOpinion;
	}

	public void setApprovalOpinion(String approvalOpinion) {
		this.approvalOpinion = approvalOpinion;
	}

	public Integer getApprovalStatus() {
		return approvalStatus;
	}

	public void setApprovalStatus(Integer approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	@Override
	public String toString() {
		return "LlmDecreaseOverflowApproval [id=" + id + ", overflowNumber=" + overflowNumber + ", userId=" + userId
				+ ", approvalDate=" + approvalDate + ", approvalOpinion=" + approvalOpinion + ", approvalStatus="
				+ approvalStatus + "]";
	}
	
}
