package	com.sdry.model.llm;

import com.sdry.utils.Page;

/**
 *
 *@ClassName: LlmCodemarkSwitch
 *@Description: 条码类型转换
 *@Author llm
 *@Date 2019-08-07 18:36:43
 *@version 1.0
*/
public class LlmCodemarkSwitch extends Page{
	/** null*/
	private Long id;
	/** 单号 */
	private String number;
	/** 创建人*/
	private Long userId;
	/** 创建时间*/
	private String createDate;
	/** 确认人*/
	private Long confirmor;
	/** 确认时间*/
	private String confirmorDate;
	/** 审批人 */
	private Long approvalId;
	/** 审批时间*/
	private String approvalDate;
	/** 状态，0:待审批，1：待确认，2：已确认*/
	private int status;
	/** 类型，0:良转不良，1：不良转良*/
	private int type;
	public void setId(Long id){
		this.id=id;
	}
	public Long getId(){
		return id;
	}
	public void setNumber(String number){
		this.number = number;
	}
	public String getNumber(){
		return number;
	}
	public void setUserId(Long userId){
		this.userId = userId;
	}
	public Long getUserId(){
		return userId;
	}
	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public void setConfirmor(Long confirmor){
		this.confirmor = confirmor;
	}
	public Long getConfirmor(){
		return confirmor;
	}
	public void setConfirmorDate(String confirmorDate){
		this.confirmorDate = confirmorDate;
	}
	public String getConfirmorDate(){
		return confirmorDate;
	}
	public void setApprovalId(Long approvalId){
		this.approvalId = approvalId;
	}
	public Long getApprovalId(){
		return approvalId;
	}
	public void setApprovalDate(String approvalDate){
		this.approvalDate = approvalDate;
	}
	public String getApprovalDate(){
		return approvalDate;
	}
	public void setStatus(int status){
		this.status = status;
	}
	public int getStatus(){
		return status;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
