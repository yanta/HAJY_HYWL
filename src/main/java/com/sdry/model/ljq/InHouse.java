package com.sdry.model.ljq;

public class InHouse {
	
	
	
	/** id*/
	private Long id;
	/** 入库单号*/
	private String come_number;
	/** 入库时间*/
	private String come_date;
	/** 入库人*/
	private String come_name;
	/** 备注*/
	private String remark;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCome_number() {
		return come_number;
	}
	public void setCome_number(String come_number) {
		this.come_number = come_number;
	}
	public String getCome_date() {
		return come_date;
	}
	public void setCome_date(String come_date) {
		this.come_date = come_date;
	}
	public String getCome_name() {
		return come_name;
	}
	public void setCome_name(String come_name) {
		this.come_name = come_name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "InHouse [id=" + id + ", come_number=" + come_number + ", come_date=" + come_date + ", come_name="
				+ come_name + ", remark=" + remark + "]";
	}
	

}
