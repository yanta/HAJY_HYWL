package com.sdry.model.ljq;
/**
  * @ClassName WaitSendArea.java
  * @Description  待发货区表
  * @author LJQ
  * @Date 2019年5月6日 下午5:08:18
  * @Version 1.0
  *
  */
public class LjqWaitSendArea {
	
	//待发货ID
	private long id;
	//物料ID
	private long mid;
	//条码
	private String code;
	//批次
	private String mBatch;
	//数量
	private long mNum;
	//拆箱人
	private String enterPerson;
	//拆箱时间
	private String moveDate;
	
	//物料名称
	private String materiel_name;
	//产品码
	private String materiel_num;
	//简码
	private String brevity_num;
	//包装数量
	private String packing_quantity;
	//单位
	private String unit;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMid() {
		return mid;
	}
	public void setMid(long mid) {
		this.mid = mid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public long getmNum() {
		return mNum;
	}
	public void setmNum(long mNum) {
		this.mNum = mNum;
	}
	public String getEnterPerson() {
		return enterPerson;
	}
	public void setEnterPerson(String enterPerson) {
		this.enterPerson = enterPerson;
	}
	public String getMoveDate() {
		return moveDate;
	}
	public void setMoveDate(String moveDate) {
		this.moveDate = moveDate;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	@Override
	public String toString() {
		return "WaitSendArea [id=" + id + ", mid=" + mid + ", code=" + code + ", mBatch=" + mBatch + ", mNum=" + mNum
				+ ", enterPerson=" + enterPerson + ", moveDate=" + moveDate + ", materiel_name=" + materiel_name
				+ ", materiel_num=" + materiel_num + ", brevity_num=" + brevity_num + ", packing_quantity="
				+ packing_quantity + ", unit=" + unit + "]";
	}
	
	
	
}

