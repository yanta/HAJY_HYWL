package com.sdry.model.ljq;

import java.util.Date;

public class MoveHouse {

	//id
	private long id;
	//物料ID
	private long mid;
	//原仓库
	private long srcWarehouseId;
	//原仓库
	private long srcRegionId;
	//原仓库
	private long srcLocationId;
	//库存
	private Integer mNum;
	//移动总数
	private Integer totality;
	//原仓库
	private long warehouseId;
	//原仓库
	private long regionId;
	//原仓库
	private long locationId;
	//移动日期
	private Date moveDate;
	//移动人
	private String movePerson;
	
	
	private String materiel_num;
	private String materiel_name;
	private String srcWarehouse_name;
	private String srcRegion_name;
	private String srcLocation_name;
	private String warehouse_name;
	private String region_name;
	private String location_name;
	
	private String move_date;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getMid() {
		return mid;
	}
	public void setMid(long mid) {
		this.mid = mid;
	}
	public long getSrcWarehouseId() {
		return srcWarehouseId;
	}
	public void setSrcWarehouseId(long srcWarehouseId) {
		this.srcWarehouseId = srcWarehouseId;
	}
	public long getSrcRegionId() {
		return srcRegionId;
	}
	public void setSrcRegionId(long srcRegionId) {
		this.srcRegionId = srcRegionId;
	}
	public long getSrcLocationId() {
		return srcLocationId;
	}
	public void setSrcLocationId(long srcLocationId) {
		this.srcLocationId = srcLocationId;
	}
	public Integer getmNum() {
		return mNum;
	}
	public void setmNum(Integer mNum) {
		this.mNum = mNum;
	}
	public Integer getTotality() {
		return totality;
	}
	public void setTotality(Integer totality) {
		this.totality = totality;
	}
	public long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public long getRegionId() {
		return regionId;
	}
	public void setRegionId(long regionId) {
		this.regionId = regionId;
	}
	public long getLocationId() {
		return locationId;
	}
	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}
	public Date getMoveDate() {
		return moveDate;
	}
	public void setMoveDate(Date moveDate) {
		this.moveDate = moveDate;
	}
	public String getMovePerson() {
		return movePerson;
	}
	public void setMovePerson(String movePerson) {
		this.movePerson = movePerson;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getSrcWarehouse_name() {
		return srcWarehouse_name;
	}
	public void setSrcWarehouse_name(String srcWarehouse_name) {
		this.srcWarehouse_name = srcWarehouse_name;
	}
	public String getSrcRegion_name() {
		return srcRegion_name;
	}
	public void setSrcRegion_name(String srcRegion_name) {
		this.srcRegion_name = srcRegion_name;
	}
	public String getSrcLocation_name() {
		return srcLocation_name;
	}
	public void setSrcLocation_name(String srcLocation_name) {
		this.srcLocation_name = srcLocation_name;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getMove_date() {
		return move_date;
	}
	public void setMove_date(String move_date) {
		this.move_date = move_date;
	}
	
	
}
