package com.sdry.model.ljq;

import java.util.Date;

/**
 * 物料详情
 * @author Administrator
 *
 */
public class MaterielInfo {

	//物料ID
	private long id;
	//容器ID
	private long container_id;
	//所属客户
	private String client;
	//物料名称
	private String materiel_name;
	//产品码
	private String materiel_num;
	//简码
	private String brevity_num;
	//保质期
	private String quality_guarantee_period;
	//允许存放天数
	private String allowable_storage_days;
	//单位
	private String unit;
	//出厂日期
	private Date out_date;
	//收货地址
	private String delivery_address;
	//出库频率
	private String outgoing_frequency;
	//入库类型
	private String storage_age;
	//放置类型
	private String placement_type;
	
	
	//仓库名称
	private String warehouse_name;
	//库区名称
	private String region_name;
	//库位名称 
	private String location_name;
	//容器名称
	private String container_name;
	//出厂日期
	private String outDate;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getContainer_id() {
		return container_id;
	}
	public void setContainer_id(long container_id) {
		this.container_id = container_id;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getQuality_guarantee_period() {
		return quality_guarantee_period;
	}
	public void setQuality_guarantee_period(String quality_guarantee_period) {
		this.quality_guarantee_period = quality_guarantee_period;
	}
	public String getAllowable_storage_days() {
		return allowable_storage_days;
	}
	public void setAllowable_storage_days(String allowable_storage_days) {
		this.allowable_storage_days = allowable_storage_days;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public Date getOut_date() {
		return out_date;
	}
	public void setOut_date(Date out_date) {
		this.out_date = out_date;
	}
	public String getDelivery_address() {
		return delivery_address;
	}
	public void setDelivery_address(String delivery_address) {
		this.delivery_address = delivery_address;
	}
	public String getOutgoing_frequency() {
		return outgoing_frequency;
	}
	public void setOutgoing_frequency(String outgoing_frequency) {
		this.outgoing_frequency = outgoing_frequency;
	}
	public String getStorage_age() {
		return storage_age;
	}
	public void setStorage_age(String storage_age) {
		this.storage_age = storage_age;
	}
	public String getPlacement_type() {
		return placement_type;
	}
	public void setPlacement_type(String placement_type) {
		this.placement_type = placement_type;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	public String getContainer_name() {
		return container_name;
	}
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}
	public String getOutDate() {
		return outDate;
	}
	public void setOutDate(String outDate) {
		this.outDate = outDate;
	}
	
}
