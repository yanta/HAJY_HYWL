package com.sdry.model.ljq;

/**
 * 分页
 * @author Administrator
 *
 */
public class Page {
	
	
	private Integer page;
	private Integer limit;
	
	
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	
	

}
