package com.sdry.model.ljq;

public class ReceivingRecord {
	
	
	/**id*/
	private Long id;
	/**物料id*/
	private Long mid;
	/**物料名称*/
	private String materiel_name;
	/**单品数量*/
	private int single_num;
	/**单位数量*/
	private int unit_num;
	/**容器名称*/
	private String container;
	/**容器数量*/
	private int container_num;
	/**不良数量*/
	private int nogood_num;
	/**入库数量*/
	private int come_num;
	/**备注*/
	private String remark;
	/**收货单明细id*/
	private Long rdid;
	/**批次*/
	private String pici;
	/**入库单号*/
	private String come_number;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public int getSingle_num() {
		return single_num;
	}
	public void setSingle_num(int single_num) {
		this.single_num = single_num;
	}
	public int getUnit_num() {
		return unit_num;
	}
	public void setUnit_num(int unit_num) {
		this.unit_num = unit_num;
	}
	public String getContainer() {
		return container;
	}
	public void setContainer(String container) {
		this.container = container;
	}
	public int getContainer_num() {
		return container_num;
	}
	public void setContainer_num(int container_num) {
		this.container_num = container_num;
	}
	public int getNogood_num() {
		return nogood_num;
	}
	public void setNogood_num(int nogood_num) {
		this.nogood_num = nogood_num;
	}
	public int getCome_num() {
		return come_num;
	}
	public void setCome_num(int come_num) {
		this.come_num = come_num;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getRdid() {
		return rdid;
	}
	public void setRdid(Long rdid) {
		this.rdid = rdid;
	}
	public String getPici() {
		return pici;
	}
	public void setPici(String pici) {
		this.pici = pici;
	}
	public String getCome_number() {
		return come_number;
	}
	public void setCome_number(String come_number) {
		this.come_number = come_number;
	}
	@Override
	public String toString() {
		return "ReceivingRecord [id=" + id + ", mid=" + mid + ", materiel_name=" + materiel_name + ", single_num="
				+ single_num + ", unit_num=" + unit_num + ", container=" + container + ", container_num="
				+ container_num + ", nogood_num=" + nogood_num + ", come_num=" + come_num + ", remark=" + remark
				+ ", rdid=" + rdid + ", pici=" + pici + ", come_number=" + come_number + "]";
	}

}
