package com.sdry.model.ljq;

import java.util.Date;

/**
 * 补货记录
 * @author Administrator
 *
 */
public class ReplenishGoods {

	//id
	private long id;
	//出库单号
	private String out_stock_order_num;
	//补发日期
	private Date replenish_time;
	//补发人员
	private String replenish_man;
	//补发原因
	private String replenish_reason;
	//备注
	private String remark;
	
	private String replenishTime;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOut_stock_order_num() {
		return out_stock_order_num;
	}

	public void setOut_stock_order_num(String out_stock_order_num) {
		this.out_stock_order_num = out_stock_order_num;
	}

	public Date getReplenish_time() {
		return replenish_time;
	}

	public void setReplenish_time(Date replenish_time) {
		this.replenish_time = replenish_time;
	}

	public String getReplenish_man() {
		return replenish_man;
	}

	public void setReplenish_man(String replenish_man) {
		this.replenish_man = replenish_man;
	}

	public String getReplenish_reason() {
		return replenish_reason;
	}

	public void setReplenish_reason(String replenish_reason) {
		this.replenish_reason = replenish_reason;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getReplenishTime() {
		return replenishTime;
	}

	public void setReplenishTime(String replenishTime) {
		this.replenishTime = replenishTime;
	}
	
}
