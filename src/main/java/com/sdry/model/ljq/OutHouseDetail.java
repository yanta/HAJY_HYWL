package com.sdry.model.ljq;

import java.util.Date;

/**
 * 出库单详情
 * @author Administrator
 *
 */
public class OutHouseDetail {

	//id
	private long id;
	//出库单ID
	private long order_id;
	//需求数量
	private long required_out_quantity;
	//实际数量
	private long actual_quantity;
	//物料名称
	private String materiel_name;
	//产品码
	private String materiel_num;
	//简码
	private String brevity_num;
	//包装数量
	private String packing_quantity;
	//单位
	private String unit;
	//批次
	private String mBatch;
	//数量
	private String mNum;
	//仓库名称
	private String warehouse_name;
	//库区
	private String region_name;
	//库位
	private String location_name;
	//入库日期
	private Date time;
	//入库人
	private String enterPerson;
	
	
	private String enterTime;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public long getOrder_id() {
		return order_id;
	}


	public void setOrder_id(long order_id) {
		this.order_id = order_id;
	}


	public long getRequired_out_quantity() {
		return required_out_quantity;
	}


	public void setRequired_out_quantity(long required_out_quantity) {
		this.required_out_quantity = required_out_quantity;
	}


	public long getActual_quantity() {
		return actual_quantity;
	}


	public void setActual_quantity(long actual_quantity) {
		this.actual_quantity = actual_quantity;
	}


	public String getMateriel_name() {
		return materiel_name;
	}


	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}


	public String getMateriel_num() {
		return materiel_num;
	}


	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}


	public String getBrevity_num() {
		return brevity_num;
	}


	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}


	public String getPacking_quantity() {
		return packing_quantity;
	}


	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}


	public String getUnit() {
		return unit;
	}


	public void setUnit(String unit) {
		this.unit = unit;
	}


	public String getmBatch() {
		return mBatch;
	}


	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}


	public String getmNum() {
		return mNum;
	}


	public void setmNum(String mNum) {
		this.mNum = mNum;
	}


	public String getWarehouse_name() {
		return warehouse_name;
	}


	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}


	public String getRegion_name() {
		return region_name;
	}


	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}


	public String getLocation_name() {
		return location_name;
	}


	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}


	public Date getTime() {
		return time;
	}


	public void setTime(Date time) {
		this.time = time;
	}


	public String getEnterPerson() {
		return enterPerson;
	}


	public void setEnterPerson(String enterPerson) {
		this.enterPerson = enterPerson;
	}


	public String getEnterTime() {
		return enterTime;
	}


	public void setEnterTime(String enterTime) {
		this.enterTime = enterTime;
	}
	
	
	
}
