package com.sdry.model.ljq;

public class Upper {

	
		//主键ID
		private Long id;
		//入库单详情ID
		private Long cdid;
		//仓库ID
		private Long warehouseId;
		//库区ID
		private Long regionId;
		//库位ID
		private Long locationId;
		//上架时间
		private String update;
		//上架人
		private String upname;
		//备注
		private String remark;
		//仓库名称
		private String warehouse_name;
		//库区名称
		private String region_name;
		//库位名称
		private String location_name;
		
		//物料名称
		private String materiel_name;
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public Long getCdid() {
			return cdid;
		}
		public void setCdid(Long cdid) {
			this.cdid = cdid;
		}
		public Long getWarehouseId() {
			return warehouseId;
		}
		public void setWarehouseId(Long warehouseId) {
			this.warehouseId = warehouseId;
		}
		public Long getRegionId() {
			return regionId;
		}
		public void setRegionId(Long regionId) {
			this.regionId = regionId;
		}
		public Long getLocationId() {
			return locationId;
		}
		public void setLocationId(Long locationId) {
			this.locationId = locationId;
		}
		public String getUpdate() {
			return update;
		}
		public void setUpdate(String update) {
			this.update = update;
		}
		public String getUpname() {
			return upname;
		}
		public void setUpname(String upname) {
			this.upname = upname;
		}
		public String getRemark() {
			return remark;
		}
		public void setRemark(String remark) {
			this.remark = remark;
		}
		public String getWarehouse_name() {
			return warehouse_name;
		}
		public void setWarehouse_name(String warehouse_name) {
			this.warehouse_name = warehouse_name;
		}
		public String getRegion_name() {
			return region_name;
		}
		public void setRegion_name(String region_name) {
			this.region_name = region_name;
		}
		public String getLocation_name() {
			return location_name;
		}
		public void setLocation_name(String location_name) {
			this.location_name = location_name;
		}
		@Override
		public String toString() {
			return "Upper [id=" + id + ", cdid=" + cdid + ", warehouseId=" + warehouseId + ", regionId=" + regionId
					+ ", locationId=" + locationId + ", update=" + update + ", upname=" + upname + ", remark=" + remark
					+ ", warehouse_name=" + warehouse_name + ", region_name=" + region_name + ", location_name="
					+ location_name + "]";
		}
		public String getMateriel_name() {
			return materiel_name;
		}
		public void setMateriel_name(String materiel_name) {
			this.materiel_name = materiel_name;
		}
}
