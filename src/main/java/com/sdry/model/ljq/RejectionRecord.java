package com.sdry.model.ljq;

public class RejectionRecord {

	
	//退货id
	private long id ;
	//退货单号
	private String rnumber;
	//收货单号
	private String snumber;
	//发货单号
	private String fnumber;
	//供货商
	private String custom;
	//退货日期
	private String rdate;
	
	
	//物料ID
	private String mid;
	//容器
	private String content;
	//容器数量
	private String num;
	//退货数量
	private String tnum;
	//退货原因
	private String reson;
	//备注
	private String remark;
	
	//物料名称
	private String materiel_name;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRnumber() {
		return rnumber;
	}

	public void setRnumber(String rnumber) {
		this.rnumber = rnumber;
	}

	public String getSnumber() {
		return snumber;
	}

	public void setSnumber(String snumber) {
		this.snumber = snumber;
	}

	public String getFnumber() {
		return fnumber;
	}

	public void setFnumber(String fnumber) {
		this.fnumber = fnumber;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public String getRdate() {
		return rdate;
	}

	public void setRdate(String rdate) {
		this.rdate = rdate;
	}

	public String getMid() {
		return mid;
	}

	public void setMid(String mid) {
		this.mid = mid;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getNum() {
		return num;
	}

	public void setNum(String num) {
		this.num = num;
	}

	public String getTnum() {
		return tnum;
	}

	public void setTnum(String tnum) {
		this.tnum = tnum;
	}

	public String getReson() {
		return reson;
	}

	public void setReson(String reson) {
		this.reson = reson;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	
	
}
