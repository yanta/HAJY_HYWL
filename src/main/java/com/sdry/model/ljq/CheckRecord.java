package com.sdry.model.ljq;

public class CheckRecord {

	/** id*/
	private Long id;
	/** 创建人*/
	private String create_name;
	/** 创建时间*/
	private String create_date;
	/** 质检单号*/
	private String check_number;
	/** 物料id*/
	private String mid;
	/** 物料名称*/
	private String materiel_name;
	/** 批次*/
	private String pici;
	/** 质检数量*/
	private int cnum;
	/** 破损数量*/
	private int snum;
	/** 合格数量*/
	private int hnum;
	/** 注释*/
	private String remark;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCreate_name() {
		return create_name;
	}
	public void setCreate_name(String create_name) {
		this.create_name = create_name;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getCheck_number() {
		return check_number;
	}
	public void setCheck_number(String check_number) {
		this.check_number = check_number;
	}
	public String getMid() {
		return mid;
	}
	public void setMid(String mid) {
		this.mid = mid;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getPici() {
		return pici;
	}
	public void setPici(String pici) {
		this.pici = pici;
	}
	public int getCnum() {
		return cnum;
	}
	public void setCnum(int cnum) {
		this.cnum = cnum;
	}
	public int getSnum() {
		return snum;
	}
	public void setSnum(int snum) {
		this.snum = snum;
	}
	public int getHnum() {
		return hnum;
	}
	public void setHnum(int hnum) {
		this.hnum = hnum;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	@Override
	public String toString() {
		return "CheckRecord [id=" + id + ", create_name=" + create_name + ", create_date=" + create_date
				+ ", check_number=" + check_number + ", mid=" + mid + ", materiel_name=" + materiel_name + ", pici="
				+ pici + ", cnum=" + cnum + ", snum=" + snum + ", hnum=" + hnum + ", remark=" + remark + "]";
	}
	
}
