package com.sdry.model.ljq;

import java.util.Date;

/**
 * 出库单
 * @author Administrator
 *
 */
public class OutHouse {

	//id
	private long id;
	//出库单号
	private String out_order_num;
	//出库类型
	private String out_stock_type;
	//出库状态
	private String out_stock_status;
	//客户
	private String customer;
	//地址
	private String adddress;
	//创建人
	private String creater;
	//创建时间
	private Date creater_date;
	//备注
	private String remark01;
	//备注
	private String remark02;
	//
	private String remark03;
	//备注
	private String remark04;
	//备注
	private String remark05;
	
	private String createrDate;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOut_order_num() {
		return out_order_num;
	}

	public void setOut_order_num(String out_order_num) {
		this.out_order_num = out_order_num;
	}

	public String getOut_stock_type() {
		return out_stock_type;
	}

	public void setOut_stock_type(String out_stock_type) {
		this.out_stock_type = out_stock_type;
	}

	public String getOut_stock_status() {
		return out_stock_status;
	}

	public void setOut_stock_status(String out_stock_status) {
		this.out_stock_status = out_stock_status;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public String getAdddress() {
		return adddress;
	}

	public void setAdddress(String adddress) {
		this.adddress = adddress;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreater_date() {
		return creater_date;
	}

	public void setCreater_date(Date creater_date) {
		this.creater_date = creater_date;
	}

	public String getRemark01() {
		return remark01;
	}

	public void setRemark01(String remark01) {
		this.remark01 = remark01;
	}

	public String getRemark02() {
		return remark02;
	}

	public void setRemark02(String remark02) {
		this.remark02 = remark02;
	}

	public String getRemark03() {
		return remark03;
	}

	public void setRemark03(String remark03) {
		this.remark03 = remark03;
	}

	public String getRemark04() {
		return remark04;
	}

	public void setRemark04(String remark04) {
		this.remark04 = remark04;
	}

	public String getRemark05() {
		return remark05;
	}

	public void setRemark05(String remark05) {
		this.remark05 = remark05;
	}

	public String getCreaterDate() {
		return createrDate;
	}

	public void setCreaterDate(String createrDate) {
		this.createrDate = createrDate;
	}
		
}
