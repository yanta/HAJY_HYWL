package com.sdry.model.ljq;

import java.util.List;

public class XtreeUtil {
	
	
	private String title;
	private String value;
	private List<XtreeUtil> data;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public List<XtreeUtil> getData() {
		return data;
	}
	public void setData(List<XtreeUtil> data) {
		this.data = data;
	}
	

}
