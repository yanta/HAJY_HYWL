package com.sdry.model.jyy;


/**
*
*@ClassName: Receive
*@Description: 收货计划
*@Author jyy
*@Date 2019-04-17 14:50:45
*@version 1.0
*/
public class Receive {

	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	private Long id ;
	/**收货单号*/
	private String receiveNumber;
	/**发货单号*/
	private String sendNumber;
	/**发货单位*/
	private String sendCompany;
	/**发货人*/
	private String sendName;
	/**发货日期*/
	private String sendDate;
	/**收货单位*/
	private String receive_company;
	/**收货人*/
	private String receiveName;
	/**收货日期*/
	private String receiveDate;
	/**状态    0:未收货， 1：已收货  2:已确认*/
	private Integer state;
	/**容器回收数量*/
	private String sureName;
	/**确认时间*/
	private String sureDate;
	/**创建人*/
	private String createName;
	/**创建时间*/
	private String createDate;
	/**备注*/
	private String remark;
	/**确认人*/
	private String confirmor;
	/**确认时间*/
	private String confirmorDate;
	/**发货人电话*/
	private String sendPhone;
	/**发货人地址*/
	private String sendAdress;
	/**供应商ID*/
	private Long customerId;
	/**图片路径*/
	private String imgLog;
	private String flag;
	private String cancellation;
	private String cancellation_time;
	private String restorer ;
	private String restorer_time;
	private String photo;
	
	/**是否是精简入库单 1：是*/
	private int isSimple;
	
	public int getIsSimple() {
		return isSimple;
	}
	public void setIsSimple(int isSimple) {
		this.isSimple = isSimple;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCancellation() {
		return cancellation;
	}
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}
	public String getCancellation_time() {
		return cancellation_time;
	}
	public void setCancellation_time(String cancellation_time) {
		this.cancellation_time = cancellation_time;
	}
	public String getRestorer() {
		return restorer;
	}
	public void setRestorer(String restorer) {
		this.restorer = restorer;
	}
	public String getRestorer_time() {
		return restorer_time;
	}
	public void setRestorer_time(String restorer_time) {
		this.restorer_time = restorer_time;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setCreateName(String createName){
		this.createName = createName== null ? null : createName.trim();
	}
	public String getCreateName(){
		return createName;
	}
	public void setCreateDate(String createDate){
		this.createDate = createDate== null ? null : createDate;
	}
	public String getCreateDate(){
		return createDate;
	}
	public String getReceive_company() {
		return receive_company;
	}
	public void setReceive_company(String receive_company) {
		this.receive_company = receive_company;
	}
	public String getSendNumber() {
		return sendNumber;
	}
	public void setSendNumber(String sendNumber) {
		this.sendNumber = sendNumber;
	}
	public void setReceiveNumber(String receiveNumber){
		this.receiveNumber = receiveNumber== null ? null : receiveNumber.trim();
	}
	public String getReceiveNumber(){
		return receiveNumber;
	}
	public void setSendCompany(String sendCompany){
		this.sendCompany = sendCompany== null ? null : sendCompany.trim();
	}
	public String getSendCompany(){
		return sendCompany;
	}
	public void setSendName(String sendName){
		this.sendName = sendName== null ? null : sendName.trim();
	}
	public String getSendName(){
		return sendName;
	}
	public void setSendDate(String sendDate){
		this.sendDate = sendDate== null ? null : sendDate;
	}
	public String getSendDate(){
		return sendDate;
	}
	public void setReceiveName(String receiveName){
		this.receiveName = receiveName== null ? null : receiveName.trim();
	}
	public String getReceiveName(){
		return receiveName;
	}
	public void setReceiveDate(String receiveDate){
		this.receiveDate = receiveDate== null ? null : receiveDate.trim();
	}
	public String getReceiveDate(){
		return receiveDate;
	}
	
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public void setSureName(String sureName){
		this.sureName = sureName== null ? null : sureName.trim();
	}
	public String getSureName(){
		return sureName;
	}
	public void setSureDate(String sureDate){
		this.sureDate = sureDate== null ? null : sureDate;
	}
	public String getSureDate(){
		return sureDate;
	}
	public void setRemark(String remark){
		this.remark = remark== null ? null : remark.trim();
	}
	public String getRemark(){
		return remark;
	}
	public String getConfirmor() {
		return confirmor;
	}
	public void setConfirmor(String confirmor) {
		this.confirmor = confirmor;
	}
	public String getConfirmorDate() {
		return confirmorDate;
	}
	public void setConfirmorDate(String confirmorDate) {
		this.confirmorDate = confirmorDate;
	}
	public String getSendPhone() {
		return sendPhone;
	}
	public void setSendPhone(String sendPhone) {
		this.sendPhone = sendPhone;
	}
	public String getSendAdress() {
		return sendAdress;
	}
	public void setSendAdress(String sendAdress) {
		this.sendAdress = sendAdress;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getImgLog() {
		return imgLog;
	}
	public void setImgLog(String imgLog) {
		this.imgLog = imgLog;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
}
