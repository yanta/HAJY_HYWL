package com.sdry.model.jyy;

/**
 * 项目版本
 * @Title:Version
 * @Package com.sdry.model.llm
 * @author llm
 * @date 2019年3月26日
 * @version 1.0
 */

public class Version {

	/** 主键ID */
	private Long vid;
	
	/** 版本号 */
	private String versionNumber;
	
	/** 下载链接 */
	private String versionUrl;
	
	/** 版本信息 */
	private String vsersionInformation;
	
	/** 备用 */
	private String remark1;
	
	/** 备用 */
	private String remark2;
	
	/** 备用 */
	private String remark3;
	
	/** 备用 */
	private String remark4;
	
	/** 备用 */
	private String remark5;

	public Version() {
		super();
	}

	public Long getVid() {
		return vid;
	}

	public void setVid(Long vid) {
		this.vid = vid;
	}

	public String getVersionNumber() {
		return versionNumber;
	}

	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}

	public String getVersionUrl() {
		return versionUrl;
	}

	public void setVersionUrl(String versionUrl) {
		this.versionUrl = versionUrl;
	}

	public String getVsersionInformation() {
		return vsersionInformation;
	}

	public void setVsersionInformation(String vsersionInformation) {
		this.vsersionInformation = vsersionInformation;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public String getRemark4() {
		return remark4;
	}

	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}

	public String getRemark5() {
		return remark5;
	}

	public void setRemark5(String remark5) {
		this.remark5 = remark5;
	}

	@Override
	public String toString() {
		return "Version [vid=" + vid + ", versionNumber=" + versionNumber + ", versionUrl=" + versionUrl
				+ ", vsersionInformation=" + vsersionInformation + ", remark1=" + remark1 + ", remark2=" + remark2
				+ ", remark3=" + remark3 + ", remark4=" + remark4 + ", remark5=" + remark5 + "]";
	}
	
}
