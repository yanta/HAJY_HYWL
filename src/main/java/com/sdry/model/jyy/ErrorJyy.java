package com.sdry.model.jyy;

/**
 * 错误信息实体
 * @author jyy
 * 2019-09-02 16:26:10
 */
public class ErrorJyy {
	/**错误发生时间*/
	private String error_time;
	/**错误类型*/
	private String error_type ;
	/**错误信息*/
	private String error_message ;
	
	public String getError_time() {
		return error_time;
	}
	public void setError_time(String error_time) {
		this.error_time = error_time;
	}
	public String getError_type() {
		return error_type;
	}
	public void setError_type(String error_type) {
		this.error_type = error_type;
	}
	public String getError_message() {
		return error_message;
	}
	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
}
