package com.sdry.model.jyy;

/**
 * @ClassName Workload
 * @Description 工作量
 * @Author lz
 * @Date 2019年7月17日 15:48:50
 * @Version 1.0
 */
public class Workload {

    //id
    private Long id;
    //质检单id
    private Long receiveDetailQuality_id;
    //工作人员姓名id
    private String workman;
    //工作人员姓名
    private String workman_name;
    //总数
    private int good_num;
    //不良品数
    private int ngood_num;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceiveDetailQuality_id() {
        return receiveDetailQuality_id;
    }

    public void setReceiveDetailQuality_id(Long receiveDetailQuality_id) {
        this.receiveDetailQuality_id = receiveDetailQuality_id;
    }

    public String getWorkman() {
        return workman;
    }

    public void setWorkman(String workman) {
        this.workman = workman;
    }

    public int getGood_num() {
        return good_num;
    }

    public void setGood_num(int good_num) {
        this.good_num = good_num;
    }

    public int getNgood_num() {
        return ngood_num;
    }

    public void setNgood_num(int ngood_num) {
        this.ngood_num = ngood_num;
    }

	public String getWorkman_name() {
		return workman_name;
	}

	public void setWorkman_name(String workman_name) {
		this.workman_name = workman_name;
	}
    
}
