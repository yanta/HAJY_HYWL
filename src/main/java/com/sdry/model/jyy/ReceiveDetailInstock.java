package com.sdry.model.jyy;

/**
 * @ClassName ReceiveDetailInstock
 * @Description 已收货未入库
 * @Author lz
 * @Date 2019年7月12日 13:42:01
 * @Version 1.0
 */
public class ReceiveDetailInstock {

    //id
    private Long id;
    //收货单详细id
    private Long receive_detail_id;
    //物料id
    private Long mid;
    //批次
    private String pici;
    //收货总数量
    private int code_num;
    //单个数量
    private int single_num;
    //物料名称
    private String materiel_name;
    //物料编号
    private String materiel_num;
    //物料规格
    private String materiel_size;
    //物料属性
    private String materiel_properties;
    //包装数量
    private String packing_quantity;
    //客户
    private String customer_name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceive_detail_id() {
        return receive_detail_id;
    }

    public void setReceive_detail_id(Long receive_detail_id) {
        this.receive_detail_id = receive_detail_id;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public String getPici() {
        return pici;
    }

    public void setPici(String pici) {
        this.pici = pici;
    }

    public int getCode_num() {
        return code_num;
    }

    public void setCode_num(int code_num) {
        this.code_num = code_num;
    }

    public String getMateriel_name() {
        return materiel_name;
    }

    public void setMateriel_name(String materiel_name) {
        this.materiel_name = materiel_name;
    }

    public String getMateriel_num() {
        return materiel_num;
    }

    public void setMateriel_num(String materiel_num) {
        this.materiel_num = materiel_num;
    }

    public String getMateriel_size() {
        return materiel_size;
    }

    public void setMateriel_size(String materiel_size) {
        this.materiel_size = materiel_size;
    }

    public String getMateriel_properties() {
        return materiel_properties;
    }

    public void setMateriel_properties(String materiel_properties) {
        this.materiel_properties = materiel_properties;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public int getSingle_num() {
        return single_num;
    }

    public void setSingle_num(int single_num) {
        this.single_num = single_num;
    }

    public String getPacking_quantity() {
        return packing_quantity;
    }

    public void setPacking_quantity(String packing_quantity) {
        this.packing_quantity = packing_quantity;
    }
}
