package com.sdry.model.jyy;
/**
 *
 *@ClassName: Up
 *@Description: 入库记录
 *@Author jyy
 *@Date 2019-05-17 17:20:06
 *@version 1.0
*/
public class Up {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/** null*/
	private Long id;
	/** 创建人名称*/
	private String createName;
	/** 创建日期*/
	private String createDate;
	/** 收货单号*/
	private String receiveNumber;
	/** 物料id*/
	private Long mid;
	/** 条码*/
	private String code;
	/** 数量*/
	private int number;
	/** 批次*/
	private String pici;
	/** 仓库id*/
	private Long cq;
	/** 库区id*/
	private Long kq;
	/** 库位id*/
	private Long kw;
	/** 备注*/
	private String remark;
	
	/**供应商id*/
	private Long customerId;
	//供应商名称
	private String customer_name;
	/** 仓库name*/
	private String cqname;
	/** 库区name*/
	private String kqname;
	/** 库位name*/
	private String kwname;
	
	
	//容器名称
    private String container_name;
    //所属客户
    private String client;
    //物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //简码
    private String brevity_num;
    //物料规格
    private String materiel_size;
    //物料属性
    private String materiel_properties;
    //单位(个、条)
    private String unit;
    //体积
    private String volume;
    //重量
    private String weight;
    //编码规则
    private String code_rule;
    //出库频率
    private String outgoing_frequency;
    //包装数量
    private String packing_quantity;
    //是否拆箱
    private String is_devanning;
    //拆箱数
    private String devanning_num;
    //入库类型(精准入库、普通入库)
    private String storage_type;
    //条码类型(1、系统码 2、客户码1 3、客户码2 4、无码(输入简码))
    private String barcode_type;
    //放置类型
    private String placement_type;
    //拆箱后容器
    private String removal_container;
    //拆箱后产品码
    private String devanning_code;
	
    //规格型号
    private String specification;
    //批次号
    private String batch_num;
    
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setCreateName(String createName){
		this.createName = createName== null ? null : createName.trim();
	}
	public String getCreateName(){
		return createName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setReceiveNumber(String receiveNumber){
		this.receiveNumber = receiveNumber== null ? null : receiveNumber.trim();
	}
	public String getReceiveNumber(){
		return receiveNumber;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public String getPici() {
		return pici;
	}
	public void setPici(String pici) {
		this.pici = pici;
	}
	public Long getCq() {
		return cq;
	}
	public void setCq(Long cq) {
		this.cq = cq;
	}
	public Long getKq() {
		return kq;
	}
	public void setKq(Long kq) {
		this.kq = kq;
	}
	public Long getKw() {
		return kw;
	}
	public void setKw(Long kw) {
		this.kw = kw;
	}
	public void setRemark(String remark){
		this.remark = remark== null ? null : remark.trim();
	}
	public String getRemark(){
		return remark;
	}
	
	
	public String getCqname() {
		return cqname;
	}
	public void setCqname(String cqname) {
		this.cqname = cqname;
	}
	public String getKqname() {
		return kqname;
	}
	public void setKqname(String kqname) {
		this.kqname = kqname;
	}
	public String getKwname() {
		return kwname;
	}
	public void setKwname(String kwname) {
		this.kwname = kwname;
	}
	
	
	
	
	public String getContainer_name() {
		return container_name;
	}
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getMateriel_size() {
		return materiel_size;
	}
	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}
	public String getMateriel_properties() {
		return materiel_properties;
	}
	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getCode_rule() {
		return code_rule;
	}
	public void setCode_rule(String code_rule) {
		this.code_rule = code_rule;
	}
	public String getOutgoing_frequency() {
		return outgoing_frequency;
	}
	public void setOutgoing_frequency(String outgoing_frequency) {
		this.outgoing_frequency = outgoing_frequency;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
	public String getIs_devanning() {
		return is_devanning;
	}
	public void setIs_devanning(String is_devanning) {
		this.is_devanning = is_devanning;
	}
	public String getDevanning_num() {
		return devanning_num;
	}
	public void setDevanning_num(String devanning_num) {
		this.devanning_num = devanning_num;
	}
	public String getStorage_type() {
		return storage_type;
	}
	public void setStorage_type(String storage_type) {
		this.storage_type = storage_type;
	}
	public String getBarcode_type() {
		return barcode_type;
	}
	public void setBarcode_type(String barcode_type) {
		this.barcode_type = barcode_type;
	}
	public String getPlacement_type() {
		return placement_type;
	}
	public void setPlacement_type(String placement_type) {
		this.placement_type = placement_type;
	}
	public String getRemoval_container() {
		return removal_container;
	}
	public void setRemoval_container(String removal_container) {
		this.removal_container = removal_container;
	}
	public String getDevanning_code() {
		return devanning_code;
	}
	public void setDevanning_code(String devanning_code) {
		this.devanning_code = devanning_code;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getSpecification() {
		return specification;
	}
	public void setSpecification(String specification) {
		this.specification = specification;
	}
	public String getBatch_num() {
		return batch_num;
	}
	public void setBatch_num(String batch_num) {
		this.batch_num = batch_num;
	}
}
