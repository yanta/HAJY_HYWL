package	com.sdry.model.jyy.unpack;
/**
 *
 *@ClassName: UnpackDetalCode
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:31
 *@version 1.0
*/
public class UnpackDetalCode {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/** id*/
	private Long id;
	/** 二级详情id*/
	private Long did;
	
	//拆箱详情实体（tdd）
	private UnpackDetal unpackDetal;
	
	
	/** 条码*/
	private String code;
	/** 数量*/
	private Integer num;
	/** 备注*/
	private String remark;
	
	/**二维码*/
	private String twoCodePath;
	/**一维码*/
	private String barCodePath;
	
	
	public UnpackDetal getUnpackDetal() {
		return unpackDetal;
	}
	public void setUnpackDetal(UnpackDetal unpackDetal) {
		this.unpackDetal = unpackDetal;
	}
	public String getTwoCodePath() {
		return twoCodePath;
	}
	public void setTwoCodePath(String twoCodePath) {
		this.twoCodePath = twoCodePath;
	}
	public String getBarCodePath() {
		return barCodePath;
	}
	public void setBarCodePath(String barCodePath) {
		this.barCodePath = barCodePath;
	}
	public Long getDid() {
		return did;
	}
	public void setDid(Long did) {
		this.did = did;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setCode(String code){
		this.code = code== null ? null : code.trim();
	}
	public String getCode(){
		return code;
	}
	public void setNum(Integer num){
		this.num = num;
	}
	public Integer getNum(){
		return num;
	}
	public void setRemark(String remark){
		this.remark = remark== null ? null : remark.trim();
	}
	public String getRemark(){
		return remark;
	}
}
