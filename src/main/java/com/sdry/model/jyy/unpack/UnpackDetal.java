package	com.sdry.model.jyy.unpack;

import com.sdry.model.lz.Materiel;

/**
 *
 *@ClassName: UnpackDetal
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:58:14
 *@version 1.0
*/
public class UnpackDetal {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/** 拆箱计划单号*/
	private String ordernum;
	
	/** id*/
	private Long id;
	/** 物料id*/
	private Long mid;
	/** 批次*/
	private String pici;
	/** 需拆箱总数*/
	private Integer znum;
	/** 翻包规格*/
	private Integer specs;
	/** 拆箱数量*/
	private Integer unum;
	/**状态：0：未拆箱，1未完成，2完成*/
	private Integer state;
	/**关联拆箱记录id*/
	private String worklog;
	
	//物料实体（tdd）
	private Materiel materiel;
	
    //物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //零件号
    private String brevity_num;
    //包装数量
  	private String packing_quantity;
    
  	
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public String getWorklog() {
		return worklog;
	}
	public void setWorklog(String worklog) {
		this.worklog = worklog;
	}
	public Integer getState() {
		return state;
	}
	public void setState(Integer state) {
		this.state = state;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getOrdernum() {
		return ordernum;
	}
	public void setOrdernum(String ordernum) {
		this.ordernum = ordernum;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setMid(Long mid){
		this.mid = mid;
	}
	public Long getMid(){
		return mid;
	}
	public void setPici(String pici){
		this.pici = pici== null ? null : pici.trim();
	}
	public String getPici(){
		return pici;
	}
	public void setZnum(Integer znum){
		this.znum = znum;
	}
	public Integer getZnum(){
		return znum;
	}
	public void setSpecs(Integer specs){
		this.specs = specs;
	}
	public Integer getSpecs(){
		return specs;
	}
	public void setUnum(Integer unum){
		this.unum = unum;
	}
	public Integer getUnum(){
		return unum;
	}
}
