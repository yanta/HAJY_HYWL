package	com.sdry.model.jyy.unpack;
/**
 *
 *@ClassName: Unpacking
 *@Description: 
 *@Author jyy
 *@Date 2019-08-20 15:57:31
 *@version 1.0
*/
public class JyyUnpacking {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/** id*/
	private Long id;
	/** 拆箱计划单号*/
	private String ordernum;
	/** 创建人*/
	private Long createName;
	/** 创建时间*/
	private String createDate;
	/** 拆箱人*/
	private Long unpackName;
	/** 拆箱时间*/
	private String unpackDate;
	/** 状态*/
	private Integer state;
	/** 备注*/
	private String remark;
	
	/** 创建人姓名*/
	private String userName;
	/** 拆箱人姓名*/
	private String unpackMan;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUnpackMan() {
		return unpackMan;
	}
	public void setUnpackMan(String unpackMan) {
		this.unpackMan = unpackMan;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setOrdernum(String ordernum){
		this.ordernum = ordernum== null ? null : ordernum.trim();
	}
	public String getOrdernum(){
		return ordernum;
	}
	public void setCreateName(Long createName){
		this.createName = createName;
	}
	public Long getCreateName(){
		return createName;
	}
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public void setUnpackName(Long unpackName){
		this.unpackName = unpackName;
	}
	public Long getUnpackName(){
		return unpackName;
	}
	public String getUnpackDate() {
		return unpackDate;
	}
	public void setUnpackDate(String unpackDate) {
		this.unpackDate = unpackDate;
	}
	public void setState(Integer state){
		this.state = state;
	}
	public Integer getState(){
		return state;
	}
	public void setRemark(String remark){
		this.remark = remark== null ? null : remark.trim();
	}
	public String getRemark(){
		return remark;
	}
}
