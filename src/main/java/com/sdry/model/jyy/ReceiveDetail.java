package com.sdry.model.jyy;


import com.sdry.model.lz.CodeMark;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *@ClassName: ReceiveDetail
 *@Description: 收货计划详情
 *@Author jyy
 *@Date 2019-04-19 14:30:17
 *@version 1.0
*/
public class ReceiveDetail {
	
	/**页码*/
	private int page;
	/**每页记录数*/
	private int limit;
	
	/**id*/
	private Long id;
	/**收货单号*/
	private String receiveNumber;
	/**物料id*/
	private Long mid;
	/**批次*/
	private String pici;

	/**不良数量*/
	private int ngNum;
	/**备注*/
	private String remark;
	//二维码
	private String rcode;

	//发货箱数(箱)
	private int sendNum;
	//发货单数(个)
	private int singleNum;
	//发货总数(个)
	private int totalNum;

	//收货单数（个）
	private int single_num;
	//收货总数(个)
	private int codeNum;
	//收货总数(个)
	private int receiveNum;

	//不良品的条码
	private String ng_rcode;
	//不良品的数量
	private int ng_count;
	//自定义精准码
	private List<CodeMark> receiveCode = new ArrayList<>();
	//收货状态（0：未收货 1:已收货 2:已入库）
	private int status;

	//容器名称
    private String container_name;
    //所属客户
    private String client;
    //物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //简码
    private String brevity_num;
    //物料规格
    private String materiel_size;
    //物料属性
    private String materiel_properties;
    //单位(个、条)
    private String unit;
    //体积
    private String volume;
    //重量
    private String weight;
    //编码规则
    private String code_rule;
    //出库频率
    private String outgoing_frequency;
    //包装数量
    private String packing_quantity;
    //是否拆箱
    private String is_devanning;
    //拆箱数
    private String devanning_num;
    //入库类型(精准入库、普通入库)
    private String storage_type;
    //条码类型(1、系统码 2、客户码1 3、客户码2 4、无码(输入简码))
    private String barcode_type;
    //放置类型
    private String placement_type;
    //拆箱后容器
    private String removal_container;
    //拆箱后产品码
    private String devanning_code;
	
	
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setReceiveNumber(String receiveNumber){
		this.receiveNumber = receiveNumber== null ? null : receiveNumber.trim();
	}
	public String getReceiveNumber(){
		return receiveNumber;
	}
	public void setMid(Long mid){
		this.mid = mid== null ? null : mid;
	}
	public Long getMid(){
		return mid;
	}
	public int getSendNum() {
		return sendNum;
	}
	public void setSendNum(int sendNum) {
		this.sendNum = sendNum;
	}
	public int getReceiveNum() {
		return receiveNum;
	}
	public void setReceiveNum(int receiveNum) {
		this.receiveNum = receiveNum;
	}
	public void setRemark(String remark){
		this.remark = remark== null ? null : remark.trim();
	}
	public String getRemark(){
		return remark;
	}
	public String getContainer_name() {
		return container_name;
	}
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}
	public String getClient() {
		return client;
	}
	public void setClient(String client) {
		this.client = client;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getMateriel_size() {
		return materiel_size;
	}
	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}
	public String getMateriel_properties() {
		return materiel_properties;
	}
	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}
	public String getCode_rule() {
		return code_rule;
	}
	public void setCode_rule(String code_rule) {
		this.code_rule = code_rule;
	}
	public String getOutgoing_frequency() {
		return outgoing_frequency;
	}
	public void setOutgoing_frequency(String outgoing_frequency) {
		this.outgoing_frequency = outgoing_frequency;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
	public String getIs_devanning() {
		return is_devanning;
	}
	public void setIs_devanning(String is_devanning) {
		this.is_devanning = is_devanning;
	}
	public String getDevanning_num() {
		return devanning_num;
	}
	public void setDevanning_num(String devanning_num) {
		this.devanning_num = devanning_num;
	}
	public String getStorage_type() {
		return storage_type;
	}
	public void setStorage_type(String storage_type) {
		this.storage_type = storage_type;
	}
	public String getBarcode_type() {
		return barcode_type;
	}
	public void setBarcode_type(String barcode_type) {
		this.barcode_type = barcode_type;
	}
	public String getPlacement_type() {
		return placement_type;
	}
	public void setPlacement_type(String placement_type) {
		this.placement_type = placement_type;
	}
	public String getRemoval_container() {
		return removal_container;
	}
	public void setRemoval_container(String removal_container) {
		this.removal_container = removal_container;
	}
	public String getPici() {
		return pici;
	}
	public void setPici(String pici) {
		this.pici = pici;
	}
	public int getNgNum() {
		return ngNum;
	}
	public void setNgNum(int ngNum) {
		this.ngNum = ngNum;
	}
	public String getDevanning_code() {
		return devanning_code;
	}
	public void setDevanning_code(String devanning_code) {
		this.devanning_code = devanning_code;
	}

	public String getRcode() {
		return rcode;
	}

	public void setRcode(String rcode) {
		this.rcode = rcode;
	}

	public int getSingleNum() {
		return singleNum;
	}

	public void setSingleNum(int singleNum) {
		this.singleNum = singleNum;
	}

	public List<CodeMark> getReceiveCode() {
		return receiveCode;
	}

	public void setReceiveCode(List<CodeMark> receiveCode) {
		this.receiveCode = receiveCode;
	}

	public int getTotalNum() {
		return totalNum;
	}

	public void setTotalNum(int totalNum) {
		this.totalNum = totalNum;
	}

	public int getCodeNum() {
		return codeNum;
	}

	public void setCodeNum(int codeNum) {
		this.codeNum = codeNum;
	}

	public int getSingle_num() {
		return single_num;
	}

	public void setSingle_num(int single_num) {
		this.single_num = single_num;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getNg_rcode() {
		return ng_rcode;
	}

	public void setNg_rcode(String ng_rcode) {
		this.ng_rcode = ng_rcode;
	}

	public int getNg_count() {
		return ng_count;
	}

	public void setNg_count(int ng_count) {
		this.ng_count = ng_count;
	}
}
