package com.sdry.model.jyy;

import com.sdry.model.lz.CodeMark;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ReceiveDetailQuality
 * @Description 质检实体
 * @Author lz
 * @Date 2019年7月12日 13:42:01
 * @Version 1.0
 */
public class ReceiveDetailQuality {

    //id
    private Long id;
    //收货单详细id
    private Long receive_detail_id;
    //物料id
    private Long mid;
    //批次
    private String pici;
    //收货总数量（个）
    private int codeNum;
    //收货箱数（箱）
    private int receive_num;
    //不良品数量
    private int ng_num;
    //良品数量
    private int yg_num;
    //收货单个数量（个）
    private int single_num;
    //物料名称
    private String materiel_name;
    //物料编号(产品码)
    private String materiel_num;
    //简码
    private String brevity_num;
    //物料规格
    private String materiel_size;
    //物料属性
    private String materiel_properties;
    //包装数量
    private String packing_quantity;
    //是否拆箱
    private String is_devanning;
    //是否质检
    private String is_check;
    //客户
    private String customer_name;
    //自定义精准码
    private List<CodeMark> receiveCode = new ArrayList<>();

    //工作量实体集合
    private List<Workload> workloadList = new ArrayList<>();
    //提交人
    private String person;
    private String remark;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceive_detail_id() {
        return receive_detail_id;
    }

    public void setReceive_detail_id(Long receive_detail_id) {
        this.receive_detail_id = receive_detail_id;
    }

    public Long getMid() {
        return mid;
    }

    public void setMid(Long mid) {
        this.mid = mid;
    }

    public String getPici() {
        return pici;
    }

    public void setPici(String pici) {
        this.pici = pici;
    }

    public int getCodeNum() {
        return codeNum;
    }

    public void setCodeNum(int codeNum) {
        this.codeNum = codeNum;
    }

    public int getReceive_num() {
        return receive_num;
    }

    public void setReceive_num(int receive_num) {
        this.receive_num = receive_num;
    }

    public int getNg_num() {
        return ng_num;
    }

    public void setNg_num(int ng_num) {
        this.ng_num = ng_num;
    }

    public int getYg_num() {
        return yg_num;
    }

    public void setYg_num(int yg_num) {
        this.yg_num = yg_num;
    }

    public int getSingle_num() {
        return single_num;
    }

    public void setSingle_num(int single_num) {
        this.single_num = single_num;
    }

    public String getMateriel_name() {
        return materiel_name;
    }

    public void setMateriel_name(String materiel_name) {
        this.materiel_name = materiel_name;
    }

    public String getMateriel_num() {
        return materiel_num;
    }

    public void setMateriel_num(String materiel_num) {
        this.materiel_num = materiel_num;
    }

    public String getBrevity_num() {
        return brevity_num;
    }

    public void setBrevity_num(String brevity_num) {
        this.brevity_num = brevity_num;
    }

    public String getMateriel_size() {
        return materiel_size;
    }

    public void setMateriel_size(String materiel_size) {
        this.materiel_size = materiel_size;
    }

    public String getMateriel_properties() {
        return materiel_properties;
    }

    public void setMateriel_properties(String materiel_properties) {
        this.materiel_properties = materiel_properties;
    }

    public String getPacking_quantity() {
        return packing_quantity;
    }

    public void setPacking_quantity(String packing_quantity) {
        this.packing_quantity = packing_quantity;
    }

    public String getIs_devanning() {
        return is_devanning;
    }

    public void setIs_devanning(String is_devanning) {
        this.is_devanning = is_devanning;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public List<Workload> getWorkloadList() {
        return workloadList;
    }

    public void setWorkloadList(List<Workload> workloadList) {
        this.workloadList = workloadList;
    }

    public List<CodeMark> getReceiveCode() {
        return receiveCode;
    }

    public void setReceiveCode(List<CodeMark> receiveCode) {
        this.receiveCode = receiveCode;
    }

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public String getIs_check() {
		return is_check;
	}

	public void setIs_check(String is_check) {
		this.is_check = is_check;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
    
}
