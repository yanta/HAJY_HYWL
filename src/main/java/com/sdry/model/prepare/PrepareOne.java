package	com.sdry.model.prepare;

import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: PrepareOne
 *@Description: 配货单
 *@Author tdd
 *@Date 2019-08-24 16:48:02
 *@version 1.0
*/
public class PrepareOne extends PageBean{
	/** 主键*/
	private Long id;
	/** 配货单号*/
	private String num;
	/** 创建人*/
	private String person;
	/** 创建时间*/
	private String createTime;
	/** 供应商*/
	private String supplyid;
	 // 供应商
    private String supply;
    public String getSupply() {
        return supply;
    }
    public void setSupply(String supply) {
        this.supply = supply;
    }
	/** 状态 0未配货  1配货完成 2配货中*/
	private String state;
	/** 备注*/
	private String remarks;
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setNum(String num){
		this.num = num== null ? null : num.trim();
	}
	public String getNum(){
		return num;
	}
	public void setPerson(String person){
		this.person = person== null ? null : person.trim();
	}
	public String getPerson(){
		return person;
	}
	public void setCreateTime(String createTime){
		this.createTime = createTime== null ? null : createTime.trim();
	}
	public String getCreateTime(){
		return createTime;
	}
	public void setSupplyid(String supplyid){
		this.supplyid = supplyid== null ? null : supplyid.trim();
	}
	public String getSupplyid(){
		return supplyid;
	}
	public void setState(String state){
		this.state = state== null ? null : state.trim();
	}
	public String getState(){
		return state;
	}
	public void setRemarks(String remarks){
		this.remarks = remarks== null ? null : remarks.trim();
	}
	public String getRemarks(){
		return remarks;
	}
}
