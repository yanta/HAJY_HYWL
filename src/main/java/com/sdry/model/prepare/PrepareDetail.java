package	com.sdry.model.prepare;

import java.util.List;

import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: PrepareDetail
 *@Description: 配货单详情
 *@Author tdd
 *@Date 2019-08-24 16:47:15
 *@version 1.0
*/
public class PrepareDetail extends PageBean{
	/** 主键*/
	private Long id;
	/** 配货单id*/
	private Long did;
	/** 物料id */
	private Long mid;
	// 产品码
    private String sqd;
    public String getSqd() {
    	return sqd;
    }
    public void setSqd(String sqd) {
    	this.sqd = sqd;
    }
    // 物料名
    private String materialName;
    public String getMaterialName() {
        return materialName;
    }
    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }
	/** 提交数量*/
	private int subnum;
	/** 需要数量*/
	private int plannum;
	/** 备注*/
	private String remarks;
	/** 状态 0未配货  1配货完成 2配货中*/
	private String state;
	// 推荐库位
    private List<String> location;
	public List<String> getLocation() {
		return location;
	}
	public void setLocation(List<String> location) {
		this.location = location;
	}
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setDid(Long did){
		this.did = did;
	}
	public Long getDid(){
		return did;
	}
	public void setMid(Long mid){
		this.mid = mid;
	}
	public Long getMid(){
		return mid;
	}
	public void setSubnum(int subnum){
		this.subnum = subnum;
	}
	public int getSubnum(){
		return subnum;
	}
	public void setPlannum(int plannum){
		this.plannum = plannum;
	}
	public int getPlannum(){
		return plannum;
	}
	public void setRemarks(String remarks){
		this.remarks = remarks== null ? null : remarks.trim();
	}
	public String getRemarks(){
		return remarks;
	}
	public void setState(String state){
		this.state = state== null ? null : state.trim();
	}
	public String getState(){
		return state;
	}
}
