package	com.sdry.model.prepare;

import java.util.List;

import com.sdry.model.lz.CodeMark;
import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: PrepareDetailCode
 *@Description: 配货单详情条码
 *@Author tdd
 *@Date 2019-08-24 16:49:04
 *@version 1.0
*/
public class PrepareDetailCode extends PageBean{
	/** 主键*/
	private Long id;
	/** 配货单详情id*/
	private Long ddid;
	/** 条码*/
	private String codeMark;
	private List<CodeMark> codeMarks;
	public List<CodeMark> getCodeMarks() {
		return codeMarks;
	}
	public void setCodeMarks(List<CodeMark> codeMarks) {
		this.codeMarks = codeMarks;
	}
	/** 库位id*/
	private String locations;
	//库位名称
    private String location_name;
    public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
	//库位编号
    private String location_num;
	public String getLocation_num() {
		return location_num;
	}
	public void setLocation_num(String location_num) {
		this.location_num = location_num;
	}
	/** 配货人*/
	private String preMan;
	/** 配货时间*/
	private String preTime;
	/**配货数量*/
	private int preNum;
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setDdid(Long ddid){
		this.ddid = ddid;
	}
	public Long getDdid(){
		return ddid;
	}
	public void setCodeMark(String codeMark){
		this.codeMark = codeMark== null ? null : codeMark.trim();
	}
	public String getCodeMark(){
		return codeMark;
	}
	public void setLocations(String locations){
		this.locations = locations== null ? null : locations.trim();
	}
	public String getLocations(){
		return locations;
	}
	public void setPreMan(String preMan){
		this.preMan = preMan== null ? null : preMan.trim();
	}
	public String getPreMan(){
		return preMan;
	}
	public void setPreTime(String preTime){
		this.preTime = preTime== null ? null : preTime.trim();
	}
	public String getPreTime(){
		return preTime;
	}
	public int getPreNum() {
		return preNum;
	}
	public void setPreNum(int preNum) {
		this.preNum = preNum;
	}
}
