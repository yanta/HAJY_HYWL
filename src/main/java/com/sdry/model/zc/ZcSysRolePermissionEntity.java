package com.sdry.model.zc;
/**
 * 
 * @ClassName:    ZcSysRolePermissionEntity   
 * @Description:  角色权限表
 * @Author:       zc   
 * @CreateDate:   2019年4月16日 上午11:23:30   
 * @Version:      v1.0
 */
public class ZcSysRolePermissionEntity {

	private Long id;
	//角色id
	private Long roleId;
	//权限id
	private Long permissionId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	public Long getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}
	
}
