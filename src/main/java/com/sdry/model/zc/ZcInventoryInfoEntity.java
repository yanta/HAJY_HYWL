package com.sdry.model.zc;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;

/**
 * 
 * @ClassName:    ZcInventoryInfoEntity   
 * @Description:  库存信息
 * @Author:       zc   
 * @CreateDate:   2019年4月19日 下午5:08:02   
 * @Version:      v1.0
 */
public class ZcInventoryInfoEntity {

	private Long id;
	//物料ID
	private Long mid;
	//批次
	private String mBatch;
	//库存总数量
	private int countAll;
	//库存数量
	private int mNum;
	//入库日期
	private String enterDate;
	//入库人
	private Long enterPerson;
	//物料实体
	private Materiel materiel;
	//入库人实体
	private ZcSysUserEntity zcSysUser;
	private String remark;
	//大箱物料数量
	private int bigNum;
	//小箱物料数量
	private int smallNum;
	//大箱物料总数量
	private int bigNumAll;
	//小箱物料总数量
	private int smallNumAll;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public int getmNum() {
		return mNum;
	}
	public void setmNum(int mNum) {
		this.mNum = mNum;
	}
	public String getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}
	public Long getEnterPerson() {
		return enterPerson;
	}
	public void setEnterPerson(Long enterPerson) {
		this.enterPerson = enterPerson;
	}
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public ZcSysUserEntity getZcSysUser() {
		return zcSysUser;
	}
	public void setZcSysUser(ZcSysUserEntity zcSysUser) {
		this.zcSysUser = zcSysUser;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public int getBigNum() {
		return bigNum;
	}
	public void setBigNum(int bigNum) {
		this.bigNum = bigNum;
	}
	public int getSmallNum() {
		return smallNum;
	}
	public void setSmallNum(int smallNum) {
		this.smallNum = smallNum;
	}
	public int getCountAll() {
		return countAll;
	}
	public void setCountAll(int countAll) {
		this.countAll = countAll;
	}
	public int getBigNumAll() {
		return bigNumAll;
	}
	public void setBigNumAll(int bigNumAll) {
		this.bigNumAll = bigNumAll;
	}
	public int getSmallNumAll() {
		return smallNumAll;
	}
	public void setSmallNumAll(int smallNumAll) {
		this.smallNumAll = smallNumAll;
	}
}
