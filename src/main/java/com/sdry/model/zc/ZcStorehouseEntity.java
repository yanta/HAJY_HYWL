package com.sdry.model.zc;
/**
 * 库房实体
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月31日 上午10:44:56
 * @Version 1.0
 */
public class ZcStorehouseEntity {

	private Long id;
	//库房名称
	private String house_name;
	//库房编号
	private String house_code;
	//所属仓库id
	private long warehouse_id;
	//库房管理员id
	private long house_clerk;
	//所属仓库名称
	private String warehouse_name;
	//库房管理员名称
	private String user_name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getHouse_name() {
		return house_name;
	}
	public void setHouse_name(String house_name) {
		this.house_name = house_name;
	}
	public String getHouse_code() {
		return house_code;
	}
	public void setHouse_code(String house_code) {
		this.house_code = house_code;
	}
	public long getWarehouse_id() {
		return warehouse_id;
	}
	public void setWarehouse_id(long warehouse_id) {
		this.warehouse_id = warehouse_id;
	}
	public long getHouse_clerk() {
		return house_clerk;
	}
	public void setHouse_clerk(long house_clerk) {
		this.house_clerk = house_clerk;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
}
