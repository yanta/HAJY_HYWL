package com.sdry.model.zc;
/**
 * 
 * @ClassName:    ZcUniversalQueryEntity   
 * @Description:  通用查询条件
 * @Author:       zc   
 * @CreateDate:   2019年4月4日 下午3:40:57   
 * @Version:      v1.0
 */
public class ZcGeneralQueryEntity {
		private long id; 
		//表名
		private String tab;
		//字段字符串
		private String strFld;
		//where条件 后加and或者or
		private String strWhere;
		//页码
		private int page;
		//每页容纳的记录数
		private int limit;
		//排序字段及规则,不用加order by
		private String sort;
		//是否得到记录总数，1为得到记录总数，其他为不得到记录总数，返回记录集
		private byte isGetCount;
		public String getTab() {
			return tab;
		}
		public void setTab(String tab) {
			this.tab = tab;
		}
		public String getStrFld() {
			return strFld;
		}
		public void setStrFld(String strFld) {
			this.strFld = strFld;
		}
		public String getStrWhere() {
			return strWhere;
		}
		public void setStrWhere(String strWhere) {
			this.strWhere = strWhere;
		}
		public int getPage() {
			return page;
		}
		public void setPage(int page) {
			this.page = page;
		}
		public int getLimit() {
			return limit;
		}
		public void setLimit(int limit) {
			this.limit = limit;
		}
		public String getSort() {
			return sort;
		}
		public void setSort(String sort) {
			this.sort = sort;
		}
		public byte getIsGetCount() {
			return isGetCount;
		}
		public void setIsGetCount(byte isGetCount) {
			this.isGetCount = isGetCount;
		}
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
}
