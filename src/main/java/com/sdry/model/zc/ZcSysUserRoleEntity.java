package com.sdry.model.zc;
/**
 * 
 * @ClassName:    ZcSysUserRoleEntity   
 * @Description:  用户角色表
 * @Author:       zc   
 * @CreateDate:   2019年4月16日 下午4:54:44   
 * @Version:      v1.0
 */
public class ZcSysUserRoleEntity {

	private Long id;
	private Long userId;
	private Long roleId;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Long getRoleId() {
		return roleId;
	}
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
}
