package com.sdry.model.zc;

import java.util.List;

import com.sdry.model.lz.CodeMark;

/**
 * 物料在库位中的信息实体
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年8月6日 上午9:46:46
 * @Version 1.0
 */
public class ZcMaterielInfoEntity {

	//库位码
	private String location_code;
	//数量
	private int mNum;
	//codeMark实体
	private List<CodeMark> codeMark;
	//操作
	private  int caozuo;

	public String getLocation_code() {
		return location_code;
	}
	public void setLocation_code(String location_code) {
		this.location_code = location_code;
	}
	public int getmNum() {
		return mNum;
	}
	public void setmNum(int mNum) {
		this.mNum = mNum;
	}

	public List<CodeMark> getCodeMark() {
		return codeMark;
	}

	public void setCodeMark(List<CodeMark> codeMark) {
		this.codeMark = codeMark;
	}

	public int getCaozuo() {
		return caozuo;
	}
	public void setCaozuo(int caozuo) {
		this.caozuo = caozuo;
	}
}
