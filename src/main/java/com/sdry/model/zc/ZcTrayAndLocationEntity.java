package com.sdry.model.zc;
/**
 * 
 * @ClassName:    ZcTrayAndLocationEntity   
 * @Description:  托盘库位绑定
 * @Author:       zc   
 * @CreateDate:   2019年6月2日 下午1:24:23   
 * @Version:      v1.0
 */
public class ZcTrayAndLocationEntity {

	private Long id;
	//托盘条码
	private String tray_code;
	//库区名称
	private String region_name;
	//库区类型
	private Long region_type;
	//库位条码
	private String location_code;
	//库位名稱
	private String location_name;
	//绑定人
	private Long binding_person;
	//绑定日期
	private String binding_date;
	//人员实体
	private ZcSysUserEntity zcSysUserEntity;
	//解绑后库位剩的托盘条码（解绑时用）
	private String new_tray_code_list;
	//解绑原因（解绑时用）
	private String unbindle_reason;
	private Long mid;
	//托盘数量
	private int tray_number;
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTray_code() {
		return tray_code;
	}
	public void setTray_code(String tray_code) {
		this.tray_code = tray_code;
	}
	public String getLocation_code() {
		return location_code;
	}
	public void setLocation_code(String location_code) {
		this.location_code = location_code;
	}
	public Long getBinding_person() {
		return binding_person;
	}
	public void setBinding_person(Long binding_person) {
		this.binding_person = binding_person;
	}
	public String getBinding_date() {
		return binding_date;
	}
	public void setBinding_date(String binding_date) {
		this.binding_date = binding_date;
	}
	public ZcSysUserEntity getZcSysUserEntity() {
		return zcSysUserEntity;
	}
	public void setZcSysUserEntity(ZcSysUserEntity zcSysUserEntity) {
		this.zcSysUserEntity = zcSysUserEntity;
	}
	public String getNew_tray_code_list() {
		return new_tray_code_list;
	}
	public void setNew_tray_code_list(String new_tray_code_list) {
		this.new_tray_code_list = new_tray_code_list;
	}
	public String getUnbindle_reason() {
		return unbindle_reason;
	}
	public void setUnbindle_reason(String unbindle_reason) {
		this.unbindle_reason = unbindle_reason;
	}
	public int getTray_number() {
		return tray_number;
	}
	public void setTray_number(int tray_number) {
		this.tray_number = tray_number;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public Long getRegion_type() {
		return region_type;
	}
	public void setRegion_type(Long region_type) {
		this.region_type = region_type;
	}
	public String getLocation_name() {
		return location_name;
	}
	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}
}