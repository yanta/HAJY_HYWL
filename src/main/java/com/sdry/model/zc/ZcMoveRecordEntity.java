package com.sdry.model.zc;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;

/**
 * 
 * @ClassName:    ZcMoveRecordEntity   
 * @Description:  移库记录表
 * @Author:       zc   
 * @CreateDate:   2019年4月24日 下午3:36:30   
 * @Version:      v1.0
 */
public class ZcMoveRecordEntity {

	private Long id;
	//物料id
	private Long mid;
	//批次
	private String mBatch;
	//目标仓库id
	private Long warehouseId;
	//目标库区id 
	private Long regionId;
	//目标库位id
	private Long locationId;
	//移动日期
	private String moveDate;
	//物料实体
	private Materiel materiel;
	//仓库实体
	private Warehouse warehouse;
	//库区实体
	private WarehouseRegion warehouseRegion;
	//库位实体
	private WarehouseRegionLocation warehouseRegionLocation;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public String getMoveDate() {
		return moveDate;
	}
	public void setMoveDate(String moveDate) {
		this.moveDate = moveDate;
	}
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public Warehouse getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(Warehouse warehouse) {
		this.warehouse = warehouse;
	}
	public WarehouseRegion getWarehouseRegion() {
		return warehouseRegion;
	}
	public void setWarehouseRegion(WarehouseRegion warehouseRegion) {
		this.warehouseRegion = warehouseRegion;
	}
	public WarehouseRegionLocation getWarehouseRegionLocation() {
		return warehouseRegionLocation;
	}
	public void setWarehouseRegionLocation(
			WarehouseRegionLocation warehouseRegionLocation) {
		this.warehouseRegionLocation = warehouseRegionLocation;
	}
}