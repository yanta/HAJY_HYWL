package com.sdry.model.zc;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @ClassName:    ZcSysRoleEntity   
 * @Description:  系统角色
 * @Author:       zc   
 * @CreateDate:   2019年4月16日 上午9:39:56   
 * @Version:      v1.0
 */
public class ZcSysRoleEntity {

	private Long id;
	//角色名称
	private String roleName;
	//角色描述
	private String description;
	//状态：0启用1停用
	private byte status;
	//逻辑删除 1是0否
	private byte isdel;
	//创建日期
	private String createdTime;
	//最后更新日期
	private String updatedTime;
	//备注
	private String remark;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
