package com.sdry.model.zc;
/**
 * 物料仓库关联表
 * @ClassName:    ZcMaterielAndWarehouseEntity   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年5月16日 下午5:27:22   
 * @Version:      v1.0
 */
public class ZcMaterielAndWarehouseEntity {

	private Long id;
	private Long mid;
	private Long wareId;
	private String wareName;
	private Long regionId;
	private String regionName;
	private Long locationId;
	private String locationName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public Long getWareId() {
		return wareId;
	}
	public void setWareId(Long wareId) {
		this.wareId = wareId;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public Long getLocationId() {
		return locationId;
	}
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}
	public String getWareName() {
		return wareName;
	}
	public void setWareName(String wareName) {
		this.wareName = wareName;
	}
	public String getRegionName() {
		return regionName;
	}
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}
	public String getLocationName() {
		return locationName;
	}
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}
}
