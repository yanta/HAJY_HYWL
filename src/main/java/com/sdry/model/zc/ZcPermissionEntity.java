package com.sdry.model.zc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @ClassName:    ZcPermissionEntity   
 * @Description:  权限实体
 * @Author:       zc   
 * @CreateDate:   2019年4月11日 上午9:46:26   
 * @Version:      v1.0
 */
public class ZcPermissionEntity implements Comparable<ZcPermissionEntity>{

	//id
	private Long id;
	//权限名称
	private String permissionName;
	//权限类型
	private String permissionType;
	//URL
	private String url;
	//父ID
	private Long pid;
	//排序
	private int sort;
	//状态：0启用1停用
	private byte status;
	//逻辑删除 1是0否
	private byte isdel;
	//创建日期
	private String createdTime;
	//最后更新日期
	private String updatedTime;
	//备注
	private String remark;
	// 子级菜单
	private List<ZcPermissionEntity> mList = new ArrayList<>();  
	
	public ZcPermissionEntity(Long id, String permissionName,
			String permissionType, String url, Long pid, int sort,
			byte status, byte isdel, String createdTime, String updatedTime,
			String remark, List<ZcPermissionEntity> mList) {
		super();
		this.id = id;
		this.permissionName = permissionName;
		this.permissionType = permissionType;
		this.url = url;
		this.pid = pid;
		this.sort = sort;
		this.status = status;
		this.isdel = isdel;
		this.createdTime = createdTime;
		this.updatedTime = updatedTime;
		this.remark = remark;
		this.mList = mList;
	}
	public ZcPermissionEntity() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPermissionName() {
		return permissionName;
	}
	public void setPermissionName(String permissionName) {
		this.permissionName = permissionName;
	}
	public String getPermissionType() {
		return permissionType;
	}
	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Long getPid() {
		return pid;
	}
	public void setPid(Long pid) {
		this.pid = pid;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public byte getStatus() {
		return status;
	}
	public void setStatus(byte status) {
		this.status = status;
	}
	public byte getIsdel() {
		return isdel;
	}
	public void setIsdel(byte isdel) {
		this.isdel = isdel;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}
	public String getUpdatedTime() {
		return updatedTime;
	}
	public void setUpdatedTime(String updatedTime) {
		this.updatedTime = updatedTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public List<ZcPermissionEntity> getmList() {
		return mList;
	}
	public void setmList(List<ZcPermissionEntity> mList) {
		this.mList = mList;
	}
	@Override
	public int compareTo(ZcPermissionEntity o) {
		// TODO Auto-generated method stub
		return (int)(this.sort - o.getSort());
	}
}
