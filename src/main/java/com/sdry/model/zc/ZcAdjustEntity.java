package com.sdry.model.zc;
/**
 * 库存调整
 * @ClassName:    ZcAdjustEntity   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年4月25日 下午4:16:26   
 * @Version:      v1.0
 */
public class ZcAdjustEntity {
	private Long inventId;
	private int mNum;
	public Long getInventId() {
		return inventId;
	}
	public void setInventId(Long inventId) {
		this.inventId = inventId;
	}
	public int getmNum() {
		return mNum;
	}
	public void setmNum(int mNum) {
		this.mNum = mNum;
	}
}
