package com.sdry.model.zc;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;

/**
 * 不良品库实体
 * @ClassName:    ZcRejectsWarehouseEntity   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年4月25日 下午5:04:10   
 * @Version:      v1.0
 */
public class ZcRejectsWarehouseEntity {

	private Long id;
	//物料id
	private Long mid;
	//批次
	private String mBatch;
	//仓库id
	private Long warehouseId;
	//不良库Id
	private Long regionId;
	//仓库
	private String warehouse;
	//不良库
	private String region;
	//不良数量
	private int totality;
	//不良描述
	private String describe;
	//入库日期
	private String enterDate;
	//入库人
	private Long enterPerson;
	//物料实体
	private Materiel materiel;
	//入库人实体
	private ZcSysUserEntity zcSysUser;
	//总数
	private int countAll;
	private WarehouseRegion warehouseRegion;
	//大小箱
	private int type;
	private String materiel_size;
	private String materiel_properties;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getmBatch() {
		return mBatch;
	}
	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}
	public int getTotality() {
		return totality;
	}
	public void setTotality(int totality) {
		this.totality = totality;
	}
	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}
	public Long getEnterPerson() {
		return enterPerson;
	}
	public void setEnterPerson(Long enterPerson) {
		this.enterPerson = enterPerson;
	}
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public ZcSysUserEntity getZcSysUser() {
		return zcSysUser;
	}
	public void setZcSysUser(ZcSysUserEntity zcSysUser) {
		this.zcSysUser = zcSysUser;
	}
	public Long getWarehouseId() {
		return warehouseId;
	}
	public void setWarehouseId(Long warehouseId) {
		this.warehouseId = warehouseId;
	}
	public Long getRegionId() {
		return regionId;
	}
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public int getCountAll() {
		return countAll;
	}
	public void setCountAll(int countAll) {
		this.countAll = countAll;
	}
	public WarehouseRegion getWarehouseRegion() {
		return warehouseRegion;
	}
	public void setWarehouseRegion(WarehouseRegion warehouseRegion) {
		this.warehouseRegion = warehouseRegion;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getMateriel_size() {
		return materiel_size;
	}
	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}
	public String getMateriel_properties() {
		return materiel_properties;
	}
	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}
}
