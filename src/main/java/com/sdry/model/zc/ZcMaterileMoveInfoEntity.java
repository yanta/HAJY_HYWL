package com.sdry.model.zc;
/**
 * 物料转移数
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年8月7日 下午4:49:07
 * @Version 1.0
 */
public class ZcMaterileMoveInfoEntity {

	private Long mid;
	private String materiel_code;
	private String pici;
	private String tray_code;
	private int num;
	private String enterDate;
	private Long enterPerson;
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getPici() {
		return pici;
	}
	public void setPici(String pici) {
		this.pici = pici;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getEnterDate() {
		return enterDate;
	}
	public void setEnterDate(String enterDate) {
		this.enterDate = enterDate;
	}
	public Long getEnterPerson() {
		return enterPerson;
	}
	public void setEnterPerson(Long enterPerson) {
		this.enterPerson = enterPerson;
	}
	public String getMateriel_code() {
		return materiel_code;
	}
	public void setMateriel_code(String materiel_code) {
		this.materiel_code = materiel_code;
	}
	public String getTray_code() {
		return tray_code;
	}
	public void setTray_code(String tray_code) {
		this.tray_code = tray_code;
	}
	@Override
	public String toString() {
		return "ZcMaterileMoveInfoEntity [mid=" + mid + ", materiel_code=" + materiel_code + ", pici=" + pici
				+ ", tray_code=" + tray_code + ", num=" + num + ", enterDate=" + enterDate + ", enterPerson="
				+ enterPerson + "]";
	}
}
