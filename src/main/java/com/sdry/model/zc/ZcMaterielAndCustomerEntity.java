package com.sdry.model.zc;
/**
 * 物料客户关联表
 * @ClassName:    ZcMaterielAndCustomerEntity   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年5月16日 下午5:27:22   
 * @Version:      v1.0
 */
public class ZcMaterielAndCustomerEntity {

	private Long id;
	private Long mid;
	private Long cid;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
}
