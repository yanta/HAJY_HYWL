package com.sdry.model.zc;

/**
 * 移库仓库
 * @Title:CancellingStockDetailBarCode
 * @Package com.example.administrator.hajy.Model
 * @author lqy
 * @date 2019年5月21日
 * @version 1.0
 */
public class Awarehouse {
	public String getWarehouse_name() {
		return warehouse_name;
	}

	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public String getLocation_name() {
		return location_name;
	}

	public void setLocation_name(String location_name) {
		this.location_name = location_name;
	}

	//仓库名称
	private String warehouse_name;

	//库区名称
	private String region_name;

	//库位名称
	private String location_name;
	
}
