package com.sdry.model.zc;
/**
 * 物料条码与托盘绑定
 * @ClassName:    ZcMaterielAndTrayEntity   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年6月3日 上午10:12:06   
 * @Version:      v1.0
 */
public class ZcMaterielAndTrayEntity {

	private Long id;
	//物料id
	private Long mid;
	//物料批次
	private String mBatch;
	//物料条码
	private String materiel_code;
	//单包装数量
	private int packing_quantity;
	//物料条码数量
	private int code_num;
	//物料数量
	private int num;
	//物料条码数量 下架用
	private int down_code_num;
	//对应库位库存数量
	private int location_count;
	//对应库位的物料是否唯一 0 是 1否
	private String location_type;
	public int getLocation_count() {
		return location_count;
	}
	public void setLocation_count(int location_count) {
		this.location_count = location_count;
	}

	public String getLocation_type() {
		return location_type;
	}

	public void setLocation_type(String location_type) {
		this.location_type = location_type;
	}
	public int getDown_code_num() {
		return down_code_num;
	}

	public void setDown_code_num(int down_code_num) {
		this.down_code_num = down_code_num;
	}

	//托盘条码
	private String tray_code;
	//库位条码
	private String location_code;
	//推荐库位
	private String better_location_name;
	//绑定人
	private Long binding_person;
	//绑定日期
	private String binding_date;
	//人员实体
	private ZcSysUserEntity zcSysUser;
	
	//需要解绑的物料码，解绑时用
	private String new_materiel_code_list;
	//解绑原因，解绑时用
	private String unbindle_reason;
	//下架类型，解绑时用
	private int downType;
	
	private int mNum;
	
	private ZcTrayAndLocationEntity zcTrayAndLocation;
	private int is_ng;
	//原区域
	private String srcRegion;
	//现区域
	private String targetRegion;
	public int getPacking_quantity() {
		return packing_quantity;
	}

	public void setPacking_quantity(int packing_quantity) {
		this.packing_quantity = packing_quantity;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getMid() {
		return mid;
	}

	public void setMid(Long mid) {
		this.mid = mid;
	}

	public String getmBatch() {
		return mBatch;
	}

	public void setmBatch(String mBatch) {
		this.mBatch = mBatch;
	}

	public String getMateriel_code() {
		return materiel_code;
	}

	public void setMateriel_code(String materiel_code) {
		this.materiel_code = materiel_code;
	}

	public int getCode_num() {
		return code_num;
	}

	public void setCode_num(int code_num) {
		this.code_num = code_num;
	}

	public String getTray_code() {
		return tray_code;
	}

	public void setTray_code(String tray_code) {
		this.tray_code = tray_code;
	}

	public Long getBinding_person() {
		return binding_person;
	}

	public void setBinding_person(Long binding_person) {
		this.binding_person = binding_person;
	}

	public String getBinding_date() {
		return binding_date;
	}

	public void setBinding_date(String binding_date) {
		this.binding_date = binding_date;
	}

	public ZcSysUserEntity getZcSysUser() {
		return zcSysUser;
	}

	public void setZcSysUser(ZcSysUserEntity zcSysUser) {
		this.zcSysUser = zcSysUser;
	}

	public String getNew_materiel_code_list() {
		return new_materiel_code_list;
	}

	public void setNew_materiel_code_list(String new_materiel_code_list) {
		this.new_materiel_code_list = new_materiel_code_list;
	}

	public int getmNum() {
		return mNum;
	}

	public void setmNum(int mNum) {
		this.mNum = mNum;
	}

	public ZcTrayAndLocationEntity getZcTrayAndLocation() {
		return zcTrayAndLocation;
	}

	public void setZcTrayAndLocation(ZcTrayAndLocationEntity zcTrayAndLocation) {
		this.zcTrayAndLocation = zcTrayAndLocation;
	}

	public String getLocation_code() {
		return location_code;
	}

	public void setLocation_code(String location_code) {
		this.location_code = location_code;
	}

	public int getDownType() {
		return downType;
	}

	public void setDownType(int downType) {
		this.downType = downType;
	}

	public String getUnbindle_reason() {
		return unbindle_reason;
	}

	public void setUnbindle_reason(String unbindle_reason) {
		this.unbindle_reason = unbindle_reason;
	}

	public String getBetter_location_name() {
		return better_location_name;
	}

	public void setBetter_location_name(String better_location_name) {
		this.better_location_name = better_location_name;
	}

	public int getIs_ng() {
		return is_ng;
	}

	public void setIs_ng(int is_ng) {
		this.is_ng = is_ng;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getSrcRegion() {
		return srcRegion;
	}

	public void setSrcRegion(String srcRegion) {
		this.srcRegion = srcRegion;
	}

	public String getTargetRegion() {
		return targetRegion;
	}

	public void setTargetRegion(String targetRegion) {
		this.targetRegion = targetRegion;
	}
}