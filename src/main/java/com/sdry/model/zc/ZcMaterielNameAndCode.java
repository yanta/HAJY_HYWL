package com.sdry.model.zc;
/**
 * 物料名称和code对应实体
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月26日 上午9:45:44
 * @Version 1.0
 */
public class ZcMaterielNameAndCode {

	private String materiel_name;
	private String materiel_code;
	private int num;
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_code() {
		return materiel_code;
	}
	public void setMateriel_code(String materiel_code) {
		this.materiel_code = materiel_code;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
}
