package	com.sdry.model.tdd;

import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: Unpacking
 *@Description: 拆箱表
 *@Author tdd
 *@Date 2019-05-14 10:56:27
 *@version 1.0
*/
public class Unpacking extends PageBean{
	/** null*/
	private Long uid;
	/** 物料id*/
	private Long mid;
	private Materiel materiel;
	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	/** 拆箱总数*/
	private int unum;
	/** 供应商id*/
	private Long customer_id;
	private Customer customer;
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public void setUid(Long uid){
		this.uid = uid;
	}
	public Long getUid(){
		return uid;
	}
	public void setMid(Long mid){
		this.mid = mid;
	}
	public Long getMid(){
		return mid;
	}
	public void setUnum(int unum){
		this.unum = unum;
	}
	public int getUnum(){
		return unum;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
}
