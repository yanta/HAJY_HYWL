package	com.sdry.model.tdd;

import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: UnpackingTask
 *@Description: 拆箱任务表
 *@Author tdd
 *@Date 2019-05-14 10:58:34
 *@version 1.0
*/
public class UnpackingTask extends PageBean{
	/** null*/
	private Long utid;
	/** 任务单号*/
	private String unumber;
	/** 拆箱id*/
	private Long uid;
	private Unpacking unpacking;
	public Unpacking getUnpacking() {
		return unpacking;
	}
	public void setUnpacking(Unpacking unpacking) {
		this.unpacking = unpacking;
	}
	/** 拆箱数量*/
	private int utnum;
	/** 待发货数量*/
	private int sendNum;
	/** 待发货箱数*/
	private int sendNumX;
	/** 上架箱数*/
	private int upperNumX;
	/** 上架数量*/
	private int upperNum;
	/** 拆箱时间*/
	private String time;
	/** 拆箱人*/
	private String entry;
	/** 备注*/
	private String utremark;
	/** 拆箱状态 0未拆箱  1已拆箱*/
	private String state;
	/** 状态 0待下架 1待拆箱 2待上架 3拆箱完成*/
	private String state1;
	
	/** 供应商id 展示*/
	private Long customer_id;
	/**客户名称 展示*/
    private String customer_name;
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	
	public void setUtid(Long utid){
		this.utid = utid;
	}
	public Long getUtid(){
		return utid;
	}
	public void setUnumber(String unumber){
		this.unumber = unumber== null ? null : unumber.trim();
	}
	public String getUnumber(){
		return unumber;
	}
	public void setUid(Long uid){
		this.uid = uid;
	}
	public Long getUid(){
		return uid;
	}
	public void setUtnum(int utnum){
		this.utnum = utnum;
	}
	public int getUtnum(){
		return utnum;
	}
	public void setSendNum(int sendNum){
		this.sendNum = sendNum;
	}
	public int getSendNum(){
		return sendNum;
	}
	public void setUpperNum(int upperNum){
		this.upperNum = upperNum;
	}
	public int getUpperNum(){
		return upperNum;
	}
	public int getSendNumX() {
		return sendNumX;
	}
	public void setSendNumX(int sendNumX) {
		this.sendNumX = sendNumX;
	}
	public int getUpperNumX() {
		return upperNumX;
	}
	public void setUpperNumX(int upperNumX) {
		this.upperNumX = upperNumX;
	}
	public void setTime(String time){
		this.time = time== null ? null : time.trim();
	}
	public String getTime(){
		return time;
	}
	public void setEntry(String entry){
		this.entry = entry== null ? null : entry.trim();
	}
	public String getEntry(){
		return entry;
	}
	public void setUtremark(String utremark){
		this.utremark = utremark== null ? null : utremark.trim();
	}
	public String getUtremark(){
		return utremark;
	}
	public void setState(String state){
		this.state = state== null ? null : state.trim();
	}
	public String getState(){
		return state;
	}
	public void setState1(String state1){
		this.state1 = state1== null ? null : state1.trim();
	}
	public String getState1(){
		return state1;
	}
}
