package com.sdry.model.cb;

/**
 * 类型枚举
 * @author cb
 * @date 2019-09-03
 */
public enum TypeEnum {

    TYPE_STATE_SH("TYPE_STATE_SH","收货"),
    TYPE_STATE_ZJ("TYPE_STATE_ZJ","质检"),
    TYPE_STATE_SJ("TYPE_STATE_SJ","上架"),
    TYPE_STATE_CK("TYPE_STATE_CK","出库"),
    TYPE_STATE_CX("TYPE_STATE_CX","拆箱"),
    TYPE_STATE_KK("TYPE_STATE_KK",null),


    TYPE_STATE_CG("TYPE_STATE_CG","成功"),
    TYPE_STATE_SB("TYPE_STATE_SB","失败"),

    //已收货，已出库
    TYPE_STATE_YSH("TYPE_STATE_YSH","1"),

    //未出货，未出库
    TYPE_STATE_WSH("TYPE_STATE_WSH","0");


    private String name;

    private String value;

    TypeEnum(String name,String value) {

        this.name = name;

        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
