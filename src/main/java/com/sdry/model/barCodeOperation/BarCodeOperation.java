package	com.sdry.model.barCodeOperation;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.utils.PageBean;

/**
 *
 *@ClassName: BarCodeOperation
 *@Description: 条码操作记录
 *@Author tdd
 *@Date 2019-08-07 17:03:56
 *@version 1.0
*/
public class BarCodeOperation extends PageBean{
	/** 主键*/
	private Long id;
	/** 操作人员id*/
	private Long operator;
	private ZcSysUserEntity user;
	public ZcSysUserEntity getUser() {
		return user;
	}
	public void setUser(ZcSysUserEntity user) {
		this.user = user;
	}
	/** 操作时间*/
	private String operatingTime;
	/** 条码*/
	private String barCode;
	/** 操作类型*/
	private String type;
	/** 操作结果*/
	private String result;
	/** 库区id*/
	private String locationId;
	private WarehouseRegion warehouseRegion;
	public WarehouseRegion getWarehouseRegion() {
		return warehouseRegion;
	}
	public void setWarehouseRegion(WarehouseRegion warehouseRegion) {
		this.warehouseRegion = warehouseRegion;
	}
	/** 批次*/
	private String batch;
	/** 物料id*/
	private Long materielId;
	private Materiel materiel;

	private String param1;
	private String param2;
	private String param3;
	private String param4;
	private String param5;
	private String param6;


	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	public Long getId(){
		return id;
	}
	public void setOperator(Long operator){
		this.operator = operator;
	}
	public Long getOperator(){
		return operator;
	}
	public void setOperatingTime(String operatingTime){
		this.operatingTime = operatingTime== null ? null : operatingTime.trim();
	}
	public String getOperatingTime(){
		return operatingTime;
	}
	public void setBarCode(String barCode){
		this.barCode = barCode== null ? null : barCode.trim();
	}
	public String getBarCode(){
		return barCode;
	}
	public void setType(String type){
		this.type = type== null ? null : type.trim();
	}
	public String getType(){
		return type;
	}
	public void setResult(String result){
		this.result = result== null ? null : result.trim();
	}
	public String getResult(){
		return result;
	}
	public void setLocationId(String locationId){
		this.locationId = locationId;
	}
	public String getLocationId(){
		return locationId;
	}
	public void setBatch(String batch){
		this.batch = batch== null ? null : batch.trim();
	}
	public String getBatch(){
		return batch;
	}
	public void setMaterielId(Long materielId){
		this.materielId = materielId;
	}
	public Long getMaterielId(){
		return materielId;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam4() {
		return param4;
	}

	public void setParam4(String param4) {
		this.param4 = param4;
	}

	public String getParam5() {
		return param5;
	}

	public void setParam5(String param5) {
		this.param5 = param5;
	}

	public String getParam6() {
		return param6;
	}

	public void setParam6(String param6) {
		this.param6 = param6;
	}
}
