package com.sdry.model.lz;

/**
 * @ClassName Department
 * @Description 部門信息
 * @Author lz
 * @Date 2019年4月11日 11:26:17
 * @Version 1.0
 */
public class Department {

    //id
    private Long id;
    //部门编号
    private String dept_num;
    //部门名称
    private String dept_name;
    //部门主管id
    private Long dept_governor;
    //部门主管name
    private String dept_governor_name;
    //部门电话
    private String dept_phone;
    //部门人数
    private String dept_population;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDept_num() {
        return dept_num;
    }

    public void setDept_num(String dept_num) {
        this.dept_num = dept_num;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public Long getDept_governor() {
        return dept_governor;
    }

    public void setDept_governor(Long dept_governor) {
        this.dept_governor = dept_governor;
    }

    public String getDept_governor_name() {
		return dept_governor_name;
	}

	public void setDept_governor_name(String dept_governor_name) {
		this.dept_governor_name = dept_governor_name;
	}

	public String getDept_phone() {
        return dept_phone;
    }

    public void setDept_phone(String dept_phone) {
        this.dept_phone = dept_phone;
    }

    public String getDept_population() {
		return dept_population;
	}

	public void setDept_population(String dept_population) {
		this.dept_population = dept_population;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
