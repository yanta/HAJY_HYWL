package com.sdry.model.lz;
/**
 * 盘点条码
 * @ClassName 
 * @Description 
 * @Author zc
 * @Date 2019年7月22日 下午3:46:15
 * @Version 1.0
 */
public class LzInventoryDetailsCodeEntity {

	private long id;
	//盘点单详情id
	private long invenId;
	//条码
	private String code;
	//理论数量
	private int theory_num;
	//实际数量
	private int reality_num;
	//差异数量
	private int diff_num;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getInvenId() {
		return invenId;
	}
	public void setInvenId(long invenId) {
		this.invenId = invenId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getTheory_num() {
		return theory_num;
	}
	public void setTheory_num(int theory_num) {
		this.theory_num = theory_num;
	}
	public int getReality_num() {
		return reality_num;
	}
	public void setReality_num(int reality_num) {
		this.reality_num = reality_num;
	}
	public int getDiff_num() {
		return diff_num;
	}
	public void setDiff_num(int diff_num) {
		this.diff_num = diff_num;
	}
}
