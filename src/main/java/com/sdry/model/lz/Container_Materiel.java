package com.sdry.model.lz;

/**
 * @ClassName Container_Materiel
 * @Description 物料容器中间表
 * @Author lz
 * @Date 2019年4月8日 17:03:20
 * @Version 1.0
 */
public class Container_Materiel {

    //ID
    private Long id;
    //容器ID
    private Long container_id;
    //物料ID
    private Long materiel_id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContainer_id() {
        return container_id;
    }

    public void setContainer_id(Long container_id) {
        this.container_id = container_id;
    }

    public Long getMateriel_id() {
        return materiel_id;
    }

    public void setMateriel_id(Long materiel_id) {
        this.materiel_id = materiel_id;
    }
}
