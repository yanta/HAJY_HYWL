package com.sdry.model.lz;

/**
 * @ClassName MaterielDetail
 * @Description 物料明细
 * @Author lz
 * @Date 2019年4月11日 11:52:16
 * @Version 1.0
 */
public class MaterielDetail {

    //id
    private Long id;
    //物料id
    private Long materiel_id;
    //供应商id
    private Long supplier_id;
    //零件编号
    private String part_num;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMateriel_id() {
        return materiel_id;
    }

    public void setMateriel_id(Long materiel_id) {
        this.materiel_id = materiel_id;
    }

    public Long getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(Long supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getPart_num() {
        return part_num;
    }

    public void setPart_num(String part_num) {
        this.part_num = part_num;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
