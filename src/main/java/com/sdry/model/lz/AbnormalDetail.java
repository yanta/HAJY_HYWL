package com.sdry.model.lz;

/**
 * 
 * @ClassName AbnormalDetail
 * @Description yichang详细
 * @Author zy
 * @Date 2019年3月8日 11:46:52
 * @Version 1.0
 */
public class AbnormalDetail {

	// ID
	private Long id;
	// 单ID
	private Long aid;
	// 物料id
	private Long mid;
	

	private String knum;

	private String bnum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getAid() {
		return aid;
	}

	public void setAid(Long aid) {
		this.aid = aid;
	}

	public Long getMid() {
		return mid;
	}

	public void setMid(Long mid) {
		this.mid = mid;
	}

	public String getKnum() {
		return knum;
	}

	public void setKnum(String knum) {
		this.knum = knum;
	}

	public String getBnum() {
		return bnum;
	}

	public void setBnum(String bnum) {
		this.bnum = bnum;
	}
  
    
	

}
