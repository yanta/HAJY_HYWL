package com.sdry.model.lz;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * @ClassName Staff
 * @Description 员工信息
 * @Author lz
 * @Date 2019年4月11日 11:29:35
 * @Version 1.0
 */
public class Staff {

    //id
    private Long id;
    //姓名
    private String name;
    //编号
    private String number;
    //部门id
    private Long dept_id;
    //岗位id
    private Long post_id;
    //部门名称
    private String dept_name;
    //岗位名称
    private String post_name;
    //性别
    private String gender;
    //出生日期
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birth_time;
    //身份证号
    private String idCard;
    //政治面貌
    private String politics_status;
    //婚姻状况
    private String marital_status;
    //家庭住址
    private String family_address;
    //电话
    private String phone;
    //银行卡号
    private String card_no;
    //状态
    private String status;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Long getDept_id() {
        return dept_id;
    }

    public void setDept_id(Long dept_id) {
        this.dept_id = dept_id;
    }

    public Long getPost_id() {
        return post_id;
    }

    public void setPost_id(Long post_id) {
        this.post_id = post_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getBirth_time() {
        return birth_time;
    }

    public void setBirth_time(Date birth_time) {
        this.birth_time = birth_time;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getPolitics_status() {
        return politics_status;
    }

    public void setPolitics_status(String politics_status) {
        this.politics_status = politics_status;
    }

    public String getMarital_status() {
        return marital_status;
    }

    public void setMarital_status(String marital_status) {
        this.marital_status = marital_status;
    }

    public String getFamily_address() {
        return family_address;
    }

    public void setFamily_address(String family_address) {
        this.family_address = family_address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
