package com.sdry.model.lz;

/**
 * @ClassName Region
 * @Description 地区表
 * @Author lz
 * @Date 2019年5月27日 10:55:24
 * @Version 1.0
 */
public class Region {
    //id
    private Long id;
    //地区所在城市
    private String region_city;
    //地区名称
    private String region_name;

    public Long getId() {

        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion_city() {
        return region_city;
    }

    public void setRegion_city(String region_city) {
        this.region_city = region_city;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }
}
