package com.sdry.model.lz;
/**
 * @ClassName OutStockOrder
 * @Description 出库单
 * @Author lz
 * @Date 2019年5月20日 14:05:28
 * @Version 1.0
 */
public class OutStockOrder {
	//ID
	private Long id;
	//出库单号
	private String out_stock_num;
	//创建时间
	private String create_date;
	//物料来源（0：待发货区、1：物料表）
	private int source;
	//仓库ID
	private Long warehouse_id;
	//主机厂ID
	private Long customer_id;
	//创建人ID
	private Long user_id;
	//状态(0：未出库、1：已出库)
	private int status;
	//客户单号
	private String customer_order;
	//0:精简出库，1：扫码出库、2：紧急出库
	private int type;
	private String warehouse_name;
	private String warehouse_address;
	private String userName;
	private String phone;
	private String contacts_name;
	private String contacts_tel;
	private String customer_adress;
	private String customer_name;
	private int tab;
	private String start_date;
	private String end_date;
	private String arrival_date;
	private Long deliveryId;
	private String deliveryNumber;
	private String return_reason;
	//erica
	
	private String remark;
	private String flag;
	private String cancellation;
	private String cancellation_time;
	private String restorer ;
	private String restorer_time;
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCancellation() {
		return cancellation;
	}
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}
	public String getCancellation_time() {
		return cancellation_time;
	}
	public void setCancellation_time(String cancellation_time) {
		this.cancellation_time = cancellation_time;
	}
	public String getRestorer() {
		return restorer;
	}
	public void setRestorer(String restorer) {
		this.restorer = restorer;
	}
	public String getRestorer_time() {
		return restorer_time;
	}
	public void setRestorer_time(String restorer_time) {
		this.restorer_time = restorer_time;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOut_stock_num() {
		return out_stock_num;
	}
	public void setOut_stock_num(String out_stock_num) {
		this.out_stock_num = out_stock_num;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public int getSource() {
		return source;
	}
	public void setSource(int source) {
		this.source = source;
	}
	public Long getWarehouse_id() {
		return warehouse_id;
	}
	public void setWarehouse_id(Long warehouse_id) {
		this.warehouse_id = warehouse_id;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public Long getUser_id() {
		return user_id;
	}
	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getWarehouse_address() {
		return warehouse_address;
	}
	public void setWarehouse_address(String warehouse_address) {
		this.warehouse_address = warehouse_address;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContacts_name() {
		return contacts_name;
	}
	public void setContacts_name(String contacts_name) {
		this.contacts_name = contacts_name;
	}
	public String getContacts_tel() {
		return contacts_tel;
	}
	public void setContacts_tel(String contacts_tel) {
		this.contacts_tel = contacts_tel;
	}
	public String getCustomer_adress() {
		return customer_adress;
	}
	public void setCustomer_adress(String customer_adress) {
		this.customer_adress = customer_adress;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getCustomer_order() {
		return customer_order;
	}

	public void setCustomer_order(String customer_order) {
		this.customer_order = customer_order;
	}
	public int getTab() {
		return tab;
	}
	public void setTab(int tab) {
		this.tab = tab;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getArrival_date() {
		return arrival_date;
	}
	public void setArrival_date(String arrival_date) {
		this.arrival_date = arrival_date;
	}
	public Long getDeliveryId() {
		return deliveryId;
	}
	public void setDeliveryId(Long deliveryId) {
		this.deliveryId = deliveryId;
	}
	public String getDeliveryNumber() {
		return deliveryNumber;
	}
	public void setDeliveryNumber(String deliveryNumber) {
		this.deliveryNumber = deliveryNumber;
	}
	public String getReturn_reason() {
		return return_reason;
	}
	public void setReturn_reason(String return_reason) {
		this.return_reason = return_reason;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
}
