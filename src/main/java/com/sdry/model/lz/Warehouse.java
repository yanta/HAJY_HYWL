package com.sdry.model.lz;

import java.util.List;

/**
 * @ClassName Warehouse
 * @Description 仓库信息
 * @Author lz
 * @Date 2019年4月11日 11:09:54
 * @Version 1.0
 */
public class Warehouse {

    //id
    private Long id;
    //地区id
    private Long region_id;
    //地区名称
    private String region_name;
    //仓库管理员id
    private String staff_id;
    //仓库管理员姓名
    private String staff_name;
    //仓库名称
    private String warehouse_name;
    //仓库编号
    private String warehouse_num;
    //仓库属性
    private String warehouse_properties;
    //仓库地址
    private String warehouse_address;
    //仓库拥有者
    private String warehouse_owner;
    //仓库状态
    private int status;
    //创建日期
    private String create_time;
    //创建人id
    private Long create_person;
    //创建人名字
    private String create_person_name;
    //备注
    private String remark;
    //
    private String userName;
    
    private String phone;
    private List<WarehouseRegion> warehouseRegion;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Long getRegion_id() {
		return region_id;
	}

	public void setRegion_id(Long region_id) {
		this.region_id = region_id;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	public String getStaff_name() {
		return staff_name;
	}
	public void setStaff_name(String staff_name) {
		this.staff_name = staff_name;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getWarehouse_num() {
		return warehouse_num;
	}
	public void setWarehouse_num(String warehouse_num) {
		this.warehouse_num = warehouse_num;
	}
	public String getWarehouse_properties() {
		return warehouse_properties;
	}
	public void setWarehouse_properties(String warehouse_properties) {
		this.warehouse_properties = warehouse_properties;
	}
	public String getWarehouse_address() {
		return warehouse_address;
	}
	public void setWarehouse_address(String warehouse_address) {
		this.warehouse_address = warehouse_address;
	}
	public String getWarehouse_owner() {
		return warehouse_owner;
	}
	public void setWarehouse_owner(String warehouse_owner) {
		this.warehouse_owner = warehouse_owner;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreate_time() {
		return create_time;
	}
	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getCreate_person() {
		return create_person;
	}
	public void setCreate_person(Long create_person) {
		this.create_person = create_person;
	}
	public String getCreate_person_name() {
		return create_person_name;
	}
	public void setCreate_person_name(String create_person_name) {
		this.create_person_name = create_person_name;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public List<WarehouseRegion> getWarehouseRegion() {
		return warehouseRegion;
	}
	public void setWarehouseRegion(List<WarehouseRegion> warehouseRegion) {
		this.warehouseRegion = warehouseRegion;
	}
}
