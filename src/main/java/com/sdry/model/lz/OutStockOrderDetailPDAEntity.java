package com.sdry.model.lz;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName OutStockOrderDetailPDAEntity
 * @Description 返回pda实体详细
 * @Author lz
 * @Date 2019年5月22日 11:53:17
 * @Version 1.0
 */
public class OutStockOrderDetailPDAEntity {

	private Long id;
	//供应商名称
	private String customer_name;
	//物料位置
	private String materiel_location;
	//物料实体
	private Materiel materiel;
	//出库单ID
	private Long out_stock_num_id;
	//预计数量
	private int pre_num;
	//实际数量
	private int act_num;
	//规格型号（拼接）
	private String materiel_size_properties;
	//自定义精准码
	private List<CodeMark> receiveCode = new ArrayList<>();
	//单个数量
	private int single_num;
	//大箱数量
	private int big_num;
	//小箱数量
	private int small_num;
	private int actual_quantity;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getMateriel_location() {
		return materiel_location;
	}

	public void setMateriel_location(String materiel_location) {
		this.materiel_location = materiel_location;
	}

	public Materiel getMateriel() {
		return materiel;
	}
	public void setMateriel(Materiel materiel) {
		this.materiel = materiel;
	}
	public Long getOut_stock_num_id() {
		return out_stock_num_id;
	}
	public void setOut_stock_num_id(Long out_stock_num_id) {
		this.out_stock_num_id = out_stock_num_id;
	}
	public int getPre_num() {
		return pre_num;
	}
	public void setPre_num(int pre_num) {
		this.pre_num = pre_num;
	}
	public int getAct_num() {
		return act_num;
	}
	public void setAct_num(int act_num) {
		this.act_num = act_num;
	}

	public String getMateriel_size_properties() {
		return materiel_size_properties;
	}

	public void setMateriel_size_properties(String materiel_size_properties) {
		this.materiel_size_properties = materiel_size_properties;
	}

	public int getSingle_num() {
		return single_num;
	}

	public void setSingle_num(int single_num) {
		this.single_num = single_num;
	}

	public int getBig_num() {
		return big_num;
	}

	public void setBig_num(int big_num) {
		this.big_num = big_num;
	}

	public int getSmall_num() {
		return small_num;
	}

	public void setSmall_num(int small_num) {
		this.small_num = small_num;
	}

	public List<CodeMark> getReceiveCode() {
		return receiveCode;
	}

	public void setReceiveCode(List<CodeMark> receiveCode) {
		this.receiveCode = receiveCode;
	}

	public int getActual_quantity() {
		return actual_quantity;
	}

	public void setActual_quantity(int actual_quantity) {
		this.actual_quantity = actual_quantity;
	}
}
