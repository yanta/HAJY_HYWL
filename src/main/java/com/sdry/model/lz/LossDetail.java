package com.sdry.model.lz;

/**
 * 
 * @ClassName LossDetail
 * @Description 损益详细
 * @Author zy
 * @Date 2019年3月8日 11:46:52
 * @Version 1.0
 */
public class LossDetail {

	// ID
	private Long id;
	// 损益单ID
	private Long loss_spillover_id;
	// 物料id
	private Long mid;
	// 差异数量
	private Long difference_quantity;

	private String type;

	private String reason;
  
	private String approval_status;
	
	private Long materielId;
	private String inventoryOrderId;
	public String getApproval_status() {
		return approval_status;
	}

	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}

	public String getMateriel_num() {
		return materiel_num;
	}

	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}

	public String getMateriel_size() {
		return materiel_size;
	}

	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}

	public String getMateriel_properties() {
		return materiel_properties;
	}

	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}

	// 物料名称
	private String materiel_name;
	// 产品码
	private String materiel_num;
	// 物料规格
	private String materiel_size;
	// 物料属性
	private String materiel_properties;

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	private String customer_name;
	// 库区名称
	private String region_name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLoss_spillover_id() {
		return loss_spillover_id;
	}

	public void setLoss_spillover_id(Long loss_spillover_id) {
		this.loss_spillover_id = loss_spillover_id;
	}

	public Long getMid() {
		return mid;
	}

	public void setMid(Long mid) {
		this.mid = mid;
	}

	public Long getDifference_quantity() {
		return difference_quantity;
	}

	public void setDifference_quantity(Long difference_quantity) {
		this.difference_quantity = difference_quantity;
	}

	public Long getMaterielId() {
		return materielId;
	}

	public void setMaterielId(Long materielId) {
		this.materielId = materielId;
	}

	public String getInventoryOrderId() {
		return inventoryOrderId;
	}

	public void setInventoryOrderId(String inventoryOrderId) {
		this.inventoryOrderId = inventoryOrderId;
	}

}
