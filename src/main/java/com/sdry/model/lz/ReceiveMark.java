package com.sdry.model.lz;

import com.sdry.model.jyy.ReceiveDetail;

/**
 * @ClassName ReceiveMark
 * @Description 收货条码绑定
 * @Author lz
 * @Date 2019年8月13日 17:26:41
 * @Version 1.0
 */
public class ReceiveMark {
	private Long id;
	//收货单详细id
	private Long receive_detail_id;
	private ReceiveDetail receiveDetail;
	public ReceiveDetail getReceiveDetail() {
		return receiveDetail;
	}

	public void setReceiveDetail(ReceiveDetail receiveDetail) {
		this.receiveDetail = receiveDetail;
	}

	//数量
    private int num;
    //物料SQD
    private String SAP_QAD;
    //物料条码
    private String code;
    //二维码路径
    private String twoCodePath;
    //条形码路径
    private String barCodePath;

    public Long getReceive_detail_id() {
		return receive_detail_id;
	}

	public void setReceive_detail_id(Long receive_detail_id) {
		this.receive_detail_id = receive_detail_id;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

	public String getSAP_QAD() {
		return SAP_QAD;
	}

	public void setSAP_QAD(String sAP_QAD) {
		SAP_QAD = sAP_QAD;
	}

	public String getTwoCodePath() {
		return twoCodePath;
	}

	public void setTwoCodePath(String twoCodePath) {
		this.twoCodePath = twoCodePath;
	}

	public String getBarCodePath() {
		return barCodePath;
	}

	public void setBarCodePath(String barCodePath) {
		this.barCodePath = barCodePath;
	}
}
