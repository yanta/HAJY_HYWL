package com.sdry.model.lz;

import java.util.List;

import com.sdry.model.zc.ZcMaterielNameAndCode;

/**
 * @ClassName WarehouseRegionLocation
 * @Description 库位信息
 * @Author lz
 * @Date 2019年4月11日 11:20:10
 * @Version 1.0
 */
public class WarehouseRegionLocation {

    //id
    private Long id;
    //库区id
    private Long region_id;
    //库区名称
    private String region_name;
    //库位名称
    private String location_name;
    //库位编号
    private String location_num;

    //库位总容量
    private String total_capacity;
    //库位已使用容量
    private String alreadyuse_capacity;
    //二维码
    private String rcode;

    //库位状态
    private int location_status;
    //备注
    private String remark;
    /*zc*/
    private List<ZcMaterielNameAndCode> zcMaterielNameAndCode; 
    private String materiel_code; 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRegion_id() {
        return region_id;
    }

    public void setRegion_id(Long region_id) {
        this.region_id = region_id;
    }

    public String getRegion_name() {
        return region_name;
    }

    public void setRegion_name(String region_name) {
        this.region_name = region_name;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getLocation_num() {
        return location_num;
    }

    public void setLocation_num(String location_num) {
        this.location_num = location_num;
    }

    public int getLocation_status() {
        return location_status;
    }

    public void setLocation_status(int location_status) {
        this.location_status = location_status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTotal_capacity() {
        return total_capacity;
    }

    public void setTotal_capacity(String total_capacity) {
        this.total_capacity = total_capacity;
    }

    public String getAlreadyuse_capacity() {
        return alreadyuse_capacity;
    }

    public void setAlreadyuse_capacity(String alreadyuse_capacity) {
        this.alreadyuse_capacity = alreadyuse_capacity;
    }

    public String getRcode() {
        return rcode;
    }

    public void setRcode(String rcode) {
        this.rcode = rcode;
    }

	public List<ZcMaterielNameAndCode> getZcMaterielNameAndCode() {
		return zcMaterielNameAndCode;
	}

	public void setZcMaterielNameAndCode(
			List<ZcMaterielNameAndCode> zcMaterielNameAndCode) {
		this.zcMaterielNameAndCode = zcMaterielNameAndCode;
	}

	public String getMateriel_code() {
		return materiel_code;
	}

	public void setMateriel_code(String materiel_code) {
		this.materiel_code = materiel_code;
	}
}
