package com.sdry.model.lz;
/**
 * @ClassName WaitSendArea
 * @Description 待发货区 
 * @Author lz
 * @Date 2019年5月21日 17:17:29
 * @Version 1.0
 */
public class WaitSendArea {

	private Long id;
	//物料ID
	private Long materiel_id;
	//物料数量
	private int materiel_num;
	//拆箱人
	private String move_man;
	//拆箱日期
	private String move_date;
	//出库状态
	private int status;
	/** 供应商id*/
	private Long customer_id;
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getMateriel_id() {
		return materiel_id;
	}
	public void setMateriel_id(Long materiel_id) {
		this.materiel_id = materiel_id;
	}
	public int getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(int materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getMove_man() {
		return move_man;
	}
	public void setMove_man(String move_man) {
		this.move_man = move_man;
	}
	public String getMove_date() {
		return move_date;
	}
	public void setMove_date(String move_date) {
		this.move_date = move_date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}	
}
