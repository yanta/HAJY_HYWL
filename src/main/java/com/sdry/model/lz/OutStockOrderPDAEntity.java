package com.sdry.model.lz;
/**
 * @ClassName OutStockOrderPDAEntity
 * @Description 返回pda实体
 * @Author lz
 * @Date 2019年5月22日 11:49:42
 * @Version 1.0
 */
public class OutStockOrderPDAEntity {

	//出库单id
	private Long id;
	//退库单号
	private String out_stock_num;
	//仓库名称
	private String warehouse_name;
	//主机厂名称
	private String customer_name;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOut_stock_num() {
		return out_stock_num;
	}
	public void setOut_stock_num(String out_stock_num) {
		this.out_stock_num = out_stock_num;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	
	
}
