package com.sdry.model.lz;
/**
 * @ClassName LossSpillover
 * @Description 损溢管理
 * @Author lz
 * @Date 2019年3月8日 15:53:28
 * @Version 1.0
 */
public class LossSpillover {

	//ID
	private Long id;
	//盘点单
	private String inventory_order;
	//损益单单号
	private String loss_spillover_order;
	//损溢类型
	private String loss_spillover_type;
	//盘点前
	private Long inventory_before;
	//盘点后
	private Long inventory_after;
	//盘点差异
	private Long inventory_different;
	//差异备注
	private String different_remark;
	//审批人ID
	private String approval_id;
	//审批人名称
	private String approval_name;
	//物料名称
	private String materialName;
	//物料编号
	private String materialNumber;
	//创建人
	private String creater;
	//创建时间
	private String creater_date;
	//备注
	private String remark01;
	//抄送人ID
	private String copy_id;
	//抄送人姓名
	private String copy_name;
	//审批状态  0 未审批   1 审批通过  2 审批驳回
	private String approval_status;
	//审批意见
	private String approval_idea;
	//创建人姓名
	private String createrName;
	
	private String tab;
	
	
	
	public String getTab() {
		return tab;
	}
	public void setTab(String tab) {
		this.tab = tab;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInventory_order() {
		return inventory_order;
	}
	public void setInventory_order(String inventory_order) {
		this.inventory_order = inventory_order;
	}
	public String getLoss_spillover_order() {
		return loss_spillover_order;
	}
	public void setLoss_spillover_order(String loss_spillover_order) {
		this.loss_spillover_order = loss_spillover_order;
	}
	public String getLoss_spillover_type() {
		return loss_spillover_type;
	}
	public void setLoss_spillover_type(String loss_spillover_type) {
		this.loss_spillover_type = loss_spillover_type;
	}
	public Long getInventory_before() {
		return inventory_before;
	}
	public void setInventory_before(Long inventory_before) {
		this.inventory_before = inventory_before;
	}
	public Long getInventory_after() {
		return inventory_after;
	}
	public void setInventory_after(Long inventory_after) {
		this.inventory_after = inventory_after;
	}
	public Long getInventory_different() {
		return inventory_different;
	}
	public void setInventory_different(Long inventory_different) {
		this.inventory_different = inventory_different;
	}
	public String getDifferent_remark() {
		return different_remark;
	}
	public void setDifferent_remark(String different_remark) {
		this.different_remark = different_remark;
	}
	
	public String getMaterialName() {
		return materialName;
	}
	public void setMaterialName(String materialName) {
		this.materialName = materialName;
	}
	public String getMaterialNumber() {
		return materialNumber;
	}
	public void setMaterialNumber(String materialNumber) {
		this.materialNumber = materialNumber;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public String getCreater_date() {
		return creater_date;
	}
	public void setCreater_date(String creater_date) {
		this.creater_date = creater_date;
	}
	public String getRemark01() {
		return remark01;
	}
	public void setRemark01(String remark01) {
		this.remark01 = remark01;
	}
	public String getApproval_name() {
		return approval_name;
	}
	public void setApproval_name(String approval_name) {
		this.approval_name = approval_name;
	}

	public String getCopy_name() {
		return copy_name;
	}
	public void setCopy_name(String copy_name) {
		this.copy_name = copy_name;
	}
	public String getApproval_status() {
		return approval_status;
	}
	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}
	public String getApproval_idea() {
		return approval_idea;
	}
	public void setApproval_idea(String approval_idea) {
		this.approval_idea = approval_idea;
	}
	public String getCreaterName() {
		return createrName;
	}
	public void setCreaterName(String createrName) {
		this.createrName = createrName;
	}
	public String getApproval_id() {
		return approval_id;
	}
	public void setApproval_id(String approval_id) {
		this.approval_id = approval_id;
	}
	public String getCopy_id() {
		return copy_id;
	}
	public void setCopy_id(String copy_id) {
		this.copy_id = copy_id;
	}
}
