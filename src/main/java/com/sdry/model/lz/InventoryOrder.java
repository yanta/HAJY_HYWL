package com.sdry.model.lz;
/**
 * 
 * @ClassName InventoryOrder
 * @Description 盘点单
 * @Author lz
 * @Date 2019年3月8日 11:50:36
 * @Version 1.0
 */
public class InventoryOrder {

	//ID
	private Long id;
	//盘点单号
	private String inventory_order_num;
	//盘点类型
	private String inventory_type;
	//盘点人
	private Long inventory_man;
	//盘点时间
	private String inventory_time;
	//备注
	private String remark;
	//盘点仓库
	private Long inventory_warehouse;
	
	private String userName;
	private String warehouse_name;
	//供应商ID
	private Long customer_id;
	private String customer_name;
	//状态：0、未盘点，1、已盘点， 2、已复盘 ，3、已确认
	private String tab;
	
	//------------jyy-start---------------
	/**复核人*/
	private Long checked_man;
	private String checkedMan;
	/**复核时间*/
	private String checked_time;
	/**确认人*/
	private Long confirmed_man;
	private String confirmedMan;
	/**确认时间*/
	private String confirmed_time;
	//------------jyy-end---------------
	//erica
		
	private String flag;
	private String cancellation;
	private String cancellation_time;
	private String restorer ;
	private String restorer_time;
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getCancellation() {
		return cancellation;
	}
	public void setCancellation(String cancellation) {
		this.cancellation = cancellation;
	}
	public String getCancellation_time() {
		return cancellation_time;
	}
	public void setCancellation_time(String cancellation_time) {
		this.cancellation_time = cancellation_time;
	}
	public String getRestorer() {
		return restorer;
	}
	public void setRestorer(String restorer) {
		this.restorer = restorer;
	}
	public String getRestorer_time() {
		return restorer_time;
	}
	public void setRestorer_time(String restorer_time) {
		this.restorer_time = restorer_time;
	}
	public String getCheckedMan() {
		return checkedMan;
	}
	public void setCheckedMan(String checkedMan) {
		this.checkedMan = checkedMan;
	}
	public String getConfirmedMan() {
		return confirmedMan;
	}
	public void setConfirmedMan(String confirmedMan) {
		this.confirmedMan = confirmedMan;
	}
	public Long getChecked_man() {
		return checked_man;
	}
	public void setChecked_man(Long checked_man) {
		this.checked_man = checked_man;
	}
	public String getChecked_time() {
		return checked_time;
	}
	public void setChecked_time(String checked_time) {
		this.checked_time = checked_time;
	}
	public Long getConfirmed_man() {
		return confirmed_man;
	}
	public void setConfirmed_man(Long confirmed_man) {
		this.confirmed_man = confirmed_man;
	}
	public String getConfirmed_time() {
		return confirmed_time;
	}
	public void setConfirmed_time(String confirmed_time) {
		this.confirmed_time = confirmed_time;
	}
	public String getTab() {
		return tab;
	}
	public void setTab(String tab) {
		this.tab = tab;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getInventory_order_num() {
		return inventory_order_num;
	}
	public void setInventory_order_num(String inventory_order_num) {
		this.inventory_order_num = inventory_order_num;
	}
	public String getInventory_type() {
		return inventory_type;
	}
	public void setInventory_type(String inventory_type) {
		this.inventory_type = inventory_type;
	}
	public Long getInventory_man() {
		return inventory_man;
	}
	public void setInventory_man(Long inventory_man) {
		this.inventory_man = inventory_man;
	}
	public String getInventory_time() {
		return inventory_time;
	}
	public void setInventory_time(String inventory_time) {
		this.inventory_time = inventory_time;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public Long getInventory_warehouse() {
		return inventory_warehouse;
	}
	public void setInventory_warehouse(Long inventory_warehouse) {
		this.inventory_warehouse = inventory_warehouse;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getWarehouse_name() {
		return warehouse_name;
	}
	public void setWarehouse_name(String warehouse_name) {
		this.warehouse_name = warehouse_name;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
}
