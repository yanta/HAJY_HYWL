package com.sdry.model.lz;
/**
 * 
 * @ClassName WarehouseRegionType
 * @Description 库区类型表
 * @Author lz
 * @Date 2019年4月19日 16:04:09
 * @Version 1.0
 */
public class WarehouseRegionType {

	private Long id;
	private String region_type;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegion_type() {
		return region_type;
	}
	public void setRegion_type(String region_type) {
		this.region_type = region_type;
	}
	
}
