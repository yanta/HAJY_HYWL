package com.sdry.model.lz;
/**
 * @ClassName AbnormalAdjustment
 * @Description 异常调整(订单补发)
 * @Author lz
 * @Date 2019年4月23日 10:22:27
 * @Version 1.0
 */
public class AbnormalAdjustment {

	//id
	private Long id;
	//出库单号
	private String out_stock_order_num;
	//补发日期
	private String replenish_time;
	//补发人员
	private String replenish_man;
	//补发原因
	private String replenish_reason;
	//补发状态
	private String remark;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOut_stock_order_num() {
		return out_stock_order_num;
	}
	public void setOut_stock_order_num(String out_stock_order_num) {
		this.out_stock_order_num = out_stock_order_num;
	}
	public String getReplenish_time() {
		return replenish_time;
	}
	public void setReplenish_time(String replenish_time) {
		this.replenish_time = replenish_time;
	}
	public String getReplenish_man() {
		return replenish_man;
	}
	public void setReplenish_man(String replenish_man) {
		this.replenish_man = replenish_man;
	}
	public String getReplenish_reason() {
		return replenish_reason;
	}
	public void setReplenish_reason(String replenish_reason) {
		this.replenish_reason = replenish_reason;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
}
