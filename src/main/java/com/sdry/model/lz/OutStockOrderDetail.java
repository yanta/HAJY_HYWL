package com.sdry.model.lz;
/**
 * @ClassName OutStockOrderDetail
 * @Description 出库单详细
 * @Author lz
 * @Date 2019年3月8日 11:45:14
 * @Version 1.0
 */
public class OutStockOrderDetail {
	//id
	private Long id;
	//订单id
	private Long out_stock_num_id;
	//物料id
	private Long material_id;
	//物料名称
	private String materiel_name;
	//供应商名称
	private String  customer_name;
	//产品码
	private String materiel_num;
	//简码
	private String brevity_num;
	//物料规格
	private String materiel_size;
	//物料型号
	private String materiel_properties;
	//需求数量
	private int material_quantity;
	//实际数量
	private int actual_quantity;
	//物料状态
	private int status;
	//供应商id
	private Long customer_id;
	/*======================*/
	private int mNum_sum;
	/*======================*/
	private int good_num;
	/*==========库位推荐============*/
	private String recommend_stock;
	//待发货区数量
	private int single_num;
	//大箱数量
	private int big_num;
	//可出库总数
	private int small_num;
	//签收数量
	private int arrival_quantity;
	//退回数量
	private int return_quantity;
	//退回原因
	private String return_reason;
	private int act_num;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOut_stock_num_id() {
		return out_stock_num_id;
	}
	public void setOut_stock_num_id(Long out_stock_num_id) {
		this.out_stock_num_id = out_stock_num_id;
	}
	public Long getMaterial_id() {
		return material_id;
	}
	public void setMaterial_id(Long material_id) {
		this.material_id = material_id;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public int getMaterial_quantity() {
		return material_quantity;
	}
	public void setMaterial_quantity(int material_quantity) {
		this.material_quantity = material_quantity;
	}
	public int getActual_quantity() {
		return actual_quantity;
	}
	public void setActual_quantity(int actual_quantity) {
		this.actual_quantity = actual_quantity;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getMateriel_size() {
		return materiel_size;
	}
	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}
	public String getMateriel_properties() {
		return materiel_properties;
	}
	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}

	public int getSingle_num() {
		return single_num;
	}

	public void setSingle_num(int single_num) {
		this.single_num = single_num;
	}

	public int getBig_num() {
		return big_num;
	}

	public void setBig_num(int big_num) {
		this.big_num = big_num;
	}

	public int getSmall_num() {
		return small_num;
	}

	public void setSmall_num(int small_num) {
		this.small_num = small_num;
	}
	public int getArrival_quantity() {
		return arrival_quantity;
	}
	public void setArrival_quantity(int arrival_quantity) {
		this.arrival_quantity = arrival_quantity;
	}
	public int getReturn_quantity() {
		return return_quantity;
	}
	public void setReturn_quantity(int return_quantity) {
		this.return_quantity = return_quantity;
	}
	public int getAct_num() {
		return act_num;
	}
	public void setAct_num(int act_num) {
		this.act_num = act_num;
	}
	public int getmNum_sum() {
		return mNum_sum;
	}
	public void setmNum_sum(int mNum_sum) {
		this.mNum_sum = mNum_sum;
	}
	public int getGood_num() {
		return good_num;
	}
	public void setGood_num(int good_num) {
		this.good_num = good_num;
	}
	public String getRecommend_stock() {
		return recommend_stock;
	}
	public void setRecommend_stock(String recommend_stock) {
		this.recommend_stock = recommend_stock;
	}
	public String getReturn_reason() {
		return return_reason;
	}
	public void setReturn_reason(String return_reason) {
		this.return_reason = return_reason;
	}
	
}
