package com.sdry.model.lz;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @ClassName Customer
 * @Description 客户信息
 * @Author lz
 * @Date 2019年4月11日 11:45:36
 * @Version 1.0
 */
public class Customer {

    //id
    private Long id;
    //联系人id
    private Long contacts_id;
    //联系人姓名
    private String contacts_name;
    //客户名称
    private String customer_name;
    //客户编号
    private String customer_num;
    //客户类型
    private String customer_type;
    //客户所在地区
    private String customer_region;
    //客户地址
    private String customer_adress;
    //客户邮箱
    private String customer_email;
    //客户邮编
    private String customer_zipcode;
    //状态
    private int status;
    //创建日期
    private String create_time;
    //备注
    private String remark;

    //库区id（1,2,3）
    private String region_idArr;
  //erica

    private String barcode;
    
    private String photo;

    //库区名称（1,2,3）
    private String region_name;
    public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getContacts_id() {
        return contacts_id;
    }

    public void setContacts_id(Long contacts_id) {
        this.contacts_id = contacts_id;
    }

    public String getContacts_name() {
        return contacts_name;
    }

    public void setContacts_name(String contacts_name) {
        this.contacts_name = contacts_name;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_num() {
        return customer_num;
    }

    public void setCustomer_num(String customer_num) {
        this.customer_num = customer_num;
    }

    public String getCustomer_type() {
        return customer_type;
    }

    public void setCustomer_type(String customer_type) {
        this.customer_type = customer_type;
    }

    public String getCustomer_region() {
        return customer_region;
    }

    public void setCustomer_region(String customer_region) {
        this.customer_region = customer_region;
    }

    public String getCustomer_adress() {
        return customer_adress;
    }

    public void setCustomer_adress(String customer_adress) {
        this.customer_adress = customer_adress;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_zipcode() {
        return customer_zipcode;
    }

    public void setCustomer_zipcode(String customer_zipcode) {
        this.customer_zipcode = customer_zipcode;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
    //联系人电话
    private String contacts_tel;
    //联系人职位
    private String contacts_position;
	public String getContacts_tel() {
		return contacts_tel;
	}
	public void setContacts_tel(String contacts_tel) {
		this.contacts_tel = contacts_tel;
	}
	public String getContacts_position() {
		return contacts_position;
	}
	public void setContacts_position(String contacts_position) {
		this.contacts_position = contacts_position;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public String getRegion_idArr() {
		return region_idArr;
	}

	public void setRegion_idArr(String region_idArr) {
		this.region_idArr = region_idArr;
	}
}
