package com.sdry.model.lz;
import com.sdry.utils.PageBean;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
/**
 * @ClassName Materiel
 * @Description 物料信息
 * @Author lz
 * @Date 2019年4月11日 11:54:41
 * @Version 1.0
 */
public class MaterielInStock extends PageBean{

	//物料批次
	private Set<String> batch = new HashSet<>();
    //id
    private Long id;
    //容器id
    private Long container_id;
    //容器名称
    private String container_name;
    //物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //简码
    private String brevity_num;
    //物料规格
    private String materiel_size;
    //物料型号
    private String materiel_properties;
    //保质期
    private String quality_guarantee_period;
    //库龄
    private String allowable_storage_days;
    //单位(个、条)
    private String unit;
    //体积
    private String volume;
    //重量
    private String weight;
    //出厂日期
    //@DateTimeFormat(pattern = "yyyy-MM-dd")
    private String out_date;
    //编码规则
    private String code_rule;
    //收货地址
    private String delivery_address;
    //出库频率
    private String outgoing_frequency;
    //包装数量
    private String packing_quantity;
    //是否拆箱
    private String is_devanning;
    //拆箱数
    private String devanning_num;
    //入库类型(精准入库、普通入库)
    private String storage_type;
    //条码类型(1、系统码 2、客户码1 3、客户码2 4、无码(输入简码))
    private String barcode_type;
    //顺序号
    private String sequence_number;
    //放置类型
    private String placement_type;
    //拆箱后容器id
    private Long removal_container;
    //拆箱后容器名称
    private String removal_container_name;
    //拆箱后产品码
    private String devanning_code;
    //备注
    private String remark;
    //需要拆箱数量
    private int ycxnum;
    //供应商id
    private Long customer_id;
    //供应商名称
    private String customer_name;
    //(供应商实体)
    private Customer customer;
    //库区id
    private Long region_id;
    //库区名称
    private String region_name;
    //(库区实体)
    private WarehouseRegion region;
    //上限值
    private int upper_value;
    //下限值
    private int lower_value;
    //zc----原库区id
    private Long src_region_id;
    //张月添加
    private Long mid;
    private String knum;
    private String bnum;
    
	public int getYcxnum() {
		return ycxnum;
	}
	public void setYcxnum(int ycxnum) {
		this.ycxnum = ycxnum;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getContainer_id() {
		return container_id;
	}
	public void setContainer_id(Long container_id) {
		this.container_id = container_id;
	}
	public String getContainer_name() {
		return container_name;
	}
	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}
	public String getMateriel_name() {
		return materiel_name;
	}
	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}
	public String getMateriel_num() {
		return materiel_num;
	}
	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}
	public String getBrevity_num() {
		return brevity_num;
	}
	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}
	public String getMateriel_size() {
		return materiel_size;
	}
	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}
	public String getMateriel_properties() {
		return materiel_properties;
	}
	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}
	public String getQuality_guarantee_period() {
		return quality_guarantee_period;
	}
	public void setQuality_guarantee_period(String quality_guarantee_period) {
		this.quality_guarantee_period = quality_guarantee_period;
	}
	public String getAllowable_storage_days() {
		return allowable_storage_days;
	}
	public void setAllowable_storage_days(String allowable_storage_days) {
		this.allowable_storage_days = allowable_storage_days;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	public String getWeight() {
		return weight;
	}
	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getOut_date() {
		return out_date;
	}

	public void setOut_date(String out_date) {
		this.out_date = out_date;
	}

	public String getCode_rule() {
		return code_rule;
	}
	public void setCode_rule(String code_rule) {
		this.code_rule = code_rule;
	}
	public String getDelivery_address() {
		return delivery_address;
	}
	public void setDelivery_address(String delivery_address) {
		this.delivery_address = delivery_address;
	}
	public String getOutgoing_frequency() {
		return outgoing_frequency;
	}
	public void setOutgoing_frequency(String outgoing_frequency) {
		this.outgoing_frequency = outgoing_frequency;
	}
	public String getPacking_quantity() {
		return packing_quantity;
	}
	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}
	public String getIs_devanning() {
		return is_devanning;
	}
	public void setIs_devanning(String is_devanning) {
		this.is_devanning = is_devanning;
	}
	public String getDevanning_num() {
		return devanning_num;
	}
	public void setDevanning_num(String devanning_num) {
		this.devanning_num = devanning_num;
	}
	public String getStorage_type() {
		return storage_type;
	}
	public void setStorage_type(String storage_type) {
		this.storage_type = storage_type;
	}
	public String getBarcode_type() {
		return barcode_type;
	}
	public void setBarcode_type(String barcode_type) {
		this.barcode_type = barcode_type;
	}
	public String getSequence_number() {
		return sequence_number;
	}
	public void setSequence_number(String sequence_number) {
		this.sequence_number = sequence_number;
	}
	public String getPlacement_type() {
		return placement_type;
	}
	public void setPlacement_type(String placement_type) {
		this.placement_type = placement_type;
	}
	public Long getRemoval_container() {
		return removal_container;
	}
	public void setRemoval_container(Long removal_container) {
		this.removal_container = removal_container;
	}
	public String getRemoval_container_name() {
		return removal_container_name;
	}
	public void setRemoval_container_name(String removal_container_name) {
		this.removal_container_name = removal_container_name;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getDevanning_code() {
		return devanning_code;
	}
	public void setDevanning_code(String devanning_code) {
		this.devanning_code = devanning_code;
	}
	public Long getCustomer_id() {
		return customer_id;
	}
	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Long getRegion_id() {
		return region_id;
	}
	public void setRegion_id(Long region_id) {
		this.region_id = region_id;
	}
	public WarehouseRegion getRegion() {
		return region;
	}
	public void setRegion(WarehouseRegion region) {
		this.region = region;
	}
	public int getUpper_value() {
		return upper_value;
	}

	public void setUpper_value(int upper_value) {
		this.upper_value = upper_value;
	}

	public int getLower_value() {
		return lower_value;
	}

	public void setLower_value(int lower_value) {
		this.lower_value = lower_value;
	}
	public String getCustomer_name() {
		return customer_name;
	}
	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}
	public String getRegion_name() {
		return region_name;
	}
	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}
	public Long getSrc_region_id() {
		return src_region_id;
	}
	public void setSrc_region_id(Long src_region_id) {
		this.src_region_id = src_region_id;
	}
	public Long getMid() {
		return mid;
	}
	public void setMid(Long mid) {
		this.mid = mid;
	}
	public String getKnum() {
		return knum;
	}
	public void setKnum(String knum) {
		this.knum = knum;
	}
	public String getBnum() {
		return bnum;
	}
	public void setBnum(String bnum) {
		this.bnum = bnum;
	}
	public Set<String> getBatch() {
		return batch;
	}
	public void setBatch(Set<String> batch) {
		this.batch = batch;
	}
}
