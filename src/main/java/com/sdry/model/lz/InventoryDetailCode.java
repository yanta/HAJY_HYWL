package com.sdry.model.lz;

/**
 * @ClassName InventoryDetailCode
 * @Description 盘点详细条码
 * @Author jyy
 * @Date 2019-08-07 15:23:50
 * @Version 2.0
 */
public class InventoryDetailCode {
	
	/**主键id*/
	private Long id;
	/**盘点详情id*/
	private Long invenId;
	/**条码*/
	private String code;
	/**库存数量（帐面、理论）*/
	private int theory_num;
	/**实际盘点数量*/
	private int reality_num;
	/**差异*/
	private int diff_num;
	/**库位编号*/
	private String location_num;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getInvenId() {
		return invenId;
	}
	public void setInvenId(Long invenId) {
		this.invenId = invenId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getTheory_num() {
		return theory_num;
	}
	public void setTheory_num(int theory_num) {
		this.theory_num = theory_num;
	}
	public int getReality_num() {
		return reality_num;
	}
	public void setReality_num(int reality_num) {
		this.reality_num = reality_num;
	}
	public int getDiff_num() {
		return diff_num;
	}
	public void setDiff_num(int diff_num) {
		this.diff_num = diff_num;
	}
	public String getLocation_num() {
		return location_num;
	}
	public void setLocation_num(String location_num) {
		this.location_num = location_num;
	}
	@Override
	public String toString() {
		return "InventoryDetailCode [id=" + id + ", invenId=" + invenId + ", code=" + code + ", theory_num="
				+ theory_num + ", reality_num=" + reality_num + ", diff_num=" + diff_num + ", location_num="
				+ location_num + "]";
	}
	
}
