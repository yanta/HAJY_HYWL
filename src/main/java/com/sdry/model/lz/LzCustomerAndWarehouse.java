package com.sdry.model.lz;

/**
 * @ClassName LzCustomerAndWarehouse
 * @Description 客户表绑定仓库
 * @Author lz
 * @Date 2019年5月29日 09:29:23
 * @Version 1.0
 */
public class LzCustomerAndWarehouse {
    //id
    private Long id;
    //客户id
    private Long cid;
    //仓库id
    private Long warehouse_id;
    //库区id
    private Long region_id;
    //是否空置0空1不空
    private byte is_empty;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    public Long getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(Long warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public Long getRegion_id() {
        return region_id;
    }

    public void setRegion_id(Long region_id) {
        this.region_id = region_id;
    }

	public byte getIs_empty() {
		return is_empty;
	}

	public void setIs_empty(byte is_empty) {
		this.is_empty = is_empty;
	}
}
