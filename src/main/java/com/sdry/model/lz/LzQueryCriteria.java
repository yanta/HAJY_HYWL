package com.sdry.model.lz;

/**
 * @ClassName LzQueryCriteria
 * @Description 分页查询条件
 * @Author lz
 * @Date 2019年3月5日 14:10:36
 * @Version 1.0
 */
public class LzQueryCriteria {
    //页码
    private int page;
    //每页条数
    private int limit;
    //模糊查询条件1
    private String keyword01;
    //模糊查询条件2
    private String keyword02;
    //模糊查询条件3
    private String keyword03;
    //模糊查询条件4
    private String keyword04;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getKeyword01() {
        return keyword01;
    }

    public void setKeyword01(String keyword01) {
        this.keyword01 = keyword01;
    }

    public String getKeyword02() {
        return keyword02;
    }

    public void setKeyword02(String keyword02) {
        this.keyword02 = keyword02;
    }

	public String getKeyword03() {
		return keyword03;
	}

	public void setKeyword03(String keyword03) {
		this.keyword03 = keyword03;
	}

	public String getKeyword04() {
		return keyword04;
	}

	public void setKeyword04(String keyword04) {
		this.keyword04 = keyword04;
	}
}
