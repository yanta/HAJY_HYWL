package com.sdry.model.lz;
/**
 * 
 * @ClassName LoadingProcess
 * @Description 装车处理
 * @Author lz
 * @Date 2019年3月8日 11:55:27
 * @Version 1.0
 */
public class LoadingProcess {

	//id
	private Long id;
	//出库/补货单id
	private Long out_order_id;
	//装车类型
	private String loading_process_type;
	//装车状态
	private String loading_process_status;
	//装车日期
	private String loading_process_date;
	//司机id
	private String driver_id;
	//车辆id
	private Long car_id;
	//备注
	private String remark;
	//装车单号
	private String loading_process_num;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getOut_order_id() {
		return out_order_id;
	}
	public void setOut_order_id(Long out_order_id) {
		this.out_order_id = out_order_id;
	}
	public String getLoading_process_type() {
		return loading_process_type;
	}
	public void setLoading_process_type(String loading_process_type) {
		this.loading_process_type = loading_process_type;
	}
	public String getLoading_process_status() {
		return loading_process_status;
	}
	public void setLoading_process_status(String loading_process_status) {
		this.loading_process_status = loading_process_status;
	}
	public String getLoading_process_date() {
		return loading_process_date;
	}
	public void setLoading_process_date(String loading_process_date) {
		this.loading_process_date = loading_process_date;
	}
	
	public String getDriver_id() {
		return driver_id;
	}
	public void setDriver_id(String driver_id) {
		this.driver_id = driver_id;
	}
	public Long getCar_id() {
		return car_id;
	}
	public void setCar_id(Long car_id) {
		this.car_id = car_id;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getLoading_process_num() {
		return loading_process_num;
	}
	public void setLoading_process_num(String loading_process_num) {
		this.loading_process_num = loading_process_num;
	}
}
