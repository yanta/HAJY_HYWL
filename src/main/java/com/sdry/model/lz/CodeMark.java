package com.sdry.model.lz;

import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.web.controller.jyy.JyJumpPageMenu;

/**
 * @ClassName CodeMark
 * @Description 物料详细
 * @Author lz
 * @Date 2019年7月10日 13:49:50
 * @Version 1.0
 */
public class CodeMark {

	//收货详细表每种物料的id
	private Long receive_detail_id;
	//条码
	private String code;
	//单箱数量
	private int num;
	//0:良品 1：不良
	private int is_ng;
	//收货单实体
	private ReceiveDetailQuality receiveDetailQuality;
	//物料托盘
	private ZcMaterielAndTrayEntity zcMaterielAndTray;
	public Long getReceive_detail_id() {
		return receive_detail_id;
	}

	public void setReceive_detail_id(Long receive_detail_id) {
		this.receive_detail_id = receive_detail_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getIs_ng() {
		return is_ng;
	}

	public void setIs_ng(int is_ng) {
		this.is_ng = is_ng;
	}

	public ReceiveDetailQuality getReceiveDetailQuality() {
		return receiveDetailQuality;
	}

	public void setReceiveDetailQuality(ReceiveDetailQuality receiveDetailQuality) {
		this.receiveDetailQuality = receiveDetailQuality;
	}

	public ZcMaterielAndTrayEntity getZcMaterielAndTray() {
		return zcMaterielAndTray;
	}

	public void setZcMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTray) {
		this.zcMaterielAndTray = zcMaterielAndTray;
	}
}