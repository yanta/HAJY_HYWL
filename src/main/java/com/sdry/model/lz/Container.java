package com.sdry.model.lz;

/**
 * @ClassName Container
 * @Description 容器信息
 * @Author lz
 * @Date 2019年4月16日 11:21:28
 * @Version 1.0
 */
public class Container {

    //id
    private Long id;
    //容器名称
    private String container_name;
    //容器编号
    private String container_num;
    //容器尺寸
    private String container_size;
    //容器状态
    private String container_status;
    //二维码
    private String rcode;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContainer_name() {
        return container_name;
    }

    public void setContainer_name(String container_name) {
        this.container_name = container_name;
    }

    public String getContainer_num() {
        return container_num;
    }

    public void setContainer_num(String container_num) {
        this.container_num = container_num;
    }

    public String getContainer_size() {
        return container_size;
    }

    public void setContainer_size(String container_size) {
        this.container_size = container_size;
    }

    public String getContainer_status() {
		return container_status;
	}

	public void setContainer_status(String container_status) {
		this.container_status = container_status;
	}

	public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRcode() {
        return rcode;
    }

    public void setRcode(String rcode) {
        this.rcode = rcode;
    }
}
