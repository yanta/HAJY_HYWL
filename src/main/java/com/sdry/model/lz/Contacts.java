package com.sdry.model.lz;

/**
 * @ClassName 联系人
 * @Description Contacts
 * @Author lz
 * @Date 2019年4月12日 10:46:33
 * @Version 1.0
 */
public class Contacts {
    //id
    private Long id;
    //联系人名称
    private String contacts_name;
    //联系人电话
    private String contacts_tel;
    //联系人职位
    private String contacts_position;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContacts_name() {
        return contacts_name;
    }

    public void setContacts_name(String contacts_name) {
        this.contacts_name = contacts_name;
    }

    public String getContacts_tel() {
        return contacts_tel;
    }

    public void setContacts_tel(String contacts_tel) {
        this.contacts_tel = contacts_tel;
    }

    public String getContacts_position() {
        return contacts_position;
    }

    public void setContacts_position(String contacts_position) {
        this.contacts_position = contacts_position;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
