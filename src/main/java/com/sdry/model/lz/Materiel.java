package com.sdry.model.lz;
import com.sdry.utils.PageBean;
import java.util.List;
/**
 * @ClassName Materiel
 * @Description 物料信息
 * @Author lz
 * @Date 2019年4月11日 11:54:41
 * @Version 1.0
 */
public class Materiel extends PageBean{

    //id
    private Long id;
    //容器id
    private Long container_id;
    //容器名称
    private String container_name;
    //物料名称
    private String materiel_name;
    //产品码
    private String materiel_num;
    //简码
    private String brevity_num;
    //物料规格
    private String materiel_size;
	//物料型号
	private String materiel_properties;
	//保质期
	private String quality_guarantee_period;
	//库龄
	private String allowable_storage_days;
	//单位(个、条)
	private String unit;
	//体积
	private String volume;
	//重量
	private String weight;
	//出厂日期
	private String out_date;
	//编码规则
	private String code_rule;
	//出库频率
	private String outgoing_frequency;
	//包装数量
	private String packing_quantity;
	//是否拆箱（0：是 1：否）
	private String is_devanning;
	//需拆箱数
	private String devanning_num;
	//入库类型(精准入库、普通入库)
	private String storage_type;
	//条码类型（0：精准码 1：产品码 2：简码）
	private String barcode_type;
	//顺序号
	private String sequence_number;
	//放置类型（0：货架 1：平地）
	private String placement_type;
	//拆箱后容器id
	private Long removal_container;
	//拆箱后容器名称
	private String removal_container_name;
	//备注
	private String remark;
	//拆箱后产品码
	private String devanning_code;
	//收货地址
	private String delivery_address;
	//上限值
	private int upper_value;
	//下限值
	private int lower_value;
    //需要拆箱数量
    private int ycxnum;
    //供应商id
    private Long customer_id;
    //供应商名称
    private String customer_name;
    //库区id
    private Long region_id;
    //库区名称
    private String region_name;
	//是否綁定托盘
	private int is_bindtray;
	//绑定托盘的类型
	private int tray_type;
	//是否质检
	private int is_check;
	//(供应商实体)
	private Customer customer;
    //zc----原库区id
    private Long src_region_id;
    private String pici;
    private String better_location;
    private int is_ng;
    private int num;
	//(库区实体)
	private WarehouseRegion region;
    //张月添加
    private Long mid;
    private String knum;
    private String bnum;
    private Long receive_detail_quality_id;
    //llm新增（20190820）
    private String sendNum;
    private String singleNum;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getContainer_id() {
		return container_id;
	}

	public void setContainer_id(Long container_id) {
		this.container_id = container_id;
	}

	public String getContainer_name() {
		return container_name;
	}

	public void setContainer_name(String container_name) {
		this.container_name = container_name;
	}

	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}

	public String getMateriel_num() {
		return materiel_num;
	}

	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}

	public String getBrevity_num() {
		return brevity_num;
	}

	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}

	public String getMateriel_size() {
		return materiel_size;
	}

	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}

	public String getMateriel_properties() {
		return materiel_properties;
	}

	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}

	public String getQuality_guarantee_period() {
		return quality_guarantee_period;
	}

	public void setQuality_guarantee_period(String quality_guarantee_period) {
		this.quality_guarantee_period = quality_guarantee_period;
	}

	public String getAllowable_storage_days() {
		return allowable_storage_days;
	}

	public void setAllowable_storage_days(String allowable_storage_days) {
		this.allowable_storage_days = allowable_storage_days;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getOut_date() {
		return out_date;
	}

	public void setOut_date(String out_date) {
		this.out_date = out_date;
	}

	public String getCode_rule() {
		return code_rule;
	}

	public void setCode_rule(String code_rule) {
		this.code_rule = code_rule;
	}

	public String getOutgoing_frequency() {
		return outgoing_frequency;
	}

	public void setOutgoing_frequency(String outgoing_frequency) {
		this.outgoing_frequency = outgoing_frequency;
	}

	public String getPacking_quantity() {
		return packing_quantity;
	}

	public void setPacking_quantity(String packing_quantity) {
		this.packing_quantity = packing_quantity;
	}

	public String getIs_devanning() {
		return is_devanning;
	}

	public void setIs_devanning(String is_devanning) {
		this.is_devanning = is_devanning;
	}

	public String getDevanning_num() {
		return devanning_num;
	}

	public void setDevanning_num(String devanning_num) {
		this.devanning_num = devanning_num;
	}

	public String getStorage_type() {
		return storage_type;
	}

	public void setStorage_type(String storage_type) {
		this.storage_type = storage_type;
	}

	public String getBarcode_type() {
		return barcode_type;
	}

	public void setBarcode_type(String barcode_type) {
		this.barcode_type = barcode_type;
	}

	public String getSequence_number() {
		return sequence_number;
	}

	public void setSequence_number(String sequence_number) {
		this.sequence_number = sequence_number;
	}

	public String getPlacement_type() {
		return placement_type;
	}

	public void setPlacement_type(String placement_type) {
		this.placement_type = placement_type;
	}

	public Long getRemoval_container() {
		return removal_container;
	}

	public void setRemoval_container(Long removal_container) {
		this.removal_container = removal_container;
	}

	public String getRemoval_container_name() {
		return removal_container_name;
	}

	public void setRemoval_container_name(String removal_container_name) {
		this.removal_container_name = removal_container_name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getDevanning_code() {
		return devanning_code;
	}

	public void setDevanning_code(String devanning_code) {
		this.devanning_code = devanning_code;
	}

	public String getDelivery_address() {
		return delivery_address;
	}

	public void setDelivery_address(String delivery_address) {
		this.delivery_address = delivery_address;
	}

	public int getUpper_value() {
		return upper_value;
	}

	public void setUpper_value(int upper_value) {
		this.upper_value = upper_value;
	}

	public int getLower_value() {
		return lower_value;
	}

	public void setLower_value(int lower_value) {
		this.lower_value = lower_value;
	}

	public int getYcxnum() {
		return ycxnum;
	}

	public void setYcxnum(int ycxnum) {
		this.ycxnum = ycxnum;
	}

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public Long getRegion_id() {
		return region_id;
	}

	public void setRegion_id(Long region_id) {
		this.region_id = region_id;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	public int getIs_bindtray() {
		return is_bindtray;
	}

	public void setIs_bindtray(int is_bindtray) {
		this.is_bindtray = is_bindtray;
	}

	public int getTray_type() {
		return tray_type;
	}

	public void setTray_type(int tray_type) {
		this.tray_type = tray_type;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Long getSrc_region_id() {
		return src_region_id;
	}

	public void setSrc_region_id(Long src_region_id) {
		this.src_region_id = src_region_id;
	}

	public WarehouseRegion getRegion() {
		return region;
	}

	public void setRegion(WarehouseRegion region) {
		this.region = region;
	}

	public Long getMid() {
		return mid;
	}

	public void setMid(Long mid) {
		this.mid = mid;
	}

	public String getKnum() {
		return knum;
	}

	public void setKnum(String knum) {
		this.knum = knum;
	}

	public String getBnum() {
		return bnum;
	}

	public void setBnum(String bnum) {
		this.bnum = bnum;
	}

	public String getPici() {
		return pici;
	}

	public void setPici(String pici) {
		this.pici = pici;
	}

	public String getBetter_location() {
		return better_location;
	}

	public void setBetter_location(String better_location) {
		this.better_location = better_location;
	}

	public int getIs_check() {
		return is_check;
	}

	public void setIs_check(int is_check) {
		this.is_check = is_check;
	}

	public int getIs_ng() {
		return is_ng;
	}

	public void setIs_ng(int is_ng) {
		this.is_ng = is_ng;
	}

	public Long getReceive_detail_quality_id() {
		return receive_detail_quality_id;
	}

	public void setReceive_detail_quality_id(Long receive_detail_quality_id) {
		this.receive_detail_quality_id = receive_detail_quality_id;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public String getSendNum() {
		return sendNum;
	}

	public void setSendNum(String sendNum) {
		this.sendNum = sendNum;
	}

	public String getSingleNum() {
		return singleNum;
	}

	public void setSingleNum(String singleNum) {
		this.singleNum = singleNum;
	}
}
