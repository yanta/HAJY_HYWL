package com.sdry.model.lz;

import java.util.List;

/**
 * 
 * @ClassName InventoryDetail
 * @Description 盘点详细
 * @Author lz
 * @Date 2019年3月8日 11:46:52
 * @Version 1.0
 */
public class InventoryDetail {

	// ID
	private Long id;
	// 订单ID
	private Long order_id;
	// 物料名称
	private String materiel_name;
	// 实际数量
	private Long actual_quantity;
	// 理论数量
	private Long theoretical_quantity;
	// 差异数量
	private Long difference_quantity;
	// 产品码
	private String materiel_num;
	// 简码
	private String brevity_num;
	// 物料ID
	private Long materiel_id;
	// 库区名称
	private String region_name;
	//状态
	private int tab;
	private List<CodeMark> receiveCode; 
	
	/**jyy-条码详情（三级）*/
	private List<InventoryDetailCode> inventoryDetailCode; 
	
	/**jyy-库位编号*/
	private String location_num;
	
	/**jyy-复盘数量*/
	private int check_num;
	
	/**jyy-复盘差异*/
	private int check_diff;
	
	public int getCheck_num() {
		return check_num;
	}

	public void setCheck_num(int check_num) {
		this.check_num = check_num;
	}

	public int getCheck_diff() {
		return check_diff;
	}

	public void setCheck_diff(int check_diff) {
		this.check_diff = check_diff;
	}

	public List<InventoryDetailCode> getInventoryDetailCode() {
		return inventoryDetailCode;
	}

	public void setInventoryDetailCode(List<InventoryDetailCode> inventoryDetailCode) {
		this.inventoryDetailCode = inventoryDetailCode;
	}

	public String getLocation_num() {
		return location_num;
	}

	public void setLocation_num(String location_num) {
		this.location_num = location_num;
	}

	public String getRegion_name() {
		return region_name;
	}

	public void setRegion_name(String region_name) {
		this.region_name = region_name;
	}

	// 物料规格
	private String materiel_size;
	// 物料属性
	private String materiel_properties;

	public String getMateriel_size() {
		return materiel_size;
	}

	public void setMateriel_size(String materiel_size) {
		this.materiel_size = materiel_size;
	}

	public String getMateriel_properties() {
		return materiel_properties;
	}

	public void setMateriel_properties(String materiel_properties) {
		this.materiel_properties = materiel_properties;
	}

	// 供应商ID
	private Long customer_id;
	private String customer_name;
	// 单位(个、条)
	private String unit;

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getOrder_id() {
		return order_id;
	}

	public void setOrder_id(Long order_id) {
		this.order_id = order_id;
	}

	public String getMateriel_name() {
		return materiel_name;
	}

	public void setMateriel_name(String materiel_name) {
		this.materiel_name = materiel_name;
	}

	public Long getActual_quantity() {
		return actual_quantity;
	}

	public void setActual_quantity(Long actual_quantity) {
		this.actual_quantity = actual_quantity;
	}

	public Long getTheoretical_quantity() {
		return theoretical_quantity;
	}

	public void setTheoretical_quantity(Long theoretical_quantity) {
		this.theoretical_quantity = theoretical_quantity;
	}

	public Long getDifference_quantity() {
		return difference_quantity;
	}

	public void setDifference_quantity(Long difference_quantity) {
		this.difference_quantity = difference_quantity;
	}

	public String getMateriel_num() {
		return materiel_num;
	}

	public void setMateriel_num(String materiel_num) {
		this.materiel_num = materiel_num;
	}

	public String getBrevity_num() {
		return brevity_num;
	}

	public void setBrevity_num(String brevity_num) {
		this.brevity_num = brevity_num;
	}

	public Long getMateriel_id() {
		return materiel_id;
	}

	public void setMateriel_id(Long materiel_id) {
		this.materiel_id = materiel_id;
	}	

	public Long getCustomer_id() {
		return customer_id;
	}

	public void setCustomer_id(Long customer_id) {
		this.customer_id = customer_id;
	}

	public String getCustomer_name() {
		return customer_name;
	}

	public void setCustomer_name(String customer_name) {
		this.customer_name = customer_name;
	}

	public int getTab() {
		return tab;
	}

	public void setTab(int tab) {
		this.tab = tab;
	}

	public List<CodeMark> getReceiveCode() {
		return receiveCode;
	}

	public void setReceiveCode(List<CodeMark> receiveCode) {
		this.receiveCode = receiveCode;
	}
}
