package com.sdry.model.lz;
/**
 * 待发货区数量和该物料拆箱的总数量
 * @ClassName WaitNumAndTotalNum
 * @Description 待发货区数量和该物料拆箱的总数量
 * @Author lz
 * @Date 2019年6月2日 10:34:29
 * @Version 1.0
 */
public class WaitNumAndTotalNum {

	//该种物料待发货区数量
	private String wait_num;
	//该物料可拆想的总数
	private String total_num;
	
	public String getWait_num() {
		return wait_num;
	}
	public void setWait_num(String wait_num) {
		this.wait_num = wait_num;
	}
	public String getTotal_num() {
		return total_num;
	}
	public void setTotal_num(String total_num) {
		this.total_num = total_num;
	}
	
	
}
