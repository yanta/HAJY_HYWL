package com.sdry.model.lz;

/**
 * @ClassName Post
 * @Description 岗位信息
 * @Author lz
 * @Date 2019年4月11日 11:42:03
 * @Version 1.0
 */
public class Post {

    //id
    private Long id;
    //所属部门id
    private Long dept_id;
    //部门名称
    private String dept_name;
    //岗位编号
    private String post_num;
    //岗位名称
    private String post_name;
    //状态
    private int status;
    //创建日期
    private String create_time;
    //备注
    private String remark;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDept_id() {
        return dept_id;
    }

    public void setDept_id(Long dept_id) {
        this.dept_id = dept_id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public String getPost_num() {
        return post_num;
    }

    public void setPost_num(String post_num) {
        this.post_num = post_num;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
