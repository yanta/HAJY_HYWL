package com.sdry.mapper.ljq;

import java.util.List;
import java.util.Map;

import com.sdry.model.lz.LossSpillover;

public interface ApprovalManagerMapper {

	//分页查询我提交的审批
	public List<LossSpillover> ICommitedQueryPage(Map<String ,Object> map);
	//获取我提交的审批总数
	public int ICommitedQueryCount(Map<String ,Object> map);
	
	//完成审批
	public int CompleteApproval(Map<String,Object> map);
	
	//分页查询要我审批的
	public List<LossSpillover> IApprovaledQueryPage(Map<String ,Object> map);
	//获取要我审批的总数
	public int IApprovaledQueryCount(Map<String,Object> map);
}
