package	com.sdry.mapper.llm;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.llm.LlmCodemarkSwitch;
/**
 *
 *@ClassName: LlmCodemarkSwitchService
 *@Description: 条码类型转换
 *@Author llm
 *@Date 2019-08-07 18:36:43
 *@version 1.0
*/
public interface LlmCodemarkSwitchMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public LlmCodemarkSwitch queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitch> queryAllByMution(LlmCodemarkSwitch param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<LlmCodemarkSwitch> findPageByMution(LlmCodemarkSwitch param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(LlmCodemarkSwitch param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(LlmCodemarkSwitch param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(LlmCodemarkSwitch param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
}
