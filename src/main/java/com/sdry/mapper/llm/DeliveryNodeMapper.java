package	com.sdry.mapper.llm;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.llm.DeliveryNode;
import com.sdry.model.llm.DeliveryOutStock;
import com.sdry.model.lz.OutStockOrder;
import com.sdry.model.lz.OutStockOrderDetail;
import com.sdry.utils.Page;

/**
 *
 *@ClassName: DeliveryNodeService
 *@Description: 发货单
 *@Author llm
 *@Date 2019-07-17 14:14:41
 *@version 1.0
*/
public interface DeliveryNodeMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public DeliveryNode queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryNode> queryAllByMution(DeliveryNode param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryNode> findPageByMution(DeliveryNode param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(DeliveryNode param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(DeliveryNode param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(DeliveryNode param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**   
	 * 根据送货单ID分页查询出库单
	 * @param parseLong
	 * @return             
	 */
	public List<OutStockOrder> queryStockOutByDeliveryId(DeliveryOutStock param);
	/**   
	 * 根据送货单ID查询出库单总数 
	 * @param parseLong
	 * @return             
	 */
	public Integer selectCountStockOutByDeliveryId(DeliveryOutStock param);
	/**   
	 * 分页查询所有出库单   
	 * @return             
	 */
	public List<OutStockOrder> queryAllStockOut(Page page);
	/**   
	 * 统计出库单总数   
	 * @return             
	 */
	public Integer selectCountAllStockOut();
	/**   
	 * 根据登录人查询所有送货单 
	 * @param parseLong
	 * @return             
	 */
	public List<DeliveryNode> queryDeliveryByUid(long userId);
	/**   
	 * 根据送货单ID查询所有出库单   
	 * @param deliveryId
	 * @return             
	 */
	public List<OutStockOrder> queryStockOutByDeliveryId2(long deliveryId);
	/**   
	 * 根据出库单ID查询所有出库单详情  
	 * @param orderId
	 * @return             
	 */
	public List<OutStockOrderDetail> queryOutStockDetailByOrderId(long orderId);
	/**   
	 * 修改出库单详情   
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @param string6             
	 */
	public int updateById2(@Param("detailId")String string, @Param("fielName")String string2, @Param("fielValue")String string3, 
			@Param("fielName2")String string4, @Param("fielValue2")String string5, @Param("tableName")String string6);
	/**   
	 * 根据用户ID查出库单  
	 * @param parseLong
	 * @return             
	 */
	public List<OutStockOrder> queryStockOutByUserId(long userId);
	/**   
	 * 修改出库单详情   
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 * @param string5
	 * @param string6  
	 * @param string7
	 * @param string8            
	 */
	public int updateById3(@Param("detailId")String string, @Param("fielName")String string2, @Param("fielValue")String string3, 
			@Param("fielName2")String string4, @Param("fielValue2")String string5, 
			@Param("fielName3")String string6, @Param("fielValue3")String string7, @Param("tableName")String string8);
	
	/**   
	 * 修改出库单详情   
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 */
	public int updateById4(@Param("detailId")String string, @Param("fielName")String string2, @Param("fielValue")String string3, 
			@Param("tableName")String string4);
	
	/**   
	 * 根据物料ID改库存 
	 * @param string
	 * @param string2
	 * @param string3
	 * @param string4
	 */
	public int updateInventoryByMid(@Param("mid")String string, @Param("fielName")String string2, @Param("fielValue")String string3, 
			@Param("tableName")String string4);
	
}
