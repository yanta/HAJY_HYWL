package com.sdry.mapper.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.llm.LlmInventoryInfo;
import com.sdry.model.llm.LlmRegionStock;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.llm.LocationStock;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcSysUserEntity;

/**
 * 业务查询Mapper
 * @Title:InquiryBusinessMapper
 * @Package com.sdry.mapper.llm
 * @author llm
 * @date 2019年4月23日
 * @version 1.0
 */
public interface InquiryBusinessMapper {

	/**
	 * APP 根据物料名称查库存信息	
	 * @Title:appGetInventoryInfoByMateriel   
	 * @param materielName
	 * @return
	 */
	List<LlmInventoryInfo> appGetInventoryInfoByMateriel(String materielName);
	
	/**
	 * 分页查库存信息	
	 * @Title:appGetInventoryInfoByMateriel   
	 * @param map
	 * @return
	 */
	List<LlmInventoryInfo> listPageInventoryInfo(Map<String, Object> map);
	
	/**
	 * 根据条件获取库存信息总行数
	 * @Title:getCountInventoryInfo   
	 * @param llmInventoryInfo
	 * @return
	 */
	Integer getCountInventoryInfo(LlmInventoryInfo llmInventoryInfo);

	/**   
	 * 根据ID查询库区
	 * @Title:getWarehouseById   
	 * @param id
	 * @return             
	 */
	WarehouseRegion getWarehouseRegionById(Long id);
	
	/**   
	 * 根据ID查询库位
	 * @Title:getWarehouseById   
	 * @param id
	 * @return             
	 */
	WarehouseRegionLocation getWarehouseRegionLocationById(Long id);
	
	/**   
	 * 查询所有库区
	 * @Title:getAllWarehouseRegion   
	 * @return             
	 */
	List<WarehouseRegion> getAllWarehouseRegion();

	/**   
	 * 查询所有库位
	 * @Title:getAllWarehouseRegionLocation   
	 * @return             
	 */
	List<WarehouseRegionLocation> getAllWarehouseRegionLocation();

	/**   
	 * 查询数量统计总行数
	 * @Title:materielQuantityStatistics   
	 * @param materielName
	 * @return             
	 */
	int getCountMaterielQuantityStatistics(String materielName);


	/**
	 * 根据物料ID查询入库数量
	 * @Title:getPutStockQuantityByMid   
	 * @param mid
	 * @return
	 */
	List<Integer> getPutStockQuantityByMid(Long mid);
	
	/**
	 * 根据物料ID查询库存数量
	 * @Title:getInventoryQuantityByMid   
	 * @param mid
	 * @return
	 */
	List<Integer> getInventoryQuantityByMid(Long mid);
	
	/**
	 * 根据物料名称查询出库数量
	 * @Title:getOutStockQuantityByMname   
	 * @param mname
	 * @return
	 */
	List<Long> getOutStockQuantityByMname(String mname);
	

	
	/**   
	 * 根据员工ID查数据
	 * @Title:getUserById   
	 * @param approval_id
	 * @return             
	 */
	ZcSysUserEntity getUserById(Long id);
	
	/**   
	 * 根据退库单ID查上架记录总条数
	 * @Title:getPutawayByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getPutawayByCancellingStockId(Long id);

	
	/**   
	 * 根据退库单ID查入库记录总条数
	 * @Title:getPutStockByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getPutStockByCancellingStockId(Long id);
	
	/**   
	 * 根据退库单ID查验收记录总条数
	 * @Title:getCheckStockByCancellingStockId   
	 * @param id
	 * @return             
	 */
	int getCheckStockByCancellingStockId(Long id);

	/**   
	 * 获取仓库库位总行数
	 * @Title:getCountLocationStock   
	 * @return             
	 */
	int getCountLocationStock();

	/**   
	 * 获取仓库库位物料信息
	 * @Title:getAllLocationStock   
	 * @return             
	 */
	List<LocationStock> getAllLocationStock();
	
	/**
	 * 根据库位ID获取库存信息
	 * @Title:getMaterielByLocationId   
	 * @param id
	 * @return
	 */
	List<LocationStock> getMaterielByLocationId(Long id);
	
	/**   
	 * app端获取所有仓库
	 * @Title:appGetStock   
	 * @return             
	 */
	List<LlmWarehouseStock> appGetStock();
	
	/**   
	 * app端 根据仓库ID获取库区
	 * @Title:appGetStock   
	 * @return             
	 */
	List<LlmRegionStock> appGetRegionStockByWarehouseId(Long id);
	
	/**   
	 * app端 根据库区ID获取库位
	 * @Title:appGetStock   
	 * @return             
	 */
	List<WarehouseRegionLocation> appGetLocationStockByRegionId(Long id);
	
}
