package com.sdry.mapper.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.llm.LlmCancellingStockReason;

/**
 * 退库原因
 * @Title:CancellingReasonMapper
 * @Package com.sdry.mapper.llm
 * @author llm
 * @date 2019年5月15日
 * @version 1.0
 */
public interface CancellingStockReasonMapper {

	/**
	 * 获取退库原因总行数
	 * @Title:getCountCancellingStockReason  
	 * @param cancellingStockReason
	 * @return
	 */
	Integer getCountCancellingStockReason(LlmCancellingStockReason cancellingStockReason);
	
	/**
	 * 分页查询退库原因
	 * @Title:listPageCancellingStockReason   
	 * @param map 参数集合
	 * @return
	 */
	List<LlmCancellingStockReason> listPageCancellingStockReason(Map<String, Object> map);
	
	/**
	 * 新增退库原因
	 * @Title:saveCancellingStockReason  
	 * @param cancellingStockReason 退库原因实体类
	 * @return
	 */
	Integer saveCancellingStockReason(LlmCancellingStockReason cancellingStockReason);
	
	/**
	 * 修改退库原因
	 * @Title:updateCancellingStockReason   
	 * @param cancellingStockReason 退库原因实体类
	 * @return
	 */
	Integer updateCancellingStockReason(LlmCancellingStockReason cancellingStockReason);
	
	/**
	 * 删除退库原因
	 * @Title:deleteCancellingStockReason
	 * @param id 退库原因ID
	 * @return
	 */
	Integer deleteCancellingStockReason(Long id);
	
	/**
	 * 获取所有退库原因
	 * @Title:getAllCancellingStockReason   
	 * @return
	 */
	List<LlmCancellingStockReason> getAllCancellingStockReason();
}
