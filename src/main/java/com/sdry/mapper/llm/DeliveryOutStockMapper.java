package	com.sdry.mapper.llm;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.llm.DeliveryOutStock;

/**
 *
 *@ClassName: DeliveryOutStockService
 *@Description: 发货单和出库单的中间表
 *@Author llm
 *@Date 2019-07-17 16:39:44
 *@version 1.0
*/
public interface DeliveryOutStockMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public DeliveryOutStock queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryOutStock> queryAllByMution(DeliveryOutStock param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<DeliveryOutStock> findPageByMution(DeliveryOutStock param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(DeliveryOutStock param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(DeliveryOutStock param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(DeliveryOutStock param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 出库单ID拼接的字符串
	 * @param deliveryId 送货单ID
	 * @return 影响行数
	*/
	public Integer delete(@Param("ids")String ids, @Param("deliveryId")String deliveryId);
}
