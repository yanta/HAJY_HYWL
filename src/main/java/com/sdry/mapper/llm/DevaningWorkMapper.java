package com.sdry.mapper.llm;

import java.util.List;

import com.sdry.model.llm.DevaningWork;

/**
 * 拆箱工作量
 * @Package com.sdry.service.llm
 * @author llm
 * @date 2019年7月31日
 * @version 1.0
 */
public interface DevaningWorkMapper {

	/**   
	 * 保存
	 * @param work
	 * @return             
	 */
	Long insert(DevaningWork work);

	/**  
	 * 根据拆箱记录编号查工作量 
	 * @param num
	 * @return             
	 */
	List<DevaningWork> queryWorkByNum(String num);

}
