package com.sdry.mapper.llm;

import java.util.List;
import java.util.Map;

import com.sdry.model.llm.LlmCodemarkReturn;
import com.sdry.model.llm.DevaningLog;
import com.sdry.model.llm.LlmCancellingStocksDetail;
import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;

import org.apache.ibatis.annotations.Param;

/**
 * 退库单详情Mapper
 * @Title:CancellingStockDetailMapper
 * @Package com.sdry.mapper.llm
 * @author llm
 * @date 2019年5月14日
 * @version 1.0
 */
public interface CancellingStockDetailMapper {

	/**
	 * 获取退库单详情总行数
	 * @Title:getCountCancellingStockDetail   
	 * @param csid 退库单ID
	 * @return
	 */
	Integer getCountCancellingStockDetail(LlmCancellingStocksDetail cancellingStockDetail);
	
	/**
	 * 分页查询退库单详情
	 * @Title:listPageCancellingStockDetail   
	 * @param map 参数集合
	 * @return
	 */
	List<LlmCancellingStocksDetail> listPageCancellingStockDetail(Map<String, Object> map);
	
	/**
	 * 新增退库单详情
	 * @Title:saveCancellingStockDetail   
	 * @param cancellingStockDetail 退库单详情实体类
	 * @return
	 */
	Integer saveCancellingStockDetail(LlmCancellingStocksDetail cancellingStockDetail);
	
	/**
	 * 修改退库单详情
	 * @Title:updateCancellingStockDetail   
	 * @param cancellingStockDetail 退库单详情实体类
	 * @return
	 */
	Integer updateCancellingStockDetail(LlmCancellingStocksDetail cancellingStockDetail);
	
	/**
	 * 删除退库单详情
	 * @Title:deleteCancellingStockDetail
	 * @param id 退库单详情ID
	 * @return
	 */
	Integer deleteCancellingStockDetail(Long id);

	/**   
	 * 根据退库单号查询详情
	 * @Title:getCancellingStockDetailById   
	 * @param cancellingNumber 退库单号
	 * @return             
	 */
	List<LlmCancellingStocksDetail> getCancellingStockDetailByNumber(String cancellingNumber);
	
	/**   
	 * 根据退库单详情ID查询退库单详情
	 * @Title:getCancellingStockDetailByDetailId   
	 * @param id
	 * @return             
	 */
	LlmCancellingStocksDetail getCancellingStockDetailByDetailId(Long id);
	
	/**  
	 * 根据物料ID获取库存数量 
	 * @Title:getQuantityByMid   
	 * @param id
	 * @return             
	 */
	//ZcInventoryInfoEntity getQuantityByMid(Long id);
	List<ZcInventoryInfoEntity> getQuantityByMid(Map<String, Object> map);
	
	/**   
	 * @Title:getCountMaterielByGood   
	 * @param map
	 * @return             
	 */
	Integer getCountMaterielByGood(Map<String, Object> map);

	/**   
	 * @Title:getCountMaterielByBad   
	 * @param map
	 * @return             
	 */
	Integer getCountMaterielByBad(Map<String, Object> map);

	/**   
	 * @Title:listPageMaterielByGood   
	 * @param map
	 * @return             
	 */
	List<Materiel> listPageMaterielByGood(Map<String, Object> map);

	/**   
	 * @Title:listPageMaterielByBad   
	 * @param map
	 * @return             
	 */
	List<Materiel> listPageMaterielByBad(Map<String, Object> map);
	
	/**   
	 * 修改退库物料数量
	 * @Title:updateCancellingStockDetail2   
	 * @param map
	 * @return             
	 */
	//int updateCancellingStockDetail2(Map<String, Object> map);
	int updateCancellingStockDetail2(@Param("id")String id, @Param("actualQuantity")String actualQuantity,
									 @Param("single_num")String single_num, @Param("big_num")String big_num,
									 @Param("small_num")String small_num);

	/**   
	 * 根据物料ID和退库单号查详情
	 * @Title:getCancellingStockDetailByMidAndNumber   
	 * @param cancellingStockDetail
	 * @return             
	 */
	List<LlmCancellingStocksDetail> getCancellingStockDetailByMidAndNumber(LlmCancellingStocksDetail cancellingStockDetail);
	
	/**   
	 * 修改库存数量
	 * @Title:updateInventory   
	 * @param map             
	 */
	Integer updateInventory(Map<String, Object> map);

	/**   
	 * 根据仓库ID分页查询物料
	 * @Title:getCountMaterielByWarehouseId   
	 * @param map
	 * @return             
	 */
	int getCountMaterielByWarehouseId(Map<String, Object> map);

	/**   
	 * 根据仓库ID分页查询物料
	 * @Title:listPageMaterielByWarehouseId   
	 * @param map
	 * @return             
	 */
	List<Materiel> listPageMaterielByWarehouseId(Map<String, Object> map);
	
	/**   
	 * 修改拆箱总数
	 * @Title:updateUnpacking   
	 * @param map             
	 */
	Integer updateUnpacking(Map<String, Object> map);

	/**   
	 * 查询不良品数量
	 * @Title:getBadQuantityByMid   
	 * @param map
	 * @return             
	 */
	ZcRejectsWarehouseEntity getBadQuantityByMid(Map<String, Object> map);

	/**   
	 * 查询良品数量
	 * @Title:getGoodQuantityByMid   
	 * @param map
	 * @return             
	 */
	ZcInventoryInfoEntity getGoodQuantityByMid(Map<String, Object> map);
	
	/**   
	 * 删除不良品库存数据
	 * @Title:deleteRejectsWarehouse   
	 * @param id             
	 */
	Integer deleteRejectsWarehouse(Long id);

	/**   
	 * 修改不良品库存数据
	 * @Title:updateRejectsWarehouse   
	 * @param map             
	 */
	Integer updateRejectsWarehouse(Map<String, Object> map);
	
	/**
	 * 根据物料ID查库存
	 * @param materialId
	 * @return
	 */
	List<ZcInventoryInfoEntity> getInventoryByMid(Long materialId);
	
	/**
	 * 刪除庫存
	 * @param id
	 */
	Integer deleteInventoryById(Long id);
	
	/**
	 * 查不良品
	 * @param materielId
	 * @return
	 */
	List<ZcRejectsWarehouseEntity> queryRejectsByMaterielId(Long materielId);
	
	/**   
	 * 根据id查供应商
	 * @Title:getCustomerById   
	 * @param id
	 * @return             
	 */
	Customer getCustomerById(Long id);

	/**
	 * 修改退库单详细的状态
	 * @param id
	 * @return
	 */
	int updateCancellingStockDetail3(Long id);

	int updateCancellingStockDetail4(Map<String, Object> map);
	
	/**
	 * 根据出库单详细修改出库单的实际值
	 */
	int updateCancellingStockDetailActNum(@Param("materiel_id")String materiel_id, @Param("act_num")String act_num,
										 @Param("single_num")String single_num, @Param("big_num")String big_num,
										 @Param("small_num")String small_num);
	
	/**   
	 * @param codemark
	 * @return             
	 */
	Integer insert(LlmCodemarkReturn codemark);

	/**   
	 * @param detailId
	 * @return             
	 */
	Integer deleteByDetailId(Long detailId);

	/**   
	 * @param detailId
	 * @return             
	 */
	List<LlmCodemarkReturn> queryCodemarkById(Long detailId);
	/**   
	 * @param code
	 * @return             
	 */
	CodeMark queryCodeByCode(String code);
	
	/**   
	 * @Title:reductionInventory   
	 * @param mid	物料ID
	 * @param pici	物料批次
	 * @param num	退库数量             
	 */
	int reductionInventory(@Param("mid")String mid, @Param("pici")String pici, @Param("num")String num);

	/**   
	 * 根据物料ID查良品数量  
	 * @param parseLong
	 * @return             
	 */
	Integer getGoodQuantityInInventoryInfo(long mid);

	/**   
	 * 根据物料ID查不良品数量 
	 * @param parseLong
	 * @return             
	 */
	Integer getBadQuantityInRejectsWarehouse(long mid);
	
	/**   
	 * 查询精准码关联的物料的总行数
	 * @param materiel
	 * @return             
	 */
	int getCountCodemark(Materiel materiel);

	/**   
	 * 分页查询查询精准码关联的物料
	 * @param map
	 * @return             
	 */
	List<Materiel> listPageCodemark(Map<String, Object> map);
	
	/**   
	 * 根据入库单详情ID查询条码 
	 * @param detailId
	 * @return             
	 */
	List<CodeMark> queryCodemarkById2(Long detailId);
	
	/**   
	 * 分页查询查询拆箱记录关联的物料
	 * @param map
	 * @return             
	 */
	List<Materiel> listPageDevaning(Map<String, Object> map);
	
	/**   
	 * 查询拆箱前条码 
	 * @param number 拆箱单号
	 * @return             
	 */
	List<DevaningLog> queryCodemarkByNum1(String number);
	
	/**   
	 * 查询拆箱后条码 
	 * @param number 拆箱单号
	 * @return             
	 */
	List<DevaningLog> queryCodemarkByNum2(String number);
	
	/**
	 * 保存拆箱记录
	 * @param devaningLog
	 * @return
	 */
	int insertDevaning(DevaningLog devaningLog);
	
	/**   
	 * 查拆箱记录总数
	 * @param map
	 * @return             
	 */
	int getCountDevaning(Map<String, Object> map);
	
	/**   
	 * @return             
	 */
	int getCountByNum1(String number);
	
	/**   
	 * @return             
	 */
	int getCountByNum2(String number);

	/**
	 * 根据物料查询精简出库的库存(精简出库)
	 * @param materielId
	 * @return
	 */
	List<ZcInventoryInfoEntity> getInventoryOnlyCountByMid(Long materielId);

	/**
	 * 如果这条数据为0删掉该数据(精简出库)
	 * @param id
	 */
	void deleteInventoryOnlyCountById(Long id);

	/**
	 * 修改该库存数量(精简出库)
	 * @param map
	 */
	void updateInventoryOnlyCount(Map<String, Object> map);
	/**   
	 * 查询物料库存数量
	 * @param material_id
	 * @return             
	 */
	Integer getSumQuantityInInventoryInfo(Long mid);
	/**   
	 * 查询物料库存数量
	 * @param material_id
	 * @return             
	 */
	Integer getSumQuantityInInventoryInfoCache(Long mid);
}
