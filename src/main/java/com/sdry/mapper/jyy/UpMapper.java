package com.sdry.mapper.jyy;
import java.util.List;

import com.sdry.model.jyy.ReceiveDetail;
import com.sdry.model.jyy.ReceiveDetailInstock;
import com.sdry.model.lz.LzQueryCriteria;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.jyy.Up;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcInventoryInfoEntity;
/**
 *
 *@ClassName: UpService
 *@Description: 入库记录
 *@Author jyy
 *@Date 2019-05-17 17:20:06
 *@version 1.0
*/
public interface UpMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public Up queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Up> queryAllByMution(Up param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Up> findPageByMution(Up param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(Up param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(Up param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(Up param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**
	 * 添加库存信息
	 * @param zcInventoryInfoEntity
	 */
	public Long insertStock(ZcInventoryInfoEntity zcInventoryInfoEntity);
	/**
	 * 添加库存信息（计数）
	 * @param zcInventoryInfoEntity
	 */
	public Long insertStockOnlyCount(ZcInventoryInfoEntity zcInventoryInfoEntity);
	/**
	 * 新增物料-库存对应关系信息
	 * @param up
	 * @return
	 */
	public Long insertMw(Up up);
	/**
	 * 根据仓库名称查询仓库id
	 * @param cqname 仓库名称
	 * @return 仓库id
	 */
	public Long getwidByWname(String cqname);
	/**
	 * 根据物料id和批次号查询库存信息
	 * @param mid 物料id
	 * @param batch_num 批次号
	 * @return
	 */
	public ZcInventoryInfoEntity selectByMid(@Param("mid")String mid, @Param("batch_num")String batch_num);
	public ZcInventoryInfoEntity selectByMid2(@Param("mid")String mid, @Param("batch_num")String batch_num);
	
	/**
	 * 修改库存信息
	 * @return
	 */
	public Long editStock(ZcInventoryInfoEntity zcInventoryInfoEntity);
	/**
	 * 修改库存信息(计数)
	 * @return
	 */
	public Long editStockOnlyCount(ZcInventoryInfoEntity zcInventoryInfoEntity);
	
	/**
	 * 根据产品码和规格型号查询物料
	 * @return
	 */
	public Materiel queryMaterielByRemarkAndSpecification(@Param("remark")String remark, @Param("materiel_size")String materiel_size, @Param("materiel_properties")String materiel_properties);

	/**
	 * 根据库区id查找库区名称
	 * @param region_id
	 * @return
	 */
	public WarehouseRegion queryRegionNameById(Long region_id);

	/**
	 * 分页查询已收货未入库
	 * @param criteria
	 * @return
	 */
	List<ReceiveDetailInstock> queryReceiveDetailInstockCriteria(LzQueryCriteria criteria);

	/**
	 * 查询已收货未入库的数量
	 * @param criteria
	 * @return
	 */
	int countReceiveDetailInstockCriteria(LzQueryCriteria criteria);

	/**
	 * 给不良品绑定条码
	 * @param receiveDetail
	 */
	void bingdNGCode(ReceiveDetail receiveDetail);

	/**
	 * 减去已收货未入库数量
	 * @param id
	 */
	void updateReceiveDetailInstockCodeNum(@Param("id")Long id, @Param("final_value")Long final_value);
	
	/**
	 * 根据供应商id查询供应商的邮箱
	 * @param customer_id
	 * @return
	 */
	public Customer queryCustomerEmailById(Long customer_id);
}