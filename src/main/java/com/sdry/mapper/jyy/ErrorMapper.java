package com.sdry.mapper.jyy;

import com.sdry.model.jyy.ErrorJyy;

public interface ErrorMapper {

	/**
	 * 添加错误信息
	 * @param errorJyy 错误实体
	 * @return 返回影响行数
	 */
	public int addError(ErrorJyy errorJyy);

	/**
	 * 测试
	 * @param errorJyy
	 * @return
	 */
	public int addTest(ErrorJyy errorJyy);

}
