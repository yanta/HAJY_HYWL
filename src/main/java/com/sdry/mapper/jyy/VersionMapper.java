package com.sdry.mapper.jyy;

import com.sdry.model.jyy.Version;
/**
 * 版本管理Mapper
 * @Title:VersionMapper
 * @Package com.sdry.mapper.llm
 * @author llm
 * @date 2019年3月26日
 * @version 1.0
 */

public interface VersionMapper {

	/**   
	 * 获取最新版本
	 * @Title:getLatestVersion   
	 * @return             
	 */
	Version getLatestVersion();
}
