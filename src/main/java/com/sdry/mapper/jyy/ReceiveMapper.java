package com.sdry.mapper.jyy;
import java.util.List;
import java.util.Map;

import com.sdry.model.jyy.Receive;
import com.sdry.model.jyy.Up;
import com.sdry.model.llm.LlmRegionStock;
import com.sdry.model.llm.LlmWarehouseStock;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegionLocation;
/**
 *
 *@ClassName: ReceiveService
 *@Description: 收货计划
 *@Author jyy
 *@Date 2019-04-18 09:35:01
 *@version 1.0
*/
public interface ReceiveMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public Receive queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Receive> queryAllByMution(Receive param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<Receive> findPageByMution(Receive param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(Receive param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(Receive param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(Receive param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	
	/**
	 * 根据物料产品码查询物料所有信息
	 * @param maNum 物料产品码
	 * @return
	 */
	public Materiel selectMaByMaNum(String maNum);
	
	/**
	 * 根据物料id 和  供应商名称查询物料的仓库信息
	 * @param mid 物料id
	 * @return
	 */
	public List<LlmWarehouseStock> selectWarehouseByMid(Up up);
	/**
	 * 根据物料id和仓库id查询库区信息
	 * @param ws
	 * @return
	 */
	public List<LlmRegionStock> selectRegionStockByWarehouseId(LlmWarehouseStock ws);
	/**
	 * 根据物料id和库区id查询库位信息
	 * @param rs
	 * @return
	 */
	public List<WarehouseRegionLocation> selectLocationStockByRegionId(LlmRegionStock rs);
	
	/**
	 * 根据供应商 名称 查询相关物料信息
	 * @param client
	 * @return
	 */
	public List<Materiel> materielList(Long id);
	/**
	 * 根据物料id查询供应商
	 */
	public List<Customer> selectCustomerByMid(Long id);
	
	/**
	 * 根据物料id查询库区位置
	 * @param mid 物料id
	 * @return
	 */
	public Materiel queryMaterielLocationById(Long mid);
	
	/**   
	 * 根据条件查询物料 
	 * @param materiel 物料实体
	 * @return             
	 */
	public List<Materiel> queryAllMaterielByParam(Materiel materiel);
	/**
	 * 根据条件查询单据（不区分发货单，精简发货）
	 * @param receive
	 * @return
	 */
	public List<Receive> allByMution(Receive receive);
	
    public int restor(Map map);
	
	public int cacel(Map map);
	/**
	 * 根据id条件查询收货数量
	 * @param receive
	 * @return
	 */
	public int  queryCodeNumByid(int id);
	
}
