package com.sdry.mapper.lz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.AbnormalAdjustment;
import com.sdry.model.lz.AbnormalDetail;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.OutStockOrder;

/**
 * @ClassName AbnormalAdjustmentMapper
 * @Description 异常调整
 * @Author lz
 * @Date 2019年4月23日 11:02:33
 * @Version 1.0
 */
public interface AbnormalAdjustmentMapper {
	
	Long addAbnormalAdjustment(AbnormalAdjustment abnormalAdjustment);

	Long deleteAbnormalAdjustmentById(long id);

	Long updateAbnormalAdjustmentById(@Param("id")String id,@Param("fieldName")String fieldName, @Param("fieldValue")String fieldValue);
	Long updateAbnormalDetailById(@Param("id")String id,@Param("fieldName")String fieldName, @Param("fieldValue")String fieldValue);
	List<AbnormalAdjustment> queryAbnormalAdjustmentCriteria(
			LzQueryCriteria criteria);

	int countAbnormalAdjustmentCriteria(LzQueryCriteria criteria);
	
	List<OutStockOrder> queryOutStockOrderByNum(String out_order_num);
	
	List<Materiel>  queryMaCuRe(long cid);
	
	Long addAbnormalDetail(AbnormalDetail AbnormalDetail);
	
	List<Materiel> queryAbnormalDetail(long aid);
	List<AbnormalDetail> queryMateridByAbnormalid(long aid);
	AbnormalDetail queryAbnormalDetailByid(long id);

}
