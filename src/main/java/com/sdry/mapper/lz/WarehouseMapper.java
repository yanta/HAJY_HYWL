package com.sdry.mapper.lz;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Warehouse;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcGeneralQueryEntity;

import java.util.List;

/**
 * @ClassName WarehouseMapper
 * @Description 仓库信息
 * @Author lz
 * @Date 2019年4月9日 10:21:15
 * @Version 1.0
 */
public interface WarehouseMapper {

    Long addWarehouse(Warehouse warehouse);

    Long deleteWarehouseById(long id);

    Long updateWarehouse(Warehouse warehouse);

    List<Warehouse> queryWarehouseCriteria(LzQueryCriteria criteria);

    int countWarehouseCriteria(LzQueryCriteria criteria);


    Long addWarehouseRegion(WarehouseRegion warehouseRegion);

    Long deleteWarehouseRegionById(long id);

    Long updateWarehouseRegion(WarehouseRegion warehouseRegion);

    List<WarehouseRegion> queryWarehouseRegionCriteria(ZcGeneralQueryEntity zcGeneralQueryEntity);

    int countWarehouseRegionCriteria(ZcGeneralQueryEntity zcGeneralQueryEntity);


    Long addWarehouseRegionLocation(WarehouseRegionLocation warehouseRegionLocation);

    Long deleteWarehouseRegionLocationById(long id);

    Long updateWarehouseRegionLocation(WarehouseRegionLocation warehouseRegionLocation);

    List<WarehouseRegionLocation> queryWarehouseRegionLocationCriteria(LzQueryCriteria criteria);

    int countWarehouseRegionLocationCriteria(LzQueryCriteria criteria);
    
    List<Warehouse> queryAllWarehouse();

	List<WarehouseRegion> queryAllWarehouseRegion();
}
