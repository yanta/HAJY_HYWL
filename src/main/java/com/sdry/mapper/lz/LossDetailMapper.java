package com.sdry.mapper.lz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.CodeMark;
import com.sdry.model.lz.LossDetail;
import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzInventoryDetailsCodeEntity;
import com.sdry.model.zc.ZcInventoryInfoEntity;

/**
 * 
 * @ClassName LossDetailMapper
 * @Description 损益单详细
 * @Author lz
 * @Date 2019年3月26日 14:20:06
 * @Version 1.0
 */
public interface LossDetailMapper {

	Long addLossDetail(LossDetail lossDetail);
    List<LossDetail>  queryLossDetail(Long lossid);
    int  countLossDetail(Long lossid);
    Long  updateLossDetail(LossDetail lossDetail);
    List<LossDetail>  queryMidByLossId(Long lossid);
    List<ZcInventoryInfoEntity>  queryMnumByMId(Long mid);
    LossSpillover queryLossById(Long lossid);
    /**   
	 * 根据物料ID找到所有库存的条码   
	 * @param mid
	 * @return             
	 */
	List<CodeMark> queryCodeById(Long mid);
	
	/**   
	 * 根据损益单ID找到所有盘点的条码   
	 * @param mid
	 * @return             
	 */
	List<LzInventoryDetailsCodeEntity> queryCodeById2(@Param("lossId")Long lossId, @Param("mid")Long mid);
}
