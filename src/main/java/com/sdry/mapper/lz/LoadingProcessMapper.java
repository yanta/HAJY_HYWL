package com.sdry.mapper.lz;

import java.util.List;

import com.sdry.model.lz.LoadingProcess;
import com.sdry.model.lz.OutStockOrder;

/**
 * @ClassName LoadingProcessMapper
 * @Description 装车处理
 * @Author lz
 * @Date 2019年4月25日 11:26:47
 * @Version 1.0
 */
public interface LoadingProcessMapper {

	Long addLoadingProcess(LoadingProcess loadingProcess);
	
	Long deleteLoadingProcessById(long id);
	
	List<OutStockOrder> queryOutStockOrderByOutStockOrderNum(
			String out_stock_order_num);

}
