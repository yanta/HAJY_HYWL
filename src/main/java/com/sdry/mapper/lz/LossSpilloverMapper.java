package com.sdry.mapper.lz;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.LossSpillover;
import com.sdry.model.lz.LzQueryCriteria;

/**
 * @ClassName LossSpilloverMapper
 * @Description 损溢管理
 * @Author lz
 * @Date 2019年3月25日 11:08:14
 * @Version 1.0
 */
public interface LossSpilloverMapper {

	Long addLossSpillover(LossSpillover lossSpillover);

	Long deleteLossSpilloverById(long id);
	Long updateLossTabByNum(@Param("num")String num);

	Long updateLossSpilloverById(@Param("id")String id,@Param("fieldName")String fieldName, @Param("fieldValue")String fieldValue);

	List<LossSpillover> queryLossSpilloverCriteria(LzQueryCriteria criteria);

	int countLossSpilloverCriteria(LzQueryCriteria criteria);
}
