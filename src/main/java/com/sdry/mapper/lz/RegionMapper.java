package com.sdry.mapper.lz;

import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Region;

import java.util.List;

/**
 * @ClassName RegionMapper
 * @Description 地区信息
 * @Author lz
 * @Date 2019年5月27日 11:27:36
 * @Version 1.0
 */
public interface RegionMapper {

    Long addRegion(Region region);

    Long deleteRegionById(long id);

    Long updateRegion(Region region);

    List<Region> queryRegionCriteria(LzQueryCriteria criteria);

    int countRegionCriteria(LzQueryCriteria criteria);

    List<Region> queryAllRegion();
}
