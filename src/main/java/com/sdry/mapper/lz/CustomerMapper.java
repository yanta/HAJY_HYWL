package com.sdry.mapper.lz;

import com.sdry.model.lz.Contacts;
import com.sdry.model.lz.Customer;
import com.sdry.model.lz.LzQueryCriteria;
import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WaitSendArea;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.zc.ZcInventoryInfoEntity;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @ClassName CustomerMapper
 * @Description 客户信息
 * @Author lz
 * @Date 2019年4月16日 09:28:04
 * @Version 1.0
 */
public interface CustomerMapper {

    Long addCustomer(Customer customer);

    Long deleteCustomerById(long id);

    Long updateCustomer(Customer customer);

    List<Customer> queryCustomerCriteria(LzQueryCriteria criteria);

    int countCustomerCriteria(LzQueryCriteria criteria);
    
    
    Long addContacts(Contacts contacts);

	Long deleteContactsById(long id);

	Long updateContacts(Contacts contacts);

	List<Contacts> queryContactsCriteria(LzQueryCriteria criteria);

	int countContactsCriteria(LzQueryCriteria criteria);
	
	List<Contacts> queryAllContacts();
	
	List<Customer> queryCustomerCriteriaByType(LzQueryCriteria criteria);

	int countCustomerCriteriaByType(LzQueryCriteria criteria);
	
	List<Materiel> queryInventoryInfoByCustomerId(long id);
	
	List<Customer> queryAllCustomer();
	
	/**
	 * 根据客户id和物料id查询待发货区数量
	 */
	List<WaitSendArea> queryWaitSendAreaCountByCustomerIdAndMaterielId(long materiel_id);
	
	/**
	 * 根据物料id查询库存中小箱的数量
	 * @param id
	 * @return
	 */
	List<ZcInventoryInfoEntity> queryInventorySmallNum(Long materiel_id);
	
	/**
	 * 修改待发货区数量
	 * @param response
	 * @param materiel_id 物料id
	 * @param customer_id 客户id
	 * @param materiel_num 物料数量
	 */
	void updateWaitSendAreaValue(@Param("materiel_id")String materiel_id, @Param("materiel_num")String materiel_num);
	
	/**
	 * 删除待发货区数量为0的记录
	 * @param response
	 * @param materiel_id 物料id
	 */
	void deleteWaitSendAreaValue(String materiel_id);
	/**
	 * 通过客户id查询他的所有仓库
	 * @param id
	 * @return
	 */
	List<WarehouseRegion> selectWareListByCid(Long id);
}
