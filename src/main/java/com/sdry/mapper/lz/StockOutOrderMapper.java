package com.sdry.mapper.lz;

import java.util.List;
import java.util.Map;

import com.sdry.model.jyy.ReceiveDetailQuality;
import com.sdry.model.llm.BarCode;
import com.sdry.model.lz.*;
import com.sdry.model.zc.ZcInventoryInfoEntity;
import com.sdry.model.zc.ZcSysUserEntity;

import org.apache.ibatis.annotations.Param;

/**
 * @ClassName StockOutOrderMapper
 * @Description 出库管理
 * @Author lz
 * @Date 2019年3月26日 14:23:11
 * @Version 1.0
 */
public interface StockOutOrderMapper {

	List<OutStockOrder> queryOutStockOrderCriteria(LzQueryCriteria criteria);

	int countOutStockOrderCriteria(LzQueryCriteria criteria);

	/**
	 * 查询已出库的出库单
	 * @param criteria
	 * @return
	 */
	List<OutStockOrder> queryOutStockOrderCriteria2(LzQueryCriteria criteria);

	/**
	 * 查询已出库的出库单数量
	 * @param criteria
	 * @return
	 */
	int countOutStockOrderCriteria2(LzQueryCriteria criteria);

	Long addStockOutOrder(OutStockOrder order);

	Long addStockOutDetailList(OutStockOrderDetail detail);

	List<OutStockOrderDetail> queryOutStockOrderCriteriaById(LzQueryCriteria criteria);

	int countOutStockOrderCriteriaById(LzQueryCriteria criteria);

	Long updateOutStockOrderById(@Param("id")String id,@Param("fieldName")String fieldName, @Param("fieldValue")String fieldValue);

	Long updateOutStockOrderDetailById(@Param("id")String id,@Param("fieldName")String fieldName, @Param("fieldValue")String fieldValue);

	Long deleteOutStockOrderById(long id);

	Long deleteOutStockOrderDetailById1(long id);
	
	List<OutStockOrder> queryOutStockOrderCriteriaPDA(LzQueryCriteria criteria);

	int countOutStockOrderCriteriaPDA(LzQueryCriteria criteria);
	
	List<Customer> queryAllCustomer();

	List<Staff> queryAllStaff();

	List<Car> queryAllCar();

	List<OutStockOrder> queryAllOutStockOrder();
	
	List<Customer> queryCustomerByName(String customer_name);
	
	Long updateZcInventoryInfoById(@Param("materiel_id")String materiel_id, @Param("res3")String res3);

	List<OutStockOrder> queryOutStockOrderAndroid();

	List<OutStockOrderDetail> queryOutStockOrderDetailByIdAndroid(String order_id);

	ZcInventoryInfoEntity triggerWarning(String zcInventoryInfoId);

	Long updateStockOutOrderDetailStatusAndroid(BarCode barCode);
	
	
	Warehouse queryWarehouseById(long id);
	
	ZcSysUserEntity queryUserById(long id);
	
	Customer queryCustomerById(long id);

	Contacts queryContactsById(long id);
	
	Long updateStockOutOrder(OutStockOrder outStockOrder);
	
	List<Materiel> queryWaitSendAreaAndStock(LzQueryCriteria criteria);

	int countWaitSendAreaAndStock(LzQueryCriteria criteria);
	
	List<ZcInventoryInfoEntity> queryInventoryInfo(LzQueryCriteria criteria);

	int countInventoryInfo(LzQueryCriteria criteria);

	/**
	 * PDA查询所有出库单接口
	 * @return
	 */
	List<OutStockOrder> queryOutStockOrderForPDA();

	/**
	 * PDA根据出库单ID查询出库单详细接口
	 * @return
	 */
	List<OutStockOrderDetail> queryOutStockOrderDetailByIdForPDA(long out_stock_num_id);

	/**
	 * 根据物料ID查询物料
	 * @param material_id 物料id
	 * @return
	 */
	Materiel queryMaterielById(long material_id);

	/**
	 * 根据出库单详细修改出库单的实际值
	 */
	void updateOutStockOrderDetailActNum(@Param("id")String id, @Param("act_num")String act_num,
										 @Param("single_num")String single_num, @Param("big_num")String big_num,
										 @Param("small_num")String small_num);

	/**
	 * 根据出库单id修改出库单状态
	 * @param outStockOrderDetail
	 */
	void updateOutStockOrderStatus(OutStockOrderDetailPDAEntity outStockOrderDetail);

	/**
	 * 提交待发货区和库存物料
	 * @param outStockOrderDetail
	 * @return
	 */
	Long addStockOutOrderDetail(OutStockOrderDetail outStockOrderDetail);

	/**
	 * 根据供应商id查询供应商名称
	 * @param customer_id
	 */
	Customer queryCustomerNameById(Long customer_id);
	
	/**
     * 查找所有供应商
     * @return
     */
	List<Customer> queryAllCustomerByType();
	
	/**
	 * 查找所有库区
	 * @return
	 */
	List<WarehouseRegion> queryAllRegion();
	
	/**
	 * 查询所有主机厂
	 * @throws Exception
	 */
	List<Customer> queryAllCustomerByType1();
	
	Car queryDriver(long  id);
	
	/**
	 * 查询该出库单详细是否存在该物料
	 * @param outStockOrderDetail 出库单详细
	 * @throws Exception
	 */
	List<OutStockOrderDetail> queryExistMaterial(
			OutStockOrderDetail outStockOrderDetail);
	
	/**
	 * 根据出库单id查询出库单状态
	 * @param out_stock_num_id 出库单id
	 * @throws Exception
	 */
	List<OutStockOrder> queryOutStockOrderStatusByOutStockNumId(long out_stock_num_id);

	/**
	 * 根据仓库id查询仓库
	 * @param warehouse_id
	 * @return
	 */
	Warehouse getWarehouseById(Long warehouse_id);

	int addCodeMarkOut(CodeMark codeMark);

	CodeMark queryCodeMarkOutByReciveid(long rid);

	/**
	 * 根据质检单详情id查询质检单
	 * @param receive_detail_id
	 * @return
	 */
	ReceiveDetailQuality queryReceiveDetailQualityById(Long receive_detail_id);

	/**
	 * 根据质检单中的物料id和批次减库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	void reductionInventory(@Param("mid")String mid, @Param("pici")String pici, @Param("num")String num);
	/**
	 * 根据质检单中的物料id和批次减缓存库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	void reductionInventoryCache(@Param("mid")String mid, @Param("pici")String pici, @Param("num")String num);

	/**
	 * 根据精准码删除已出库的精准码
	 * @param code
	 */
	void deleteCodemarkOutByCode(String code);
	
	/**
	 * 根据质检单中的物料id和批次减不良库存
	 * @param mid 物料id
	 * @param pici 物料批次
	 */
	void reductionRejectsWarehouse(@Param("mid")String mid, @Param("pici")String pici, @Param("num")String num);
	
	/**   
	 * @return             
	 */
	List<OutStockOrder> queryDeliveryOrderForPDA();
	

	int cancelout(Map map);
	
	int restorout(Map map);
	/**
	 * PDA查询所有紧急出库单接口
	 * @return
	 */
	List<OutStockOrder> queryUrgencyOrderForPDA();

	/**   
	 * 根据物料ID查询良品区数量
	 * @param material_id
	 * @return             
	 */
	Integer queryGoodQuantity(Long material_id);

	/**   
	 * 根据物料ID查询待发货区数量
	 * @param material_id
	 * @return             
	 */
	Integer queryCacheQuantity(Long material_id);
	
	/**   
	 * 根据物料ID查询待检区数量
	 * @param material_id
	 * @return             
	 */
	Integer queryUnCheckQuantity(Long material_id);

	/**   
	 * 根据出库单详情ID查询出库单类型 
	 * @param id
	 * @return             
	 */
	int queryOutStockOrderByNumber(Long id);
}