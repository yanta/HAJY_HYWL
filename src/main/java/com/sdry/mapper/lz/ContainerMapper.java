package com.sdry.mapper.lz;

import com.sdry.model.lz.Container;
import com.sdry.model.lz.LzQueryCriteria;

import java.util.List;

/**
 * @ClassName ContainerMapper
 * @Description 容器信息
 * @Author lz
 * @Date 2019年4月16日 11:28:23
 * @Version 1.0
 */
public interface ContainerMapper {

    Long addContainer(Container container);

    Long deleteContainerById(long id);

    Long updateContainer(Container container);

    List<Container> queryContainerCriteria(LzQueryCriteria criteria);

    int countContainerCriteria(LzQueryCriteria criteria);
}
