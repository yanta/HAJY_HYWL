package	com.sdry.mapper.barCodeOperation;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.barCodeOperation.BarCodeOperation;
import com.sdry.model.llm.BarCode;
import com.sdry.model.lz.CodeMark;
/**
 *
 *@ClassName: BarCodeOperationService
 *@Description: 条码操作记录
 *@Author tdd
 *@Date 2019-08-07 17:03:56
 *@version 1.0
*/
public interface BarCodeOperationMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public BarCodeOperation queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<BarCodeOperation> queryAllByMution(BarCodeOperation param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<BarCodeOperation> findPageByMution(BarCodeOperation param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(BarCodeOperation param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(BarCodeOperation param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(BarCodeOperation param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
	/**   
	 * 根据用户名查用户ID
	 * @param userId
	 * @return             
	 */
	public Long queryIdByName(String userId);
	/**   
	 * 查询同一物料同一批次的所有条码   
	 * @param materiel_code
	 * @return             
	 */
	public List<CodeMark> selectCountOnSameCodeByCode(String materiel_code);
	/**   
	 * 根据条码查是否质检 
	 * @param code
	 * @return             
	 */
	public Integer queryStatusByCode(String code);
}
