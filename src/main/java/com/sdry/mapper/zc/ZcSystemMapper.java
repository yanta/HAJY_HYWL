package com.sdry.mapper.zc;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.Department;
import com.sdry.model.lz.Post;
import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcPermissionEntity;
import com.sdry.model.zc.ZcSysRoleEntity;
import com.sdry.model.zc.ZcSysRolePermissionEntity;
import com.sdry.model.zc.ZcSysUserEntity;
import com.sdry.model.zc.ZcSysUserRoleEntity;

/**
 * 
 * @ClassName:    ZcSystemMapper   
 * @Description:  系统管理
 * @Author:       zc   
 * @CreateDate:   2019年4月11日 上午10:05:08   
 * @Version:      v1.0
 */
public interface ZcSystemMapper {
	/**
	 * 查询用户信息
	 * @param zcSysUserEntity
	 * @return
	 */
	public ZcSysUserEntity selectSysUserByAccountName(ZcSysUserEntity zcSysUserEntity);
	/**
	 * 查询全部权限列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcPermissionEntity> selectAllMenuList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 新增权限
	 * @param zcPermissionEntity
	 * @return
	 */
	public int insertPermission(ZcPermissionEntity zcPermissionEntity);
	/**
	 * 修改权限
	 * @param zcPermissionEntity
	 * @return
	 */
	public int updatePermission(ZcPermissionEntity zcPermissionEntity);
	/**
	 * 条件删除权限
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int deletePermissionById(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 条件查询用户列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcSysUserEntity> selectSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 通过用户id查询他的角色
	 * @param userId
	 * @return
	 */
	public List<ZcSysRoleEntity> selectRoleListByuserId(Long userId);
	/**
	 * 条件查询用户列表数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 新增用户
	 * @param zcSysUserEntity
	 * @return
	 */
	public int insertSysUser(ZcSysUserEntity zcSysUserEntity);
	/**
	 * 保存用户角色
	 * @param zcSysUserRoleEntity
	 * @return
	 */
	public int insertSysUserRole(ZcSysUserRoleEntity zcSysUserRoleEntity);
	/**
	 * 通过用户id删除用户
	 * @param userId
	 * @return
	 */
	public int deleteSysUserByUserId(Long userId);
	/**
	 * 修改用户
	 * @param zcSysUserEntity
	 * @return
	 */
	public int updateSysUser(ZcSysUserEntity zcSysUserEntity);
	/**
	 * 删除用户角色
	 * @param userId
	 * @return
	 */
	public int deleteSysUserRoleByUserId(Long userId);
	/**
	 * 删除用户
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int deleteUserById(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 条件查询角色列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcSysRoleEntity> selectSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 条件查询角色列表数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 全查角色列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcSysRoleEntity> selectAllSysRoleList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 新增角色
	 * @param zcSysRoleEntity
	 * @return
	 */
	public int insertSysRole(ZcSysRoleEntity zcSysRoleEntity);
	/**
	 * 修改角色
	 * @param zcSysRoleEntity
	 * @return
	 */
	public int updateSysRole(ZcSysRoleEntity zcSysRoleEntity);

	/**
	 * 保存权限
	 * @param zcSysRolePermissionEntity
	 * @return
	 */
	public int saveRoleOperation(ZcSysRolePermissionEntity zcSysRolePermissionEntity);
	/**
	 * 删除权限
	 * @param roleId
	 * @return
	 */
	public int deleteRoleOperation(Long roleId);
	/**
	 * 查询角色的权限
	 * @param roleId
	 * @return
	 */
	public List<ZcSysRolePermissionEntity> selectAllRoleOperation(Long roleId);
	/**
	 * 查询全部部门列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<Department> selectAllDeptList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询岗位列表通过部门id
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<Post> selectAllPostList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 全查用户列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcSysUserEntity> selectAllSysUserList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询用户是否有权限
	 * @param id
	 * @return
	 */
	public int countPermissionByroleId(Long id);
	/**
	 * 核对原密码
	 * @param accountName
	 * @param srcPsd
	 * @return
	 */
	public int checkPsd(@Param("accountName")String accountName,@Param("srcPsd")String srcPsd);
	/**
	 * 修改密码
	 * @param accountName
	 * @param md5Encode
	 * @return
	 */
	public int editPsd(@Param("accountName")String accountName,@Param("newPsd")String newPsd);
	/**
	 * 查看用户是否存在
	 * @param accountName
	 * @return
	 */
	public int countUser(String accountName);
	/**
	 * 删除详情
	 * @param id
	 */
	public void deleteDetails(long id);
	/**
	 * 通过角色权限查菜单
	 * @param string
	 * @return
	 */
	public List<ZcSysRolePermissionEntity> selectAllRoleOperation2Index(
			String id);
	/**
	 * 查询工号是否重复
	 * @param jobNumber
	 * @return
	 */
	public int selectCountByNumber(String jobNumber);
	/**
	 * 根据条件查用户
	 * @param zcSysUserEntity 条件
	 * @return
	 */
	public List<ZcSysUserEntity> selectSysUserByMution(ZcSysUserEntity zcSysUserEntity);
}