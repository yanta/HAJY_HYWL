package com.sdry.mapper.zc;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegion;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcMaterielAndCustomerEntity;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcTrayAndLocationEntity;

/**
 * 上下架管理
 * @ClassName:    ZcUpAndDownMapper   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年6月2日 下午1:35:20   
 * @Version:      v1.0
 */
public interface ZcUpAndDownMapper {
	/**
	 * 上架前查询库位是否为空
	 * @param zcTrayAndLocationEntity
	 * @return
	 */
	public int countTrayAndLocation(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 库位为空上架（新增）
	 * @param zcTrayAndLocationEntity
	 * @return
	 */
	public int bindingTrayAndLocation(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 库位不为空上架（追加）
	 * @param zcTrayAndLocationEntity
	 * @return
	 */
	public int addTrayAndLocation(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 查询库位中的托盘
	 * @param zcTrayAndLocationEntity
	 * @return
	 */
	public ZcTrayAndLocationEntity selectTrayCodeByLocationCode(ZcTrayAndLocationEntity zcTrayAndLocationEntity);
	/**
	 * 下架
	 * @param zcTrayAndLocationEntity
	 * @return
	 */
	public int unbindTrayAndLocation(ZcTrayAndLocationEntity zcTrayAndLocationEntity);
	/**
	 * 上架前查询物料id
	 * @param tray_code
	 * @return
	 */
	public Long selectMid(String tray_code);
	/**
	 * 查询入库类型
	 * @param mid
	 * @return
	 */
	public String selectStorageTypeByMid(Long mid);
	
	/**
	 * 根据托盘码先查询数据库判断该托盘是否已经绑定过
	 * @return
	 */
	public List<ZcTrayAndLocationEntity> queryAllTrayAndLocation();
	/**
	 * 记录下架记录
	 * @param zcTrayAndLocationEntity
	 */
	public void insertUnbindleRecord(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 查询托盘是否已经上架
	 * @param trayCode
	 * @return
	 */
	public int selectYetOnByTrayCode(String trayCode);
	/**
	 * 查询物料所在库区id
	 * @param selectMid
	 * @return
	 */
	public Long selectRegionIdByMid(Long mid);
	/**
	 * 通过库区id和库位条码查询库位是否存在
	 * @param location_code
	 * @param region_id
	 * @return
	 */
	public WarehouseRegionLocation selectLocationIsExist(WarehouseRegionLocation location);
	/**
	 * 扫描托盘条码后推荐容量最大的库位
	 */
	public List<WarehouseRegionLocation> selectTopLocation(Long region_id);
	/**
	 * 查询库区id
	 * @param materiel
	 * @return
	 */
	public Long selectRegionId(Materiel materiel);
	/**
	 * 查询所有库位信息
	 */
	public List<WarehouseRegionLocation> selectTopLocationAllRegion();
	/**
	 * 查询全部库区
	 * @return
	 */
	public List<WarehouseRegion> selectAllRegion();
	/**
	 * 通过库位查询物料条码
	 * @param location_num
	 * @return
	 */
	public List<String> selectCodeByLocationCode(String location_num);
}
