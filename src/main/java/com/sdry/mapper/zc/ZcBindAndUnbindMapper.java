package com.sdry.mapper.zc;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.sdry.model.lz.Materiel;
import com.sdry.model.lz.WarehouseRegionLocation;
import com.sdry.model.zc.ZcMaterielAndTrayEntity;
import com.sdry.model.zc.ZcMaterielNameAndCode;
import com.sdry.model.zc.ZcMaterileMoveInfoEntity;

public interface ZcBindAndUnbindMapper {
	/**
	 * 通过物料精准码查询物料详情
	 * @param materiel_code
	 */
	public Materiel selectMaterielInfoByCode(String materiel_code);
	/**
	 * 查询这个物料条码是否已经绑定过了
	 * @param materiel_code
	 */
	public int selectBindByMaterielCode(String materiel_code);
	/**
	 * 绑定
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int bindingMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 通过物料条码查询物料所在位置
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielLocationByCode(String materiel_code);
	/**
	 * 通过物料条码查询物料所在位置查整个库位的物料
	 * @param materiel_code
	 * @return
	 */
	public List<ZcMaterielAndTrayEntity> selectMaterielLocationByCodeNew(String materiel_code);
	/**
	 * 解绑前查询原托盘物料
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielCodeByTrayCode(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 解绑
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int downAndUnbind(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 绑定前查询托盘是否为空
	 * @param tray_code
	 * @return
	 */
	public ZcMaterielAndTrayEntity selectMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 托盘不为空是追加物料
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public int addMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 通过产品码，规格型号查询物料id
	 * @param zcMaterielAndTrayEntity
	 * @return
	 */
	public Long selectMidByNewCode(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 删除空库位空托盘
	 */
	public void deleteEmpty();
	/**
	 * 解绑托盘物料
	 * @param zcMaterielAndTrayEntity
	 */
	public void unbindMaterielAndTray(ZcMaterielAndTrayEntity zcMaterielAndTrayEntity);
	/**
	 * 推荐最优库位
	 * @param region_id
	 * @return
	 */
	public WarehouseRegionLocation selectTopOneLocation(Long region_id);
	/**
	 * 下架库位推荐（先进先出）
	 */
	public WarehouseRegionLocation selectTopLocationDown(String materiel_num);
	/**
	 * 通过物料条码查询物料名称
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterielNameAndCode selectNameByCode(String materiel_code);
	/**
	 * 通过条码查数量
	 * @param string
	 * @return
	 */
	public int selectNumByCode(String string);
	/**
	 * 通过库位码查询库区是否是不良区
	 * @param location_code
	 * @return
	 */
	public int selectBadRegionByLocation(String location_code);
	/**
	 * 不良品库位推荐
	 * @return
	 */
	public WarehouseRegionLocation selectTopOneLocationBad();
	/**
	 * 通过物料条码查询物料id和批次
	 * @param materiel_code
	 * @return
	 */
	public ZcMaterileMoveInfoEntity selectMaterileMoveInfoByCode(String materiel_code);
	/**
	 * 给质检区加库存
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int insertQualityWare(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 从待检区出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outQuality(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 入良品库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int inGood(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 入不良品库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int inBad(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 从良品库出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outGood(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 入缓存区库存
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int inCache(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 从不良品库出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outBad(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 从缓存区出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outCache(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 入损益区库存
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int inProfitAndLoss(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 损益区出库
	 * @param zcMaterileMoveInfoEntity
	 * @return
	 */
	public int outProfitAndLoss(ZcMaterileMoveInfoEntity zcMaterileMoveInfoEntity);
	/**
	 * 通过条码查询托盘
	 * @param materiel_code
	 * @return
	 */
	public String selectTrayCodeByMcode(String materiel_code);
	/**
	 * 查询托盘是否在库位
	 * @param tray_code
	 * @return
	 */
	public int selectCountByTray(String tray_code);
	/**
	 * 通过物料条码查询物料id和批次,原托盘
	 * @param string
	 * @return
	 */
	public ZcMaterileMoveInfoEntity selectMaterileMoveInfoByCodeSrc(String materiel_code);
	/**
	 * 扫码查询物料位置（下架）
	 * @param materiel_code
	 * @return
	 */
	public List<ZcMaterielAndTrayEntity> selectMaterielLocationByCodeDown(String materiel_code);
	/**
	 * 通过物料条码查询物料是否在对应位置
	 * @param materiel_code
	 * @param location_code 库区编号
	 * @return
	 */
	public List<ZcMaterielAndTrayEntity> selectMaterielLocationByCodeDownLocation(@Param("materiel_code")String materiel_code,@Param("location_code")String location_code);
	public String selectByMaterielCode(String materiel_code);
	
	/**
	 * 根据精准码查询库位集合
	 * @param code
	 * @return
	 */
	public List<WarehouseRegionLocation> queryLocationByCode(String code);
	/**
	 * 查询库位码是否在良品区
	 * @param location_code
	 * @return
	 */
	public int selectCountGoodByLocationCode(String location_code);
	/**
	 * 查询库位码是否在不良品区
	 * @param location_code
	 * @return
	 */
	public int selectCountBadByLocationCode(String location_code);
}
