package com.sdry.mapper.zc;

import java.util.List;

import com.sdry.model.zc.ZcGeneralQueryEntity;
import com.sdry.model.zc.ZcRejectsWarehouseEntity;

/**
 * 不良库管理
 * @ClassName:    ZcRejectsWarehouseMapper   
 * @Description:  
 * @Author:       zc   
 * @CreateDate:   2019年4月25日 下午5:17:02   
 * @Version:      v1.0
 */
public interface ZcRejectsWarehouseMapper {

	/**
	 * 查询不良库列表
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public List<ZcRejectsWarehouseEntity> selectRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询不良库列表数量
	 * @param zcGeneralQueryEntity
	 * @return
	 */
	public int countRejectsWarehouseList(ZcGeneralQueryEntity zcGeneralQueryEntity);
	/**
	 * 查询该绑定是否存在
	 * @param zcRejectsWarehouseEntity
	 * @return
	 */
	public int countInventoryInfoByRegionId(ZcRejectsWarehouseEntity zcRejectsWarehouseEntity);
	/**
	 * 不良库增加数量
	 * @param zcRejectsWarehouseEntity
	 * @return
	 */
	public int addNum(ZcRejectsWarehouseEntity zcRejectsWarehouseEntity);
	/**
	 * 绑定新库
	 * @param zcRejectsWarehouseEntity
	 * @return
	 */
	public int bindingNewRejectsWare(ZcRejectsWarehouseEntity zcRejectsWarehouseEntity);
}
