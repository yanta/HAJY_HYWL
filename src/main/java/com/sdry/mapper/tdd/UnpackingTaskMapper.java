package	com.sdry.mapper.tdd;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sdry.model.tdd.UnpackingTask;
/**
 *
 *@ClassName: UnpackingTaskService
 *@Description: 拆箱任务表
 *@Author tdd
 *@Date 2019-05-14 10:58:34
 *@version 1.0
*/
public interface UnpackingTaskMapper {
	/** 
	 * 根据主键id查询实体
	 * @param id 主键id
	 * @return 实体
	*/
	public UnpackingTask queryById(Long id);
	/** 
	 * 条件查询所有
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackingTask> queryAllByMution(UnpackingTask param);
	/** 
	 * 条件分页查询
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public List<UnpackingTask> findPageByMution(UnpackingTask param);
	/** 
	 * 条件查询记录数
	 * @param param 实体条件
	 * @return 实体集合
	*/
	public Integer selectCountByMution(UnpackingTask param);
	/** 
	 * 插入返回id
	 * @param param 实体条件
	 * @return 主键id
	*/
	public Long insert(UnpackingTask param);
	/** 
	 * 根据主键更新 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	*/
	public Integer update(UnpackingTask param);
	/** 
	 * 根据主键更新状态 返回影响行数
	 * @param param 实体条件
	 * @return 影响行数
	 */
	public Integer updateState(UnpackingTask param);
	/** 
	 * 根据主键拼接的字符串删除返回影响行数
	 * @param ids 主键拼接的字符串
	 * @return 影响行数
	*/
	public Integer delete(String ids);
}
